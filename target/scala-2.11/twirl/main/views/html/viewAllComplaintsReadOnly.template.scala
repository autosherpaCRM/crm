
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object viewAllComplaintsReadOnly_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class viewAllComplaintsReadOnly extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template8[String,String,String,String,List[String],List[Location],List[Workshop],List[String],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(oem:String,dealercode:String,dealerName:String,userName:String,cresList:List[String],locationList:List[Location],workshopList:List[Workshop],distinctFunctions:List[String]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.175*/("""

"""),_display_(/*4.2*/mainPageCRE("AutoSherpaCRM",userName,dealerName,dealercode,oem)/*4.65*/ {_display_(Seq[Any](format.raw/*4.67*/("""

"""),format.raw/*6.1*/("""<style>
 .accordion-toggle:after """),format.raw/*7.26*/("""{"""),format.raw/*7.27*/("""
  
	"""),format.raw/*9.2*/("""font-family:'FontAwesome';
	content:"\f077";
	float: right;
	color: inherit;
"""),format.raw/*13.1*/("""}"""),format.raw/*13.2*/("""
"""),format.raw/*14.1*/(""".panel-heading.collapsed .accordion-toggle:after """),format.raw/*14.50*/("""{"""),format.raw/*14.51*/("""
 
	"""),format.raw/*16.2*/("""content:"\f078";
"""),format.raw/*17.1*/("""}"""),format.raw/*17.2*/("""
"""),format.raw/*18.1*/(""".panel-primary>.panel-heading """),format.raw/*18.31*/("""{"""),format.raw/*18.32*/("""
	"""),format.raw/*19.2*/("""color: #fff;
	background-color: #0288D1;
	border-color: #0288D1;
"""),format.raw/*22.1*/("""}"""),format.raw/*22.2*/("""
"""),format.raw/*23.1*/(""".btn-info """),format.raw/*23.11*/("""{"""),format.raw/*23.12*/("""
	"""),format.raw/*24.2*/("""background-color: #03A9F4;
	border-color: #5B987C!important;
	"""),format.raw/*26.2*/("""}"""),format.raw/*26.3*/("""
	
"""),format.raw/*28.1*/(""".panel-group .panel-collapse """),format.raw/*28.30*/("""{"""),format.raw/*28.31*/("""
    """),format.raw/*29.5*/("""margin-top: 0px !important;
"""),format.raw/*30.1*/("""}"""),format.raw/*30.2*/("""
"""),format.raw/*31.1*/(""".panel-group .panel-heading """),format.raw/*31.29*/("""{"""),format.raw/*31.30*/("""
    """),format.raw/*32.5*/("""padding: 5px;
"""),format.raw/*33.1*/("""}"""),format.raw/*33.2*/("""
"""),format.raw/*34.1*/("""</style>

"""),_display_(/*36.2*/helper/*36.8*/.form(action = routes.CallInteractionController.downloadExcelFile)/*36.74*/ {_display_(Seq[Any](format.raw/*36.76*/("""
"""),format.raw/*37.1*/("""<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading" style="text-align:center"><b>COMPLAINT HISTORY</b></div>
			<div class="panel-body">
			
				<div class="col-md-2">
				<label>Location</label>
				<select class="form-control" id="selLocId" name="location">
					"""),_display_(/*46.7*/for(loc <- locationList) yield /*46.31*/{_display_(Seq[Any](format.raw/*46.32*/("""						
						"""),format.raw/*47.7*/("""<option value=""""),_display_(/*47.23*/loc/*47.26*/.getName()),format.raw/*47.36*/("""">"""),_display_(/*47.39*/loc/*47.42*/.getName()),format.raw/*47.52*/("""</option>
					""")))}),format.raw/*48.7*/("""
				"""),format.raw/*49.5*/("""</select>
				</div>
				<div class="col-md-2">
				<label>Function</label>
				<select class="form-control" id="selFuncId" name="functionName">
							<option value="0">Select Function</option>
							"""),_display_(/*55.9*/for(func <- distinctFunctions) yield /*55.39*/{_display_(Seq[Any](format.raw/*55.40*/("""						
						"""),format.raw/*56.7*/("""<option value=""""),_display_(/*56.23*/func),format.raw/*56.27*/("""">"""),_display_(/*56.30*/func),format.raw/*56.34*/("""</option>
					""")))}),format.raw/*57.7*/("""	
				"""),format.raw/*58.5*/("""</select>
				</div>
				<div class="col-md-2">
				<label>Start Date</label>
				<input type="text" class="datepicker form-control" id="raisedDateId" name="raisedDate" readonly/>
					
				
				</div>
				<div class="col-md-2">
				<label>End Date</label>
				<input type="text" class="datepicker form-control" id="endDateId" name="endDate" readonly/>
					
				
				</div>
				<div class="col-md-2">
				<label>Status</label>
				<select class="form-control" id="complainHistoryID" name="complaintStatus">
					<option value="0">--Select--</option>
					<option value="New">NEW COMPLAINT</option>
					<option value="Assigned">ASSIGNED COMPLAINT</option>
					<option value="Closed">CLOSED COMPLAINT</option>
				</select>
				</div>
				<div class="col-md-1">
				<div class="form-group">
				<label></label>
				<button type="button" class="btn btn-success form-control" onclick="viewComplaintData();">SUBMIT</button>
				</div>
			</div>
			
			<!--  <div class="col-md-1">
				<div class="form-group">
				<label></label>
				<button type="submit" class="btn btn-success">Download CSV Format</select>
				</button>
				</div>
			</div> -->
		</div>
	</div>
</div>
</div>



<div class="row" id="openComplaintID">
  <div class="col-lg-12">
    <div class="panel panel-primary">
      <div class="panel-heading nameappend" style="text-align:center;">  
          <b>COMPLAINTS LIST</b>
   </div>
		<div class="panel-body">
		<div class="dataTable_wrapper">  
	
<table  class="table table-striped table-bordered table-hover" style="width:100%" cellspacing="0" id="ComplaintID">
        <thead>
            <tr>

            <th>COMPLAINT No</th>
            <th>ISSUE DATE</th>             
               <th>AGING (Days)</th>
                 <th>REGISTERED BY</th>
                  <th>SOURCE</th>
                 <th>FUNCTION</th>               
                     <th>VEHICLE No</th>
                      <th>VIEW</th>  
            </tr>
        </thead>
       
         <tbody>
               
             </tbody>
            </table>
            </div>
               
    </div>
  </div>
</div>
</div>
  <!-- Open Complain Popup -->
  <div class="modal fade" id="OpenComplaintPopup" role="dialog">
    <div class="modal-dialog modal-lg">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
        <div class="modal-body">
          
          
<div class="panel-group">
    <div class="panel panel-primary">
        <div class="panel-heading" style="text-align: center;"><b>OPEN COMPLAINT RESOLUTION FORM<b></div>
        <div id="" class="panel">
		
            <div class="panel-body">
            <h4 style="color: #E80B0B;"><b>CUSTOMER DETAILS :<b></h4>      
                <div class="row">
                <div class="col-md-6">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
				
                    <tr>
                      <td><strong> VEHICLE NUMBER*:</strong></td>
                      <td id="veh_num_pop" class="text-primary"></td>
                    </tr>
                   
                    <tr>
                      <td><strong>CUSTOMER NAME:</strong></td>
                      <td id="cust_name_pop" class="text-primary"></td>
                    </tr>
                 
                     <tr>
                       <td><strong>MOBILE No: </strong></td>
                      <td id="cust_phone_pop" class="text-primary"></td>
                    </tr>
					 
					  <tr>
                      <td><strong>EMAIL: </strong></td>
                      <td id="cust_email" class="text-primary"></td>
                    </tr> 
				
					  <tr>
                      <td><strong>ADDRESS: </strong></td>
                      <td id="addressName" class="text-primary"></td>
                    </tr>
					
						  <tr>
                      <td><strong>CHASSIS No: </strong></td>
                      <td id="chassisno" class="text-primary"></td>
                    </tr>
                    </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                 <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
				   
                  <tr>
                      <td><strong>MODEL:</strong></td>
                      <td id="model" class="text-primary"></td>
                    </tr>
					   <tr>
                      <td><strong>VARIANT: </strong></td>
                      <td id="variant" class="text-primary"></td>
                    </tr>
					 <tr>
                      <td><strong>SALE DATE: </strong></td>
                      <td id="saledate" class="text-primary"></td>
                    </tr>
						<tr>
                      <td><strong>LAST SERVICE: </strong></td>
                      <td id="lastserv" class="text-primary"></td>
                    </tr>
						  <tr>
                      <td><strong>SERVICE ADVISOR : </strong></td>
                      <td id="serviceadvisor" class="text-primary"></td>
                    </tr>
						  <tr>
                      <td><strong>WORKSHOP: </strong></td>
                      <td id="workshop" class="text-primary"></td>
                    </tr>
					
					
                    
                  </tbody>
                </table>
                </div>
                           
              </div>
                <h4 style="color: #E80B0B;"><b>COMPLAINT INFORMATION:<b></h4>      
                <div class="row">
                <div class="col-md-6">
                  <table class="table table-condensed table-responsive table-user-information">
                 <tbody>
 <tr>
                      <td><strong> COMPLAINT NUMBER</strong></td>
                      <td id="compnum" class="text-primary"> </td>
                    </tr>
                     <tr>
                      <td><strong> COMPLAINT STATUS</strong></td>
                      <td id="compstatus" class="text-primary"> </td>
                    </tr>
                    <tr>
                      <td><strong> FUNCTION:</strong></td>
                      <td id="function" class="text-primary"> </td>
                    </tr>
                   
                    <tr>
                      <td><strong>CATEGORY:</strong></td>
                      <td id="complaintType" class="text-primary"></td>
                    </tr>
            <tr>
                      <td rowspan="2"><strong>DESCRIPTION:</strong></td>
                      <td  id="description" class="text-primary"></td>
                    </tr>
                 
                    </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                 <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
   
                  <tr>
                      <td><strong>SOURCE:</strong></td>
                      <td id="source"class="text-primary">  </td>
                    </tr>
                    <tr>
                      <td><strong>SUB CATEGORY 1: </strong></td>
                      <td id="subcomplaintType"class="text-primary"></td>
                    </tr>


                    
                  </tbody>

                </table>
                </div>
                           
              </div>
						<div class="row">
<div class="col-lg-12">
	<div class="panel panel-primary">
  <div class="panel-heading" align="center"  onclick="trys();" data-toggle="collapse" data-parent="#accordion" data-target="#collapseOne4" style="cursor:pointer;">
		<h4 class="panel-title accordion-toggle"></h4>
		<img src=""""),_display_(/*285.14*/routes/*285.20*/.Assets.at("images/INTERACTION HISTORY.png")),format.raw/*285.64*/("""" style="width:17px;"/>&nbsp;<b>COMPLAINTS  HISTORY</b>
		 </div>
		 <div id="collapseOne4"  class="panel-collapse collapse"
		<div class="panel-body">
		<div class="row">
                    <table class="table table-border table-responsive" id="ComplaintHistory" style="display:none">
		
		
				</table>
		
                        </div>
		</div>
		</div>
		</div>
		</div>
		</br>
						
						
                        <div class="row">
                            <div class="col-lg-12" style="">

<!--  <input type="button" class="btn btn-info pull-right Resolve_It" id="Resolve_It" value="Resolve It">
<input type="button" class="btn btn-info" id="Reassign_It" value="Assign It">-->
 
                             
                </div>
                </div>
              
           <div class="row" style="display:none" id="Resolve_compID">
              
                                <h4 class="modal-title" style="text-align:center; color:red;"><b> COMPLAINT RESOLUTION</b></h4>  
              	 <div class="col-lg-3">
               <div class="form-group">
                <label for="reasonfor">REASON FOR </label>
                <input type="text" id="reasonfor"class="form-control" name="reasonfor"  autocomplete="off" />
              </div>
            </div>
           <div class="col-lg-3">
               <div class="form-group">
                <label for="complaintStatus">COMPLAINT STATUS</label>
                
                <select id="complaintStatus" class="form-control" name="complaintStatus">
                    
                    <option value="Closed">Closed</option>
                     <option value="Open">Open</option>
                    
                </select>
              </div>
            </div>
             <div class="col-lg-3">
               <div class="form-group">
                <label for="customerStatus">CUSTOMER STATUS</label>
                
                 <select id="customerStatus" class="form-control" name="customerStatus">
                    
                    <option value="Satisfied">Satisfied</option>
                     <option value="Dissatisfied">Dissatisfied</option>
                    
                </select>
              </div>
            </div>
            
             <div class="col-lg-3">
               <div class="form-group">
                <label for="actionTaken">ACTION TAKEN </label>
                <input type="text" id="actionTaken"class="form-control" name="actionTaken"   autocomplete="off" />
              </div>
            </div>
            
             <div class="col-lg-3">
               <div class="form-group">
                <label for="resolutionBy">RESOLUTION By: </label>
                <input type="text" id="resolutionBy"class="form-control" name="resolutionBy" value=""""),_display_(/*357.102*/userName),format.raw/*357.110*/("""" autocomplete="off" />
              </div>
            </div>
                  <div class="text-right">
  <button type="button" id="resolve_savebymanager" class="btn btn-success resolve_savebymanager" data-dismiss="modal">Update</button>

          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
           
              </div>
                
           		<div class="row" style="display:none;" id="reassign_compID">
                                <h4 class="modal-title" style="text-align:center; color:red;"><b>ASSIGNMENT COMPLAINT</b></h4>  

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label for="customerName">CITY </label>
                            <select id="city" class="form-control" onclick="ajaxCallToLoadWorkShopByCity();">
                                <option value="0" >--Select--</option>
                                """),_display_(/*376.34*/for(location_list<-locationList) yield /*376.66*/{_display_(Seq[Any](format.raw/*376.67*/("""
                                
                              
                                """),format.raw/*379.33*/("""<option value=""""),_display_(/*379.49*/location_list/*379.62*/.getName()),format.raw/*379.72*/("""">"""),_display_(/*379.75*/location_list/*379.88*/.getName()),format.raw/*379.98*/("""</option>
                              
                             """)))}),format.raw/*381.31*/("""
                              """),format.raw/*382.31*/("""</select>
                        </div>
                    </div>
                <div class="col-lg-3">
                        <div class="form-group">
                            <label for="workshop">LOCATION </label>
                            <select id="workshopSelected" class="form-control">
                                <option value="select" >Select</option>
                                """),_display_(/*390.34*/for(workshoplist<-workshopList) yield /*390.65*/{_display_(Seq[Any](format.raw/*390.66*/("""
                                """),format.raw/*391.33*/("""<option value=""""),_display_(/*391.49*/workshoplist/*391.61*/.getWorkshopName()),format.raw/*391.79*/("""">"""),_display_(/*391.82*/workshoplist/*391.94*/.getWorkshopName()),format.raw/*391.112*/("""</option>
                             """)))}),format.raw/*392.31*/("""
                              """),format.raw/*393.31*/("""</select>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label for="customerName">FUNCTION </label>
                            <select id="FUNCTION" class="form-control">
                                <option value="0">--Select--</option>
                                <option value="SALES">SALES</option>   
                                <option value="AFTERSALES">AFTERSALES</option>
                                <option value="INSURANCE">INSURANCE</option>
                                <option value="FINANCE">FINANCE</option>
                                <option value="PARTS">PARTS</option>
                                <option value="PRODUCT">PRODUCT</option>
                                <option value="USED CAR SALES">USED CAR SALES</option>                            
                              </select>
                        </div>
                    </div>
					

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label for="customerName">OWNERSHIP </label>
                            
                            <select id="OWNERSHIP" class="form-control">
                                
                                <option value="0">--Select--</option>
                                """),_display_(/*420.34*/for(post1 <- cresList) yield /*420.56*/{_display_(Seq[Any](format.raw/*420.57*/("""
                                """),format.raw/*421.33*/("""<option value=""""),_display_(/*421.49*/post1),format.raw/*421.54*/("""">"""),_display_(/*421.57*/post1),format.raw/*421.62*/("""</option>
                              
                              """)))}),format.raw/*423.32*/("""
                              """),format.raw/*424.31*/("""</select>
                             
                              
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label for="customerName">PRIORITY </label>
                          
                            <select id="PRIORITY" class="form-control">
                                <option value="0">--Select--</option>
                                <option value="2">High</option>
                                <option value="3" selected="selected">Medium</option>
                                <option value="4">Low</option>
                              </select>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label for="customerName">ESCALATION 1 </label>
                            <input type="text" class="form-control" name="ESCALATION1"  id="ESCALATION1" autocomplete="off" />
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label for="customerName">ESCALATION 2 </label>
                            <input type="text" class="form-control" name="ESCALATION2"  id="ESCALATION2" autocomplete="off" />
                        </div>
                    </div>
                    
                        <div class="text-right">
                    <button class="btn btn-success AddComplaintResolution" type="button" >Submit</button>
                    <button type="button" class="btn btn-danger"  data-dismiss="modal">Close</button>
                </div>
                </div>
		
            </div>
        </div>
    </div>
    


       
    </div>
        </div>
        
      </div>
      
    </div>
  </div>
  
  """)))}),format.raw/*475.4*/("""

 """),format.raw/*477.2*/("""<script>


function  getcomplaintnum(complaintNum,veh_num)"""),format.raw/*480.48*/("""{"""),format.raw/*480.49*/("""
	"""),format.raw/*481.2*/("""//alert();
		
		 $.blockUI();
        var urlDisposition = "/CRE/searchcomplaintNum/" + complaintNum + "/" + veh_num +"";
		
        $.ajax("""),format.raw/*486.16*/("""{"""),format.raw/*486.17*/("""
               """),format.raw/*487.16*/("""url: urlDisposition

    """),format.raw/*489.5*/("""}"""),format.raw/*489.6*/(""").done(function (data) """),format.raw/*489.29*/("""{"""),format.raw/*489.30*/("""
        """),format.raw/*490.9*/("""if (data != null) """),format.raw/*490.27*/("""{"""),format.raw/*490.28*/("""
        """),format.raw/*491.9*/("""console.log(data);
      console.log(complaintNum);
           
      $.unblockUI();   
        $("#veh_num_pop").html(data.vehicleRegNo);           
           $("#cust_phone_pop").html(data.customerMobileNo);      
           $("#cust_name_pop").html(data.customerName);
           $("#cust_email").html(data.customerEmail);      
            $("#serviceadvisor").html(data.serviceadvisor);
            $("#addressName").html(data.preferredAdress);
           $("#chassisno").html(data.chassisNo);
          $("#saledate").html(data.saleDate);
           $("#variant").html(data.variant);
           $("#model").html(data.model);
             $("#function").html(data.functionName);
           $("#compnum").html(data.complaintNumber);
           $("#compstatus").html(data.complaintStatus);
         $("#serviceadvisor").html(data.serviceadvisor);
          $("#source").html(data.sourceName);
        $("#description").html(data.description);
         $("#subcomplaintType").html(data.subcomplaintType);
          $("#complaintType").html(data.complaintType);
           $("#function").html(data.functionName);
            $("#assignOwnership").html(data.assignedUser);
             $("#prorityName").html(data.priority);
              $("#cityName").html(data.location);
              $("#lastserv").html(data.lastServiceDate);
              $("#workshop").html(data.workshop);
             
             // alert(data.chassisNo);

 """),format.raw/*522.2*/("""}"""),format.raw/*522.3*/("""
          
"""),format.raw/*524.1*/("""}"""),format.raw/*524.2*/(""");
          $('#OpenComplaintPopup').modal("""),format.raw/*525.42*/("""{"""),format.raw/*525.43*/("""
        """),format.raw/*526.9*/("""show: 'true'
    """),format.raw/*527.5*/("""}"""),format.raw/*527.6*/("""); 
"""),format.raw/*528.1*/("""}"""),format.raw/*528.2*/("""
 """),format.raw/*529.2*/("""function trys() """),format.raw/*529.18*/("""{"""),format.raw/*529.19*/("""
	 """),format.raw/*530.3*/("""$.blockUI();
$("#ComplaintHistory").toggle();
     var complaintNumber = $('#compnum').text();
     var vehregnumber = $('#veh_num_pop').text();
            var urlDisposition="/CREManager/getComplaintHistoryAll/"+complaintNumber+"/"+vehregnumber+"";
	$.ajax("""),format.raw/*535.9*/("""{"""),format.raw/*535.10*/("""
		"""),format.raw/*536.3*/("""url:urlDisposition 
	"""),format.raw/*537.2*/("""}"""),format.raw/*537.3*/(""").done(function(data)"""),format.raw/*537.24*/("""{"""),format.raw/*537.25*/("""
            """),format.raw/*538.13*/("""//console.log("test");
            //console.log(data);
	$.unblockUI();
           var tableHeaderRowCount = 1;
            var table = document.getElementById('ComplaintHistory');
                          	$("#ComplaintHistory").find("tr").remove();
                        $('#ComplaintHistory tbody tr').empty();

           th = $('<tr>');
                th.append('<th>COMPLAINT No</th>');
                th.append('<th>COMPLAINT STATUS</th>');
                th.append('<th>ISSUE DATE</th>');                
                th.append('<th>AGING (Days)</th>');
                th.append('<th>REGISTERED BY</th>');
                th.append('<th>SOURCE</th>');
                th.append('<th>FUNCTION</th>');
                th.append('<th>VEHICLE No</th>');
               // th.append('<th>RESOLVED BY</th>');
                $('#ComplaintHistory').append(th);
  
            for (i = 0; i < data.length; i++) """),format.raw/*558.47*/("""{"""),format.raw/*558.48*/("""


           """),format.raw/*561.12*/("""var     complaintNum =      data[i].complaintNumber;
               var     complaintStatus =      data[i].complaintStatus;
              var     issueDate =       data[i].issueDate;
             var     ageOfComplaint =        data[i].ageOfComplaint;
             var     wyzUser =       data[i].wyzUser;
               var     sourceName =      data[i].sourceName;
             var     functionName =        data[i].functionName;
           var vehregno = data[i].vehicleRegNo;
			var resolBy=data[i].resolutionBy;
            
                tr = $('<tr/>');
                    tr.append('<td id="complaintNum">'+complaintNum+'</td>');
                      tr.append('<td id="compstatus">'+complaintStatus+'</td>');
                 tr.append('<td id="issueDateS">'+issueDate+'</td>');
                  tr.append('<td id="ageOfComplaint">'+ageOfComplaint+'</td>');
                   tr.append('<td id="wyzUser">'+wyzUser+'</td>');
                   tr.append('<td id="sourceName">'+sourceName+'</td>'); 
                   tr.append('<td id="functionName">'+functionName+'</td>');
                   tr.append('<td id="vehregno">'+vehregno+'</td>');
                  // tr.append('<td id="resolBy">'+resolBy+'</td>');
                
                $('#ComplaintHistory').append(tr); 
            """),format.raw/*583.13*/("""}"""),format.raw/*583.14*/("""
            
    """),format.raw/*585.5*/("""}"""),format.raw/*585.6*/(""");
            
    """),format.raw/*587.5*/("""}"""),format.raw/*587.6*/("""

"""),format.raw/*589.1*/("""function viewComplaintData() """),format.raw/*589.30*/("""{"""),format.raw/*589.31*/("""
	"""),format.raw/*590.2*/("""// $.blockUI();
     var filterData = document.getElementById('complainHistoryID').value;

    var varLoc=document.getElementById('selLocId').value;
    var varfunc=document.getElementById('selFuncId').value;
    var varraisedDate=document.getElementById('raisedDateId').value;
    var varendDate = document.getElementById('endDateId').value;

    var excelFileName=varLoc+"_"+filterData;

    if(varraisedDate=="")"""),format.raw/*600.26*/("""{"""),format.raw/*600.27*/("""
    	"""),format.raw/*601.6*/("""varraisedDate="0";
        """),format.raw/*602.9*/("""}"""),format.raw/*602.10*/("""
    """),format.raw/*603.5*/("""if(varendDate=="")"""),format.raw/*603.23*/("""{"""),format.raw/*603.24*/("""
    	"""),format.raw/*604.6*/("""varendDate="0";
        """),format.raw/*605.9*/("""}"""),format.raw/*605.10*/("""
    
   """),format.raw/*607.4*/("""// alert("varfunc : "+varfunc+" varraisedDate : "+varraisedDate+" varLoc : "+varLoc );
    var complainHistoryID = $("#complainHistoryID").text();
    var urlDisposition="/CREManager/getComplaintsDataByFilter/"+filterData+"/"+varLoc+"/"+varfunc+"/"+varraisedDate+"/"+varendDate;

    if ($.fn.DataTable.isDataTable( '#ComplaintID' ) ) """),format.raw/*611.56*/("""{"""),format.raw/*611.57*/("""
    	"""),format.raw/*612.6*/("""var table = $('#ComplaintID').DataTable();
    	table.clear();table.destroy();
    	"""),format.raw/*614.6*/("""}"""),format.raw/*614.7*/("""

    	"""),format.raw/*616.6*/("""$("#ComplaintID thead").find("tr").remove();

    	th=$('<tr />');
    	th.append('<th>COMPLAINT NO</th>');
    	th.append('<th>ISSUE DATE</th>');
    	th.append('<th>AGING (Days)</th>');
    	th.append('<th>REGISTERED BY</th>');
    	th.append('<th>SOURCE</th>');
    	th.append('<th>FUNCTION</th>');
    	th.append('<th>VEHICLE NO</th>');
    	th.append('<th>VIEW</th>');
    	
    	$('#ComplaintID thead').append(th);

    	$('#ComplaintID').dataTable( """),format.raw/*630.35*/("""{"""),format.raw/*630.36*/("""
    		
    		  """),format.raw/*632.9*/(""""ajax": """),format.raw/*632.17*/("""{"""),format.raw/*632.18*/("""
    		    """),format.raw/*633.11*/(""""url": urlDisposition,
    		    "dataSrc": function ( json ) """),format.raw/*634.40*/("""{"""),format.raw/*634.41*/("""
    		      """),format.raw/*635.13*/("""for ( var i=0,ien=json.length ; i<ien ; i++ ) """),format.raw/*635.59*/("""{"""),format.raw/*635.60*/("""
    		    	 

    		
    		"""),format.raw/*639.7*/("""json[i][0] =  json[i].complaintNumber;
    		json[i][1] =  json[i].issueDate;
    		json[i][2] =  json[i].ageOfComplaint;
    		json[i][3] =  json[i].wyzUser;
    		json[i][4] =  json[i].sourceName;
    		json[i][5] =  json[i].functionName;
    		json[i][6] =  json[i].vehicleRegNo;
    		json[i][7] = '<span style="color:blue" onclick=\"getcomplaintnum(\''+json[i].complaintNumber+'\',\''+json[i].vehicleRegNo+'\') \" class=\"getModel getModal viewComplaints viewComplaintsresolve\"> view</span>';
    		
    		

    		      """),format.raw/*650.13*/("""}"""),format.raw/*650.14*/("""
    		     
    		      """),format.raw/*652.13*/("""return json;
    		    """),format.raw/*653.11*/("""}"""),format.raw/*653.12*/("""
    		  """),format.raw/*654.9*/("""}"""),format.raw/*654.10*/(""",
    		  			"searching":false,
    		        	"scrollY": "350px",
    		        	"scrollX":"auto",
    		            dom: '<"html5buttons"B>lTfgitp',
    		             buttons: [
    		                """),format.raw/*660.23*/("""{"""),format.raw/*660.24*/(""" """),format.raw/*660.25*/("""extend: 'copy' """),format.raw/*660.40*/("""}"""),format.raw/*660.41*/(""",
    		                """),format.raw/*661.23*/("""{"""),format.raw/*661.24*/(""" """),format.raw/*661.25*/("""extend: 'csv' ,title:excelFileName"""),format.raw/*661.59*/("""}"""),format.raw/*661.60*/(""",
    		                """),format.raw/*662.23*/("""{"""),format.raw/*662.24*/(""" """),format.raw/*662.25*/("""extend: 'excel', title: excelFileName """),format.raw/*662.63*/("""}"""),format.raw/*662.64*/(""",
    		               ],
    		               "fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*664.78*/("""{"""),format.raw/*664.79*/("""
    		                   """),format.raw/*665.26*/("""$('td', nRow).attr('nowrap','nowrap');
    		                   return nRow;
    		                   """),format.raw/*667.26*/("""}"""),format.raw/*667.27*/("""              
    		"""),format.raw/*668.7*/("""}"""),format.raw/*668.8*/(""" """),format.raw/*668.9*/(""");



	    $('#Reassign_It').click(function()"""),format.raw/*672.40*/("""{"""),format.raw/*672.41*/("""
	            """),format.raw/*673.14*/("""$("#reassign_compID").show();
	            $("#Resolve_compID").hide();
	    """),format.raw/*675.6*/("""}"""),format.raw/*675.7*/(""");
	    $('#Resolve_It').click(function()"""),format.raw/*676.39*/("""{"""),format.raw/*676.40*/(""" 
	          """),format.raw/*677.12*/("""$("#Resolve_compID").show();
	            $("#reassign_compID").hide();
	    """),format.raw/*679.6*/("""}"""),format.raw/*679.7*/(""");
		
    
	
    """),format.raw/*683.5*/("""}"""),format.raw/*683.6*/("""
	
"""),format.raw/*685.1*/("""</script>
""")))}),format.raw/*686.2*/("""


"""))
      }
    }
  }

  def render(oem:String,dealercode:String,dealerName:String,userName:String,cresList:List[String],locationList:List[Location],workshopList:List[Workshop],distinctFunctions:List[String]): play.twirl.api.HtmlFormat.Appendable = apply(oem,dealercode,dealerName,userName,cresList,locationList,workshopList,distinctFunctions)

  def f:((String,String,String,String,List[String],List[Location],List[Workshop],List[String]) => play.twirl.api.HtmlFormat.Appendable) = (oem,dealercode,dealerName,userName,cresList,locationList,workshopList,distinctFunctions) => apply(oem,dealercode,dealerName,userName,cresList,locationList,workshopList,distinctFunctions)

  def ref: this.type = this

}


}

/**/
object viewAllComplaintsReadOnly extends viewAllComplaintsReadOnly_Scope0.viewAllComplaintsReadOnly
              /*
                  -- GENERATED --
                  DATE: Thu Dec 28 12:24:31 IST 2017
                  SOURCE: D:/CRMNEW/28th Dec/crm/app/views/viewAllComplaintsReadOnly.scala.html
                  HASH: 516dd8a42d313535024045a5581a659e08ca68d7
                  MATRIX: 862->3|1131->176|1161->181|1232->244|1271->246|1301->250|1362->284|1390->285|1423->292|1531->373|1559->374|1588->376|1665->425|1694->426|1727->432|1772->450|1800->451|1829->453|1887->483|1916->484|1946->487|2041->555|2069->556|2098->558|2136->568|2165->569|2195->572|2286->636|2314->637|2346->642|2403->671|2432->672|2465->678|2521->707|2549->708|2578->710|2634->738|2663->739|2696->745|2738->760|2766->761|2795->763|2834->776|2848->782|2923->848|2963->850|2992->852|3350->1184|3390->1208|3429->1209|3470->1223|3513->1239|3525->1242|3556->1252|3586->1255|3598->1258|3629->1268|3676->1285|3709->1291|3944->1500|3990->1530|4029->1531|4070->1545|4113->1561|4138->1565|4168->1568|4193->1572|4240->1589|4274->1596|12373->9667|12389->9673|12455->9717|15373->12606|15404->12614|16427->13609|16476->13641|16516->13642|16645->13742|16689->13758|16712->13771|16744->13781|16775->13784|16798->13797|16830->13807|16935->13880|16996->13912|17441->14329|17489->14360|17529->14361|17592->14395|17636->14411|17658->14423|17698->14441|17729->14444|17751->14456|17792->14474|17865->14515|17926->14547|19421->16014|19460->16036|19500->16037|19563->16071|19607->16087|19634->16092|19665->16095|19692->16100|19798->16174|19859->16206|21888->18204|21921->18209|22011->18270|22041->18271|22072->18274|22246->18419|22276->18420|22322->18437|22377->18464|22406->18465|22458->18488|22488->18489|22526->18499|22573->18517|22603->18518|22641->18528|24138->19997|24167->19998|24209->20012|24238->20013|24312->20058|24342->20059|24380->20069|24426->20087|24455->20088|24488->20093|24517->20094|24548->20097|24593->20113|24623->20114|24655->20118|24947->20382|24977->20383|25009->20387|25059->20409|25088->20410|25138->20431|25168->20432|25211->20446|26181->21387|26211->21388|26257->21405|27617->22736|27647->22737|27695->22757|27724->22758|27774->22780|27803->22781|27835->22785|27893->22814|27923->22815|27954->22818|28408->23243|28438->23244|28473->23251|28529->23279|28559->23280|28593->23286|28640->23304|28670->23305|28705->23312|28758->23337|28788->23338|28827->23349|29195->23688|29225->23689|29260->23696|29374->23782|29403->23783|29440->23792|29939->24262|29969->24263|30015->24281|30052->24289|30082->24290|30123->24302|30215->24365|30245->24366|30288->24380|30363->24426|30393->24427|30453->24459|31019->24996|31049->24997|31105->25024|31158->25048|31188->25049|31226->25059|31256->25060|31494->25269|31524->25270|31554->25271|31598->25286|31628->25287|31682->25312|31712->25313|31742->25314|31805->25348|31835->25349|31889->25374|31919->25375|31949->25376|32016->25414|32046->25415|32180->25520|32210->25521|32266->25548|32399->25652|32429->25653|32479->25675|32508->25676|32537->25677|32615->25726|32645->25727|32689->25742|32796->25821|32825->25822|32896->25864|32926->25865|32969->25879|33076->25958|33105->25959|33154->25980|33183->25981|33216->25986|33259->25998
                  LINES: 27->2|32->2|34->4|34->4|34->4|36->6|37->7|37->7|39->9|43->13|43->13|44->14|44->14|44->14|46->16|47->17|47->17|48->18|48->18|48->18|49->19|52->22|52->22|53->23|53->23|53->23|54->24|56->26|56->26|58->28|58->28|58->28|59->29|60->30|60->30|61->31|61->31|61->31|62->32|63->33|63->33|64->34|66->36|66->36|66->36|66->36|67->37|76->46|76->46|76->46|77->47|77->47|77->47|77->47|77->47|77->47|77->47|78->48|79->49|85->55|85->55|85->55|86->56|86->56|86->56|86->56|86->56|87->57|88->58|315->285|315->285|315->285|387->357|387->357|406->376|406->376|406->376|409->379|409->379|409->379|409->379|409->379|409->379|409->379|411->381|412->382|420->390|420->390|420->390|421->391|421->391|421->391|421->391|421->391|421->391|421->391|422->392|423->393|450->420|450->420|450->420|451->421|451->421|451->421|451->421|451->421|453->423|454->424|505->475|507->477|510->480|510->480|511->481|516->486|516->486|517->487|519->489|519->489|519->489|519->489|520->490|520->490|520->490|521->491|552->522|552->522|554->524|554->524|555->525|555->525|556->526|557->527|557->527|558->528|558->528|559->529|559->529|559->529|560->530|565->535|565->535|566->536|567->537|567->537|567->537|567->537|568->538|588->558|588->558|591->561|613->583|613->583|615->585|615->585|617->587|617->587|619->589|619->589|619->589|620->590|630->600|630->600|631->601|632->602|632->602|633->603|633->603|633->603|634->604|635->605|635->605|637->607|641->611|641->611|642->612|644->614|644->614|646->616|660->630|660->630|662->632|662->632|662->632|663->633|664->634|664->634|665->635|665->635|665->635|669->639|680->650|680->650|682->652|683->653|683->653|684->654|684->654|690->660|690->660|690->660|690->660|690->660|691->661|691->661|691->661|691->661|691->661|692->662|692->662|692->662|692->662|692->662|694->664|694->664|695->665|697->667|697->667|698->668|698->668|698->668|702->672|702->672|703->673|705->675|705->675|706->676|706->676|707->677|709->679|709->679|713->683|713->683|715->685|716->686
                  -- GENERATED --
              */
          