
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object complaintResolutionSA_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class complaintResolutionSA extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template5[String,String,String,List[Complaint],List[Complaint],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(dealercode:String,dealerName:String,userName:String,complaintData:List[Complaint],compalintDataclosed:List[Complaint]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.121*/("""

"""),_display_(/*4.2*/mainPageServiceAdvisor("AutoSherpaCRM",userName,dealerName,dealercode)/*4.72*/ {_display_(Seq[Any](format.raw/*4.74*/("""
"""),format.raw/*5.1*/("""<div class="row" >
  <div class="col-lg-12">
    <div class="panel panel-primary">
      <div class="panel-heading">   
        COMPLAINT RESOLUTION MODULE</div>
		<div class="panel-body">
		<h4 style="text-align: center;color:red;"><b>OPEN COMPLAINTS</b></h4>
<table id="baseTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>COMPLAINT No.</th>
                <th>STATUS</th>
                <th>ISSUE DATE</th>
                <th>AGING (Days)</th>
                <th>REGISTERED BY</th>
                <th>SOURCE</th>
                <th>FUNCTION</th>
                <th>CATEGORY</th>
                <th>VEHICLE No.</th>
<!--                <th>MOBILE No.</th>
                <th>ASSIGNED TO</th>-->
                <th >View</th>
            </tr>
        </thead>
       
        <tbody>
               """),_display_(/*31.17*/for(post <- complaintData) yield /*31.43*/ {_display_(Seq[Any](format.raw/*31.45*/("""
"""),format.raw/*32.1*/("""<!--            <tr class="rowclass"""),_display_(/*32.37*/post/*32.41*/.getComplaintNumber()),format.raw/*32.62*/("""" value=""""),_display_(/*32.72*/post/*32.76*/.getComplaintNumber()),format.raw/*32.97*/("""" id=""""),_display_(/*32.104*/post/*32.108*/.getComplaintNumber()),format.raw/*32.129*/("""">
                <td class="complaintNum" id="tdclass"""),_display_(/*33.54*/post/*33.58*/.getComplaintNumber()),format.raw/*33.79*/("""">"""),_display_(/*33.82*/post/*33.86*/.getComplaintNumber()),format.raw/*33.107*/("""</td>
                <td id="tdclass"""),_display_(/*34.33*/post/*34.37*/.getComplaintNumber()),format.raw/*34.58*/("""">"""),_display_(/*34.61*/post/*34.65*/.getComplaintStatus()),format.raw/*34.86*/("""</td>
                <td id="tdclass"""),_display_(/*35.33*/post/*35.37*/.getComplaintNumber()),format.raw/*35.58*/("""">"""),_display_(/*35.61*/post/*35.65*/.getIssueDate()),format.raw/*35.80*/("""</td>
                <td id="tdclass"""),_display_(/*36.33*/post/*36.37*/.getComplaintNumber()),format.raw/*36.58*/("""">"""),_display_(/*36.61*/post/*36.65*/.getAgeOfComplaint()),format.raw/*36.85*/("""</td>
                <td id="tdclass"""),_display_(/*37.33*/post/*37.37*/.getComplaintNumber()),format.raw/*37.58*/("""">"""),_display_(/*37.61*/post/*37.65*/.getWyzUser().getUserName()),format.raw/*37.92*/("""</td>
                <td id="tdclass"""),_display_(/*38.33*/post/*38.37*/.getComplaintNumber()),format.raw/*38.58*/("""">"""),_display_(/*38.61*/post/*38.65*/.getFunctionName()),format.raw/*38.83*/("""</td>
                <td id="tdclass"""),_display_(/*39.33*/post/*39.37*/.getComplaintNumber()),format.raw/*39.58*/("""">"""),_display_(/*39.61*/post/*39.65*/.getSourceName()),format.raw/*39.81*/("""</td>
                <td id="tdclass"""),_display_(/*40.33*/post/*40.37*/.getComplaintNumber()),format.raw/*40.58*/("""">"""),_display_(/*40.61*/post/*40.65*/.getSubcomplaintType()),format.raw/*40.87*/("""</td>
                <td class="veh_num" id="tdclass"""),_display_(/*41.49*/post/*41.53*/.getComplaintNumber()),format.raw/*41.74*/("""">"""),_display_(/*41.77*/post/*41.81*/.getVehicleRegNo()),format.raw/*41.99*/("""</td>
                <td id="tdclass"""),_display_(/*42.33*/post/*42.37*/.getComplaintNumber()),format.raw/*42.58*/("""">"""),_display_(/*42.61*/post/*42.65*/.getCustomerPhone()),format.raw/*42.84*/("""</td>
                <td>dfgdfg</td>
                <td><a href="" class="comp"""),_display_(/*44.44*/post/*44.48*/.getComplaintNumber()),format.raw/*44.69*/("""" value=""""),_display_(/*44.79*/post/*44.83*/.getComplaintNumber()),format.raw/*44.104*/("""" data-id=""""),_display_(/*44.116*/post/*44.120*/.getComplaintNumber()),format.raw/*44.141*/("""" data-toggle="modal"  data-target="#OpenComplaintPopup" onclick="ajaxcomplaintNumber();" ><i class="fa fa-arrow-circle-right"></i>&nbsp;View</a></td>
                
            </tr>
            -->
            <tr id="rowclass" class="rowclass"""),_display_(/*48.47*/post/*48.51*/.getComplaintNumber()),format.raw/*48.72*/("""">
                            <td id="compNum" >"""),_display_(/*49.48*/post/*49.52*/.getComplaintNumber()),format.raw/*49.73*/("""</td>
                            <td id="status">"""),_display_(/*50.46*/post/*50.50*/.getComplaintStatus()),format.raw/*50.71*/("""</td>
                            <td id="issueDate">"""),_display_(/*51.49*/post/*51.53*/.getIssueDate()),format.raw/*51.68*/("""</td>
                            <td id="x">"""),_display_(/*52.41*/post/*52.45*/.getAgeOfComplaint()),format.raw/*52.65*/("""</td>
                            <td id="userName">"""),_display_(/*53.48*/post/*53.52*/.getWyzUser().getUserName()),format.raw/*53.79*/("""</td>
                            <td id="y">"""),_display_(/*54.41*/post/*54.45*/.getFunctionName()),format.raw/*54.63*/("""</td>
                            <td id="z">"""),_display_(/*55.41*/post/*55.45*/.getSourceName()),format.raw/*55.61*/("""</td>
           
                                                        <td id="a">"""),_display_(/*57.69*/post/*57.73*/.getSubcomplaintType()),format.raw/*57.95*/("""</td>

                            <td id="vREgNO">"""),_display_(/*59.46*/post/*59.50*/.getVehicleRegNo()),format.raw/*59.68*/("""</td>

                            <td><a class="getModel" data-id=""""),_display_(/*61.63*/post/*61.67*/.getComplaintNumber()),format.raw/*61.88*/("""" href="" data-toggle="modal" data-target="#OpenComplaintPopup"  data-backdrop="static" data-keyboard="false"><i class="fa fa-arrow-circle-right"></i>&nbsp;View</a></td>

                        </tr>
            """)))}),format.raw/*64.14*/("""
            """),format.raw/*65.13*/("""</tbody>
            </table>
            
            <h4 style="text-align: center;color:red;"><b>CLOSED  COMPLAINTS</b></h4>
            <table id="complaint" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>COMPLAINT No.</th>
                <th>STATUS</th>
                <th>ISSUE DATE</th>
                <th>AGING (Days)</th>
                <th>REGISTERED BY</th>
                <th>SOURCE</th>
                <th>FUNCTION</th>
                <th>CATEGORY</th>
                <th>VEHICLE No.</th>
<!--                <th>MOBILE No.</th>
                <th>ASSIGNED TO</th>-->
                <th>View</th>
            </tr>
        </thead>
  
          <tbody>
               """),_display_(/*88.17*/for(post <- compalintDataclosed) yield /*88.49*/ {_display_(Seq[Any](format.raw/*88.51*/("""

            """),format.raw/*90.13*/("""<tr id="rowclass" class="rowclass """),_display_(/*90.48*/post/*90.52*/.getComplaintNumber()),format.raw/*90.73*/("""">
                            <td id="compNumclosed" >"""),_display_(/*91.54*/post/*91.58*/.getComplaintNumber()),format.raw/*91.79*/("""</td>
                            <td id="statusclosed">"""),_display_(/*92.52*/post/*92.56*/.getComplaintStatus()),format.raw/*92.77*/("""</td>
                            <td id="issueDateclosed">"""),_display_(/*93.55*/post/*93.59*/.getIssueDate()),format.raw/*93.74*/("""</td>
                            <td id="xclosed">"""),_display_(/*94.47*/post/*94.51*/.getAgeOfComplaint()),format.raw/*94.71*/("""</td>
                            <td id="userNameclosed">"""),_display_(/*95.54*/post/*95.58*/.getWyzUser().getUserName()),format.raw/*95.85*/("""</td>
                            <td id="yclosed">"""),_display_(/*96.47*/post/*96.51*/.getFunctionName()),format.raw/*96.69*/("""</td>
                            <td id="zclosed">"""),_display_(/*97.47*/post/*97.51*/.getSourceName()),format.raw/*97.67*/("""</td>
                            
                            <td id="aclosed">"""),_display_(/*99.47*/post/*99.51*/.getSubcomplaintType()),format.raw/*99.73*/("""</td>

                            <td id="vREgNOclosed">"""),_display_(/*101.52*/post/*101.56*/.getVehicleRegNo()),format.raw/*101.74*/("""</td>

                            <td><a class="getModelclosed" data-id=""""),_display_(/*103.69*/post/*103.73*/.getComplaintNumber()),format.raw/*103.94*/("""" href="" data-toggle="modal" data-target="#ClosedComplaintPopup"  data-backdrop="static" data-keyboard="false"><i class="fa fa-arrow-circle-right"></i>&nbsp;View</a></td>

                        </tr>

            """)))}),format.raw/*107.14*/("""
            """),format.raw/*108.13*/("""</tbody>
            </table>
            
   </div>
   </div>
   </div>
   </div>

  <!-- Open Complain Popup -->
  <div class="modal fade" id="OpenComplaintPopup" role="dialog">
    <div class="modal-dialog modal-lg">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
        <div class="modal-body">
          
          
<div class="panel-group">
    <div class="panel panel-primary">
        <div class="panel-heading" style="text-align: center;"><b>OPEN COMPLAINT RESOLUTION FORM<b></div>
        <div id="" class="panel">
		
            <div class="panel-body">
            <h4 style="color: #E80B0B;"><b>CUSTOMER DETAILS :<b></h4>      
                <div class="row">
                <div class="col-md-6">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
				
                    <tr>
                      <td><strong> VEHICLE NUMBER*:</strong></td>
                      <td id="veh_num_pop"class="text-primary"></td>
                    </tr>
                   
                    <tr>
                      <td><strong>CUSTOMER NAME:</strong></td>
                      <td id="cust_name_pop"class="text-primary"></td>
                    </tr>
                 
                     <tr>
                       <td><strong>MOBILE No: </strong></td>
                      <td id="cust_phone_pop" class="text-primary"></td>
                    </tr>
					 
					  <tr>
                      <td><strong>EMAIL: </strong></td>
                      <td id="cust_email"class="text-primary"></td>
                    </tr> 
				
					  <tr>
                      <td><strong>ADDRESS: </strong></td>
                      <td id="addressName"class="text-primary"></td>
                    </tr>
					
						  <tr>
                      <td><strong>CHASSIS No: </strong></td>
                      <td id="chassisno"class="text-primary"></td>
                    </tr>
                    </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                 <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
				   
                  <tr>
                      <td><strong>MODEL:</strong></td>
                      <td id="model" class="text-primary"></td>
                    </tr>
					   <tr>
                      <td><strong>VARIANT: </strong></td>
                      <td id="variant"class="text-primary"></td>
                    </tr>
					 <tr>
                      <td><strong>SALE DATE: </strong></td>
                      <td id="saledate" class="text-primary"></td>
                    </tr>
						<tr>
                      <td><strong>LAST SERVICE: </strong></td>
                      <td id="lastserv"class="text-primary"></td>
                    </tr>
						  <tr>
                      <td><strong>SERVICE ADVISOR : </strong></td>
                      <td id="serviceadvisor"class="text-primary"></td>
                    </tr>
						  <tr>
                      <td><strong>WORKSHOP: </strong></td>
                     <td id="workshopOpen" class="text-primary"></td>
                    </tr>
					
					
                    
                  </tbody>
                </table>
                </div>
                           
              </div>
			   <h4 style="color: #E80B0B;"><b>COMPLAINT INFORMATION:<b></h4>      
                <div class="row">
                <div class="col-md-6">
                  <table class="table table-condensed table-responsive table-user-information">
                 <tbody>

                    <tr>
                      <td><strong> FUNCTION:</strong></td>
                      <td id="function" class="text-primary"> </td>
                    </tr>
                   
                    <tr>
                      <td><strong>CATEGORY:</strong></td>
                      <td id="complaintType" class="text-primary"></td>
                    </tr>
            <tr>
                      <td rowspan="2"><strong>DESCRIPTION:</strong></td>
                      <td  id="description"class="text-primary"></td>
                    </tr>
                 
                    </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                 <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
   
                  <tr>
                      <td><strong>SOURCE:</strong></td>
                      <td id="source"class="text-primary">  </td>
                    </tr>
                    <tr>
                      <td><strong>SUB CATEGORY 1: </strong></td>
                      <td id="subcomplaintType"class="text-primary"></td>
                    </tr>


                    
                  </tbody>

                </table>
                </div>
                           
              </div>
             <h4 style="color: #E80B0B;"><b>ASSIGN COMPLAINT :<b></h4>      
                <div class="row">
                <div class="col-md-6">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
				
                    <tr>
                      <td><strong> CITY:</strong></td>
                      <td class="text-primary" id="cityName"> </td>
                    </tr>
                   
                    <tr>
                      <td><strong>OWNERSHIP:</strong></td>
                      <td class="text-primary" id="assignOwnership"></td>
                    </tr>
					  <tr>
                      <td><strong>ESCALATION 1:</strong></td>
                      <td class="text-primary" ></td>
                    </tr>
                 
                    </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                 <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
				   
                  <tr>
                      <td><strong>FUNCTION:</strong></td>
                      <td class="text-primary" id="function">  </td>
                    </tr>
					   <tr>
                      <td><strong>PRIORITY: </strong></td>
                      <td class="text-primary" id="prorityName"></td>
                    </tr>
					 <tr>
                      <td><strong>ESCALATION 2:</strong></td>
                      <td class="text-primary"></td>
                    </tr>
                 
				
					
                    
                  </tbody>
                </table>
                </div>
                           
              </div>
              
           <div class="row">
              
              	<h4 style="color: #E80B0B;"><b>COMPLAINT RESOLUTION<b></b></b></h4>
             
           <div class="col-lg-3">
               <div class="form-group">
                <label for="complaintStatus">COMPLAINT STATUS</label>
                
                <select id="complaintStatus" class="form-control" name="complaintStatus">
                    
                    <option value="Closed">Closed</option>
                     <option value="Open">Open</option>
                    
                </select>
              </div>
            </div>
             <div class="col-lg-3">
               <div class="form-group">
                <label for="customerStatus">CUSTOMER STATUS</label>
                
                 <select id="customerStatus" class="form-control" name="customerStatus">
                    
                    <option value="Satisfied">Satisfied</option>
                     <option value="Dissatisfied">Dissatisfied</option>
                    
                </select>
              </div>
            </div>
            
             <div class="col-lg-3">
               <div class="form-group">
                <label for="actionTaken">ACTION TAKEN </label>
                <input type="text" id="actionTaken"class="form-control" name="actionTaken"   autocomplete="off" />
              </div>
            </div>
            
             <div class="col-lg-3">
               <div class="form-group">
                <label for="resolutionBy">RESOLUTION By: </label>
                <input type="text" id="resolutionBy"class="form-control" name="resolutionBy" value=""""),_display_(/*341.102*/userName),format.raw/*341.110*/("""" autocomplete="off" />
              </div>
            </div>
           
              </div>
                <div class="row">
					 	 <div class="col-md-6">
               <div class="form-group">
                <label for="reasonfor">REASON FOR </label>
                <textarea type="text" id="reasonfor"class="form-control" name="reasonfor"  autocomplete="off" ></textarea>
              </div>
            </div>
				</div>
           
		
            </div>
        </div>
    </div>
    
  <div class="text-right">
  <button type="button" id="resolve_save" class="btn btn-success resolve_save" data-dismiss="modal">Update</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>

       
    </div>
        </div>
        
      </div>
      
    </div>
  </div>
  
  <!-- Closed complaint Popup -->
    <div class="modal fade" id="ClosedComplaintPopup" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
        <div class="modal-body">
          
          
<div class="panel-group">
  
    <div class="panel panel-primary">
        <div class="panel-heading" style="text-align: center;"><b>CLOSED COMPLAINT RESOLUTION FORM<b></div>
        <div id="" class="panel">
		
            <div class="panel-body">
            
            
            
            
            
        <h4 style="color: #E80B0B;"><b>CUSTOMER DETAILS :<b></h4>      
                <div class="row">
                <div class="col-md-6">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
				
                    <tr>
                      <td><strong> VEHICLE NUMBER*:</strong></td>
                      <td id="veh_num_popClosed"class="text-primary"></td>
                    </tr>
                   
                    <tr>
                      <td><strong>CUSTOMER NAME:</strong></td>
                      <td id="cust_name_popClosed"class="text-primary"></td>
                    </tr>
                 
                     <tr>
                       <td><strong>MOBILE No: </strong></td>
                      <td id="cust_phone_popClosed" class="text-primary"></td>
                    </tr>
					 
					  <tr>
                      <td><strong>EMAIL: </strong></td>
                      <td id="cust_emailClosed"class="text-primary"></td>
                    </tr> 
				
					  <tr>
                      <td><strong>ADDRESS: </strong></td>
                      <td id="addressNameClosed"class="text-primary"></td>
                    </tr>
					
						  <tr>
                      <td><strong>CHASSIS No: </strong></td>
                      <td id="chassisnoClosed"class="text-primary"></td>
                    </tr>
                    </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                 <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
				   
                  <tr>
                      <td><strong>MODEL:</strong></td>
                      <td id="modelClosed" class="text-primary"></td>
                    </tr>
					   <tr>
                      <td><strong>VARIANT: </strong></td>
                      <td id="variantClosed"class="text-primary"></td>
                    </tr>
					 <tr>
                      <td><strong>SALE DATE: </strong></td>
                      <td id="saledateClosed" class="text-primary"></td>
                    </tr>
						<tr>
                      <td><strong>LAST SERVICE: </strong></td>
                      <td id="lastservClosed"class="text-primary"></td>
                    </tr>
						  <tr>
                      <td><strong>SERVICE ADVISOR : </strong></td>
                      <td id="serviceadvisorClosed"class="text-primary"></td>
                    </tr>
						  <tr>
                      <td><strong>WORKSHOP: </strong></td>
                      <td id="workshopClosed" class="text-primary"></td>
                    </tr>
					
					
                    
                  </tbody>
                </table>
                </div>
                           
              </div>
            <h4 style="color: #E80B0B;"><b>COMPLAINT INFORMATION:<b></h4>      
                <div class="row">
                <div class="col-md-6">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
				
                    <tr>
                      <td><strong> FUNCTION:</strong></td>
                      <td id="functionClosed" class="text-primary"> </td>
                    </tr>
                   
                    <tr>
                      <td><strong>CATEGORY:</strong></td>
                      <td id="complaintTypeClosed" class="text-primary"></td>
                    </tr>
					  <tr>
                      <td rowspan="2"><strong>DESCRIPTION:</strong></td>
                      <td  id="descriptionClosed"class="text-primary"></td>
                    </tr>
                 
                    </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                 <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
				   
                  <tr>
                      <td><strong>SOURCE:</strong></td>
                      <td id="sourceClosed"class="text-primary">  </td>
                    </tr>
					   <tr>
                      <td><strong>SUB CATEGORY 1: </strong></td>
                      <td id="subcomplaintTypeClosed" class="text-primary"></td>
                    </tr>
				
					
                    
                  </tbody>
                </table>
                </div>
                           
              </div>
			     <h4 style="color: #E80B0B;"><b>ASSIGN COMPLAINT :<b></h4>      
                <div class="row">
                <div class="col-md-6">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
				
                    <tr>
                      <td><strong> CITY:</strong></td>
                      <td class="text-primary" id="cityNameClosed"> </td>
                    </tr>
                   
                    <tr>
                      <td><strong>OWNERSHIP:</strong></td>
                      <td class="text-primary" id = "ownershipClosed"></td>
                    </tr>
					  <tr>
                      <td><strong>ESCALATION 1:</strong></td>
                      <td class="text-primary"></td>
                    </tr>
                 
                    </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                 <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
				   
                  <tr>
                      <td><strong>FUNCTION:</strong></td>
                      <td class="text-primary" id="functionNameClosed">  </td>
                    </tr>
					   <tr>
                      <td><strong>PRIORITY: </strong></td>
                      <td class="text-primary" id="priorityClosed"></td>
                    </tr>
					 <tr>
                      <td><strong>ESCALATION 2:</strong></td>
                      <td class="text-primary"></td>
                    </tr>
                 
				
					
                    
                  </tbody>
                </table>
                </div>
                           
              </div>
              
           <div class="row">
              
              	<h4 style="color: #E80B0B;"><b>COMPLAINT RESOLUTION<b></b></b></h4>
            
           <div class="col-lg-3">
               <div class="form-group">
                <label for="complaintStatusClosed">COMPLAINT STATUS</label>
                <input type="text" id="complaintStatusClosed"class="form-control" name="complaintStatusClosed"   autocomplete="off" readonly/>
              </div>
            </div>
             <div class="col-lg-3">
               <div class="form-group">
                <label for="customerStatusClosed">CUSTOMER STATUS</label>
                <input type="text" id="customerStatusClosed"class="form-control" name="customerStatusClosed"   autocomplete="off" readonly/>
              </div>
            </div>
            
             <div class="col-lg-3">
               <div class="form-group">
                <label for="actionTakenClosed">ACTION TAKEN </label>
                <input type="text" id="actionTakenClosed"class="form-control" name="actionTakenClosed"   autocomplete="off" readonly />
              </div>
            </div>
            
             <div class="col-lg-3">
               <div class="form-group">
                <label for="resolutionByClosed">RESOLUTION By: </label>
                <input type="text" id="resolutionByClosed"class="form-control" name="resolutionByClosed" value=""""),_display_(/*592.114*/userName),format.raw/*592.122*/("""" autocomplete="off" readonly/>
              </div>
            </div>
           
              </div>
                <div class="row">
					  	 <div class="col-lg-6">
               <div class="form-group">
                <label for="reasonforClosed">REASON FOR </label>
                <input type="textarea" id="reasonforClosed"class="form-control" name="reasonforClosed"  autocomplete="off" ></textarea>
              </div>
            </div>
				</div>
           
		
            </div>
        </div>
    </div>
    
  <div class="text-right">
  <button type="button" id="resolve_save_closed" class="btn btn-success resolve_save_closed" data-dismiss="modal">Update</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>

       
    </div>
        </div>
        
      </div>
      
    </div>
  </div>
        </div>
        
      </div>
      
    </div>
  </div>


""")))}),format.raw/*632.2*/("""

"""),format.raw/*634.1*/("""<script>
	$(".getModel").on(
			"click",
			function() """),format.raw/*637.15*/("""{"""),format.raw/*637.16*/("""

				"""),format.raw/*639.5*/("""$.blockUI();
				
				var $row = $(this).closest("tr"); // Find the row
				var complaintNum = $row.find("#compNum").text(); // Find the text
				var $text2 = $row.find("#issueDate").text();
				var $text3 = $row.find("#userName").text();
				var veh_num = $row.find("#vREgNO").text();
				var status = $row.find("#status").text();
				var $text5 = $row.find("#x").text();
				var $text6 = $row.find("#y").text();
				var $text7 = $row.find("#z").text();
				var $text8 = $row.find("#a").text();
				var $text9 = $row.find("#issueDate").text();
				var $text10 = $row.find("#custPhone").text();
				$(".resolve_save").val(complaintNum);

				var urlDisposition = "/CRE/searchcomplaintNum/" + complaintNum
						+ "/" + veh_num + "";
				$.ajax("""),format.raw/*657.12*/("""{"""),format.raw/*657.13*/("""
					"""),format.raw/*658.6*/("""url : urlDisposition

				"""),format.raw/*660.5*/("""}"""),format.raw/*660.6*/(""").done(function(data) """),format.raw/*660.28*/("""{"""),format.raw/*660.29*/("""
					"""),format.raw/*661.6*/("""if (data != null) """),format.raw/*661.24*/("""{"""),format.raw/*661.25*/("""
						"""),format.raw/*662.7*/("""console.log(data);
						//  console.log(complaintNum);

						$("#veh_num_pop").html(data.vehicleRegNo);
						$("#cust_phone_pop").html(data.customerMobileNo);
						$("#cust_name_pop").html(data.customerName);
						$("#cust_email").html(data.customerEmail);
						$("#serviceadvisor").html(data.serviceadvisor);
						$("#addressName").html(data.preferredAdress);
						$("#chassisno").html(data.chassisNo);
						$("#saledate").html(data.saleDate);
						$("#variant").html(data.variant);
						$("#model").html(data.model);
						$("#function").html(data.functionName);
						$("#serviceadvisor").html(data.serviceadvisor);
						$("#source").html(data.sourceName);
						$("#description").html(data.description);
						$("#subcomplaintType").html(data.subcomplaintType);
						$("#complaintType").html(data.complaintType);
						$("#function").html(data.functionName);
						$("#assignOwnership").html(data.assignedUser);
						$("#prorityName").html(data.priority);
						$("#cityName").html(data.location);
						$("#workshopOpen").html(data.workshop);
						//alert(data.preferredAdress);
						$.unblockUI();
					"""),format.raw/*688.6*/("""}"""),format.raw/*688.7*/("""
				"""),format.raw/*689.5*/("""}"""),format.raw/*689.6*/(""");
			"""),format.raw/*690.4*/("""}"""),format.raw/*690.5*/(""");
</script>
  <script>
			$(".resolve_save")
					.on(
							'click',
							function() """),format.raw/*696.19*/("""{"""),format.raw/*696.20*/("""

								"""),format.raw/*698.9*/("""var complaintNum = $(".resolve_save").val();

								var reasonFor = $("#reasonfor").val();

								var complaintStatus = $("#complaintStatus")
										.val();
								var actionTaken = $("#actionTaken").val();
								var resolutionBy = $("#resolutionBy").val();
								var customerStatus = $("#customerStatus").val();

								//alert(complaintNum);

								if (reasonFor == '') """),format.raw/*710.30*/("""{"""),format.raw/*710.31*/("""
									"""),format.raw/*711.10*/("""Lobibox.notify('warning', """),format.raw/*711.36*/("""{"""),format.raw/*711.37*/("""
										"""),format.raw/*712.11*/("""continueDelayOnInactiveTab : true,
										msg : 'Reason for is Required'
									"""),format.raw/*714.10*/("""}"""),format.raw/*714.11*/(""");

									return false;

								"""),format.raw/*718.9*/("""}"""),format.raw/*718.10*/("""
								"""),format.raw/*719.9*/("""if (actionTaken == '') """),format.raw/*719.32*/("""{"""),format.raw/*719.33*/("""
									"""),format.raw/*720.10*/("""Lobibox.notify('warning', """),format.raw/*720.36*/("""{"""),format.raw/*720.37*/("""
										"""),format.raw/*721.11*/("""continueDelayOnInactiveTab : true,
										msg : 'Action Taken is Required'
									"""),format.raw/*723.10*/("""}"""),format.raw/*723.11*/(""");

									return false;
								"""),format.raw/*726.9*/("""}"""),format.raw/*726.10*/(""" """),format.raw/*726.11*/("""else """),format.raw/*726.16*/("""{"""),format.raw/*726.17*/("""
									"""),format.raw/*727.10*/("""var urlDisposition = "/CRE/updateComplaintsResolution/"
											+ complaintNum
											+ "/"
											+ reasonFor
											+ "/"
											+ complaintStatus
											+ "/"
											+ customerStatus
											+ "/"
											+ actionTaken + "/" + resolutionBy;
									$
											.ajax("""),format.raw/*738.18*/("""{"""),format.raw/*738.19*/("""
												"""),format.raw/*739.13*/("""url : urlDisposition
											"""),format.raw/*740.12*/("""}"""),format.raw/*740.13*/(""")
											.done(
													function(data) """),format.raw/*742.29*/("""{"""),format.raw/*742.30*/("""
														"""),format.raw/*743.15*/("""if (data != null) """),format.raw/*743.33*/("""{"""),format.raw/*743.34*/("""
															"""),format.raw/*744.16*/("""Lobibox
																	.alert(
																			'success',
																			"""),format.raw/*747.20*/("""{"""),format.raw/*747.21*/("""
																				"""),format.raw/*748.21*/("""msg : 'Complaint Has been Resolved for open Successfully....'
																			"""),format.raw/*749.20*/("""}"""),format.raw/*749.21*/(""");

														"""),format.raw/*751.15*/("""}"""),format.raw/*751.16*/("""
														"""),format.raw/*752.15*/("""window
																.setTimeout(
																		function() """),format.raw/*754.30*/("""{"""),format.raw/*754.31*/("""
																			"""),format.raw/*755.20*/("""location
																					.reload()
																		"""),format.raw/*757.19*/("""}"""),format.raw/*757.20*/(""", 4000)

													"""),format.raw/*759.14*/("""}"""),format.raw/*759.15*/(""");

								"""),format.raw/*761.9*/("""}"""),format.raw/*761.10*/("""

							"""),format.raw/*763.8*/("""}"""),format.raw/*763.9*/(""");
		</script>
	 <script>
			$(".resolve_save_closed")
					.on(
							'click',
							function() """),format.raw/*769.19*/("""{"""),format.raw/*769.20*/("""

								"""),format.raw/*771.9*/("""//alert("closed submit");
								var complaintNumClosed = $(
										".resolve_save_closed").val();
								var reasonForClosed = $("#reasonforClosed")
										.val();
								var complaintStatusClosed = $(
										"#complaintStatusClosed").val();
								var actionTakenClosed = $("#actionTakenClosed")
										.val();
								var resolutionByClosed = $(
										"#resolutionByClosed").val();
								var customerStatusClosed = $(
										"#customerStatusClosed").val();

								//alert(complaintNumClosed);

								if (reasonforClosed == '') """),format.raw/*787.36*/("""{"""),format.raw/*787.37*/("""
									"""),format.raw/*788.10*/("""Lobibox.notify('warning', """),format.raw/*788.36*/("""{"""),format.raw/*788.37*/("""
										"""),format.raw/*789.11*/("""continueDelayOnInactiveTab : true,
										msg : 'Please Enter the Reason..'
									"""),format.raw/*791.10*/("""}"""),format.raw/*791.11*/(""");

									return false;

								"""),format.raw/*795.9*/("""}"""),format.raw/*795.10*/(""" """),format.raw/*795.11*/("""else """),format.raw/*795.16*/("""{"""),format.raw/*795.17*/("""
									"""),format.raw/*796.10*/("""var urlDisposition = "/CRE/updateComplaintsResolutionClosed/"
											+ complaintNumClosed
											+ "/"
											+ reasonForClosed
											+ "/"
											+ complaintStatusClosed
											+ "/"
											+ customerStatusClosed
											+ "/"
											+ actionTakenClosed
											+ "/"
											+ resolutionByClosed;
									$
											.ajax("""),format.raw/*809.18*/("""{"""),format.raw/*809.19*/("""
												"""),format.raw/*810.13*/("""url : urlDisposition
											"""),format.raw/*811.12*/("""}"""),format.raw/*811.13*/(""")
											.done(
													function(data) """),format.raw/*813.29*/("""{"""),format.raw/*813.30*/("""
														"""),format.raw/*814.15*/("""if (data != null) """),format.raw/*814.33*/("""{"""),format.raw/*814.34*/("""
															"""),format.raw/*815.16*/("""Lobibox
																	.alert(
																			'success',
																			"""),format.raw/*818.20*/("""{"""),format.raw/*818.21*/("""
																				"""),format.raw/*819.21*/("""msg : 'Complaint Has been Resolved for closed Successfully....'
																			"""),format.raw/*820.20*/("""}"""),format.raw/*820.21*/(""");

														"""),format.raw/*822.15*/("""}"""),format.raw/*822.16*/("""
														"""),format.raw/*823.15*/("""window
																.setTimeout(
																		function() """),format.raw/*825.30*/("""{"""),format.raw/*825.31*/("""
																			"""),format.raw/*826.20*/("""location
																					.reload()
																		"""),format.raw/*828.19*/("""}"""),format.raw/*828.20*/(""", 4000)

													"""),format.raw/*830.14*/("""}"""),format.raw/*830.15*/(""");

								"""),format.raw/*832.9*/("""}"""),format.raw/*832.10*/("""
							"""),format.raw/*833.8*/("""}"""),format.raw/*833.9*/(""");
		</script>
    
<script>
	$(".getModelclosed").on(
			"click",
			function() """),format.raw/*839.15*/("""{"""),format.raw/*839.16*/("""
				"""),format.raw/*840.5*/("""$.blockUI();
				
				var $row = $(this).closest("tr"); // Find the row
				var complaintNumclosed = $row.find("#compNumclosed").text(); // Find the text
				var $text2 = $row.find("#issueDateclosed").text();
				var $text3 = $row.find("#userNameclosed").text();
				var veh_numclosed = $row.find("#vREgNOclosed").text();
				var status = $row.find("#statusclosed").text();
				var $text5 = $row.find("#xclosed").text();
				var $text6 = $row.find("#yclosed").text();
				var $text7 = $row.find("#zclosed").text();
				var $text8 = $row.find("#aclosed").text();
				var $text9 = $row.find("#issueDateclosed").text();
				//     var $text10 = $row.find("#custPhoneclosed").text();
				$(".resolve_save_closed").val(complaintNumclosed);

				var urlDisposition = "/CRE/searchcomplaintNumClosed/"
						+ complaintNumclosed + "/" + veh_numclosed + "";
				$.ajax("""),format.raw/*858.12*/("""{"""),format.raw/*858.13*/("""
					"""),format.raw/*859.6*/("""url : urlDisposition

				"""),format.raw/*861.5*/("""}"""),format.raw/*861.6*/(""")
						.done(
								function(data) """),format.raw/*863.24*/("""{"""),format.raw/*863.25*/("""
									"""),format.raw/*864.10*/("""if (data != null) """),format.raw/*864.28*/("""{"""),format.raw/*864.29*/("""
										"""),format.raw/*865.11*/("""console.log(data);
										console.log(complaintNumclosed);

										$("#veh_num_popClosed").html(
												data.vehicleRegNo);
										$("#cust_phone_popClosed").html(
												data.customerMobileNo);
										$("#cust_name_popClosed").html(
												data.customerName);
										$("#cust_emailClosed").html(
												data.customerEmail);
										$("#serviceadvisorClosed").html(
												data.serviceadvisor);
										$("#addressNameClosed").html(
												data.preferredAdress);
										$("#chassisnoClosed").html(
												data.chassisNo);
										$("#saledateClosed")
												.html(data.saleDate);
										$("#variantClosed").html(data.variant);
										$("#modelClosed").html(data.model);
										$("#serviceadvisorClosed").html(
												data.serviceadvisor);
										$("#sourceClosed")
												.html(data.sourceName);
										$("#descriptionClosed").html(
												data.description);
										$("#subcomplaintTypeClosed").html(
												data.subcomplaintType);
										$("#complaintTypeClosed").html(
												data.complaintType);
										$("#functionClosed").html(
												data.functionName);
										$("#resolutionByClosed").val(
												data.resolutionBy);
										$("#functionNameClosed").html(
												data.functionName);
										$("#priorityClosed")
												.html(data.priority);
										$("#cityNameClosed")
												.html(data.location);
										$("#ownershipClosed")
												.html(data.wyzUser);
										$("#complaintStatusClosed").val(
												data.complaintStatus);
										$("#customerStatusClosed").val(
												data.customerstatus);
										$("#actionTakenClosed").val(
												data.actionTaken);
										$("#reasonforClosed").val(
												data.reasonFor);
										$("#workshopClosed")
												.html(data.workshop);

										$.unblockUI();

										//alert(data.preferredAdress);

										//alert(data.resolutionBy);
										//            $("#address").val(data.preferredAdress);
									"""),format.raw/*925.10*/("""}"""),format.raw/*925.11*/("""
								"""),format.raw/*926.9*/("""}"""),format.raw/*926.10*/(""");
			"""),format.raw/*927.4*/("""}"""),format.raw/*927.5*/(""");
</script>    
  
   """))
      }
    }
  }

  def render(dealercode:String,dealerName:String,userName:String,complaintData:List[Complaint],compalintDataclosed:List[Complaint]): play.twirl.api.HtmlFormat.Appendable = apply(dealercode,dealerName,userName,complaintData,compalintDataclosed)

  def f:((String,String,String,List[Complaint],List[Complaint]) => play.twirl.api.HtmlFormat.Appendable) = (dealercode,dealerName,userName,complaintData,compalintDataclosed) => apply(dealercode,dealerName,userName,complaintData,compalintDataclosed)

  def ref: this.type = this

}


}

/**/
object complaintResolutionSA extends complaintResolutionSA_Scope0.complaintResolutionSA
              /*
                  -- GENERATED --
                  DATE: Thu Dec 28 12:24:28 IST 2017
                  SOURCE: D:/CRMNEW/28th Dec/crm/app/views/complaintResolutionSA.scala.html
                  HASH: b1381373a286648847823d47cf04a78acafd1eca
                  MATRIX: 823->3|1038->122|1068->127|1146->197|1185->199|1213->201|2162->1123|2204->1149|2244->1151|2273->1153|2336->1189|2349->1193|2391->1214|2428->1224|2441->1228|2483->1249|2518->1256|2532->1260|2575->1281|2659->1338|2672->1342|2714->1363|2744->1366|2757->1370|2800->1391|2866->1430|2879->1434|2921->1455|2951->1458|2964->1462|3006->1483|3072->1522|3085->1526|3127->1547|3157->1550|3170->1554|3206->1569|3272->1608|3285->1612|3327->1633|3357->1636|3370->1640|3411->1660|3477->1699|3490->1703|3532->1724|3562->1727|3575->1731|3623->1758|3689->1797|3702->1801|3744->1822|3774->1825|3787->1829|3826->1847|3892->1886|3905->1890|3947->1911|3977->1914|3990->1918|4027->1934|4093->1973|4106->1977|4148->1998|4178->2001|4191->2005|4234->2027|4316->2082|4329->2086|4371->2107|4401->2110|4414->2114|4453->2132|4519->2171|4532->2175|4574->2196|4604->2199|4617->2203|4657->2222|4767->2305|4780->2309|4822->2330|4859->2340|4872->2344|4915->2365|4955->2377|4969->2381|5012->2402|5291->2654|5304->2658|5346->2679|5424->2730|5437->2734|5479->2755|5558->2807|5571->2811|5613->2832|5695->2887|5708->2891|5744->2906|5818->2953|5831->2957|5872->2977|5953->3031|5966->3035|6014->3062|6088->3109|6101->3113|6140->3131|6214->3178|6227->3182|6264->3198|6379->3286|6392->3290|6435->3312|6516->3366|6529->3370|6568->3388|6666->3459|6679->3463|6721->3484|6969->3701|7011->3715|7832->4509|7880->4541|7920->4543|7964->4559|8026->4594|8039->4598|8081->4619|8165->4676|8178->4680|8220->4701|8305->4759|8318->4763|8360->4784|8448->4845|8461->4849|8497->4864|8577->4917|8590->4921|8631->4941|8718->5001|8731->5005|8779->5032|8859->5085|8872->5089|8911->5107|8991->5160|9004->5164|9041->5180|9151->5263|9164->5267|9207->5289|9295->5349|9309->5353|9349->5371|9454->5448|9468->5452|9511->5473|9764->5694|9807->5708|18632->14504|18663->14512|28195->24015|28226->24023|29239->25005|29271->25009|29358->25067|29388->25068|29424->25076|30215->25838|30245->25839|30280->25846|30336->25874|30365->25875|30416->25897|30446->25898|30481->25905|30528->25923|30558->25924|30594->25932|31773->27083|31802->27084|31836->27090|31865->27091|31900->27098|31929->27099|32054->27195|32084->27196|32124->27208|32552->27607|32582->27608|32622->27619|32677->27645|32707->27646|32748->27658|32864->27745|32894->27746|32962->27786|32992->27787|33030->27797|33082->27820|33112->27821|33152->27832|33207->27858|33237->27859|33278->27871|33396->27960|33426->27961|33492->27999|33522->28000|33552->28001|33586->28006|33616->28007|33656->28018|34001->28334|34031->28335|34074->28349|34136->28382|34166->28383|34245->28433|34275->28434|34320->28450|34367->28468|34397->28469|34443->28486|34557->28571|34587->28572|34638->28594|34749->28676|34779->28677|34828->28697|34858->28698|34903->28714|34999->28781|35029->28782|35079->28803|35168->28863|35198->28864|35251->28888|35281->28889|35323->28903|35353->28904|35392->28915|35421->28916|35555->29021|35585->29022|35625->29034|36229->29609|36259->29610|36299->29621|36354->29647|36384->29648|36425->29660|36544->29750|36574->29751|36642->29791|36672->29792|36702->29793|36736->29798|36766->29799|36806->29810|37217->30192|37247->30193|37290->30207|37352->30240|37382->30241|37461->30291|37491->30292|37536->30308|37583->30326|37613->30327|37659->30344|37773->30429|37803->30430|37854->30452|37967->30536|37997->30537|38046->30557|38076->30558|38121->30574|38217->30641|38247->30642|38297->30663|38386->30723|38416->30724|38469->30748|38499->30749|38541->30763|38571->30764|38608->30773|38637->30774|38753->30861|38783->30862|38817->30868|39724->31746|39754->31747|39789->31754|39845->31782|39874->31783|39943->31823|39973->31824|40013->31835|40060->31853|40090->31854|40131->31866|42274->33980|42304->33981|42342->33991|42372->33992|42407->33999|42436->34000
                  LINES: 27->2|32->2|34->4|34->4|34->4|35->5|61->31|61->31|61->31|62->32|62->32|62->32|62->32|62->32|62->32|62->32|62->32|62->32|62->32|63->33|63->33|63->33|63->33|63->33|63->33|64->34|64->34|64->34|64->34|64->34|64->34|65->35|65->35|65->35|65->35|65->35|65->35|66->36|66->36|66->36|66->36|66->36|66->36|67->37|67->37|67->37|67->37|67->37|67->37|68->38|68->38|68->38|68->38|68->38|68->38|69->39|69->39|69->39|69->39|69->39|69->39|70->40|70->40|70->40|70->40|70->40|70->40|71->41|71->41|71->41|71->41|71->41|71->41|72->42|72->42|72->42|72->42|72->42|72->42|74->44|74->44|74->44|74->44|74->44|74->44|74->44|74->44|74->44|78->48|78->48|78->48|79->49|79->49|79->49|80->50|80->50|80->50|81->51|81->51|81->51|82->52|82->52|82->52|83->53|83->53|83->53|84->54|84->54|84->54|85->55|85->55|85->55|87->57|87->57|87->57|89->59|89->59|89->59|91->61|91->61|91->61|94->64|95->65|118->88|118->88|118->88|120->90|120->90|120->90|120->90|121->91|121->91|121->91|122->92|122->92|122->92|123->93|123->93|123->93|124->94|124->94|124->94|125->95|125->95|125->95|126->96|126->96|126->96|127->97|127->97|127->97|129->99|129->99|129->99|131->101|131->101|131->101|133->103|133->103|133->103|137->107|138->108|371->341|371->341|622->592|622->592|662->632|664->634|667->637|667->637|669->639|687->657|687->657|688->658|690->660|690->660|690->660|690->660|691->661|691->661|691->661|692->662|718->688|718->688|719->689|719->689|720->690|720->690|726->696|726->696|728->698|740->710|740->710|741->711|741->711|741->711|742->712|744->714|744->714|748->718|748->718|749->719|749->719|749->719|750->720|750->720|750->720|751->721|753->723|753->723|756->726|756->726|756->726|756->726|756->726|757->727|768->738|768->738|769->739|770->740|770->740|772->742|772->742|773->743|773->743|773->743|774->744|777->747|777->747|778->748|779->749|779->749|781->751|781->751|782->752|784->754|784->754|785->755|787->757|787->757|789->759|789->759|791->761|791->761|793->763|793->763|799->769|799->769|801->771|817->787|817->787|818->788|818->788|818->788|819->789|821->791|821->791|825->795|825->795|825->795|825->795|825->795|826->796|839->809|839->809|840->810|841->811|841->811|843->813|843->813|844->814|844->814|844->814|845->815|848->818|848->818|849->819|850->820|850->820|852->822|852->822|853->823|855->825|855->825|856->826|858->828|858->828|860->830|860->830|862->832|862->832|863->833|863->833|869->839|869->839|870->840|888->858|888->858|889->859|891->861|891->861|893->863|893->863|894->864|894->864|894->864|895->865|955->925|955->925|956->926|956->926|957->927|957->927
                  -- GENERATED --
              */
          