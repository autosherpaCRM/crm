
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object addCustomer_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class addCustomer extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[String,String,String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(dealercode:String,dealerName:String,userName:String,oem:String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.66*/("""

"""),_display_(/*4.2*/mainPageCRE("AutoSherpaCRM",userName,dealerName,dealercode,oem)/*4.65*/ {_display_(Seq[Any](format.raw/*4.67*/("""
"""),_display_(/*5.2*/helper/*5.8*/.form(routes.CallInteractionController.postAddCustomer() ,'id -> "form1")/*5.81*/  {_display_(Seq[Any](format.raw/*5.84*/("""

"""),format.raw/*7.1*/("""<div class="row" >
  <div class="col-lg-12">
    <div class="panel panel-primary">
      <div class="panel-heading">   
        Add Customer </div>
		<div class="panel-body">
            <div class="col-lg-3">
              <div class="form-group">
                <label for="customerName">Customer Name<i style="color:red">*</i>:</label>
                <input type="text" class="form-control textOnlyAccepted" name="customerName"  id="customerName" autocomplete="off"  required/>
              </div>
            </div>
             <div class="col-lg-3">             
              <div class="form-group">
                <label for="customerPhone">Customer Phone:<i style="color:red">*</i></label>
                <input type="text" class="form-control numberOnly" maxlength="10" name="phoneNumber" id="customerPhone" autocomplete="off" required/>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label for="customerEmail">Customer Email:</label>
                <input type="text" class="form-control" name="emailAddress" id="customerEmail" autocomplete="off"  required/>
              </div>
            </div>
           <div class="col-lg-3">
              <div class="form-group">
                <label for="customerAddress">Customer Address :</label>
                <input type="text" class="form-control" name="addressLine1" id="customerAddress" autocomplete="off"  required/>
              </div>
            </div>
           <!--   <div class="col-lg-3">
              <div class="form-group">
                <label for="vehicleNumber">Vehicle Number :</label>
                <input type="text" class="form-control" name="vehicleNumber" id="vehicleNumber" autocomplete="off"  required/>
              </div>
            </div> -->
          <div class="col-lg-3">
              <div class="form-group">
                <label for="vehicleRegNo">Vehicle Reg No :<i style="color:red">*</i></label>
                <input type="text" class="form-control" name="vehicleRegNo" id="vehicleRegNo" autocomplete="off"  required/>
              </div>
            </div>
            
            <div class="col-lg-3">
              <div class="form-group">
                <label for="model">Model :<i style="color:red">*</i></label>
                <input type="text" class="form-control" name="model"  id="model" autocomplete="off"  required/>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label for="milege">Mileage :<i style="color:red">*</i></label>
                <input type="text" class="form-control numberOnly" name="milege" maxlength="6" id="milege" autocomplete="off"  required/>
              </div>
            </div> 
            <div class="col-lg-3">                  
             <div class="form-group">
                <label for="">&nbsp;</label>
             <input type="button" class="btn btn-primary btn-block" onclick="return validatingCustomerForm()" value="Submit" />
       
      </div>
      
        </div>
        
        </div>
     </div>
     </div>
     
     </div>
     
    
     """)))}),format.raw/*78.7*/("""
     """)))}),format.raw/*79.7*/("""
     
        """),format.raw/*81.9*/("""<script>
            
            function validatingCustomerForm()"""),format.raw/*83.46*/("""{"""),format.raw/*83.47*/("""
			
			"""),format.raw/*85.4*/("""var custName=$("#customerName").val();
			var custPhone=$("#customerPhone").val();
			var custRegn=$("#vehicleRegNo").val();
			var custModel=$("#model").val();
			var custMilege=$("#milege").val();
				if(custName=='' && custPhone=='' && custRegn=='' && custModel=='' && custMilege=='')"""),format.raw/*90.89*/("""{"""),format.raw/*90.90*/("""
			"""),format.raw/*91.4*/("""Lobibox.notify('warning', """),format.raw/*91.30*/("""{"""),format.raw/*91.31*/("""
							"""),format.raw/*92.8*/("""continueDelayOnInactiveTab: true,
							msg: 'Please Enter The Mandatory Fields .'
						"""),format.raw/*94.7*/("""}"""),format.raw/*94.8*/(""");
						return false;
			
		"""),format.raw/*97.3*/("""}"""),format.raw/*97.4*/("""
		
		"""),format.raw/*99.3*/("""else if(custName=='')"""),format.raw/*99.24*/("""{"""),format.raw/*99.25*/("""
			"""),format.raw/*100.4*/("""Lobibox.notify('warning', """),format.raw/*100.30*/("""{"""),format.raw/*100.31*/("""
							"""),format.raw/*101.8*/("""continueDelayOnInactiveTab: true,
							msg: 'Please Enter Name'
						"""),format.raw/*103.7*/("""}"""),format.raw/*103.8*/(""");
						return false;
			
		"""),format.raw/*106.3*/("""}"""),format.raw/*106.4*/("""
		"""),format.raw/*107.3*/("""else if(custPhone=='')"""),format.raw/*107.25*/("""{"""),format.raw/*107.26*/("""
			"""),format.raw/*108.4*/("""Lobibox.notify('warning', """),format.raw/*108.30*/("""{"""),format.raw/*108.31*/("""
							"""),format.raw/*109.8*/("""continueDelayOnInactiveTab: true,
							msg: 'Please Enter Phone Number'
						"""),format.raw/*111.7*/("""}"""),format.raw/*111.8*/(""");
						return false;
			
		"""),format.raw/*114.3*/("""}"""),format.raw/*114.4*/("""
		"""),format.raw/*115.3*/("""else if(custPhone.length < 10 )"""),format.raw/*115.34*/("""{"""),format.raw/*115.35*/("""
			
		
			"""),format.raw/*118.4*/("""Lobibox.notify('warning', """),format.raw/*118.30*/("""{"""),format.raw/*118.31*/("""
							"""),format.raw/*119.8*/("""continueDelayOnInactiveTab: true,
							msg: 'Invalid Phone No.'
						"""),format.raw/*121.7*/("""}"""),format.raw/*121.8*/(""");
						
						return false;
						
						
			
		"""),format.raw/*127.3*/("""}"""),format.raw/*127.4*/("""
		"""),format.raw/*128.3*/("""else if(custRegn=='')"""),format.raw/*128.24*/("""{"""),format.raw/*128.25*/("""
			"""),format.raw/*129.4*/("""Lobibox.notify('warning', """),format.raw/*129.30*/("""{"""),format.raw/*129.31*/("""
							"""),format.raw/*130.8*/("""continueDelayOnInactiveTab: true,
							msg: 'Please Enter Registration No.'
						"""),format.raw/*132.7*/("""}"""),format.raw/*132.8*/(""");
						return false;
			
		"""),format.raw/*135.3*/("""}"""),format.raw/*135.4*/("""
			"""),format.raw/*136.4*/("""else if(custModel=='')"""),format.raw/*136.26*/("""{"""),format.raw/*136.27*/("""
			"""),format.raw/*137.4*/("""Lobibox.notify('warning', """),format.raw/*137.30*/("""{"""),format.raw/*137.31*/("""
							"""),format.raw/*138.8*/("""continueDelayOnInactiveTab: true,
							msg: 'Please Enter Model No.'
						"""),format.raw/*140.7*/("""}"""),format.raw/*140.8*/(""");
						return false;
			
		"""),format.raw/*143.3*/("""}"""),format.raw/*143.4*/("""
			"""),format.raw/*144.4*/("""else if(custMilege=='')"""),format.raw/*144.27*/("""{"""),format.raw/*144.28*/("""
			"""),format.raw/*145.4*/("""Lobibox.notify('warning', """),format.raw/*145.30*/("""{"""),format.raw/*145.31*/("""
							"""),format.raw/*146.8*/("""continueDelayOnInactiveTab: true,
							msg: 'Please Enter Milege'
						"""),format.raw/*148.7*/("""}"""),format.raw/*148.8*/(""");
						return false;
			
		"""),format.raw/*151.3*/("""}"""),format.raw/*151.4*/("""
			
			
			"""),format.raw/*154.4*/("""else"""),format.raw/*154.8*/("""{"""),format.raw/*154.9*/("""
			
			"""),format.raw/*156.4*/("""}"""),format.raw/*156.5*/("""
			 
               """),format.raw/*158.16*/("""var vehicleReg= document.getElementById("vehicleRegNo").value;
               

              // alert(vehicleReg);
               var urlDisposition = "/CRE/vehicleRegNoExist/" + vehicleReg + "";
               $.ajax("""),format.raw/*163.23*/("""{"""),format.raw/*163.24*/("""
                   """),format.raw/*164.20*/("""url: urlDisposition

               """),format.raw/*166.16*/("""}"""),format.raw/*166.17*/(""").done(function (data) """),format.raw/*166.40*/("""{"""),format.raw/*166.41*/("""

                   """),format.raw/*168.20*/("""if (data > 0) """),format.raw/*168.34*/("""{"""),format.raw/*168.35*/("""

                	   """),format.raw/*170.21*/("""Lobibox.notify('error', """),format.raw/*170.45*/("""{"""),format.raw/*170.46*/("""
						"""),format.raw/*171.7*/("""msg: 'This Vehicle Reg no already exist cannot add to database'
						"""),format.raw/*172.7*/("""}"""),format.raw/*172.8*/(""");
						return false;
           		    		        

                   """),format.raw/*176.20*/("""}"""),format.raw/*176.21*/("""else"""),format.raw/*176.25*/("""{"""),format.raw/*176.26*/("""
                	   """),format.raw/*177.21*/("""document.getElementById('form1').submit();
                	   Lobibox.notify('info', """),format.raw/*178.44*/("""{"""),format.raw/*178.45*/("""
   						"""),format.raw/*179.10*/("""msg: 'Submitted successfully!'
   						"""),format.raw/*180.10*/("""}"""),format.raw/*180.11*/(""");

                	   
   						return true;

                       """),format.raw/*185.24*/("""}"""),format.raw/*185.25*/("""
               """),format.raw/*186.16*/("""}"""),format.raw/*186.17*/(""");
               
               """),format.raw/*188.16*/("""}"""),format.raw/*188.17*/("""
    
  
  """),format.raw/*191.3*/("""$('.numberOnly').keypress(function (e) """),format.raw/*191.42*/("""{"""),format.raw/*191.43*/("""
		
        """),format.raw/*193.9*/("""if (isNaN(this.value + "" + "." + String.fromCharCode(e.charCode))) """),format.raw/*193.77*/("""{"""),format.raw/*193.78*/("""
					"""),format.raw/*194.6*/("""return false;
        """),format.raw/*195.9*/("""}"""),format.raw/*195.10*/("""

    """),format.raw/*197.5*/("""}"""),format.raw/*197.6*/(""");
	
	$(".textOnlyAccepted").keypress(function (e) """),format.raw/*199.47*/("""{"""),format.raw/*199.48*/("""
            """),format.raw/*200.13*/("""var code = e.keyCode || e.which;
            if ((code < 65 || code > 90) && (code < 97 || code > 122) && code != 32 && code != 46)
            """),format.raw/*202.13*/("""{"""),format.raw/*202.14*/("""
                 """),format.raw/*203.18*/("""return false;
            """),format.raw/*204.13*/("""}"""),format.raw/*204.14*/("""
        """),format.raw/*205.9*/("""}"""),format.raw/*205.10*/(""");
    

   
    
</script>"""))
      }
    }
  }

  def render(dealercode:String,dealerName:String,userName:String,oem:String): play.twirl.api.HtmlFormat.Appendable = apply(dealercode,dealerName,userName,oem)

  def f:((String,String,String,String) => play.twirl.api.HtmlFormat.Appendable) = (dealercode,dealerName,userName,oem) => apply(dealercode,dealerName,userName,oem)

  def ref: this.type = this

}


}

/**/
object addCustomer extends addCustomer_Scope0.addCustomer
              /*
                  -- GENERATED --
                  DATE: Thu Dec 28 12:24:24 IST 2017
                  SOURCE: D:/CRMNEW/28th Dec/crm/app/views/addCustomer.scala.html
                  HASH: e85b0268541767e200e22248573aef612e1c79a0
                  MATRIX: 778->3|937->67|967->72|1038->135|1077->137|1105->140|1118->146|1199->219|1239->222|1269->226|4572->3499|4610->3507|4654->3524|4751->3593|4780->3594|4817->3604|5137->3896|5166->3897|5198->3902|5252->3928|5281->3929|5317->3938|5436->4030|5464->4031|5523->4063|5551->4064|5586->4072|5635->4093|5664->4094|5697->4099|5752->4125|5782->4126|5819->4135|5921->4209|5950->4210|6010->4242|6039->4243|6071->4247|6122->4269|6152->4270|6185->4275|6240->4301|6270->4302|6307->4311|6417->4393|6446->4394|6506->4426|6535->4427|6567->4431|6627->4462|6657->4463|6699->4477|6754->4503|6784->4504|6821->4513|6923->4587|6952->4588|7036->4644|7065->4645|7097->4649|7147->4670|7177->4671|7210->4676|7265->4702|7295->4703|7332->4712|7446->4798|7475->4799|7535->4831|7564->4832|7597->4837|7648->4859|7678->4860|7711->4865|7766->4891|7796->4892|7833->4901|7940->4980|7969->4981|8029->5013|8058->5014|8091->5019|8143->5042|8173->5043|8206->5048|8261->5074|8291->5075|8328->5084|8432->5160|8461->5161|8521->5193|8550->5194|8593->5209|8625->5213|8654->5214|8692->5224|8721->5225|8773->5248|9026->5472|9056->5473|9106->5494|9173->5532|9203->5533|9255->5556|9285->5557|9337->5580|9380->5594|9410->5595|9463->5619|9516->5643|9546->5644|9582->5652|9681->5723|9710->5724|9814->5799|9844->5800|9877->5804|9907->5805|9958->5827|10074->5914|10104->5915|10144->5926|10214->5967|10244->5968|10349->6044|10379->6045|10425->6062|10455->6063|10520->6099|10550->6100|10592->6114|10660->6153|10690->6154|10732->6168|10829->6236|10859->6237|10894->6244|10945->6267|10975->6268|11011->6276|11040->6277|11122->6330|11152->6331|11195->6345|11370->6491|11400->6492|11448->6511|11504->6538|11534->6539|11572->6549|11602->6550
                  LINES: 27->2|32->2|34->4|34->4|34->4|35->5|35->5|35->5|35->5|37->7|108->78|109->79|111->81|113->83|113->83|115->85|120->90|120->90|121->91|121->91|121->91|122->92|124->94|124->94|127->97|127->97|129->99|129->99|129->99|130->100|130->100|130->100|131->101|133->103|133->103|136->106|136->106|137->107|137->107|137->107|138->108|138->108|138->108|139->109|141->111|141->111|144->114|144->114|145->115|145->115|145->115|148->118|148->118|148->118|149->119|151->121|151->121|157->127|157->127|158->128|158->128|158->128|159->129|159->129|159->129|160->130|162->132|162->132|165->135|165->135|166->136|166->136|166->136|167->137|167->137|167->137|168->138|170->140|170->140|173->143|173->143|174->144|174->144|174->144|175->145|175->145|175->145|176->146|178->148|178->148|181->151|181->151|184->154|184->154|184->154|186->156|186->156|188->158|193->163|193->163|194->164|196->166|196->166|196->166|196->166|198->168|198->168|198->168|200->170|200->170|200->170|201->171|202->172|202->172|206->176|206->176|206->176|206->176|207->177|208->178|208->178|209->179|210->180|210->180|215->185|215->185|216->186|216->186|218->188|218->188|221->191|221->191|221->191|223->193|223->193|223->193|224->194|225->195|225->195|227->197|227->197|229->199|229->199|230->200|232->202|232->202|233->203|234->204|234->204|235->205|235->205
                  -- GENERATED --
              */
          