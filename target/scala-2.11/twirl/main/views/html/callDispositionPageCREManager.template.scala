
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object callDispositionPageCREManager_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class callDispositionPageCREManager extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template8[List[Location],String,String,Map[Long, String],List[Campaign],List[ServiceTypes],List[String],List[CallDispositionData],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(listCity:List[Location],dealerName:String,user:String,allCREHash:Map[Long,String],campaignList :List[Campaign],serviceTypeList :List[ServiceTypes],bookedServiceTypeList:List[String],dispoList:List[CallDispositionData]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.221*/("""
"""),_display_(/*2.2*/mainPageCREManger("AutoSherpaCRM",user,dealerName)/*2.52*/ {_display_(Seq[Any](format.raw/*2.54*/("""	



"""),format.raw/*6.1*/("""<style>
    td, th """),format.raw/*7.12*/("""{"""),format.raw/*7.13*/("""
                
        """),format.raw/*9.9*/("""border: 1px solid black;
        overflow: hidden; 
        text-align:center!important;
    """),format.raw/*12.5*/("""}"""),format.raw/*12.6*/("""

"""),format.raw/*14.1*/("""</style>
 
  <div>

        <div class="panel panel-primary">
            <div class="panel-heading">   
                Scheduled Call Log Information </div>
            <div class="panel-body">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#home" data-toggle="tab" id="Tab1" onclick="assignedInteractionDataMR();">Scheduled</a> </li>
                    <li><a href="#profile" data-toggle="tab" id="Tab2" onclick="ajaxCallForFollowUpRequiredServerMR();">Follow Up Required</a> </li>
                    <li><a href="#messages" data-toggle="tab" id="Tab3" onclick="ajaxCallForserviceBookedServerDataMR();">Service Booked</a> </li>
                    <li><a href="#settings" data-toggle="tab" id="Tab4" onclick="ajaxCallForServiceNotRequiredServerMR();" >Service Not Required</a> </li>
                    <li><a href="#nonContacts" data-toggle="tab" id="Tab5" onclick="ajaxCallForNonContactsServerMR();">Non Contacts</a> </li>
                    <li><a href="#droppedBuc" data-toggle="tab" id="Tab6" onclick="ajaxCallFordroppedCallsServerDataMR();">Dropped</a> </li>
					<li><a href="#missedCallBuc" data-toggle="tab" id="Tab7" onclick="ajaxCallMissedCallsServerDataMR();">Missed Calls</a> </li>
                    <li><a href="#incomingCallBuc" data-toggle="tab" id="Tab8" onclick="ajaxCallForincomingCallsServerDataMR();">Incoming Calls</a> </li>  
                    <li><a href="#outgoingCallBuc" data-toggle="tab" id="Tab9" onclick="ajaxCallForOutgoingCallBucCallsServerDataMR();">Outgoing Calls</a> </li>
                    
                </ul>
                <div class="tab-content">
                <div class="panel panel-default" style="border-top: #fff;">
				<div class="panel-body">
                <div class="row">
		<div class="col-md-2" id="cityDiv">
		<div class="form-group">
                <label>City </label>
               
               <select class="form-control" id="city" name="cityName" onchange="ajaxCallToLoadWorkShopByCity();"> 
                <option value='--Select--'>--Select--</option>
                
				"""),_display_(/*45.6*/for(city <- listCity) yield /*45.27*/{_display_(Seq[Any](format.raw/*45.28*/("""                  	                         
                    """),format.raw/*46.21*/("""<option value=""""),_display_(/*46.37*/city/*46.41*/.getName()),format.raw/*46.51*/("""">"""),_display_(/*46.54*/city/*46.58*/.getName()),format.raw/*46.68*/("""</option>
                    """)))}),format.raw/*47.22*/("""
                """),format.raw/*48.17*/("""</select>
</div>
            </div> 
			<div class="col-md-2" id="workshopDiv">
			<div class="form-group">
                <label>WorkShop Location </label>               
                <select class="form-control" id="workshop" name="workshopId" onchange="ajaxCallToLoadCRESByWorkshop();">                 			    
                

                </select>
			</div>
            </div> 
		
            <div class="col-md-2" id="cresDiv">
			 <label>Select CRE's </label>
			<div class="form-group">
              <select class="selectpicker filter form-control" data-column-index="7" id="ddlCreIds" name="ddlCreIds" multiple>
                
                    
                </select> 
</div>


            </div>
            
                <div class="col-md-2 tab-pane" id="campaignDiv">
                                
                                <label>Select Campaign</label>
						<select class="filter form-control" id="campaignName" data-column-index="0" name="campaignName">
							<option value="0" >--Select--</option>
							"""),_display_(/*78.9*/for(campaign_List<-campaignList) yield /*78.41*/{_display_(Seq[Any](format.raw/*78.42*/("""
		                                """),format.raw/*79.35*/("""<option value=""""),_display_(/*79.51*/campaign_List/*79.64*/.getCampaignName()),format.raw/*79.82*/("""">"""),_display_(/*79.85*/campaign_List/*79.98*/.getCampaignName()),format.raw/*79.116*/("""</option>
		                             """)))}),format.raw/*80.33*/("""
		                                
						"""),format.raw/*82.7*/("""</select>
					</div>
					
					<div class="col-md-2" id="fromDateDiv">
					<label>From Assign Date : </label>
					<input type="text" class="filter form-control datepickerFilter" placeholder="From Assign Date" data-column-index="1" id="fromduedaterange" name="fromduedaterange" readonly>
				  </div>
				  <div class="col-md-2" id="toDateDiv">
					<label>To Assign Date : </label>
					<input type="text" class="filter form-control datepickerFilter" placeholder="To Assign Date" data-column-index="2" id="toduedaterange" name="toduedaterange" readonly>
				  </div>
				  </div>
				  <div class="row">
				   
						<div class="col-md-2" id="serviceTypeDiv">
					
			
						<label>Select service type</label>
						<select class="filter form-control" id="ServiceTypeName" data-column-index="3" name="ServiceTypeName">
							<option value="0" >--Select--</option>
							"""),_display_(/*102.9*/for(serviceType_List<-serviceTypeList) yield /*102.47*/{_display_(Seq[Any](format.raw/*102.48*/("""
			                                """),format.raw/*103.36*/("""<option value=""""),_display_(/*103.52*/serviceType_List/*103.68*/.getServiceTypeName()),format.raw/*103.89*/("""">"""),_display_(/*103.92*/serviceType_List/*103.108*/.getServiceTypeName()),format.raw/*103.129*/("""</option>
			                             """)))}),format.raw/*104.34*/("""
			                                
						"""),format.raw/*106.7*/("""</select>
					</div>
					 
					<div class="col-md-2" id="serviceBookTypeDiv" style="display:none">						
					<label>Select Booked service type</label>
						<select class="filter form-control" id="serviceBookedType" data-column-index="4" name="serviceBookedType">
							<option value="0" >--Select--</option>
							"""),_display_(/*113.9*/for(bookedServiceType_List<-bookedServiceTypeList) yield /*113.59*/{_display_(Seq[Any](format.raw/*113.60*/("""
 """),format.raw/*114.2*/("""<option value=""""),_display_(/*114.18*/bookedServiceType_List),format.raw/*114.40*/("""">"""),_display_(/*114.43*/bookedServiceType_List),format.raw/*114.65*/("""</option>					                   
           """)))}),format.raw/*115.13*/("""
					                                
						"""),format.raw/*117.7*/("""</select>
					</div>
					<div class="col-md-2" id="lastDispoTypeDiv" style="display:none">						
					<label>Last Disposition</label>
						<select class="filter form-control" id="lastDispo" data-column-index="5" >
							<option value="0" >--Select--</option>
								"""),_display_(/*123.10*/for(dispo <- dispoList) yield /*123.33*/{_display_(Seq[Any](format.raw/*123.34*/("""
			"""),format.raw/*124.4*/("""<option value=""""),_display_(/*124.20*/dispo/*124.25*/.getDisposition()),format.raw/*124.42*/("""">"""),_display_(/*124.45*/dispo/*124.50*/.getDisposition()),format.raw/*124.67*/("""</option>
			
		""")))}),format.raw/*126.4*/("""
							
					                                
						"""),format.raw/*129.7*/("""</select>
					</div>
					<div class="col-md-2" id="droppedcountDiv" style="display:none">						
					<label>Dropped Count</label>
						<select class="filter form-control" id="droppedCount" data-column-index="6">
							<option value="0" >--Select--</option>
							<option value="1" >1</option>
							<option value="2" >2</option>
							<option value="3" >3</option>
							<option value="4" >4</option>
							
					                                
						</select>
					</div>
				</div>
				</div>
				</div>
                
                    <div class="panel panel-default tab-pane fade in active" id="home">
                        <div class="panel-body inf-content">              

                            <div class="dataTable_wrapper">                            
                                <div >
                                    <table class="table table-striped table-bordered table-hover" id="assignedInteractionTableMR">
                                        <thead>
                                            <tr> 

                                             <th>Customer Name</th>
                                                <th>RegNo.</th>
                                                <th>Model</th> 
                                                <th>Category</th>
                                                <th>Loyalty</th>
                                                <th>DueDate</th>
                                                <th>Type</th>
                                                <th>Forecast </th>
                                                <th>PSFStatus</th>
                                                <th>DND</th>
                                                <th>Complaint</th>
                                                <th>CRE Name</th>
                                            </tr>
                                        </thead>
                                        <tbody>								
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default tab-pane fade" id="profile">
                        <div class="panel-body inf-content">
                            <div class="dataTable_wrapper">                            
                                <div >
                                    <table class="table table-striped table-bordered table-hover" id="followUpRequiredServerDataMR" >
                                        <thead>
                                         <tr> 
                                          		<th>Campaign Name</th>
                                          		<th>Call Date</th>
                                                <th>Customer Name</th>
                                                <th>Mobile Number</th>                                                                   
                                                <th>Vehicle RegNo.</th>
                                                <th>Service Due</th>
                                                <th>CRE Name</th>
                                                <th>FollowUp Date</th>                                    
                                                <th>FollowUp Time</th>
                                                <th>Last Disposition</th>
												<th>Is Call Initiated</th>         
                                               <!--  <th>Download Media</th> -->
                                            </tr>   
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default tab-pane fade" id="messages">
                        <div class="panel-body inf-content">
                            <div class="dataTable_wrapper">                            
                                <div >
                                    <table class="table table-striped table-bordered table-hover" id="serviceBookedServerDataMR">
                                        <thead>
                                            <tr> 
                                            	<th>Campaign Name</th>
                                            	<th>Call Date</th>
                                                <th>Customer Name</th>
                                                <th>Mobile Number</th>         
                                                <th>Vehicle RegNo.</th>
                                                <th>Service Due</th>
                                                <th>CRE Name</th>
                                                <th>Service ScheduledDate</th>  
                                                <th>Service ScheduledTime</th>  
                                                <th>Booking Status</th>
												<th>Is Call Initiated</th>
                                                <!--  <th>Download Media</th> -->
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default tab-pane fade" id="settings">
                         <div class="panel-body inf-content">
                        <div class="dataTable_wrapper">                            
                            <div >
                                <table class="table table-striped table-bordered table-hover" id="serviceNotRequiredServerDataMR">
                                    <thead>
                                        <tr> 
                                        	<th>Campaign Name</th>
                                        	<th>Call Date</th>             	
                                            <th>Customer Name</th>
                                            <th>Mobile Number</th>         
                                            <th>Vehicle RegNo.</th>
                                            <th>Service Due</th>
                                            <th>CRE Name</th>
                                            <th>Last Disposition</th>
                                            <th>Reason</th>
                                           <!--  <th>Download Media</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    </div><!--End tab panel-->
                    <div class="panel panel-default tab-pane fade " id="nonContacts">
                        <div class="panel-body inf-content"> 
                            <div >
                                <table class="table table-striped table-bordered table-hover" id="nonContactsServerDataMR">
                                    <thead>
                                        <tr> 
                                        <th>Campaign Name</th>
                                        <th>Call Date</th>                         
                                            <th>Customer Name</th>
                                            <th>Mobile Number</th>         
                                            <th>Vehicle RegNo.</th>
                                            <th>Service Due</th>
                                            <th>CRE Name</th>
                                            <th>Last Disposition</th>
                                            <th>Is Call Initiated</th>         											
                                         <!--  <th>Download Media</th> -->   
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default tab-pane fade" id="droppedBuc">
                        <div class="panel-body inf-content">
                            <div class="dataTable_wrapper">                            
                                <div style="overflow-x: auto">
                                    <table class="table table-striped table-bordered table-hover" id="droppedCallsServerDataMR">
                                        <thead>
                                            <tr>
                                            	<th>Campaign Name</th> 
                                            	<th>Call Date</th>
                                                <th>Customer Name</th>
                                                <th>Mobile Number</th>         
                                                <th>Vehicle RegNo.</th>
                                                <th>Service Due</th>
                                                <th>CRE Name</th>
												<th>Is Call Initiated</th>         
                                                <th>Last Disposition</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
					 <div class="panel panel-default tab-pane fade" id="missedCallBuc">
                        <div class="panel-body inf-content">
                            <div class="dataTable_wrapper">                            
                                <div style="overflow-x: auto">
                                    <table class="table table-striped table-bordered table-hover" id="missedCallsServerDataMR" width="100%">
                                        <thead>
                                            <tr>
       											<th>Call Date</th>
                                                <th>Customer Name</th>
                                                <th>Vehicle RegNo.</th>                                               
                                                <th>Mobile Number</th>
                                                <th>CRE Name</th>
                                                <!--  <th>Download Media</th> -->
                                                
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="panel panel-default tab-pane fade" id="incomingCallBuc">
                        <div class="panel-body inf-content">
                            <div class="dataTable_wrapper">                            
                                <div style="overflow-x: auto">
                                    <table class="table table-striped table-bordered table-hover" id="incomingCallsServerDataMR" width="100%">
                                        <thead>
                                            <tr>
                                            	<th>Call Date</th>
                                                <th>Customer Name</th>
                                                <th>Vehicle RegNo.</th>                                                
                                                <th>Mobile Number</th>
                                                <th>CRE Name</th>
                                               <!--  <th>Download Media</th> -->
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="panel panel-default tab-pane fade" id="outgoingCallBuc">
                        <div class="panel-body inf-content">
                            <div class="dataTable_wrapper">                            
                                <div style="overflow-x: auto">
                                    <table class="table table-striped table-bordered table-hover" id="outgoingCallsServerDataMR" width="100%">
                                        <thead>
                                            <tr>
                                            	<th>Call Date</th>
                                                <th>Customer Name</th>
                                                <th>Vehicle RegNo.</th>                                                
                                                <th>Mobile Number</th>
                                                <th>CRE Name</th>
                                               <!--  <th>Download Media</th> -->
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div><!--End tab content-->
            </div><!--End panel body-->
        </div>
   
</div>
""")))}),format.raw/*386.2*/(""" 

"""),format.raw/*388.1*/("""<script type="text/javascript"> 

function assignedInteractionDataMR()"""),format.raw/*390.37*/("""{"""),format.raw/*390.38*/("""
    """),format.raw/*391.5*/("""// alert("server side data load");
    document.getElementById("cresDiv").style.display = "block";
	document.getElementById("workshopDiv").style.display = "block";
	document.getElementById("cityDiv").style.display = "block";	
	document.getElementById("campaignDiv").style.display = "block";
    document.getElementById("serviceBookTypeDiv").style.display = "none";
    document.getElementById("serviceTypeDiv").style.display = "block";
    document.getElementById("fromDateDiv").style.display = "block";
	document.getElementById("toDateDiv").style.display = "block";
	document.getElementById("lastDispoTypeDiv").style.display = "none";
	document.getElementById("droppedcountDiv").style.display = "none";

    var UserIds="",i;
    myOption = document.getElementById('ddlCreIds');
    
for (i=0;i<myOption.options.length;i++)"""),format.raw/*406.40*/("""{"""),format.raw/*406.41*/("""
    """),format.raw/*407.5*/("""if(myOption.options[i].selected)"""),format.raw/*407.37*/("""{"""),format.raw/*407.38*/("""
        """),format.raw/*408.9*/("""if(myOption.options[i].value != "--Select--")"""),format.raw/*408.54*/("""{"""),format.raw/*408.55*/("""
    	 """),format.raw/*409.7*/("""UserIds = UserIds + myOption.options[i].value + ",";
        """),format.raw/*410.9*/("""}"""),format.raw/*410.10*/("""
    """),format.raw/*411.5*/("""}"""),format.raw/*411.6*/("""
"""),format.raw/*412.1*/("""}"""),format.raw/*412.2*/("""
"""),format.raw/*413.1*/("""if(UserIds.length > 0)"""),format.raw/*413.23*/("""{"""),format.raw/*413.24*/("""
    	"""),format.raw/*414.6*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
"""),format.raw/*415.1*/("""}"""),format.raw/*415.2*/("""
"""),format.raw/*416.1*/("""else"""),format.raw/*416.5*/("""{"""),format.raw/*416.6*/("""
"""),format.raw/*417.1*/("""UserIds="Select";
"""),format.raw/*418.1*/("""}"""),format.raw/*418.2*/("""
    """),format.raw/*419.5*/("""var ajaxUrl = "/assignedInteractionTableDataMR/"+UserIds+"";
    
    
    var table= $('#assignedInteractionTableMR').dataTable( """),format.raw/*422.60*/("""{"""),format.raw/*422.61*/("""
        """),format.raw/*423.9*/(""""bDestroy": true,
        "processing": true,
        "serverSide": true,
        "scrollY": 300,
		 "scrollX": true,
        "paging": true,
        "searching":true,
        "ordering":false,	
        "ajax": ajaxUrl,
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*432.59*/("""{"""),format.raw/*432.60*/("""
              """),format.raw/*433.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*435.15*/("""}"""),format.raw/*435.16*/("""
    """),format.raw/*436.5*/("""}"""),format.raw/*436.6*/(""" """),format.raw/*436.7*/(""");

    FilterOptionDataMR(table);
    
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e)"""),format.raw/*440.61*/("""{"""),format.raw/*440.62*/("""
      """),format.raw/*441.7*/("""$($.fn.dataTable.tables(true)).DataTable()
         .columns.adjust();
   """),format.raw/*443.4*/("""}"""),format.raw/*443.5*/(""");
    
    """),format.raw/*445.5*/("""}"""),format.raw/*445.6*/("""




"""),format.raw/*450.1*/("""function ajaxCallForFollowUpRequiredServerMR()"""),format.raw/*450.47*/("""{"""),format.raw/*450.48*/("""
	"""),format.raw/*451.2*/("""document.getElementById("cresDiv").style.display = "block";
	document.getElementById("workshopDiv").style.display = "block";
	document.getElementById("cityDiv").style.display = "block";	
	document.getElementById("campaignDiv").style.display = "block";
	document.getElementById("serviceBookTypeDiv").style.display = "none";
	document.getElementById("serviceTypeDiv").style.display = "block";
	document.getElementById("fromDateDiv").style.display = "block";
	document.getElementById("toDateDiv").style.display = "block";
	document.getElementById("lastDispoTypeDiv").style.display = "none";
	document.getElementById("droppedcountDiv").style.display = "none";
	          

	    var UserIds="",i;
	    myOption = document.getElementById('ddlCreIds');
	    
	for (i=0;i<myOption.options.length;i++)"""),format.raw/*466.41*/("""{"""),format.raw/*466.42*/("""
	    """),format.raw/*467.6*/("""if(myOption.options[i].selected)"""),format.raw/*467.38*/("""{"""),format.raw/*467.39*/("""
	        """),format.raw/*468.10*/("""if(myOption.options[i].value != "--Select--")"""),format.raw/*468.55*/("""{"""),format.raw/*468.56*/("""
	    	 """),format.raw/*469.8*/("""UserIds = UserIds + myOption.options[i].value + ",";
	        """),format.raw/*470.10*/("""}"""),format.raw/*470.11*/("""
	    """),format.raw/*471.6*/("""}"""),format.raw/*471.7*/("""
	"""),format.raw/*472.2*/("""}"""),format.raw/*472.3*/("""
	"""),format.raw/*473.2*/("""if(UserIds.length > 0)"""),format.raw/*473.24*/("""{"""),format.raw/*473.25*/("""
	    	"""),format.raw/*474.7*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
	"""),format.raw/*475.2*/("""}"""),format.raw/*475.3*/("""
	"""),format.raw/*476.2*/("""else"""),format.raw/*476.6*/("""{"""),format.raw/*476.7*/("""
	"""),format.raw/*477.2*/("""UserIds="Select";
	"""),format.raw/*478.2*/("""}"""),format.raw/*478.3*/("""
	    """),format.raw/*479.6*/("""var ajaxUrl = "/followUpCallLogTableDataMR/"+UserIds+"";
	    
	    
	    var table= $('#followUpRequiredServerDataMR').dataTable( """),format.raw/*482.63*/("""{"""),format.raw/*482.64*/("""
	        """),format.raw/*483.10*/(""""bDestroy": true,
	        "processing": true,
	        "serverSide": true,
	        "scrollY": 300,
			 "scrollX": true,
	        "paging": true,
	        "searching":true,
	        "ordering":false,	
	        "ajax": ajaxUrl,
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*492.60*/("""{"""),format.raw/*492.61*/("""
              """),format.raw/*493.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*495.15*/("""}"""),format.raw/*495.16*/("""
	    """),format.raw/*496.6*/("""}"""),format.raw/*496.7*/(""" """),format.raw/*496.8*/(""");

	    FilterOptionDataMR(table);
	    
	"""),format.raw/*500.2*/("""}"""),format.raw/*500.3*/("""


"""),format.raw/*503.1*/("""function ajaxCallForserviceBookedServerDataMR()"""),format.raw/*503.48*/("""{"""),format.raw/*503.49*/("""
	"""),format.raw/*504.2*/("""document.getElementById("cresDiv").style.display = "block";
	document.getElementById("workshopDiv").style.display = "block";
	document.getElementById("cityDiv").style.display = "block";	
	document.getElementById("campaignDiv").style.display = "block";
	document.getElementById("serviceBookTypeDiv").style.display = "block";
	document.getElementById("serviceTypeDiv").style.display = "none";
	document.getElementById("fromDateDiv").style.display = "block";
	document.getElementById("toDateDiv").style.display = "block";
	document.getElementById("lastDispoTypeDiv").style.display = "none";
	document.getElementById("droppedcountDiv").style.display = "none";

	          

	    var UserIds="",i;
	    myOption = document.getElementById('ddlCreIds');
	    
	for (i=0;i<myOption.options.length;i++)"""),format.raw/*520.41*/("""{"""),format.raw/*520.42*/("""
	    """),format.raw/*521.6*/("""if(myOption.options[i].selected)"""),format.raw/*521.38*/("""{"""),format.raw/*521.39*/("""
	        """),format.raw/*522.10*/("""if(myOption.options[i].value != "--Select--")"""),format.raw/*522.55*/("""{"""),format.raw/*522.56*/("""
	    	 """),format.raw/*523.8*/("""UserIds = UserIds + myOption.options[i].value + ",";
	        """),format.raw/*524.10*/("""}"""),format.raw/*524.11*/("""
	    """),format.raw/*525.6*/("""}"""),format.raw/*525.7*/("""
	"""),format.raw/*526.2*/("""}"""),format.raw/*526.3*/("""
	"""),format.raw/*527.2*/("""if(UserIds.length > 0)"""),format.raw/*527.24*/("""{"""),format.raw/*527.25*/("""
	    	"""),format.raw/*528.7*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
	"""),format.raw/*529.2*/("""}"""),format.raw/*529.3*/("""
	"""),format.raw/*530.2*/("""else"""),format.raw/*530.6*/("""{"""),format.raw/*530.7*/("""
	"""),format.raw/*531.2*/("""UserIds="Select";
	"""),format.raw/*532.2*/("""}"""),format.raw/*532.3*/("""
	    """),format.raw/*533.6*/("""var ajaxUrl = "/serviceBookedServerDataTableMR/"+UserIds+"";
	    
	    
	    var table= $('#serviceBookedServerDataMR').dataTable( """),format.raw/*536.60*/("""{"""),format.raw/*536.61*/("""
	        """),format.raw/*537.10*/(""""bDestroy": true,
	        "processing": true,
	        "serverSide": true,
	        "scrollY": 300,
			 "scrollX": true,
	        "paging": true,
	        "searching":true,
	        "ordering":false,	
	        "ajax": ajaxUrl,
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*546.60*/("""{"""),format.raw/*546.61*/("""
              """),format.raw/*547.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*549.15*/("""}"""),format.raw/*549.16*/("""
	    """),format.raw/*550.6*/("""}"""),format.raw/*550.7*/(""" """),format.raw/*550.8*/(""");

	    FilterOptionDataMR(table);
	    
	   	    
	    """),format.raw/*555.6*/("""}"""),format.raw/*555.7*/("""

"""),format.raw/*557.1*/("""function ajaxCallForNonContactsServerMR()"""),format.raw/*557.42*/("""{"""),format.raw/*557.43*/("""
	"""),format.raw/*558.2*/("""//alert("noncontact");
	
		document.getElementById("cresDiv").style.display = "block";
		document.getElementById("workshopDiv").style.display = "block";
		document.getElementById("cityDiv").style.display = "block";	
		document.getElementById("campaignDiv").style.display = "block";
		document.getElementById("serviceBookTypeDiv").style.display = "none";
		document.getElementById("serviceTypeDiv").style.display = "none";
		document.getElementById("fromDateDiv").style.display = "block";
		document.getElementById("toDateDiv").style.display = "block";
		document.getElementById("lastDispoTypeDiv").style.display = "block";
		document.getElementById("droppedcountDiv").style.display = "block";

	var UserIds="",i;
    myOption = document.getElementById('ddlCreIds');
    
for (i=0;i<myOption.options.length;i++)"""),format.raw/*574.40*/("""{"""),format.raw/*574.41*/("""
    """),format.raw/*575.5*/("""if(myOption.options[i].selected)"""),format.raw/*575.37*/("""{"""),format.raw/*575.38*/("""
        """),format.raw/*576.9*/("""if(myOption.options[i].value != "Select")"""),format.raw/*576.50*/("""{"""),format.raw/*576.51*/("""
    		"""),format.raw/*577.7*/("""UserIds = UserIds + myOption.options[i].value + ",";
        """),format.raw/*578.9*/("""}"""),format.raw/*578.10*/("""
    """),format.raw/*579.5*/("""}"""),format.raw/*579.6*/("""
"""),format.raw/*580.1*/("""}"""),format.raw/*580.2*/("""
	"""),format.raw/*581.2*/("""if(UserIds.length > 0)"""),format.raw/*581.24*/("""{"""),format.raw/*581.25*/("""
    	"""),format.raw/*582.6*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
	"""),format.raw/*583.2*/("""}"""),format.raw/*583.3*/("""
	"""),format.raw/*584.2*/("""else"""),format.raw/*584.6*/("""{"""),format.raw/*584.7*/("""
		"""),format.raw/*585.3*/("""UserIds="Select";
	"""),format.raw/*586.2*/("""}"""),format.raw/*586.3*/("""
    """),format.raw/*587.5*/("""var ajaxUrl = "/nonContactsServerDataTableMR/"+UserIds
     
    var table = $('#nonContactsServerDataMR').dataTable( """),format.raw/*589.58*/("""{"""),format.raw/*589.59*/("""
        """),format.raw/*590.9*/(""""bDestroy": true,
        "processing": true,
        "serverSide": true,
        "scrollY": 300,
		 "scrollX": true,
        "paging": true,
        "searching":true,
        "ordering":false,			
        "ajax": ajaxUrl,
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*599.59*/("""{"""),format.raw/*599.60*/("""
              """),format.raw/*600.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*602.15*/("""}"""),format.raw/*602.16*/("""
    """),format.raw/*603.5*/("""}"""),format.raw/*603.6*/(""" """),format.raw/*603.7*/(""");

    FilterOptionDataMR(table);
    
    """),format.raw/*607.5*/("""}"""),format.raw/*607.6*/("""





"""),format.raw/*613.1*/("""function ajaxCallForServiceNotRequiredServerMR()"""),format.raw/*613.49*/("""{"""),format.raw/*613.50*/("""
	"""),format.raw/*614.2*/("""//alert("servicenot required");
	
	document.getElementById("cresDiv").style.display = "block";
	document.getElementById("workshopDiv").style.display = "block";
	document.getElementById("cityDiv").style.display = "block";	
	document.getElementById("campaignDiv").style.display = "block";
	document.getElementById("serviceBookTypeDiv").style.display = "none";
	document.getElementById("serviceTypeDiv").style.display = "none";
	document.getElementById("fromDateDiv").style.display = "none";
	document.getElementById("toDateDiv").style.display = "none";
	document.getElementById("lastDispoTypeDiv").style.display = "none";
	document.getElementById("droppedcountDiv").style.display = "none";

	var UserIds="",i;
    myOption = document.getElementById('ddlCreIds');
    
for (i=0;i<myOption.options.length;i++)"""),format.raw/*630.40*/("""{"""),format.raw/*630.41*/("""
    """),format.raw/*631.5*/("""if(myOption.options[i].selected)"""),format.raw/*631.37*/("""{"""),format.raw/*631.38*/("""
        """),format.raw/*632.9*/("""if(myOption.options[i].value != "Select")"""),format.raw/*632.50*/("""{"""),format.raw/*632.51*/("""
    		"""),format.raw/*633.7*/("""UserIds = UserIds + myOption.options[i].value + ",";
        """),format.raw/*634.9*/("""}"""),format.raw/*634.10*/("""
    """),format.raw/*635.5*/("""}"""),format.raw/*635.6*/("""
"""),format.raw/*636.1*/("""}"""),format.raw/*636.2*/("""
	"""),format.raw/*637.2*/("""if(UserIds.length > 0)"""),format.raw/*637.24*/("""{"""),format.raw/*637.25*/("""
    	"""),format.raw/*638.6*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
	"""),format.raw/*639.2*/("""}"""),format.raw/*639.3*/("""
	"""),format.raw/*640.2*/("""else"""),format.raw/*640.6*/("""{"""),format.raw/*640.7*/("""
		"""),format.raw/*641.3*/("""UserIds="Select";
	"""),format.raw/*642.2*/("""}"""),format.raw/*642.3*/("""
    """),format.raw/*643.5*/("""var ajaxUrl = "/serviceNotRequiredServerDataTableMR/"+UserIds
     
    var table= $('#serviceNotRequiredServerDataMR').dataTable( """),format.raw/*645.64*/("""{"""),format.raw/*645.65*/("""
        """),format.raw/*646.9*/(""""bDestroy": true,
        "processing": true,
        "serverSide": true,
        "scrollY": 300,
        "paging": true,
        "searching":true,
        "ordering":false,			
        "ajax": ajaxUrl,
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*654.59*/("""{"""),format.raw/*654.60*/("""
              """),format.raw/*655.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*657.15*/("""}"""),format.raw/*657.16*/("""
    """),format.raw/*658.5*/("""}"""),format.raw/*658.6*/(""" """),format.raw/*658.7*/(""");

    FilterOptionDataMR(table);
    
    """),format.raw/*662.5*/("""}"""),format.raw/*662.6*/("""

"""),format.raw/*664.1*/("""function ajaxCallFordroppedCallsServerDataMR()"""),format.raw/*664.47*/("""{"""),format.raw/*664.48*/("""
	"""),format.raw/*665.2*/("""document.getElementById("cresDiv").style.display = "block";
	document.getElementById("workshopDiv").style.display = "block";
	document.getElementById("cityDiv").style.display = "block";	
	document.getElementById("campaignDiv").style.display = "none";
	document.getElementById("serviceBookTypeDiv").style.display = "none";
	document.getElementById("serviceTypeDiv").style.display = "none";
	document.getElementById("fromDateDiv").style.display = "none";
	document.getElementById("toDateDiv").style.display = "none";
	document.getElementById("lastDispoTypeDiv").style.display = "none";
	document.getElementById("droppedcountDiv").style.display = "none";


	    var UserIds="",i;
	    myOption = document.getElementById('ddlCreIds');
	    
	for (i=0;i<myOption.options.length;i++)"""),format.raw/*680.41*/("""{"""),format.raw/*680.42*/("""
	    """),format.raw/*681.6*/("""if(myOption.options[i].selected)"""),format.raw/*681.38*/("""{"""),format.raw/*681.39*/("""
	        """),format.raw/*682.10*/("""if(myOption.options[i].value != "--Select--")"""),format.raw/*682.55*/("""{"""),format.raw/*682.56*/("""
	    	 """),format.raw/*683.8*/("""UserIds = UserIds + myOption.options[i].value + ",";
	        """),format.raw/*684.10*/("""}"""),format.raw/*684.11*/("""
	    """),format.raw/*685.6*/("""}"""),format.raw/*685.7*/("""
	"""),format.raw/*686.2*/("""}"""),format.raw/*686.3*/("""
	"""),format.raw/*687.2*/("""if(UserIds.length > 0)"""),format.raw/*687.24*/("""{"""),format.raw/*687.25*/("""
	    	"""),format.raw/*688.7*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
	"""),format.raw/*689.2*/("""}"""),format.raw/*689.3*/("""
	"""),format.raw/*690.2*/("""else"""),format.raw/*690.6*/("""{"""),format.raw/*690.7*/("""
	"""),format.raw/*691.2*/("""UserIds="Select";
	"""),format.raw/*692.2*/("""}"""),format.raw/*692.3*/("""
	    """),format.raw/*693.6*/("""var ajaxUrl = "/droppedCallsServerDataTableMR/"+UserIds+"";
	    
	    
	    var table= $('#droppedCallsServerDataMR').dataTable( """),format.raw/*696.59*/("""{"""),format.raw/*696.60*/("""
	        """),format.raw/*697.10*/(""""bDestroy": true,
	        "processing": true,
	        "serverSide": true,
	        "scrollY": 300,
			 "scrollX": true,
	        "paging": true,
	        "searching":true,
	        "ordering":false,	
	        "ajax": ajaxUrl,
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*706.60*/("""{"""),format.raw/*706.61*/("""
              """),format.raw/*707.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*709.15*/("""}"""),format.raw/*709.16*/("""
	    """),format.raw/*710.6*/("""}"""),format.raw/*710.7*/(""" """),format.raw/*710.8*/(""");

	    FilterOptionDataMR(table);
	   
	    
	"""),format.raw/*715.2*/("""}"""),format.raw/*715.3*/("""
"""),format.raw/*716.1*/("""function ajaxCallMissedCallsServerDataMR()"""),format.raw/*716.43*/("""{"""),format.raw/*716.44*/("""
	"""),format.raw/*717.2*/("""document.getElementById("cresDiv").style.display = "block";
	document.getElementById("workshopDiv").style.display = "block";
	document.getElementById("cityDiv").style.display = "block";	
	document.getElementById("campaignDiv").style.display = "none";
	document.getElementById("serviceBookTypeDiv").style.display = "none";
	document.getElementById("serviceTypeDiv").style.display = "none";
	document.getElementById("fromDateDiv").style.display = "block";
	document.getElementById("toDateDiv").style.display = "block";
	document.getElementById("lastDispoTypeDiv").style.display = "none";
	document.getElementById("droppedcountDiv").style.display = "none";


	    var UserIds="",i;
	    myOption = document.getElementById('ddlCreIds');
	    
	for (i=0;i<myOption.options.length;i++)"""),format.raw/*732.41*/("""{"""),format.raw/*732.42*/("""
	    """),format.raw/*733.6*/("""if(myOption.options[i].selected)"""),format.raw/*733.38*/("""{"""),format.raw/*733.39*/("""
	        """),format.raw/*734.10*/("""if(myOption.options[i].value != "--Select--")"""),format.raw/*734.55*/("""{"""),format.raw/*734.56*/("""
	    	 """),format.raw/*735.8*/("""UserIds = UserIds + myOption.options[i].value + ",";
	        """),format.raw/*736.10*/("""}"""),format.raw/*736.11*/("""
	    """),format.raw/*737.6*/("""}"""),format.raw/*737.7*/("""
	"""),format.raw/*738.2*/("""}"""),format.raw/*738.3*/("""
	"""),format.raw/*739.2*/("""if(UserIds.length > 0)"""),format.raw/*739.24*/("""{"""),format.raw/*739.25*/("""
	    	"""),format.raw/*740.7*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
	"""),format.raw/*741.2*/("""}"""),format.raw/*741.3*/("""
	"""),format.raw/*742.2*/("""else"""),format.raw/*742.6*/("""{"""),format.raw/*742.7*/("""
	"""),format.raw/*743.2*/("""UserIds="Select";
	"""),format.raw/*744.2*/("""}"""),format.raw/*744.3*/("""
	    """),format.raw/*745.6*/("""var ajaxUrl = "/missedCallsServerDataTableMR/"+UserIds+"";
	    
	    
	    var table= $('#missedCallsServerDataMR').dataTable( """),format.raw/*748.58*/("""{"""),format.raw/*748.59*/("""
	        """),format.raw/*749.10*/(""""bDestroy": true,
	        "processing": true,
	        "serverSide": true,
	        "scrollY": 300,
			 "scrollX": true,
	        "paging": true,
	        "searching":true,
	        "ordering":false,	
	        "ajax": ajaxUrl,
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*758.60*/("""{"""),format.raw/*758.61*/("""
              """),format.raw/*759.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*761.15*/("""}"""),format.raw/*761.16*/("""
	    """),format.raw/*762.6*/("""}"""),format.raw/*762.7*/(""" """),format.raw/*762.8*/(""");

	    FilterOptionDataMR(table);
	   
	    
	"""),format.raw/*767.2*/("""}"""),format.raw/*767.3*/("""

"""),format.raw/*769.1*/("""function ajaxCallForincomingCallsServerDataMR()"""),format.raw/*769.48*/("""{"""),format.raw/*769.49*/("""
	"""),format.raw/*770.2*/("""document.getElementById("cresDiv").style.display = "block";
	document.getElementById("workshopDiv").style.display = "block";
	document.getElementById("cityDiv").style.display = "block";	
	document.getElementById("campaignDiv").style.display = "none";
	document.getElementById("serviceBookTypeDiv").style.display = "none";
	document.getElementById("serviceTypeDiv").style.display = "none";
	document.getElementById("fromDateDiv").style.display = "block";
	document.getElementById("toDateDiv").style.display = "block";
	document.getElementById("lastDispoTypeDiv").style.display = "none";
	document.getElementById("droppedcountDiv").style.display = "none";


	    var UserIds="",i;
	    myOption = document.getElementById('ddlCreIds');
	    
	for (i=0;i<myOption.options.length;i++)"""),format.raw/*785.41*/("""{"""),format.raw/*785.42*/("""
	    """),format.raw/*786.6*/("""if(myOption.options[i].selected)"""),format.raw/*786.38*/("""{"""),format.raw/*786.39*/("""
	        """),format.raw/*787.10*/("""if(myOption.options[i].value != "--Select--")"""),format.raw/*787.55*/("""{"""),format.raw/*787.56*/("""
	    	 """),format.raw/*788.8*/("""UserIds = UserIds + myOption.options[i].value + ",";
	        """),format.raw/*789.10*/("""}"""),format.raw/*789.11*/("""
	    """),format.raw/*790.6*/("""}"""),format.raw/*790.7*/("""
	"""),format.raw/*791.2*/("""}"""),format.raw/*791.3*/("""
	"""),format.raw/*792.2*/("""if(UserIds.length > 0)"""),format.raw/*792.24*/("""{"""),format.raw/*792.25*/("""
	    	"""),format.raw/*793.7*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
	"""),format.raw/*794.2*/("""}"""),format.raw/*794.3*/("""
	"""),format.raw/*795.2*/("""else"""),format.raw/*795.6*/("""{"""),format.raw/*795.7*/("""
	"""),format.raw/*796.2*/("""UserIds="Select";
	"""),format.raw/*797.2*/("""}"""),format.raw/*797.3*/("""
	    """),format.raw/*798.6*/("""var ajaxUrl = "/incomingCallsServerDataTableMR/"+UserIds+"";
	    
	    
	    var table= $('#incomingCallsServerDataMR').dataTable( """),format.raw/*801.60*/("""{"""),format.raw/*801.61*/("""
	        """),format.raw/*802.10*/(""""bDestroy": true,
	        "processing": true,
	        "serverSide": true,
	        "scrollY": 300,
			 "scrollX": true,
	        "paging": true,
	        "searching":true,
	        "ordering":false,	
	        "ajax": ajaxUrl,
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*811.60*/("""{"""),format.raw/*811.61*/("""
              """),format.raw/*812.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*814.15*/("""}"""),format.raw/*814.16*/("""
	    """),format.raw/*815.6*/("""}"""),format.raw/*815.7*/(""" """),format.raw/*815.8*/(""");

	    FilterOptionDataMR(table);
	   
	    
	"""),format.raw/*820.2*/("""}"""),format.raw/*820.3*/("""

"""),format.raw/*822.1*/("""function ajaxCallForOutgoingCallBucCallsServerDataMR()"""),format.raw/*822.55*/("""{"""),format.raw/*822.56*/("""
	"""),format.raw/*823.2*/("""document.getElementById("cresDiv").style.display = "block";
	document.getElementById("workshopDiv").style.display = "block";
	document.getElementById("cityDiv").style.display = "block";	
	document.getElementById("campaignDiv").style.display = "none";
	document.getElementById("serviceBookTypeDiv").style.display = "none";
	document.getElementById("serviceTypeDiv").style.display = "none";
	document.getElementById("fromDateDiv").style.display = "block";
	document.getElementById("toDateDiv").style.display = "block";
	document.getElementById("lastDispoTypeDiv").style.display = "none";
	document.getElementById("droppedcountDiv").style.display = "none";


	    var UserIds="",i;
	    myOption = document.getElementById('ddlCreIds');
	    
	for (i=0;i<myOption.options.length;i++)"""),format.raw/*838.41*/("""{"""),format.raw/*838.42*/("""
	    """),format.raw/*839.6*/("""if(myOption.options[i].selected)"""),format.raw/*839.38*/("""{"""),format.raw/*839.39*/("""
	        """),format.raw/*840.10*/("""if(myOption.options[i].value != "--Select--")"""),format.raw/*840.55*/("""{"""),format.raw/*840.56*/("""
	    	 """),format.raw/*841.8*/("""UserIds = UserIds + myOption.options[i].value + ",";
	        """),format.raw/*842.10*/("""}"""),format.raw/*842.11*/("""
	    """),format.raw/*843.6*/("""}"""),format.raw/*843.7*/("""
	"""),format.raw/*844.2*/("""}"""),format.raw/*844.3*/("""
	"""),format.raw/*845.2*/("""if(UserIds.length > 0)"""),format.raw/*845.24*/("""{"""),format.raw/*845.25*/("""
	    	"""),format.raw/*846.7*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
	"""),format.raw/*847.2*/("""}"""),format.raw/*847.3*/("""
	"""),format.raw/*848.2*/("""else"""),format.raw/*848.6*/("""{"""),format.raw/*848.7*/("""
	"""),format.raw/*849.2*/("""UserIds="Select";
	"""),format.raw/*850.2*/("""}"""),format.raw/*850.3*/("""
	    """),format.raw/*851.6*/("""var ajaxUrl = "/outgoingCallsServerDataTableMR/"+UserIds+"";
	    
	    
	    var table= $('#outgoingCallsServerDataMR').dataTable( """),format.raw/*854.60*/("""{"""),format.raw/*854.61*/("""
	        """),format.raw/*855.10*/(""""bDestroy": true,
	        "processing": true,
	        "serverSide": true,
	        "scrollY": 300,
			 "scrollX": true,
	        "paging": true,
	        "searching":true,
	        "ordering":false,	
	        "ajax": ajaxUrl,
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*864.60*/("""{"""),format.raw/*864.61*/("""
              """),format.raw/*865.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*867.15*/("""}"""),format.raw/*867.16*/("""
	    """),format.raw/*868.6*/("""}"""),format.raw/*868.7*/(""" """),format.raw/*868.8*/(""");

	    FilterOptionDataMR(table);
	   
"""),format.raw/*872.1*/("""}"""),format.raw/*872.2*/("""
"""),format.raw/*873.1*/("""</script>
<script type="text/javascript">  
function FilterOptionDataMR(table)"""),format.raw/*875.35*/("""{"""),format.raw/*875.36*/("""
	"""),format.raw/*876.2*/("""var values = [];
	$('.filter').on('change', function() """),format.raw/*877.39*/("""{"""),format.raw/*877.40*/("""
    	"""),format.raw/*878.6*/("""console.log("inside filteroptionsdata data");
	    var i= $(this).data('columnIndex');
	    var v = $(this).val();
	    console.log("i : "+i);
		console.log("v length "+v.length);
	    
	    if(v.length>0)"""),format.raw/*884.20*/("""{"""),format.raw/*884.21*/("""
		    
		    """),format.raw/*886.7*/("""if(i == 7)"""),format.raw/*886.17*/("""{"""),format.raw/*886.18*/("""	

		    	"""),format.raw/*888.8*/("""var UserIds="",j;
		        myOption = document.getElementById('ddlCreIds');
		        console.log("here"+myOption);		        
		    for (j=0;j<myOption.options.length;j++)"""),format.raw/*891.46*/("""{"""),format.raw/*891.47*/("""
		        """),format.raw/*892.11*/("""if(myOption.options[j].selected)"""),format.raw/*892.43*/("""{"""),format.raw/*892.44*/("""
		            """),format.raw/*893.15*/("""if(myOption.options[j].value != "Select")"""),format.raw/*893.56*/("""{"""),format.raw/*893.57*/("""
		        		"""),format.raw/*894.13*/("""UserIds = UserIds + myOption.options[j].value + ",";
		            """),format.raw/*895.15*/("""}"""),format.raw/*895.16*/("""
		        """),format.raw/*896.11*/("""}"""),format.raw/*896.12*/("""
		    """),format.raw/*897.7*/("""}"""),format.raw/*897.8*/("""
		    	"""),format.raw/*898.8*/("""if(UserIds.length > 0)"""),format.raw/*898.30*/("""{"""),format.raw/*898.31*/("""
		        	"""),format.raw/*899.12*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
		        	values[7]=UserIds;
		    	"""),format.raw/*901.8*/("""}"""),format.raw/*901.9*/("""else"""),format.raw/*901.13*/("""{"""),format.raw/*901.14*/("""
		    		"""),format.raw/*902.9*/("""UserIds="Select";
		    		values[7]=UserIds;
		    	"""),format.raw/*904.8*/("""}"""),format.raw/*904.9*/("""
		    
		    """),format.raw/*906.7*/("""}"""),format.raw/*906.8*/("""else"""),format.raw/*906.12*/("""{"""),format.raw/*906.13*/("""
		    	"""),format.raw/*907.8*/("""values[i] = v;
			    """),format.raw/*908.8*/("""}"""),format.raw/*908.9*/("""
	
			       
		  
		    """),format.raw/*912.7*/("""}"""),format.raw/*912.8*/("""		    
	    
	    	"""),format.raw/*914.7*/("""console.log(JSON.stringify(values));	   	
	  	  //clear global search values
	 	  //table.api().search('');
	 	  $(this).data('columnIndex');
	 	 //var table = $('#nonContactsServerDataMR').dataTable();
	 	 if(values.length>0) """),format.raw/*919.25*/("""{"""),format.raw/*919.26*/("""
			 """),format.raw/*920.5*/("""for(var i=0;i<values.length;i++)"""),format.raw/*920.37*/("""{"""),format.raw/*920.38*/("""
				 """),format.raw/*921.6*/("""console.log("values[i] : "+values[i]);	
			    table.api().columns(i).search(values[i]);
			 """),format.raw/*923.5*/("""}"""),format.raw/*923.6*/("""
			 """),format.raw/*924.5*/("""table.api().draw(); 
		"""),format.raw/*925.3*/("""}"""),format.raw/*925.4*/("""
	 	
		 	  
	"""),format.raw/*928.2*/("""}"""),format.raw/*928.3*/(""");
	
	
"""),format.raw/*931.1*/("""}"""),format.raw/*931.2*/("""
"""),format.raw/*932.1*/("""</script>

 <script type="text/javascript">
	window.onload = assignedInteractionDataMR();
</script>
    """))
      }
    }
  }

  def render(listCity:List[Location],dealerName:String,user:String,allCREHash:Map[Long, String],campaignList:List[Campaign],serviceTypeList:List[ServiceTypes],bookedServiceTypeList:List[String],dispoList:List[CallDispositionData]): play.twirl.api.HtmlFormat.Appendable = apply(listCity,dealerName,user,allCREHash,campaignList,serviceTypeList,bookedServiceTypeList,dispoList)

  def f:((List[Location],String,String,Map[Long, String],List[Campaign],List[ServiceTypes],List[String],List[CallDispositionData]) => play.twirl.api.HtmlFormat.Appendable) = (listCity,dealerName,user,allCREHash,campaignList,serviceTypeList,bookedServiceTypeList,dispoList) => apply(listCity,dealerName,user,allCREHash,campaignList,serviceTypeList,bookedServiceTypeList,dispoList)

  def ref: this.type = this

}


}

/**/
object callDispositionPageCREManager extends callDispositionPageCREManager_Scope0.callDispositionPageCREManager
              /*
                  -- GENERATED --
                  DATE: Thu Dec 28 15:26:36 IST 2017
                  SOURCE: D:/CRMNEW/28th Dec/crm/app/views/callDispositionPageCREManager.scala.html
                  HASH: 6bdb76e8de9538877b90e0b98d2764309c6d0c3b
                  MATRIX: 906->1|1221->220|1249->223|1307->273|1346->275|1381->284|1428->304|1456->305|1510->333|1633->429|1661->430|1692->434|3852->2568|3889->2589|3928->2590|4022->2656|4065->2672|4078->2676|4109->2686|4139->2689|4152->2693|4183->2703|4246->2735|4292->2753|5403->3838|5451->3870|5490->3871|5554->3907|5597->3923|5619->3936|5658->3954|5688->3957|5710->3970|5750->3988|5824->4031|5895->4075|6821->4974|6876->5012|6916->5013|6982->5050|7026->5066|7052->5082|7095->5103|7126->5106|7153->5122|7197->5143|7273->5187|7346->5232|7702->5561|7769->5611|7809->5612|7840->5615|7884->5631|7928->5653|7959->5656|8003->5678|8082->5725|8157->5772|8463->6050|8503->6073|8543->6074|8576->6079|8620->6095|8635->6100|8674->6117|8705->6120|8720->6125|8759->6142|8809->6161|8893->6217|23872->21165|23905->21170|24006->21242|24036->21243|24070->21249|24938->22088|24968->22089|25002->22095|25063->22127|25093->22128|25131->22138|25205->22183|25235->22184|25271->22192|25361->22254|25391->22255|25425->22261|25454->22262|25484->22264|25513->22265|25543->22267|25594->22289|25624->22290|25659->22297|25740->22350|25769->22351|25799->22353|25831->22357|25860->22358|25890->22360|25937->22379|25966->22380|26000->22386|26162->22519|26192->22520|26230->22530|26546->22817|26576->22818|26621->22834|26728->22912|26758->22913|26792->22919|26821->22920|26850->22921|26983->23025|27013->23026|27049->23034|27153->23110|27182->23111|27224->23125|27253->23126|27291->23136|27366->23182|27396->23183|27427->23186|28263->23993|28293->23994|28328->24001|28389->24033|28419->24034|28459->24045|28533->24090|28563->24091|28600->24100|28692->24163|28722->24164|28757->24171|28786->24172|28817->24175|28846->24176|28877->24179|28928->24201|28958->24202|28994->24210|29076->24264|29105->24265|29136->24268|29168->24272|29197->24273|29228->24276|29276->24296|29305->24297|29340->24304|29503->24438|29533->24439|29573->24450|29898->24746|29928->24747|29973->24763|30080->24841|30110->24842|30145->24849|30174->24850|30203->24851|30278->24898|30307->24899|30341->24905|30417->24952|30447->24953|30478->24956|31316->25765|31346->25766|31381->25773|31442->25805|31472->25806|31512->25817|31586->25862|31616->25863|31653->25872|31745->25935|31775->25936|31810->25943|31839->25944|31870->25947|31899->25948|31930->25951|31981->25973|32011->25974|32047->25982|32129->26036|32158->26037|32189->26040|32221->26044|32250->26045|32281->26048|32329->26068|32358->26069|32393->26076|32557->26211|32587->26212|32627->26223|32952->26519|32982->26520|33027->26536|33134->26614|33164->26615|33199->26622|33228->26623|33257->26624|33347->26686|33376->26687|33408->26691|33478->26732|33508->26733|33539->26736|34394->27562|34424->27563|34458->27569|34519->27601|34549->27602|34587->27612|34657->27653|34687->27654|34723->27662|34813->27724|34843->27725|34877->27731|34906->27732|34936->27734|34965->27735|34996->27738|35047->27760|35077->27761|35112->27768|35194->27822|35223->27823|35254->27826|35286->27830|35315->27831|35347->27835|35395->27855|35424->27856|35458->27862|35607->27982|35637->27983|35675->27993|35993->28282|36023->28283|36068->28299|36175->28377|36205->28378|36239->28384|36268->28385|36297->28386|36373->28434|36402->28435|36442->28447|36519->28495|36549->28496|36580->28499|37430->29320|37460->29321|37494->29327|37555->29359|37585->29360|37623->29370|37693->29411|37723->29412|37759->29420|37849->29482|37879->29483|37913->29489|37942->29490|37972->29492|38001->29493|38032->29496|38083->29518|38113->29519|38148->29526|38230->29580|38259->29581|38290->29584|38322->29588|38351->29589|38383->29593|38431->29613|38460->29614|38494->29620|38656->29753|38686->29754|38724->29764|39021->30032|39051->30033|39096->30049|39203->30127|39233->30128|39267->30134|39296->30135|39325->30136|39401->30184|39430->30185|39462->30189|39537->30235|39567->30236|39598->30239|40419->31031|40449->31032|40484->31039|40545->31071|40575->31072|40615->31083|40689->31128|40719->31129|40756->31138|40848->31201|40878->31202|40913->31209|40942->31210|40973->31213|41002->31214|41033->31217|41084->31239|41114->31240|41150->31248|41232->31302|41261->31303|41292->31306|41324->31310|41353->31311|41384->31314|41432->31334|41461->31335|41496->31342|41658->31475|41688->31476|41728->31487|42053->31783|42083->31784|42128->31800|42235->31878|42265->31879|42300->31886|42329->31887|42358->31888|42439->31941|42468->31942|42498->31944|42569->31986|42599->31987|42630->31990|43453->32784|43483->32785|43518->32792|43579->32824|43609->32825|43649->32836|43723->32881|43753->32882|43790->32891|43882->32954|43912->32955|43947->32962|43976->32963|44007->32966|44036->32967|44067->32970|44118->32992|44148->32993|44184->33001|44266->33055|44295->33056|44326->33059|44358->33063|44387->33064|44418->33067|44466->33087|44495->33088|44530->33095|44690->33226|44720->33227|44760->33238|45085->33534|45115->33535|45160->33551|45267->33629|45297->33630|45332->33637|45361->33638|45390->33639|45471->33692|45500->33693|45532->33697|45608->33744|45638->33745|45669->33748|46492->34542|46522->34543|46557->34550|46618->34582|46648->34583|46688->34594|46762->34639|46792->34640|46829->34649|46921->34712|46951->34713|46986->34720|47015->34721|47046->34724|47075->34725|47106->34728|47157->34750|47187->34751|47223->34759|47305->34813|47334->34814|47365->34817|47397->34821|47426->34822|47457->34825|47505->34845|47534->34846|47569->34853|47733->34988|47763->34989|47803->35000|48128->35296|48158->35297|48203->35313|48310->35391|48340->35392|48375->35399|48404->35400|48433->35401|48514->35454|48543->35455|48575->35459|48658->35513|48688->35514|48719->35517|49542->36311|49572->36312|49607->36319|49668->36351|49698->36352|49738->36363|49812->36408|49842->36409|49879->36418|49971->36481|50001->36482|50036->36489|50065->36490|50096->36493|50125->36494|50156->36497|50207->36519|50237->36520|50273->36528|50355->36582|50384->36583|50415->36586|50447->36590|50476->36591|50507->36594|50555->36614|50584->36615|50619->36622|50783->36757|50813->36758|50853->36769|51178->37065|51208->37066|51253->37082|51360->37160|51390->37161|51425->37168|51454->37169|51483->37170|51556->37215|51585->37216|51615->37218|51724->37298|51754->37299|51785->37302|51870->37358|51900->37359|51935->37366|52175->37577|52205->37578|52249->37594|52288->37604|52318->37605|52358->37617|52562->37792|52592->37793|52633->37805|52694->37837|52724->37838|52769->37854|52839->37895|52869->37896|52912->37910|53009->37978|53039->37979|53080->37991|53110->37992|53146->38000|53175->38001|53212->38010|53263->38032|53293->38033|53335->38046|53454->38137|53483->38138|53516->38142|53546->38143|53584->38153|53666->38207|53695->38208|53739->38224|53768->38225|53801->38229|53831->38230|53868->38239|53919->38262|53948->38263|54005->38292|54034->38293|54083->38314|54344->38546|54374->38547|54408->38553|54469->38585|54499->38586|54534->38593|54657->38688|54686->38689|54720->38695|54772->38719|54801->38720|54845->38736|54874->38737|54912->38747|54941->38748|54971->38750
                  LINES: 27->1|32->1|33->2|33->2|33->2|37->6|38->7|38->7|40->9|43->12|43->12|45->14|76->45|76->45|76->45|77->46|77->46|77->46|77->46|77->46|77->46|77->46|78->47|79->48|109->78|109->78|109->78|110->79|110->79|110->79|110->79|110->79|110->79|110->79|111->80|113->82|133->102|133->102|133->102|134->103|134->103|134->103|134->103|134->103|134->103|134->103|135->104|137->106|144->113|144->113|144->113|145->114|145->114|145->114|145->114|145->114|146->115|148->117|154->123|154->123|154->123|155->124|155->124|155->124|155->124|155->124|155->124|155->124|157->126|160->129|417->386|419->388|421->390|421->390|422->391|437->406|437->406|438->407|438->407|438->407|439->408|439->408|439->408|440->409|441->410|441->410|442->411|442->411|443->412|443->412|444->413|444->413|444->413|445->414|446->415|446->415|447->416|447->416|447->416|448->417|449->418|449->418|450->419|453->422|453->422|454->423|463->432|463->432|464->433|466->435|466->435|467->436|467->436|467->436|471->440|471->440|472->441|474->443|474->443|476->445|476->445|481->450|481->450|481->450|482->451|497->466|497->466|498->467|498->467|498->467|499->468|499->468|499->468|500->469|501->470|501->470|502->471|502->471|503->472|503->472|504->473|504->473|504->473|505->474|506->475|506->475|507->476|507->476|507->476|508->477|509->478|509->478|510->479|513->482|513->482|514->483|523->492|523->492|524->493|526->495|526->495|527->496|527->496|527->496|531->500|531->500|534->503|534->503|534->503|535->504|551->520|551->520|552->521|552->521|552->521|553->522|553->522|553->522|554->523|555->524|555->524|556->525|556->525|557->526|557->526|558->527|558->527|558->527|559->528|560->529|560->529|561->530|561->530|561->530|562->531|563->532|563->532|564->533|567->536|567->536|568->537|577->546|577->546|578->547|580->549|580->549|581->550|581->550|581->550|586->555|586->555|588->557|588->557|588->557|589->558|605->574|605->574|606->575|606->575|606->575|607->576|607->576|607->576|608->577|609->578|609->578|610->579|610->579|611->580|611->580|612->581|612->581|612->581|613->582|614->583|614->583|615->584|615->584|615->584|616->585|617->586|617->586|618->587|620->589|620->589|621->590|630->599|630->599|631->600|633->602|633->602|634->603|634->603|634->603|638->607|638->607|644->613|644->613|644->613|645->614|661->630|661->630|662->631|662->631|662->631|663->632|663->632|663->632|664->633|665->634|665->634|666->635|666->635|667->636|667->636|668->637|668->637|668->637|669->638|670->639|670->639|671->640|671->640|671->640|672->641|673->642|673->642|674->643|676->645|676->645|677->646|685->654|685->654|686->655|688->657|688->657|689->658|689->658|689->658|693->662|693->662|695->664|695->664|695->664|696->665|711->680|711->680|712->681|712->681|712->681|713->682|713->682|713->682|714->683|715->684|715->684|716->685|716->685|717->686|717->686|718->687|718->687|718->687|719->688|720->689|720->689|721->690|721->690|721->690|722->691|723->692|723->692|724->693|727->696|727->696|728->697|737->706|737->706|738->707|740->709|740->709|741->710|741->710|741->710|746->715|746->715|747->716|747->716|747->716|748->717|763->732|763->732|764->733|764->733|764->733|765->734|765->734|765->734|766->735|767->736|767->736|768->737|768->737|769->738|769->738|770->739|770->739|770->739|771->740|772->741|772->741|773->742|773->742|773->742|774->743|775->744|775->744|776->745|779->748|779->748|780->749|789->758|789->758|790->759|792->761|792->761|793->762|793->762|793->762|798->767|798->767|800->769|800->769|800->769|801->770|816->785|816->785|817->786|817->786|817->786|818->787|818->787|818->787|819->788|820->789|820->789|821->790|821->790|822->791|822->791|823->792|823->792|823->792|824->793|825->794|825->794|826->795|826->795|826->795|827->796|828->797|828->797|829->798|832->801|832->801|833->802|842->811|842->811|843->812|845->814|845->814|846->815|846->815|846->815|851->820|851->820|853->822|853->822|853->822|854->823|869->838|869->838|870->839|870->839|870->839|871->840|871->840|871->840|872->841|873->842|873->842|874->843|874->843|875->844|875->844|876->845|876->845|876->845|877->846|878->847|878->847|879->848|879->848|879->848|880->849|881->850|881->850|882->851|885->854|885->854|886->855|895->864|895->864|896->865|898->867|898->867|899->868|899->868|899->868|903->872|903->872|904->873|906->875|906->875|907->876|908->877|908->877|909->878|915->884|915->884|917->886|917->886|917->886|919->888|922->891|922->891|923->892|923->892|923->892|924->893|924->893|924->893|925->894|926->895|926->895|927->896|927->896|928->897|928->897|929->898|929->898|929->898|930->899|932->901|932->901|932->901|932->901|933->902|935->904|935->904|937->906|937->906|937->906|937->906|938->907|939->908|939->908|943->912|943->912|945->914|950->919|950->919|951->920|951->920|951->920|952->921|954->923|954->923|955->924|956->925|956->925|959->928|959->928|962->931|962->931|963->932
                  -- GENERATED --
              */
          