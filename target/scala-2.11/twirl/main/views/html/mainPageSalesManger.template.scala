
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object mainPageSalesManger_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class mainPageSalesManger extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[String,String,String,Html,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(title: String,userName : String,dealerName:String)(content: Html):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.68*/("""
"""),format.raw/*2.1*/("""<!DOCTYPE html>

<html>
<head>
<title>"""),_display_(/*6.9*/title),format.raw/*6.14*/("""</title>
<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
<meta http-equiv="cache-control" content="max-age=0" >
<meta http-equiv="expires" content="0" >
<meta http-equiv="pragma" content="no-cache" >
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link rel="shortcut icon" type="image/png" href=""""),_display_(/*12.51*/routes/*12.57*/.Assets.at("images/favicon.ico")),format.raw/*12.89*/("""">
<script src=""""),_display_(/*13.15*/routes/*13.21*/.Assets.at("javascripts/jquery-1.7.1.min.js")),format.raw/*13.66*/("""" type="text/javascript"></script>
<link rel="stylesheet" href=""""),_display_(/*14.31*/routes/*14.37*/.Assets.at("css/bootstrap.min.css")),format.raw/*14.72*/("""">
<link rel="stylesheet" href=""""),_display_(/*15.31*/routes/*15.37*/.Assets.at("css/jquery-ui.min.css")),format.raw/*15.72*/("""">
<link rel="stylesheet" href=""""),_display_(/*16.31*/routes/*16.37*/.Assets.at("css/skin-red.css")),format.raw/*16.67*/("""">
<link rel="stylesheet" href=""""),_display_(/*17.31*/routes/*17.37*/.Assets.at("css/AdminLTE.min.css")),format.raw/*17.71*/("""">
<link rel="stylesheet" href=""""),_display_(/*18.31*/routes/*18.37*/.Assets.at("css/dataTables.bootstrap.css")),format.raw/*18.79*/("""">
<link rel="stylesheet" href=""""),_display_(/*19.31*/routes/*19.37*/.Assets.at("css/dataTables.responsive.css")),format.raw/*19.80*/("""">
<link rel="stylesheet" href=""""),_display_(/*20.31*/routes/*20.37*/.Assets.at("css/font-awesome.min.css")),format.raw/*20.75*/("""">
<link rel="stylesheet" href=""""),_display_(/*21.31*/routes/*21.37*/.Assets.at("css/ionicons.min.css")),format.raw/*21.71*/("""">
<link rel="stylesheet" href=""""),_display_(/*22.31*/routes/*22.37*/.Assets.at("css/Wyz_css.css")),format.raw/*22.66*/("""">
<link rel="stylesheet" href=""""),_display_(/*23.31*/routes/*23.37*/.Assets.at("css/bootstrap-select.min.css")),format.raw/*23.79*/("""">
<link href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/themes/blitzer/jquery-ui.css" rel="stylesheet" type="text/css" />
</head>

<body class="hold-transition skin-red sidebar-mini">
<div class="wrapper" id="SalesManagerPage"> 
  
  <!-- Main Header -->
  <header class="main-header"> 
    
    <!-- Logo --> 
    <a href="/SalesManager" class="logo"> <span class="logo-mini"><b>A</b>DCC</span> <span> <img src=""""),_display_(/*34.104*/routes/*34.110*/.Assets.at("images/dealercustomerconnectlogoblack2.png")),format.raw/*34.166*/("""" width="200" height="44" style="margin-left: -20px;"/></span> </a> 
    
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation"> 
      <!-- Sidebar toggle button--> 
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button"> <span class="sr-only">Toggle navigation</span> </a>
     
      <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
      
         <li><a href="#"><i class="fa fa-user fa-fw"></i>"""),_display_(/*44.59*/dealerName),format.raw/*44.69*/("""</a></li>      		
     	 <li><a href="#"><i class="fa fa-user fa-fw"></i>"""),_display_(/*45.57*/userName),format.raw/*45.65*/("""</a></li> 
     	 <li><a href="/SalesManager/changepassword"><i class="fa fa-sign-out fa-fw"></i>Change Password</a></li>                   
                    
                    <li><a href="/logoutUser" onclick="ClearHistory()"><i class="fa fa-sign-out fa-fw"></i>LogOut</a></li>              
            
             
            </ul>
        
      </div>
    </nav>
  </header>
  <aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu">
        <li class="treeview"> <a href="#"><i class="fa fa-sitemap"></i> <span>Sales Manager<span class="fa arrow"></span></span> <i class="fa fa-angle-left pull-right"></i></a>
          <ul class="treeview-menu">
              
            <li><a href="/SalesManager/viewSalesCallLog"><i class="fa fa-phone-square"></i>View Sales Call Logs</a></li>           
             <li><a href="/SalesManager/viewassignedCall"><i class="fa fa-phone"></i>Sales Assigned Calls</a> </li>
             <li><a href="/SalesManager/uploadCallsBySalesManager"><i class="fa fa-upload"></i>Upload ScheduledCalls SEWise</a> </li>
             <li><a href="/SalesManager/gettransferingCallsofSalesManager"><i class="fa fa-random"></i>Change Sales Assignment</a> </li>
             <li><a href="/SalesManager/directUploadSalesSchCalls"><i class="fa fa-upload"></i>Upload ScheduledCalls ForAll</a> </li>
              
          </ul>
        </li>
        
        
        
        
         <li class="treeview"> <a href="#"><i class="fa fa-file-text"></i> <span>Reports<span class="fa arrow"></span></span> <i class="fa fa-angle-left pull-right"></i></a>
          <ul class="treeview-menu">
              
               <li><a href="/SalesManager/viewReport"><i class="fa fa-bar-chart-o"></i> Chart and Pie Reports</a> </li>
               <li><a href="/SalesManager/getSalesDownloadPage"><i class="fa fa-download"></i> Download Reports</a> </li>
             
            </ul>
        </li>
        
        
        
        
        
        
        
        
      </ul>            <!-- /.sidebar-menu --> 
    </section>    <!-- /.sidebar --> 
  </aside>
  <div class="content-wrapper">
    <section class="content">
      <div class="row">
        <div class="col-sm-3"> 
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><div id="salesSchCountSalesMan">..</div></h3>
              <p>Sales Scheduled Calls</p>
            </div>
            <div class="icon"> <i class="ion-android-call"></i> </div>
             
            <!--<span class="pull-left" data-toggle="modal" data-target=".myModal1">View Details</span>--> 
          </div>
        </div> <!-- ./col3 -->
        <div class="col-sm-3"> 
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><div id="salesPerConvTDSalesMan">..</div></h3>
              <p>% Conversion Rate</p>
            </div>
            <div class="icon"> <i class="ion-android-checkbox-outline"></i> </div>
            </div>
        </div> <!-- ./col -->
        <div class="col-sm-3"> 
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>44</h3>
              <p>VAS</p>
            </div>
            <div class="icon"> <i class="ion-person-add"></i> </div>
             </div>
        </div> <!-- ./col -->
        <div class="col-sm-3"> 
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
               <h3><div id="salesSchCountPendingSalesMan">..</div></h3>
              <p>Pending calls</p>
            </div>
            <div class="icon"> <i class="ion-android-download"></i> </div>
             </div>
        </div> <!-- ./col --> 
      </div>  <!-- /.row --> 
      
      """),_display_(/*140.8*/content),format.raw/*140.15*/("""
      """),format.raw/*141.7*/("""</section>
    </div>
    <footer class="main-footer">
      <div class="pull-right hidden-xs"> ADCC </div>
      <strong>WyzMindz &copy; 2016 <a href="http://www.wyzmindz.com/">Company</a>.</strong> All rights reserved.
    </footer>
</div>
<script src=""""),_display_(/*148.15*/routes/*148.21*/.Assets.at("javascripts/jquery.js")),format.raw/*148.56*/("""" type="text/javascript"></script> 
<script src=""""),_display_(/*149.15*/routes/*149.21*/.Assets.at("javascripts/jquery-ui.min.js")),format.raw/*149.63*/("""" type="text/javascript"></script> 
<script src=""""),_display_(/*150.15*/routes/*150.21*/.Assets.at("javascripts/bootstrap.min.js")),format.raw/*150.63*/("""" type="text/javascript"></script> 
<script src=""""),_display_(/*151.15*/routes/*151.21*/.Assets.at("javascripts/app.js")),format.raw/*151.53*/("""" type="text/javascript"></script> 
<script src=""""),_display_(/*152.15*/routes/*152.21*/.Assets.at("javascripts/jquery.dataTables.min.js")),format.raw/*152.71*/("""" type="text/javascript"></script> 
<script src=""""),_display_(/*153.15*/routes/*153.21*/.Assets.at("javascripts/dataTables.bootstrap.min.js")),format.raw/*153.74*/("""" type="text/javascript"></script> 
<script src=""""),_display_(/*154.15*/routes/*154.21*/.Assets.at("javascripts/Chart.min.js")),format.raw/*154.59*/("""" type="text/javascript"></script> 
<script src=""""),_display_(/*155.15*/routes/*155.21*/.Assets.at("javascripts/fastclick.min.js")),format.raw/*155.63*/("""" type="text/javascript"></script> 
<script src=""""),_display_(/*156.15*/routes/*156.21*/.Assets.at("javascripts/Wyz_Scripts.js")),format.raw/*156.61*/("""" type="text/javascript"></script>
<script src=""""),_display_(/*157.15*/routes/*157.21*/.Assets.at("javascripts/jquery.blockUI.js")),format.raw/*157.64*/("""" type="text/javascript"></script> 
<script src=""""),_display_(/*158.15*/routes/*158.21*/.Assets.at("javascripts/bootstrap-select.min.js")),format.raw/*158.70*/("""" type="text/javascript"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDh_ehAgNa_D8qb-DcBg-mSYytYZ0KXDEg&callback=initMap"
  type="text/javascript"></script>

</body>
</html>
"""))
      }
    }
  }

  def render(title:String,userName:String,dealerName:String,content:Html): play.twirl.api.HtmlFormat.Appendable = apply(title,userName,dealerName)(content)

  def f:((String,String,String) => (Html) => play.twirl.api.HtmlFormat.Appendable) = (title,userName,dealerName) => (content) => apply(title,userName,dealerName)(content)

  def ref: this.type = this

}


}

/**/
object mainPageSalesManger extends mainPageSalesManger_Scope0.mainPageSalesManger
              /*
                  -- GENERATED --
                  DATE: Thu Dec 28 12:24:29 IST 2017
                  SOURCE: D:/CRMNEW/28th Dec/crm/app/views/mainPageSalesManger.scala.html
                  HASH: 2f643a65481e414d10d52532cebb73c1553c6507
                  MATRIX: 792->1|953->67|981->69|1049->112|1074->117|1466->482|1481->488|1534->520|1579->538|1594->544|1660->589|1753->655|1768->661|1824->696|1885->730|1900->736|1956->771|2017->805|2032->811|2083->841|2144->875|2159->881|2214->915|2275->949|2290->955|2353->997|2414->1031|2429->1037|2493->1080|2554->1114|2569->1120|2628->1158|2689->1192|2704->1198|2759->1232|2820->1266|2835->1272|2885->1301|2946->1335|2961->1341|3024->1383|3483->1814|3499->1820|3577->1876|4090->2362|4121->2372|4223->2447|4252->2455|8237->6413|8266->6420|8302->6428|8593->6691|8609->6697|8666->6732|8745->6783|8761->6789|8825->6831|8904->6882|8920->6888|8984->6930|9063->6981|9079->6987|9133->7019|9212->7070|9228->7076|9300->7126|9379->7177|9395->7183|9470->7236|9549->7287|9565->7293|9625->7331|9704->7382|9720->7388|9784->7430|9863->7481|9879->7487|9941->7527|10019->7577|10035->7583|10100->7626|10179->7677|10195->7683|10266->7732
                  LINES: 27->1|32->1|33->2|37->6|37->6|43->12|43->12|43->12|44->13|44->13|44->13|45->14|45->14|45->14|46->15|46->15|46->15|47->16|47->16|47->16|48->17|48->17|48->17|49->18|49->18|49->18|50->19|50->19|50->19|51->20|51->20|51->20|52->21|52->21|52->21|53->22|53->22|53->22|54->23|54->23|54->23|65->34|65->34|65->34|75->44|75->44|76->45|76->45|171->140|171->140|172->141|179->148|179->148|179->148|180->149|180->149|180->149|181->150|181->150|181->150|182->151|182->151|182->151|183->152|183->152|183->152|184->153|184->153|184->153|185->154|185->154|185->154|186->155|186->155|186->155|187->156|187->156|187->156|188->157|188->157|188->157|189->158|189->158|189->158
                  -- GENERATED --
              */
          