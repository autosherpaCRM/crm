
package views.html.upload

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object fileUpload_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class fileUpload extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[List[String],String,String,List[Location],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(uploadTypes:List[String],user:String,dealer:String,locationList:List[Location]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.82*/("""
"""),_display_(/*2.2*/mainPageCREManger("AutoSherpaCRM",user,dealer)/*2.48*/ {_display_(Seq[Any](format.raw/*2.50*/("""
 """),format.raw/*3.2*/("""<div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title"><b>Instructions</b></h3>
        </div>
        <div class="panel-body">
            <ul>
                <li>The maximum file size for uploads is <strong>5999 KB</strong> </li>
                <li>Only file types of xlsx are allowed </li>
                <li>You can <strong>drag &amp; drop</strong> files from your desktop on this webpage. </li>
            </ul>
        </div>
    </div>
<div class="panel panel-info">
  <div class="panel-heading"><b>Manage Upload files</b></div>
  <div class="panel-body">

   
    
     <!-- The file upload form used as target for the file upload widget -->
    <form id="fileupload" action="/postfile" method="POST" enctype="multipart/form-data">
    <input type="hidden" id="uploadTypeHidden" name="uploadTypeHidden" value="test" ></input>
        <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
        
		<br>
		<div class="row">
		<div class="col-lg-12">
		<div class="col-md-4">
		<label>Upload Type : </label>
                              <select name="uploadType" id="uploadType" class="form-control m-b" onchange="ajaxCampaignList();">
                              """),_display_(/*32.32*/for(uploadType <- uploadTypes) yield /*32.62*/ {_display_(Seq[Any](format.raw/*32.64*/("""
 										"""),format.raw/*33.12*/("""<option value=""""),_display_(/*33.28*/uploadType),format.raw/*33.38*/("""">"""),_display_(/*33.41*/uploadType),format.raw/*33.51*/("""</option>
								""")))}),format.raw/*34.10*/("""
                               """),format.raw/*35.32*/("""</select>
                               <br />
							</div>
<div class="col-md-2">							
							  <label>Location : </label>
							   
                              <select name="location" id="location_id" onchange="workshopsBasedOnLoca('location_id');" class="form-control m-b" required>
										<option value="0">--SELECT--</option>
 										"""),_display_(/*43.13*/for(localist <- locationList) yield /*43.42*/ {_display_(Seq[Any](format.raw/*43.44*/("""
 										"""),format.raw/*44.12*/("""<option value=""""),_display_(/*44.28*/localist/*44.36*/.getCityId()),format.raw/*44.48*/("""">"""),_display_(/*44.51*/localist/*44.59*/.getName()),format.raw/*44.69*/("""</option>
								""")))}),format.raw/*45.10*/("""
								
                               """),format.raw/*47.32*/("""</select>
							   </div>
							   <div class="col-md-2">							
							  <label>Workshop : </label>
							   
                              <select name="workshop_id" id="workshop_id" class="form-control m-b" required>                             
								
                               </select>
							   </div>
							   <div class="col-md-2" style="display:none;" id="CampIDDis">
							   <label>Campaign Name : </label>
                              <select name="campaignName" id="CampaignId" class="form-control m-b" >								
                               </select> 
							   </div>
							   <div class="col-md-2" style="display:none;"  id="campaignFromDate" >
							   <label>From Date : </label>
							   <input type="text" class="datepickerFilter form-control m-b" name="campaignFromDate" readonly>
							    </div>
								<div class="col-md-2" style="display:none;" id="campaignToDate">
								<label>To Date : </label>
								<input type="text" class="datepickerFilter form-control m-b" name="campaignToDate" readonly>
								</div>
							  </div>
							  </div>
							   
        <div class="row fileupload-buttonbar">
            <div class="col-lg-7">
                <!-- The fileinput-button span is used to style the file input field as button -->
                <span class="btn btn-success fileinput-button">
                    <i class="glyphicon glyphicon-plus"></i>
                    <span>Add files...</span>
                    <input type="file" name="uploadfile" multiple>
                </span>
                <button type="submit" class="btn btn-primary start">
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start upload</span>
                </button>
                <button type="button" class="btn btn-danger delete">
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Delete</span>
                </button>
                <input type="checkbox" class="toggle">
                <span></span>
                <!--<button class="btn btn-success wyzprocess" data-type="POST" data-url=""""),format.raw/*90.91*/("""{"""),format.raw/*90.92*/("""%=file.processUrl%"""),format.raw/*90.110*/("""}"""),format.raw/*90.111*/("""""""),format.raw/*90.112*/("""{"""),format.raw/*90.113*/("""% if (file.deleteWithCredentials) """),format.raw/*90.147*/("""{"""),format.raw/*90.148*/(""" """),format.raw/*90.149*/("""%"""),format.raw/*90.150*/("""}"""),format.raw/*90.151*/(""" """),format.raw/*90.152*/("""data-xhr-fields='"""),format.raw/*90.169*/("""{"""),format.raw/*90.170*/(""""withCredentials":true"""),format.raw/*90.192*/("""}"""),format.raw/*90.193*/("""'"""),format.raw/*90.194*/("""{"""),format.raw/*90.195*/("""% """),format.raw/*90.197*/("""}"""),format.raw/*90.198*/(""" """),format.raw/*90.199*/("""%"""),format.raw/*90.200*/("""}"""),format.raw/*90.201*/(""">
                    <i class="glyphicon glyphicon-check"></i>
                    <span>Submit For Reporting</span>
                </button>
				<input type="checkbox" class="poggle">-->
                
               <!-- The global file processing state -->
                <span class="fileupload-process"></span>
            </div>
            <!-- The global progress state -->
            <div class="col-lg-5 fileupload-progress fade">
                <!-- The global progress bar -->
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                </div>
                <!-- The extended global progress state -->
                <div class="progress-extended">&nbsp;</div>
            </div>
        </div>
        <!-- The table listing the files available for upload/download -->
        <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
    </form>
   
   
	</div>
<!-- The blueimp Gallery widget -->
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">�</a>
    <a class="next">�</a>
    <a class="close">�</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
</div>
 </div>


<div class="modal fade" id="uploadStatusPopup" role="dialog">
    <div class="modal-dialog modal-lg">
    
     
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="text-align:center"><b>Upload Status</b> </h4>
        </div>
        <div class="modal-body">
           <div class="table table-responsive">
      <table class="table table-striped table-bordered table-hover" id="uploadlist">
                                    <thead>
                                 <tr> 
                                    <th>File Name</th>
                                    <th>filePath</th>
                                    <th>Uploaded By</th>
                                    <th>Status</th>                                   
                                    <th>Uploaded Date</th>
                                    <th>Number Accepted</th>                                    
                                    <th>Processing Errors</th>
                                    <th>Number Discarded</th>
                                    <th>Error</th>
                                   	<th>isError</th>
                                    <th>Processing Start</th>
                                    <th>Processing End</th>
                                 </tr>
                              </thead>
                                <tbody>
                   </tbody>
           </table>
        </div>
        </div>
      
      </div>
      
    </div>
  </div>

 
  <!--
 <div class="row">
  <div class="col-lg-12">
    
    <br>
    <div class="panel panel-info">
      <div class="panel-heading"><b>Search</b></div>
      <div class="panel-body">
        <div class="table table-responsive">
      <table class="table table-striped table-bordered table-hover" id="uploadlist">
                                    <thead>
                                 <tr> 
                                    <th>File Name</th>
                                    <th>filePath</th>
                                    <th>Uploaded By</th>
                                    <th>Status</th>                                   
                                    <th>Uploaded Date</th>
                                    <th>Number Accepted</th>                                    
                                    <th>Processing Errors</th>
                                    <th>Number Discarded</th>
                                    <th>Error</th>
                                   	<th>isError</th>
                                    <th>Processing Start</th>
                                    <th>Processing End</th>
                                 </tr>
                              </thead>
                                <tbody>
                   </tbody>
           </table>
        </div>
      </div>
    </div>
  </div>
</div>

-->
 
""")))}),format.raw/*205.2*/("""




"""),format.raw/*210.1*/("""<script type="text/javascript">
 function getResultIntable(idIs)"""),format.raw/*211.33*/("""{"""),format.raw/*211.34*/("""
	 """),format.raw/*212.3*/("""//alert("getResultIntable : "+idIs);
  	var selectedType = idIs;
  	console.log(selectedType);
  	 var tableHeaderRowCount = 1;
     var table = document.getElementById('uploadlist');
     var rowCount = table.rows.length;
     for (var i = tableHeaderRowCount; i < rowCount; i++) """),format.raw/*218.59*/("""{"""),format.raw/*218.60*/("""
         """),format.raw/*219.10*/("""table.deleteRow(tableHeaderRowCount);
     """),format.raw/*220.6*/("""}"""),format.raw/*220.7*/("""
     
     """),format.raw/*222.6*/("""$('#uploadlist').dataTable( """),format.raw/*222.34*/("""{"""),format.raw/*222.35*/("""
    	 """),format.raw/*223.7*/("""destroy: true,
        "processing": false,
        "serverSide": false,        
        "paging": true,
        "searching":false,
        "ordering":false,
        "ajax": "/uploadListById/"+selectedType,

        "columnDefs": [
                       """),format.raw/*232.24*/("""{"""),format.raw/*232.25*/("""
                           """),format.raw/*233.28*/(""""render": function ( data, type, row ) """),format.raw/*233.67*/("""{"""),format.raw/*233.68*/("""
                                """),format.raw/*234.33*/("""if(row[9] == true)
                               		return '<a href="'+data+'">error link</a>';
                               		else
                                   		return "";
                           """),format.raw/*238.28*/("""}"""),format.raw/*238.29*/(""",
                           "targets": 8
                       """),format.raw/*240.24*/("""}"""),format.raw/*240.25*/(""","""),format.raw/*240.26*/("""{"""),format.raw/*240.27*/("""
                           """),format.raw/*241.28*/(""""render": function ( data, type, row ) """),format.raw/*241.67*/("""{"""),format.raw/*241.68*/("""
                              		"""),format.raw/*242.33*/("""return '<a href="'+row[1]+'">'+data+'</a>';
                          """),format.raw/*243.27*/("""}"""),format.raw/*243.28*/(""",
                          "targets": 0
                          
                           """),format.raw/*246.28*/("""}"""),format.raw/*246.29*/(""",
                       """),format.raw/*247.24*/("""{"""),format.raw/*247.25*/(""" """),format.raw/*247.26*/(""""visible": false,  "targets": [ 1,9 ] """),format.raw/*247.64*/("""}"""),format.raw/*247.65*/("""
                   """),format.raw/*248.20*/("""]
        
    """),format.raw/*250.5*/("""}"""),format.raw/*250.6*/(""" """),format.raw/*250.7*/(""");    
		
 """),format.raw/*252.2*/("""}"""),format.raw/*252.3*/("""
    

"""),format.raw/*255.1*/("""</script>
<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
"""),format.raw/*258.1*/("""{"""),format.raw/*258.2*/("""% for (var i=0, file; file=o.files[i]; i++) """),format.raw/*258.46*/("""{"""),format.raw/*258.47*/(""" """),format.raw/*258.48*/("""%"""),format.raw/*258.49*/("""}"""),format.raw/*258.50*/("""
    """),format.raw/*259.5*/("""<tr class="template-upload fade">
       <td>
            <p class="name">"""),format.raw/*261.29*/("""{"""),format.raw/*261.30*/("""%=file.name%"""),format.raw/*261.42*/("""}"""),format.raw/*261.43*/("""</p>
            <strong class="error text-danger"></strong>
        </td>
        <td>
            <p class="size">Processing...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
        </td>
        <td>
            """),format.raw/*269.13*/("""{"""),format.raw/*269.14*/("""% if (!i && !o.options.autoUpload) """),format.raw/*269.49*/("""{"""),format.raw/*269.50*/(""" """),format.raw/*269.51*/("""%"""),format.raw/*269.52*/("""}"""),format.raw/*269.53*/("""
                """),format.raw/*270.17*/("""<button class="btn btn-primary start" disabled>
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start</span>
                </button>
            """),format.raw/*274.13*/("""{"""),format.raw/*274.14*/("""% """),format.raw/*274.16*/("""}"""),format.raw/*274.17*/(""" """),format.raw/*274.18*/("""%"""),format.raw/*274.19*/("""}"""),format.raw/*274.20*/("""
            """),format.raw/*275.13*/("""{"""),format.raw/*275.14*/("""% if (!i) """),format.raw/*275.24*/("""{"""),format.raw/*275.25*/(""" """),format.raw/*275.26*/("""%"""),format.raw/*275.27*/("""}"""),format.raw/*275.28*/("""
                """),format.raw/*276.17*/("""<button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            """),format.raw/*280.13*/("""{"""),format.raw/*280.14*/("""% """),format.raw/*280.16*/("""}"""),format.raw/*280.17*/(""" """),format.raw/*280.18*/("""%"""),format.raw/*280.19*/("""}"""),format.raw/*280.20*/("""
        """),format.raw/*281.9*/("""</td>
		<td>&nbsp;</td>
    </tr>
"""),format.raw/*284.1*/("""{"""),format.raw/*284.2*/("""% """),format.raw/*284.4*/("""}"""),format.raw/*284.5*/(""" """),format.raw/*284.6*/("""%"""),format.raw/*284.7*/("""}"""),format.raw/*284.8*/("""
"""),format.raw/*285.1*/("""</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
"""),format.raw/*288.1*/("""{"""),format.raw/*288.2*/("""% for (var i=0, file; file=o.files[i]; i++) """),format.raw/*288.46*/("""{"""),format.raw/*288.47*/(""" """),format.raw/*288.48*/("""%"""),format.raw/*288.49*/("""}"""),format.raw/*288.50*/("""
    """),format.raw/*289.5*/("""<tr class="template-download fade">
        <td>
            <p class="name">
                """),format.raw/*292.17*/("""{"""),format.raw/*292.18*/("""% if (file.url) """),format.raw/*292.34*/("""{"""),format.raw/*292.35*/(""" """),format.raw/*292.36*/("""%"""),format.raw/*292.37*/("""}"""),format.raw/*292.38*/("""
                    """),format.raw/*293.21*/("""<a href=""""),format.raw/*293.30*/("""{"""),format.raw/*293.31*/("""%=file.url%"""),format.raw/*293.42*/("""}"""),format.raw/*293.43*/("""" title=""""),format.raw/*293.52*/("""{"""),format.raw/*293.53*/("""%=file.name%"""),format.raw/*293.65*/("""}"""),format.raw/*293.66*/("""" download=""""),format.raw/*293.78*/("""{"""),format.raw/*293.79*/("""%=file.name%"""),format.raw/*293.91*/("""}"""),format.raw/*293.92*/("""" """),format.raw/*293.94*/("""{"""),format.raw/*293.95*/("""%=file.thumbnailUrl?'data-gallery':''%"""),format.raw/*293.133*/("""}"""),format.raw/*293.134*/(""">"""),format.raw/*293.135*/("""{"""),format.raw/*293.136*/("""%=file.name%"""),format.raw/*293.148*/("""}"""),format.raw/*293.149*/("""</a>
                """),format.raw/*294.17*/("""{"""),format.raw/*294.18*/("""% """),format.raw/*294.20*/("""}"""),format.raw/*294.21*/(""" """),format.raw/*294.22*/("""else """),format.raw/*294.27*/("""{"""),format.raw/*294.28*/(""" """),format.raw/*294.29*/("""%"""),format.raw/*294.30*/("""}"""),format.raw/*294.31*/("""
                    """),format.raw/*295.21*/("""<span>"""),format.raw/*295.27*/("""{"""),format.raw/*295.28*/("""%=file.name%"""),format.raw/*295.40*/("""}"""),format.raw/*295.41*/("""</span>
                """),format.raw/*296.17*/("""{"""),format.raw/*296.18*/("""% """),format.raw/*296.20*/("""}"""),format.raw/*296.21*/(""" """),format.raw/*296.22*/("""%"""),format.raw/*296.23*/("""}"""),format.raw/*296.24*/("""
            """),format.raw/*297.13*/("""</p>
            """),format.raw/*298.13*/("""{"""),format.raw/*298.14*/("""% if (file.error) """),format.raw/*298.32*/("""{"""),format.raw/*298.33*/(""" """),format.raw/*298.34*/("""%"""),format.raw/*298.35*/("""}"""),format.raw/*298.36*/("""
                """),format.raw/*299.17*/("""<div><span class="label label-danger">Error</span> """),format.raw/*299.68*/("""{"""),format.raw/*299.69*/("""%=file.error%"""),format.raw/*299.82*/("""}"""),format.raw/*299.83*/("""</div>
            """),format.raw/*300.13*/("""{"""),format.raw/*300.14*/("""% """),format.raw/*300.16*/("""}"""),format.raw/*300.17*/(""" """),format.raw/*300.18*/("""%"""),format.raw/*300.19*/("""}"""),format.raw/*300.20*/("""
        """),format.raw/*301.9*/("""</td>
        <td>
            <span class="size">"""),format.raw/*303.32*/("""{"""),format.raw/*303.33*/("""%=o.formatFileSize(file.size)%"""),format.raw/*303.63*/("""}"""),format.raw/*303.64*/("""</span>
        </td>
		<td>
            """),format.raw/*306.13*/("""{"""),format.raw/*306.14*/("""% if (file.deleteUrl) """),format.raw/*306.36*/("""{"""),format.raw/*306.37*/(""" """),format.raw/*306.38*/("""%"""),format.raw/*306.39*/("""}"""),format.raw/*306.40*/("""
                """),format.raw/*307.17*/("""<button class="btn btn-danger delete" data-type=""""),format.raw/*307.66*/("""{"""),format.raw/*307.67*/("""%=file.deleteType%"""),format.raw/*307.85*/("""}"""),format.raw/*307.86*/("""" data-url=""""),format.raw/*307.98*/("""{"""),format.raw/*307.99*/("""%=file.deleteUrl%"""),format.raw/*307.116*/("""}"""),format.raw/*307.117*/("""""""),format.raw/*307.118*/("""{"""),format.raw/*307.119*/("""% if (file.deleteWithCredentials) """),format.raw/*307.153*/("""{"""),format.raw/*307.154*/(""" """),format.raw/*307.155*/("""%"""),format.raw/*307.156*/("""}"""),format.raw/*307.157*/(""" """),format.raw/*307.158*/("""data-xhr-fields='"""),format.raw/*307.175*/("""{"""),format.raw/*307.176*/(""""withCredentials":true"""),format.raw/*307.198*/("""}"""),format.raw/*307.199*/("""'"""),format.raw/*307.200*/("""{"""),format.raw/*307.201*/("""% """),format.raw/*307.203*/("""}"""),format.raw/*307.204*/(""" """),format.raw/*307.205*/("""%"""),format.raw/*307.206*/("""}"""),format.raw/*307.207*/(""">
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Delete</span>
                </button>
                <input type="checkbox" name="delete" value="1" class="toggle">


            """),format.raw/*314.13*/("""{"""),format.raw/*314.14*/("""% """),format.raw/*314.16*/("""}"""),format.raw/*314.17*/(""" """),format.raw/*314.18*/("""else """),format.raw/*314.23*/("""{"""),format.raw/*314.24*/(""" """),format.raw/*314.25*/("""%"""),format.raw/*314.26*/("""}"""),format.raw/*314.27*/("""
                """),format.raw/*315.17*/("""<button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            """),format.raw/*319.13*/("""{"""),format.raw/*319.14*/("""% """),format.raw/*319.16*/("""}"""),format.raw/*319.17*/(""" """),format.raw/*319.18*/("""%"""),format.raw/*319.19*/("""}"""),format.raw/*319.20*/("""
        """),format.raw/*320.9*/("""</td>
		<td>
			"""),format.raw/*322.4*/("""{"""),format.raw/*322.5*/("""% if (file.deleteUrl) """),format.raw/*322.27*/("""{"""),format.raw/*322.28*/(""" """),format.raw/*322.29*/("""%"""),format.raw/*322.30*/("""}"""),format.raw/*322.31*/("""
              """),format.raw/*323.15*/("""<button class="btn btn-success wyzprocess" data-type="POST" data-url=""""),format.raw/*323.85*/("""{"""),format.raw/*323.86*/("""%=file.processUrl%"""),format.raw/*323.104*/("""}"""),format.raw/*323.105*/("""""""),format.raw/*323.106*/("""{"""),format.raw/*323.107*/("""% if (file.deleteWithCredentials) """),format.raw/*323.141*/("""{"""),format.raw/*323.142*/(""" """),format.raw/*323.143*/("""%"""),format.raw/*323.144*/("""}"""),format.raw/*323.145*/(""" """),format.raw/*323.146*/("""data-xhr-fields='"""),format.raw/*323.163*/("""{"""),format.raw/*323.164*/(""""withCredentials":true"""),format.raw/*323.186*/("""}"""),format.raw/*323.187*/("""'"""),format.raw/*323.188*/("""{"""),format.raw/*323.189*/("""% """),format.raw/*323.191*/("""}"""),format.raw/*323.192*/(""" """),format.raw/*323.193*/("""%"""),format.raw/*323.194*/("""}"""),format.raw/*323.195*/(""">
                    <i class="glyphicon glyphicon-check"></i>
                    <span>Click to Submit</span>
                </button>
				
			 """),format.raw/*328.5*/("""{"""),format.raw/*328.6*/("""% """),format.raw/*328.8*/("""}"""),format.raw/*328.9*/(""" """),format.raw/*328.10*/("""%"""),format.raw/*328.11*/("""}"""),format.raw/*328.12*/("""
		"""),format.raw/*329.3*/("""</td>
    </tr>
"""),format.raw/*331.1*/("""{"""),format.raw/*331.2*/("""% """),format.raw/*331.4*/("""}"""),format.raw/*331.5*/(""" """),format.raw/*331.6*/("""%"""),format.raw/*331.7*/("""}"""),format.raw/*331.8*/("""
"""),format.raw/*332.1*/("""</script>



"""))
      }
    }
  }

  def render(uploadTypes:List[String],user:String,dealer:String,locationList:List[Location]): play.twirl.api.HtmlFormat.Appendable = apply(uploadTypes,user,dealer,locationList)

  def f:((List[String],String,String,List[Location]) => play.twirl.api.HtmlFormat.Appendable) = (uploadTypes,user,dealer,locationList) => apply(uploadTypes,user,dealer,locationList)

  def ref: this.type = this

}


}

/**/
object fileUpload extends fileUpload_Scope0.fileUpload
              /*
                  -- GENERATED --
                  DATE: Thu Dec 28 12:24:31 IST 2017
                  SOURCE: D:/CRMNEW/28th Dec/crm/app/views/upload/fileUpload.scala.html
                  HASH: f7feea0edd5f10daaf10784da32d701ca12e2ed1
                  MATRIX: 797->1|972->81|1000->84|1054->130|1093->132|1122->135|2450->1436|2496->1466|2536->1468|2577->1481|2620->1497|2651->1507|2681->1510|2712->1520|2763->1540|2824->1573|3212->1934|3257->1963|3297->1965|3338->1978|3381->1994|3398->2002|3431->2014|3461->2017|3478->2025|3509->2035|3560->2055|3631->2098|5826->4265|5855->4266|5902->4284|5932->4285|5962->4286|5992->4287|6055->4321|6085->4322|6115->4323|6145->4324|6175->4325|6205->4326|6251->4343|6281->4344|6332->4366|6362->4367|6392->4368|6422->4369|6453->4371|6483->4372|6513->4373|6543->4374|6573->4375|11168->8939|11206->8949|11300->9014|11330->9015|11362->9019|11678->9306|11708->9307|11748->9318|11820->9362|11849->9363|11891->9377|11948->9405|11978->9406|12014->9414|12307->9678|12337->9679|12395->9708|12463->9747|12493->9748|12556->9782|12798->9995|12828->9996|12924->10063|12954->10064|12984->10065|13014->10066|13072->10095|13140->10134|13170->10135|13233->10169|13333->10240|13363->10241|13490->10339|13520->10340|13575->10366|13605->10367|13635->10368|13702->10406|13732->10407|13782->10428|13827->10445|13856->10446|13885->10447|13926->10460|13955->10461|13993->10471|14143->10593|14172->10594|14245->10638|14275->10639|14305->10640|14335->10641|14365->10642|14399->10648|14504->10724|14534->10725|14575->10737|14605->10738|15026->11130|15056->11131|15120->11166|15150->11167|15180->11168|15210->11169|15240->11170|15287->11188|15508->11380|15538->11381|15569->11383|15599->11384|15629->11385|15659->11386|15689->11387|15732->11401|15762->11402|15801->11412|15831->11413|15861->11414|15891->11415|15921->11416|15968->11434|16186->11623|16216->11624|16247->11626|16277->11627|16307->11628|16337->11629|16367->11630|16405->11640|16470->11677|16499->11678|16529->11680|16558->11681|16587->11682|16616->11683|16645->11684|16675->11686|16829->11812|16858->11813|16931->11857|16961->11858|16991->11859|17021->11860|17051->11861|17085->11867|17211->11964|17241->11965|17286->11981|17316->11982|17346->11983|17376->11984|17406->11985|17457->12007|17495->12016|17525->12017|17565->12028|17595->12029|17633->12038|17663->12039|17704->12051|17734->12052|17775->12064|17805->12065|17846->12077|17876->12078|17907->12080|17937->12081|18005->12119|18036->12120|18067->12121|18098->12122|18140->12134|18171->12135|18222->12157|18252->12158|18283->12160|18313->12161|18343->12162|18377->12167|18407->12168|18437->12169|18467->12170|18497->12171|18548->12193|18583->12199|18613->12200|18654->12212|18684->12213|18738->12238|18768->12239|18799->12241|18829->12242|18859->12243|18889->12244|18919->12245|18962->12259|19009->12277|19039->12278|19086->12296|19116->12297|19146->12298|19176->12299|19206->12300|19253->12318|19333->12369|19363->12370|19405->12383|19435->12384|19484->12404|19514->12405|19545->12407|19575->12408|19605->12409|19635->12410|19665->12411|19703->12421|19784->12473|19814->12474|19873->12504|19903->12505|19976->12549|20006->12550|20057->12572|20087->12573|20117->12574|20147->12575|20177->12576|20224->12594|20302->12643|20332->12644|20379->12662|20409->12663|20450->12675|20480->12676|20527->12693|20558->12694|20589->12695|20620->12696|20684->12730|20715->12731|20746->12732|20777->12733|20808->12734|20839->12735|20886->12752|20917->12753|20969->12775|21000->12776|21031->12777|21062->12778|21094->12780|21125->12781|21156->12782|21187->12783|21218->12784|21477->13014|21507->13015|21538->13017|21568->13018|21598->13019|21632->13024|21662->13025|21692->13026|21722->13027|21752->13028|21799->13046|22017->13235|22047->13236|22078->13238|22108->13239|22138->13240|22168->13241|22198->13242|22236->13252|22282->13270|22311->13271|22362->13293|22392->13294|22422->13295|22452->13296|22482->13297|22527->13313|22626->13383|22656->13384|22704->13402|22735->13403|22766->13404|22797->13405|22861->13439|22892->13440|22923->13441|22954->13442|22985->13443|23016->13444|23063->13461|23094->13462|23146->13484|23177->13485|23208->13486|23239->13487|23271->13489|23302->13490|23333->13491|23364->13492|23395->13493|23576->13646|23605->13647|23635->13649|23664->13650|23694->13651|23724->13652|23754->13653|23786->13657|23832->13675|23861->13676|23891->13678|23920->13679|23949->13680|23978->13681|24007->13682|24037->13684
                  LINES: 27->1|32->1|33->2|33->2|33->2|34->3|63->32|63->32|63->32|64->33|64->33|64->33|64->33|64->33|65->34|66->35|74->43|74->43|74->43|75->44|75->44|75->44|75->44|75->44|75->44|75->44|76->45|78->47|121->90|121->90|121->90|121->90|121->90|121->90|121->90|121->90|121->90|121->90|121->90|121->90|121->90|121->90|121->90|121->90|121->90|121->90|121->90|121->90|121->90|121->90|121->90|236->205|241->210|242->211|242->211|243->212|249->218|249->218|250->219|251->220|251->220|253->222|253->222|253->222|254->223|263->232|263->232|264->233|264->233|264->233|265->234|269->238|269->238|271->240|271->240|271->240|271->240|272->241|272->241|272->241|273->242|274->243|274->243|277->246|277->246|278->247|278->247|278->247|278->247|278->247|279->248|281->250|281->250|281->250|283->252|283->252|286->255|289->258|289->258|289->258|289->258|289->258|289->258|289->258|290->259|292->261|292->261|292->261|292->261|300->269|300->269|300->269|300->269|300->269|300->269|300->269|301->270|305->274|305->274|305->274|305->274|305->274|305->274|305->274|306->275|306->275|306->275|306->275|306->275|306->275|306->275|307->276|311->280|311->280|311->280|311->280|311->280|311->280|311->280|312->281|315->284|315->284|315->284|315->284|315->284|315->284|315->284|316->285|319->288|319->288|319->288|319->288|319->288|319->288|319->288|320->289|323->292|323->292|323->292|323->292|323->292|323->292|323->292|324->293|324->293|324->293|324->293|324->293|324->293|324->293|324->293|324->293|324->293|324->293|324->293|324->293|324->293|324->293|324->293|324->293|324->293|324->293|324->293|324->293|325->294|325->294|325->294|325->294|325->294|325->294|325->294|325->294|325->294|325->294|326->295|326->295|326->295|326->295|326->295|327->296|327->296|327->296|327->296|327->296|327->296|327->296|328->297|329->298|329->298|329->298|329->298|329->298|329->298|329->298|330->299|330->299|330->299|330->299|330->299|331->300|331->300|331->300|331->300|331->300|331->300|331->300|332->301|334->303|334->303|334->303|334->303|337->306|337->306|337->306|337->306|337->306|337->306|337->306|338->307|338->307|338->307|338->307|338->307|338->307|338->307|338->307|338->307|338->307|338->307|338->307|338->307|338->307|338->307|338->307|338->307|338->307|338->307|338->307|338->307|338->307|338->307|338->307|338->307|338->307|338->307|338->307|345->314|345->314|345->314|345->314|345->314|345->314|345->314|345->314|345->314|345->314|346->315|350->319|350->319|350->319|350->319|350->319|350->319|350->319|351->320|353->322|353->322|353->322|353->322|353->322|353->322|353->322|354->323|354->323|354->323|354->323|354->323|354->323|354->323|354->323|354->323|354->323|354->323|354->323|354->323|354->323|354->323|354->323|354->323|354->323|354->323|354->323|354->323|354->323|354->323|354->323|359->328|359->328|359->328|359->328|359->328|359->328|359->328|360->329|362->331|362->331|362->331|362->331|362->331|362->331|362->331|363->332
                  -- GENERATED --
              */
          