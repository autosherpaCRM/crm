
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object changeAssinedCalls_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class changeAssinedCalls extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template5[List[WyzUser],List[CallDispositionData],List[Campaign],String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(userlist:List[WyzUser],dispoList:List[CallDispositionData],camplist:List[Campaign],user:String,dealercode:String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.116*/("""
"""),_display_(/*2.2*/mainPageCREManger("AutoSherpaCRM",user,dealercode)/*2.52*/{_display_(Seq[Any](format.raw/*2.53*/("""

"""),format.raw/*4.1*/("""<div class="panel panel-primary">
  <div class="panel-heading">Change Assignment
  <button type="button" class="btn btn-warning pull-right btn-xs" id="clearAllId">Clear All</button>
  </div>
  <div class="panel-body">
  <div class="row">
  <div class="col-md-2">
	<label>Agent Name : </label>
	<div class="form-group">
	<select class="multiSelectClass form-control" id="crename" multiple="multiple">
		
		"""),_display_(/*15.4*/for(user <- userlist) yield /*15.25*/{_display_(Seq[Any](format.raw/*15.26*/("""
			"""),format.raw/*16.4*/("""<option value=""""),_display_(/*16.20*/user/*16.24*/.getId()),format.raw/*16.32*/("""">"""),_display_(/*16.35*/user/*16.39*/.getUserName()),format.raw/*16.53*/("""</option>
			
		""")))}),format.raw/*18.4*/("""
	
	"""),format.raw/*20.2*/("""</select>
  </div>
  </div>
  <div class="col-md-2">
  <div class="form-group">
	<label>From Assign Date : </label>
	<input type="text" class="form-control AssFromDtClass datepicker" placeholder="From Assign Date" id="assignFromDate" readonly>
  </div>
  </div>
   <div class="col-md-2">
   <div class="form-group">
	<label>To Assign Date : </label>
	<input type="text" class="form-control AssToDtClass datepicker" placeholder="To Assign Date" id="assignToDate" readonly>
  </div>
  </div>
   <div class="col-md-2">
	<label>Disposition Status : </label>
	<div class="form-group">
	<select class="multiSelectClass form-control" id="all_dispositions" multiple="multiple">
	
		<option value="0">Not yet Called</option>
		"""),_display_(/*41.4*/for(dispo <- dispoList) yield /*41.27*/{_display_(Seq[Any](format.raw/*41.28*/("""
			"""),format.raw/*42.4*/("""<option value=""""),_display_(/*42.20*/dispo/*42.25*/.getId()),format.raw/*42.33*/("""">"""),_display_(/*42.36*/dispo/*42.41*/.getDisposition()),format.raw/*42.58*/("""</option>
			
		""")))}),format.raw/*44.4*/("""
	"""),format.raw/*45.2*/("""</select>
  </div>
  </div>
  <div class="col-md-2">
	<label>Campaign : </label>
	<div class="form-group">
	<select class="form-control CampaignClass" id="campaignSelected">
		<option value="0">--Select--</option>		
		"""),_display_(/*53.4*/for(camlist <- camplist) yield /*53.28*/{_display_(Seq[Any](format.raw/*53.29*/("""
			"""),format.raw/*54.4*/("""<option value=""""),_display_(/*54.20*/camlist/*54.27*/.getId()),format.raw/*54.35*/("""">"""),_display_(/*54.38*/camlist/*54.45*/.getCampaignName),format.raw/*54.61*/("""</option>
			
		""")))}),format.raw/*56.4*/("""
	"""),format.raw/*57.2*/("""</select>
  </div>
  </div>
  <div class="col-md-2"><br />
	
	<button type="button" class="btn btn-info btn-block" id="ViewAlltblBox" onclick="ajaxViewAssignDataList()">View</button>
	
  </div>
  
  </div>
  <br />
  
   <span type="label" id="viewCallsERR" style="color:red"></span>
 
  <div class="row" id="selectAllDIv" style="display:none">
  <div class="col-md-12">
  <table id="tbl" class="table table-bordered table-responsive">
<thead>
<tr>
<th style="width: 100px;"> <input type="checkbox" id ="chckHead"/> Select All</th>
<th>User Name</th>
<th>Customer Name</th>
<th>Mobile Number</th>
<th>Vehicle RegNo.</th>
<th>Disposition</th>
<th>Campaign</th>
<th style="display:none;"></th>

</tr>
</thead>
<tbody>

</tbody>
</table>
  </div>
  </div>
  
  <span type="label" id="assignCallsERR" style="color:red"></span>
  <div class="row">
	<div class="col-md-2">
	<label>Assign To</label>
	<div class="form-group">
	<select class="multiSelectTop form-control" id="userlist" multiple="multiple" name="userId[]" required>
		"""),_display_(/*100.4*/for(user <- userlist) yield /*100.25*/{_display_(Seq[Any](format.raw/*100.26*/("""
			"""),format.raw/*101.4*/("""<option value=""""),_display_(/*101.20*/user/*101.24*/.getId()),format.raw/*101.32*/("""">"""),_display_(/*101.35*/user/*101.39*/.getUserName()),format.raw/*101.53*/("""</option>
			
		""")))}),format.raw/*103.4*/("""
	"""),format.raw/*104.2*/("""</select>
  </div>
  </div>
  	<div class="col-md-2"><br />
		 <button type="button" class="btn btn-info" onclick="ajaxAssignDataList();" >Change Assignment</button>
	  </div>
 
  </div>
  
  </div>
</div>



""")))}),format.raw/*118.2*/("""

"""),format.raw/*120.1*/("""<script>
 $(document).ready(function() """),format.raw/*121.31*/("""{"""),format.raw/*121.32*/("""
 
       """),format.raw/*123.8*/("""$('.multiSelectClass').multiselect("""),format.raw/*123.43*/("""{"""),format.raw/*123.44*/(""" 
         """),format.raw/*124.10*/("""includeSelectAllOption: true,
           enableFiltering:true,
           buttonWidth: '190px'        
           
     """),format.raw/*128.6*/("""}"""),format.raw/*128.7*/(""");
	 $('.multiSelectTop').multiselect("""),format.raw/*129.36*/("""{"""),format.raw/*129.37*/(""" 
         """),format.raw/*130.10*/("""includeSelectAllOption: true,
           enableFiltering:true,
           buttonWidth: '190px',
maxHeight: 150,
            dropUp: true	    
           
     """),format.raw/*136.6*/("""}"""),format.raw/*136.7*/(""");
     
"""),format.raw/*138.1*/("""}"""),format.raw/*138.2*/(""");
</script>

<script type="text/javascript">
    $('.chcktbl').click(function () """),format.raw/*142.37*/("""{"""),format.raw/*142.38*/("""
            
           
            """),format.raw/*145.13*/("""$('.chcktbl:not(:checked)').attr('disabled', true);
      
            $('.chcktbl:not(:checked)').attr('disabled', false);

            
        
    """),format.raw/*151.5*/("""}"""),format.raw/*151.6*/(""");
</script>
<script type="text/javascript">
    $('#chckHead').click(function () """),format.raw/*154.38*/("""{"""),format.raw/*154.39*/("""
 
        """),format.raw/*156.9*/("""if (this.checked == false) """),format.raw/*156.36*/("""{"""),format.raw/*156.37*/("""
 
            """),format.raw/*158.13*/("""$('.chcktbl:checked').attr('checked', false);
        """),format.raw/*159.9*/("""}"""),format.raw/*159.10*/("""
        """),format.raw/*160.9*/("""else """),format.raw/*160.14*/("""{"""),format.raw/*160.15*/("""
            """),format.raw/*161.13*/("""$('.chcktbl:not(:checked)').attr('checked', true);
 
        """),format.raw/*163.9*/("""}"""),format.raw/*163.10*/("""
    """),format.raw/*164.5*/("""}"""),format.raw/*164.6*/("""); 
 
</script>
<script>
	$("#clearAllId").click(function()"""),format.raw/*168.35*/("""{"""),format.raw/*168.36*/("""
		"""),format.raw/*169.3*/("""$(".AssFromDtClass").val('');
		$(".AssToDtClass").val('');
		$(".CampaignClass").val('0');
		$("#selectAllDIv").hide();
			
		
	"""),format.raw/*175.2*/("""}"""),format.raw/*175.3*/(""");
	$("#ViewAlltblBox").click(function()"""),format.raw/*176.38*/("""{"""),format.raw/*176.39*/("""
		"""),format.raw/*177.3*/("""$("#selectAllDIv").show();
	"""),format.raw/*178.2*/("""}"""),format.raw/*178.3*/(""");
</script>
<script>
function ajaxViewAssignDataList()"""),format.raw/*181.34*/("""{"""),format.raw/*181.35*/("""

	"""),format.raw/*183.2*/("""var campSelec = $('#campaignSelected').val();
	var fromdate = $('#assignFromDate').val();
	var todate = $('#assignToDate').val();
	var u = $("#crename").val();
	var v = $("#all_dispositions").val();
	
	
	if(campSelec =='0' || v ==null || u ==null || fromdate=='' || todate=='')"""),format.raw/*190.75*/("""{"""),format.raw/*190.76*/("""
		"""),format.raw/*191.3*/("""document.getElementById("viewCallsERR").innerText="please provide all values";
        return false;
	"""),format.raw/*193.2*/("""}"""),format.raw/*193.3*/("""else"""),format.raw/*193.7*/("""{"""),format.raw/*193.8*/("""

		"""),format.raw/*195.3*/("""document.getElementById("viewCallsERR").innerText="";

	var selected_cres;
	var x = document.getElementById("crename");
	var j = 0;
	for (var i = 0; i < x.options.length; i++) """),format.raw/*200.45*/("""{"""),format.raw/*200.46*/("""
		"""),format.raw/*201.3*/("""if (x.options[i].selected == true) """),format.raw/*201.38*/("""{"""),format.raw/*201.39*/("""
			"""),format.raw/*202.4*/("""if (j == 0)
				selected_cres = x.options[i].value;
			else
				selected_cres = selected_cres + ","
						+ x.options[i].value;

			//console.log(selected_cres);
			j++;
		"""),format.raw/*210.3*/("""}"""),format.raw/*210.4*/("""
	"""),format.raw/*211.2*/("""}"""),format.raw/*211.3*/("""
	
	"""),format.raw/*213.2*/("""var selected_dispositions;
	var y = document.getElementById("all_dispositions");
	var k = 0;
	for (var a = 0; a < y.options.length; a++) """),format.raw/*216.45*/("""{"""),format.raw/*216.46*/("""
		"""),format.raw/*217.3*/("""if (y.options[a].selected == true) """),format.raw/*217.38*/("""{"""),format.raw/*217.39*/("""
			"""),format.raw/*218.4*/("""if (k == 0)
				selected_dispositions = y.options[a].value;
			else
				selected_dispositions = selected_dispositions
						+ "," + y.options[a].value;

			//console.log(selected_dispositions);
			k++;
		"""),format.raw/*226.3*/("""}"""),format.raw/*226.4*/("""
	"""),format.raw/*227.2*/("""}"""),format.raw/*227.3*/("""

	"""),format.raw/*229.2*/("""var urlLink = "/CREManager/changeAssignedCalls	"
    
    $('#tbl').dataTable( """),format.raw/*231.26*/("""{"""),format.raw/*231.27*/("""
     """),format.raw/*232.6*/(""""bDestroy": true,
     "processing": true,
     "serverSide": true,
     "scrollY": 300,
     "paging": true,
     "searching": false,
     "ordering":false,
     "ajax": """),format.raw/*239.14*/("""{"""),format.raw/*239.15*/("""
	        """),format.raw/*240.10*/("""'type': 'POST',
	        'url': urlLink,
	        'data': """),format.raw/*242.18*/("""{"""),format.raw/*242.19*/("""
	        	"""),format.raw/*243.11*/("""campSelec: ''+campSelec,
	        	fromdate:''+fromdate,
	        	todate:''+todate,
	        	selected_cres:''+selected_cres,
	        	selected_dispositions:''+selected_dispositions,
	        	
	        """),format.raw/*249.10*/("""}"""),format.raw/*249.11*/(""",
     """),format.raw/*250.6*/("""}"""),format.raw/*250.7*/("""
 """),format.raw/*251.2*/("""}"""),format.raw/*251.3*/(""" """),format.raw/*251.4*/("""); 
"""),format.raw/*252.1*/("""}"""),format.raw/*252.2*/("""
     
 """),format.raw/*254.2*/("""}"""),format.raw/*254.3*/("""


"""),format.raw/*257.1*/("""function ajaxAssignDataList()"""),format.raw/*257.30*/("""{"""),format.raw/*257.31*/("""

	"""),format.raw/*259.2*/("""var campSelec = $('#campaignSelected').val();
	var fromdate = $('#assignFromDate').val();
	var todate = $('#assignToDate').val();

	

	var u=$('#userlist').val();
	var selected_camps = new Array();
	$("input[name='callId[]']:checked").each(function(i) """),format.raw/*267.55*/("""{"""),format.raw/*267.56*/("""
		"""),format.raw/*268.3*/("""selected_camps.push($(this).val());
	"""),format.raw/*269.2*/("""}"""),format.raw/*269.3*/(""");

	//alert("u :"+u);
	//alert("selected_camps :"+selected_camps);
	
	if(u =='' || selected_camps =='')"""),format.raw/*274.35*/("""{"""),format.raw/*274.36*/("""

		"""),format.raw/*276.3*/("""document.getElementById("assignCallsERR").innerText="please provide all values for Assignment";
        return false;

		"""),format.raw/*279.3*/("""}"""),format.raw/*279.4*/("""else"""),format.raw/*279.8*/("""{"""),format.raw/*279.9*/("""

			"""),format.raw/*281.4*/("""document.getElementById("assignCallsERR").innerText="";

			var selected_cres_filter;
			var z = document.getElementById("crename");
			var l = 0;
			for (var i = 0; i < z.options.length; i++) """),format.raw/*286.47*/("""{"""),format.raw/*286.48*/("""
				"""),format.raw/*287.5*/("""if (z.options[i].selected == true) """),format.raw/*287.40*/("""{"""),format.raw/*287.41*/("""
					"""),format.raw/*288.6*/("""if (l == 0)
						selected_cres_filter = z.options[i].value;
					else
						selected_cres_filter = selected_cres_filter + ","
								+ z.options[i].value;

					//console.log(selected_cres);
					l++;
				"""),format.raw/*296.5*/("""}"""),format.raw/*296.6*/("""
			"""),format.raw/*297.4*/("""}"""),format.raw/*297.5*/("""
			
			"""),format.raw/*299.4*/("""var selected_dispositions;
			var y = document.getElementById("all_dispositions");
			var k = 0;
			for (var a = 0; a < y.options.length; a++) """),format.raw/*302.47*/("""{"""),format.raw/*302.48*/("""
				"""),format.raw/*303.5*/("""if (y.options[a].selected == true) """),format.raw/*303.40*/("""{"""),format.raw/*303.41*/("""
					"""),format.raw/*304.6*/("""if (k == 0)
						selected_dispositions = y.options[a].value;
					else
						selected_dispositions = selected_dispositions
								+ "," + y.options[a].value;

					//console.log(selected_dispositions);
					k++;
				"""),format.raw/*312.5*/("""}"""),format.raw/*312.6*/("""
			"""),format.raw/*313.4*/("""}"""),format.raw/*313.5*/("""		

	"""),format.raw/*315.2*/("""var selected_cres;
	var x = document.getElementById("userlist");
	var j = 0;
	for (var i = 0; i < x.options.length; i++) """),format.raw/*318.45*/("""{"""),format.raw/*318.46*/("""
		"""),format.raw/*319.3*/("""if (x.options[i].selected == true) """),format.raw/*319.38*/("""{"""),format.raw/*319.39*/("""
			"""),format.raw/*320.4*/("""if (j == 0)
				selected_cres = x.options[i].value;
			else
				selected_cres = selected_cres + ","
						+ x.options[i].value;

			//console.log(selected_cres);
			j++;
		"""),format.raw/*328.3*/("""}"""),format.raw/*328.4*/("""
	"""),format.raw/*329.2*/("""}"""),format.raw/*329.3*/("""

	
	
	"""),format.raw/*333.2*/("""var selected_camps = new Array();
	$("input[name='callId[]']:checked").each(function(i) """),format.raw/*334.55*/("""{"""),format.raw/*334.56*/("""
		"""),format.raw/*335.3*/("""selected_camps.push($(this).val());
	"""),format.raw/*336.2*/("""}"""),format.raw/*336.3*/(""");
	
	//alert(selected_camps.length);
	
	var selectAllStatus = document.getElementById("chckHead").checked;
                alert(selectAllStatus);
	
	
	
	
	var urlLink = "/CREManager/assignCallToAgents"
		$.ajax("""),format.raw/*347.10*/("""{"""),format.raw/*347.11*/("""
	        """),format.raw/*348.10*/("""type: 'POST',	       
	        url: urlLink,
	        data: """),format.raw/*350.16*/("""{"""),format.raw/*350.17*/("""
	        	
	        	"""),format.raw/*352.11*/("""selected_cres:''+selected_cres,
	        	selected_camps:''+selected_camps,
	        	selectAllStatus:''+selectAllStatus,
	        	campSelec: ''+campSelec,
	        	fromdate:''+fromdate,
	        	todate:''+todate,
	        	selected_cres_filter:''+selected_cres_filter,
	        	selected_dispositions:''+selected_dispositions,
	        	
	        	
	        """),format.raw/*362.10*/("""}"""),format.raw/*362.11*/(""",
	        success: function (json) """),format.raw/*363.35*/("""{"""),format.raw/*363.36*/("""
				
	        	"""),format.raw/*365.11*/("""window.location='changeAssignedCalls';
	        """),format.raw/*366.10*/("""}"""),format.raw/*366.11*/(""",
	        error: function () """),format.raw/*367.29*/("""{"""),format.raw/*367.30*/("""
	        	
	        	
	        """),format.raw/*370.10*/("""}"""),format.raw/*370.11*/("""
	       
	    """),format.raw/*372.6*/("""}"""),format.raw/*372.7*/(""");

		"""),format.raw/*374.3*/("""}"""),format.raw/*374.4*/("""
	    
	
"""),format.raw/*377.1*/("""}"""),format.raw/*377.2*/("""



  """),format.raw/*381.3*/("""</script>
 """))
      }
    }
  }

  def render(userlist:List[WyzUser],dispoList:List[CallDispositionData],camplist:List[Campaign],user:String,dealercode:String): play.twirl.api.HtmlFormat.Appendable = apply(userlist,dispoList,camplist,user,dealercode)

  def f:((List[WyzUser],List[CallDispositionData],List[Campaign],String,String) => play.twirl.api.HtmlFormat.Appendable) = (userlist,dispoList,camplist,user,dealercode) => apply(userlist,dispoList,camplist,user,dealercode)

  def ref: this.type = this

}


}

/**/
object changeAssinedCalls extends changeAssinedCalls_Scope0.changeAssinedCalls
              /*
                  -- GENERATED --
                  DATE: Thu Dec 28 16:11:55 IST 2017
                  SOURCE: D:/CRMNEW/28th Dec/crm/app/views/changeAssinedCalls.scala.html
                  HASH: 7cf42b5a09e112d3521f5a5c8d7a50435b9edb9c
                  MATRIX: 833->1|1043->115|1071->118|1129->168|1167->169|1197->173|1640->590|1677->611|1716->612|1748->617|1791->633|1804->637|1833->645|1863->648|1876->652|1911->666|1960->685|1993->691|2759->1431|2798->1454|2837->1455|2869->1460|2912->1476|2926->1481|2955->1489|2985->1492|2999->1497|3037->1514|3086->1533|3116->1536|3369->1763|3409->1787|3448->1788|3480->1793|3523->1809|3539->1816|3568->1824|3598->1827|3614->1834|3651->1850|3700->1869|3730->1872|4827->2942|4865->2963|4905->2964|4938->2969|4982->2985|4996->2989|5026->2997|5057->3000|5071->3004|5107->3018|5157->3037|5188->3040|5443->3264|5475->3268|5544->3308|5574->3309|5614->3321|5678->3356|5708->3357|5749->3369|5901->3493|5930->3494|5998->3533|6028->3534|6069->3546|6262->3711|6291->3712|6330->3723|6359->3724|6474->3810|6504->3811|6574->3852|6759->4009|6788->4010|6902->4095|6932->4096|6973->4109|7029->4136|7059->4137|7105->4154|7188->4209|7218->4210|7256->4220|7290->4225|7320->4226|7363->4240|7454->4303|7484->4304|7518->4310|7547->4311|7639->4374|7669->4375|7701->4379|7864->4514|7893->4515|7963->4556|7993->4557|8025->4561|8082->4590|8111->4591|8198->4649|8228->4650|8261->4655|8574->4939|8604->4940|8636->4944|8768->5048|8797->5049|8829->5053|8858->5054|8892->5060|9102->5241|9132->5242|9164->5246|9228->5281|9258->5282|9291->5287|9499->5467|9528->5468|9559->5471|9588->5472|9622->5478|9791->5618|9821->5619|9853->5623|9917->5658|9947->5659|9980->5664|10220->5876|10249->5877|10280->5880|10309->5881|10342->5886|10452->5967|10482->5968|10517->5975|10724->6153|10754->6154|10794->6165|10883->6225|10913->6226|10954->6238|11194->6449|11224->6450|11260->6458|11289->6459|11320->6462|11349->6463|11378->6464|11411->6469|11440->6470|11478->6480|11507->6481|11541->6487|11599->6516|11629->6517|11662->6522|11951->6782|11981->6783|12013->6787|12079->6825|12108->6826|12246->6935|12276->6936|12310->6942|12462->7066|12491->7067|12523->7071|12552->7072|12587->7079|12814->7277|12844->7278|12878->7284|12942->7319|12972->7320|13007->7327|13250->7542|13279->7543|13312->7548|13341->7549|13379->7559|13554->7705|13584->7706|13618->7712|13682->7747|13712->7748|13747->7755|14001->7981|14030->7982|14063->7987|14092->7988|14127->7995|14280->8119|14310->8120|14342->8124|14406->8159|14436->8160|14469->8165|14677->8345|14706->8346|14737->8349|14766->8350|14805->8361|14923->8450|14953->8451|14985->8455|15051->8493|15080->8494|15333->8718|15363->8719|15403->8730|15494->8792|15524->8793|15577->8817|15978->9189|16008->9190|16074->9227|16104->9228|16151->9246|16229->9295|16259->9296|16319->9327|16349->9328|16413->9363|16443->9364|16488->9381|16517->9382|16553->9390|16582->9391|16622->9403|16651->9404|16689->9414
                  LINES: 27->1|32->1|33->2|33->2|33->2|35->4|46->15|46->15|46->15|47->16|47->16|47->16|47->16|47->16|47->16|47->16|49->18|51->20|72->41|72->41|72->41|73->42|73->42|73->42|73->42|73->42|73->42|73->42|75->44|76->45|84->53|84->53|84->53|85->54|85->54|85->54|85->54|85->54|85->54|85->54|87->56|88->57|131->100|131->100|131->100|132->101|132->101|132->101|132->101|132->101|132->101|132->101|134->103|135->104|149->118|151->120|152->121|152->121|154->123|154->123|154->123|155->124|159->128|159->128|160->129|160->129|161->130|167->136|167->136|169->138|169->138|173->142|173->142|176->145|182->151|182->151|185->154|185->154|187->156|187->156|187->156|189->158|190->159|190->159|191->160|191->160|191->160|192->161|194->163|194->163|195->164|195->164|199->168|199->168|200->169|206->175|206->175|207->176|207->176|208->177|209->178|209->178|212->181|212->181|214->183|221->190|221->190|222->191|224->193|224->193|224->193|224->193|226->195|231->200|231->200|232->201|232->201|232->201|233->202|241->210|241->210|242->211|242->211|244->213|247->216|247->216|248->217|248->217|248->217|249->218|257->226|257->226|258->227|258->227|260->229|262->231|262->231|263->232|270->239|270->239|271->240|273->242|273->242|274->243|280->249|280->249|281->250|281->250|282->251|282->251|282->251|283->252|283->252|285->254|285->254|288->257|288->257|288->257|290->259|298->267|298->267|299->268|300->269|300->269|305->274|305->274|307->276|310->279|310->279|310->279|310->279|312->281|317->286|317->286|318->287|318->287|318->287|319->288|327->296|327->296|328->297|328->297|330->299|333->302|333->302|334->303|334->303|334->303|335->304|343->312|343->312|344->313|344->313|346->315|349->318|349->318|350->319|350->319|350->319|351->320|359->328|359->328|360->329|360->329|364->333|365->334|365->334|366->335|367->336|367->336|378->347|378->347|379->348|381->350|381->350|383->352|393->362|393->362|394->363|394->363|396->365|397->366|397->366|398->367|398->367|401->370|401->370|403->372|403->372|405->374|405->374|408->377|408->377|412->381
                  -- GENERATED --
              */
          