
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object commonPSFDispositionPage_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class commonPSFDispositionPage extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template16[String,Long,Long,Long,Long,String,String,String,Customer,Vehicle,WyzUser,Service,List[SMSTemplate],List[String],List[String],Html,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(oem:String,typeOfPSF:Long,dispositionHistory:Long,interactionid:Long,uniqueid:Long,dealercode:String,dealerName:String,userName:String,customerData:Customer,vehicleData:Vehicle,userData:WyzUser,latestService:Service,smsTemplates:List[SMSTemplate],complaintOFCust:List[String],statesList:List[String],psfDayPage:Html):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.319*/("""

"""),_display_(/*3.2*/mainPageCRE("AutoSherpaCRM",userName,dealerName,dealercode,oem)/*3.65*/ {_display_(Seq[Any](format.raw/*3.67*/("""
	
	"""),format.raw/*5.2*/("""<style>
	.ColorGreen"""),format.raw/*6.13*/("""{"""),format.raw/*6.14*/("""
		"""),format.raw/*7.3*/("""background-color:green;
	"""),format.raw/*8.2*/("""}"""),format.raw/*8.3*/("""
	
	"""),format.raw/*10.2*/(""".ColorRed"""),format.raw/*10.11*/("""{"""),format.raw/*10.12*/("""
		"""),format.raw/*11.3*/("""background-color:red;
	"""),format.raw/*12.2*/("""}"""),format.raw/*12.3*/("""
"""),format.raw/*13.1*/("""</style>

"""),_display_(/*15.2*/helper/*15.8*/.form(action=routes.PSFController.postCommonPSFDispositionPage())/*15.73*/{_display_(Seq[Any](format.raw/*15.74*/("""
"""),format.raw/*16.1*/("""<link rel="stylesheet" href=""""),_display_(/*16.31*/routes/*16.37*/.Assets.at("css/cre.css")),format.raw/*16.62*/("""">
<input type="hidden" id="pFlag" name="pFlag" value=""""),_display_(/*17.54*/{if(customerData.getPreferredAdress()!=null){customerData.getPreferredAdress().getAddressType()}}),format.raw/*17.151*/("""">
<input type="hidden" name="customer_Id" id="customer_Id" value=""""),_display_(/*18.66*/{customerData.getId()}),format.raw/*18.88*/("""">
<input type="hidden" id="wyzUser_Id" name="wyzUser_Id" value=""""),_display_(/*19.64*/{userData.getId()}),format.raw/*19.82*/("""">
<input type="hidden" id="vehical_Id" name="vehicalId" value=""""),_display_(/*20.63*/{vehicleData.getVehicle_id()}),format.raw/*20.92*/("""">
<input type="hidden" name="uniqueidForCallSync" value=""""),_display_(/*21.57*/uniqueid),format.raw/*21.65*/("""">
<input type="hidden" id="workshopIdis" value=""""),_display_(/*22.48*/{if(latestService.getWorkshop()!=null){latestService.getWorkshop().getId()}}),format.raw/*22.124*/("""">
<input type="hidden" id="location_Id" value=""""),_display_(/*23.47*/{userData.getLocation().getCityId()}),format.raw/*23.83*/("""">
<input type="hidden" name="interactionid" value='"""),_display_(/*24.51*/interactionid),format.raw/*24.64*/("""'>
<input type="hidden" name="dispositionHistory" value='"""),_display_(/*25.56*/dispositionHistory),format.raw/*25.74*/("""'>
<input type="hidden" name="typeOfPSF" value='"""),_display_(/*26.47*/typeOfPSF),format.raw/*26.56*/("""'>
<input type="hidden" name="oem" value='"""),_display_(/*27.41*/oem),format.raw/*27.44*/("""'>



<div class="row" style="text-transform:uppercase;">
<!-- 4User div Starts -->
	<div class="col-md-4">
			  <div id="panelDemo10" class="panel panel-info custinfoData">
      <div class="panel-heading" style="text-align:center; padding: 5px;"> <b><i class="fa fa-user"></i>&nbsp;
        
        """),_display_(/*37.10*/{if(customerData.getCustomerName()!=null){
        
        customerData.getCustomerName();
        
        }else{
        
        customerData.getConcatinatedName();							
        
        }}),format.raw/*45.11*/(""" """),format.raw/*45.12*/("""</b><br />
        <b><i class="fa fa-user-circle">
		<input type="text" id="driverIdUpdate" style="border:none;background-color:#1797be;" value=""""),_display_(/*47.96*/{if(customerData.getUserDriver()!=null){
        
        customerData.getUserDriver();
        
        }}),format.raw/*51.11*/("""" readonly></i>
         </b></div>
      <div class="panel-body">
        <div class="row">
          <div class="col-sm-6 phoneselect">
            <div class="col-xs-3">
             <!--  <input type="button"  class="btn btn-primary" value=""""),_display_(/*57.74*/{customerData.getPreferredPhone().getPhoneNumber()}),format.raw/*57.125*/("""" id="ddl_phone_no" name="ddl_phone_no" style="cursor:default ;width: 150px;margin-left: -137px;font-size: medium;"> -->
			 <select class="btn btn-primary" id="ddl_phone_no" name="ddl_phone_no" style="cursor:default ;width: 150px;margin-left: -137px;font-size: medium;">
									"""),_display_(/*59.11*/for( phoneList <- customerData.getPhones()) yield /*59.54*/{_display_(Seq[Any](format.raw/*59.55*/("""
											
									"""),_display_(/*61.11*/{if(phoneList.isIsPreferredPhone ==true){
									
									if(phoneList.getPhoneNumber()!=null){
									<option value={phoneList.getPhoneNumber()} selected="selected">{phoneList.getPhoneNumber()}</option>
									}}else{ 
									
										if(phoneList.getPhoneNumber()!=null){
										<option value={phoneList.getPhoneNumber()} >{phoneList.getPhoneNumber()}</option>
										}}}),format.raw/*69.14*/("""
									""")))}),format.raw/*70.11*/("""						
								 """),format.raw/*71.10*/("""</select>
            </div>
            <button type="button" class="btn btn-success phoneinfobtn"  data-target="#addBtn" data-toggle="modal" data-backdrop="static" data-keyboard="false"><i class="fa fa-pencil-square-o fa-lg"></i></button>
            
<button type="button" style="Display:none" id="DNDbtn" class="btn btn-danger">DND</button>			
            <!--<button type="button" class="btn btn-success test" id="btnDnd" data-target="#DNDConfirm" data-toggle="modal" data-backdrop="static" data-keyboard="false">DND</button>--> 
            
          </div>
          <div class="col-sm-12" style="margin-top: 10px;">
            <div class="col-xs-4">
              <input type="hidden" id="isCallinitaited" name="isCallinitaited" value="NotDailed">
              <input type="hidden" id="pref_number_callini" value=""""),_display_(/*82.69*/{customerData.getPreferredPhone().getPhone_Id()}),format.raw/*82.117*/("""">
              <button type="button" class="btn btn-primary btn-lg" onclick ="callfunctionFromPage('"""),_display_(/*83.101*/{customerData.getPreferredPhone().getPhone_Id()}),format.raw/*83.149*/("""','"""),_display_(/*83.153*/uniqueid),format.raw/*83.161*/("""','"""),_display_(/*83.165*/customerData/*83.177*/.getId()),format.raw/*83.185*/("""');"  style="border-radius: 29px;" ><i class="fa fa-phone-square fa-2x" aria-hidden="true" title="Make Call" ></i></button>
            </div>
            <div class="col-xs-4">
              <button type="button" class="btn btn-primary btn-lg " style="border-radius: 29px;"><i class="fa fa-envelope-square fa-2x" aria-hidden="true"></i></button>
            </div>
            <div class="col-xs-4">
              <button type="button" data-toggle="modal" data-target="#smsPopuId" data-backdrop="static" data-keyboard="false" class="btn btn-primary  btn-lg" style="border-radius: 29px;"><i class="fa fa-comment fa-2x" title="SMS" ></i></button>
            </div>
          </div>
          <div class="col-sm-12" style="margin-top: 6px;">
            <div class="row">
              <div class="col-xs-12" > <span><b><u>Email:</u>&nbsp; </b></span> <span style="font-size:15px" id="ddl_email"> """),_display_(/*94.127*/{if(customerData.getPreferredEmail()!=null){
                customerData.getPreferredEmail().getEmailAddress()
                }}),format.raw/*96.19*/(""" """),format.raw/*96.20*/("""</span> </div>
            </div>
            <div class="row">
              <div class="col-xs-6"> <span><b><u>DOA:</u>&nbsp; </b></span> <span>"""),_display_(/*99.84*/{customerData.getAnniversaryDateStr()}),format.raw/*99.122*/("""</span> </div>
              <div class="col-xs-6" > <span><b><u>DOB:</u>&nbsp; </b></span> <span>"""),_display_(/*100.85*/{customerData.getDobStr()}),format.raw/*100.111*/("""</span> </div>
            </div>
          </div>
          <div class="col-sm-12">
            <div class="row">
              <div class="col-xs-12"> <span><b><u>Address:</u>&nbsp; </b></span> <span class="addressBlog" id="prefAdressUpdate"> """),_display_(/*105.132*/{if(customerData.getPreferredAdress()!=null){
                if(customerData.getPreferredAdress().getConcatenatedAdress()!=null){
                customerData.getPreferredAdress().getConcatenatedAdress();
                }else{
                if(customerData.getConcatinatedPrefAddree()!=null){
                customerData.getConcatinatedPrefAddree();
                }    
                }
				}}),format.raw/*113.7*/(""" """),format.raw/*113.8*/("""</span> </div>
            </div>
          </div>
          <div class="col-xs-12">
            <div class="row">
              <div class="col-xs-6"> <b><u>Pref Time:</u>&nbsp;</b><span>"""),_display_(/*118.75*/{customerData.getPreferred_time_start()}),format.raw/*118.115*/(""" """),format.raw/*118.116*/("""To """),_display_(/*118.120*/{customerData.getPreferred_time_end()}),format.raw/*118.158*/("""</span> </div>
              <div class="col-xs-6"> <b><u>Pref Contact:</u>&nbsp;</b> <span>"""),_display_(/*119.79*/{customerData.getMode_of_contact()}),format.raw/*119.114*/("""</span> </div>
            </div>
          </div>
		  <div class="col-xs-12">
            <div class="row">
              <div class="col-xs-12"> <b><u>Pref Day:</u>&nbsp;</b> <span>"""),_display_(/*124.76*/{customerData.getPreferred_day()}),format.raw/*124.109*/("""</span> </div>
              
            </div>
          </div>
          <!--button-->
          <div class="col-sm-12">
            <div class="row">
              <div class="col-sm-6"> """),_display_(/*131.39*/if(customerData.getSegment()!=null)/*131.74*/{_display_(Seq[Any](format.raw/*131.75*/("""
                
                """),_display_(/*133.18*/for(segmentIterate <- customerData.getSegment()) yield /*133.66*/{_display_(Seq[Any](format.raw/*133.67*/("""
                """),format.raw/*134.17*/("""<button type="button" class="btn btn-info btn-flat btn-block loya" style="cursor:default;"><b style="font-size:15px"> """),_display_(/*134.136*/if(segmentIterate.getName()!="" && segmentIterate.getName()!=null)/*134.202*/{_display_(Seq[Any](format.raw/*134.203*/("""
                """),_display_(/*135.18*/segmentIterate/*135.32*/.getName()),format.raw/*135.42*/("""
                """)))}/*136.18*/else/*136.22*/{_display_(Seq[Any](format.raw/*136.23*/("""
                """),format.raw/*137.17*/("""NA
                """)))}),format.raw/*138.18*/(""" """),format.raw/*138.19*/("""</b></button>
                """)))}),format.raw/*139.18*/("""
                """)))}),format.raw/*140.18*/("""
				"""),format.raw/*141.5*/("""</div>
            </div>
          </div>
        </div>
      </div>
    </div>
			   </div>
		  <div class="col-md-8" style="padding: 0px;">
							<div class="col-sm-4" style="padding: 0px;">
			<!-- START panel-->
					<div id="panelDemo10" class="panel panel-info">
					<div class="panel-heading"><b>VEHICLE&nbsp;INFO</b><img src=""""),_display_(/*152.68*/routes/*152.74*/.Assets.at("images/car.png")),format.raw/*152.102*/("""" class="pull-right" style="width:28px;"></img></div>
						<div class="panel-body" style="height: 110px;overflow: auto;padding: 5px;">
						"""),_display_(/*154.8*/if(vehicleData!=null)/*154.29*/{_display_(Seq[Any](format.raw/*154.30*/("""
		
						
						
						"""),format.raw/*158.7*/("""<span><b>"""),_display_(/*158.17*/{vehicleData.getVehicleRegNo()}),format.raw/*158.48*/("""</b></span>&nbsp;|&nbsp;<span><b>"""),_display_(/*158.82*/{vehicleData.getModel()}),format.raw/*158.106*/("""</b></span></br>
						<span><b>"""),_display_(/*159.17*/{vehicleData.getVariant()}),format.raw/*159.43*/("""</b>&nbsp;|&nbsp;<b>"""),_display_(/*159.64*/{vehicleData.getColor()}),format.raw/*159.88*/("""</b></span></br>
						<span>Mileage(Kms):&nbsp;<b>"""),_display_(/*160.36*/{vehicleData.getOdometerReading()}),format.raw/*160.70*/("""</b></span></br>
						<span>Sale&nbsp;Date:&nbsp;<b class="servi" style="background-color: #f05050; color: #fff;border-radius: 3px;   font-size: 13px;border-color: transparent;">"""),_display_(/*161.164*/{vehicleData.getSaleDateStr()}),format.raw/*161.194*/("""</b></b></span>
										
						
				""")))}),format.raw/*164.6*/("""
						"""),format.raw/*165.7*/("""</div>
									</div>
									<!-- END panel-->
							</div>
<div class="col-sm-4" style="padding: 0px;">
<!-- START panel-->
<div id="panelDemo10" class="panel panel-info">
<div class="panel-heading specialoffer"><b>NEXT&nbsp;SERVICE&nbsp;DUE</b><img src=""""),_display_(/*172.85*/routes/*172.91*/.Assets.at("images/services1.png")),format.raw/*172.125*/("""" class="pull-right" style="width:28px;"></img></div>
<div class="panel-body" style="height: 110px;overflow-x:hidden;padding: 5px;">
<span><b class="servi" style="background-color: #f05050; color: #fff;border-radius: 3px;font-size: 13px;border-color: transparent;">Null</b>&nbsp;|&nbsp;<b>"""),_display_(/*174.158*/{vehicleData.getNextServicetype()}),format.raw/*174.192*/("""</b></span><br>
<span>Forecast&nbsp;Logic: &nbsp;<b>Null</b></span><br>
<span>Avg&nbsp;Running/Month(Kms):&nbsp;<b>Null</b></span></br>
<span>No&nbsp;Show(Months):&nbsp;<b>Null</b></span>
</div>
</div>
</div>
							
<div class="col-sm-4" style="padding: 0px;">
<!-- START panel-->
<div id="panelDemo10" class="panel panel-info">
<div class="panel-heading"><b>LAST&nbsp;SERVICE&nbsp;STATUS</b><img src=""""),_display_(/*185.75*/routes/*185.81*/.Assets.at("images/car-insurance.png")),format.raw/*185.119*/("""" class="pull-right" style="width:28px;"></img></div>
<div class="panel-body" style="height: 110px;overflow: auto;padding: 5px;">
<span><b>"""),_display_(/*187.11*/{vehicleData.getLastServiceDate()}),format.raw/*187.45*/("""</b> :&nbsp;|&nbsp;<b>"""),_display_(/*187.68*/{vehicleData.getLastServiceType()}),format.raw/*187.102*/("""</b></span><br>
<span>Workshop:&nbsp;<b>
"""),_display_(/*189.2*/{if(latestService.getWorkshop()!=null){
latestService.getWorkshop().getWorkshopName()
}
}),format.raw/*192.2*/("""

"""),format.raw/*194.1*/("""</b></span><br>
<span>PSF Status :&nbsp;<b>"""),_display_(/*195.29*/{latestService.getLastPSFStatus()}),format.raw/*195.63*/("""</b></span><br>
<span>PSF Date :&nbsp;<b></b></span>
</div>

</div>
<!-- END panel-->
</div>

<div class="col-sm-4" style="padding: 0px;">
<!-- START panel-->
<div id="panelDemo10" class="panel panel-info">
<div class="panel-heading"><b>INSURANCE</b><img src=""""),_display_(/*206.55*/routes/*206.61*/.Assets.at("images/truck.png")),format.raw/*206.91*/("""" class="pull-right" style="width:28px;"></img></div>
<div class="panel-body" style="height: 110px;overflow: auto;padding: 5px;">

		

<span>Expires&nbsp;On:&nbsp;<b class="servi" style="background-color: #f05050;color: #fff;border-radius: 3px;   font-size: 13px;border-color: transparent;"></b></b></span><br>
<span style="font-size:13px">New&nbsp;India&nbsp;Assurance</span><br>
<span style="font-size:13px">Policy&nbsp;No:&nbsp;<b></b></span><br>
<span style="font-size:13px">Premium(Rs.):&nbsp;<b></b></span>


</div>

</div>
<!-- END panel-->
</div>
<div class="col-sm-4" style="padding: 0px;">
<!-- START panel-->
<div id="panelDemo10" class="panel panel-info">
<div class="panel-heading"><b>OTHER&nbsp;UPSELL</b><img src=""""),_display_(/*225.63*/routes/*225.69*/.Assets.at("images/warranty-certificate.png")),format.raw/*225.114*/("""" class="pull-right" style="width:28px;"></img></div>
<div class="panel-body" style="height: 110px;overflow: auto;padding: 5px;">
<span>EW&nbsp;Due:&nbsp;<b class="servi" style="background-color: #f05050;color: #fff;border-radius: 3px; font-size: 13px;
border-color: transparent;">"""),_display_(/*228.30*/{vehicleData.getExtendedWarentyDue()}),format.raw/*228.67*/("""</b></span><br>
<span>Warranty&nbsp;Due:&nbsp;<b class="servi" style="font-size: 13px;"></b></span><br>
<span style="font-size: 13px;">EXCHANGE:&nbsp;<b>"""),_display_(/*230.51*/{vehicleData.getExchange()}),format.raw/*230.78*/("""</b></span></br>
<span>&nbsp;</span>


</div>

</div>
<!-- END panel-->
</div>
<div class="col-sm-4" style="padding: 0px;">
<!-- START panel-->
<div id="panelDemo10" class="panel panel-info">
<div class="panel-heading"><b>COMPLAINTS</b><img src=""""),_display_(/*242.56*/routes/*242.62*/.Assets.at("images/businessman.png")),format.raw/*242.98*/("""" class="pull-right" style="width:28px;"></img></div>
<div class="panel-body" style="height: 110px;overflow: auto;padding: 5px;">
<span>OPEN:<b class="servi" style="background-color: #f05050; margin-left: 15px; color: #fff;border-radius: 3px; font-size: 17px;    border-color: transparent;">"""),_display_(/*244.163*/complaintOFCust/*244.178*/.get(0)),format.raw/*244.185*/("""</b></span><br>
<span> CLOSED:<b class="servi" style="background-color: #27c24c; color: #fff;border-radius: 3px;     font-size: 13px;
border-color: transparent;">"""),_display_(/*246.30*/complaintOFCust/*246.45*/.get(1)),format.raw/*246.52*/("""</b></span><br>
<span style="font-size: 14PX;">Last&nbsp;Raised&nbsp;On:&nbsp;<b>"""),_display_(/*247.67*/complaintOFCust/*247.82*/.get(2)),format.raw/*247.89*/("""</b></span><br>
<span style="font-size: 13px;">RESOLVED&nbsp;BY:&nbsp;<b>"""),_display_(/*248.59*/complaintOFCust/*248.74*/.get(3)),format.raw/*248.81*/("""</b>&nbsp;</span>

</div>

</div>
<!-- END panel-->
</div>
			</div>
  <div class="modal fade" id="addBtn" role="dialog">
								<div class="modal-dialog modal-lg">

									<!-- Modal content-->
									<div class="modal-content">
										<div class="modal-header" style="padding:1px;text-align:center;color:red;">
											<button type="button" class="close" data-dismiss="modal" class="crossBtnCorner"><img src=""""),_display_(/*262.103*/routes/*262.109*/.Assets.at("images/close1.png")),format.raw/*262.140*/(""""></button>
											<h4 class="modal-title"><b>UPDATE CUSTOMER INFORMATION</b></h4>
										</div>
										
								 <div class="modal-body">
					 <div class="row">
						  <div class="col-sm-12" style="margin-top: 5px;">
								<div class="col-sm-3">
								  <lable><b>Owner:</b></lable>
								  <input type="text" class="form-control" id="customerNameEdit" value=""""),_display_(/*271.81*/customerData/*271.93*/.getCustomerName()),format.raw/*271.111*/("""" name="" readonly>
								  <br/>
								</div>
								
								<div class="col-sm-3">
								  <lable><b>User/Driver:</b></lable>
								  <input type="text" class="form-control textOnlyAccepted" id="driverNameEdit" value=""""),_display_(/*277.96*/customerData/*277.108*/.getUserDriver()),format.raw/*277.124*/("""" name="">
								  <br/>
								</div>
								
								
							  </div>
							  </br>
							  <div class="col-sm-12">
								<div class="col-sm-3">
								  <lable><b>Permanent Address:</b></lable>
								  <textarea class="form-control permanentAddress" rows="2" id="permanentAddress" name="permanentAddress" readonly >"""),_display_(/*287.125*/{if(customerData.getPermanentAddressData()!=null){
	  
	  customerData.getPermanentAddressData().getAddressLine1();
	  
	  }}),format.raw/*291.6*/("""_
	  """),_display_(/*292.5*/{if(customerData.getPermanentAddressData()!=null){
	  
	  customerData.getPermanentAddressData().getAddressLine2();
	  
	  }}),format.raw/*296.6*/("""_
	  """),_display_(/*297.5*/{if(customerData.getPermanentAddressData()!=null){
	  
	  customerData.getPermanentAddressData().getAddressLine3();
	  
	  }}),format.raw/*301.6*/("""</textarea>
								  <br/>
								</div>
								<div class="col-sm-1">
								  <button type="button" class="btn btn-success peditAddressmodal" data-toggle="modal" data-target="#driver_modal1" style="margin-top: 25px;">Add</button>
								</div>
								<div class="col-md-3">
								  <lable><b>Residence Address:</b></lable>
								  <textarea class="form-control" rows="2" id="residenceAddress" name="residenceAddress" readonly>"""),_display_(/*309.107*/{if(customerData.getResidenceAddressData()!=null){
	  
	  customerData.getResidenceAddressData().getAddressLine1();
	  
	  }}),format.raw/*313.6*/("""_
	  """),_display_(/*314.5*/{if(customerData.getResidenceAddressData()!=null){
	  
	  customerData.getResidenceAddressData().getAddressLine2();
	  
	  }}),format.raw/*318.6*/("""_
	  """),_display_(/*319.5*/{if(customerData.getResidenceAddressData()!=null){
	  
	  customerData.getResidenceAddressData().getAddressLine3();
	  
	  }}),format.raw/*323.6*/("""</textarea>
								</div>
								<div class="col-md-1">
								  <button type="button" class="btn btn-success reditAddressmodal"  data-toggle="modal" data-target="#driver_modal2" style="margin-top: 25px;">Add</button>
								</div>
								<div class="col-md-3">
								  <lable><b>Office Address:</b></lable>
								  <textarea class="form-control" rows="2" id="officeAddress" name="officeAddress" readonly >"""),_display_(/*330.102*/{if(customerData.getOfficeAddressData()!=null){
	  
	  customerData.getOfficeAddressData().getAddressLine1();
	  
	  }}),format.raw/*334.6*/("""_
	  """),_display_(/*335.5*/{if(customerData.getOfficeAddressData()!=null){
	  
	  customerData.getOfficeAddressData().getAddressLine2();
	  
	  }}),format.raw/*339.6*/("""_
	  """),_display_(/*340.5*/{if(customerData.getOfficeAddressData()!=null){
	  
	  customerData.getOfficeAddressData().getAddressLine3();
	  
	  }}),format.raw/*344.6*/("""</textarea>
								</div>
								<div class="col-md-1">
								  <button type="button" class="btn btn-success oeditAddressmodal" data-toggle="modal" data-target="#driver_modal3" style="margin-top: 25px;">Add</button>
								</div>
							  </div>
							  <hr>
							  <div class="col-sm-12">
								<p style="color:Red;"><b>PREFERRED COMMUNICATION ADDRESS</b></p>
								<div class="col-md-4">
								  <input type="radio"  id="checkbox1" name="ADDRESS" value="0">
								  Permanent Address</div>
								<div class="col-md-4">
								  <input type="radio"  id="checkbox2" name="ADDRESS" value="2" >
								  Residence Address</div>
								<div class="col-md-4">
								  <input type="radio"  id="checkbox3" name="ADDRESS" value="1" >
								  Office Address</div>
							  </div>
							   <hr>
							  <div class="col-sm-12" style="margin-top: 5px;">
								<div class="col-sm-3">
								  <lable><b>Preferred Email:</b></lable>
								  <select class="form-control" id="email" name="email">
									"""),_display_(/*368.11*/for( emailList <- customerData.getEmails()) yield /*368.54*/{_display_(Seq[Any](format.raw/*368.55*/("""
											
									"""),_display_(/*370.11*/{if(emailList.isIsPreferredEmail() ==true)
									
									<option value={emailList.getEmailAddress()} selected="selected">{emailList.getEmailAddress()}</option>
									else 
										<option value={emailList.getEmailAddress()} >{emailList.getEmailAddress()}</option>
										}),format.raw/*375.12*/("""
									""")))}),format.raw/*376.11*/("""						
								 """),format.raw/*377.10*/("""</select>
								</div>
								<div class="col-xs-1"><br />  
								  <button type="button" class="btn btn-success oeditEmailmodal" data-toggle="modal" data-target="#driver_modal4">Add</button>
								</div>
								<div class="col-sm-3">
								  <lable><b>Date OF Birth:</b></lable>
								  <input type="text" class="datepicMYDropDown form-control" id="dob" value=""""),_display_(/*384.86*/{customerData.getDob()}),format.raw/*384.109*/("""" name="dob" readonly>
								</div>
								<div class="col-md-3">
								  <lable><b>Date of Anniversary</b></lable>
								  <input type="text" class="datepicMYDropDown form-control" id="anniversary_date" value=""""),_display_(/*388.99*/{customerData.getAnniversary_date()}),format.raw/*388.135*/("""" name="anniversary_date" readonly>
								</div>
							  </div>
							   <hr>
							  <div class="col-sm-12" style="margin-top: 5px;">
								<h5 style="color:Red;"><b>MANAGE PREFERENCES</b></h5>
								<div class="col-sm-3">
								  <lable><b>Preferred Contact #:</b></lable>
								   <select class="form-control" id="preffered_contact_num" name="preffered_contact_num">
									"""),_display_(/*397.11*/for( phoneList <- customerData.getPhones()) yield /*397.54*/{_display_(Seq[Any](format.raw/*397.55*/("""
											
									"""),_display_(/*399.11*/{if(phoneList.isIsPreferredPhone ==true)
									
									<option value={phoneList.phone_Id.toString()} selected="selected">{phoneList.getPhoneNumber()}</option>
									else 
										<option value={phoneList.phone_Id.toString()} >{phoneList.getPhoneNumber()}</option>
										}),format.raw/*404.12*/("""
									""")))}),format.raw/*405.11*/("""						
								 """),format.raw/*406.10*/("""</select>
								  
								</div>
								<div class="col-sm-1">
								 <input type="button"  class="btn btn-success"  id="tanniversary_edit" data-toggle="modal" data-target="#myModalAddPhone"   style="margin-top: 20px;"  value="Add">
										   </div>
								<div class="col-md-3">
								  <lable><b>Preferred Mode of Contact:</b></lable>
								  <div class="col-sm-12" id="modeOfCon"  style="display:none">
									<select class="form-control" id="preffered_mode_contact" name="preffered_mode_contact" multiple="multiple" >
									  <option value="Email">Email</option>
									  <option value="Phone">Phone</option>
									  <option value="SMS">SMS</option>
									</select>
								  </div>
								  <input type="text" class="form-control" id="tpreffered_mode_contact"  value=""""),_display_(/*421.89*/{customerData.getMode_of_contact()}),format.raw/*421.124*/("""" readonly>
								</div>
								<div class="col-md-1">
								  <input type="button"  class="btn btn-success"  id="tpreffered_mode_contact_edit" style="margin-top: 20px;" value="Add">
								  
								</div>
								<div class="col-md-3">
								  <lable><b>Preferred Day to Contact:</b></lable>
								  <div class="col-sm-12" id="daysWeek" style="display:none">
									<select class="form-control" id="preffered_day_contact" name="preffered_day_contact" multiple="multiple">
									  <option value="Sunday">Sunday</option>
									  <option value="Monday">Monday</option>
									  <option value="Tuesday">Tuesday</option>
									  <option value="Wednesday">Wednesday</option>
									  <option value="Thursday">Thursday</option>
									  <option value="Friday">Friday</option>
									  <option value="Saturday">Saturday</option>
									</select>
								  </div>
								  <input type="text" class="form-control" id="tpreffered_day_contact"  value=""""),_display_(/*440.88*/{customerData.getPreferred_day()}),format.raw/*440.121*/("""" readonly >
								</div>
								<div class="col-md-1">
								  <input type="button"  class="btn btn-success"  id="tpreffered_day_contact_edit" style="margin-top: 20px;" value="Add">
								</div>
							  </div>
							  <br>
							   <hr>
							  <div class="col-sm-12" style="margin-top: 5px;">
								<div class="col-sm-3">
								  <lable><b>Preferred Time Of Contact(Start Time):</b></lable>
								  <input type="text" class="form-control single-input" id="dateStarttime" name="dateStarttime" value=""""),_display_(/*451.112*/{customerData.getPreferred_time_start()}),format.raw/*451.152*/("""" readonly>
								</div>
								<div class="col-md-3">
								  <lable><b>Preferred Time Of Contact(End Time):</b></lable>
								  <input type="text" class="form-control single-input" id="dateEndtime" name="dateEndtime" value=""""),_display_(/*455.108*/{customerData.getPreferred_time_end()}),format.raw/*455.146*/("""" readonly>
								</div>
								<div class="col-sm-6">
								  <lable><b>Comments:(250 characters only)</b></lable>
								  <textarea type="text" class="form-control" id="custEditComment" maxlength="250" rows="2" name="custEditComment" value=""""),_display_(/*459.131*/{customerData.getComments()}),format.raw/*459.159*/("""">"""),_display_(/*459.162*/{customerData.getComments()}),format.raw/*459.190*/("""</textarea>
								</div>
							  </div>
							 
							</div>
				</div>

										<div class="modal-footer">
											<button type="button" id="pn_save" class="btn btn-success" onClick="ajaxCallForAddcustomerinfo();" data-dismiss="modal">Save</button>

										</div>
									</div>

								</div>
							</div>
		  
			  
		  <!-- 4User Div End -->
   


</div>



<!--sashi model-->
<div class="modal fade" id="driver_modal1" role="dialog">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" class="crossBtnCorner"><img src=""""),_display_(/*489.94*/routes/*489.100*/.Assets.at("images/close1.png")),format.raw/*489.131*/(""""></button>
		
		<p class="modal-title" style="tex-align:center"><b>Permanent Address</b></p>
	  </div>
	  <div class="modal-body">
		<table>
		  <tr>
			<td>Address Line1:</td>
			<td><textarea class="form-control paddr_line1" rows="1"   placeholder="Please enter Company Name/Flat No & Building name." ></textarea>
			  <br/></td>
		 
			<td>Address Line2:</td>
			<td><textarea class="form-control paddr_line2" rows="1"   placeholder="Please enter Road number/ Road name."></textarea>
			  <br/></td>
			  <td>Address Line3:</td>
			<td><textarea class="form-control paddr_line3" rows="1"  placeholder="Please enter Road number/ Road name."></textarea>
			  <br/></td>
		  </tr>
		  <tr>
			<td>City</td>
			<td><input type="text" class="form-control paddr_line4 textOnlyAccepted" rows="1" value=""""),_display_(/*509.94*/{if( customerData.getPermanentAddressData()!=null){ customerData.getPermanentAddressData().getCity()} }),format.raw/*509.197*/(""""  placeholder="Please enter City" /><br/></td>
		  
			<td>State</td>
			<td><input type="text"  class="form-control paddr_line5 textOnlyAccepted" rows="1"  value=""""),_display_(/*512.96*/{if( customerData.getPermanentAddressData()!=null){ customerData.getPermanentAddressData().getState()} }),format.raw/*512.200*/("""" placeholder="Please enter State"/><br/></td>
		  </tr>

		  <tr>
			<td>PinCode</td>			
			<td><input type="text"   maxlength="6" class="form-control paddr_line6 numberOnly" rows="1" value=""""),_display_(/*517.104*/{if( customerData.getResidenceAddressData()!=null){ customerData.getResidenceAddressData().getPincode()}else{0} }),format.raw/*517.217*/(""""  placeholder="Please enter PinCode"/><br/></td>
		  
			<td>Country</td>
			
			<td><input type="text"  class="form-control paddr_line7" rows="1" value="India"  readonly><br/></td>
		  </tr>
		</table>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-primary paddr_submit" data-dismiss="modal">Submit</button>
	  </div>
	</div>
  </div>
</div>
<div class="modal fade" id="driver_modal2" role="dialog">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" class="crossBtnCorner"><img src=""""),_display_(/*535.94*/routes/*535.100*/.Assets.at("images/close1.png")),format.raw/*535.131*/(""""></button>
		
		<p class="modal-title" style="tex-align:center"><b>Residence Address</b></p>
	  </div>
	  <div class="modal-body">
		<table>
		  <tr>
			<td>Address Line1:</td>
			<td><textarea class="form-control raddr_line1" rows="1"  name="raddressline1" placeholder="Please enter Company Name/Flat No & Building name." ></textarea>
			  <br/></td>
		
			<td>Address Line2:</td>
			<td><textarea class="form-control raddr_line2" rows="1"  name="raddressline2" placeholder="Please enter Road number/ Road name."></textarea>
			  <br/></td>
			  
			 <td>Address Line3:</td>
			<td><textarea class="form-control raddr_line3" rows="1"  name="raddressline3" placeholder="Please enter Road number/ Road name."></textarea>
			  <br/></td> 
		  </tr>
		   <tr>
			<td>City</td>
			<td><input type="text" class="form-control raddr_line4 textOnlyAccepted" rows="1"   value=""""),_display_(/*556.96*/{if( customerData.getResidenceAddressData()!=null){ customerData.getResidenceAddressData().getCity()} }),format.raw/*556.199*/("""" placeholder="Please enter City" /><br/></td>
		  
			<td>State</td>
			<td><input type="text"  class="form-control raddr_line5 textOnlyAccepted" rows="1"  value=""""),_display_(/*559.96*/{if( customerData.getResidenceAddressData()!=null){ customerData.getResidenceAddressData().getState()} }),format.raw/*559.200*/("""" placeholder="Please enter State"/><br/></td>
		  </tr>

		  <tr>
			<td>PinCode</td>
			<td><input type="text" maxlength="6" class="form-control raddr_line6 numberOnly" rows="1"  value=""""),_display_(/*564.103*/{if( customerData.getResidenceAddressData()!=null){ customerData.getResidenceAddressData().getPincode()}else{0} }),format.raw/*564.216*/("""" placeholder="Please enter PinCode"/><br/></td>
		  
			<td>Country</td>
			<td><input type="text"  class="form-control raddr_line7" rows="1" value="India" readonly><br/></td>
		  </tr>
		
		</table>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-primary raddr_submit" data-dismiss="modal">Submit</button>
	  </div>
	</div>
  </div>
</div>
<div class="modal fade" id="driver_modal3" role="dialog">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" class="crossBtnCorner"><img src=""""),_display_(/*582.94*/routes/*582.100*/.Assets.at("images/close1.png")),format.raw/*582.131*/(""""></button>
		<p class="modal-title" style="tex-align:center"><b>Office Address</b></p>
	  </div>
	  <div class="modal-body">
		<table>
		  <tr>
			<td>Address Line1:</td>
			<td><textarea class="form-control oaddr_line1" rows="2"  placeholder="Please enter Company Name/Flat No & Building name." name="oaddressline1">"""),_display_(/*589.148*/{if( customerData.getOfficeAddressData()!=null){ customerData.getOfficeAddressData().getAddressLine1()} }),format.raw/*589.253*/("""</textarea>
			  <br/></td>
		  
			<td>Address Line2:</td>
			<td><textarea class="form-control oaddr_line2" rows="2" placeholder="Please enter Road number/ Road name." name="oaddressline2">"""),_display_(/*593.133*/{if( customerData.getOfficeAddressData()!=null){ customerData.getOfficeAddressData().getAddressLine2()} }),format.raw/*593.238*/("""</textarea>
			  <br/></td>
			<td>Address Line3:</td>
			<td><textarea class="form-control oaddr_line3" rows="2" placeholder="Please enter Road number/ Road name." name="oaddressline3">"""),_display_(/*596.133*/{if( customerData.getOfficeAddressData()!=null){ customerData.getOfficeAddressData().getAddressLine3()} }),format.raw/*596.238*/("""</textarea>
			  <br/></td>
		  </tr>
		  <tr>
			<td>City</td>
			<td><input type="text" class="form-control oaddr_line7 textOnlyAccepted" id="paddr_line7" rows="1"  name="paddressline3" value=""""),_display_(/*601.133*/{if( customerData.getOfficeAddressData()!=null){ customerData.getOfficeAddressData().getCity()} }),format.raw/*601.230*/(""""placeholder="Please enter City" /><br/></td>
		  
			<td>State</td>
			<td><input type="text"  class="form-control oaddr_line4 textOnlyAccepted" id="paddr_line4" rows="1"  name="paddressline4" value=""""),_display_(/*604.134*/{if( customerData.getOfficeAddressData()!=null){ customerData.getOfficeAddressData().getState()} }),format.raw/*604.232*/("""" placeholder="Please enter State" /><br/></td>
		  </tr>

		  <tr>
			<td>PinCode</td>
			<td><input type="text" maxlength="6"  class="form-control oaddr_line5 numberOnly" id="paddr_line5" rows="1" value=""""),_display_(/*609.120*/{if( customerData.getResidenceAddressData()!=null){ customerData.getResidenceAddressData().getPincode()}else{0} }),format.raw/*609.233*/("""" name="paddressline5" placeholder="Please enter PinCode"/><br/></td>
		  
			<td>Country</td>
			<td><input type="text"  class="form-control oaddr_line6" rows="1" value="India" readonly><br/></td>
			
		  </tr>
		</table>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-primary oaddr_submit" data-dismiss="modal">Submit</button>
	  </div>
	</div>
  </div>
</div>

<div class="modal fade" id="driver_modal4" role="dialog">
    <div class="modal-dialog modal-xs">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" class="crossBtnCorner"><img src=""""),_display_(/*630.102*/routes/*630.108*/.Assets.at("images/close1.png")),format.raw/*630.139*/(""""></button>
          <h4 class="modal-title">ADD EMAIL</h4>
        </div>
		
		
        <div class="modal-body">
		<div class="row">
        <div class="col-xs-12">
        <div class="col-xs-4">
        <input type="text" name="myEmailNum" class="form-control email"  maxlength="30" min="5" max="30" id="myEmailNum" >
      </div>
        </div>
        </div>
		</div>
        <div class="modal-footer">
   <button type="button" class="btn btn-default" data-dismiss="modal" id="saveEmailCust">SAVE</button>
        </div>
      </div>
      
    </div>
  </div>
<div id="smallModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
		<h4 class="modal-title">Message</h4>
	  </div>
	  <div class="modal-body">
		<p>This feature is not enabled for """),_display_(/*659.39*/userName),format.raw/*659.47*/(""".<br>
		  Contact Administrator.</p>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
	  </div>
	</div>
  </div>
</div>
<div id="smallModal1" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
		<h4 class="modal-title">E-Mail</h4>
	  </div>
	  <div class="modal-body">
		<p>This feature is not enabled for """),_display_(/*676.39*/userName),format.raw/*676.47*/(""" """),format.raw/*676.48*/(""".<br>
		  Contact Administrator.</p>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
	  </div>
	</div>
  </div>
</div>


<!---bottom fixed popup----->
<div class="modal fade" id="bottonFixedId" role="dialog">
<div class="modal-dialog">

  <!-- Modal content-->
  <div class="modal-content">
	<div class="modal-header">
	  <button type="button" class="close" data-dismiss="modal" class="crossBtnCorner"><img src=""""),_display_(/*694.95*/routes/*694.101*/.Assets.at("images/close1.png")),format.raw/*694.132*/(""""></button>
	  <h4 class="modal-title">Call Script</h4>
	</div>
	<div class="modal-body">
	  <p>Hi Good Morning!. </p>
	</div>
	<div class="modal-footer">
	  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	</div>
  </div>
  
</div>
</div>

  
  
  <div class="modal fade" id="DNDConfirm" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" class="crossBtnCorner"><img src=""""),_display_(/*716.102*/routes/*716.108*/.Assets.at("images/close1.png")),format.raw/*716.139*/(""""></button>
          <h4 class="modal-title" style="text-align:center;">Do Not Disturb(DND) Tagging</h4>
        </div>
        <div class="modal-body">
          <b>Do you want to tag the customer """),_display_(/*720.47*/customerData/*720.59*/.getCustomerName()),format.raw/*720.77*/("""  """),format.raw/*720.79*/("""as �Do Not Disturb /Call� ?<br> Click �YES� to confirm</b><br><br>
		  
  <div class="radio-inline">
      <label><input type="radio" name="RadioDND" id="RadioDNDYes" value="makeDND">YES</label>
    </div>
    <div class="radio-inline">
      <label><input type="radio" name="RadioDND" id="RadioDNDNo" value="removeDND">NO</label>
    </div>
        </div>
        <div class="modal-footer" style="text-align:center;" >
         
<button type="button" class="btn btn-success" id="submitDndBtn" data-dismiss="modal">submit</button>		  
        </div>
      </div>
      
    </div>
  </div>
  
  
   <div class="modal fade" id="smsPopuId" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="text-align:center;background-color: #106A86;color: #fff;">
          <button type="button" class="close" data-dismiss="modal" class="crossBtnCorner"><img src=""""),_display_(/*745.102*/routes/*745.108*/.Assets.at("images/close1.png")),format.raw/*745.139*/(""""></button>
          <h4 class="modal-title" ><b>SMS OPTIONS<b></h4>
        </div>
        <div class="">
   <ul class="nav nav-pills nav-stacked col-md-3">
   """),_display_(/*750.5*/for(smsTemplate<-smsTemplates) yield /*750.35*/{_display_(Seq[Any](format.raw/*750.36*/("""
   		"""),format.raw/*751.6*/("""<li><a class="clssTemplType" id="typAnch_"""),_display_(/*751.48*/{smsTemplate.getSmsId()}),format.raw/*751.72*/("""" href="#tab_"""),_display_(/*751.86*/{smsTemplate.getSmsId()}),format.raw/*751.110*/("""" data-toggle="pill">"""),_display_(/*751.132*/{smsTemplate.getSmsType()}),format.raw/*751.158*/("""</a></li>
   	""")))}),format.raw/*752.6*/("""
 
"""),format.raw/*754.1*/("""</ul>
<div class="tab-content col-md-9 smstabclass">
<input type="hidden" id="smsrequestReference" value=""""),_display_(/*756.55*/{vehicleData.getVehicle_id()}),format.raw/*756.84*/("""" />
  """),_display_(/*757.4*/for(smsTemplate<-smsTemplates) yield /*757.34*/{_display_(Seq[Any](format.raw/*757.35*/("""
  	

        """),format.raw/*760.9*/("""<div class="tab-pane """),_display_(/*760.31*/if(smsTemplate.getSmsId()==1)/*760.60*/{_display_(Seq[Any](format.raw/*760.61*/("""active""")))}),format.raw/*760.68*/("""" id="tab_"""),_display_(/*760.79*/{smsTemplate.getSmsId()}),format.raw/*760.103*/("""">
        <input type="hidden" id="typ_"""),_display_(/*761.39*/{smsTemplate.getSmsId()}),format.raw/*761.63*/(""""  />
             <h4><u>"""),_display_(/*762.22*/{smsTemplate.getSmsType()}),format.raw/*762.48*/("""</u></h4>
			 <textarea name="text" rows="5" cols="45" id="txt_"""),_display_(/*763.55*/{smsTemplate.getSmsId()}),format.raw/*763.79*/("""" class="cl_"""),_display_(/*763.92*/{smsTemplate.getSmsId()}),format.raw/*763.116*/("""" disabled="disabled" style="border:none; background-color:#fff;">"""),_display_(/*763.183*/{smsTemplate.getSmsTemplate()}),format.raw/*763.213*/("""</textarea>
<br/>
		<input type="button" name="set_Value" id="ed_"""),_display_(/*765.49*/{smsTemplate.getSmsId()}),format.raw/*765.73*/("""" class="btn btn-info cmmnbtnclass"  value="Edit"  >
     

        </div>
 
 """)))}),format.raw/*770.3*/("""
 """),format.raw/*771.2*/("""</div>	
<!-- tab content -->
        </div>
        <div class="modal-footer">
          <button type="button" id="smsSendbtn" class="btn btn-success" data-dismiss="modal">SEND SMS</button>
        </div>
      </div>
    </div>
  </div>
 
  <!-- Trigger the modal with a button -->

  <!-- Modal -->
  <div class="modal fade" id="myModalForInfo" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="background-color:#1797BE;text-align:center;">
          <button type="button" class="close" data-dismiss="modal" class="crossBtnCorner"><img src=""""),_display_(/*790.102*/routes/*790.108*/.Assets.at("images/close1.png")),format.raw/*790.139*/(""""></button>
          <h4 class="modal-title heading" style="color:#fff"><b>SERVICE HISTORY -<<VEH No.>> - <<SERVICE DATE>><b> </h4>
        </div>
        <div class="modal-body">
         

   <div class="row" style="border:2px solid #1797BE;margin-right: -8px;margin-left: -6px;">
                <div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                    <tr>
                      <td><strong>CUSTOMER ID</strong></td>
                      <td class="text-primary"> 1311935638 </td>
                    </tr>
					
					<tr>
                      <td><strong>S/ADVISOR</strong></td>
                      <td class="text-primary"> </td>
                    </tr>
					<tr>
                      <td><strong>MILEAGE (Kms)</strong></td>
                      <td class="text-primary">34233</td>
                    </tr>
					  
					 <tr>
                      <td><strong>PART DISC</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
                   <tr>
                      <td><strong>PARTS BILL</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
                    </tbody>
                    </table>
                </div>
                <div class="col-md-4">
                 <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                 
					<tr>
                      <td><strong> JOBCARD No.</strong></td>
                      <td class="text-primary">JC13010045</td>
                    </tr>
				
					<tr>
                      <td><strong>TECHNICIAN</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
					<tr>
                      <td><strong>Bill No.</strong></td>
                      <td class="text-primary">BR13010474</td>
                    </tr>
					 
					
					<tr>
                      <td><strong>LAB DISC</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					 <tr>
                      <td><strong>LAB BILL</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
                   
                  </tbody>
                </table>
                </div>
                 <div class="col-md-4">
                 <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                
					<tr>
                      <td><strong>W/SHOP</strong></td>
                      <td class="text-primary">UNIV RD</td>
                    </tr>
					
					
					<tr>
                      <td><strong>S/TYPE</strong></td>
                      <td class="text-primary">FR3</td>
                    </tr>
					<tr>
                      <td><strong>Bill Date</strong></td>
                      <td class="text-primary">10/27/2015</td>
                    </tr>
					
					
					 <tr>
                      <td><strong>DISCOUNT</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
                  
					 <tr>
                      <td><strong>BILL AMOUNT</strong></td>
                      <td class="text-primary">656577</td>
                    </tr>
                  </tbody>
                </table>
                </div>
	</div>
			<div class="row" style="border:2px solid #1797BE;margin-top: 5px;margin-right: -8px;margin-left: -6px;">
	   
		
			<h4 style="text-align:center;"><b>REMARKS<b></h4>
    			<div class="col-lg-4" style="text-align: center;">
  			       <div class="form-group">
     			    <label for="">DEFECT DETAILS</label>
     			     <textarea type="" class="form-control" rows="3" id=""  readonly></textarea>
 			     </div>
		 </div> 
  		    <div class="col-lg-4" style="    text-align: center;">
  			<div class="form-group">
     			 <label for="">LABOUR DETAILS</label>
     			 <textarea type="" class="form-control" id=""  rows="3" readonly></textarea>
 			 </div>
		 </div>
       
  		<div class="col-lg-4" style="    text-align: center;">
  			<div class="form-group">
     			 <label for="">JC REMARKS</label>
     			  <textarea type="" class="form-control" id=""  rows="3" readonly></textarea>
 			 </div>
		 </div>
		 
 

</div>

<div class="row" style="border:2px solid #1797BE;    margin-right: -8px;
    margin-left: -6px;    margin-top: 5px;">
<div class="row">
                <div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                    <tr>
                      <td><strong>3rd DAY PSF</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
					
					
                    </tbody>
                    </table>
                </div>
				<div class="col-md-4" >
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                  
					<tr>
                      <td><strong>DATE</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
					
                    </tbody>
                    </table>
                </div>
				<div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                   
					
					<tr>
                      <td><strong>DONE BY</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
                    </tbody>
                    </table>
                </div>
				</div>
				
				
<div class="row">
                <div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                    <tr>
                      <td><strong>6th DAY PSF</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
					
					
                    </tbody>
                    </table>
                </div>
				<div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                  
					<tr>
                      <td><strong>DATE</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
					
                    </tbody>
                    </table>
                </div>
				<div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                   
					
					<tr>
                      <td><strong>DONE BY</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
                    </tbody>
                    </table>
                </div>
				</div>
				
<div class="row">
                <div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                    <tr>
                      <td><strong>30th DAY PSF</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
					
					
                    </tbody>
                    </table>
                </div>
				<div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                  
					<tr>
                      <td><strong>DATE</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
					
                    </tbody>
                    </table>
                </div>
				<div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                   
					
					<tr>
                      <td><strong>DONE BY</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
                    </tbody>
                    </table>
                </div>
				</div>
</div>
        </div>
      
      </div>
      
    </div>
  </div>
  
  <!-- PSF Disposition Form --> 
  <div class="row">
  <div class="col-lg-12"> 
		
					
					"""),_display_(/*1059.7*/psfDayPage),format.raw/*1059.17*/("""


				
	"""),format.raw/*1063.2*/("""</div>
	</div>
	
<!-----------INTERACTIONH HISTORY--------->
<div class="row">
<div class="col-lg-12">
	<div class="panel panel-primary">
  <div class="panel-heading" align="center"; style="background-color:#1797BE;" data-toggle="collapse" data-parent="#accordion" data-target="#collapseOne3">
		<h4 class="panel-title accordion-toggle"></h4>
		<img src=""""),_display_(/*1072.14*/routes/*1072.20*/.Assets.at("images/INTERACTION HISTORY.png")),format.raw/*1072.64*/("""" style="width:17px;"/>&nbsp;<b>INTERACTION HISTORY</b> </div>
  <div id="collapseOne3" class="panel-collapse collapse in">
		<div class="panel-body">
	  <div class="panel with-nav-tabs">
			<div class="panel-heading clearfix">
		  <div>
				<ul class="nav nav-tabs">
			  <li class="expand"><a href="#tab1primary" data-toggle="tab" onclick="callHistoryByVehicle();" >PREVIOUS COMMENTS</a></li>
			  <li class="expand"><a href="#tab2primary" data-toggle="tab" onclick="callHistoryByVehicle();">DISPOSITIONS</a></li>
			  <li class="expand"><a href="#tab3primary" data-toggle="tab" onclick="smsHistoryOfUser();">SMS</a></li>
			  <li class="expand"><a href="#tab4primary" data-toggle="tab">E-Mail</a></li>
			  <li class="expand"><a href="#tab5primary" data-toggle="tab" onclick="complaintsOFVehicle();">COMPLAINTS</a></li>
			</ul>
			  </div>
		</div>
			<div class="panel-body">
		  <div class="tab-content">
				<div class="tab-pane fade in active" id="tab1primary">
			  <div class="row">
					<div class="col-sm-6">
				  <div class="form-group">
						<table class="table table-condensed table-responsive table-user-information" id="previousCommentTable">
						<thead>
							<tr>
						  <th></th>
						  <th></th>						  
						 
						</tr>
						  </thead>
					  <tbody>
							
						</tbody>
					  
					</table>
					  </div>
				</div>
				  </div>
			</div>
				<div class="tab-pane fade" id="tab2primary">
			  <div class="row">
					<div class="col-sm-6">
				  <div class="form-group">
						<table class="table" id="dispositionHistory">
					  <thead>
							<tr>
						  <th>Date</th>
						  <th>Time</th>
						  <th>Project</th>
						  <th>Disposition</th>
						  <th>Next FollowUpDate</th>
						  <th>CRE ID</th>
						 
						</tr>
						  </thead>
					  <tbody>
						  
					 
						</tbody>
					  
					</table>
					  </div>
				</div>
				  </div>
			</div>
			<div class="tab-pane fade" id="tab3primary">
			  <div class="row">
				<div class="col-sm-6">
				
						<table class="table table-condensed table-responsive" id="sms_history">
						 <thead>
							<tr>
						  <th>DATE</th>
						  <th>TIME</th>
						  <th>SOURCE AUTO/CRE ID</th>
						  <th>SCENARIO</th>
						  <th>MESSAGE</th>
						  <th>STATUS</th>
						 
						</tr>
						  </thead>
					  <tbody>
							
						</tbody>
					  
					</table>
					
				</div>
				
				</div>
			</div>
			<div class="tab-pane fade" id="tab4primary">
			  <div class="row">
					<div class="col-sm-6">
				  <div class="form-group">
						<table class="table table-condensed table-responsive table-user-information">
					  <tbody>
							
						</tbody>
					  
					</table>
					  </div>
				</div>
				  </div>
			</div>
			<div class="tab-pane fade" id="tab5primary">
			  <div class="row">
					<div class="col-sm-6">
				  <div class="form-group">
						<table class="table table-condensed table-responsive table-user-information" id="complaint_history">
					  <thead>
							<tr>
						  <th>COMPLAINT No.</th>
						  <th>ISSUE DATE</th>
						  <th>SOURCE</th>
						  <th>FUNCTION</th>
						  <th>CATEGORY</th>
						  <th>STATUS</th>
						 
						</tr>
						  </thead>
					  <tbody>
							
						</tbody>
					  
					</table>
					  </div>
				</div>
				  </div>
			</div>
			  </div>
		</div>
		  </div>
	</div>
	  </div>
</div>
  </div>
</div>

    <div class="modal fade" id="myModalAddPhone" role="dialog">
    <div class="modal-dialog modal-xs">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" class="crossBtnCorner"><img src=""""),_display_(/*1216.102*/routes/*1216.108*/.Assets.at("images/close1.png")),format.raw/*1216.139*/(""""></button>
          <h4 class="modal-title">ADD PHONE NUMBER</h4>
        </div>
		
		
        <div class="modal-body">
		<div class="row">
        <div class="col-xs-12">
        <div class="col-xs-4">
        <input type="text" name="myPhoneNum" class="form-control numberOnly" maxlength="10" min="10" max="10" id="myPhoneNum" >
      </div>
        </div>
        </div>
		</div>
        <div class="modal-footer">
   <button type="button" class="btn btn-default" data-dismiss="modal" id="savePhoneCust">SAVE</button>
        </div>
      </div>
      
    </div>
  </div>
   <!--------------------------------Side bar popup start------------------------------------------>
  <div id="fixedsocial">
    <div class="facebookflat" data-toggle="modal" data-target="#myModal1" >
	<!-- <img src='"""),_display_(/*1240.18*/routes/*1240.24*/.Assets.at("images/car.png")),format.raw/*1240.52*/("""' style="width: 50px;" /> -->
	<img src='"""),_display_(/*1241.13*/routes/*1241.19*/.Assets.at("images/sports-car.jpg")),format.raw/*1241.54*/("""' style="width: 50px; border-radius: 5px;" data-toggle="tooltip" data-placement="left" title="Vehicle" />
	</div>
    <div class="twitterflat" data-toggle="modal" data-target="#myModal2" >
	<img src='"""),_display_(/*1244.13*/routes/*1244.19*/.Assets.at("images/service.jpg")),format.raw/*1244.51*/("""' style="width: 50px;border-radius: 5px;" data-toggle="tooltip" data-placement="left" title="Service" onclick="ajaxCallServiceLoadOfCustomer();"/>
	<!-- <img src='"""),_display_(/*1245.18*/routes/*1245.24*/.Assets.at("images/car-insurance.png")),format.raw/*1245.62*/("""' style="width: 50px;" /> -->
	</div> 
     <div class="twitterflat1" data-toggle="modal" data-target="#myModal3" >
	 <img src='"""),_display_(/*1248.14*/routes/*1248.20*/.Assets.at("images/car-insu.jpg")),format.raw/*1248.53*/("""' style="width: 50px; border-radius: 5px;" data-toggle="tooltip" data-placement="left" title="Insurance"/> 
	 <!-- <img src='"""),_display_(/*1249.19*/routes/*1249.25*/.Assets.at("images/INSURANCE1.png")),format.raw/*1249.60*/("""' style="width: 50px;" /> --> 
	 </div> 
      
</div>
<div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding: 5px; text-align: center;">
          <button type="button" class="close" data-dismiss="modal" class="crossBtnCorner"><img src=""""),_display_(/*1259.102*/routes/*1259.108*/.Assets.at("images/close1.png")),format.raw/*1259.139*/(""""></button>
          <h4 class="modal-title" style="text-align: center;"><b>VEHICLE INFORMATION<b /></h4>
        </div>
        <div class="modal-body">
		<div class="table table-responsive">
          <table id="example700" class = "table table-bordered">
 
  <thead class="makefixed">
				   <tr>
				  <th style="width: 11%;">Model</th>
			   <th style="width: 11%;">Variant</th>
				<th style="width: 11%;">Color</th>
			   <th style="width: 12%;">Registration&nbsp;No</th>
				<th style="width: 11%;">Engine&nbsp;No</th>
				<th style="width: 11%;">Chassis&nbsp;No</th>
				  <th style="width: 11%;">Sale&nbsp;Date</th>
				<th style="width: 11%;">Fuel&nbsp;Type</th>
				</tr>
			  </thead>
			  <tbody>
			   
			"""),_display_(/*1280.5*/for(post <- customerData.getVehicles()) yield /*1280.44*/ {_display_(Seq[Any](format.raw/*1280.46*/("""
			"""),format.raw/*1281.4*/("""<tr>
			  <td style="text-align:center"><input type="text-primary" id="model" value=""""),_display_(/*1282.82*/post/*1282.86*/.getModel()),format.raw/*1282.97*/(""""  style="border:none;text-align:center" readonly></td>
			  <td style="text-align:center"><input type="text-primary" id="variant" value=""""),_display_(/*1283.84*/post/*1283.88*/.getVariant()),format.raw/*1283.101*/("""" style="border:none;text-align:center" readonly></td>
			  <td style="text-align:center"><input type="text-primary" id="color" value=""""),_display_(/*1284.82*/post/*1284.86*/.getColor()),format.raw/*1284.97*/("""" style="border:none;text-align:center" readonly></td>
			  <td style="text-align:center">
			  <div class="col-sm-12">
			  <div class="col-sm-8">
			  <input type="text-primary" class="vehicalRegNo" id="vehicalRegNo" value=""""),_display_(/*1288.80*/post/*1288.84*/.getVehicleRegNo()),format.raw/*1288.102*/("""" style="border:none;margin-right: 15px;" readonly>
				</p>
				</div>
				<div class="col-sm-2">
				  <input type="button" value="+"  id="editRegistrationno" style="color: rgb(255, 255, 255); background-color: rgba(0, 166, 90, 0.85098);">
				 </div>
				 <div class="col-sm-2">
				  <input type="button" class="btn btn-success btn-xs" value="Add" id="updateRegistrationno" onmouseenter="ajaxAddRegistrationno();" style="display:none;">
				</div>
			  </div></td>
			  <td style="text-align:center"> 
			  <div class="col-sm-12">
			   <div class="col-sm-8">
			  <input type="text-primary" id="engineNo" value=""""),_display_(/*1301.55*/post/*1301.59*/.getEngineNo()),format.raw/*1301.73*/("""" style="border:none;margin-right: 15px;" readonly>
				</p>
				  </div>
			<!-- <div class="col-sm-2">
				  <input type="button" value="+" id="editEngineno" style="color: rgb(255, 255, 255); background-color: rgba(0, 166, 90, 0.85098);">
				</div>  <div class="col-sm-2">
				  <input type="button" class="btn btn-success btn-xs" value="Add" id="updateEngineno" onmouseenter="ajaxAddEngineno();" style="display:none;">
				</div>-->
				</div></td>
			  <td style="text-align:center"><div class="col-sm-12 ">
			   <div class="col-sm-8">
			  <input type="text-primary" id="chassisNo" value=""""),_display_(/*1312.56*/post/*1312.60*/.getChassisNo()),format.raw/*1312.75*/("""" style="border:none;margin-right: 15px;" readonly>
				 </div>
				<!--<div class="col-sm-2">
				  <input type="button" value="+" id="editChassisno" style="color: rgb(255, 255, 255); background-color: rgba(0, 166, 90, 0.85098);">
				  </div>  
				  <div class="col-sm-2">  
				  <input type="button" class="btn btn-success btn-xs" value="Add" id="updateChassisno" onmouseenter="ajaxAddChassisno();" style="display:none;">
				</div>-->
				</div></td>
			
			  <td style="text-align:center"><input type="text-primary" id="saleDate" value=""""),_display_(/*1322.85*/post/*1322.89*/.getSaleDate()),format.raw/*1322.103*/("""" style="border:none;text-align:center" readonly></td>
			  <td style="text-align:center"><input type="text-primary" id="fuelType" value=""""),_display_(/*1323.85*/post/*1323.89*/.getFuelType()),format.raw/*1323.103*/("""" style="border:none;text-align:center" readonly></td>
			</tr>			""")))}),format.raw/*1324.13*/("""
				"""),format.raw/*1325.5*/("""</tbody>
</table>
</div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
  <!-- Modal -->
  <div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header" style="padding: 5px; text-align: center;">
          <button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1344.80*/routes/*1344.86*/.Assets.at("images/close1.png")),format.raw/*1344.117*/(""""></button>
          <h4 class="modal-title">SERVICE INFORMATION</h4>
        </div>
        <div class="modal-body"  style="padding: 5px;">
		<div class="table table-responsive">
          <table id="example800" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
 
   <thead class="makefixed">
                <tr>
						<th>Repair Order&nbsp;Date</th>
						<th>Repair Order&nbsp;No.</th>
						<th>Bill&nbsp;Date</th>
						<th>Kilometer</th>
						<th>Model</th>
						<th>Registration No.</th>
						<th>Service Type</th>
						<th>Menu Code Description</th>
						<th>Service Advisor Name</th>
						<th>Customer Name</th>
						<th>Group (Category)</th>
						<th>Part Amount</th>
						<th>Labour Amount</th>
						<th>Total Amount</th> 
					</tr>
                  </thead>
				   <tbody>
                                   
                    </tbody>
			  
</table>

</div>
        </div>
     
      </div>
      
    </div>
  </div>
  
  <!-- Modal -->
  <div class="modal fade" id="myModal3" role="dialog">
    <div class="modal-dialog modal-lg" >
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" class="crossBtnCorner"><img src=""""),_display_(/*1390.102*/routes/*1390.108*/.Assets.at("images/close1.png")),format.raw/*1390.139*/(""""></button>
          <h4 class="modal-title" style="tex-align:center;"><b>INSURANCE INFORMATION</b></h4>
        </div>
        <div class="modal-body">
		<div class="table table-responsive">
          <table id="" class = "table table-bordered">
	 
	  <thead class="makefixed">
					  <tr>
					<th>Policy Issue Date</th>
					<th>Policy Number</th>
					<th>Policy type</th>
					<th>Class</th>
					<th>Add-On</th>
					<th>Due Date</th>
					<th>Gross Premium</th>
					<th>IDV</th>
					<th>OD(%)</th>
					<th>Basic OD</th>
					<th>NCB(%)</th>
					<th>NCB Value</th>
					<th>Discount</th>
					<th>OD Premium</th>
					<th>TP Premium</th>
					<th>PA Premium</th>
					<th>Legal Liability</th>
					<th>Net Liability</th>
					<th>Add-On Premium</th>
					<th>Other Premium</th>
					<th>Net Premium</th>
					<th>Tax</th>
					<!--<th>Gross Premium</th>
					<th>Coverage&nbsp;Period</th>
					<th>Policy&nbsp;Due&nbsp;Date</th>
					<th>Insurance&nbsp;Company</th>
					<th>NCB(%age)</th>
					<th>NCB&nbsp;Amount(Rs.)</th>
					<th>PREMIUM</th>
					<th>Service&nbsp;Tax</th>
					<th>Total&nbsp;Premium</th>
					<th>Last&nbsp;Renewal&nbsp;By</th>-->
					
					</tr>
				  </thead>
				<tbody>
				"""),_display_(/*1435.6*/for(insuranceData <-customerData.getInsurances()) yield /*1435.55*/{_display_(Seq[Any](format.raw/*1435.56*/("""
				 
				  """),format.raw/*1437.7*/("""<tr>
				 <!--  <td><a data-toggle="modal" data-target="#otherdetails" ><i class="fa fa-info-circle" data-toggel="tooltip" title="Other Details" style="font-size:30px;color:red;"></i></a></td> -->
					<td>"""),_display_(/*1439.11*/insuranceData/*1439.24*/.getPolicyDueDateStr()),format.raw/*1439.46*/("""</td>
					<td>"""),_display_(/*1440.11*/insuranceData/*1440.24*/.getPolicyNo()),format.raw/*1440.38*/("""</td>
					<td>"""),_display_(/*1441.11*/insuranceData/*1441.24*/.getInsuranceCompanyName()),format.raw/*1441.50*/("""</td>
					<td>"""),_display_(/*1442.11*/insuranceData/*1442.24*/.getPolicyType()),format.raw/*1442.40*/("""</td>
					<td>"""),_display_(/*1443.11*/insuranceData/*1443.24*/.getClassType()),format.raw/*1443.39*/("""</td>
					<td>"""),_display_(/*1444.11*/insuranceData/*1444.24*/.getAddOn()),format.raw/*1444.35*/("""</td>
					<td>"""),_display_(/*1445.11*/insuranceData/*1445.24*/.getPolicyDueDate()),format.raw/*1445.43*/("""</td>
					<td>"""),_display_(/*1446.11*/insuranceData/*1446.24*/.getGrossPremium()),format.raw/*1446.42*/("""</td>
					<td>"""),_display_(/*1447.11*/insuranceData/*1447.24*/.getIdv()),format.raw/*1447.33*/("""</td>
					<td>"""),_display_(/*1448.11*/insuranceData/*1448.24*/.getOdPercentage()),format.raw/*1448.42*/("""</td>
					 <td>"""),_display_(/*1449.12*/insuranceData/*1449.25*/.getNcBPercentage()),format.raw/*1449.44*/("""</td>
					<td>"""),_display_(/*1450.11*/insuranceData/*1450.24*/.getNcBAmountStr()),format.raw/*1450.42*/("""</td>
					<td>"""),_display_(/*1451.11*/insuranceData/*1451.24*/.getDiscountPercentage()),format.raw/*1451.48*/("""</td>
					<td>"""),_display_(/*1452.11*/insuranceData/*1452.24*/.getODpremium()),format.raw/*1452.39*/("""</td>
					<td>"""),_display_(/*1453.11*/insuranceData/*1453.24*/.getaPPremium()),format.raw/*1453.39*/("""</td>
					<td>"""),_display_(/*1454.11*/insuranceData/*1454.24*/.getpAPremium()),format.raw/*1454.39*/("""</td>
					<td>"""),_display_(/*1455.11*/insuranceData/*1455.24*/.getLegalLiability()),format.raw/*1455.44*/("""</td>
					<td>"""),_display_(/*1456.11*/insuranceData/*1456.24*/.getNetLiability()),format.raw/*1456.42*/("""</td>
					<td>"""),_display_(/*1457.11*/insuranceData/*1457.24*/.getAdd_ON_Premium()),format.raw/*1457.44*/("""</td>
					<td>"""),_display_(/*1458.11*/insuranceData/*1458.24*/.getOtherPremium()),format.raw/*1458.42*/("""</td>
					<td>"""),_display_(/*1459.11*/insuranceData/*1459.24*/.getPremiumAmountStr()),format.raw/*1459.46*/("""</td>
					<td>"""),_display_(/*1460.11*/insuranceData/*1460.24*/.getServiceTax()),format.raw/*1460.40*/("""</td>
				  </tr>
				  """)))}),format.raw/*1462.8*/("""
					"""),format.raw/*1463.6*/("""</tbody> 
	</table>
        </div>
		</div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  <!--------------------------------Side bar popup end------------------------------------------>
  
  <!--Popup For Driver Allocation-->
<div class="modal fade" id="DriverAllcationPOPUp" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
	   <div class="modal-header" style="text-align:center">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Driver Allocation</h4>																													
        </div>
             <div class="modal-body" style="padding:4px;">
  <div class="panel panel-primary" >
	<div class="panel-heading" style="padding:0px;"> </div>
	<div class="panel-body">
			<div class="col-md-12">
			
			<div class="form-inline">
  <div class="form-group">
<label for="comments"><b> BOOKING DATE :<b></label>
<input type="text" class="form-control datepicMYDropDown" id="changeserviceBookingDate" name="pickupDate" onchange="AssignBtnBkreviewScript();" readonly>
  </div>
  </div>
<br />
</div>

                <div class="col-md-12">
                <table class="table table-responsive">
                  <tbody>
                    <tr>
                      <td><strong> New Booking Date: </strong></td>
                      <td class="text-primary" id="newbookingdate"></td>
                    </tr>
                     <tr>
                      <td><strong> New Driver Name:</strong></td>
                      <td class="text-primary" id="newDriver"></td>
                    </tr>
					<tr>
                      <td><strong>New Time Slot:</strong></td>
                      <td class="text-primary" id="newTimeSlot"></td>
                    </tr>
                  </tbody>
                </table>
                </div>
              
	 <div>
        <input type="hidden" id="tempValue" value="0">
        <input type="hidden" id="tempIncreValue" value="0">
        <input type="hidden" name="time_From" id="startValue" value="0">
        <input type="hidden" name="time_To" id="endValue" value="0">
        <input type="hidden" name="driverId" id="driverValue" value="0">
      </div>
  <table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" id="tableID"  style="cursor: pointer;">
    <thead>  
        
    </thead>
    <tbody> 
           
    </tbody>
  </table>
  <button type="button" id="allcateSubmit" class="btn btn-primary pull-right" data-dismiss="modal">Submit</button>
</div>
</div>
 </div>
       </div>
      
    </div>
  </div>
  
  <!-- Adress pop up is -->
  <div class="modal fade" id="AddNewAddressPopup" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1551.78*/routes/*1551.84*/.Assets.at("images/close1.png")),format.raw/*1551.115*/(""""></button>
        <h4 class="modal-title"><b style="text-align:center; color:Red;">Add New Address</b></h4>
      </div>
      <div class="modal-body">
	 
		  <div class="row" id="AddAddressDiv">
		   <div class="col-md-12">
			<div class="col-md-6">
			  <div class="form-group">
				<label for="comments"><b>Add Address1<b></label>
				<textarea class="form-control" rows="1" id="AddAddress1MMS" name="address1New" ></textarea>
			  </div>
			</div>
			<div class="col-md-6">
			  <div class="form-group">
				<label for="comments"><b>Add Address2<b></label>
				<textarea class="form-control" rows="1" id="AddAddress2MMS" name="address2New" ></textarea>
			  </div>
			</div>
			
			<div class="col-md-6">
			  <div class="form-group">
				<label for="comments"><b>State<b></label>
				<select class="form-control" name="stateNew" id="AddAddrMMSState" onchange="getCityByStateSelection('AddAddrMMSState','cityInputPopup3');">
				  <option value="0">--SELECT--</option>
		"""),_display_(/*1576.4*/for(states <- statesList) yield /*1576.29*/{_display_(Seq[Any](format.raw/*1576.30*/("""
           
          """),format.raw/*1578.11*/("""<option value=""""),_display_(/*1578.27*/states),format.raw/*1578.33*/("""">"""),_display_(/*1578.36*/states),format.raw/*1578.42*/("""</option>
            """)))}),format.raw/*1579.14*/("""
		
				"""),format.raw/*1581.5*/("""</select>
			  </div>
			</div>
			
			<div class="col-md-6">
			  <div class="form-group">
				<label for="comments"><b>City<b></label>
				<select class="form-control" name="cityNew" placeholder="City" id="cityInputPopup3">	
                                
                              
        
                                    </select>
			  </div>
			</div>
			<div class="col-md-6">
			  <div class="form-group">
				<label for="comments"><b>PinCode<b></label>
				<input type="text" class="form-control numberOnly" name="pincodeNew" value="0" id="PinCode1MMS" maxlength="6" />
			  </div>
			</div>
			
			</div>
		   
		  </div>
  

<div class="modal-footer">
  <button type="button" class="btn btn-success" id="AddAddrMMSSave" data-dismiss="modal">Save</button>
</div>
</div>
    </div>
  </div>
</div>


<!-- Incomplete Survey -->

<div class="modal fade" id="IncompleteSurvey" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Incomplete Survey</h4>
        </div>
        <div class="modal-body">
         <div class="row">
				  <div class="col-md-3">
					<div class="form-group">
					  <label for="followUpDate">Follow Up Date</label>
					  <input type="text" name="psfAppointmentDate" class="showOnlyFutureDate form-control" id="incompleteDate" readonly>
					</div>
				  </div>
				  <div class="col-md-3">
					<div class="form-group">
					  <label for="followUpTime">Follow Up Time</label>
					  <input type="text" name="psfAppointmentTime" class="single-input form-control" id="" readonly>
					</div>
				  </div>
				   <div class="col-md-6">
					<div class="form-group">
					  <label for="followUpTime">Remarks</label>
					  <textarea type="text" name="remarksList[0]" class="form-control" id=""></textarea>
					</div>
				  </div>
				
        </div>
		
        </div>
<div class="modal-footer">
          <button type="submit" class="btn btn-success" name="" data-dismiss="modal">Submit</button>
        </div>      
      </div>
      
    </div>
  </div>
  
  
""")))}),format.raw/*1660.2*/("""


"""),format.raw/*1663.1*/("""<script src=""""),_display_(/*1663.15*/routes/*1663.21*/.Assets.at("javascripts/disposition.js")),format.raw/*1663.61*/("""" type="text/javascript"></script>


""")))}),format.raw/*1666.2*/("""
"""))
      }
    }
  }

  def render(oem:String,typeOfPSF:Long,dispositionHistory:Long,interactionid:Long,uniqueid:Long,dealercode:String,dealerName:String,userName:String,customerData:Customer,vehicleData:Vehicle,userData:WyzUser,latestService:Service,smsTemplates:List[SMSTemplate],complaintOFCust:List[String],statesList:List[String],psfDayPage:Html): play.twirl.api.HtmlFormat.Appendable = apply(oem,typeOfPSF,dispositionHistory,interactionid,uniqueid,dealercode,dealerName,userName,customerData,vehicleData,userData,latestService,smsTemplates,complaintOFCust,statesList,psfDayPage)

  def f:((String,Long,Long,Long,Long,String,String,String,Customer,Vehicle,WyzUser,Service,List[SMSTemplate],List[String],List[String],Html) => play.twirl.api.HtmlFormat.Appendable) = (oem,typeOfPSF,dispositionHistory,interactionid,uniqueid,dealercode,dealerName,userName,customerData,vehicleData,userData,latestService,smsTemplates,complaintOFCust,statesList,psfDayPage) => apply(oem,typeOfPSF,dispositionHistory,interactionid,uniqueid,dealercode,dealerName,userName,customerData,vehicleData,userData,latestService,smsTemplates,complaintOFCust,statesList,psfDayPage)

  def ref: this.type = this

}


}

/**/
object commonPSFDispositionPage extends commonPSFDispositionPage_Scope0.commonPSFDispositionPage
              /*
                  -- GENERATED --
                  DATE: Thu Dec 28 12:24:28 IST 2017
                  SOURCE: D:/CRMNEW/28th Dec/crm/app/views/commonPSFDispositionPage.scala.html
                  HASH: ae4249cf9c83765de8c52c6de967308dfbfd79a2
                  MATRIX: 907->1|1320->318|1350->323|1421->386|1460->388|1492->394|1540->415|1568->416|1598->420|1650->446|1677->447|1710->453|1747->462|1776->463|1807->467|1858->491|1886->492|1915->494|1954->507|1968->513|2042->578|2081->579|2110->581|2167->611|2182->617|2228->642|2312->699|2431->796|2527->865|2570->887|2664->954|2703->972|2796->1038|2846->1067|2933->1127|2962->1135|3040->1186|3138->1262|3215->1312|3272->1348|3353->1402|3387->1415|3473->1474|3512->1492|3589->1542|3619->1551|3690->1595|3714->1598|4054->1911|4278->2114|4307->2115|4483->2264|4615->2375|4894->2627|4967->2678|5278->2962|5337->3005|5376->3006|5428->3031|5847->3429|5890->3441|5935->3458|6799->4295|6869->4343|7001->4447|7071->4495|7103->4499|7133->4507|7165->4511|7187->4523|7217->4531|8153->5439|8306->5571|8335->5572|8512->5722|8572->5760|8700->5860|8749->5886|9029->6137|9459->6546|9488->6547|9710->6741|9773->6781|9804->6782|9837->6786|9898->6824|10020->6918|10078->6953|10295->7142|10351->7175|10578->7374|10623->7409|10663->7410|10728->7447|10793->7495|10833->7496|10880->7514|11028->7633|11105->7699|11146->7700|11193->7719|11217->7733|11249->7743|11288->7762|11302->7766|11342->7767|11389->7785|11442->7806|11472->7807|11536->7839|11587->7858|11621->7864|11999->8214|12015->8220|12066->8248|12238->8393|12269->8414|12309->8415|12365->8443|12403->8453|12456->8484|12518->8518|12565->8542|12627->8576|12675->8602|12724->8623|12770->8647|12851->8700|12907->8734|13117->8915|13170->8945|13243->8987|13279->8995|13575->9263|13591->9269|13648->9303|13969->9595|14026->9629|14469->10044|14485->10050|14546->10088|14716->10230|14772->10264|14823->10287|14880->10321|14951->10365|15064->10457|15096->10461|15169->10506|15225->10540|15525->10812|15541->10818|15593->10848|16370->11597|16386->11603|16454->11648|16767->11933|16826->11970|17010->12126|17059->12153|17346->12412|17362->12418|17420->12454|17743->12748|17769->12763|17799->12770|17992->12935|18017->12950|18046->12957|18157->13040|18182->13055|18211->13062|18314->13137|18339->13152|18368->13159|18834->13596|18851->13602|18905->13633|19322->14022|19344->14034|19385->14052|19650->14289|19673->14301|19712->14317|20081->14657|20231->14786|20265->14793|20415->14922|20449->14929|20599->15058|21075->15505|21225->15634|21259->15641|21409->15770|21443->15777|21593->15906|22045->16329|22189->16452|22223->16459|22367->16582|22401->16589|22545->16712|23621->17760|23681->17803|23721->17804|23774->17829|24086->18119|24130->18131|24176->18148|24587->18531|24633->18554|24884->18777|24943->18813|25373->19215|25433->19258|25473->19259|25526->19284|25838->19574|25882->19586|25928->19603|26773->20420|26831->20455|27854->21450|27910->21483|28469->22013|28532->22053|28799->22291|28860->22329|29144->22584|29195->22612|29227->22615|29278->22643|29981->23318|29998->23324|30052->23355|30901->24176|31027->24279|31224->24448|31351->24552|31578->24750|31714->24863|32384->25505|32401->25511|32455->25542|33374->26433|33500->26536|33696->26704|33823->26808|34046->27002|34182->27115|34849->27754|34866->27760|34920->27791|35275->28117|35403->28222|35628->28418|35756->28523|35975->28713|36103->28818|36333->29019|36453->29116|36687->29321|36808->29419|37049->29631|37185->29744|37932->30462|37949->30468|38003->30499|38996->31464|39026->31472|39633->32051|39663->32059|39693->32060|40225->32564|40242->32570|40296->32601|40919->33195|40936->33201|40990->33232|41222->33436|41244->33448|41284->33466|41315->33468|42321->34445|42338->34451|42392->34482|42587->34650|42634->34680|42674->34681|42709->34688|42779->34730|42825->34754|42867->34768|42914->34792|42965->34814|43014->34840|43061->34856|43094->34861|43231->34970|43282->34999|43318->35008|43365->35038|43405->35039|43450->35056|43500->35078|43539->35107|43579->35108|43618->35115|43657->35126|43704->35150|43774->35192|43820->35216|43876->35244|43924->35270|44017->35335|44063->35359|44104->35372|44151->35396|44247->35463|44300->35493|44396->35561|44442->35585|44557->35669|44588->35672|45293->36348|45310->36354|45364->36385|54165->45158|54198->45168|54240->45181|54634->45546|54651->45552|54718->45596|58563->49411|58581->49417|58636->49448|59486->50269|59503->50275|59554->50303|59626->50346|59643->50352|59701->50387|59934->50591|59951->50597|60006->50629|60200->50794|60217->50800|60278->50838|60439->50970|60456->50976|60512->51009|60668->51136|60685->51142|60743->51177|61176->51580|61194->51586|61249->51617|62021->52361|62078->52400|62120->52402|62154->52407|62270->52494|62285->52498|62319->52509|62488->52649|62503->52653|62540->52666|62706->52803|62721->52807|62755->52818|63015->53049|63030->53053|63072->53071|63732->53702|63747->53706|63784->53720|64421->54328|64436->54332|64474->54347|65058->54902|65073->54906|65111->54920|65280->55060|65295->55064|65333->55078|65434->55146|65469->55152|66075->55729|66092->55735|66147->55766|67534->57123|67552->57129|67607->57160|68894->58419|68961->58468|69002->58469|69046->58484|69284->58693|69308->58706|69353->58728|69399->58745|69423->58758|69460->58772|69506->58789|69530->58802|69579->58828|69625->58845|69649->58858|69688->58874|69734->58891|69758->58904|69796->58919|69842->58936|69866->58949|69900->58960|69946->58977|69970->58990|70012->59009|70058->59026|70082->59039|70123->59057|70169->59074|70193->59087|70225->59096|70271->59113|70295->59126|70336->59144|70383->59162|70407->59175|70449->59194|70495->59211|70519->59224|70560->59242|70606->59259|70630->59272|70677->59296|70723->59313|70747->59326|70785->59341|70831->59358|70855->59371|70893->59386|70939->59403|70963->59416|71001->59431|71047->59448|71071->59461|71114->59481|71160->59498|71184->59511|71225->59529|71271->59546|71295->59559|71338->59579|71384->59596|71408->59609|71449->59627|71495->59644|71519->59657|71564->59679|71610->59696|71634->59709|71673->59725|71732->59752|71768->59759|74981->62943|74998->62949|75053->62980|76084->63983|76127->64008|76168->64009|76223->64034|76268->64050|76297->64056|76329->64059|76358->64065|76415->64089|76454->64099|78784->66397|78819->66403|78862->66417|78879->66423|78942->66463|79015->66504
                  LINES: 27->1|32->1|34->3|34->3|34->3|36->5|37->6|37->6|38->7|39->8|39->8|41->10|41->10|41->10|42->11|43->12|43->12|44->13|46->15|46->15|46->15|46->15|47->16|47->16|47->16|47->16|48->17|48->17|49->18|49->18|50->19|50->19|51->20|51->20|52->21|52->21|53->22|53->22|54->23|54->23|55->24|55->24|56->25|56->25|57->26|57->26|58->27|58->27|68->37|76->45|76->45|78->47|82->51|88->57|88->57|90->59|90->59|90->59|92->61|100->69|101->70|102->71|113->82|113->82|114->83|114->83|114->83|114->83|114->83|114->83|114->83|125->94|127->96|127->96|130->99|130->99|131->100|131->100|136->105|144->113|144->113|149->118|149->118|149->118|149->118|149->118|150->119|150->119|155->124|155->124|162->131|162->131|162->131|164->133|164->133|164->133|165->134|165->134|165->134|165->134|166->135|166->135|166->135|167->136|167->136|167->136|168->137|169->138|169->138|170->139|171->140|172->141|183->152|183->152|183->152|185->154|185->154|185->154|189->158|189->158|189->158|189->158|189->158|190->159|190->159|190->159|190->159|191->160|191->160|192->161|192->161|195->164|196->165|203->172|203->172|203->172|205->174|205->174|216->185|216->185|216->185|218->187|218->187|218->187|218->187|220->189|223->192|225->194|226->195|226->195|237->206|237->206|237->206|256->225|256->225|256->225|259->228|259->228|261->230|261->230|273->242|273->242|273->242|275->244|275->244|275->244|277->246|277->246|277->246|278->247|278->247|278->247|279->248|279->248|279->248|293->262|293->262|293->262|302->271|302->271|302->271|308->277|308->277|308->277|318->287|322->291|323->292|327->296|328->297|332->301|340->309|344->313|345->314|349->318|350->319|354->323|361->330|365->334|366->335|370->339|371->340|375->344|399->368|399->368|399->368|401->370|406->375|407->376|408->377|415->384|415->384|419->388|419->388|428->397|428->397|428->397|430->399|435->404|436->405|437->406|452->421|452->421|471->440|471->440|482->451|482->451|486->455|486->455|490->459|490->459|490->459|490->459|520->489|520->489|520->489|540->509|540->509|543->512|543->512|548->517|548->517|566->535|566->535|566->535|587->556|587->556|590->559|590->559|595->564|595->564|613->582|613->582|613->582|620->589|620->589|624->593|624->593|627->596|627->596|632->601|632->601|635->604|635->604|640->609|640->609|661->630|661->630|661->630|690->659|690->659|707->676|707->676|707->676|725->694|725->694|725->694|747->716|747->716|747->716|751->720|751->720|751->720|751->720|776->745|776->745|776->745|781->750|781->750|781->750|782->751|782->751|782->751|782->751|782->751|782->751|782->751|783->752|785->754|787->756|787->756|788->757|788->757|788->757|791->760|791->760|791->760|791->760|791->760|791->760|791->760|792->761|792->761|793->762|793->762|794->763|794->763|794->763|794->763|794->763|794->763|796->765|796->765|801->770|802->771|821->790|821->790|821->790|1090->1059|1090->1059|1094->1063|1103->1072|1103->1072|1103->1072|1247->1216|1247->1216|1247->1216|1271->1240|1271->1240|1271->1240|1272->1241|1272->1241|1272->1241|1275->1244|1275->1244|1275->1244|1276->1245|1276->1245|1276->1245|1279->1248|1279->1248|1279->1248|1280->1249|1280->1249|1280->1249|1290->1259|1290->1259|1290->1259|1311->1280|1311->1280|1311->1280|1312->1281|1313->1282|1313->1282|1313->1282|1314->1283|1314->1283|1314->1283|1315->1284|1315->1284|1315->1284|1319->1288|1319->1288|1319->1288|1332->1301|1332->1301|1332->1301|1343->1312|1343->1312|1343->1312|1353->1322|1353->1322|1353->1322|1354->1323|1354->1323|1354->1323|1355->1324|1356->1325|1375->1344|1375->1344|1375->1344|1421->1390|1421->1390|1421->1390|1466->1435|1466->1435|1466->1435|1468->1437|1470->1439|1470->1439|1470->1439|1471->1440|1471->1440|1471->1440|1472->1441|1472->1441|1472->1441|1473->1442|1473->1442|1473->1442|1474->1443|1474->1443|1474->1443|1475->1444|1475->1444|1475->1444|1476->1445|1476->1445|1476->1445|1477->1446|1477->1446|1477->1446|1478->1447|1478->1447|1478->1447|1479->1448|1479->1448|1479->1448|1480->1449|1480->1449|1480->1449|1481->1450|1481->1450|1481->1450|1482->1451|1482->1451|1482->1451|1483->1452|1483->1452|1483->1452|1484->1453|1484->1453|1484->1453|1485->1454|1485->1454|1485->1454|1486->1455|1486->1455|1486->1455|1487->1456|1487->1456|1487->1456|1488->1457|1488->1457|1488->1457|1489->1458|1489->1458|1489->1458|1490->1459|1490->1459|1490->1459|1491->1460|1491->1460|1491->1460|1493->1462|1494->1463|1582->1551|1582->1551|1582->1551|1607->1576|1607->1576|1607->1576|1609->1578|1609->1578|1609->1578|1609->1578|1609->1578|1610->1579|1612->1581|1691->1660|1694->1663|1694->1663|1694->1663|1694->1663|1697->1666
                  -- GENERATED --
              */
          