
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object messageTemplateSuperAdmin_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class messageTemplateSuperAdmin extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template7[String,String,String,String,List[SMSTemplate],List[MessageParameters],List[SavedSearchResult],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*3.2*/(dealercode:String,dealerName:String,userName:String,oem:String,smsTemplateList :List[SMSTemplate],msgParameters :List[MessageParameters], savedSearchList :List[SavedSearchResult]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*3.182*/("""

"""),_display_(/*5.2*/mainPageCRE("AutoSherpaCRM",userName,dealerName,dealercode,oem)/*5.65*/ {_display_(Seq[Any](format.raw/*5.67*/("""
"""),format.raw/*6.1*/("""<link rel="stylesheet" href=""""),_display_(/*6.31*/routes/*6.37*/.Assets.at("css/gsdk-base.css")),format.raw/*6.68*/("""">

<div class="container-fluid">
  <div class="panel panel-primary">
    <div class="panel-heading" style="text-align:center;"><strong>SEARCH CUSTOMER</strong> </div>
    <div class="panel-body">
      
      <!---------------------Wizard1----------------------------------------->
      <div class="wizard-container">
        <div class="card wizard-card ct-wizard-green" id="wizard"> 
          
          <!--        You can switch "ct-wizard-azzure"  with one of the next bright colors: "ct-wizard-blue", "ct-wizard-green", "ct-wizard-orange", "ct-wizard-red"             -->
          
          <ul>
            <li><a href="#step1" data-toggle="tab" class="tabRound" >SEARCH</a></li>
            <li><a href="#step2" data-toggle="tab" class="tabRound">MESSAGE</a></li>
            <li><a href="#step14" data-toggle="tab" class="tabRound">LIST</a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane animated  bounceInRight" id="step1"> 
              
              <!-------------------------------Step1------------------------------------->
              <div class="panel panel-primary" >
                <div class="panel-heading" style="text-align:center;">Search 
                             </div>
                <div class="panel-body" style="overflow:auto">
				
				<label class="radio-inline">
				<input type="hidden" id="hiddenSelectAllFlag" value="">
      <input type="radio" name="SearchCust" value="Search Customer">Search Customer
      
    </label>
    <label class="radio-inline">
      <input type="radio" name="SearchCust" value="Upload Customer">Upload Customer
    </label> 
				
				<div style="display:none" id="SarchDivID" class="animated  bounceInRight">
                  <div class="row">
                    <div class="col-md-12">
				<div class="col-md-3">
	<div class="form-group">	<br />
	<a href="/searchByCustomer"><button class="btn btn-info" >Create New Search</button></a>
	</div>
	</div>
	<div class="col-md-3">
	<div class="form-group">
	<label>Select Saved Search</label>
	<select class="form-control" id="savedName" name="savedName">
		<option value="0" >--Select--</option>
                                """),_display_(/*55.34*/for(savedSearch_List<-savedSearchList) yield /*55.72*/{_display_(Seq[Any](format.raw/*55.73*/("""
                                """),format.raw/*56.33*/("""<option value=""""),_display_(/*56.49*/savedSearch_List/*56.65*/.getSavedName()),format.raw/*56.80*/("""">"""),_display_(/*56.83*/savedSearch_List/*56.99*/.getSavedName()),format.raw/*56.114*/("""</option>
                             """)))}),format.raw/*57.31*/("""
	"""),format.raw/*58.2*/("""</select>
  </div>
  </div>
  <br />
  <button type="button" id="SearchId" class="btn btn-info" >Get Customer</button>
  </div>
  <br />

                  </div>
                  </div>
				  <br />
				   <br />
<div style="display:none;" id="uploadDivID" class="animated  bounceInRight" >
                  <div class="row fileupload-buttonbar">
            <div class="col-lg-7">
                <!-- The fileinput-button span is used to style the file input field as button -->
                <span class="btn btn-success fileinput-button">
                    <i class="glyphicon glyphicon-plus"></i>
                    <span>Add files...</span>
                    <input type="file" name="uploadfile" multiple>
                </span>
                <button type="submit" class="btn btn-primary start">
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start upload</span>
                </button>
                <button type="button" class="btn btn-danger delete">
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Delete</span>
                </button>
                <input type="checkbox" class="toggle">
                <span></span>
                <!--<button class="btn btn-success wyzprocess" data-type="POST" data-url=""""),format.raw/*89.91*/("""{"""),format.raw/*89.92*/("""%=file.processUrl%"""),format.raw/*89.110*/("""}"""),format.raw/*89.111*/("""""""),format.raw/*89.112*/("""{"""),format.raw/*89.113*/("""% if (file.deleteWithCredentials) """),format.raw/*89.147*/("""{"""),format.raw/*89.148*/(""" """),format.raw/*89.149*/("""%"""),format.raw/*89.150*/("""}"""),format.raw/*89.151*/(""" """),format.raw/*89.152*/("""data-xhr-fields='"""),format.raw/*89.169*/("""{"""),format.raw/*89.170*/(""""withCredentials":true"""),format.raw/*89.192*/("""}"""),format.raw/*89.193*/("""'"""),format.raw/*89.194*/("""{"""),format.raw/*89.195*/("""% """),format.raw/*89.197*/("""}"""),format.raw/*89.198*/(""" """),format.raw/*89.199*/("""%"""),format.raw/*89.200*/("""}"""),format.raw/*89.201*/(""">
                    <i class="glyphicon glyphicon-check"></i>
                    <span>Submit For Reporting</span>
                </button>
				<input type="checkbox" class="poggle">-->
                
               <!-- The global file processing state -->
                <span class="fileupload-process"></span>
            </div>
            <!-- The global progress state -->
            <div class="col-lg-5 fileupload-progress fade">
                <!-- The global progress bar -->
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                </div>
                <!-- The extended global progress state -->
                <div class="progress-extended">&nbsp;</div>
            </div>
        </div>
                  </div>
				  
				  
				
				    <div class="SearchCustId" style="display:none;" >
					 <div class="panel panel-primary" >
                <div class="panel-heading" style="text-align:center;">Search List </div>
                <div class="panel-body">
      <table class="table table-striped table-bordered table-hover" id="customerSearchTables">
                                    <thead>
                                 <tr>
                                    <th>Customer Name</th>
                                    <th>Vehicle RegNo.</th>
                                    <th>Category</th>                                   
                                    <th>Phone Number</th>                                    
                                    <th>Next Service Date</th>
                                    <th>Model</th>
                                    <th>Variant</th>
                                    <th>Last Disposition</th>
									<th>Select All this <input type="checkbox" id="selectAll"/></th>
                                 </tr>
                              </thead>
                                <tbody>
							
                   </tbody>
           </table>
        </div>
        </div>
        </div>
		
		
		
		
		<div class="UploadCustIdDiv" style="display:none;" >
		<div class="panel panel-primary" >
                <div class="panel-heading" style="text-align:center;">Upload List </div>
                <div class="panel-body">
      <table class="table table-striped table-bordered table-hover" id="customerUploadTable">
                                    <thead>
                                 <tr>
                                    
                                    <th>Customer Name</th>
                                    <th>Vehicle RegNo.</th>
                                    <th>Category</th>                                   
                                    <th>Phone Number</th>                                    
                                    <th>Next Service Date</th>
                                    <th>Model</th>
                                    <th>Variant</th>
                                    <th>Last Disposition</th>
									<th>Select All <input type="checkbox" id=""/></th>

                                    
                                 </tr>
                              </thead>
                                <tbody>
								<tr>
									<td>Rohit</td>
									<td>KA-06 N-2201</td>
									<td></td>
									<td>9879908703</td>
									<td>30/06/2017</td>
									<td>i10 Honda</td>
									<td>Blue</td>
									<td></td>
									<td><input type="checkbox" name="uploadname"/></td>

								</tr>
								
								<tr>
									<td>Lohit</td>
									<td>KA-06 N-2202</td>
									<td></td>
									<td>9879908703</td>
									<td>30/06/2017</td>
									<td>i11 Honda</td>
									<td>Red</td>
									<td></td>
									<td><input type="checkbox" name="uploadname"/></td>

								</tr>
                                   
                   </tbody>
           </table>
        </div>
        </div>
        </div>
                  <!-- <div class="row">
			<div class="col-md-12">
			<div class="pull-right">
					<button type="button" class="btn btn-success"><i class="fa fa-check-square" aria-hidden="true"></i> &nbsp;Submit</button>
			</div>
			</div>
		</div> --> 
                
                </div>
              </div>
            </div>
            <div class="tab-pane animated  bounceInRight" id="step2" > 
              <!----------------------------------Step2----------------------------->
              <div class="panel panel-primary">
                <div class="panel-heading" style="text-align:center;">MESSAGE
                 
                </div>
                <div class="panel-body">
               <div>
	<div class="row">
    <div class="col-sm-4">
     <div class="form-group">
  <label for="sel1">Message Template</label>
 <select id="msgTypeID" name="smsTemplate" class="form-control">
                               <option value="0" >--Select--</option>
                               """),_display_(/*219.33*/for(smsTemplate_List<-smsTemplateList) yield /*219.71*/{_display_(Seq[Any](format.raw/*219.72*/("""
                               """),format.raw/*220.32*/("""<option value=""""),_display_(/*220.48*/smsTemplate_List/*220.64*/.getSmsId()),format.raw/*220.75*/("""">"""),_display_(/*220.78*/smsTemplate_List/*220.94*/.getSmsTemplate()),format.raw/*220.111*/("""</option>
                            """)))}),format.raw/*221.30*/("""
    
    
  """),format.raw/*224.3*/("""</select>
  
</div>
    </div>
    
    <div class="col-sm-4">
		<div class="form-group">
  <label for="sel1">Message Parameters</label>
  <select class="form-control" id="ParameteID" >	
     <option value="0" >--Select--</option>
                                """),_display_(/*234.34*/for(msgParameters_list<-msgParameters) yield /*234.72*/{_display_(Seq[Any](format.raw/*234.73*/("""
                                """),format.raw/*235.33*/("""<option value=""""),_display_(/*235.49*/msgParameters_list/*235.67*/.getMessageParameter()),format.raw/*235.89*/("""">"""),_display_(/*235.92*/msgParameters_list/*235.110*/.getMessageParameter()),format.raw/*235.132*/("""</option>
                             """)))}),format.raw/*236.31*/("""
    
  """),format.raw/*238.3*/("""</select>
  
</div>
    </div>
     <div class="col-sm-3">
		<div class="form-group">
  <label for="sel1">SMS API</label>
  <input type="text" class="form-control" name="smsAPI" id="smsAPI">
</div>
    </div>
	<div class="col-sm-3"><br />
	<button id="AddId" class="btn btn-info">Add</button>
	
	</div>
	
  </div>
  <div class="row">
	<div class="col-sm-4">
    <div class="form-group">
  <label for="comment"><label>
  <textarea class="form-control addedTxt" rows="4" cols="90" id="txtAreaId">
  </textarea>
</div>
    </div>
  </div>
  <button class="btn btn-success" id="SaveTemp" onclick="saveNewSmsTemplate();">Save Template</button> 
  </div>
                </div>
              </div>
            </div>
            
            <!------------------------------------step14--------------------------------------------->
            <div class="tab-pane animated  bounceInRight" id="step14">
              <div class="panel panel-primary">
                <div class="panel-heading" style="text-align:center;">List</div>
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover" id="selectedCustomers">
                                    <thead>
	                                 <tr>
	                                    <th>Customer Name</th>
	                                    <th>Vehicle RegNo.</th>
	                                    <th>Category</th>                                   
	                                    <th>Phone Number</th>                                    
	                                    <th>Next Service Date</th>
	                                    <th>Model</th>
	                                    <th>Variant</th>
	                                    <th>Last Disposition</th>                                    
	                                 </tr>
                              </thead>
                                <tbody>
							
                   </tbody>
           </table>
                </div>
                </div>
                
             
            </div>
          </div>
          <div class="wizard-footer">
            <div class="pull-right">
              <button step2='button' class='btn btn-next btn-fill btn-warning btn-wd btn-sm' id=""  name='next' value='Next' />
              Next
              </button>
              <button step2='button' class='btn btn-finish btn-fill btn-warning btn-wd btn-sm' name='finish' value='Finish' onclick="sendBulkSMS();"/>
              <i class="fa fa-check-square" aria-hidden="true" ></i>&nbsp;Send Bulk SMS
              </button>
            </div>
            <div class="pull-left">
              <button step2='button' class='btn btn-previous btn-fill btn-default btn-wd btn-sm' name='previous' value='Previous' />
              Previous
              </button>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
      <!-- wizard container --> 
      <!----------------------------Wizard End-----------------------------------> 
      
      <!------------------------------------Last--------------------------------------------->
      <div class="panel panel-primary" style="display:none;">
        <div class="panel-heading" style="text-align:center;">INTERACTION HISTORY</div>
        <div class="panel-body" style="overflow:auto"> </div>
      </div>
      <!---------------------------------------- Last End----------------------------------------------------> 
      
    </div>
  </div>
</div>
""")))}),format.raw/*328.2*/("""

"""),format.raw/*330.1*/("""<script>
var selectedUsers = [];
var dataInfo ="""),format.raw/*332.15*/("""{"""),format.raw/*332.16*/("""}"""),format.raw/*332.17*/(""";
var setFlag = false;
$("#selectAll").click(function()"""),format.raw/*334.33*/("""{"""),format.raw/*334.34*/("""
	"""),format.raw/*335.2*/("""$("input[name=searchname]").prop('checked', $(this).prop("checked"));
	setFlag = true;   
		$("#hiddenSelectAllFlag").val("selectAll");
		var savedsearchname = $("#savedName").val(); 
		
		getData(selectedUsers,setFlag,savedsearchname);
"""),format.raw/*341.1*/("""}"""),format.raw/*341.2*/(""");

function getSelectedUser()"""),format.raw/*343.27*/("""{"""),format.raw/*343.28*/("""
	"""),format.raw/*344.2*/("""$("input[name='searchname']:checked").each(function(i) """),format.raw/*344.57*/("""{"""),format.raw/*344.58*/("""
		"""),format.raw/*345.3*/("""selectedUsers.indexOf($(this).val()) === -1 ? selectedUsers.push($(this).val()) : console.log("This item already exists");	
		 """),format.raw/*346.4*/("""}"""),format.raw/*346.5*/("""); 

		var savedsearchname = $("#savedName").val(); 
		if($("#hiddenSelectAllFlag").val() == "selectAll")"""),format.raw/*349.53*/("""{"""),format.raw/*349.54*/("""
			"""),format.raw/*350.4*/("""setFlag = true;
			 
		"""),format.raw/*352.3*/("""}"""),format.raw/*352.4*/("""else"""),format.raw/*352.8*/("""{"""),format.raw/*352.9*/("""
			"""),format.raw/*353.4*/("""setFlag = false;
		"""),format.raw/*354.3*/("""}"""),format.raw/*354.4*/("""

	 	"""),format.raw/*356.4*/("""getData(selectedUsers,setFlag,savedsearchname);
		
"""),format.raw/*358.1*/("""}"""),format.raw/*358.2*/("""


"""),format.raw/*361.1*/("""function getData(selectedUsers,setFlag,savedsearchname)"""),format.raw/*361.56*/("""{"""),format.raw/*361.57*/("""
	"""),format.raw/*362.2*/("""if(setFlag == true)"""),format.raw/*362.21*/("""{"""),format.raw/*362.22*/("""
		"""),format.raw/*363.3*/("""selectedUsers = "all";
	"""),format.raw/*364.2*/("""}"""),format.raw/*364.3*/("""
	"""),format.raw/*365.2*/("""var urlLink="/getSelectedUserListforDatatable/"+ selectedUsers + "/"+setFlag+"/"+savedsearchname+""; 
	console.log(urlLink);
	 $('#selectedCustomers').dataTable("""),format.raw/*367.37*/("""{"""),format.raw/*367.38*/("""
	     """),format.raw/*368.7*/(""""bDestroy": true,
	     "processing": true,
	     "serverSide": true,
	     "scrollY": 300,
	     "paging": true,
	     "searching": false,
	     "ordering":false,
	     "ajax":  urlLink
	     """),format.raw/*376.7*/("""}"""),format.raw/*376.8*/(""");
	
"""),format.raw/*378.1*/("""}"""),format.raw/*378.2*/("""
"""),format.raw/*379.1*/("""function saveNewSmsTemplate()"""),format.raw/*379.30*/("""{"""),format.raw/*379.31*/("""
	 """),format.raw/*380.3*/("""var messageTemplate = $("#txtAreaId").val(); 
	 	 var msgAPI = $("#smsAPI").val();

	 alert(messageTemplate);
		var urlLink="/postSMSTemplate/"+messageTemplate+"/"+msgAPI+""; 
	   $.ajax("""),format.raw/*385.12*/("""{"""),format.raw/*385.13*/("""
	       """),format.raw/*386.9*/("""url: urlLink
	   """),format.raw/*387.5*/("""}"""),format.raw/*387.6*/(""").done(function () """),format.raw/*387.25*/("""{"""),format.raw/*387.26*/("""		   
		 """),format.raw/*388.4*/("""alert("saved template");
	   """),format.raw/*389.5*/("""}"""),format.raw/*389.6*/(""");
"""),format.raw/*390.1*/("""}"""),format.raw/*390.2*/("""







"""),format.raw/*398.1*/("""function sendBulkSMS()"""),format.raw/*398.23*/("""{"""),format.raw/*398.24*/("""
	"""),format.raw/*399.2*/("""//	var searchResultIds = $("#hiddenSelectAllFlag").val();
		var savedName 		= $("#savedName").val(); 
		var smstemplateId   =  $('#msgTypeID option:selected').val();

		
		console.log("after sendBulkSms"+selectedUsers);
		var searchResultIds = selectedUsers;

		dataInfo = """),format.raw/*407.14*/("""{"""),format.raw/*407.15*/("""
				"""),format.raw/*408.5*/("""savedName: savedName,
				smstemplateId: smstemplateId,
				searchResultIds: searchResultIds,
				setFlag: setFlag
		       """),format.raw/*412.10*/("""}"""),format.raw/*412.11*/(""";

		
		console.log(JSON.stringify(dataInfo));
		//var urlLink = "/postSMSBulk";
		
		 $.ajax("""),format.raw/*418.11*/("""{"""),format.raw/*418.12*/("""
			 """),format.raw/*419.5*/("""type: "POST",
	           url: "/postSMSBulk",
	           dataType: "json",
	           data: JSON.stringify(dataInfo),
	           contentType: "application/json",
	           success: function () """),format.raw/*424.34*/("""{"""),format.raw/*424.35*/("""
	             """),format.raw/*425.15*/("""alert("data sent to controller");
	           """),format.raw/*426.13*/("""}"""),format.raw/*426.14*/(""",

	       """),format.raw/*428.9*/("""}"""),format.raw/*428.10*/(""");
"""),format.raw/*429.1*/("""}"""),format.raw/*429.2*/("""


"""),format.raw/*432.1*/("""$(document).ready(function() """),format.raw/*432.30*/("""{"""),format.raw/*432.31*/("""
"""),format.raw/*433.1*/("""$('#SearchId').on('click',function()"""),format.raw/*433.37*/("""{"""),format.raw/*433.38*/("""
	 """),format.raw/*434.3*/("""var savedsearchname = $("#savedName").val(); 
	 var urlLink="/getCustomersListBySavedName/"+savedsearchname+""; 
	 console.log(urlLink);
	 $('#customerSearchTables').dataTable("""),format.raw/*437.40*/("""{"""),format.raw/*437.41*/("""
	     """),format.raw/*438.7*/(""""bDestroy": true,
	     "processing": true,
	     "serverSide": true,
	     "scrollY": 300,
	     "paging": true,
	     "searching": false,
	     "ordering":false,
	     "ajax":  urlLink
	     """),format.raw/*446.7*/("""}"""),format.raw/*446.8*/(""");
	 """),format.raw/*447.3*/("""}"""),format.raw/*447.4*/("""); 





$('#AddId').click(function()"""),format.raw/*453.29*/("""{"""),format.raw/*453.30*/(""" 
    """),format.raw/*454.5*/("""var vpara=$('#ParameteID :selected').val();
	var xpara = "("+vpara+")";
	var enteredText = $('#txtAreaId').val();
	$('.addedTxt').val(xpara);	
	var res = enteredText.concat(xpara);
	$('#txtAreaId').val(res);
"""),format.raw/*460.1*/("""}"""),format.raw/*460.2*/(""");


/*var mydropdown = document.getElementById('msgTypeID');
var mytextbox = document.getElementById('txtAreaId');
mydropdown.onchange = function()"""),format.raw/*465.33*/("""{"""),format.raw/*465.34*/("""
	 """),format.raw/*466.3*/("""mytextbox.focus();
     mytextbox.value = mytextbox.value  + this.value;
"""),format.raw/*468.1*/("""}"""),format.raw/*468.2*/("""*/


$('#msgTypeID').on('click',function()"""),format.raw/*471.38*/("""{"""),format.raw/*471.39*/("""
"""),format.raw/*472.1*/("""var	mydropdown = $('#msgTypeID option:selected').text();

$('#txtAreaId').val(mydropdown);
	
"""),format.raw/*476.1*/("""}"""),format.raw/*476.2*/(""");
$("#UploadID").click(function()"""),format.raw/*477.32*/("""{"""),format.raw/*477.33*/("""
	"""),format.raw/*478.2*/("""$(".dataTable_wrapper").show();
"""),format.raw/*479.1*/("""}"""),format.raw/*479.2*/(""");
$("#checkAll").change(function () """),format.raw/*480.35*/("""{"""),format.raw/*480.36*/("""
    """),format.raw/*481.5*/("""$("input[name=Searchname]").prop('checked', $(this).prop("checked"));
"""),format.raw/*482.1*/("""}"""),format.raw/*482.2*/(""");




//$(this).prop("checked"));
/*$(function()"""),format.raw/*488.15*/("""{"""),format.raw/*488.16*/("""
	"""),format.raw/*489.2*/("""$(".selectAll").change(function () """),format.raw/*489.37*/("""{"""),format.raw/*489.38*/("""
	    """),format.raw/*490.6*/("""var sel = $('input[name=searchname]').map(function(_, el) """),format.raw/*490.64*/("""{"""),format.raw/*490.65*/("""
	        """),format.raw/*491.10*/("""return $(el).val();
	    """),format.raw/*492.6*/("""}"""),format.raw/*492.7*/(""").get();
	    alert(sel);
    """),format.raw/*494.5*/("""}"""),format.raw/*494.6*/(""");
  """),format.raw/*495.3*/("""}"""),format.raw/*495.4*/(""");*/
  
$("input[name='SearchCust']").click(function()"""),format.raw/*497.47*/("""{"""),format.raw/*497.48*/("""
	"""),format.raw/*498.2*/("""var vsaerch = $(this).val();

	if(vsaerch=='Search Customer')"""),format.raw/*500.32*/("""{"""),format.raw/*500.33*/("""
		"""),format.raw/*501.3*/("""$("#SarchDivID").show();
		$("#uploadDivID").hide();
	$(".UploadCustIdDiv").hide();
	
	"""),format.raw/*505.2*/("""}"""),format.raw/*505.3*/("""
	"""),format.raw/*506.2*/("""else if(vsaerch=='Upload Customer')"""),format.raw/*506.37*/("""{"""),format.raw/*506.38*/("""
		"""),format.raw/*507.3*/("""$("#uploadDivID").show();
		$("#SarchDivID").hide();
		$(".SearchCustId").hide();
	
	"""),format.raw/*511.2*/("""}"""),format.raw/*511.3*/("""
	"""),format.raw/*512.2*/("""else"""),format.raw/*512.6*/("""{"""),format.raw/*512.7*/("""
	
		"""),format.raw/*514.3*/("""}"""),format.raw/*514.4*/("""
"""),format.raw/*515.1*/("""}"""),format.raw/*515.2*/(""");

$("#SearchId").click(function()"""),format.raw/*517.32*/("""{"""),format.raw/*517.33*/("""
"""),format.raw/*518.1*/("""$(".SearchCustId").show();
"""),format.raw/*519.1*/("""}"""),format.raw/*519.2*/(""");

$("#uploadcustId").click(function()"""),format.raw/*521.36*/("""{"""),format.raw/*521.37*/("""
"""),format.raw/*522.1*/("""$(".UploadCustIdDiv").show();
"""),format.raw/*523.1*/("""}"""),format.raw/*523.2*/(""");


/*$(document).ready(function() """),format.raw/*526.32*/("""{"""),format.raw/*526.33*/("""
  """),format.raw/*527.3*/("""//  $('#customerSearchTable').DataTable();
"""),format.raw/*528.1*/("""}"""),format.raw/*528.2*/(""");

$(document).ready(function() """),format.raw/*530.30*/("""{"""),format.raw/*530.31*/("""
  """),format.raw/*531.3*/("""//  $('#customerUploadTable').DataTable();
"""),format.raw/*532.1*/("""}"""),format.raw/*532.2*/(""");*/
"""),format.raw/*533.1*/("""}"""),format.raw/*533.2*/(""");
</script>"""))
      }
    }
  }

  def render(dealercode:String,dealerName:String,userName:String,oem:String,smsTemplateList:List[SMSTemplate],msgParameters:List[MessageParameters],savedSearchList:List[SavedSearchResult]): play.twirl.api.HtmlFormat.Appendable = apply(dealercode,dealerName,userName,oem,smsTemplateList,msgParameters,savedSearchList)

  def f:((String,String,String,String,List[SMSTemplate],List[MessageParameters],List[SavedSearchResult]) => play.twirl.api.HtmlFormat.Appendable) = (dealercode,dealerName,userName,oem,smsTemplateList,msgParameters,savedSearchList) => apply(dealercode,dealerName,userName,oem,smsTemplateList,msgParameters,savedSearchList)

  def ref: this.type = this

}


}

/**/
object messageTemplateSuperAdmin extends messageTemplateSuperAdmin_Scope0.messageTemplateSuperAdmin
              /*
                  -- GENERATED --
                  DATE: Thu Dec 28 15:19:46 IST 2017
                  SOURCE: D:/CRMNEW/28th Dec/crm/app/views/messageTemplateSuperAdmin.scala.html
                  HASH: 7fc4bdc1f23589997caaf2ce61e1f51477071a5f
                  MATRIX: 872->5|1148->185|1178->190|1249->253|1288->255|1316->257|1372->287|1386->293|1437->324|3714->2574|3768->2612|3807->2613|3869->2647|3912->2663|3937->2679|3973->2694|4003->2697|4028->2713|4065->2728|4137->2769|4167->2772|5548->4125|5577->4126|5624->4144|5654->4145|5684->4146|5714->4147|5777->4181|5807->4182|5837->4183|5867->4184|5897->4185|5927->4186|5973->4203|6003->4204|6054->4226|6084->4227|6114->4228|6144->4229|6175->4231|6205->4232|6235->4233|6265->4234|6295->4235|11537->9449|11592->9487|11632->9488|11694->9521|11738->9537|11764->9553|11797->9564|11828->9567|11854->9583|11894->9600|11966->9640|12010->9656|12312->9930|12367->9968|12407->9969|12470->10003|12514->10019|12542->10037|12586->10059|12617->10062|12646->10080|12691->10102|12764->10143|12802->10153|16484->13804|16516->13808|16594->13857|16624->13858|16654->13859|16740->13916|16770->13917|16801->13920|17072->14163|17101->14164|17162->14196|17192->14197|17223->14200|17307->14255|17337->14256|17369->14260|17525->14388|17554->14389|17691->14497|17721->14498|17754->14503|17807->14528|17836->14529|17868->14533|17897->14534|17930->14539|17978->14559|18007->14560|18042->14567|18123->14620|18152->14621|18186->14627|18270->14682|18300->14683|18331->14686|18379->14705|18409->14706|18441->14710|18494->14735|18523->14736|18554->14739|18746->14902|18776->14903|18812->14911|19041->15112|19070->15113|19105->15120|19134->15121|19164->15123|19222->15152|19252->15153|19284->15157|19505->15349|19535->15350|19573->15360|19619->15378|19648->15379|19696->15398|19726->15399|19764->15409|19822->15439|19851->15440|19883->15444|19912->15445|19956->15461|20007->15483|20037->15484|20068->15487|20378->15768|20408->15769|20442->15775|20599->15903|20629->15904|20758->16004|20788->16005|20822->16011|21055->16215|21085->16216|21130->16232|21206->16279|21236->16280|21277->16293|21307->16294|21339->16298|21368->16299|21402->16305|21460->16334|21490->16335|21520->16337|21585->16373|21615->16374|21647->16378|21855->16557|21885->16558|21921->16566|22150->16767|22179->16768|22213->16774|22242->16775|22314->16818|22344->16819|22379->16826|22621->17040|22650->17041|22832->17194|22862->17195|22894->17199|22997->17274|23026->17275|23100->17320|23130->17321|23160->17323|23285->17420|23314->17421|23378->17456|23408->17457|23439->17460|23500->17493|23529->17494|23596->17532|23626->17533|23660->17539|23759->17610|23788->17611|23872->17666|23902->17667|23933->17670|23997->17705|24027->17706|24062->17713|24149->17771|24179->17772|24219->17783|24273->17809|24302->17810|24362->17842|24391->17843|24425->17849|24454->17850|24539->17906|24569->17907|24600->17910|24692->17973|24722->17974|24754->17978|24873->18069|24902->18070|24933->18073|24997->18108|25027->18109|25059->18113|25176->18202|25205->18203|25236->18206|25268->18210|25297->18211|25332->18218|25361->18219|25391->18221|25420->18222|25486->18259|25516->18260|25546->18262|25602->18290|25631->18291|25701->18332|25731->18333|25761->18335|25820->18366|25849->18367|25917->18406|25947->18407|25979->18411|26051->18455|26080->18456|26144->18491|26174->18492|26206->18496|26278->18540|26307->18541|26341->18547|26370->18548
                  LINES: 27->3|32->3|34->5|34->5|34->5|35->6|35->6|35->6|35->6|84->55|84->55|84->55|85->56|85->56|85->56|85->56|85->56|85->56|85->56|86->57|87->58|118->89|118->89|118->89|118->89|118->89|118->89|118->89|118->89|118->89|118->89|118->89|118->89|118->89|118->89|118->89|118->89|118->89|118->89|118->89|118->89|118->89|118->89|118->89|248->219|248->219|248->219|249->220|249->220|249->220|249->220|249->220|249->220|249->220|250->221|253->224|263->234|263->234|263->234|264->235|264->235|264->235|264->235|264->235|264->235|264->235|265->236|267->238|357->328|359->330|361->332|361->332|361->332|363->334|363->334|364->335|370->341|370->341|372->343|372->343|373->344|373->344|373->344|374->345|375->346|375->346|378->349|378->349|379->350|381->352|381->352|381->352|381->352|382->353|383->354|383->354|385->356|387->358|387->358|390->361|390->361|390->361|391->362|391->362|391->362|392->363|393->364|393->364|394->365|396->367|396->367|397->368|405->376|405->376|407->378|407->378|408->379|408->379|408->379|409->380|414->385|414->385|415->386|416->387|416->387|416->387|416->387|417->388|418->389|418->389|419->390|419->390|427->398|427->398|427->398|428->399|436->407|436->407|437->408|441->412|441->412|447->418|447->418|448->419|453->424|453->424|454->425|455->426|455->426|457->428|457->428|458->429|458->429|461->432|461->432|461->432|462->433|462->433|462->433|463->434|466->437|466->437|467->438|475->446|475->446|476->447|476->447|482->453|482->453|483->454|489->460|489->460|494->465|494->465|495->466|497->468|497->468|500->471|500->471|501->472|505->476|505->476|506->477|506->477|507->478|508->479|508->479|509->480|509->480|510->481|511->482|511->482|517->488|517->488|518->489|518->489|518->489|519->490|519->490|519->490|520->491|521->492|521->492|523->494|523->494|524->495|524->495|526->497|526->497|527->498|529->500|529->500|530->501|534->505|534->505|535->506|535->506|535->506|536->507|540->511|540->511|541->512|541->512|541->512|543->514|543->514|544->515|544->515|546->517|546->517|547->518|548->519|548->519|550->521|550->521|551->522|552->523|552->523|555->526|555->526|556->527|557->528|557->528|559->530|559->530|560->531|561->532|561->532|562->533|562->533
                  -- GENERATED --
              */
          