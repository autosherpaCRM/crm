
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object serviceOutBound_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class serviceOutBound extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template9[List[String],List[Workshop],Customer,Vehicle,List[Location],CallInteraction,List[ServiceTypes],WyzUser,Service,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.4*/(statesList:List[String],workshopList:List[Workshop],customerData:Customer,vehicleData:Vehicle,locationList:List[Location],interOfCall:CallInteraction,servicetypeList:List[ServiceTypes],userData:WyzUser,latestService:Service):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.229*/("""
		  """),format.raw/*2.5*/("""<div style="display:none;" class="animated  bounceInRight" id="serviceBookDiv">
		  
	<input type="hidden" name="selectedFile" value=""""),_display_(/*4.51*/{if(interOfCall.getSrdisposition()!=null){interOfCall.getSrdisposition().getCallDispositionData().getDisposition()}}),format.raw/*4.167*/("""">  
<!-- BOOK MY SERVICE -->		
	  <div class="col-md-12">
			  <div class="row">
				<div class="col-md-3">
				  <div class="form-group">
					<label for="">Select Vehicle</label>
					<select class="form-control" id="vehicle" name="vehicleId_SB">
					  
						"""),_display_(/*13.8*/for(post <- customerData.getVehicles()) yield /*13.47*/{_display_(Seq[Any](format.raw/*13.48*/("""
                              
                              """),format.raw/*15.31*/("""<option value=""""),_display_(/*15.47*/post/*15.51*/.vehicle_id),format.raw/*15.62*/("""">"""),_display_(/*15.65*/post/*15.69*/.vehicleRegNo),format.raw/*15.82*/("""</option>
                              
									""")))}),format.raw/*17.11*/("""
					  
					  
					  
						
					"""),format.raw/*22.6*/("""</select>
				  </div>
				  </div>
				  <div class="col-md-3">
				  <div class="form-group">
					<label for="">Select City</label>
					<select class="form-control" id="city" name="cityName" onchange="ajaxCallToLoadWorkShopByCity();">			  
							                              
                              """),_display_(/*30.32*/for(loc <- locationList) yield /*30.56*/{_display_(Seq[Any](format.raw/*30.57*/("""
					
					"""),_display_(/*32.7*/{if(loc.getName() ==userData.getLocation().getName())
					{
									<option value="select">--Select--</option>
									<option value={userData.getLocation().getName()} selected="selected">{userData.getLocation().getName()}</option>
									}else {
										<option value={loc.getName} >{loc.getName}</option>
										}
									}),format.raw/*39.11*/("""
                              
									""")))}),format.raw/*41.11*/("""
                              
                              
                            
                              
									
					"""),format.raw/*47.6*/("""</select>
				  </div>
				</div>
				
					<div class="col-md-3">
				  <label for="">Service Booked Type<i class="lblStar">*</i></label>
				  <select class="form-control" id="serviceBookedTypeDisposition" name="serviceBookedType" onchange="">
						<option value="0">--Select--</option>
						"""),_display_(/*55.8*/for(serlist <- servicetypeList) yield /*55.39*/{_display_(Seq[Any](format.raw/*55.40*/("""
					
					"""),_display_(/*57.7*/{if(serlist.serviceTypeName ==(if(interOfCall.getServiceBooked()!=null){interOfCall.getServiceBooked().getServiceBookedType()}))
					{
									
									<option value={serlist.id.toString()} selected="selected">{serlist.serviceTypeName}</option>
									}else {
										<option value={serlist.id.toString()} >{serlist.serviceTypeName}</option>
										}
									}),format.raw/*64.11*/("""
                              
									""")))}),format.raw/*66.11*/("""	

				 """),format.raw/*68.6*/("""</select>
				</div>
				<div class="col-md-3">
				  <label for="">Workshop<i class="lblStar">*</i></label>
				  <select class="form-control" id="workshop" name="workshopId" onchange="ajaxAutoSASelectionList('workshop','date12345','serviceAdvisor'); " >
					<option value=0>--Select--</option>
					"""),_display_(/*74.7*/for(worklist <- workshopList) yield /*74.36*/{_display_(Seq[Any](format.raw/*74.37*/("""
					
					"""),_display_(/*76.7*/{if(worklist.workshopName ==(if(interOfCall.getServiceBooked()!=null){interOfCall.getServiceBooked().getWorkshop().getWorkshopName()})){
									
									<option value={worklist.id.toString()} selected="selected">{worklist.workshopName}</option>
									}else {
										<option value={worklist.id.toString()} >{worklist.workshopName}</option>
										}
									}),format.raw/*82.11*/("""
                              
                             
                              
									""")))}),format.raw/*86.11*/("""
					
				
				  """),format.raw/*89.7*/("""</select>
				</div>
				
				</div>
				 </div>
			  
			
		   <div class="col-md-12">
			  <div class="row">
			  <div class="col-md-3">
			  
			  
				  <div class="form-group">
					<label class="control-label" for="inputGroup">Select Date<i class="lblStar">*</i></label>
					<div class="input-group">
					<input type="text" name="serviceScheduledDate" class="form-control SelectDateDP" id="date12345" onchange="ajaxAutoSASelectionList('workshop','date12345','serviceAdvisor');" value=""""),_display_(/*104.187*/{if(interOfCall.getServiceBooked()!=null){interOfCall.getServiceBooked().getServiceScheduledDateStrSqlFor()}}),format.raw/*104.296*/("""" readonly>					 
					  <span class="input-group-addon" data-toggle="modal" data-target="#sa_modal" onclick="ajaxCallToLoadWorkshopSummary();"> <i style="font-size:19px; color:red;" class="fa">&#xf274;</i> </span>
					  </div>
				  </div>
				</div>
				<div class="col-md-3">
				<div class="form-group">
				  <label for="serviceScheduledTime">Select Time<i class="lblStar">*</i></label>
				  <input type="text" name="serviceScheduledTime" class="form-control timePickRange7to19" id="CommittedTimes" value=""""),_display_(/*112.122*/{if(interOfCall.getServiceBooked()!=null){interOfCall.getServiceBooked().getServiceScheduledTimeStrSqlFor()}}),format.raw/*112.231*/("""" readonly>
				</div>
		 </div>
		 <div class="col-md-3">
				  <div class="form-group">
				  <label>&nbsp;</label><br/>
					<button type="button" class="btn btn-primary btn-block" onClick="return ajaxAutoSASelection('workshop','date12345','serviceAdvisor')">Recommend SA</button>
					  
				  </div>
				</div>
				<div class="col-md-3">
				  <div class="form-group">
					<label for="">Assigned To(SA)<i class="lblStar">*</i></label>
			
					<select class="form-control insuranceAgent" id="serviceAdvisor"  name="serviceAdvisorId" onChange="ajaxAutoSAManualchange('workshop','date12345','serviceAdvisor');" >						
					"""),_display_(/*127.7*/{if(interOfCall.getServiceBooked()!=null){
					
					<option value={interOfCall.getServiceBooked().getServiceAdvisor().getAdvisorId().toString()} selected="selected">{interOfCall.getServiceBooked().getServiceAdvisor().getAdvisorName()}</option>
					
					}}),format.raw/*131.8*/("""
					"""),format.raw/*132.6*/("""</select>
				  </div>
				</div>
				<!--
				<div class="col-md-3">
				  <div class="form-group">
					<button type="button" class="btn btn-primary" onClick="return ajaxAutoSASelectionList('workshop','date12345','serviceAdvisor');">Change SA</button>
				  </div>
				</div>	
				-->
				
			  </div>
			
			
			<div class="pull-right">
			<button type="button" class="btn btn-primary" id="backtoMain">Back</button>
			<button type="button" class="btn btn-primary" id="nextToCustomerDrive">Next</button>
			</div>
			</div>
			</div>
			<div style="display:none;" class="animated  bounceInRight" id="CustomerDriveInDiv">
			<div class="col-md-12">
			<label>Choose one of the below option:</label>
		<div class="radio">
  <label>
<input type="radio" name="typeOfPickup" value="Customer Drive-In" id="CustomerDriveInID" >
Customer Drive-In </label>
</div>

<div class="radio">
  <label>
<input type="radio" name="typeOfPickup" value="true" id="PickupDropRequired" >
Pickup Drop Required</label>
</div>
<div class="radio">
		  <label>
			<input type="radio" name="typeOfPickup" value="Maruthi Mobile Support" id="MaruthiMobileSupportIn" >
			Mobile Service Support</label>
		</div>
		<div class="animated  bounceInRight" id="MSSSelectDiv" style="display:none;">
				
				<div class="col-md-3">
				  <div class="form-group">
					<label for="">Select Address</label>
					<select class="form-control" id="AddressMSSId" name="serviceBookingAddress">
					
					"""),_display_(/*178.7*/for( addre_list <- customerData.getAddresses()) yield /*178.54*/{_display_(Seq[Any](format.raw/*178.55*/("""
											
									"""),_display_(/*180.11*/{if(addre_list.getConcatenatedAdress() !=null)
									
									<option value={addre_list.getConcatenatedAdress()}>{addre_list.getConcatenatedAdress()}</option>
									else 
										<option value={addre_list.getConcatenatedAddressWith()}>{addre_list.getConcatenatedAddressWith()}</option>
										}),format.raw/*185.12*/("""
									""")))}),format.raw/*186.11*/("""					  
					
					"""),format.raw/*188.6*/("""</select>
				  </div>
				  </div>
				   <div class="col-md-3 animated  bounceInRight">
				   <div class="form-group">
				   <br>
				   <button type="button" class="btn btn-success" data-toggle="modal" data-target="#AddNewAddressPopup">Add New Address</button>
					  <button type="button" class="btn btn-success" style="display:none;" id="AssignBtnBkreview" data-toggle="modal" data-target="#DriverAllcationPOPUp" onclick="ajaxAssignBtnBkreview('workshop','date12345');">Assign Driver</button>
					
						</div>				   
			
			  </div>
			
				  
				  </div>
			<!--  	  <div style="display:none;" id="pickupDiv">
		  <div class="animated  bounceInRight">
			<div class="col-md-3">
			  <div class="form-group">
				<label for="pickUpAddress"><b>Pickup Address</b></label>
				<select class="form-control" name="pickUpAddress" id="pickUpAddPOPUP1" >
				  """),_display_(/*209.8*/for(addre_list <- customerData.getAddresses()) yield /*209.54*/{_display_(Seq[Any](format.raw/*209.55*/("""
                          """),format.raw/*210.27*/("""<option value=""""),_display_(/*210.43*/addre_list/*210.53*/.getAddressLine1()),format.raw/*210.71*/(""","""),_display_(/*210.73*/addre_list/*210.83*/.getAddressLine2()),format.raw/*210.101*/(""","""),_display_(/*210.103*/addre_list/*210.113*/.getAddressLine3()),format.raw/*210.131*/(""","""),_display_(/*210.133*/addre_list/*210.143*/.getCity()),format.raw/*210.153*/(""","""),_display_(/*210.155*/addre_list/*210.165*/.getPincode()),format.raw/*210.178*/("""">"""),_display_(/*210.181*/addre_list/*210.191*/.getAddressLine1()),format.raw/*210.209*/(""","""),_display_(/*210.211*/addre_list/*210.221*/.getAddressLine2()),format.raw/*210.239*/(""","""),_display_(/*210.241*/addre_list/*210.251*/.getAddressLine3()),format.raw/*210.269*/(""","""),_display_(/*210.271*/addre_list/*210.281*/.getCity()),format.raw/*210.291*/(""","""),_display_(/*210.293*/addre_list/*210.303*/.getPincode()),format.raw/*210.316*/("""</option>
                         
						  """)))}),format.raw/*212.10*/("""
				"""),format.raw/*213.5*/("""</select>
			  </div>
			</div>
			
			<div class="col-md-3">
			
				<div class="form-group"><br />
				   <button type="button" class="btn btn-success" data-toggle="modal" data-target="#AddNewAddressPopup">Add New Address</button>
				
				</div>
			  
			</div>
		   </div>
			  </div>-->
			  <div class="row">
				<div class="col-md-3">
							  <div class="form-group">
								<label for="memarksPickUpADD">Remarks</label>
								<textarea type="text" name="remarksList[0]" class="form-control" id="memarksPickUpADD"></textarea>
							  </div>
							</div>
			  </div>
	  <div class="pull-right">
	  <button type="button" class="btn btn-primary" id="BackToCunstomerDrive">Back</button>
	   <button type="button" class="btn btn-primary" id="NextToLead">Next</button>
	  </div>
	</div>
		  </div>
		   <div class="col-md-12 animated  bounceInRight" style="display:none;" id="finalDiv1">
			<label>Capture Lead : Is there an Upsell Opportunity.</label>
		   <br>
		   
			<div class="radio-inline">
			  <label>
				<input type="radio" name="LeadYes"  value="Capture Lead Yes" id="LeadYesID" >
				Yes</label>
			</div>
			<div class="radio-inline">
			  <label>
				<input type="radio" name="LeadYes" value="Capture Lead No" id="LeadNoID" >
				No</label>
			</div>
			<div class="row animated  bounceInRight" style="display:none;" id="LeadDiv">
			<div class="col-md-12">
			  <div class="col-md-6">
			  
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="LeadClass myOutCheckbox" name="upsellLead[0].upsellId" value="1" id="InsuranceIDCheck" onClick="loadLeadBasedOnUserLocation('InsuranceIDCheck','insuranceLead1')">
					Insurance</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="InsuranceSelect">
				 
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[0].taggedTo" id="insuranceLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments1" name="upsellLead[0].upsellComments"></textarea>
					</div>
				 
				</div>
				
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="LeadClass myOutCheckbox" name="upsellLead[1].upsellId" value="2" id="MaxicareIDCheck" onClick="loadLeadBasedOnUserLocation('MaxicareIDCheck','maxicareLead')">
					Warranty / EW</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="MaxicareSelect">
				 
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[1].taggedTo" id="maxicareLead">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments1" name="upsellLead[1].upsellComments"></textarea>
					</div>
				 
				</div>
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="LeadClass myOutCheckbox" name="upsellLead[2].upsellId" value="4" id="ShieldID" onClick="loadLeadBasedOnUserLocation('ShieldID','warrantyLead1')">
					Re-Finance / New Car Finance</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="ShieldSelect">
					
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[2].taggedTo" id="warrantyLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments2" name="upsellLead[2].upsellComments"></textarea>
					</div>
				 
				</div>
				<!-- <div class="checkbox">
				  <label>
					<input type="checkbox" class="LeadClass myOutCheckbox" name="upsellLead[1].upSellType" value="Warranty / EW" id="WARRANTYID" >
					Warranty / EW</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="WARRANTYSelect">
					
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[1].taggedTo" id="warrantyLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments2" name="upsellLead[1].upsellComments"></textarea>
					</div>
				 
				</div> -->
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="VAS myOutCheckbox" name="upsellLead[3].upsellId" value="3" id="VASID" onClick="loadLeadBasedOnUserLocation('VASID','vASLead1')"/>
					VAS</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="VASTagToSelect">
				  
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[3].taggedTo" id="vASLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments3" name="upsellLead[3].upsellComments"></textarea>
					</div>
				</div>
				</div>
				<!-- <div class="checkbox">
				  <label>
					<input type="checkbox" class="LeadClass myOutCheckbox" name="upsellLead[3].upSellType" value="Re-Finance / New Car Finance" id="ReFinanceIDCheck" />
					Re-Finance / New Car Finance</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="ReFinanceSelect">
				  
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[3].taggedTo" id="reFinanceLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments4" name="upsellLead[3].upsellComments"></textarea>
					</div>
				 </div>
				</div> -->
				<div class="col-md-6">
				<!-- <div class="checkbox">
				  <label>
					<input type="checkbox" class="Loan myOutCheckbox" name="upsellLead[4].upSellType" value="Sell Old Car" id="LoanID" />
					Sell Old Car</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="LoanSelect">
				  
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[4].taggedTo" id="sellOldCarLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments5" name="upsellLead[4].upsellComments"></textarea>
					</div>
				  </div> -->
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="EXCHANGE myOutCheckbox" name="upsellLead[4].upsellId" value="6" id="EXCHANGEID" onClick="loadLeadBasedOnUserLocation('EXCHANGEID','buyNewCarLead1')"/>
					Buy New Car / Exchange</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="EXCHANGEIDSelect">
				  
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[4].taggedTo" id="buyNewCarLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments6" name="upsellLead[4].upsellComments"></textarea>
					</div>
				 	</div>
				  <div class="checkbox">
				  <label>
					<input type="checkbox" class="UsedCar myOutCheckbox" name="upsellLead[5].upsellId" value="9" id="UsedCarID" onClick="loadLeadBasedOnUserLocation('UsedCarID','usedCarLead1')"/>
					Used Car</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="UsedCarSelect">
				  
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[5].taggedTo" id="usedCarLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments7" name="upsellLead[5].upsellComments"></textarea>
					</div>
				 
				</div>
				 <div class="checkbox">
				  <label>
					<input type="checkbox" class="UsedCar myOutCheckbox" name="upsellLead[6].upsellId" value="5" id="RoadSideAsstID" onClick="loadLeadBasedOnUserLocation('RoadSideAsstID','RoadSideAssiLead1')"/>
					Sell Old Car</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="RoadSideAssiSelect">
				  
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[6].taggedTo" id="RoadSideAssiLead1">
						<option >Select</option>
					  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments7" name="upsellLead[6].upsellComments"></textarea>
					</div>
				 
				</div>
		   
			  </div>
			</div>
			 
			</div>
			  <div class="pull-right" >
			<button type="button" class="btn btn-primary" id="BackToLead">Back</button>
			  <button type="button" class="btn btn-primary" value="" id="NextToCustFeedBack"/>Next</button>
			</div>
			</div>
		  
				
			<div class="row animated  bounceInRight" id="CustFeedBack" style="display:none;">
				  <div class="col-md-12">
				  
					<label>Does Customer has a feedback / compliant ?</label>
					<br>
					
					  <label for="feedbackYes" class="radio-inline">
						<input type="radio" name="userfeedback" onclick="loadLeadBasedOnLocationDepartment();" value="feedback Yes" id="feedbackYes" >
						Yes</label>
					
					<label class="radio-inline">
						<input type="radio" name="userfeedback" value="feedback No" id="feedbackNo" >
						No</label> 
				
			   
				<div class="row animated  bounceInRight" style="display:none;" id="feedbackDIV"><br>
				 <div class="col-md-12">
				  <div class="col-md-3">
					<label for="">Select Department<i class="lblStar">*</i></label>
					<select class="form-control selected_department" onchange="ajaxLeadTagByDepartment();" id="selected_department1" name="departmentForFB">
					 
					</select>
				  </div>
				  <div class="col-md-3">
					<label for="">Tag to</label>
					<select class="form-control" name="complaintOrFB_TagTo" id="LeadTagsByLocation1" name="complaintOrFB_TagTo">
					
					"""),_display_(/*488.7*/{if(interOfCall.getSrdisposition()!=null){
					
					<option value={interOfCall.getSrdisposition().getComplaintOrFB_TagTo()} selected="selected">{interOfCall.getSrdisposition().getComplaintOrFB_TagTo()}</option>
					
					}}),format.raw/*492.8*/("""	  
					"""),format.raw/*493.6*/("""</select>
				  </div>
				
				  <div class="col-md-3">
					<label for="comments">Feedback/Comment</label>
					<textarea class="form-control" rows="1" id="commentsOfFB" name="remarksOfFB"></textarea>
				  </div>
				  <br>
				</div>
				  </div>
				  <div class="pull-right" >
			<button type="button" class="btn btn-primary" id="BackToUpsell">Back</button>
			  <button type="button" class="btn btn-primary" value="" id="NextToLastQuestion"/>Next</button>
			</div>
		  </div>
		  </div>

<div class="row animated  bounceInRight" id="LastQuestion" style="display:none;">
				  <div class="col-md-12">
				  <div class="col-md-12">
					<label>May I please take a moment to update your contact information? This will help us serve you better in future.</label>
					<br>
					<div class="radio-inline">
					  <label for="feedbackYes">
						<input type="radio" name="CustomerFeedBackYes" value="Customer Yes" id="CustomerYes" >
						Yes</label>
					</div>
					<div class="radio-inline">
					  <label for="feedbackNo">
						<input type="radio" name="CustomerFeedBackYes" value="Customer No" id="CustomerNo" class="CustomerNo" checked>
						No</label> 
					</div>
				  </div>
				  </div>
				  <div class="pull-right" >
			<button type="button" class="btn btn-primary" id="BackToCustomerFeedback">Back</button>
			  <button type="submit" class="btn btn-primary" value="bookMyservice" name="typeOfsubmit" id="bookMyserviceSubmit"/>Submit</button>
			</div>
				  </div>
				    
				  <div class="pull-right animated  bounceInRight" style="display:none" id="ConfirmedSubmit">
			
			  <button type="submit" class="btn btn-primary" value="Confirmed" name="typeOfsubmit" id="newServiceBook"/>Submit</button>
			</div>
				  <!-- Call Me later -->
				  
				  
				   <div class="row animated  bounceInRight" style="display:none;" id="callMeLattteDiv">
		   <div class="col-md-12">
			<div class="col-md-3">
			  <div class="form-group">
				<label for="followUpDate">Follow Up Date<i class="lblStar">*</i></label>
				<input type="text" name="followUpDate" class="form-control" id="FollowUpDate" readonly>
			  </div>
			</div>
			<div class="col-md-3">
			  <div class="form-group">
				<label for="followUpTime">Follow Up Time<i class="lblStar">*</i></label>
				<input type="text" name="followUpTime" class="form-control timepicker_7" id="FollowUpTime" readonly>
			  </div>
			</div>
			<div class="col-md-3">
			  <div class="form-group">
				<label for="followUpRemarks">Remarks</label>
				<textarea type="text" name="remarksList[1]" class="form-control" id="followUpRemarks"></textarea>
			  </div>
			</div>
			<br>
			  <div class="pull-right">
			  <button type="button" class="btn btn-primary" value="" id="CallaterBack">Back</button>
				<button type="submit" class="btn btn-primary" value="callMeLater" id="callMeLaterSubmit" name="typeOfsubmit">Submit</button>
			</div>
		  </div>
		
		  </div>
		  
		    <div class="row animated  bounceInRight" style="display:none;" id="CancelServiceBk">
				<div class="col-md-12">
			<div class="col-sm-12" id="SMRInteractionFirst" >
		  <label for="">Do you want to cancel ?</label> <br />
			 
			  <div class="radio-inline">
				<label>
				  <input type="radio" name="ServiceBookingCancel" value="Service Booking">
				  Service Booking </label>
			  </div>
			  
			  """),_display_(/*580.7*/if(interOfCall.getServiceBooked() !=null)/*580.48*/{_display_(Seq[Any](format.raw/*580.49*/("""
			  	
			  	"""),_display_(/*582.8*/if(interOfCall.getServiceBooked().getPickupDrop() !=null)/*582.65*/{_display_(Seq[Any](format.raw/*582.66*/("""
			  """),format.raw/*583.6*/("""<div class="radio-inline">
				<label>
				  <input type="radio" name="ServiceBookingCancel" value="Pickup Drop">
				  Pickup Drop</label>
			  </div>
			  
			  """)))})))}),format.raw/*589.8*/("""
		"""),format.raw/*590.3*/("""</div>
			<br>
			  <div class="pull-right animated  bounceInRight" style="display:none;" id="ServeBookCancelBTN">
			  <button type="submit" class="btn btn-primary" value="pickUpCancel" name="typeOfsubmit">Submit</button>
				</div>
		  </div>
		
		  </div>
		  
		  
		  
				  
				  <!-- Service Not Required -->
				   <div class="row animated  bounceInRight" style="display:none;" id="alreadyserviceDIV" > 
				<!--No Service Required-->
				<div class="col-md-12" >
					<div class="col-md-12">
					  <div class="form-group">
					 
						<div class="checkbox AlreadyServiced">
						  <label>
							<input type="checkbox" class="NoService" name="noServiceReason" value="Already Serviced" id="AlreadyServiced" >
							Already Serviced</label>
						</div>
						<div class="animated  bounceInRight alreadyservicedDiv1" style="display:none;" id="alreadyservicedDiv1">
						  <div class="col-md-12">
							<div class="radio ServicedMyDealer">
							  <label>
								<input type="radio" name="reasonForHTML" onclick="workshopNameList();" value="Serviced At My Dealer" id="ServicedMyDealer" >
								Serviced At My Dealer</label>
							</div>
							<div class="row animated  bounceInRight" style="display:none;" id="ServicedMyDealerDiv">
							  <div class="col-md-12">
								<div class="col-md-3">
								  <label for=""><b>Last Service Date<b></label>
								 <input type="text" name="lastServiceDate" value=""""),_display_(/*625.60*/{latestService.getLastServiceDate()}),format.raw/*625.96*/("""" class="datepickerPrevious form-control" readonly/>
								</div>
								<div class="col-md-3">
								  <label for=""><b>Last service Workshop<b></label>
									<select class="form-control" id="lastServiceWorkshopList">
								  	 	<option value="0">--Select--</option>				  
								  </select>
								  
								</div>
								 <div class="col-md-3">
								  <label for=""><b>Last Mileage<b></label>
								  <input type="text" name="Last Mileage" value="" class="form-control numberOnly" maxlength="6"/>
								</div>
							<div class="col-md-3">
							  <div class="form-group">
								<label for="memarksSerMyDelar">Remarks</label>
								<textarea type="text" name="remarksList[2]" class="form-control" id="memarksSerMyDelar"></textarea>
							  </div>
							</div>
							  </div>
							</div>
							<div class="radio ServicedOtherDealer">
							  <label>
								<input type="radio" name="reasonForHTML" value="Serviced At Other Dealer" id="ServicedOtherDealer" >
								Serviced At Other Dealer</label>
							</div>
							<div class="row animated  bounceInRight" style="display:none;" id="ServicedAtOtherDealerDiv" >
							  <div class="col-md-12">
								
								 <div class="col-md-12">
									<div class="radio">
									  <label>
										<input type="radio" name="ServicedAtOtherDealerRadio" value="Autorized workshop" id="Autorizedworkshopid" >
										Authorized workshop</label>
									</div>
								  </div>
							   
								<div class="animated  bounceInRight" style="display:none;" id="AutorizedworkshopDIV">
								  <div class="col-md-12">
								   
									  <div class="col-md-2">
										<label for="dealerName"><b>Dealer Name<b></label>
										<input type="text" name="dealerName" class="form-control textOnlyAccepted" />
									  </div>
								  
									  <div class="col-md-2">
										<label for="dateOfService"><b>Date of Service<b></label>
										<input type="text" name="dateOfService" class="datepickerPrevious form-control" readonly/>
									  </div>
								 
									  <div class="col-md-2">
										<label for="mileageAtService"><b>Mileage(Km)<b></label>
										<input type="text" name="mileageAtService" class="form-control numberOnly" maxlength="6" />
									  </div>
									
									  <div class="col-md-2">
										<label for="serviceType"><b>Type of Service<b></label>
										
										<select class="form-control" id="" name="serviceType">
										<option value="select">--SELECT--</option>
										"""),_display_(/*685.12*/for(serlist <- servicetypeList) yield /*685.43*/{_display_(Seq[Any](format.raw/*685.44*/("""
											
											"""),format.raw/*687.12*/("""<option value=""""),_display_(/*687.28*/serlist/*687.35*/.serviceTypeName),format.raw/*687.51*/("""">"""),_display_(/*687.54*/serlist/*687.61*/.serviceTypeName),format.raw/*687.77*/("""</option>
										""")))}),format.raw/*688.12*/("""
										 
									
									"""),format.raw/*691.10*/("""</select>
									  </div>
									  <div class="col-md-2">
							  <div class="form-group">
								<label for="memarksAuthoDelar">Remarks</label>
								<textarea type="text" name="remarksList[3]" class="form-control" id="memarksAuthoDelar"></textarea>
							  </div>
							</div>
									  
								   
								  </div>
								  <div class="animated  bounceInRight">
							
									<div class="col-md-3">
							  <label>
								<input type="checkbox" name="checkedwithDMS" value="Checked with DMS" id="CheckedwithDMS" >
								I Have Verified with DMS<i style="color:red">*</i></label>
							</div>
						 
						  </div>
								</div>
							   
								  <div class="col-md-12">
									<div class="radio">
									  <label>
										<input type="radio" name="ServicedAtOtherDealerRadio" value="Non Autorized workshop" id="NonAutorizedworkshopid" >
										Non Authorized workshop</label>
									</div>
								  </div>
							  
								<div class="animated  bounceInRight" style="display:none;" id="NonAutorizedworkshopDiv">
								  <div class="col-md-12">
								  <div class="col-md-2">
										<label for="dealerName"><b>Workshop Name<b></label>
										<input type="text" name="dealerNameNonAuth" class="form-control textOnlyAccepted" />
									  </div>
									<div class="col-md-2">
									  <label for="dateOfServiceNonAuth"><b>Date of Service<b></label>
									  <input type="text" name="dateOfServiceNonAuth" class="datepickerPrevious form-control" readonly/>
									</div>
									<div class="col-md-2">
									  <label for="mileageAsOnDate"><b>Mileage(as on date)<b></label>
									  <input type="text" name="mileageAsOnDate" class="form-control numberOnly" maxlength="6" />
									</div>
									<div class="col-md-2">
										<label for="serviceType"><b>Type of Service<b></label>
										
										<select class="form-control" id="" name="serviceTypeNonAuth">
										<option value="select">--SELECT--</option>
										"""),_display_(/*740.12*/for(serlist <- servicetypeList) yield /*740.43*/{_display_(Seq[Any](format.raw/*740.44*/("""
											
											"""),format.raw/*742.12*/("""<option value=""""),_display_(/*742.28*/serlist/*742.35*/.serviceTypeName),format.raw/*742.51*/("""">"""),_display_(/*742.54*/serlist/*742.61*/.serviceTypeName),format.raw/*742.77*/("""</option>
										""")))}),format.raw/*743.12*/("""
										 
									
									"""),format.raw/*746.10*/("""</select>
									  </div>
									  <div class="col-md-2">
							  <div class="form-group">
								<label for="memarksAuthoDelar">Remarks</label>
								<textarea type="text" name="remarksList[5]" class="form-control" id="memarksAuthoDelar"></textarea>
							  </div>
							</div>
								  </div>
								</div>
							  </div>
							</div>
							
						  </div>
						  
						  <div class="pull-right" >
					
					  <button type="button" class="btn btn-primary" value="" id="nextToAlreadySrviceUpsell"/>Next</button>
					</div>
				  </div>
		<div class="row animated  bounceInRight" id="AlreadyServiceUpsellOpp" style="display:none;">
						  <div class="col-md-12">
				   
					<label>Capture Lead : Is there an Upsell Opportunity.</label>
				   <br>
				   
					<div class="radio-inline">
					  <label>
						<input type="radio" name="LeadYesAlradyService" onclick="" value="Capture Lead Yes AlreadyService" id="" >
						Yes</label>
					</div>
					<div class="radio-inline">
					  <label>
						<input type="radio" name="LeadYesAlradyService" value="Capture Lead No AlreadyService" id="" >
						No</label>
					</div>
					<div class="row animated  bounceInRight" style="display:none;" id="LeadDivAlreadyService">
					  <div class="col-md-6">
					 
						<div class="checkbox">
						  <label>
							<input type="checkbox" class="" name="upsellLead[7].upsellId" value="1" id="InsuranceIDCheckAlreadyServiced" onClick="loadLeadBasedOnUserLocation('InsuranceIDCheckAlreadyServiced','insuranceLead')">
							Insurance</label>
						</div>
						<div class="row animated  bounceInRight" style="display:none;" id="InsuranceSelectAlreadyService">
						  <div class="col-md-12">
							<div class="col-md-6">
							  <label for="">Tag to</label>
							  <select class="form-control" name="upsellLead[7].taggedTo" id="insuranceLead">
								<option >Select</option>
							  </select>
							   </div>
							   <div class="col-md-6">
							  <label for="comments">Remarks</label>
							  <textarea class="form-control" rows="1" id="comments" name="upsellLead[7].upsellComments"></textarea>
							</div>
						   </div>
						</div>
						
						 <div class="checkbox">
						  <label>
							<input type="checkbox" class="" name="upsellLead[8].upsellId" value="2" id="MaxicareIDCheckAlreadyServiced" onClick="loadLeadBasedOnUserLocation('MaxicareIDCheckAlreadyServiced','MaxicareLeadAlreadyServed')">
							Warranty / EW</label>
						</div>
						<div class="row animated  bounceInRight" style="display:none;" id="MaxicareSelectAlreadyService">
						  <div class="col-md-12">
							<div class="col-md-6">
							  <label for="">Tag to</label>
							  <select class="form-control" name="upsellLead[8].taggedTo" id="MaxicareLeadAlreadyServed">
								<option >Select</option>
							  </select>
							   </div>
							   <div class="col-md-6">
							  <label for="comments">Remarks</label>
							  <textarea class="form-control" rows="1" id="comments" name="upsellLead[8].upsellComments"></textarea>
							</div>
						   </div>
						</div>
						<div class="checkbox">
						  <label>
							<input type="checkbox" class="" name="upsellLead[9].upsellId" value="4" id="ShieldIDAlreadyService" onClick="loadLeadBasedOnUserLocation('ShieldIDAlreadyService','ShieldLead')">
							Re-Finance / New Car Finance</label>
						</div>
						<div class="row animated  bounceInRight" style="display:none;" id="ShieldSelectAlreadyService">
							<div class="col-md-12">
							<div class="col-md-6">
							  <label for="">Tag to</label>
							  <select class="form-control" name="upsellLead[9].taggedTo" id="ShieldLead">
								<option >Select</option>
							  </select>
							   </div>
							   <div class="col-md-6">
							  <label for="comments">Remarks</label>
							  <textarea class="form-control" rows="1" id="comments" name="upsellLead[9].upsellComments"></textarea>
							</div>
						   </div>
						<!-- <div class="checkbox">
						  <label>
							<input type="checkbox" class="" name="upsellLead[8].upSellType" value="Warranty / EW" id="WARRANTYIDAlreadyService" >
							Warranty / EW</label>
						</div>
						<div class="row animated  bounceInRight" style="display:none;" id="WARRANTYSelectAlreadyService">
							<div class="col-md-12">
							<div class="col-md-3">
							  <label for="">Tag to</label>
							  <select class="form-control" name="upsellLead[8].taggedTo" id="warrantyLead">
								<option >Select</option>
							  </select>
							   </div>
							   <div class="col-md-3">
							  <label for="comments">Remarks</label>
							  <textarea class="form-control" rows="1" id="comments" name="upsellLead[8].upsellComments"></textarea>
							</div>
						   </div> -->
						   
						</div>
						<div class="checkbox">
						  <label>
							<input type="checkbox" class="VAS" name="upsellLead[10].upsellId" value="3" id="VASIDAlreadyService" onClick="loadLeadBasedOnUserLocation('VASIDAlreadyService','vASLead')"/>
							VAS</label>
						</div>
						<div class="row animated  bounceInRight" style="display:none;" id="VASTagToSelectAlreadyService">
						   <div class="col-md-12">
							<div class="col-md-6">
							  <label for="">Tag to</label>
							  <select class="form-control" name="upsellLead[10].taggedTo" id="vASLead">
								<option >Select</option>
							  </select>
							   </div>
							   <div class="col-md-6">
							  <label for="comments">Remarks</label>
							  <textarea class="form-control" rows="1" id="comments" name="upsellLead[10].upsellComments"></textarea>
							</div>
						   </div>
						  
						</div>
						<!-- <div class="checkbox">
						  <label>
							<input type="checkbox" class="LeadClass" name="upsellLead[10].upSellType" value="Re-Finance / New Car Finance" id="ReFinanceIDCheckAlreadyService" />
							Re-Finance / New Car Finance</label>
						</div>
						<div class="row animated  bounceInRight" style="display:none;" id="ReFinanceSelectAlreadyService">
						  <div class="col-md-12">
							<div class="col-md-3">
							  <label for="">Tag to</label>
							  <select class="form-control" name="upsellLead[10].taggedTo" id="reFinanceLead">
								<option >Select</option>	
							  </select>
							   </div>
							   <div class="col-md-3">
							  <label for="comments">Remarks</label>
							  <textarea class="form-control" rows="1" id="comments" name="upsellLead[10].upsellComments"></textarea>
							</div>
						   </div>
						 
						</div> -->
						</div>
						 <div class="col-md-6">
						<!-- <div class="checkbox">
						  <label>
							<input type="checkbox" class="Loan" name="upsellLead[11].upSellType" value="Sell Old Car" id="LoanIDAlreadyService" />
							Sell Old Car</label>
						</div>
						<div class="row animated  bounceInRight" style="display:none;" id="LoanSelectAlreadyService">
						   <div class="col-md-12">
							<div class="col-md-3">
							  <label for="">Tag to</label>
							  <select class="form-control" name="upsellLead[11].taggedTo" id="sellOldCarLead">
								<option >Select</option>
							  </select>
							   </div>
							   <div class="col-md-3">
							  <label for="comments">Remarks</label>
							  <textarea class="form-control" rows="1" id="comments" name="upsellLead[11].upsellComments"></textarea>
							</div>
						   </div>
					
						</div> -->
						<div class="checkbox">
						  <label>
							<input type="checkbox" class="EXCHANGE" name="upsellLead[11].upsellId" value="6" id="EXCHANGEIDAlreadyService" onClick="loadLeadBasedOnUserLocation('EXCHANGEIDAlreadyService','buyNewCarLead')"/>
							Buy New Car / Exchange</label>
						</div>
						<div class="row animated  bounceInRight" style="display:none;" id="EXCHANGEIDSelectAlreadyService">
						   <div class="col-md-12">
							<div class="col-md-6">
							  <label for="">Tag to</label>
							  <select class="form-control" name="upsellLead[11].taggedTo" id="buyNewCarLead">
								<option >Select</option>
							  </select>
							   </div>
							   <div class="col-md-6">
							  <label for="comments">Remarks</label>
							  <textarea class="form-control" rows="1" id="comments" name="upsellLead[11].upsellComments"></textarea>
							</div>
						   </div>
						
						</div>
						  <div class="checkbox">
						  <label>
							<input type="checkbox" class="UsedCar" name="upsellLead[12].upsellId" value="9" id="UsedCarIDAlreadyService" onClick="loadLeadBasedOnUserLocation('UsedCarIDAlreadyService','usedCarLead')"/>
							Used Car</label>
						</div>
						<div class="row animated  bounceInRight" style="display:none;" id="UsedCarSelectAlreadyService">
						   <div class="col-md-12">
							<div class="col-md-6">
							  <label for="">Tag to</label>
							  <select class="form-control" name="upsellLead[12].taggedTo" id="usedCarLead">
								<option >Select</option>
							  </select>
							   </div>
							   <div class="col-md-6">
							  <label for="comments">Remarks</label>
							  <textarea class="form-control" rows="1" id="comments" name="upsellLead[12].upsellComments"></textarea>
							</div>
						   </div>
						  
						</div>
						<div class="checkbox">
						  <label>
							<input type="checkbox" class="UsedCar" name="upsellLead[13].upsellId" value="5" id="RoadSideIDAlreadyService" onClick="loadLeadBasedOnUserLocation('RoadSideIDAlreadyService','roadCarLead')"/>
							Sell Old Car</label>
						</div>
						<div class="row animated  bounceInRight" style="display:none;" id="RoadSideSelectAlreadyService">
						   <div class="col-md-12">
							<div class="col-md-6">
							  <label for="">Tag to</label>
							  <select class="form-control" name="upsellLead[13].taggedTo" id="roadCarLead">
							  </select>
							   </div>
							   <div class="col-md-6">
							  <label for="comments">Remarks</label>
							  <textarea class="form-control" rows="1" id="comments" name="upsellLead[13].upsellComments"></textarea>
							</div>
						   </div>
						  
						</div>
				   
					  </div>
					</div>
					  <div class="pull-right" >
					<button type="button" class="btn btn-primary" id="BackToAlreadySerUpsel">Back</button>
					  <button type="button" class="btn btn-primary" value="" id="NextAlreadySerFeedBack"/>Next</button>
					</div>
					
						  </div>
				
							</div>
							<div class="row animated  bounceInRight" id="AlreadyServiceFeedBAck" style="display:none;">
						  <div class="col-md-12">
						 
							<label>Does Customer has a feedback / compliant ?</label>
							<br>
							
							  <label for="feedbackYes" class="radio-inline">
								<input type="radio" name="userfeedbackAlreadyService" onclick="loadLeadBasedOnLocationDepartment();" value="feedback Yes AlreadyService" id="userfeedbackAlreadyService" >
								Yes</label>
							
							
							  <label for="feedbackNo" class="radio-inline">
								<input type="radio" name="userfeedbackAlreadyService" value="feedback No AlreadyService" id="" >
								No</label>
						
						
					   
						<div class="row animated  bounceInRight" style="display:none;" id="feedbackDIVAlreadyService"><br>
						 <div class="col-md-12">
						  <div class="col-md-3">
							<label for="">Select Department<i class="lblStar">*</i></label>
						<select class="form-control selected_department" id="selected_department" name="departmentForFB1" onchange="ajaxLeadTagByDepartment();">
						 <option>select</option>
						 
							</select>
						  </div>
						  <div class="col-md-3">
							<label for="">Tag to</label>
							<select class="form-control" id="LeadTagsByLocation" name="complaintOrFB_TagTo1">
							  
							</select>
						  </div>
						
						  <div class="col-md-3">
							<label for="comments">Feedback/Comment</label>
							<textarea class="form-control" rows="1" id="comments" name="remarksOfFB1"></textarea>
						  </div>
						  <br>
						</div>
						  </div>
						  <div class="pull-right" >
					<button type="button" class="btn btn-primary" id="BackToAlreadyServUpsell">Back</button>
					  <button type="button" class="btn btn-primary" value="" id="NextToLastAlreadySerQuestion"/>Next</button>
					</div>
				  </div>
				    </div>
					
					<div class="row animated  bounceInRight" id="LastQuestionAlreadyServ" style="display:none;">
						  <div class="col-md-12">
						  <div class="col-md-12">
							<label>May I please take a moment to update your contact information? This will help us serve you better in future.</label>
							<br>
							<div class="radio-inline">
							  <label for="">
								<input type="radio" name="radio7" value="" id=""  data-toggle="modal" data-target="#addBtn" >
								Yes</label>
							</div>
							<div class="radio-inline">
							  <label for="feedbackNo">
								<input type="radio" name="radio7" value="" class="CustomerNo" checked>
								No</label> 
							</div>
						  </div>
						  </div>
						  <div class="pull-right" >
					<button type="button" class="btn btn-primary " id="BackToAlreadyServiceFeedBack">Back</button>
					 <button type="submit" class="btn btn-primary" value="alreadyServiced" name="typeOfsubmit"id="alreadyServiced"/>Submit</button>
					</div>
						  </div>
						
						
						<div class="checkbox VehicleSold"> 
						  <label>
							<input type="checkbox" class="NoService" name="noServiceReason" value="Vehicle Sold" id="VehicleSold" >
							Vehicle Sold</label>
						</div>
						
				
						<div class="row animated  bounceInRight" style="display:none;" id="VehicelSoldYesRNo">
						<div class="col-md-12">
                        <label>	Do you have the contact details of the new owner ?</label>
                        <br>
                        <div class="radio-inline">
                          <label for="">
                            <input type="radio" name="VehicleSoldYes" value="VehicleSold Yes" id="VehicleSoldYesbtn" >
                            Yes</label>
                        </div>
                        <div class="radio-inline">
                          <label for="">
                            <input type="radio" name="VehicleSoldYes" value="VehicleSoldYes No" id="VehicleSoldNobtn" >
                            No</label> 
                        </div>
                      </div>
                     <div class="pull-right backToAllSNR">
				    <button type="button" class="btn btn-primary" value="" id="backToAlreadyServicediv">Back</button>
					<button type="button" class="btn btn-primary" value="" id="NextToPurchaseNewcarNO">Next</button>
                </div>
						
                      
						</div>
						
					<div class="row animated  bounceInRight" style="display:none;"  id="VehicleSoldClickYes">
							 
							
				<div class="row">
			 <div class="col-md-12">
 <label>Please confirm the name of the new owner. </label><br>			 
               <div class="form-group">
      <div class="col-xs-1">
       
       <select class="form-control">
<option value="Mr.">Mr.</option>
<option value="Ms.">Ms.</option>
<option value="Mrs.">Mrs.</option>

</select>
      </div>
      <div class="col-xs-2" style="margin-left: -28px;">
       
        <input class="form-control onlyAlphabetOnly" id="customerFNameConfirm" name="customerFName" placeholder="First Name" type="text">
      </div>
      <div class="col-xs-2">
       
        <input class="form-control onlyAlphabetOnly" name="customerLName" placeholder="Last Name" id="customerLNameConfirm" type="text">
      </div>
	  </div>
	  </div>
	  </div>
	  </br>
	  <div class="row">
			 <div class="col-md-12">
       <div class="col-xs-2">
              <input class="form-control numberOnly" maxlength="10" name="phoneList[0]" placeholder="Mobile1" id="Mobile1" type="text">
      </div>
      <div class="col-xs-2">
     
        <input class="form-control numberOnly" maxlength="10" name="phoneList[1]" placeholder="Mobile2" id="Mobile2" type="text">
      </div>
	  
      <div class="col-xs-1">
     
        <input class="form-control numberOnly" maxlength="4" name="phoneList[2]" placeholder="STD" id="STDCodeInput" type="text">
      </div>
      <div class="col-xs-2">
        
        <input class="form-control numberOnly" maxlength="8" name="phoneList[3]" placeholder="Landline No." id="LandlineInput" type="text">
      </div>
    </div>
        </div>
			</br>
            					
				
           <div class="row">
             <div class="col-md-12">
							
							  <div class="form-group">
							 
							 <div class="col-xs-2">
        <textarea rows="2" class="form-control" name="addressLine1" placeholder="Address Line1"></textarea>
        
		
      </div>
      <div class="col-xs-2">
       
         <textarea class="form-control" placeholder="Address Line2" name="addressLine2" id="" rows="2" ></textarea>
      </div>
	        </div>
			</div>
			</div>
			</br>
			   <div class="row">
             <div class="col-md-12">
             
             <div class="col-xs-2">
				<select class="form-control" name="state" placeholder="State" id="stateInput" onchange="getCityByStateSelection('stateInput','cityInput');">	
					<option value="0">--STATE--</option>
			"""),_display_(/*1170.5*/for(states <- statesList) yield /*1170.30*/{_display_(Seq[Any](format.raw/*1170.31*/("""
           
          """),format.raw/*1172.11*/("""<option value=""""),_display_(/*1172.27*/states),format.raw/*1172.33*/("""">"""),_display_(/*1172.36*/states),format.raw/*1172.42*/("""</option>
                              
	  """)))}),format.raw/*1174.5*/("""        """),format.raw/*1174.13*/("""</select>
        
      </div>
       <div class="col-xs-2">

       
		 <select class="form-control" name="city" placeholder="City" id="cityInput">	
                                        
                                    </select>
      </div>
      
       <div class="col-xs-2">
     
        <input class="form-control numberOnly" maxlength="6" value="0" name="pincode" placeholder="Pin" id="PinInput" type="text">
		<input type="hidden" name="country" value="India">
		
      </div>
      </div>
							 </div>
            
								 <div class="pull-right backToYesSNR ">
				    <button type="button" class="btn btn-primary" value="" id="backToSNR">Back</button>
					<button type="button" class="btn btn-primary" value="" id="NextToPurchaseCar">Next</button>
                </div>
						   
						  </div>
						</div>
								<div class="row animated  bounceInRight" style="display:none;"  id="VehicleSoldClickNo">
						  <div class="col-md-12">
						  <label for=""><b>	Did you purchase another car?<b></label><br>
						     <div class="radio-inline">
                          <label for="">
                            <input type="radio" name="PurchaseYes" value="Purchase Yes" id="" >
                            Yes</label>
                        </div>
                        <div class="radio-inline">
                          <label for="">
                            <input type="radio" name="PurchaseYes" value="Purchase No" id="" >
                            No</label> 
                        </div>
						   
						
						
						
							<div class="row animated  bounceInRight" style="display:none;"  id="PurchaseClickYes">
						  <div class="col-md-12">
						 
						   <div class="col-xs-2">
					        <label for=""> New Car Registration No</label>
					        <input class="form-control" id="vehicleRegNoId" name="vehicleRegNo" type="text">
					      </div>
					         <div class="col-xs-2">
					        <label for=""> Brand</label>
					        <input class="form-control" id="" name="variant" type="text">
					      </div>
					         <div class="col-xs-2">
					        <label for=""> Model</label>
					        <input class="form-control" id="" name="model" type="text">
					      </div>
					      <div class="col-xs-2">
					        <label for=""> Dealership Name</label>
					        <input class="form-control" id="" name="dealershipName" type="text">
					      </div>
					      <div class="col-xs-3">
					        <label for=""> Purchase Date</label>
					        <input class="form-control datepickerPrevious" name="purchaseDate" id="" type="text" readonly>
					      </div>
						  </div>
					
						   
						  </div>
							  </div>  
						  <div class="pull-right">
				 <button type="button" class="btn btn-primary" value="" id="BackToPleaseComfNewOwnre">Back</button>
					<button type="button" class="btn btn-primary" value="" id="nextToFinalEditInfoVS">next</button>
                </div>
						</div>
						
 	<div class="row animated  bounceInRight" id="VehicelSoldQuestion" style="display:none;">
				      <div class="col-md-12">
                      <div class="col-md-12">
                        <label>May I please take a moment to update your contact information? This will help us serve you better in future.</label>
                        <br>
                        <div class="radio-inline">
                          <label for="feedbackYes">
                            <input type="radio" name="radio3" value="" data-toggle="modal" data-target="#addBtn" id="" >
                            Yes</label>
                        </div>
                        <div class="radio-inline">
                          <label for="feedbackNo">
                            <input type="radio" name="radio3" value="" class="CustomerNo" checked>
                            No</label> 
                        </div>
                      </div>
                      </div>
                      <div class="pull-right" >
				<button type="button" class="btn btn-primary" id="BackToVSHVNewCar">Back</button>
                 <button type="submit" class="btn btn-primary" value="vehicleSold" name="typeOfsubmit" id="vehicleSold" />Submit</button>
                </div>
                      </div>
                      
					  
					
						
						<div class="checkbox Dissatisfiedwithpreviousservice">
						  <label>
							<input type="checkbox" class="NoService" name="noServiceReason" onclick="advisorBasedOnWorkshop();" value="Dissatisfied with previous service" id="Dissatisfiedwithpreviousservice" >
							Dissatisfied with previous service</label>
						</div>

						<div class="row animated  bounceInRight" style="display:none;"  id="txtDissatisfiedwithpreviousservice">
						  <div class="col-md-12">
							
							  <div class="col-md-3">
								<div class="form-group">
								  <label for="lastServiceDateOfDWPS"><b>Last Service Date<b></label>
								 <input type="text" name="lastServiceDateOfDWPS" value=""""),_display_(/*1290.66*/{latestService.getLastServiceDate()}),format.raw/*1290.102*/("""" class="form-control datepickerPrevious" />
								</div>
							  </div>
							   <div class="col-md-3">
								<div class="form-group">
								  <label for=""><b>Service Advisor ID<b></label>
								  <input type="text" name="serviceAdvisorID" value=""""),_display_(/*1296.62*/{if(latestService.getServiceAdvisor()!=null){latestService.getServiceAdvisor().getAdvisorName()}}),format.raw/*1296.159*/("""" class="form-control" readonly/> 
								</div>
							  </div>
							  <div class="col-md-3">
								<div class="form-group">
								  <label for=""><b>Assigned To<b></label>
								  
								  
								  <select class="form-control"  id="assignedToSA" name="assignedToSA">
								  <option value="0">--Select--</option>
								  
						
								  </select>
								</div>
							  </div>
							  <div class="col-md-3">
							  <div class="form-group">
								<label for="memarksDissSatiPreSer">Remarks</label>
								<textarea type="text" name="remarksList[4]" class="form-control" id="memarksDissSatiPreSer"></textarea>
							  </div>
							</div>
							
						   
						  </div>
						  <div class="pull-right" >
				
                  <button type="button" class="btn btn-primary" value="" id="NextDisatisfiedPrPopup"/>Next</button>
                </div>
						</div>
							<div class="row animated  bounceInRight" id="DisatisfiedPreQuestion" style="display:none;">
				      <div class="col-md-12">
                      <div class="col-md-12">
                        <label>May I please take a moment to update your contact information? This will help us serve you better in future.</label>
                        <br>
                        <div class="radio-inline">
                          <label for="feedbackYes">
                            <input type="radio" name="radio1" value="" data-toggle="modal" data-target="#addBtn" id="" >
                            Yes</label>
                        </div>
                        <div class="radio-inline">
                          <label for="feedbackNo">
                            <input type="radio" name="radio1" value="" class="CustomerNo" checked>
                            No</label> 
                        </div>
                      </div>
                      </div>
                      <div class="pull-right" >
				<button type="button" class="btn btn-primary " id="BackToDissatisfiedPreviosInc">Back</button>
                  <button type="submit" class="btn btn-primary" value="dissatifiedwithPreviousService" name="typeOfsubmit" id="dissatifiedwithPreviousService"/>Submit</button>
                </div>
                      </div>
                      
					  
						<div class="checkbox Distancefrom">
						  <label>
							<input type="checkbox" class="NoService" name="noServiceReason" value="Distance from Dealer Location" id="Distancefrom" >
							Distance from Dealer Location</label>
						</div>
						<div style="display:none;" class="animated  bounceInRight" id="DistancefromDealerLocationDIV">
						 <div class="col-md-12">
						  <div class="radio">
							<label>
							  <input type="radio" name="DistancefromDealerLocationRadio" value="Transfer to other city" id="TransfertoothercityID" >
							  Transfer to other city</label>
						  </div>
						  </div>
						  <div class="animated  bounceInRight" style="display:none;" id="txtTransfertoothercity">
						  <div class="col-md-12">
							<div class="col-md-3">
							<label for="transferedCity"><b>Enter name of the city<b></label>							
								<!-- 	<select class="form-control" name="transferedCity">
									<option value="select">select</option>
									<option value="Ahmedabad">Ahmedabad</option>
									<option value="Dhari">Dhari</option>
									<option value="Gondal">Gondal</option>
									<option value="Hapa">Hapa</option>
									<option value="Jamnagar City">Jamnagar City</option>
									<option value="Jasdan">Jasdan</option>
									<option value="Kuvadwa Road">Kuvadwa Road</option>
									<option value="Metoda">Metoda</option>
									<option value="Moti Khavdi">Moti Khavdi</option>
									<option value="Padana">Padana</option>
									<option value="Porbander">Porbander</option>									
									<option value="Rajula">Rajula</option>
									<option value="Tagore Road">Tagore Road</option>
									<option value="University Road">University Road</option>									
									<option value="Wankaner">Wankaner</option>
									<option value="Others">Others</option>									
									</select>  -->
									<input type="text" class="form-control" name="transferedCity">
						   
						  </div>
						  </div>
						  </div>
						   <div class="col-md-12">
						  <div class="radio">
							<label>
							  <input type="radio" name="DistancefromDealerLocationRadio" value="Too far" id="ToofarID" onclick="ajaxgetDealerOEM();">
							  Too far(Same city)</label>
						  </div>
						  </div>
						  <div class="animated  bounceInRight" style="display:none;"  id="txtToofar">
							<div class="col-md-12">
							 
								<div class="col-md-3">
								  <label for=""><b>Enter PIN of the city<b></label>
								  <input type="text" name="pinCodeOfCity" class="form-control numberOnly" maxlength="6" />
								</div>
							  
							  
								<div class="col-md-9">
								 <label for=""><b>Would you like to opt for <input type="text" id="oemvalue" style="border:none" readonly/><b></label>
								  <br>
								  <div class="radio-inline">
									<label>
									  <input type="radio" name="optforMMS" value="yes" id="EnterPINthecityID" >
									  Yes</label>
								  </div>
								  <div class="radio-inline">
									<label>
									  <input type="radio" name="optforMMS" value="No" id="EnterPINofthecityID" >
									  NO</label>
								  </div>
								</div>
							
							</div>
						  </div>
				 <div class="pull-right" >
				    <button type="button" class="btn btn-primary" value="" id="NextDistanceForPopup"/>Next</button>
                </div>
						</div>
						<div class="row animated  bounceInRight" id="DistanceFoRRQuestion" style="display:none;">
				      <div class="col-md-12">
                      <div class="col-md-12">
                        <label>May I please take a moment to update your contact information? This will help us serve you better in future.</label>
                        <br>
                        <div class="radio-inline">
                          <label for="feedbackYes">
                            <input type="radio" name="radio4" value="" data-toggle="modal" data-target="#addBtn" id="" >
                            Yes</label>
                        </div>
                        <div class="radio-inline">
                          <label for="feedbackNo">
                            <input type="radio" name="radio4" value="" class="CustomerNo" checked>
                            No</label> 
                        </div>
                      </div>
                      </div>
                      <div class="pull-right" >
				
				<button type="button" class="btn btn-primary " id="BackToDissatisfiedDelaerLoc">Back</button>
                  <button type="submit" class="btn btn-primary" value="vehicleSold" name="typeOfsubmit" id="dissatifiedwithPreviousServices"/>Submit</button>
                </div>
                      </div>
                      
						
						
						<div class="checkbox DissatisfiedwithSalesID">
						  <label>
							<input type="checkbox" class="NoService" name="noServiceReason" onclick="ajaxLeadOnDissatisfiedWithSales(7);" value="Dissatisfied with Sales" id="DissatisfiedwithSalesID" >
							Dissatisfaction with Sales</label>
 						</div>
						<div class="row animated  bounceInRight" style="display:none;" id="DissatisfactionwithSalesREmarksDiv">
						  <div class="col-md-12">
							
							  <div class="col-md-3">
								<label for="">Tag to</label>
								<select class="form-control" id="noServiceReasonTaggedTo" name="noServiceReasonTaggedTo">
								  <option value="0">--select--</option>
								  
								</select>
							  </div>
							
							  <div class="col-md-3">
								<label for="">Remarks</label>
								<textarea type="text" name="noServiceReasonTaggedToComments" class="form-control"></textarea>
							  </div>
							
						  </div>
						   <div class="pull-right" >
				    <button type="button" class="btn btn-primary" value="" id="NextDisSatisSalePopup"/>Next</button>
                </div>
						</div>
						<div class="row animated  bounceInRight" id="DisstisFiedSaleRQuestion" style="display:none;">
				      <div class="col-md-12">
                      <div class="col-md-12">
                        <label>May I please take a moment to update your contact information? This will help us serve you better in future.</label>
                        <br>
                        <div class="radio-inline">
                          <label for="feedbackYes">
                            <input type="radio" name="radio5" value="" data-toggle="modal" data-target="#addBtn" id="" >
                            Yes</label>
                        </div>
                        <div class="radio-inline">
                          <label for="feedbackNo">
                            <input type="radio" name="radio5" value="" class="CustomerNo" checked>
                            No</label> 
                        </div>
                      </div>
                      </div>
                      <div class="pull-right" >
				<button type="button" class="btn btn-primary" id="backToDissSatiSalse"/>Back</button>
                  <button type="submit" class="btn btn-primary" value="dissatifiedwithPreviousService" name="typeOfsubmit" id="dissatifiedwithPService"/>Submit</button>
                </div>
                      </div>
					  
						<div class="checkbox DissatisfiedwithInsuranceId">
						  <label>
							<input type="checkbox"  class="NoService" name="noServiceReason" onclick="ajaxLeadOnDissatisfiedWithSales(1);" value="Dissatisfied with Insurance" id="DissatisfiedwithInsuranceId" >
							Dissatisfaction with Insurance</label>
						</div>
						<div class="row animated  bounceInRight" style="display:none;" id="DissatisfactionwithInsuranceREmarksDiv">
						  <div class="col-md-12">
							
							  <div class="col-md-3">
								<label for="">Tag the case to</label>
								<select class="form-control" id="noServiceReasonTaggedTo1" name="noServiceReasonTaggedTo1">
								  <option value="0">--select--</option>
								</select>
							  </div>
							  <div class="col-md-3">
								<label for="">Remarks</label>
								<textarea type="text"  name="noServiceReasonTaggedToComments1" class="form-control"></textarea>
							  </div>
							
						   
						  </div>
						  <div class="pull-right" >
				    <button type="button" class="btn btn-primary" value="" id="NextDisSatInsurancPopup"/>Next</button>
                </div>
						</div>
								<div class="row animated  bounceInRight" id="DisstisInsurancQuestion" style="display:none;">
				      <div class="col-md-12">
                      <div class="col-md-12">
                        <label>May I please take a moment to update your contact information? This will help us serve you better in future.</label>
                        <br>
                        <div class="radio-inline">
                          <label for="feedbackYes">
                            <input type="radio" name="radio6" value="" data-toggle="modal" data-target="#addBtn" id="" >
                            Yes</label>
                        </div>
                        <div class="radio-inline">
                          <label for="feedbackNo">
                            <input type="radio" name="radio6" value="" class="CustomerNo" checked>
                            No</label> 
                        </div>
                      </div>
                      </div>
                      <div class="pull-right" >
				<button type="button" class="btn btn-primary" value="" id="bsckTodisInsu"/>Back</button>
                  <button type="submit" class="btn btn-primary" value="dissatifiedwithPreviousService" name="typeOfsubmit" id="dissatifiedwithService"/>Submit</button>
                </div>
                      </div>
					  <!-----------19th june 2017 Shashi-------------->
					 
                        
                        <div class="checkbox KmsNotCov">
						  <label>
							<input type="checkbox"  class="NoService" name="noServiceReason" value="Kms Not Covered" id="KmsNotCoveredId" >
							Kms not Covered</label>
							
						</div>
					   <div class="row animated  bounceInRight" style="display:none" id="kmsCoverdTextDiv">
						<div  class="col-md-3" >
						<div class="form-group">
						<label>Current Mileage</label>
							<input type="text" class="form-control numberOnly" id="CurrentMileage" name="currentMileage" max="6" value="0" maxlength="6" >
						</div>
						</div>
						<div  class="col-md-3">
						<div class="form-group">
						<label>Expected Visit Date</label>
							<input type="text" class="form-control datepickerAfter" id="expectedVisitDT" name="expectedVisitDate" readonly>
						</div>
						</div>
						<div class="pull-right" >
				             <button type="submit" class="btn btn-primary" value="vehicleSold" name="typeOfsubmit" id="kmsSubmitID"/>Submit</button>
                		</div>
						</div>
												
                    
					  
					  
					  <div class="checkbox ExcBillingId">
						  <label>
							<input type="checkbox"  class="NoService" name="noServiceReason" value="Excess Billing" id="ExcBillingId">
							Excess Billing</label>
							
						</div>
						 <div class="checkbox Stolen">
						  <label>
							<input type="checkbox"  class="NoService" name="noServiceReason" value="Stolen" id="Stolen" >
							Stolen</label>
							
						</div>
						
						 <div class="checkbox Totalloss">
						  <label>
							<input type="checkbox"  class="NoService" name="noServiceReason" value="Total loss" id="Totalloss" >
							Total loss/Damaged</label>
							
						</div>
					
                <div class="col-md-12 animated  bounceInRight" style="display:none" id="SubmitBtnStolen">
						<div class="pull-right">
				    <button type="submit" class="btn btn-primary" value="vehicleSold" name="typeOfsubmit" id=""/>Submit</button>
                </div>
                </div>
						
						
						<div class="checkbox OtherLast">
						  <label>
							<input type="checkbox"  class="NoService" name="noServiceReason" value="Other Service" id="Other" >
							Other</label>
						</div>
						<div class="animated  bounceInRight" style="display:none;" id="OtherSeriveRemarks">
						  
						   <div class="col-md-3">
							  <label for="comments">Enter other reason for no service</label>
							  <textarea class="form-control" rows="1" id="commentsOtherRemarks" name="reason" ></textarea>
						   </div>
						 
						 	  <div class="pull-right" >
				    <button type="button" class="btn btn-primary" value="" id="NextOthersPopup"/>Next</button>
                </div>
						</div>
					<div class="row animated  bounceInRight" id="OthersLastQuestion" style="display:none;">
				      <div class="col-md-12">
                      <div class="col-md-12">
                        <label>May I please take a moment to update your contact information? This will help us serve you better in future.</label>
                        <br>
                        <div class="radio-inline">
                          <label for="feedbackYes">
                            <input type="radio" name="radio8" value="" data-toggle="modal" data-target="#addBtn" id="" >
                            Yes</label>
                        </div>
                        <div class="radio-inline">
                          <label for="feedbackNo">
                            <input type="radio" name="radio8" value="" class="CustomerNo" checked>
                            No</label> 
                        </div>
                      </div>
                      </div>
                      <div class="pull-right" >
				<button type="button" class="btn btn-primary" id="BacktoOtherInsu">Back</button>
                  <button type="submit" class="btn btn-primary" value="vehicleSold" name="typeOfsubmit" id="vehicleSoldStolen"/>Submit</button>
                </div>
                      </div>
					
					</div>
				</div>
						</div>
<!--Out Bound Upsel Opportunity---------->
			
		
	 
			
		 <!--  <div class="col-md-4" id="NoComments" style="display:none;">
			<div class="row">
			  <label for="comments"><b>Remarks<b></label>
			  <textarea class="form-control" rows="2" id="comments" name="comments"></textarea>
			</div>
		  </div> -->
		


<!--  Inbound Call-->

				  
				  """))
      }
    }
  }

  def render(statesList:List[String],workshopList:List[Workshop],customerData:Customer,vehicleData:Vehicle,locationList:List[Location],interOfCall:CallInteraction,servicetypeList:List[ServiceTypes],userData:WyzUser,latestService:Service): play.twirl.api.HtmlFormat.Appendable = apply(statesList,workshopList,customerData,vehicleData,locationList,interOfCall,servicetypeList,userData,latestService)

  def f:((List[String],List[Workshop],Customer,Vehicle,List[Location],CallInteraction,List[ServiceTypes],WyzUser,Service) => play.twirl.api.HtmlFormat.Appendable) = (statesList,workshopList,customerData,vehicleData,locationList,interOfCall,servicetypeList,userData,latestService) => apply(statesList,workshopList,customerData,vehicleData,locationList,interOfCall,servicetypeList,userData,latestService)

  def ref: this.type = this

}


}

/**/
object serviceOutBound extends serviceOutBound_Scope0.serviceOutBound
              /*
                  -- GENERATED --
                  DATE: Thu Dec 28 12:24:30 IST 2017
                  SOURCE: D:/CRMNEW/28th Dec/crm/app/views/serviceOutBound.scala.html
                  HASH: c86ef21189f0bbc79029e13bbb4b34a007afb043
                  MATRIX: 869->3|1190->228|1222->234|1385->371|1522->487|1822->761|1877->800|1916->801|2008->865|2051->881|2064->885|2096->896|2126->899|2139->903|2173->916|2257->969|2326->1011|2674->1332|2714->1356|2753->1357|2794->1372|3158->1715|3233->1759|3404->1903|3735->2208|3782->2239|3821->2240|3862->2255|4261->2633|4336->2677|4373->2687|4708->2996|4753->3025|4792->3026|4833->3041|5230->3417|5368->3524|5416->3545|5954->4054|6086->4163|6637->4685|6769->4794|7440->5438|7724->5701|7759->5708|9294->7216|9358->7263|9398->7264|9451->7289|9784->7600|9828->7612|9877->7633|10789->8518|10852->8564|10892->8565|10949->8593|10993->8609|11013->8619|11053->8637|11083->8639|11103->8649|11144->8667|11175->8669|11196->8679|11237->8697|11268->8699|11289->8709|11322->8719|11353->8721|11374->8731|11410->8744|11442->8747|11463->8757|11504->8775|11535->8777|11556->8787|11597->8805|11628->8807|11649->8817|11690->8835|11721->8837|11742->8847|11775->8857|11806->8859|11827->8869|11863->8882|11942->8929|11976->8935|22929->19861|23180->20091|23218->20101|26672->23528|26723->23569|26763->23570|26807->23587|26874->23644|26914->23645|26949->23652|27154->23823|27186->23827|28679->25292|28737->25328|31349->27912|31397->27943|31437->27944|31492->27970|31536->27986|31553->27993|31591->28009|31622->28012|31639->28019|31677->28035|31731->28057|31795->28092|33868->30137|33916->30168|33956->30169|34011->30195|34055->30211|34072->30218|34110->30234|34141->30237|34158->30244|34196->30260|34250->30282|34314->30317|51928->47903|51971->47928|52012->47929|52067->47954|52112->47970|52141->47976|52173->47979|52202->47985|52281->48032|52319->48040|57482->53174|57542->53210|57837->53476|57958->53573
                  LINES: 27->1|32->1|33->2|35->4|35->4|44->13|44->13|44->13|46->15|46->15|46->15|46->15|46->15|46->15|46->15|48->17|53->22|61->30|61->30|61->30|63->32|70->39|72->41|78->47|86->55|86->55|86->55|88->57|95->64|97->66|99->68|105->74|105->74|105->74|107->76|113->82|117->86|120->89|135->104|135->104|143->112|143->112|158->127|162->131|163->132|209->178|209->178|209->178|211->180|216->185|217->186|219->188|240->209|240->209|240->209|241->210|241->210|241->210|241->210|241->210|241->210|241->210|241->210|241->210|241->210|241->210|241->210|241->210|241->210|241->210|241->210|241->210|241->210|241->210|241->210|241->210|241->210|241->210|241->210|241->210|241->210|241->210|241->210|241->210|241->210|241->210|243->212|244->213|519->488|523->492|524->493|611->580|611->580|611->580|613->582|613->582|613->582|614->583|620->589|621->590|656->625|656->625|716->685|716->685|716->685|718->687|718->687|718->687|718->687|718->687|718->687|718->687|719->688|722->691|771->740|771->740|771->740|773->742|773->742|773->742|773->742|773->742|773->742|773->742|774->743|777->746|1201->1170|1201->1170|1201->1170|1203->1172|1203->1172|1203->1172|1203->1172|1203->1172|1205->1174|1205->1174|1321->1290|1321->1290|1327->1296|1327->1296
                  -- GENERATED --
              */
          