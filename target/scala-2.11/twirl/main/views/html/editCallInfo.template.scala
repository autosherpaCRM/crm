
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object editCallInfo_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class editCallInfo extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template7[String,String,String,String,Long,models.CallInteraction,Service,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(oem:String,dealercode:String,dealerName:String,userName:String,id: Long,postcallinfo:models.CallInteraction,latest_service:Service):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
import models._
import scala._

Seq[Any](format.raw/*1.134*/("""
"""),_display_(/*4.2*/mainPageCRE("AutoSherpaCRM",userName,dealerName,dealercode,oem)/*4.65*/ {_display_(Seq[Any](format.raw/*4.67*/("""
"""),_display_(/*5.2*/helper/*5.8*/.form(routes.CallInfoController.postcallLogeditForCRE(id), 'class -> "form-horizontal",'enctype ->     "multipart/form-data", 'id -> "updateForm")/*5.154*/  {_display_(Seq[Any](format.raw/*5.157*/("""
        
"""),format.raw/*7.1*/("""<div class="row" >
  <div class="col-lg-12">
    <div class="panel panel-primary">
      <div class="panel-heading">   
        Customer Information </div>
		<div class="panel-body">
            <div class="col-lg-3">
              <div class="form-group">
                <label for="customerName">Customer Name :</label>
                <input type="text" class="form-control" name="customerName" value='"""),_display_(/*16.85*/{postcallinfo.getCustomer().getCustomerName()}),format.raw/*16.131*/("""' id="customerName" autocomplete="off"  required/>
              </div>
            </div>
             <div class="col-lg-3">             
              <div class="form-group">
                <label for="customerPhone">Customer Phone:</label>
                <input type="text" class="form-control"  value='' id="customerPhone" autocomplete="off" readonly/>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label for="customerEmail">Customer Email:</label>
                <input type="text" class="form-control"  id="customerEmail" autocomplete="off"  />
              </div>
            </div>
           <div class="col-lg-3">
              <div class="form-group">
                <label for="customerAddress">Customer Address :</label>
                <input type="text" class="form-control"  value='' id="customerAddress" autocomplete="off"  />
              </div>
            </div>
             <div class="col-lg-3">
              <div class="form-group">
                <label for="vehicleNumber">Vehicle Number :</label>
                <input type="text" class="form-control" name="vehicleNumber" value='"""),_display_(/*40.86*/{postcallinfo.getVehicle().getVehicleNumber()}),format.raw/*40.132*/("""' id="vehicleNumber" autocomplete="off" readonly />
              </div>
            </div>
          <div class="col-lg-3">
              <div class="form-group">
                <label for="vehicleRegNo">Vehicle RegNo :</label>
                <input type="text" class="form-control" name="vehicleRegNo" value='"""),_display_(/*46.85*/{postcallinfo.getVehicle().getVehicleRegNo()}),format.raw/*46.130*/("""' id="vehicleRegNo" autocomplete="off"  readonly/>
              </div>
            </div>
            
            <div class="col-lg-3">
              <div class="form-group">
                <label for="model">Model :</label>
                <input type="text" class="form-control" name="model" value='"""),_display_(/*53.78*/{postcallinfo.getVehicle().getModel()}),format.raw/*53.116*/("""' id="model" autocomplete="off"  />
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label for="odometerReading">Odometer Reading :</label>
                <input type="text" class="form-control" name="odometerReading" value='"""),_display_(/*59.88*/{postcallinfo.getVehicle().getOdometerReading()}),format.raw/*59.136*/("""' id="odometerReading" autocomplete="off"  />
              </div>
            </div>                   
            
         
        </div>
        </div>
     </div>
     </div>
     
        
           
<div class="row" >
  <div class="col-lg-12">
    <div class="panel panel-primary">
      <div class="panel-heading">   
        Service Information </div>
		<div class="panel-body">         
              
             
            <div class="col-lg-3">
              <div class="form-group">
                <label for="nextServiceDue">Next Service Due Date :</label>
                <input type="text" class="form-control"  name="nextServiceDue" value='"""),_display_(/*82.88*/{latest_service.getNextServiceDue()}),format.raw/*82.124*/("""' id="nextServiceDue"  autocomplete="off" readonly />
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label for="nextServiceType">Next Service Type:</label>
                <input type="text" class="form-control"  name="nextServiceType" value='"""),_display_(/*88.89*/{latest_service.getNextServiceType()}),format.raw/*88.126*/("""' id="nextServiceType"  autocomplete="off"  readonly/>
              </div>
          </div>
          
         <div class="col-lg-3">
              <div class="form-group">
                <label for="forecastLogic">Forecast Logic:</label>
                <input type="text" class="form-control"  name="forecastLogic" value='"""),_display_(/*95.87*/{postcallinfo.getVehicle().getForecastLogic()}),format.raw/*95.133*/("""' id="forecastLogic"  autocomplete="off"  readonly/>
              </div>
          </div>
             
               
         <div class="col-lg-3">
              <div class="form-group">
                <label for="serviceNoShowPeriod">Service NO Show Period: </label>
                <input type="text" class="form-control"  name="serviceNoShowPeriod" value='"""),_display_(/*103.93*/{latest_service.getServiceNoShowPeriod()}),format.raw/*103.134*/("""' id="serviceNoShowPeriod"  autocomplete="off" readonly />
              </div>
          </div>
             <div class="col-lg-3">
              <div class="form-group">
                <label for="nextServisedueBasedon_Tenure">Service Due Based on Tenure: </label>
                <input type="text" class="form-control"  name="nextServisedueBasedon_Tenure" value='"""),_display_(/*109.102*/{postcallinfo.getVehicle().getNextServisedueBasedon_Tenure()}),format.raw/*109.163*/("""' id="nextServisedueBasedon_Tenure"  autocomplete="off" readonly />
              </div>
          </div>  
               <div class="col-lg-3">
              <div class="form-group">
                <label for="nextServiseDateBasedon_Running"> Service Due Based on Mileage: </label>
                <input type="text" class="form-control"  name="nextServiseDateBasedon_Running" value='"""),_display_(/*115.104*/{postcallinfo.getVehicle().getNextServiseDateBasedon_Running}),format.raw/*115.165*/("""' id="nextServiseDateBasedon_Running"  autocomplete="off"  readonly/>
              </div>
          </div>
              
            <div class="col-lg-3">
              <div class="form-group">
                <label for="lastServiceMeterReading">Last Service Meter Reading:</label>
                <input type="text" class="form-control" name="lastServiceMeterReading" value='"""),_display_(/*122.96*/{latest_service.getLastServiceMeterReading()}),format.raw/*122.141*/("""' id="lastServiceMeterReading"  autocomplete="off"  readonly/>
              </div>
            </div>          
            <div class="col-lg-3">
              <div class="form-group">
                <label for="lastServiceType">Last Service Type :</label>
                <input type="text" class="form-control" name="lastServiceType" value='"""),_display_(/*128.88*/{latest_service.getLastServiceType()}),format.raw/*128.125*/("""' id="lastServiceType"  autocomplete="off"  readonly/>
              </div>
            </div>
            
            <div class="col-lg-3">
              <div class="form-group">
                <label for="lastServiceDate">Last Service Date:</label>
                <input type="text"  class="form-control" name="lastServiceDate" value='"""),_display_(/*135.89*/{latest_service.getLastServiceDate}),format.raw/*135.124*/("""' id="lastServiceDate"  autocomplete="off"  readonly/>
              </div>
            </div> 
             <div class="col-lg-3">
              <div class="form-group">
                <label for="saleDate">Sale Date :</label>
                <input type="text" class="form-control" name="saleDate" value='"""),_display_(/*141.81*/{latest_service.getSaleDate()}),format.raw/*141.111*/("""' id="saleDate"   autocomplete="off"  readonly/>
              </div>
            </div>
              
         
        </div>
        </div>
        </div>
        </div>
        
     <div class="row">
         <div class="col-lg-12">
      
  <div class="panel panel-primary">
    <div class="panel-heading"> Interaction History</div>
    <div class="panel-body">          
            """),_display_(/*157.14*/if(postcallinfo.getSrdisposition().getCallDispositionData().getDisposition()=="Book My Service")/*157.110*/{_display_(Seq[Any](format.raw/*157.111*/(""" 
           """),format.raw/*158.12*/("""<div class="col-lg-3">
              <div class="form-group">
                <label for="serviceScheduledDate">Service Scheduled Date :</label>
                <input type="text"  class="datepicker form-control" name="serviceScheduledDate" value='' id="serviceScheduledDate" autocomplete="off"  />
              </div>
            </div>
             <div class="col-lg-3">
              <div class="form-group">
                <label for="serviceScheduledTime">Service Scheduled Time :</label>
                <input type="text"  class="single-input form-control" name="serviceScheduledTime" value='' id="serviceScheduledTime" autocomplete="off"  />
              </div>
            </div>
            
            <div class="col-md-3">
            <div class="form-group">
              <label for="pickUpAddress">Pick Up Address :</label>
              <input type="text" class="form-control" name="pickUpAddress" value='"""),_display_(/*174.84*/{postcallinfo.getServiceBooked().getPickupDrop().getPickUpAddress()}),format.raw/*174.152*/("""' id="pickUpAddress" autocomplete="off"/>             
            </div>
          </div>          
          <div class="col-md-3">
            <div class="form-group">
              <label for="serviceAdvisor">ASSIGNED TO (SA):</label>
              <input type="text"  class="form-control" name="serviceAdvisor" value='"""),_display_(/*180.86*/{postcallinfo.getServiceBooked().getServiceAdvisor()}),format.raw/*180.139*/("""' id="serviceAdvisor"  autocomplete="off"  readonly />             
            </div>
          </div>
          
           """)))}),format.raw/*184.13*/("""
           """),_display_(/*185.13*/if(postcallinfo.getSrdisposition().getCallDispositionData().getDisposition()=="Call Me Later")/*185.107*/{_display_(Seq[Any](format.raw/*185.108*/(""" 
           """),format.raw/*186.12*/("""<div class="col-lg-3">
              <div class="form-group">
                <label for="followUpDate">Follow Up Date :</label>
                <input type="text"  class="datepicker form-control" name="followUpDate" value='"""),_display_(/*189.97*/{postcallinfo.getSrdisposition().getFollowUpDate()}),format.raw/*189.148*/("""' id="followUpDate" autocomplete="off"  />
              </div
            </div>
    </div>
             <div class="col-lg-3">
              <div class="form-group">
                <label for="followUpTime">Follow Up Time :</label>
                <input type="text"  class="single-input form-control" name="followUpTime" value='"""),_display_(/*196.99*/{postcallinfo.getSrdisposition().getFollowUpTime()}),format.raw/*196.150*/("""' id="followUpTime" autocomplete="off"  />
              </div>
             </div>
           """)))}),format.raw/*199.13*/(""" 
            
          """),format.raw/*201.11*/("""<div class="col-lg-3">
              <div class="form-group">
                <label for="comments">Comments :</label>
                <input type="text" class="form-control" name="comments" value='"""),_display_(/*204.81*/{postcallinfo.getSrdisposition().getComments()}),format.raw/*204.128*/("""' id="comments" autocomplete="off"  />
              </div>
            </div> 
           <div class="col-lg-3">
              <div class="form-group">
                <label >Call Disposition :</label>
                <input type="text" class="form-control"  value='"""),_display_(/*210.66*/{postcallinfo.getSrdisposition().getCallDispositionData().getDisposition()}),format.raw/*210.141*/("""' id="callDisposition" autocomplete="off"  readonly/>
              </div>
            </div>
          </div> 
            
             
          </div>       
      
      
       <div class="col-lg-12">
        <div class="col-lg-6">
      
      <div class='text-right'>
        <input type="submit" class="btn btn-primary" value="Save Changes" />
      </div>
      </div>
       <div class="col-lg-6">
      <div>
      <a href="/CRE/getDispositionPageOfTab" class="btn btn-primary">Cancel</a>
      </div>
      </div>
      </div>
      
      
      </div>
     </div>
    
     
       
    
  
""")))}),format.raw/*241.2*/("""
""")))}),format.raw/*242.2*/("""
"""))
      }
    }
  }

  def render(oem:String,dealercode:String,dealerName:String,userName:String,id:Long,postcallinfo:models.CallInteraction,latest_service:Service): play.twirl.api.HtmlFormat.Appendable = apply(oem,dealercode,dealerName,userName,id,postcallinfo,latest_service)

  def f:((String,String,String,String,Long,models.CallInteraction,Service) => play.twirl.api.HtmlFormat.Appendable) = (oem,dealercode,dealerName,userName,id,postcallinfo,latest_service) => apply(oem,dealercode,dealerName,userName,id,postcallinfo,latest_service)

  def ref: this.type = this

}


}

/**/
object editCallInfo extends editCallInfo_Scope0.editCallInfo
              /*
                  -- GENERATED --
                  DATE: Thu Dec 28 12:24:28 IST 2017
                  SOURCE: D:/CRMNEW/28th Dec/crm/app/views/editCallInfo.scala.html
                  HASH: dfc884cef2ab478cdae31a166a4c92dac49d1f08
                  MATRIX: 816->1|1074->133|1102->171|1173->234|1212->236|1240->239|1253->245|1408->391|1449->394|1487->406|1930->822|1998->868|3253->2096|3321->2142|3668->2462|3735->2507|4075->2820|4135->2858|4477->3173|4547->3221|5263->3910|5321->3946|5682->4280|5741->4317|6103->4652|6171->4698|6573->5072|6637->5113|7041->5488|7125->5549|7548->5943|7632->6004|8048->6392|8116->6437|8497->6790|8557->6827|8934->7176|8992->7211|9335->7526|9388->7556|9824->7964|9931->8060|9972->8061|10015->8075|10987->9019|11078->9087|11436->9417|11512->9470|11675->9601|11717->9615|11822->9709|11863->9710|11906->9724|12162->9952|12236->10003|12604->10343|12678->10394|12809->10493|12865->10520|13095->10722|13165->10769|13468->11044|13566->11119|14236->11758|14270->11761
                  LINES: 27->1|33->1|34->4|34->4|34->4|35->5|35->5|35->5|35->5|37->7|46->16|46->16|70->40|70->40|76->46|76->46|83->53|83->53|89->59|89->59|112->82|112->82|118->88|118->88|125->95|125->95|133->103|133->103|139->109|139->109|145->115|145->115|152->122|152->122|158->128|158->128|165->135|165->135|171->141|171->141|187->157|187->157|187->157|188->158|204->174|204->174|210->180|210->180|214->184|215->185|215->185|215->185|216->186|219->189|219->189|226->196|226->196|229->199|231->201|234->204|234->204|240->210|240->210|271->241|272->242
                  -- GENERATED --
              */
          