
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object PSFCallLogPageCREManager_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class PSFCallLogPageCREManager extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template7[List[Location],String,String,Map[Long, String],List[Campaign],List[ServiceTypes],List[CallDispositionData],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(listCity:List[Location],dealerName:String,user:String,allCREHash:Map[Long,String],campaignListPSF :List[Campaign],serviceTypeList :List[ServiceTypes],dispoList:List[CallDispositionData]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.189*/("""
"""),_display_(/*2.2*/mainPageCREManger("AutoSherpaCRM",user,dealerName)/*2.52*/ {_display_(Seq[Any](format.raw/*2.54*/("""	



"""),format.raw/*6.1*/("""<style>
    td, th """),format.raw/*7.12*/("""{"""),format.raw/*7.13*/("""
        """),format.raw/*8.9*/("""border: 1px solid black;
        overflow: hidden; 
        text-align:center!important;
    """),format.raw/*11.5*/("""}"""),format.raw/*11.6*/("""
"""),format.raw/*12.1*/("""</style>

        <div class="panel panel-primary">
            <div class="panel-heading">   
                PSF Call Log Information </div>
            <div class="panel-body">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#home" data-toggle="tab" id="Tab1" onclick="PSFassignedInteractionDataMR();">Scheduled</a> </li>
                    <li><a href="#profile" data-toggle="tab" id="Tab2" onclick="ajaxPSFCallForFollowUpRequiredServerMR(4);">Follow Up Required</a> </li>
                    <li><a href="#messages" data-toggle="tab" id="Tab3" onclick="ajaxPSFCallForFollowUpRequiredServerMR(22);">Completed Survey</a> </li>
                    <li><a href="#settings" data-toggle="tab" id="Tab4" onclick="ajaxPSFCallForFollowUpRequiredServerMR(25);" >Appointments</a> </li>
                    <li><a href="#nonContacts" data-toggle="tab" id="Tab5" onclick="ajaxPSFCallForNonContactsServerMR(1);">Non Contacts</a> </li>
                    <li><a href="#droppedBuc" data-toggle="tab" id="Tab6" onclick="ajaxPSFCallForNonContactsServerMR(2);">Dropped</a> </li>
                </ul>
                <div class="tab-content">
                <div class="panel panel-default" style="border-top: #fff;">
				<div class="panel-body">
                <div class="row">
		<div class="col-md-2" id="cityDiv">
		<div class="form-group">
                <label>City </label>
               
               <select class="form-control" id="city" name="cityName" onchange="ajaxCallToLoadWorkShopByCity();"> 
                <option value='--Select--'>--Select--</option>
                
				"""),_display_(/*37.6*/for(city <- listCity) yield /*37.27*/{_display_(Seq[Any](format.raw/*37.28*/("""                  	                         
                    """),format.raw/*38.21*/("""<option value=""""),_display_(/*38.37*/city/*38.41*/.getName()),format.raw/*38.51*/("""">"""),_display_(/*38.54*/city/*38.58*/.getName()),format.raw/*38.68*/("""</option>
                    """)))}),format.raw/*39.22*/("""
                """),format.raw/*40.17*/("""</select>
</div>
            </div> 
			<div class="col-md-2" id="workshopDiv">
			<div class="form-group">
                <label>WorkShop Location </label>               
                <select class="form-control" id="workshop" name="workshopId" onchange="ajaxCallToLoadCRESByWorkshop();">                 			    
                

                </select>
			</div>
            </div> 
		
            <div class="col-md-2" id="cresDiv">
			 <label>Select CRE's </label>
			<div class="form-group">
              <select class="selectpicker filter form-control" data-column-index="7" id="ddlCreIds" name="ddlCreIds" multiple>
                
                    
                </select> 
</div>


            </div>
            
                <div class="col-md-2 tab-pane" id="campaignDiv" >
                                
                                <label>Select Campaign</label>
						<select class="filter form-control" id="campaignName" data-column-index="0" name="campaignName">
							<option value="0" >--Select--</option>
							"""),_display_(/*70.9*/for(campaign_List<-campaignListPSF) yield /*70.44*/{_display_(Seq[Any](format.raw/*70.45*/("""
		                                """),format.raw/*71.35*/("""<option value=""""),_display_(/*71.51*/campaign_List/*71.64*/.getCampaignName()),format.raw/*71.82*/("""">"""),_display_(/*71.85*/campaign_List/*71.98*/.getCampaignName()),format.raw/*71.116*/("""</option>
		                             """)))}),format.raw/*72.33*/("""
		                                
						"""),format.raw/*74.7*/("""</select>
					</div>
					
					<div class="col-md-2" id="fromDateDiv">
					<label>From Assign Date : </label>
					<input type="text" class="filter form-control datepickerFilter" placeholder="From Assign Date" data-column-index="1" id="fromduedaterange" name="fromduedaterange" readonly>
				  </div>
				  <div class="col-md-2" id="toDateDiv">
					<label>To Assign Date : </label>
					<input type="text" class="filter form-control datepickerFilter" placeholder="To Assign Date" data-column-index="2" id="toduedaterange" name="toduedaterange" readonly>
				  </div>
				  </div>
				  <div class="row">
				   
						<div class="col-md-2" id="serviceTypeDiv" style="display:none;">
					
			
						<label>Select PSF Day</label>
						<select class="filter form-control" id="ServiceTypeName" data-column-index="3" name="ServiceTypeName">
							<option value="0" >--Select--</option>
							<option value="psf1stday">PSF 1st Day</option>
							<option value="psf3rdday">PSF 3rd Day</option>
							<option value="psf4thday">PSF 4th Day</option>
							<option value="psf6thday">PSF 6th Day</option>
							<option value="psf15thday">PSF 15th Day</option>
							<option value="psf30thday">PSF 30th Day</option>
							
			                                
						</select>
					</div>
					 
					<div class="col-md-3" id="serviceBookTypeDiv" style="display:none">						
					<label>Select Booked service type</label>
						<select class="filter form-control" id="serviceBookedType" data-column-index="4" name="serviceBookedType">
							<option value="0" >--Select--</option>
	
					                                
						</select>
					</div>
					<div class="col-md-3" id="lastDispoTypeDiv" style="display:none">						
					<label>Last Disposition</label>
						<select class="filter form-control" id="lastDispo" data-column-index="5" >
							<option value="0" >--Select--</option>
								"""),_display_(/*117.10*/for(dispo <- dispoList) yield /*117.33*/{_display_(Seq[Any](format.raw/*117.34*/("""
			"""),format.raw/*118.4*/("""<option value=""""),_display_(/*118.20*/dispo/*118.25*/.getDisposition()),format.raw/*118.42*/("""">"""),_display_(/*118.45*/dispo/*118.50*/.getDisposition()),format.raw/*118.67*/("""</option>
			
		""")))}),format.raw/*120.4*/("""
							
					                                
						"""),format.raw/*123.7*/("""</select>
					</div>
					<div class="col-md-3" id="droppedcountDiv" style="display:none">						
					<label>Dropped Count</label>
						<select class="filter form-control" id="droppedCount" data-column-index="6">
							<option value="0" >--Select--</option>
							<option value="1" >1</option>
							<option value="2" >2</option>
							<option value="3" >3</option>
							<option value="4" >4</option>
							
					                                
						</select>
					</div>
				</div>
				</div>
				</div>
                
                    <div class="panel panel-default tab-pane fade in active" id="home">
                        <div class="panel-body inf-content">              

                            <div class="dataTable_wrapper">                            
                                <div >
                                    <table class="table table-striped table-bordered table-hover" id="PSFassignedInteractionTableMR">
                                        <thead>
                                            <tr> 

                                            
				    		<th>Campaign Name</th>
				    		<th>CRE Name</th>
				    		<th>Customer Name</th>
				    		<th>Mobile Number</th>
				    		<th>Vehicle Reg No</th>
				    		<th>Model</th>
				    		<th>RONumber</th>
				    		<th>RODate</th>
				    		<th>Bill Date</th>
				    		<th>Category</th>
                                            </tr>
                                        </thead>
                                        <tbody>								
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="profile">
                        <div class="panel-body inf-content">
                            <div class="dataTable_wrapper">                            
                                <div >
                                    <table class="table table-striped table-bordered table-hover" id="PSFfollowUpRequiredServerDataMR4" >
                                        <thead>
                                         <tr> 
                                          		<th>Campaign Name</th>
                                          		<th>CRE Name</th>
                                                <th>Customer Name</th>
                                                <th>Mobile Number</th>                                                                   
                                                <th>Vehicle RegNo.</th>
                                                <th>FollowUp Date</th>                                    
                                                <th>FollowUp Time</th>
                                                <th>Call date</th>
                                                <th>RO Number</th>
                                                <th>RO Date</th>
                                                <th>Bill Date</th>
                                                <th>Survey Date</th>
                                                <th>Model</th>                                                                                               
                                                
                                            </tr>   
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="messages">
                        <div class="panel-body inf-content">
                            <div class="dataTable_wrapper">                            
                                <div >
                                    <table class="table table-striped table-bordered table-hover" id="PSFfollowUpRequiredServerDataMR22">
                                        <thead>
                                            <tr> 
                                            	<th>Campaign Name</th>
                                          		<th>CRE Name</th>
                                                <th>Customer Name</th>
                                                <th>Mobile Number</th>                                                                   
                                                <th>Vehicle RegNo.</th>
                                                <th>Call date</th>
                                                <th>RO Number</th>
                                                <th>RO Date</th>
                                                <th>Bill Date</th>
                                                <th>Survey Date</th>
                                                <th>Model</th> 
                                                                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="settings">
                         <div class="panel-body inf-content">
                        <div class="dataTable_wrapper">                            
                            <div >
                                <table class="table table-striped table-bordered table-hover" id="PSFfollowUpRequiredServerDataMR25">
                                    <thead>
                                        <tr> 
                                        	<th>Campaign Name</th>
                                          		<th>CRE Name</th>
                                                <th>Customer Name</th>
                                                <th>Mobile Number</th>                                                                   
                                                <th>Vehicle RegNo.</th>
                                                <th>Call date</th>
                                                <th>RO Number</th>
                                                <th>RO Date</th>
                                                <th>Bill Date</th>
                                                <th>Survey Date</th>
                                                <th>Model</th>
                                                <th>PSF Appointment Date</th>
                                                <th>PSF Appointment Time</th>
                                                  
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    </div><!--End tab panel-->
                    <div class="tab-pane fade " id="nonContacts">
                        <div class="panel-body inf-content"> 
                            <div >
                                <table class="table table-striped table-bordered table-hover" id="PSFnonContactsServerDataMR1">
                                    <thead>
                                        <tr> 
                                        	<th>Campaign Name</th>
                                       	 	<th>CRE Name</th>                         
                                            <th>Customer Name</th>
                                            <th>Mobile Number</th>         
                                            <th>Vehicle RegNo.</th>
                                            <th>Call date</th>
                                                <th>RO Number</th>
                                                <th>Bill Date</th>
                                                <th>Survey Date</th>
                                                <th>Model</th>
                                                <th>Last Disposition</th>
                                                 											
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="droppedBuc">
                        <div class="panel-body inf-content">
                            <div class="dataTable_wrapper">                            
                                <div style="overflow-x: auto">
                                    <table class="table table-striped table-bordered table-hover" id="PSFnonContactsServerDataMR2">
                                        <thead>
                                                 <tr> 
                                        	<th>Campaign Name</th>
                                       	 	<th>CRE Name</th>                         
                                            <th>Customer Name</th>
                                            <th>Mobile Number</th>         
                                            <th>Vehicle RegNo.</th>                                      
                                                <th>Call date</th>
                                                <th>RO Number</th>
                                                <th>Bill Date</th>
                                                <th>Survey Date</th>
                                                <th>Model</th>
                                                <th>Last Disposition</th>
                                                 											
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
				</div><!--End tab content-->
            </div><!--End panel body-->
        </div>
   

""")))}),format.raw/*317.2*/(""" 

"""),format.raw/*319.1*/("""<script type="text/javascript"> 

function PSFassignedInteractionDataMR()"""),format.raw/*321.40*/("""{"""),format.raw/*321.41*/("""
    """),format.raw/*322.5*/("""// alert("server side data load");
    document.getElementById("cresDiv").style.display = "block";
	//document.getElementById("workshopDiv").style.display = "block";
	//document.getElementById("cityDiv").style.display = "block";	
	document.getElementById("fromDateDiv").style.display = "block";
	document.getElementById("toDateDiv").style.display = "block";
	document.getElementById("campaignDiv").style.display = "block";
    //document.getElementById("serviceBookTypeDiv").style.display = "none";
    //document.getElementById("serviceTypeDiv").style.display = "block";

	//document.getElementById("lastDispoTypeDiv").style.display = "none";
	//document.getElementById("droppedcountDiv").style.display = "none";

    var UserIds="",i;
    myOption = document.getElementById('ddlCreIds');
    
for (i=0;i<myOption.options.length;i++)"""),format.raw/*338.40*/("""{"""),format.raw/*338.41*/("""
    """),format.raw/*339.5*/("""if(myOption.options[i].selected)"""),format.raw/*339.37*/("""{"""),format.raw/*339.38*/("""
        """),format.raw/*340.9*/("""if(myOption.options[i].value != "--Select--")"""),format.raw/*340.54*/("""{"""),format.raw/*340.55*/("""
    	 """),format.raw/*341.7*/("""UserIds = UserIds + myOption.options[i].value + ",";
    	 console.log("Users id"+UserIds);
        """),format.raw/*343.9*/("""}"""),format.raw/*343.10*/("""
    """),format.raw/*344.5*/("""}"""),format.raw/*344.6*/("""
"""),format.raw/*345.1*/("""}"""),format.raw/*345.2*/("""
"""),format.raw/*346.1*/("""if(UserIds.length > 0)"""),format.raw/*346.23*/("""{"""),format.raw/*346.24*/("""
    	"""),format.raw/*347.6*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
"""),format.raw/*348.1*/("""}"""),format.raw/*348.2*/("""
"""),format.raw/*349.1*/("""else"""),format.raw/*349.5*/("""{"""),format.raw/*349.6*/("""
"""),format.raw/*350.1*/("""UserIds="Select";
"""),format.raw/*351.1*/("""}"""),format.raw/*351.2*/("""
    """),format.raw/*352.5*/("""var ajaxUrl = "/CREManager/PSFassignedInteractionTableDataMR/"+UserIds+"";
    
    
    var table= $('#PSFassignedInteractionTableMR').dataTable( """),format.raw/*355.63*/("""{"""),format.raw/*355.64*/("""
        """),format.raw/*356.9*/(""""bDestroy": true,
        "processing": true,
        "serverSide": true,
        "scrollY": 300,
		 "scrollX": true,
        "paging": true,
        "searching":true,
        "ordering":false,	
        "ajax": ajaxUrl,
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*365.59*/("""{"""),format.raw/*365.60*/("""
              """),format.raw/*366.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*368.15*/("""}"""),format.raw/*368.16*/("""
    """),format.raw/*369.5*/("""}"""),format.raw/*369.6*/(""" """),format.raw/*369.7*/(""");

    FilterOptionDataMR(table);
    
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e)"""),format.raw/*373.61*/("""{"""),format.raw/*373.62*/("""
      """),format.raw/*374.7*/("""$($.fn.dataTable.tables(true)).DataTable()
         .columns.adjust();
   """),format.raw/*376.4*/("""}"""),format.raw/*376.5*/(""");
    
    """),format.raw/*378.5*/("""}"""),format.raw/*378.6*/("""




"""),format.raw/*383.1*/("""function ajaxPSFCallForFollowUpRequiredServerMR(buckettype)"""),format.raw/*383.60*/("""{"""),format.raw/*383.61*/("""
	"""),format.raw/*384.2*/("""document.getElementById("cresDiv").style.display = "block";
	//document.getElementById("workshopDiv").style.display = "block";
	//document.getElementById("cityDiv").style.display = "block";	
	document.getElementById("campaignDiv").style.display = "block";
	//document.getElementById("serviceBookTypeDiv").style.display = "none";
	//document.getElementById("serviceTypeDiv").style.display = "block";
	document.getElementById("fromDateDiv").style.display = "block";
	document.getElementById("toDateDiv").style.display = "block";
	document.getElementById("lastDispoTypeDiv").style.display = "block";
	//document.getElementById("droppedcountDiv").style.display = "none";
	          

	    var UserIds="",i;
	    myOption = document.getElementById('ddlCreIds');
	    
	for (i=0;i<myOption.options.length;i++)"""),format.raw/*399.41*/("""{"""),format.raw/*399.42*/("""
	    """),format.raw/*400.6*/("""if(myOption.options[i].selected)"""),format.raw/*400.38*/("""{"""),format.raw/*400.39*/("""
	        """),format.raw/*401.10*/("""if(myOption.options[i].value != "--Select--")"""),format.raw/*401.55*/("""{"""),format.raw/*401.56*/("""
	    	 """),format.raw/*402.8*/("""UserIds = UserIds + myOption.options[i].value + ",";
	        """),format.raw/*403.10*/("""}"""),format.raw/*403.11*/("""
	    """),format.raw/*404.6*/("""}"""),format.raw/*404.7*/("""
	"""),format.raw/*405.2*/("""}"""),format.raw/*405.3*/("""
	"""),format.raw/*406.2*/("""if(UserIds.length > 0)"""),format.raw/*406.24*/("""{"""),format.raw/*406.25*/("""
	    	"""),format.raw/*407.7*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
	"""),format.raw/*408.2*/("""}"""),format.raw/*408.3*/("""
	"""),format.raw/*409.2*/("""else"""),format.raw/*409.6*/("""{"""),format.raw/*409.7*/("""
	"""),format.raw/*410.2*/("""UserIds="Select";
	"""),format.raw/*411.2*/("""}"""),format.raw/*411.3*/("""
	    """),format.raw/*412.6*/("""var ajaxUrl = "/CREManager/PSFfollowUpCallLogTableDataMR/"+UserIds+"/"+buckettype+"";
	    
	    
	    var table= $('#PSFfollowUpRequiredServerDataMR'+buckettype).dataTable( """),format.raw/*415.77*/("""{"""),format.raw/*415.78*/("""
	        """),format.raw/*416.10*/(""""bDestroy": true,
	        "processing": true,
	        "serverSide": true,
	        "scrollY": 300,
			 "scrollX": true,
	        "paging": true,
	        "searching":true,
	        "ordering":false,	
	        "ajax": ajaxUrl,
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*425.60*/("""{"""),format.raw/*425.61*/("""
              """),format.raw/*426.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*428.15*/("""}"""),format.raw/*428.16*/("""
	    """),format.raw/*429.6*/("""}"""),format.raw/*429.7*/(""" """),format.raw/*429.8*/(""");

	    FilterOptionDataMR(table);
	    
	"""),format.raw/*433.2*/("""}"""),format.raw/*433.3*/("""



"""),format.raw/*437.1*/("""function ajaxPSFCallForNonContactsServerMR(buckettype)"""),format.raw/*437.55*/("""{"""),format.raw/*437.56*/("""
	"""),format.raw/*438.2*/("""//alert("noncontact");
	
		document.getElementById("cresDiv").style.display = "block";
		//document.getElementById("workshopDiv").style.display = "block";
		//document.getElementById("cityDiv").style.display = "block";	
		document.getElementById("campaignDiv").style.display = "block";
		//document.getElementById("serviceBookTypeDiv").style.display = "none";
		//document.getElementById("serviceTypeDiv").style.display = "none";
		document.getElementById("fromDateDiv").style.display = "block";
		document.getElementById("toDateDiv").style.display = "block";
		document.getElementById("lastDispoTypeDiv").style.display = "block";
		//document.getElementById("droppedcountDiv").style.display = "block";

	var UserIds="",i;
    myOption = document.getElementById('ddlCreIds');
    
for (i=0;i<myOption.options.length;i++)"""),format.raw/*454.40*/("""{"""),format.raw/*454.41*/("""
    """),format.raw/*455.5*/("""if(myOption.options[i].selected)"""),format.raw/*455.37*/("""{"""),format.raw/*455.38*/("""
        """),format.raw/*456.9*/("""if(myOption.options[i].value != "Select")"""),format.raw/*456.50*/("""{"""),format.raw/*456.51*/("""
    		"""),format.raw/*457.7*/("""UserIds = UserIds + myOption.options[i].value + ",";
        """),format.raw/*458.9*/("""}"""),format.raw/*458.10*/("""
    """),format.raw/*459.5*/("""}"""),format.raw/*459.6*/("""
"""),format.raw/*460.1*/("""}"""),format.raw/*460.2*/("""
	"""),format.raw/*461.2*/("""if(UserIds.length > 0)"""),format.raw/*461.24*/("""{"""),format.raw/*461.25*/("""
    	"""),format.raw/*462.6*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
	"""),format.raw/*463.2*/("""}"""),format.raw/*463.3*/("""
	"""),format.raw/*464.2*/("""else"""),format.raw/*464.6*/("""{"""),format.raw/*464.7*/("""
		"""),format.raw/*465.3*/("""UserIds="Select";
	"""),format.raw/*466.2*/("""}"""),format.raw/*466.3*/("""
    """),format.raw/*467.5*/("""var ajaxUrl = "/CREManager/PSFnonContactsServerDataTableMR/"+UserIds+"/"+buckettype+"";
     
    var table = $('#PSFnonContactsServerDataMR'+buckettype).dataTable( """),format.raw/*469.72*/("""{"""),format.raw/*469.73*/("""
        """),format.raw/*470.9*/(""""bDestroy": true,
        "processing": true,
        "serverSide": true,
        "scrollY": 300,
		 "scrollX": true,
        "paging": true,
        "searching":true,
        "ordering":false,			
        "ajax": ajaxUrl,
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*479.59*/("""{"""),format.raw/*479.60*/("""
              """),format.raw/*480.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*482.15*/("""}"""),format.raw/*482.16*/("""
    """),format.raw/*483.5*/("""}"""),format.raw/*483.6*/(""" """),format.raw/*483.7*/(""");

    FilterOptionDataMR(table);
    
    """),format.raw/*487.5*/("""}"""),format.raw/*487.6*/("""






"""),format.raw/*494.1*/("""</script>
<script type="text/javascript">  
function FilterOptionDataMR(table)"""),format.raw/*496.35*/("""{"""),format.raw/*496.36*/("""
	"""),format.raw/*497.2*/("""var values = [];
	$('.filter').on('change', function() """),format.raw/*498.39*/("""{"""),format.raw/*498.40*/("""
    	"""),format.raw/*499.6*/("""console.log("inside filteroptionsdata data");
	    var i= $(this).data('columnIndex');
	    var v = $(this).val();
	    console.log("i : "+i);
		console.log("v length "+v.length);
	    
	    if(v.length>0)"""),format.raw/*505.20*/("""{"""),format.raw/*505.21*/("""
		    
		    """),format.raw/*507.7*/("""if(i == 7)"""),format.raw/*507.17*/("""{"""),format.raw/*507.18*/("""	

		    	"""),format.raw/*509.8*/("""var UserIds="",j;
		        myOption = document.getElementById('ddlCreIds');
		        console.log("here"+myOption);		        
		    for (j=0;j<myOption.options.length;j++)"""),format.raw/*512.46*/("""{"""),format.raw/*512.47*/("""
		        """),format.raw/*513.11*/("""if(myOption.options[j].selected)"""),format.raw/*513.43*/("""{"""),format.raw/*513.44*/("""
		            """),format.raw/*514.15*/("""if(myOption.options[j].value != "Select")"""),format.raw/*514.56*/("""{"""),format.raw/*514.57*/("""
		        		"""),format.raw/*515.13*/("""UserIds = UserIds + myOption.options[j].value + ",";
		            """),format.raw/*516.15*/("""}"""),format.raw/*516.16*/("""
		        """),format.raw/*517.11*/("""}"""),format.raw/*517.12*/("""
		    """),format.raw/*518.7*/("""}"""),format.raw/*518.8*/("""
		    	"""),format.raw/*519.8*/("""if(UserIds.length > 0)"""),format.raw/*519.30*/("""{"""),format.raw/*519.31*/("""
		        	"""),format.raw/*520.12*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
		        	values[7]=UserIds;
		    	"""),format.raw/*522.8*/("""}"""),format.raw/*522.9*/("""else"""),format.raw/*522.13*/("""{"""),format.raw/*522.14*/("""
		    		"""),format.raw/*523.9*/("""UserIds="Select";
		    		values[7]=UserIds;
		    	"""),format.raw/*525.8*/("""}"""),format.raw/*525.9*/("""
		    
		    """),format.raw/*527.7*/("""}"""),format.raw/*527.8*/("""else"""),format.raw/*527.12*/("""{"""),format.raw/*527.13*/("""
		    	"""),format.raw/*528.8*/("""values[i] = v;
			    """),format.raw/*529.8*/("""}"""),format.raw/*529.9*/("""
	
			       
		  
		    """),format.raw/*533.7*/("""}"""),format.raw/*533.8*/("""		    
	    
	    	"""),format.raw/*535.7*/("""console.log(JSON.stringify(values));	   	
	  	  //clear global search values
	 	  //table.api().search('');
	 	  $(this).data('columnIndex');
	 	 //var table = $('#nonContactsServerDataMR').dataTable();
	 	 if(values.length>0) """),format.raw/*540.25*/("""{"""),format.raw/*540.26*/("""
			 """),format.raw/*541.5*/("""for(var i=0;i<values.length;i++)"""),format.raw/*541.37*/("""{"""),format.raw/*541.38*/("""
				 """),format.raw/*542.6*/("""console.log("values[i] : "+values[i]);	
			    table.api().columns(i).search(values[i]);
			 """),format.raw/*544.5*/("""}"""),format.raw/*544.6*/("""
			 """),format.raw/*545.5*/("""table.api().draw(); 
		"""),format.raw/*546.3*/("""}"""),format.raw/*546.4*/("""
	 	
		 	  
	"""),format.raw/*549.2*/("""}"""),format.raw/*549.3*/(""");
	
	
"""),format.raw/*552.1*/("""}"""),format.raw/*552.2*/("""
"""),format.raw/*553.1*/("""</script>

 <script type="text/javascript">
	window.onload = PSFassignedInteractionDataMR();
</script>
    """))
      }
    }
  }

  def render(listCity:List[Location],dealerName:String,user:String,allCREHash:Map[Long, String],campaignListPSF:List[Campaign],serviceTypeList:List[ServiceTypes],dispoList:List[CallDispositionData]): play.twirl.api.HtmlFormat.Appendable = apply(listCity,dealerName,user,allCREHash,campaignListPSF,serviceTypeList,dispoList)

  def f:((List[Location],String,String,Map[Long, String],List[Campaign],List[ServiceTypes],List[CallDispositionData]) => play.twirl.api.HtmlFormat.Appendable) = (listCity,dealerName,user,allCREHash,campaignListPSF,serviceTypeList,dispoList) => apply(listCity,dealerName,user,allCREHash,campaignListPSF,serviceTypeList,dispoList)

  def ref: this.type = this

}


}

/**/
object PSFCallLogPageCREManager extends PSFCallLogPageCREManager_Scope0.PSFCallLogPageCREManager
              /*
                  -- GENERATED --
                  DATE: Thu Dec 28 15:26:37 IST 2017
                  SOURCE: D:/CRMNEW/28th Dec/crm/app/views/PSFCallLogPageCREManager.scala.html
                  HASH: 8c06c3028d22dcf2bba61ab65da1f9994445a289
                  MATRIX: 883->1|1166->188|1194->191|1252->241|1291->243|1326->252|1373->272|1401->273|1437->283|1560->379|1588->380|1617->382|3298->2037|3335->2058|3374->2059|3468->2125|3511->2141|3524->2145|3555->2155|3585->2158|3598->2162|3629->2172|3692->2204|3738->2222|4850->3308|4901->3343|4940->3344|5004->3380|5047->3396|5069->3409|5108->3427|5138->3430|5160->3443|5200->3461|5274->3504|5345->3548|7314->5489|7354->5512|7394->5513|7427->5518|7471->5534|7486->5539|7525->5556|7556->5559|7571->5564|7610->5581|7660->5600|7744->5656|18701->16582|18734->16587|18838->16662|18868->16663|18902->16669|19781->17519|19811->17520|19845->17526|19906->17558|19936->17559|19974->17569|20048->17614|20078->17615|20114->17623|20244->17725|20274->17726|20308->17732|20337->17733|20367->17735|20396->17736|20426->17738|20477->17760|20507->17761|20542->17768|20623->17821|20652->17822|20682->17824|20714->17828|20743->17829|20773->17831|20820->17850|20849->17851|20883->17857|21062->18007|21092->18008|21130->18018|21446->18305|21476->18306|21521->18322|21628->18400|21658->18401|21692->18407|21721->18408|21750->18409|21883->18513|21913->18514|21949->18522|22053->18598|22082->18599|22124->18613|22153->18614|22191->18624|22279->18683|22309->18684|22340->18687|23187->19505|23217->19506|23252->19513|23313->19545|23343->19546|23383->19557|23457->19602|23487->19603|23524->19612|23616->19675|23646->19676|23681->19683|23710->19684|23741->19687|23770->19688|23801->19691|23852->19713|23882->19714|23918->19722|24000->19776|24029->19777|24060->19780|24092->19784|24121->19785|24152->19788|24200->19808|24229->19809|24264->19816|24470->19993|24500->19994|24540->20005|24865->20301|24895->20302|24940->20318|25047->20396|25077->20397|25112->20404|25141->20405|25170->20406|25245->20453|25274->20454|25310->20462|25393->20516|25423->20517|25454->20520|26319->21356|26349->21357|26383->21363|26444->21395|26474->21396|26512->21406|26582->21447|26612->21448|26648->21456|26738->21518|26768->21519|26802->21525|26831->21526|26861->21528|26890->21529|26921->21532|26972->21554|27002->21555|27037->21562|27119->21616|27148->21617|27179->21620|27211->21624|27240->21625|27272->21629|27320->21649|27349->21650|27383->21656|27579->21823|27609->21824|27647->21834|27965->22123|27995->22124|28040->22140|28147->22218|28177->22219|28211->22225|28240->22226|28269->22227|28345->22275|28374->22276|28416->22290|28525->22370|28555->22371|28586->22374|28671->22430|28701->22431|28736->22438|28976->22649|29006->22650|29050->22666|29089->22676|29119->22677|29159->22689|29363->22864|29393->22865|29434->22877|29495->22909|29525->22910|29570->22926|29640->22967|29670->22968|29713->22982|29810->23050|29840->23051|29881->23063|29911->23064|29947->23072|29976->23073|30013->23082|30064->23104|30094->23105|30136->23118|30255->23209|30284->23210|30317->23214|30347->23215|30385->23225|30467->23279|30496->23280|30540->23296|30569->23297|30602->23301|30632->23302|30669->23311|30720->23334|30749->23335|30806->23364|30835->23365|30884->23386|31145->23618|31175->23619|31209->23625|31270->23657|31300->23658|31335->23665|31458->23760|31487->23761|31521->23767|31573->23791|31602->23792|31646->23808|31675->23809|31713->23819|31742->23820|31772->23822
                  LINES: 27->1|32->1|33->2|33->2|33->2|37->6|38->7|38->7|39->8|42->11|42->11|43->12|68->37|68->37|68->37|69->38|69->38|69->38|69->38|69->38|69->38|69->38|70->39|71->40|101->70|101->70|101->70|102->71|102->71|102->71|102->71|102->71|102->71|102->71|103->72|105->74|148->117|148->117|148->117|149->118|149->118|149->118|149->118|149->118|149->118|149->118|151->120|154->123|348->317|350->319|352->321|352->321|353->322|369->338|369->338|370->339|370->339|370->339|371->340|371->340|371->340|372->341|374->343|374->343|375->344|375->344|376->345|376->345|377->346|377->346|377->346|378->347|379->348|379->348|380->349|380->349|380->349|381->350|382->351|382->351|383->352|386->355|386->355|387->356|396->365|396->365|397->366|399->368|399->368|400->369|400->369|400->369|404->373|404->373|405->374|407->376|407->376|409->378|409->378|414->383|414->383|414->383|415->384|430->399|430->399|431->400|431->400|431->400|432->401|432->401|432->401|433->402|434->403|434->403|435->404|435->404|436->405|436->405|437->406|437->406|437->406|438->407|439->408|439->408|440->409|440->409|440->409|441->410|442->411|442->411|443->412|446->415|446->415|447->416|456->425|456->425|457->426|459->428|459->428|460->429|460->429|460->429|464->433|464->433|468->437|468->437|468->437|469->438|485->454|485->454|486->455|486->455|486->455|487->456|487->456|487->456|488->457|489->458|489->458|490->459|490->459|491->460|491->460|492->461|492->461|492->461|493->462|494->463|494->463|495->464|495->464|495->464|496->465|497->466|497->466|498->467|500->469|500->469|501->470|510->479|510->479|511->480|513->482|513->482|514->483|514->483|514->483|518->487|518->487|525->494|527->496|527->496|528->497|529->498|529->498|530->499|536->505|536->505|538->507|538->507|538->507|540->509|543->512|543->512|544->513|544->513|544->513|545->514|545->514|545->514|546->515|547->516|547->516|548->517|548->517|549->518|549->518|550->519|550->519|550->519|551->520|553->522|553->522|553->522|553->522|554->523|556->525|556->525|558->527|558->527|558->527|558->527|559->528|560->529|560->529|564->533|564->533|566->535|571->540|571->540|572->541|572->541|572->541|573->542|575->544|575->544|576->545|577->546|577->546|580->549|580->549|583->552|583->552|584->553
                  -- GENERATED --
              */
          