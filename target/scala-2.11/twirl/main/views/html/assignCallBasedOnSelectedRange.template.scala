
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object assignCallBasedOnSelectedRange_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class assignCallBasedOnSelectedRange extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template19[Long,Long,Long,String,String,String,String,Boolean,Boolean,List[String],List[Vehicle],String,String,List[Campaign],List[Location],List[Campaign],List[Campaign],List[webmodels.AssignedListForCRE],List[ServiceTypes],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(campaignNamePSF:Long,campaignNameCat:Long,campaignTypeCat:Long,custCat:String,serviceType:String,fromDateValue:String,toDateValue:String,isRangeSelected:Boolean,toAssigncall:Boolean,allCRE:List[String],filteredData:List[Vehicle],dealerName:String,user:String,campaign_list:List[Campaign],locationList:List[Location],campaignList:List[Campaign],campaignListPSF:List[Campaign],psf_assign_data:List[webmodels.AssignedListForCRE],servicetypes:List[ServiceTypes]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.461*/("""

"""),_display_(/*4.2*/mainPageCREManger("AutoSherpaCRM",user,dealerName)/*4.52*/ {_display_(Seq[Any](format.raw/*4.54*/("""

"""),_display_(/*6.2*/helper/*6.8*/.form(action = routes.CallInteractionController.getSelectedAssignCalls(),'enctype -> "multipart/form-data")/*6.115*/{_display_(Seq[Any](format.raw/*6.116*/("""

"""),format.raw/*8.1*/("""<input type="hidden" name="fromDateToAss" value=""""),_display_(/*8.51*/fromDateValue),format.raw/*8.64*/("""">
<input type="hidden" name="toDateToAss" value=""""),_display_(/*9.49*/toDateValue),format.raw/*9.60*/("""">
<input type="hidden" name="customerCatToAss" value=""""),_display_(/*10.54*/custCat),format.raw/*10.61*/("""">
<input type="hidden" name="serviceTypeToAss" value=""""),_display_(/*11.54*/serviceType),format.raw/*11.65*/("""">
<input type="hidden" name="campaignTypeToAss" value=""""),_display_(/*12.55*/campaignTypeCat),format.raw/*12.70*/("""">
<input type="hidden" name="campaignNameToAss" value=""""),_display_(/*13.55*/campaignNameCat),format.raw/*13.70*/("""">
<input type="hidden" name="campaignPSFToAss" value=""""),_display_(/*14.54*/campaignNamePSF),format.raw/*14.69*/("""">


<div class="panel panel-info">
        <div class="panel-heading"><strong>Call Allocation | To Assign Calls</strong> </div>
        <div class="panel-body">
            <div class="row">
                
                """),_display_(/*22.18*/if(toAssigncall)/*22.34*/{_display_(Seq[Any](format.raw/*22.35*/("""     
      """),format.raw/*23.7*/("""<div class="col-sm-3">   
      <div class="form-group">
       <label>Assignment Type :</label>    
      <select class="form-control" name="campaignType" id="campaignTypeData">  
      <option value='0'>--Select--</option>        
          """),_display_(/*28.12*/for(list_camp <- campaign_list) yield /*28.43*/{_display_(Seq[Any](format.raw/*28.44*/("""
		"""),format.raw/*29.3*/("""<option value=""""),_display_(/*29.19*/list_camp/*29.28*/.id),format.raw/*29.31*/("""">"""),_display_(/*29.34*/list_camp/*29.43*/.campaignType),format.raw/*29.56*/("""</option>		
	 """)))}),format.raw/*30.4*/("""
      """),format.raw/*31.7*/("""</select>
      </div>
      </div>
      """)))}),format.raw/*34.8*/(""" 

                                """),format.raw/*36.33*/("""<div class="col-md-3 campaignTypePSF" style="display:none;">
                                 <label>Select PSF Type :</label>    
                                <select  id="campaignNamePSF" name="campaignNamePSF" class="form-control" value=""""),_display_(/*38.115*/campaignNamePSF),format.raw/*38.130*/("""">
                                      <option value='0'>--Select--</option>
                                        """),_display_(/*40.42*/for(listcamp <- campaignListPSF) yield /*40.74*/{_display_(Seq[Any](format.raw/*40.75*/("""
                                """),format.raw/*41.33*/("""<option value=""""),_display_(/*41.49*/listcamp/*41.57*/.id),format.raw/*41.60*/("""">"""),_display_(/*41.63*/listcamp/*41.71*/.campaignName),format.raw/*41.84*/("""</option>		
                                         """)))}),format.raw/*42.43*/("""
                                """),format.raw/*43.33*/("""</select> 
                            </div>
            <div class="col-md-3 selectcampaign" style="display:none">
                                <div class="form-group">
                                    <label for="">Select Campaign Name</label>
                                    <select class="form-control" id="campaignName" name="campaignName" value=""""),_display_(/*48.112*/campaignNameCat),format.raw/*48.127*/("""">	
                                        <option value='0'>--Select--</option>
                                        """),_display_(/*50.42*/for(list_camp <- campaignList) yield /*50.72*/{_display_(Seq[Any](format.raw/*50.73*/("""
                                """),format.raw/*51.33*/("""<option value=""""),_display_(/*51.49*/list_camp/*51.58*/.id),format.raw/*51.61*/("""">"""),_display_(/*51.64*/list_camp/*51.73*/.campaignName),format.raw/*51.86*/("""</option>		
                                         """)))}),format.raw/*52.43*/("""
                                    
                                     
                                    """),format.raw/*55.37*/("""</select>

                                </div>
                            </div>
                           				
                                
            </div>   
              
        <div class="row">
            """),_display_(/*64.14*/if(isRangeSelected)/*64.33*/{_display_(Seq[Any](format.raw/*64.34*/("""
            """),format.raw/*65.13*/("""<div class=col-md-3>
                    <label>Select CRE's</label>
    			<select  multiple="multiple" id="data[]" name="data[]" class="selectpicker" required="required">                        			    
                     	"""),_display_(/*68.24*/for(data1 <- allCRE) yield /*68.44*/{_display_(Seq[Any](format.raw/*68.45*/("""                      	                         
                    
                        """),format.raw/*70.25*/("""<option value=""""),_display_(/*70.41*/data1),format.raw/*70.46*/("""">"""),_display_(/*70.49*/data1),format.raw/*70.54*/("""</option>
                      				
                      	""")))}),format.raw/*72.25*/("""			 	
                      					
                	"""),format.raw/*74.18*/("""</select>
                				
            </div>
            """)))}),format.raw/*77.14*/("""
            """),_display_(/*78.14*/if(isRangeSelected)/*78.33*/{_display_(Seq[Any](format.raw/*78.34*/(""" 
            """),format.raw/*79.13*/("""<div class="col-sm-3">
        <div class="form-group" >
         <label></label>  
         <button type="submit" class="btn btn-sm btn-primary" id="js-upload-submit1" name="selectedFile" style="margin-top:26px" value="assignCalls">Assign Calls</button>
     
      </div>
      </div>
           """)))}),format.raw/*86.13*/("""
       """),_display_(/*87.9*/if(toAssigncall)/*87.25*/{_display_(Seq[Any](format.raw/*87.26*/("""     
      """),format.raw/*88.7*/("""<div class="col-sm-3">   
      <div class="form-group">
       <label>From Date :</label>    
      <input type="text-primary" class="datepicker form-control" id="singleData" name="singleData" value=""""),_display_(/*91.108*/fromDateValue),format.raw/*91.121*/("""" readonly>
      </div>
      </div>
      """)))}),format.raw/*94.8*/("""
      """),_display_(/*95.8*/if(toAssigncall)/*95.24*/{_display_(Seq[Any](format.raw/*95.25*/("""
      """),format.raw/*96.7*/("""<div class="col-sm-3">   
      <div class="form-group">   
      <label>To Date :</label>    
      <input type="text-primary" class="datepicker form-control" id="tranferData" name="tranferData" value=""""),_display_(/*99.110*/toDateValue),format.raw/*99.121*/("""" readonly>       
     	</div>
      </div>
      """)))}),format.raw/*102.8*/("""
      
	  
	 """),_display_(/*105.4*/if(toAssigncall)/*105.20*/{_display_(Seq[Any](format.raw/*105.21*/(""" 
	  	
      """),format.raw/*107.7*/("""<div class="col-sm-3">   
      <div class="form-group">   
      <label>Customer Category :</label>    
      <select class="form-control" id="cust_category" name="cust_category" value=""""),_display_(/*110.84*/custCat),format.raw/*110.91*/("""">
		<option value="All">All</option>
		<option value="A+">A+</option>
		<option value="B+">B+</option>
	 
      </select>       
     	</div>
      </div>
     """)))}),format.raw/*118.7*/(""" 
	
	  
	  """),_display_(/*121.5*/if(toAssigncall)/*121.21*/{_display_(Seq[Any](format.raw/*121.22*/("""
      """),format.raw/*122.7*/("""<div class="col-sm-3">   
      <div class="form-group">   
      <label>Service Type :</label>    
     <select class="form-control" id="service_type" name="service_type" value=""""),_display_(/*125.81*/serviceType),format.raw/*125.92*/("""">
						<option value="All">All</option>
					    """),_display_(/*127.11*/for(servTyp <- servicetypes) yield /*127.39*/{_display_(Seq[Any](format.raw/*127.40*/("""
					    	
					    	"""),format.raw/*129.11*/("""<option value=""""),_display_(/*129.27*/servTyp/*129.34*/.getServiceTypeName()),format.raw/*129.55*/("""">"""),_display_(/*129.58*/servTyp/*129.65*/.getServiceTypeName()),format.raw/*129.86*/("""</option>
					    	
					    """)))}),format.raw/*131.11*/("""	  
      """),format.raw/*132.7*/("""</select>      
     	</div>
      </div>
         
          
          <div class="col-md-3 locationSelect" >
                                           
                                                  <label for="cityName">Select City</label>
                                                  <select class="form-control" id="city" name="cityName" onchange="ajaxCallToLoadWorkShopByCity();">	
                                                     <option value='select'>--Select City--</option>
                                                       """),_display_(/*142.57*/for(location_list<-locationList) yield /*142.89*/{_display_(Seq[Any](format.raw/*142.90*/("""
                                                     """),format.raw/*143.54*/("""<option value=""""),_display_(/*143.70*/location_list/*143.83*/.getName()),format.raw/*143.93*/("""">"""),_display_(/*143.96*/location_list/*143.109*/.getName()),format.raw/*143.119*/("""</option>
                                                         """)))}),format.raw/*144.59*/("""
                                                  """),format.raw/*145.51*/("""</select>

                                          
                                        </div>
                                        <div class="col-md-3 workshopSel">
                                          
                                                <label for="workshopId">Select Workshop</label>
                                                <select class="form-control" id="workshop" name="workshopId" >					  
					         <option value=""></option>
                                                </select>
                                           		 
                                        </div>
      """)))}),format.raw/*157.8*/(""" 
	  
	   """),_display_(/*159.6*/if(toAssigncall)/*159.22*/{_display_(Seq[Any](format.raw/*159.23*/("""
     	"""),format.raw/*160.7*/("""<div class="col-sm-3">
        <div class="form-group" >
        <button type="submit" id="js-upload-submit" name="selectedFile" class="btn btn-sm btn-primary viewAssignCalls" style="margin-top:26px" value="viewAssignCalls">View Calls</button>
     
      </div>
      </div>
           """)))}),format.raw/*166.13*/("""
	   """),format.raw/*167.5*/("""</div>
	   </div>      
           
           
        </div>
          

<div class="row">
      <div class="col-lg-12">
        <div class="panel panel-success">
          <div class="panel-heading"> List Of  Calls To Assign</div>
          <!-- /.panel-heading -->
          <div class="panel-body">
            <div class="dataTable_wrapper">
              <table class="table table-striped table-bordered table-hover" id="dataTables-example25">
                <thead>
                  <tr>
                  					
                                    <th>Customer Name</th>
                                    <th>Mobile Number</th>                                    
                                    <th>Vehicle RegNo.</th>
                                    <th>Model</th>
                                    <th>Variant</th>
                                    <th>Next Service Date</th>
                                    
                                    
                  </tr>
                </thead>
                <tbody>
                """),_display_(/*196.18*/if(campaignTypeCat==4)/*196.40*/{_display_(Seq[Any](format.raw/*196.41*/("""               	
                  			
                  			"""),_display_(/*198.23*/for(psfData <- psf_assign_data) yield /*198.54*/{_display_(Seq[Any](format.raw/*198.55*/("""
                 				"""),format.raw/*199.22*/("""<tr>                  				
                   				<td>"""),_display_(/*200.29*/psfData/*200.36*/.getCustomer_name()),format.raw/*200.55*/("""</td>
                              	<td>"""),_display_(/*201.37*/psfData/*201.44*/.getMobile_number()),format.raw/*201.63*/("""</td>                              	
                              	<td>"""),_display_(/*202.37*/psfData/*202.44*/.getVehicle_RegNo()),format.raw/*202.63*/("""V</td>
                              	<td>"""),_display_(/*203.37*/psfData/*203.44*/.getVeh_model()),format.raw/*203.59*/("""</td>
                                <td>"""),_display_(/*204.38*/psfData/*204.45*/.getVariant()),format.raw/*204.58*/("""</td>
                                <td></td>
                                
                              	</tr>
                              	""")))}),format.raw/*208.33*/("""
                  """)))}/*209.20*/else/*209.24*/{_display_(Seq[Any](format.raw/*209.25*/("""
                  
                  		
                             """),_display_(/*212.31*/for(post<-filteredData) yield /*212.54*/{_display_(Seq[Any](format.raw/*212.55*/("""
                               """),format.raw/*213.32*/("""<tr >
                  				
                   				<td>"""),_display_(/*215.29*/post/*215.33*/.customer.customerName),format.raw/*215.55*/("""</td>
                              	<td>"""),_display_(/*216.37*/if(post.customer.getPreferredPhone()!= null)/*216.81*/{_display_(Seq[Any](format.raw/*216.82*/("""
                              		"""),_display_(/*217.34*/post/*217.38*/.customer.getPreferredPhone().getPhoneNumber()),format.raw/*217.84*/("""
                              	""")))}),format.raw/*218.33*/("""</td>                              	
                              	<td>"""),_display_(/*219.37*/post/*219.41*/.vehicleRegNo),format.raw/*219.54*/("""</td>
                              	<td>"""),_display_(/*220.37*/post/*220.41*/.model),format.raw/*220.47*/("""</td>
                                <td>"""),_display_(/*221.38*/post/*221.42*/.variant),format.raw/*221.50*/("""</td>
                                <td>"""),_display_(/*222.38*/post/*222.42*/.nextServicedate),format.raw/*222.58*/("""</td>                            	
                             
                                
                              	</tr>
                              	
                              	
                  """)))}),format.raw/*228.20*/("""	
                  
                  """)))}),format.raw/*230.20*/("""
                  
                  
                """),format.raw/*233.17*/("""</tbody>
              </table>
            </div>
          </div>
          <!-- /.panel-body --> 
        </div>
        <!-- /.panel --> 
      </div>
      <!-- /.col-lg-12 --> 
    </div>

""")))}),format.raw/*244.2*/("""
""")))}),format.raw/*245.2*/("""

"""),format.raw/*247.1*/("""<script>
        $(document).ready(function () """),format.raw/*248.39*/("""{"""),format.raw/*248.40*/("""


        	"""),format.raw/*251.10*/("""$('.viewAssignCalls').click(function()"""),format.raw/*251.48*/("""{"""),format.raw/*251.49*/("""
            	
                """),format.raw/*253.17*/("""var city = $("#city").val();
                var workshop = $("#workshop").val();
                var fromDate = $("#singleData").val();
                var toDate = $("#tranferData").val();
               

                if (fromDate != ''  && toDate != '' && city != 'select' && workshop != 'select') """),format.raw/*259.98*/("""{"""),format.raw/*259.99*/("""

                	 """),format.raw/*261.19*/("""$('.locationSelect').css("display","none");
                     $('.workshopSel').css("display","none");
                    
                    return true;
                """),format.raw/*265.17*/("""}"""),format.raw/*265.18*/(""" """),format.raw/*265.19*/("""else """),format.raw/*265.24*/("""{"""),format.raw/*265.25*/("""
                    
                       """),format.raw/*267.24*/("""if (city == 'select' || city == '') """),format.raw/*267.60*/("""{"""),format.raw/*267.61*/("""

                        """),format.raw/*269.25*/("""Lobibox.notify('warning', """),format.raw/*269.51*/("""{"""),format.raw/*269.52*/("""
                            """),format.raw/*270.29*/("""msg: "Please select the city"
                        """),format.raw/*271.25*/("""}"""),format.raw/*271.26*/(""");
                        return false;

                    """),format.raw/*274.21*/("""}"""),format.raw/*274.22*/("""
                    """),format.raw/*275.21*/("""if (workshop == 'select' || workshop == '') """),format.raw/*275.65*/("""{"""),format.raw/*275.66*/("""

                        """),format.raw/*277.25*/("""Lobibox.notify('warning', """),format.raw/*277.51*/("""{"""),format.raw/*277.52*/("""
                            """),format.raw/*278.29*/("""msg: "Please select the workshop"
                        """),format.raw/*279.25*/("""}"""),format.raw/*279.26*/(""");
                        return false;

                    """),format.raw/*282.21*/("""}"""),format.raw/*282.22*/("""

                    """),format.raw/*284.21*/("""if (fromDate == '') """),format.raw/*284.41*/("""{"""),format.raw/*284.42*/("""

                        """),format.raw/*286.25*/("""Lobibox.notify('warning', """),format.raw/*286.51*/("""{"""),format.raw/*286.52*/("""
                            """),format.raw/*287.29*/("""msg: "Please enter the start date"
                        """),format.raw/*288.25*/("""}"""),format.raw/*288.26*/(""");
                        return false;

                    """),format.raw/*291.21*/("""}"""),format.raw/*291.22*/("""
                    """),format.raw/*292.21*/("""if (toDate == '') """),format.raw/*292.39*/("""{"""),format.raw/*292.40*/("""

                        """),format.raw/*294.25*/("""Lobibox.notify('warning', """),format.raw/*294.51*/("""{"""),format.raw/*294.52*/("""
                            """),format.raw/*295.29*/("""msg: "Please enter the End date"
                        """),format.raw/*296.25*/("""}"""),format.raw/*296.26*/(""");
                        return false;

                    """),format.raw/*299.21*/("""}"""),format.raw/*299.22*/("""                    
                 
                """),format.raw/*301.17*/("""}"""),format.raw/*301.18*/("""
            """),format.raw/*302.13*/("""}"""),format.raw/*302.14*/(""");

        """),format.raw/*304.9*/("""}"""),format.raw/*304.10*/(""");

    </script>
"""))
      }
    }
  }

  def render(campaignNamePSF:Long,campaignNameCat:Long,campaignTypeCat:Long,custCat:String,serviceType:String,fromDateValue:String,toDateValue:String,isRangeSelected:Boolean,toAssigncall:Boolean,allCRE:List[String],filteredData:List[Vehicle],dealerName:String,user:String,campaign_list:List[Campaign],locationList:List[Location],campaignList:List[Campaign],campaignListPSF:List[Campaign],psf_assign_data:List[webmodels.AssignedListForCRE],servicetypes:List[ServiceTypes]): play.twirl.api.HtmlFormat.Appendable = apply(campaignNamePSF,campaignNameCat,campaignTypeCat,custCat,serviceType,fromDateValue,toDateValue,isRangeSelected,toAssigncall,allCRE,filteredData,dealerName,user,campaign_list,locationList,campaignList,campaignListPSF,psf_assign_data,servicetypes)

  def f:((Long,Long,Long,String,String,String,String,Boolean,Boolean,List[String],List[Vehicle],String,String,List[Campaign],List[Location],List[Campaign],List[Campaign],List[webmodels.AssignedListForCRE],List[ServiceTypes]) => play.twirl.api.HtmlFormat.Appendable) = (campaignNamePSF,campaignNameCat,campaignTypeCat,custCat,serviceType,fromDateValue,toDateValue,isRangeSelected,toAssigncall,allCRE,filteredData,dealerName,user,campaign_list,locationList,campaignList,campaignListPSF,psf_assign_data,servicetypes) => apply(campaignNamePSF,campaignNameCat,campaignTypeCat,custCat,serviceType,fromDateValue,toDateValue,isRangeSelected,toAssigncall,allCRE,filteredData,dealerName,user,campaign_list,locationList,campaignList,campaignListPSF,psf_assign_data,servicetypes)

  def ref: this.type = this

}


}

/**/
object assignCallBasedOnSelectedRange extends assignCallBasedOnSelectedRange_Scope0.assignCallBasedOnSelectedRange
              /*
                  -- GENERATED --
                  DATE: Thu Dec 28 12:24:25 IST 2017
                  SOURCE: D:/CRMNEW/28th Dec/crm/app/views/assignCallBasedOnSelectedRange.scala.html
                  HASH: 73c3f31d0748734a5494fa8525d138333242deed
                  MATRIX: 1003->5|1558->464|1588->469|1646->519|1685->521|1715->526|1728->532|1844->639|1883->640|1913->644|1989->694|2022->707|2100->759|2131->770|2215->827|2243->834|2327->891|2359->902|2444->960|2480->975|2565->1033|2601->1048|2685->1105|2721->1120|2982->1354|3007->1370|3046->1371|3086->1384|3362->1633|3409->1664|3448->1665|3479->1669|3522->1685|3540->1694|3564->1697|3594->1700|3612->1709|3646->1722|3692->1738|3727->1746|3803->1792|3868->1829|4143->2076|4180->2091|4329->2213|4377->2245|4416->2246|4478->2280|4521->2296|4538->2304|4562->2307|4592->2310|4609->2318|4643->2331|4729->2386|4791->2420|5188->2789|5225->2804|5377->2929|5423->2959|5462->2960|5524->2994|5567->3010|5585->3019|5609->3022|5639->3025|5657->3034|5691->3047|5777->3102|5920->3217|6182->3452|6210->3471|6249->3472|6291->3486|6548->3716|6584->3736|6623->3737|6747->3833|6790->3849|6816->3854|6846->3857|6872->3862|6966->3925|7047->3978|7144->4044|7186->4059|7214->4078|7253->4079|7296->4094|7633->4400|7669->4410|7694->4426|7733->4427|7773->4440|8006->4645|8041->4658|8119->4706|8154->4715|8179->4731|8218->4732|8253->4740|8488->4947|8521->4958|8607->5013|8652->5031|8678->5047|8718->5048|8761->5063|8980->5254|9009->5261|9210->5431|9252->5446|9278->5462|9318->5463|9354->5471|9565->5654|9598->5665|9680->5719|9725->5747|9765->5748|9818->5772|9862->5788|9879->5795|9922->5816|9953->5819|9970->5826|10013->5847|10078->5880|10117->5891|10710->6456|10759->6488|10799->6489|10883->6544|10927->6560|10950->6573|10982->6583|11013->6586|11037->6599|11070->6609|11171->6678|11252->6730|11928->7375|11968->7388|11994->7404|12034->7405|12070->7413|12396->7707|12430->7713|13554->8809|13586->8831|13626->8832|13717->8895|13765->8926|13805->8927|13857->8950|13941->9006|13958->9013|13999->9032|14070->9075|14087->9082|14128->9101|14230->9175|14247->9182|14288->9201|14360->9245|14377->9252|14414->9267|14486->9311|14503->9318|14538->9331|14724->9485|14765->9506|14779->9510|14819->9511|14921->9585|14961->9608|15001->9609|15063->9642|15150->9701|15164->9705|15208->9727|15279->9770|15333->9814|15373->9815|15436->9850|15450->9854|15518->9900|15584->9934|15686->10008|15700->10012|15735->10025|15806->10068|15820->10072|15848->10078|15920->10122|15934->10126|15964->10134|16036->10178|16050->10182|16088->10198|16344->10422|16418->10464|16505->10522|16743->10729|16777->10732|16809->10736|16886->10784|16916->10785|16960->10800|17027->10838|17057->10839|17119->10872|17459->11183|17489->11184|17540->11206|17749->11386|17779->11387|17809->11388|17843->11393|17873->11394|17949->11441|18014->11477|18044->11478|18101->11506|18156->11532|18186->11533|18245->11563|18329->11618|18359->11619|18453->11684|18483->11685|18534->11707|18607->11751|18637->11752|18694->11780|18749->11806|18779->11807|18838->11837|18926->11896|18956->11897|19050->11962|19080->11963|19133->11987|19182->12007|19212->12008|19269->12036|19324->12062|19354->12063|19413->12093|19502->12153|19532->12154|19626->12219|19656->12220|19707->12242|19754->12260|19784->12261|19841->12289|19896->12315|19926->12316|19985->12346|20072->12404|20102->12405|20196->12470|20226->12471|20312->12528|20342->12529|20385->12543|20415->12544|20457->12558|20487->12559
                  LINES: 27->2|32->2|34->4|34->4|34->4|36->6|36->6|36->6|36->6|38->8|38->8|38->8|39->9|39->9|40->10|40->10|41->11|41->11|42->12|42->12|43->13|43->13|44->14|44->14|52->22|52->22|52->22|53->23|58->28|58->28|58->28|59->29|59->29|59->29|59->29|59->29|59->29|59->29|60->30|61->31|64->34|66->36|68->38|68->38|70->40|70->40|70->40|71->41|71->41|71->41|71->41|71->41|71->41|71->41|72->42|73->43|78->48|78->48|80->50|80->50|80->50|81->51|81->51|81->51|81->51|81->51|81->51|81->51|82->52|85->55|94->64|94->64|94->64|95->65|98->68|98->68|98->68|100->70|100->70|100->70|100->70|100->70|102->72|104->74|107->77|108->78|108->78|108->78|109->79|116->86|117->87|117->87|117->87|118->88|121->91|121->91|124->94|125->95|125->95|125->95|126->96|129->99|129->99|132->102|135->105|135->105|135->105|137->107|140->110|140->110|148->118|151->121|151->121|151->121|152->122|155->125|155->125|157->127|157->127|157->127|159->129|159->129|159->129|159->129|159->129|159->129|159->129|161->131|162->132|172->142|172->142|172->142|173->143|173->143|173->143|173->143|173->143|173->143|173->143|174->144|175->145|187->157|189->159|189->159|189->159|190->160|196->166|197->167|226->196|226->196|226->196|228->198|228->198|228->198|229->199|230->200|230->200|230->200|231->201|231->201|231->201|232->202|232->202|232->202|233->203|233->203|233->203|234->204|234->204|234->204|238->208|239->209|239->209|239->209|242->212|242->212|242->212|243->213|245->215|245->215|245->215|246->216|246->216|246->216|247->217|247->217|247->217|248->218|249->219|249->219|249->219|250->220|250->220|250->220|251->221|251->221|251->221|252->222|252->222|252->222|258->228|260->230|263->233|274->244|275->245|277->247|278->248|278->248|281->251|281->251|281->251|283->253|289->259|289->259|291->261|295->265|295->265|295->265|295->265|295->265|297->267|297->267|297->267|299->269|299->269|299->269|300->270|301->271|301->271|304->274|304->274|305->275|305->275|305->275|307->277|307->277|307->277|308->278|309->279|309->279|312->282|312->282|314->284|314->284|314->284|316->286|316->286|316->286|317->287|318->288|318->288|321->291|321->291|322->292|322->292|322->292|324->294|324->294|324->294|325->295|326->296|326->296|329->299|329->299|331->301|331->301|332->302|332->302|334->304|334->304
                  -- GENERATED --
              */
          