
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object dealerRegistration_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class dealerRegistration extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template3[String,String,play.data.Form[Dealer],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(dealerName:String,userName:String,form: play.data.Form[Dealer]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.66*/("""
"""),_display_(/*2.2*/main("AutoSherpaCRM",userName,dealerName)/*2.43*/ {_display_(Seq[Any](format.raw/*2.45*/("""
"""),_display_(/*3.2*/helper/*3.8*/.form(action = routes.DealerController.adddealerForm ,'id -> "myFormValidation")/*3.88*/{_display_(Seq[Any](format.raw/*3.89*/("""
   
"""),format.raw/*5.1*/("""<div class="fluid-container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="panel-title">Dealer Registration </h3>
        </div>
        <div class="panel-body">
          
            <div class='row'>
              <div class='col-sm-6'>
                <div class='form-group'>
                <label for="dealerId">Dealer Id :</label>   
                 <div class="input-group" data-validate="userName">       		
         		<input type="text" class="form-control" name="dealerId" placeholder="Enter Dealer Id ..." title="DealerId is required" required>   
         		<span class="input-group-addon danger"><span class="glyphicon glyphicon-remove"></span></span> </div>        		
                 </div>
                 <div class='form-group'>
                <label for="dealerName">Dealer Name :</label>  
                 <div class="input-group" data-validate="userName">        		
         		<input type="text" class="form-control" name="dealerName" placeholder="Enter Dealer Name..." title="DealerName is required" required>  
         		<span class="input-group-addon danger"><span class="glyphicon glyphicon-remove"></span></span> </div>         		
                 </div>                
              </div>
              <div class='col-sm-6'>
              <div class='form-group'>
                <label for="dealerAddress">Dealer Address :</label> 
                 <div class="input-group" data-validate="userName">         		
         		<input type="text" class="form-control" name="dealerAddress" placeholder="Enter Dealer Address..." title="Dealer Address is required" required> 
         		<span class="input-group-addon danger"><span class="glyphicon glyphicon-remove"></span></span> </div>          		
                 </div> 
                 <div class='form-group'>
                <label for="dealerCode">Dealer Code :</label> 
                 <div class="input-group" data-validate="userName">         		
         		<input type="text" class="form-control" name="dealerCode" placeholder="Enter Dealer Code..." title="Dealer Code is required" required>   
         		<span class="input-group-addon danger"><span class="glyphicon glyphicon-remove"></span></span> </div>        		
                 </div>
                <div class='text-right'>
                  <input type='submit' class='btn btn-primary' value='Submit'/>
                </div>
              </div>
            </div>
         
        </div>
      </div>
    </div>
  </div>
</div>
    """)))}),format.raw/*53.6*/("""
   

""")))}),format.raw/*56.2*/("""

"""))
      }
    }
  }

  def render(dealerName:String,userName:String,form:play.data.Form[Dealer]): play.twirl.api.HtmlFormat.Appendable = apply(dealerName,userName,form)

  def f:((String,String,play.data.Form[Dealer]) => play.twirl.api.HtmlFormat.Appendable) = (dealerName,userName,form) => apply(dealerName,userName,form)

  def ref: this.type = this

}


}

/**/
object dealerRegistration extends dealerRegistration_Scope0.dealerRegistration
              /*
                  -- GENERATED --
                  DATE: Thu Dec 28 12:24:28 IST 2017
                  SOURCE: D:/CRMNEW/28th Dec/crm/app/views/dealerRegistration.scala.html
                  HASH: 2804207db1413cf1d5077888f1070ec460cbccf1
                  MATRIX: 801->1|960->65|988->68|1037->109|1076->111|1104->114|1117->120|1205->200|1243->201|1276->208|3943->2845|3983->2855
                  LINES: 27->1|32->1|33->2|33->2|33->2|34->3|34->3|34->3|34->3|36->5|84->53|87->56
                  -- GENERATED --
              */
          