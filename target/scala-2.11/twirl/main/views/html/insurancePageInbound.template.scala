
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object insurancePageInbound_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class insurancePageInbound extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template6[List[InsuranceAgent],Customer,Vehicle,List[Location],CallInteraction,WyzUser,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(insurAgent:List[InsuranceAgent],customerData:Customer,vehicleData:Vehicle,locationList:List[Location],interOfCall:CallInteraction,userData:WyzUser):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.150*/("""
 """),format.raw/*2.2*/("""<div class="col-md-12" id="InBoundDiv" style="display:none;">
			<div class="row animated  bounceInRight" id="LeadSourceHideIn">
			<div class="col-md-3">
			  <div class="form-group">
				<label for=""><b>Lead Source</b></label>
				<select class="form-control" name="inBoundLeadSource" id="inBoundLeadSourceSelectVal" >
					<option value="0">Select</option>
				  <option value="Phone">Phone</option>
				  <option value="Walk In">Walk In</option>
				  <option value="Website">Website</option>
				  <option value="E-mail">E-mail</option>
				  <option value="Reference">Reference</option>
				  <option value="Inbound Call">Inbound Call</option>
				  <option value="Body Shop">Body Shop</option>
				  <option value="Others">Others</option>
				</select>
			  </div>
			</div>		
			
			
		</div>
		<div class="col-md-3 animated  bounceInRight BookMyServiceIn" id="BookMyServiceIn">
			  <div class="form-group">
				
			<div class="checkbox-inline" >
			<label>
			  <input type="checkbox" name="contactType" class="InCallServiceBook" value="Book Appointment" id="InCallServiceBook">
			  Book Appointment</label>
		  </div>
	
			  </div>
		</div>
	<div style="display:none;" class="animated  bounceInRight" id="InCallserviceBookDiv">
		  <div class="col-md-12">
			  <div class="row">
				<div class="col-md-3">
				  <div class="form-group">
					<label for="">Select Vehicle</label>
					<select class="form-control" id="vehicleIn" name="vehicleIn">
					  """),_display_(/*41.9*/for(post <- customerData.getVehicles()) yield /*41.48*/{_display_(Seq[Any](format.raw/*41.49*/("""
                              
                              """),format.raw/*43.31*/("""<option value=""""),_display_(/*43.47*/post/*43.51*/.vehicle_id),format.raw/*43.62*/("""">"""),_display_(/*43.65*/post/*43.69*/.vehicleRegNo),format.raw/*43.82*/("""</option>
                              
					""")))}),format.raw/*45.7*/("""
					  
							
						
					"""),format.raw/*49.6*/("""</select>
				  </div>
				  </div>
				  <div class="col-md-3">
				  <div class="form-group">
					<label for="">Select City</label>
					<select class="form-control" id="cityIn" name="cityIn">
					 
					  <option value=""""),_display_(/*57.24*/{userData.getLocation().getName()}),format.raw/*57.58*/("""">"""),_display_(/*57.61*/{userData.getLocation().getName()}),format.raw/*57.95*/("""</option>
					  
					</select>
				  </div>
				</div>
						
				<div class="col-md-3">
				  <label for="">Renewal Type</label>
				  <select class="form-control" id="renewalTypeInID" name="renewalTypeIn" onchange="">
						<option value="0">--Select--</option>
						<option value="1st Renewal">1st Renewal</option>
						<option value="2nd Renewal">2nd Renewal</option>
						<option value="3rd Renewal">3rd Renewal</option>
						<option value="4th Renewal">4th Renewal</option>
						<option value="5th Renewal">5th Renewal</option>
						<option value="6th Renewal">6th Renewal</option>
						<option value="7th Renewal">7th Renewal</option>
						
						
						

				 </select>
				</div>
				<div class="col-md-3">
				  <label for="">Renewal Mode</label>
				  <select class="form-control" name="renewalModeIn" id="RenewalModeIDS">
				  <option value="0">--Select--</option>
				  <option value="PI">PI</option>
				  <option value="NPI">NPI</option>
				 </select>
				</div>
				</div>
			 </div>
				
		   <div class="col-md-12">
			  <div class="row">
			  <div class="col-md-3">
				  <div class="form-group">
					<label class="control-label" for="inputGroup">Appointment Date</label>
					
					  <input type="text" name="appointmentScheduledDateIn" class="form-control SelectDateinsura" id="date123456" onChange="ajaxAutoSASelectionListInsurance('date123456','serviceAdvisorIdIn');" readonly />
					  
				  </div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
					  <label for="followUpDate">Appointment Time</label>
					  <input type="text" name="appointmentFromTimeIn" class="timePickRange7to19 form-control" id="time_FromDriverIn" readonly>
					</div>
				  </div>
			
		  <div class="col-md-3">
				  <div class="form-group">
				  <label>&nbsp;</label><br/>
					<button type="button" class="btn btn-primary btn-block" onClick="return ajaxAutoSASelectionIn('date123456','serviceAdvisorIdIn')">Recommend Agent</button>
					  
				  </div>
				</div>
				<div class="col-md-3">
				  <div class="form-group">
					<label for="">Assigned Insurance Agent</label>
					<select class="form-control" id="serviceAdvisorIdIn" onChange="ajaxAutoSAManualchangeIns('date123456','serviceAdvisorIdIn');" name="insuranceAgentDataIn">
					 
						
					  </select>
				  </div>
				</div>
			  </div>
			
			
			<div class="pull-right">
				<button type="button" class="btn btn-primary" id="BackToCustomerMainInBound">Back</button>
				<button type="button" class="btn btn-primary" id="nextToCustomerDriveIn">Next</button>
				
			</div>
			</div>
			</div>
			<div style="display:none;" class="animated  bounceInRight" id="HomeAdnShowrromVisit">
			<div class="col-md-12">
			<label>Choose one of the below option:</label>
			<br />	
		<div class="radio-inline">
		  <label>
			<input type="radio" name="typeOfPickupIn" value="Home Visit" id="homeVisitIdIn" >
			Home Visit</label>
		</div>
		
	  <div class="radio-inline">
		  <label>
			<input type="radio" name="typeOfPickupIn" value="Showroom Visit" id="showroomVisitIdIn" onclick="ajaxAddShowRoomIn();">
			Showroom Visit</label>
		</div>
		<div class="animated  bounceInRight" id="MSSSelectDivIn" style="display:none;">
				
			<div class="col-md-3">
				   <label for="">List Of Showrooms:</label>
					<div class="form-group">
					 
					  <select class="form-control" id="ShowroomsSelectIdInB" name="showRoom_idIn">
						<option value="0">--Select--</option>
						
					  </select>
					</div>
				  </div>
				 <!--    <div class="col-md-3">
					<div class="form-group">
					  <label for="followUpDate">Appointment From</label>
					  <input type="text" name="appointmentFromTimeIn" class="timePickRange7to19 form-control" id="time_FromDriverIn" readonly>
					</div>
				  </div>
				  <div class="col-md-3">
					<div class="form-group">
					  <label for="followUpTime">Appointment To:</label>
					  <input type="text" name="appointmentToTimeIn" class="timePickRange7to19 form-control" id="time_ToDriverIn" readonly>
					</div>
				  </div> -->
				  <div class="col-md-3">
					<div class="form-group"><br />
					 <button type="button" class="btn btn-success" data-toggle="modal" data-target="#AddNewAddressPopup" style="margin-top: 4px;">Add New Address</button>
					</div>
				  </div>
				 
				  
				  </div>
	<div style="display:none;" id="pickupDivIn">
		  <div class="animated  bounceInRight">
			<div class="col-md-3">
			  <div class="form-group">
				<label for="pickUpAddress"><b>Confirm Address</b></label>
				<select class="form-control" name="pickUpAddressIn" id="pickUpAddressIn" >
				  """),_display_(/*187.8*/for(addre_list <- customerData.getAddresses()) yield /*187.54*/{_display_(Seq[Any](format.raw/*187.55*/("""
                          """),format.raw/*188.27*/("""<option value=""""),_display_(/*188.43*/addre_list/*188.53*/.getAddressLine1()),format.raw/*188.71*/(""","""),_display_(/*188.73*/addre_list/*188.83*/.getAddressLine2()),format.raw/*188.101*/(""","""),_display_(/*188.103*/addre_list/*188.113*/.getAddressLine3()),format.raw/*188.131*/(""","""),_display_(/*188.133*/addre_list/*188.143*/.getCity()),format.raw/*188.153*/(""","""),_display_(/*188.155*/addre_list/*188.165*/.getPincode()),format.raw/*188.178*/("""">"""),_display_(/*188.181*/addre_list/*188.191*/.getAddressLine1()),format.raw/*188.209*/(""","""),_display_(/*188.211*/addre_list/*188.221*/.getAddressLine2()),format.raw/*188.239*/(""","""),_display_(/*188.241*/addre_list/*188.251*/.getAddressLine3()),format.raw/*188.269*/(""","""),_display_(/*188.271*/addre_list/*188.281*/.getCity()),format.raw/*188.291*/(""","""),_display_(/*188.293*/addre_list/*188.303*/.getPincode()),format.raw/*188.316*/("""</option>
                         
						  """)))}),format.raw/*190.10*/("""
				 
				"""),format.raw/*192.5*/("""</select>
			  </div>
			</div>
						  
			<!--    <div class="col-md-3">
					<div class="form-group">
					  <label for="followUpDate">Appointment From</label>
					  <input type="text" name="appointmentFromTimeHomevisitIn" class="timePickRange7to19 form-control" id="time_FromDriverInHM" readonly>
					</div>
				  </div>
				  <div class="col-md-3">
					<div class="form-group">
					  <label for="followUpTime">Appointment To:</label>
					  <input type="text" name="appointmentToTimeHomeVisitIn" class="timePickRange7to19 form-control" id="time_ToDriverInHM" readonly>
					</div>
				  </div> -->
				  <div class="col-md-3">
					<div class="form-group"><br />
					   <button type="button" class="btn btn-success" data-toggle="modal" data-target="#AddNewAddressPopup">Add New Address</button>
					</div>
				  </div>
			   
		  </div>
		  
		
		 
	  </div>
	  <div class="pull-right">
	  <button type="button" class="btn btn-primary" id="BackToinsuhmVisit">Back</button>
	   <button type="button" class="btn btn-primary" id="NextInsurnceAddOns1">Next</button>
	  </div>
	</div>
		  </div>
		  <!--end-->
		
		 
		<div style="display:none;" class="animated  bounceInRight" id="Add_OnsIndurace1InB">
		
		 <label>Any Add-Ons covers prefered by customer? (Details will be shared by the Insurance Executive) </label>
              <br>
			  <div class="radio-inline">
		  <label>
			<input type="radio" name="AddOnsYesInB" value="Yes" id="AddOnsYesRIdInB" >
			Yes</label>
		</div>
		<div class="radio-inline">
		  <label>
			<input type="radio" name="AddOnsYesInB" value="No" id="AddOnsNoRIdInB" >
			No</label>
		</div>
		
		<div style="display:none;" id="AllAdd_onsDivInB" class="animated  bounceInRight">
		<div class="row">
              <div class="col-md-12">
              <p><b>Popular Options :</b></p>
           <div class="col-xs-3">
           <div class="form-group">
           <label class="checkbox-inline">
      <input type="checkbox" value="ComprehensivePolicy" name="addOnsPrefered_PopularOptions[9]" class="Add_OnschkInB">Comprehensive Policy
    </label>
           </div>
           </div>
            <div class="col-xs-3">
            <div class="form-group">
           <label class="checkbox-inline">
      <input type="checkbox" value="NCB Protect Cover" name="addOnsPrefered_PopularOptions[10]" class="Add_OnschkInB">NCB Protect Cover
    </label>
    </div>
           </div>
           <div class="col-xs-3">
           <div class="form-group">
           <label class="checkbox-inline">
      <input type="checkbox" value="Zero Dep Cover" name="addOnsPrefered_PopularOptions[11]" class="Add_OnschkInB">Zero Dep Cover
    </label>
    </div>
           </div>
           <div class="col-xs-3">
           <div class="form-group">
           <label class="checkbox-inline">
      <input type="checkbox" value="Invoice Cover" name="addOnsPrefered_PopularOptions[12]" class="Add_OnschkInB">Invoice Cover
    </label>
           </div>
           </div>
           <div class="col-xs-3">
           <div class="form-group">
           <label class="checkbox-inline">
      <input type="checkbox" value="Engine Protect" name="addOnsPrefered_PopularOptions[13]" class="Add_OnschkInB">Engine Protect
    </label>
           </div>
           </div>
           <div class="col-xs-3">
           <div class="form-group">
           <label class="checkbox-inline">
      <input type="checkbox" value=" Electronic Circuit Cover" name="addOnsPrefered_PopularOptions[14]" class="Add_OnschkInB"> Electronic Circuit Cover
    </label>
           </div>
           </div>
          
           <div class="col-xs-3">
           <div class="form-group">
           <label class="checkbox-inline">
      <input type="checkbox" value="Passenger Cover" name="addOnsPrefered_PopularOptions[15]" class="Add_OnschkInB">Passenger Cover
    </label>
           </div>
           </div>
           <div class="col-xs-3">
           <div class="form-group">
           <label class="checkbox-inline">
      <input type="checkbox" value="Quick Assistance Cover" name="addOnsPrefered_PopularOptions[16]" class="Add_OnschkInB">Quick Assistance Cover
    </label>
           </div>
           </div>
           <div class="col-xs-3">
           <div class="form-group">
           <label class="checkbox-inline">
      <input type="checkbox" value="Voluntary Deductible Cover" name="addOnsPrefered_PopularOptions[17]" class="Add_OnschkInB">Voluntary Deductible Cover
    </label>
           </div>
           </div>
           <br />

              </div>
            </div>
		<div class="row">
              <div class="col-md-12">

              <p><b>Other Options:</b></p>
           <div class="col-xs-3">
           <div class="form-group">
           <label class="checkbox-inline">
      <input type="checkbox" value="Loss Of Personal Belongings"  name="addOnsPrefered_OtherOptions[8]" class="Add_OnschkInB">Loss Of Personal Belongings
    </label>
           </div>
           </div>
           <div class="col-xs-3">
           <div class="form-group">
           <label class="checkbox-inline">
      <input type="checkbox" value="Windshield Glass Cover" name="addOnsPrefered_OtherOptions[9]" class="Add_OnschkInB">Windshield Glass Cover
    </label>
           </div>
           </div>
           <div class="col-xs-3">
           <div class="form-group">
           <label class="checkbox-inline">
      <input type="checkbox" value="Consumable Expenses" name="addOnsPrefered_OtherOptions[10]" class="Add_OnschkInB">Consumable Expenses
    </label>
           </div>
           </div>
           <div class="col-xs-3">
           <div class="form-group">
           <label class="checkbox-inline">
      <input type="checkbox" value="Hospital Daily Cash Allowance" name="addOnsPrefered_OtherOptions[11]" class="Add_OnschkInB">Hospital Daily Cash Allowance
    </label>
           </div>
           </div>
           <div class="col-xs-3">
           <div class="form-group">
           <label class="checkbox-inline">
      <input type="checkbox" value="Key Protector Cover" name="addOnsPrefered_OtherOptions[12]" class="Add_OnschkInB">Key Protector Cover
    </label>
           </div>
           </div>
           <div class="col-xs-3">
           <div class="form-group">
           <label class="checkbox-inline">
      <input type="checkbox" value="Consumable Expenses" name="addOnsPrefered_OtherOptions[13]" class="Add_OnschkInB">Consumable Expenses
    </label>
           </div>
           </div>
           <div class="col-xs-3">
           <div class="form-group">
           <label class="checkbox-inline">
      <input type="checkbox" value="Additional Expense Coverage" name="addOnsPrefered_OtherOptions[14]" class="Add_OnschkInB">Additional Expense Coverage 
    </label>
           </div>
           </div>
            <div class="col-xs-3">
           <div class="form-group">
           <label class="checkbox-inline">
      <input type="checkbox" value="Loss of DL / RC" name="addOnsPrefered_OtherOptions[15]" class="Add_OnschkInB">Loss of DL / RC
    </label>
           </div>
           </div>



           </div>
           </div>
		</div>
				
	  
	 
	  <div class="pull-right">
	  <button type="button" class="btn btn-primary" id="BackToNewInsu1InB">Back</button>
	   <button type="button" class="btn btn-primary" id="NextToNewInsu1InB">Next</button>
	  </div>
	</div>
		  
		   <div style="display:none;" class="animated  bounceInRight" id="PremiumInsu2InB">
			<div class="col-md-12">
			<label>Have you shared the tentative 'Insurance Premium' with customer ?:</label>
		<br /> 
		<div class="radio-inline">
		  <label>
			<input type="radio" name="PremiumYesInB" value="Yes" id="PremiumYesIDInB" >
			Yes</label>
		</div>
		<div class="radio-inline">
	  
		  <label>
			<input type="radio" name="PremiumYesInB" value="No" id="PremiumNoIDInB" >
			No</label>
	</div>
			
 <div  style="display:none;" id="InsurancePremiumDivInB">
        <div class="row">
      <div class="col-xs-3">
       <div class="form-group">
        <label for="InsuranceCompany">Insurance Company</label>
          <select class="form-control input-sm" name="insuranceCompanyIn" id="InsuCompSelectIDInB">
          <option value="0">--Select--</option>
           <option value="Agriculture Insurance Company of India">Agriculture Insurance Company of India</option>
			<option value="Apollo Munich Insurance">Apollo Munich Insurance</option>
			<option value="Bajaj Allianz General Insurance">Bajaj Allianz General Insurance</option>
			<option value="Bharti AXA General Insurance">Bharti AXA General Insurance</option>
			<option value="Cholamandalam MS General Insurance">Cholamandalam MS General Insurance</option>
			<option value="Cigna TTK Health Insurance Company Ltd - Cigna TTK">Cigna TTK Health Insurance Company Ltd - Cigna TTK</option>
			<option value="ECGC (Export Credit Guarantee Corporation of India)">ECGC (Export Credit Guarantee Corporation of India)</option>
			<option value="Future Generali India Insurance Company Ltd">Future Generali India Insurance Company Ltd</option>
			<option value="HDFC ERGO General Insurance">HDFC ERGO General Insurance</option>
			<option value="ICICI Lombard General Insurance Company Limited">ICICI Lombard General Insurance Company Limited</option>
			<option value="IFFCO Tokio General Insurance Company Limited">IFFCO Tokio General Insurance Company Limited</option>
			<option value="L&T General Insurance">L&T General Insurance</option>
			<option value="Liberty Videocon General Insurance">Liberty Videocon General Insurance</option>
			<option value="Magma HDI General Insurance">Magma HDI General Insurance</option>
			<option value="Max Bupa General Insurance">Max Bupa General Insurance</option>
			<option value="National Insurance Company Limited">National Insurance Company Limited</option>
			<option value="New India Assurance">New India Assurance</option>
			<option value="Oriental Insurance Company">Oriental Insurance Company</option>
			<option value="Raheja QBE General Insurance Company Ltd - Raheja QBE">Raheja QBE General Insurance Company Ltd - Raheja QBE</option>
			<option value="Reliance General Insurance">Reliance General Insurance</option>
			<option value="Religare Health Insurance">Religare Health Insurance</option>
			<option value="Royal Sundaram General Insurance Co. Limited">Royal Sundaram General Insurance Co. Limited</option>
			<option value="SBI General Insurance Company Ltd">SBI General Insurance Company Ltd</option>
			<option value="Shriram General Insurance Company Ltd">Shriram General Insurance Company Ltd</option>
			<option value="Star Health and Allied Insurance">Star Health and Allied Insurance</option>
			<option value="Tata AIG General Insurance">Tata AIG General Insurance</option>
			<option value="United India Insurance">United India Insurance</option>
			<option value="Universal Sompo General Insurance">Universal Sompo General Insurance</option>
			<option value="Others">Others</option>
        </select>
        </div>
      </div>
      <div class="col-xs-3">
       <div class="form-group">
        <label for="InsuDSAID">DSA (if any)</label>
        <input class="form-control input-sm"  name="dsaIn" id="InsuDSAIDInB" type="text">
      </div>
      </div>
      
    </div>	
	 <div class="row">
	  <div class="col-md-3">
                <div class="form-group">
                  <label>Premium (with Tax)</label>
                  <div class="input-group">
                    <input type="text" class="form-control numberOnly" value="0" name="premiumwithTaxIn" id="PremiumwithTaxInB" readonly>
                   <span class="input-group-addon" id="editFinalPremiumInB" style="cursor:pointer" data-toggle="tooltip" title="Reset"><i class="fa fa-pencil"></i> </span> </div> 
                </div>
              </div>
			  
     
	  <div class="col-xs-3">
       <div class="form-group">
       <br />
        <button type="button" class="btn btn-primary" id="" data-target="#InsuPremiumPopup" data-toggle="modal" data-backdrop="static" data-keyboard="false" onclick="ajaxODPercentage();"><i class="fa fa-calculator fa-lg"></i>&nbsp;Calculate Premium</button>
      </div>
      </div>
     
    </div>
	 </div>
	 
	  <div class="pull-right">
	  <button type="button" class="btn btn-primary" id="BackToNewInsu2InB">Back</button>
	   <button type="button" class="btn btn-primary" id="NextToNewInsu2InB">Next</button>
	  </div>
	
		  </div>
		  </div>
		   <div style="display:none;" class="animated  bounceInRight" id="nomineeDetails3InB">
			<div class="col-md-12">
			<label>Customer's nominee details available ?:</label>
		<br /> 
		<div class="radio-inline">
		  <label>
			<input type="radio" name="nomineeYesInB" value="Yes" id="nomineeYesIDInB" >
			Yes</label>
		</div>
		<div class="radio-inline">
		  <label>
			<input type="radio" name="nomineeYesInB" value="No" id="nomineeNoIDInB" >
			No</label>
		</div>
		<div class="row" style="display:none;" id="nomineeDivInB">
			<div class="col-md-3">
				<div class="form-group">
					<label>Nominee Name</label>
					<input type="text" class="form-control textOnlyAccepted" name="nomineeNameIn" id="NomineeNameIDInB" />
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label>Nominee Age</label>
					<input type="text" class="form-control numberOnly" name="nomineeAgeIn" maxlength="2" id="NomineeAgeIDInB"  />
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label>Nominee's Relation with Owner</label>
					<input type="text" class="form-control textOnlyAccepted" name="nomineeRelationWithOwnerIn" id="NomineeRelationIDInB" />
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label>Appointee Name</label>
					<input type="text" class="form-control textOnlyAccepted" name="appointeeNameIn" id="AppointeeNameIDInB"  />
				</div>
			</div>
		</div>
		
				
	  
	 
	  <div class="pull-right">
	 <button type="button" class="btn btn-primary" id="BackToNewInsu3InB">Back</button>
	   <button type="button" class="btn btn-primary" id="NextToNewInsu3InB">Next</button> 
	    <!-- <button type="button" class="btn btn-primary" id="BackToCunstomerDriveIn">Back</button>
	   <button type="button" class="btn btn-primary" id="NextToLeadInbound">Next</button> -->
	  </div>
	</div>
		  </div>
	  			   
<!------------inBound Upsel Opportunity------------------>
		 <div class="col-md-12 animated  bounceInRight" style="display:none;" id="finalDiv1Inbound">
			<label>Capture Lead : Is there an Upsell Opportunity.</label>
		   <br>
		   
			<div class="radio-inline">
			  <label>
				<input type="radio" name="LeadYesIn" onclick="loadLeadBasedOnLocation();" value="Capture Lead YesIn" id="LeadYesIDIn" >
				Yes</label>
			</div>
			<div class="radio-inline">
			  <label>
				<input type="radio" name="LeadYesIn" value="Capture Lead NoIn" id="LeadNoIDIn" >
				No</label>
			</div>
			<div class="row animated  bounceInRight" style="display:none;" id="LeadDivIn">
			  <div class="col-md-6">
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="LeadClassIn myCheckbox" name="upsellLead[14].upSellType" value="Insurance" name="LeadClassIn" value="InsuranceIn" id="InsuranceIDCheckIn">
					Periodic Maintenance Service</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="InsuranceSelectIn">
				  <div class="col-md-12">
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[14].taggedTo" id="insuranceLead2">
								<option >Select</option>
						</select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <input class="form-control" rows="1" id="comments8" name="upsellLead[14].upsellComments">
					</div>
				   </div>
				</div>
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="LeadClassIn myCheckbox" name="upsellLead[15].upSellType" value="Warranty / EW" id="WARRANTYIDIn" >
					Warranty / EW</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="WARRANTYSelectIn">
					<div class="col-md-12">
					<div class="col-md-6">
					  <label for="">Tag to</label>
					 <select class="form-control" name="upsellLead[15].taggedTo" id="warrantyLead2">
								<option >Select</option>
							  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments9" name="upsellLead[15].upsellComments"></textarea>
					</div>
				   </div>
				 
				</div>
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="VASIn myCheckbox" name="upsellLead[16].upSellType" value="VAS" id="VASIDIn" />
					VAS</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="VASTagToSelectIn">
				   <div class="col-md-12">
					<div class="col-md-6">
					  <label for="">Tag to</label>
					 <select class="form-control" name="upsellLead[16].taggedTo" id="vASLead2">
								<option >Select</option>
							  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments10" name="upsellLead[16].upsellComments"></textarea>
					</div>
				   </div>
				  
				</div>
				<div class="checkbox">
				  <label>
			   <input type="checkbox" class="LeadClassIn myCheckbox" name="upsellLead[17].upSellType" value="Re-Finance / New Car Finance" id="ReFinanceIDCheckIn" />
					Re-Finance / New Car Finance</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="ReFinanceSelectIn">
				  <div class="col-md-12">
					<div class="col-md-6">
					  <label for="">Tag to</label>
					 <select class="form-control" name="upsellLead[17].taggedTo" id="reFinanceLead2">
								<option >Select</option>	
							  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments11" name="upsellLead[17].upsellComments"></textarea>
					</div>
				   </div>
				</div>
				</div>
				 <div class="col-md-6">
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="LoanIn myCheckbox" name="upsellLead[18].upSellType" value="Sell Old Car" id="LoanIDIn" />
					Sell Old Car</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="LoanSelectIn">
				   <div class="col-md-12">
					<div class="col-md-6">
					  <label for="">Tag to</label>
					 <select class="form-control" name="upsellLead[18].taggedTo" id="sellOldCarLead2">
								<option >Select</option>
							  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments12" name="upsellLead[18].upsellComments"></textarea>
					</div>
				   </div>
				</div>
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="EXCHANGEIn myCheckbox" name="upsellLead[19].upSellType" value="Buy New Car / Exchange" id="EXCHANGEIDIn" />
					Buy New Car / Exchange</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="EXCHANGEIDSelectIn">
				   <div class="col-md-12">
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[1"""),
format.raw("""9].taggedTo" id="buyNewCarLead2">
								<option >Select</option>
							  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments13" name="upsellLead[19].upsellComments"></textarea>
					</div>
				   </div>
				</div>
				  <div class="checkbox">
				  <label>
					<input type="checkbox" class="UsedCarIn myCheckbox" name="upsellLead[20].upSellType" value="UsedCar" id="UsedCarIDIn" />
					Used Car</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="UsedCarSelectIn">
				   <div class="col-md-12">
					<div class="col-md-6">
					  <label for="">Tag to</label>
					 <select class="form-control" name="upsellLead[20].taggedTo" id="usedCarLead2">
								<option >Select</option>
							  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments14" name="upsellLead[20].upsellComments"></textarea>
					</div>
				   </div>
			</div>
		   
			  </div>
			</div>
			  <div class="pull-right" >
			<button type="button" class="btn btn-primary" id="BackToLeadIn">Back</button>
			  <button type="button" class="btn btn-primary" value="" id="NextToCustFeedBackIn"/>Next</button>
			</div>
			</div>
			
			
			<!--Complaints Inbound -->
			
			<div class="col-md-12 animated  bounceInRight" id="CustFeedBackIn" style="display:none;">
				 
				  <div class="col-md-12">
					<label>Does Customer has a feedback / compliant ?</label>
					<br>
					<div class="radio-inline">
					  <label for="feedbackYesIn">
						<input type="radio" name="userfeedbackIn" onclick="loadLeadBasedOnLocationDepartment();" value="feedback YesIn" id="feedbackYesIn" >
						Yes</label>
					</div>
					<div class="radio-inline">
					  <label for="feedbackNoIn">
						<input type="radio" name="userfeedbackIn" value="feedback NoIn" id="feedbackNoIn" >
						No</label> 
					</div>
				  </div>
			   
				<div class="row animated  bounceInRight" style="display:none;" id="feedbackDIVIn"><br>
				 <div class="col-md-12">
				  <div class="col-md-3">
					<label for="">Select Department<i class="lblStar">*</i></label>
					<select class="form-control selected_department" name="departmentForFB2" id="selected_department2" onchange="ajaxLeadTagByDepartmentInbound();">
						 <option value="0">select</option>
						 
							</select>
				  </div>
				  <div class="col-md-3">
					<label for="">Tag to</label>
					<select class="form-control" id="LeadTagsByLocation2" name="complaintOrFB_TagTo2">
					"""),_display_(/*729.7*/{if(interOfCall.getSrdisposition()!=null){
					
					<option value={interOfCall.getSrdisposition().getComplaintOrFB_TagTo()} selected="selected">{interOfCall.getSrdisposition().getComplaintOrFB_TagTo()}</option>
					
					}}),format.raw/*733.8*/("""		  
					"""),format.raw/*734.6*/("""</select>
				  </div>
				
				  <div class="col-md-3">
					<label for="comments">Feedback/Comment</label>
					<textarea class="form-control" rows="1" id="commentsDSA" name="remarksOfFB2"></textarea>
				  </div>
				  <br>
				</div>
				  </div>
		<div class="pull-right" >
			<button type="button" class="btn btn-primary" id="BackToUpsellIn">Back</button>
			  <button type="button" class="btn btn-primary" value="" id="NextToInBoundLastQuest"/>Next</button>
			</div>
		 
		  </div>
		  <div class="row animated  bounceInRight" id="LastQuestionInbound" style="display:none;">
						  <div class="col-md-12">
						  <div class="col-md-12">
							<label>May I please take a moment to update your contact information? This will help us serve you better in future.</label>
							<br>
							<div class="radio-inline">
							  <label for="">
								<input type="radio" name="radio7" value="" id=""  data-toggle="modal" data-target="#addBtn" >
								Yes</label>
							</div>
							<div class="radio-inline">
							  <label for="feedbackNo">
								<input type="radio" name="radio7" value="" id="" >
								No</label> 
							</div>
						  </div>
						
						  <div class="pull-right" >
					<button type="button" class="btn btn-primary " id="BackToToInboindComplaint">Back</button>
					<button type="submit" class="btn btn-primary" value="inBoundCallSubmit" id="ToInboindComplaintSubmit" name="typeOfsubmit"/>Submit</button>
					</div>
						  </div>
		    </div>
		  </div>
				  
		  
		  
		 <!--EndLead-->
		 <!--EndLead-->"""))
      }
    }
  }

  def render(insurAgent:List[InsuranceAgent],customerData:Customer,vehicleData:Vehicle,locationList:List[Location],interOfCall:CallInteraction,userData:WyzUser): play.twirl.api.HtmlFormat.Appendable = apply(insurAgent,customerData,vehicleData,locationList,interOfCall,userData)

  def f:((List[InsuranceAgent],Customer,Vehicle,List[Location],CallInteraction,WyzUser) => play.twirl.api.HtmlFormat.Appendable) = (insurAgent,customerData,vehicleData,locationList,interOfCall,userData) => apply(insurAgent,customerData,vehicleData,locationList,interOfCall,userData)

  def ref: this.type = this

}


}

/**/
object insurancePageInbound extends insurancePageInbound_Scope0.insurancePageInbound
              /*
                  -- GENERATED --
                  DATE: Thu Dec 28 12:24:29 IST 2017
                  SOURCE: D:/CRMNEW/28th Dec/crm/app/views/insurancePageInbound.scala.html
                  HASH: 38d111cb9284daaeb62c9ee956b6dfc92f051484
                  MATRIX: 845->1|1089->149|1118->152|2650->1658|2705->1697|2744->1698|2836->1762|2879->1778|2892->1782|2924->1793|2954->1796|2967->1800|3001->1813|3080->1862|3140->1895|3401->2129|3456->2163|3486->2166|3541->2200|8317->6949|8380->6995|8420->6996|8477->7024|8521->7040|8541->7050|8581->7068|8611->7070|8631->7080|8672->7098|8703->7100|8724->7110|8765->7128|8796->7130|8817->7140|8850->7150|8881->7152|8902->7162|8938->7175|8970->7178|8991->7188|9032->7206|9063->7208|9084->7218|9125->7236|9156->7238|9177->7248|9218->7266|9249->7268|9270->7278|9303->7288|9334->7290|9355->7300|9391->7313|9470->7360|9511->7373|32267->30082|32518->30312|32557->30323
                  LINES: 27->1|32->1|33->2|72->41|72->41|72->41|74->43|74->43|74->43|74->43|74->43|74->43|74->43|76->45|80->49|88->57|88->57|88->57|88->57|218->187|218->187|218->187|219->188|219->188|219->188|219->188|219->188|219->188|219->188|219->188|219->188|219->188|219->188|219->188|219->188|219->188|219->188|219->188|219->188|219->188|219->188|219->188|219->188|219->188|219->188|219->188|219->188|219->188|219->188|219->188|219->188|219->188|219->188|221->190|223->192|761->729|765->733|766->734
                  -- GENERATED --
              */
          