
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object followUpCallsOfTodayCRE_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class followUpCallsOfTodayCRE extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template5[String,String,String,String,List[CallInteraction],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(oem:String,dealercode:String,dealerName:String,userName:String,followUpdata: List[CallInteraction]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
import helper._
import scala._

Seq[Any](format.raw/*1.102*/("""
"""),_display_(/*4.2*/mainPageCRE("AutoSherpaCRM",userName,dealerName,dealercode,oem)/*4.65*/ {_display_(Seq[Any](format.raw/*4.67*/("""
            
"""),format.raw/*6.1*/("""<style>
    td, th """),format.raw/*7.12*/("""{"""),format.raw/*7.13*/("""
                
        """),format.raw/*9.9*/("""border: 1px solid black;
        overflow: hidden; 
        text-align:center!important;
    """),format.raw/*12.5*/("""}"""),format.raw/*12.6*/("""
"""),format.raw/*13.1*/("""</style>

<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">  
                        <a href="/CRE/getDispositionPageOfTab" class="btn btn-primary btn-xs pull-right"><i class="fa fa-arrow-left"></i>&nbsp Back</a>                  
                        
                         Follow Up Required Calls Of Today
                        </div>
                        
                         <div class="panel-body">
                            <div class="dataTable_wrapper">                            
                            <div style="overflow-x: auto">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example15">
                                    <thead>
                                 <tr> 
                                	                            	
                                    <th>Customer Name</th>
                                    <th>Mobile Number</th>                                   
                                    <th>Vehicle RegNo.</th>
                                    <th>Model</th>
                                    <th>Follow Up Time</th>
                                    <th>Disposition Form</th>
                                    
                                   
                                                                                                      
                                    
                                 </tr>
                              </thead>
                                <tbody>
                                    
                                 """),_display_(/*45.35*/for(post <- followUpdata) yield /*45.60*/ {_display_(Seq[Any](format.raw/*45.62*/("""
                               """),format.raw/*46.32*/("""<tr>                               
                              	<td>"""),_display_(/*47.37*/post/*47.41*/.getCustomer().getCustomerName()),format.raw/*47.73*/("""</td>
                              	<td>"""),_display_(/*48.37*/{if(post.getCustomer().getPreferredPhone() !=null){
                                    
                                    post.getCustomer().getPreferredPhone().getPhoneNumber()
                                    
                                    }}),format.raw/*52.39*/("""</td>                              	
                              	 <td>"""),_display_(/*53.38*/post/*53.42*/.getVehicle().vehicleRegNo),format.raw/*53.68*/("""</td>
                              	<td>"""),_display_(/*54.37*/post/*54.41*/.getVehicle().model),format.raw/*54.60*/("""</td>  
                              	<td>"""),_display_(/*55.37*/post/*55.41*/.getSrdisposition().followUpTime),format.raw/*55.73*/("""</td>                            	
                              	 <td><a href="/CRE/getDispositionFormPage/"""),_display_(/*56.75*/post/*56.79*/.customer.getId()),format.raw/*56.96*/("""/"""),_display_(/*56.98*/post/*56.102*/.vehicle.getVehicle_id),format.raw/*56.124*/("""/service"><i class="fa fa-pencil-square" data-toggel="tooltip" title="Disposition" style="font-size:30px;color:#DD4B39;"></i></a></td>                           	                              	                                                                     
                                 </tr>
                                      
                                     """)))}),format.raw/*59.39*/("""
                             
                                   
                                """),format.raw/*62.33*/("""</tbody>
                               
                            </table>
                                </div>
                            </div>
                           
                            
                        </div>
                       
                    </div>
                    
                </div>
               
            </div>
            
""")))}))
      }
    }
  }

  def render(oem:String,dealercode:String,dealerName:String,userName:String,followUpdata:List[CallInteraction]): play.twirl.api.HtmlFormat.Appendable = apply(oem,dealercode,dealerName,userName,followUpdata)

  def f:((String,String,String,String,List[CallInteraction]) => play.twirl.api.HtmlFormat.Appendable) = (oem,dealercode,dealerName,userName,followUpdata) => apply(oem,dealercode,dealerName,userName,followUpdata)

  def ref: this.type = this

}


}

/**/
object followUpCallsOfTodayCRE extends followUpCallsOfTodayCRE_Scope0.followUpCallsOfTodayCRE
              /*
                  -- GENERATED --
                  DATE: Thu Dec 28 12:24:28 IST 2017
                  SOURCE: D:/CRMNEW/28th Dec/crm/app/views/followUpCallsOfTodayCRE.scala.html
                  HASH: dfac9ab9ad4745d002df64aa58c3f5604f3a1b1f
                  MATRIX: 824->1|1050->101|1078->139|1149->202|1188->204|1230->220|1277->240|1305->241|1359->269|1482->365|1510->366|1539->368|3333->2135|3374->2160|3414->2162|3475->2195|3575->2268|3588->2272|3641->2304|3711->2347|3992->2607|4094->2682|4107->2686|4154->2712|4224->2755|4237->2759|4277->2778|4349->2823|4362->2827|4415->2859|4552->2969|4565->2973|4603->2990|4632->2992|4646->2996|4690->3018|5103->3400|5233->3502
                  LINES: 27->1|33->1|34->4|34->4|34->4|36->6|37->7|37->7|39->9|42->12|42->12|43->13|75->45|75->45|75->45|76->46|77->47|77->47|77->47|78->48|82->52|83->53|83->53|83->53|84->54|84->54|84->54|85->55|85->55|85->55|86->56|86->56|86->56|86->56|86->56|86->56|89->59|92->62
                  -- GENERATED --
              */
          