
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object error500_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class error500 extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply():play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.1*/("""<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>AutoSherpa</title>
    <meta name="msapplication-TileColor" content="#5bc0de" />
    <meta name="msapplication-TileImage" content="assets/img/metis-tile.png" />
	<link rel="stylesheet" href=""""),_display_(/*9.32*/routes/*9.38*/.Assets.at("css/AdminLTE.min.css")),format.raw/*9.72*/("""">
<link rel="stylesheet" href=""""),_display_(/*10.31*/routes/*10.37*/.Assets.at("css/font-awesome.min.css")),format.raw/*10.75*/("""">
    <!-- Bootstrap -->
   <link rel="stylesheet" href=""""),_display_(/*12.34*/routes/*12.40*/.Assets.at("css/bootstrap.min.css")),format.raw/*12.75*/("""">

  </head>
  <body class="error" style="background-color:#ecf0f5">
   <!--  <div class="container">
      <div class="col-lg-8 col-lg-offset-2 text-center">
        <div class="logo">
          <h1>500</h1>
        </div>
        <p class="lead text-muted">Oops, an Internal error has occurred!</p>
        <div class="clearfix"></div>
        
        <div class="clearfix"></div>
        <br>
        <div class="col-lg-6 col-lg-offset-3">
          <div class="btn-group btn-group-justified">
            <a href="/" class="btn btn-warning">Home</a>
             </div>
        </div>
      </div>
    </div> -->
	<div class="container">
      <section class="content">
      <div class="">
        <h2 class="headline text-yellow"> </h2>

        <div class="col-lg-8 col-lg-offset-2 text-center">
          <h2><i class="fa fa-warning text-yellow"></i> Internal Server Error!</h2>

          <p>
            We could not find the page you were looking for.
            Meanwhile, you may<br /> <a href="/"><b><u>Return To Home</u></b></a> 
          </p>

         
        </div>
        <!-- /.error-content -->
      </div>
      <!-- /.error-page -->
    </section>
    </div>
  </body>
</html>


"""))
      }
    }
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}


}

/**/
object error500 extends error500_Scope0.error500
              /*
                  -- GENERATED --
                  DATE: Thu Dec 28 12:24:28 IST 2017
                  SOURCE: D:/CRMNEW/28th Dec/crm/app/views/error500.scala.html
                  HASH: 88de9d0e263699a405733ceec19b961b07b9b73b
                  MATRIX: 833->2|1138->281|1152->287|1206->321|1267->355|1282->361|1341->399|1429->460|1444->466|1500->501
                  LINES: 32->2|39->9|39->9|39->9|40->10|40->10|40->10|42->12|42->12|42->12
                  -- GENERATED --
              */
          