
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object InsuranceCallLogPageCREManager_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class InsuranceCallLogPageCREManager extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template7[List[Location],String,String,Map[Long, String],List[Campaign],List[ServiceTypes],List[CallDispositionData],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(listCity:List[Location],dealerName:String,user:String,allCREHash:Map[Long,String],campaignListInsurance :List[Campaign],serviceTypeList :List[ServiceTypes],dispoList:List[CallDispositionData]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.195*/("""
"""),_display_(/*2.2*/mainPageCREManger("AutoSherpaCRM",user,dealerName)/*2.52*/ {_display_(Seq[Any](format.raw/*2.54*/("""	



"""),format.raw/*6.1*/("""<style>
    td, th """),format.raw/*7.12*/("""{"""),format.raw/*7.13*/("""
        """),format.raw/*8.9*/("""border: 1px solid black;
        overflow: hidden; 
        text-align:center!important;
    """),format.raw/*11.5*/("""}"""),format.raw/*11.6*/("""
"""),format.raw/*12.1*/("""</style>
 
  <div>

        <div class="panel panel-primary">
            <div class="panel-heading">   
                Insurance Call Log Information </div>
            <div class="panel-body">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#home" data-toggle="tab" id="Tab1" onclick="InsuranceassignedInteractionDataMR();">Scheduled</a> </li>
                    <li><a href="#profile" data-toggle="tab" id="Tab2" onclick="ajaxInsuranceCallForFollowUpRequiredServerMR(4);">Follow Up Required</a> </li>
                    <li><a href="#messages" data-toggle="tab" id="Tab3" onclick="ajaxInsuranceCallForFollowUpRequiredServerMR(25);">Book Appointments</a> </li>
                    <li><a href="#settings" data-toggle="tab" id="Tab4" onclick="ajaxInsuranceCallForFollowUpRequiredServerMR(26);" >Renewal Not Required</a> </li>
                    <li><a href="#nonContacts" data-toggle="tab" id="Tab5" onclick="ajaxInsuranceCallForNonContactsServerMR(1);">Non Contacts</a> </li>
                    <li><a href="#droppedBuc" data-toggle="tab" id="Tab6" onclick="ajaxInsuranceCallForNonContactsServerMR(2);">Dropped</a> </li>
                </ul>
                <div class="tab-content">
                <div class="panel panel-default" style="border-top: #fff;">
				<div class="panel-body">
                <div class="row">
		<div class="col-md-2" id="cityDiv">
		<div class="form-group">
                <label>City </label>
               
               <select class="form-control" id="city" name="cityName" onchange="ajaxCallToLoadWorkShopByCity();"> 
                <option value='--Select--'>--Select--</option>
                
				"""),_display_(/*39.6*/for(city <- listCity) yield /*39.27*/{_display_(Seq[Any](format.raw/*39.28*/("""                  	                         
                    """),format.raw/*40.21*/("""<option value=""""),_display_(/*40.37*/city/*40.41*/.getName()),format.raw/*40.51*/("""">"""),_display_(/*40.54*/city/*40.58*/.getName()),format.raw/*40.68*/("""</option>
                    """)))}),format.raw/*41.22*/("""
                """),format.raw/*42.17*/("""</select>
</div>
            </div> 
			<div class="col-md-2" id="workshopDiv">
			<div class="form-group">
                <label>WorkShop Location </label>               
                <select class="form-control" id="workshop" name="workshopId" onchange="ajaxCallToLoadCRESByWorkshop();">                 			    
                

                </select>
			</div>
            </div> 
		
            <div class="col-md-2" id="cresDiv">
			 <label>Select CRE's </label>
			<div class="form-group">
              <select class="selectpicker filter form-control" data-column-index="7" id="ddlCreIds" name="ddlCreIds" multiple>
                
                    
                </select> 
</div>


            </div>
            
                <div class="col-md-2 tab-pane" id="campaignDiv" >
                                
                                <label>Select Campaign</label>
						<select class="filter form-control" id="campaignName" data-column-index="0" name="campaignName">
							<option value="0" >--Select--</option>
							"""),_display_(/*72.9*/for(campaign_List<-campaignListInsurance) yield /*72.50*/{_display_(Seq[Any](format.raw/*72.51*/("""
		                                """),format.raw/*73.35*/("""<option value=""""),_display_(/*73.51*/campaign_List/*73.64*/.getCampaignName()),format.raw/*73.82*/("""">"""),_display_(/*73.85*/campaign_List/*73.98*/.getCampaignName()),format.raw/*73.116*/("""</option>
		                             """)))}),format.raw/*74.33*/("""
		                                
						"""),format.raw/*76.7*/("""</select>
					</div>
					
					<div class="col-md-2" id="fromDateDiv">
					<label>From Assign Date : </label>
					<input type="text" class="filter form-control datepickerFilter" placeholder="From Assign Date" data-column-index="1" id="fromduedaterange" name="fromduedaterange" readonly>
				  </div>
				  <div class="col-md-2" id="toDateDiv">
					<label>To Assign Date : </label>
					<input type="text" class="filter form-control datepickerFilter" placeholder="To Assign Date" data-column-index="2" id="toduedaterange" name="toduedaterange" readonly>
				  </div>
				  </div>
				  
				  <div class="row">
				   
							<div class="col-md-2">
				  <label for="">Renewal Type</label>
				  <select class="filter form-control"  name="renewalType" data-column-index="4">
						<option value="0">--Select--</option>
						<option value="1st Renewal">1st Renewal</option>
						<option value="2nd Renewal">2nd Renewal</option>
						<option value="3rd Renewal">3rd Renewal</option>
						<option value="4th Renewal">4th Renewal</option>
						<option value="5th Renewal">5th Renewal</option>
						<option value="6th Renewal">6th Renewal</option>
						<option value="7th Renewal">7th Renewal</option>
						<option value="8th Renewal">8th Renewal</option>
					 </select>
				</div>
					 
					<div class="col-md-3" id="serviceBookTypeDiv" style="display:none">						
					<label>Select Booked service type</label>
						<select class="filter form-control" id="serviceBookedType" data-column-index="4" name="serviceBookedType">
							<option value="0" >--Select--</option>
	
					                                
						</select>
					</div>
					<div class="col-md-3" id="lastDispoTypeDiv" style="display:none">						
					<label>Last Disposition</label>
						<select class="filter form-control" id="lastDispo" data-column-index="5" >
							<option value="0" >--Select--</option>
								"""),_display_(/*118.10*/for(dispo <- dispoList) yield /*118.33*/{_display_(Seq[Any](format.raw/*118.34*/("""
			"""),format.raw/*119.4*/("""<option value=""""),_display_(/*119.20*/dispo/*119.25*/.getDisposition()),format.raw/*119.42*/("""">"""),_display_(/*119.45*/dispo/*119.50*/.getDisposition()),format.raw/*119.67*/("""</option>
			
		""")))}),format.raw/*121.4*/("""
							
					                                
						"""),format.raw/*124.7*/("""</select>
					</div>
					<div class="col-md-3" id="droppedcountDiv" style="display:none">						
					<label>Dropped Count</label>
						<select class="filter form-control" id="droppedCount" data-column-index="6">
							<option value="0" >--Select--</option>
							<option value="1" >1</option>
							<option value="2" >2</option>
							<option value="3" >3</option>
							<option value="4" >4</option>
							
					                                
						</select>
					</div>
				</div>
                </div>
				  </div>
                    <div class="panel panel-default tab-pane fade in active" id="home">
                        <div class="panel-body inf-content">              

                            <div class="dataTable_wrapper">                            
                                <div >
                                    <table class="table table-striped table-bordered table-hover" id="InsuranceassignedInteractionTableMR">
                                        <thead>
                                            <tr> 

				    		<th>CRE Name</th>
				    						    		<th>Campaign Name</th>
				    		<th>Customer Name</th>
				    		<th>Mobile Number</th>
				    		<th>Vehicle Reg No</th>
				    		<th>ChassisNo</th>
				    		<th>Policy Due Month</th>
				    		<th>Policy Due Date</th>
				    		<th>Next Renewal Type</th>
				    		<th>Last Insurance Company</th>
                                            </tr>
                                        </thead>
                                        <tbody>								
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="profile">
                        <div class="panel-body inf-content">
                            <div class="dataTable_wrapper">                            
                                <div >
                                    <table class="table table-striped table-bordered table-hover" id="InsurancefollowUpRequiredServerDataMR4" >
                                        <thead>
                         
                                         	<tr> 
                                          		<th>Is CallInitiated</th>
                                          		<th>CRE Name</th>
                                          		<th>Campaign Name</th>
                                                <th>Customer Name</th>
                                                <th>Vehicle RegNo.</th>
                                                <th>Model</th>
                                                <th>FollowUp Date</th>                                    
                                                <th>FollowUp Time</th>
                                                <th>Renewal Type</th>
                                                <th>Reason</th>
                                                <th>Last Disposition</th>
                                            </tr>   
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="messages">
                        <div class="panel-body inf-content">
                            <div class="dataTable_wrapper">                            
                                <div >
                                    <table class="table table-striped table-bordered table-hover" id="InsurancefollowUpRequiredServerDataMR25">
                                        <thead>
                                            <tr> 
                                            	<th>Campaign Name</th>
                                          		<th>CRE Name</th>
                                                <th>Customer Name</th>
                                                <th>Vehicle RegNo.</th>
                                                <th>Appointment Date</th>                                    
                                                <th>Appointment Time</th>
                                                <th>Appointment Status</th>
                                                <th>Renewal Type</th>
                                                <th>Insurance Agent</th>
                                                <th>Last Disposition</th>
                                                <th>Reason</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="settings">
                         <div class="panel-body inf-content">
                        <div class="dataTable_wrapper">                            
                            <div >
                                <table class="table table-striped table-bordered table-hover" id="InsurancefollowUpRequiredServerDataMR26">
                                    <thead>
                                        <tr> 
                                        	<th>Campaign Name</th>
                                          		<th>CRE Name</th>
                                                <th>Customer Name</th>
                                                <th>Vehicle RegNo.</th>
                                                <th>Renewal Type</th>
                                                <th>Insurance Agent</th>
                                                <th>Last Disposition</th>
                                                <th>Reason</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    </div><!--End tab panel-->
                    <div class="tab-pane fade " id="nonContacts">
                        <div class="panel-body inf-content"> 
                            <div >
                                <table class="table table-striped table-bordered table-hover" id="InsurancenonContactsServerDataMR1">
                                    <thead>
                                        <tr> 
                                        	th>Campaign Name</th>
                                       	 	<th>CRE Name</th>                         
                                            <th>Customer Name</th>
                                                                                            <th>Call date</th>
                                            
                                            <th>Mobile Number</th>         
                                            <th>Vehicle RegNo.</th>
                                      
                
                                            
                                          
                                                <th>Last Disposition</th>
                                                 											
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="droppedBuc">
                        <div class="panel-body inf-content">
                            <div class="dataTable_wrapper">                            
                                <div style="overflow-x: auto">
                                    <table class="table table-striped table-bordered table-hover" id="InsurancenonContactsServerDataMR2">
                                        <thead>
                                                 <tr> 
                                        th>Campaign Name</th>
                                       	 	<th>CRE Name</th>                         
                                            <th>Customer Name</th>
                                                                                            <th>Call date</th>
                                            
                                            <th>Mobile Number</th>         
                                            <th>Vehicle RegNo.</th>
                                      
                
                                            
                                          
                                                <th>Last Disposition</th>
                                                 											
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
				</div><!--End tab content-->
            </div><!--End panel body-->
        </div>
   
</div>
""")))}),format.raw/*309.2*/(""" 

"""),format.raw/*311.1*/("""<script type="text/javascript"> 

function InsuranceassignedInteractionDataMR()"""),format.raw/*313.46*/("""{"""),format.raw/*313.47*/("""
    """),format.raw/*314.5*/("""// alert("server side data load");
    document.getElementById("cresDiv").style.display = "block";
	//document.getElementById("workshopDiv").style.display = "block";
	//document.getElementById("cityDiv").style.display = "block";	
	document.getElementById("fromDateDiv").style.display = "block";
	document.getElementById("toDateDiv").style.display = "block";
	document.getElementById("campaignDiv").style.display = "block";
    //document.getElementById("serviceBookTypeDiv").style.display = "none";
    //document.getElementById("serviceTypeDiv").style.display = "block";

	//document.getElementById("lastDispoTypeDiv").style.display = "none";
	//document.getElementById("droppedcountDiv").style.display = "none";

    var UserIds="",i;
    myOption = document.getElementById('ddlCreIds');
    
for (i=0;i<myOption.options.length;i++)"""),format.raw/*330.40*/("""{"""),format.raw/*330.41*/("""
    """),format.raw/*331.5*/("""if(myOption.options[i].selected)"""),format.raw/*331.37*/("""{"""),format.raw/*331.38*/("""
        """),format.raw/*332.9*/("""if(myOption.options[i].value != "--Select--")"""),format.raw/*332.54*/("""{"""),format.raw/*332.55*/("""
    	 """),format.raw/*333.7*/("""UserIds = UserIds + myOption.options[i].value + ",";
    	 console.log("Users id"+UserIds);
        """),format.raw/*335.9*/("""}"""),format.raw/*335.10*/("""
    """),format.raw/*336.5*/("""}"""),format.raw/*336.6*/("""
"""),format.raw/*337.1*/("""}"""),format.raw/*337.2*/("""
"""),format.raw/*338.1*/("""if(UserIds.length > 0)"""),format.raw/*338.23*/("""{"""),format.raw/*338.24*/("""
    	"""),format.raw/*339.6*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
"""),format.raw/*340.1*/("""}"""),format.raw/*340.2*/("""
"""),format.raw/*341.1*/("""else"""),format.raw/*341.5*/("""{"""),format.raw/*341.6*/("""
"""),format.raw/*342.1*/("""UserIds="Select";
"""),format.raw/*343.1*/("""}"""),format.raw/*343.2*/("""
    """),format.raw/*344.5*/("""var ajaxUrl = "/CREManager/InsuranceassignedInteractionTableDataMR/"+UserIds+"";
    
    
    var table= $('#InsuranceassignedInteractionTableMR').dataTable( """),format.raw/*347.69*/("""{"""),format.raw/*347.70*/("""
        """),format.raw/*348.9*/(""""bDestroy": true,
        "processing": true,
        "serverSide": true,
        "scrollY": 300,
		 "scrollX": true,
        "paging": true,
        "searching":true,
        "ordering":false,	
        "ajax": ajaxUrl,
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*357.59*/("""{"""),format.raw/*357.60*/("""
              """),format.raw/*358.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*360.15*/("""}"""),format.raw/*360.16*/("""
    """),format.raw/*361.5*/("""}"""),format.raw/*361.6*/(""" """),format.raw/*361.7*/(""");

    FilterOptionDataMR(table);
    
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e)"""),format.raw/*365.61*/("""{"""),format.raw/*365.62*/("""
      """),format.raw/*366.7*/("""$($.fn.dataTable.tables(true)).DataTable()
         .columns.adjust();
   """),format.raw/*368.4*/("""}"""),format.raw/*368.5*/(""");
    
    """),format.raw/*370.5*/("""}"""),format.raw/*370.6*/("""




"""),format.raw/*375.1*/("""function ajaxInsuranceCallForFollowUpRequiredServerMR(buckettype)"""),format.raw/*375.66*/("""{"""),format.raw/*375.67*/("""
	"""),format.raw/*376.2*/("""document.getElementById("cresDiv").style.display = "block";
	//document.getElementById("workshopDiv").style.display = "block";
	//document.getElementById("cityDiv").style.display = "block";	
	document.getElementById("campaignDiv").style.display = "block";
	//document.getElementById("serviceBookTypeDiv").style.display = "none";
	//document.getElementById("serviceTypeDiv").style.display = "block";
	document.getElementById("fromDateDiv").style.display = "block";
	document.getElementById("toDateDiv").style.display = "block";
	document.getElementById("lastDispoTypeDiv").style.display = "block";
	//document.getElementById("droppedcountDiv").style.display = "none";
	          

	    var UserIds="",i;
	    myOption = document.getElementById('ddlCreIds');
	    
	for (i=0;i<myOption.options.length;i++)"""),format.raw/*391.41*/("""{"""),format.raw/*391.42*/("""
	    """),format.raw/*392.6*/("""if(myOption.options[i].selected)"""),format.raw/*392.38*/("""{"""),format.raw/*392.39*/("""
	        """),format.raw/*393.10*/("""if(myOption.options[i].value != "--Select--")"""),format.raw/*393.55*/("""{"""),format.raw/*393.56*/("""
	    	 """),format.raw/*394.8*/("""UserIds = UserIds + myOption.options[i].value + ",";
	        """),format.raw/*395.10*/("""}"""),format.raw/*395.11*/("""
	    """),format.raw/*396.6*/("""}"""),format.raw/*396.7*/("""
	"""),format.raw/*397.2*/("""}"""),format.raw/*397.3*/("""
	"""),format.raw/*398.2*/("""if(UserIds.length > 0)"""),format.raw/*398.24*/("""{"""),format.raw/*398.25*/("""
	    	"""),format.raw/*399.7*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
	"""),format.raw/*400.2*/("""}"""),format.raw/*400.3*/("""
	"""),format.raw/*401.2*/("""else"""),format.raw/*401.6*/("""{"""),format.raw/*401.7*/("""
	"""),format.raw/*402.2*/("""UserIds="Select";
	"""),format.raw/*403.2*/("""}"""),format.raw/*403.3*/("""
	    """),format.raw/*404.6*/("""var ajaxUrl = "/CREManager/InsurancefollowUpCallLogTableDataMR/"+UserIds+"/"+buckettype+"";
	    
	    
	    var table= $('#InsurancefollowUpRequiredServerDataMR'+buckettype).dataTable( """),format.raw/*407.83*/("""{"""),format.raw/*407.84*/("""
	        """),format.raw/*408.10*/(""""bDestroy": true,
	        "processing": true,
	        "serverSide": true,
	        "scrollY": 300,
			 "scrollX": true,
	        "paging": true,
	        "searching":true,
	        "ordering":false,	
	        "ajax": ajaxUrl,
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*417.60*/("""{"""),format.raw/*417.61*/("""
              """),format.raw/*418.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*420.15*/("""}"""),format.raw/*420.16*/("""
	    """),format.raw/*421.6*/("""}"""),format.raw/*421.7*/(""" """),format.raw/*421.8*/(""");

	    FilterOptionDataMR(table);
	    
	"""),format.raw/*425.2*/("""}"""),format.raw/*425.3*/("""



"""),format.raw/*429.1*/("""function ajaxInsuranceCallForNonContactsServerMR(buckettype)"""),format.raw/*429.61*/("""{"""),format.raw/*429.62*/("""
	"""),format.raw/*430.2*/("""//alert("noncontact");
	
		document.getElementById("cresDiv").style.display = "block";
		//document.getElementById("workshopDiv").style.display = "block";
		//document.getElementById("cityDiv").style.display = "block";	
		document.getElementById("campaignDiv").style.display = "block";
		//document.getElementById("serviceBookTypeDiv").style.display = "none";
		//document.getElementById("serviceTypeDiv").style.display = "none";
		document.getElementById("fromDateDiv").style.display = "block";
		document.getElementById("toDateDiv").style.display = "block";
		document.getElementById("lastDispoTypeDiv").style.display = "block";
		//document.getElementById("droppedcountDiv").style.display = "block";

	var UserIds="",i;
    myOption = document.getElementById('ddlCreIds');
    
for (i=0;i<myOption.options.length;i++)"""),format.raw/*446.40*/("""{"""),format.raw/*446.41*/("""
    """),format.raw/*447.5*/("""if(myOption.options[i].selected)"""),format.raw/*447.37*/("""{"""),format.raw/*447.38*/("""
        """),format.raw/*448.9*/("""if(myOption.options[i].value != "Select")"""),format.raw/*448.50*/("""{"""),format.raw/*448.51*/("""
    		"""),format.raw/*449.7*/("""UserIds = UserIds + myOption.options[i].value + ",";
        """),format.raw/*450.9*/("""}"""),format.raw/*450.10*/("""
    """),format.raw/*451.5*/("""}"""),format.raw/*451.6*/("""
"""),format.raw/*452.1*/("""}"""),format.raw/*452.2*/("""
	"""),format.raw/*453.2*/("""if(UserIds.length > 0)"""),format.raw/*453.24*/("""{"""),format.raw/*453.25*/("""
    	"""),format.raw/*454.6*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
	"""),format.raw/*455.2*/("""}"""),format.raw/*455.3*/("""
	"""),format.raw/*456.2*/("""else"""),format.raw/*456.6*/("""{"""),format.raw/*456.7*/("""
		"""),format.raw/*457.3*/("""UserIds="Select";
	"""),format.raw/*458.2*/("""}"""),format.raw/*458.3*/("""
    """),format.raw/*459.5*/("""var ajaxUrl = "/CREManager/InsurancenonContactsServerDataTableMR/"+UserIds+"/"+buckettype+"";
     
    var table = $('#InsurancenonContactsServerDataMR'+buckettype).dataTable( """),format.raw/*461.78*/("""{"""),format.raw/*461.79*/("""
        """),format.raw/*462.9*/(""""bDestroy": true,
        "processing": true,
        "serverSide": true,
        "scrollY": 300,
		 "scrollX": true,
        "paging": true,
        "searching":true,
        "ordering":false,			
        "ajax": ajaxUrl,
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*471.59*/("""{"""),format.raw/*471.60*/("""
              """),format.raw/*472.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*474.15*/("""}"""),format.raw/*474.16*/("""
    """),format.raw/*475.5*/("""}"""),format.raw/*475.6*/(""" """),format.raw/*475.7*/(""");

    FilterOptionDataMR(table);
    
    """),format.raw/*479.5*/("""}"""),format.raw/*479.6*/("""






"""),format.raw/*486.1*/("""</script>
<script type="text/javascript">  
function FilterOptionDataMR(table)"""),format.raw/*488.35*/("""{"""),format.raw/*488.36*/("""
	"""),format.raw/*489.2*/("""var values = [];
	$('.filter').on('change', function() """),format.raw/*490.39*/("""{"""),format.raw/*490.40*/("""
    	"""),format.raw/*491.6*/("""console.log("inside filteroptionsdata data");
	    var i= $(this).data('columnIndex');
	    var v = $(this).val();
	    console.log("i : "+i);
		console.log("v length "+v.length);
	    
	    if(v.length>0)"""),format.raw/*497.20*/("""{"""),format.raw/*497.21*/("""
		    
		    """),format.raw/*499.7*/("""if(i == 7)"""),format.raw/*499.17*/("""{"""),format.raw/*499.18*/("""	

		    	"""),format.raw/*501.8*/("""var UserIds="",j;
		        myOption = document.getElementById('ddlCreIds');
		        console.log("here"+myOption);		        
		    for (j=0;j<myOption.options.length;j++)"""),format.raw/*504.46*/("""{"""),format.raw/*504.47*/("""
		        """),format.raw/*505.11*/("""if(myOption.options[j].selected)"""),format.raw/*505.43*/("""{"""),format.raw/*505.44*/("""
		            """),format.raw/*506.15*/("""if(myOption.options[j].value != "Select")"""),format.raw/*506.56*/("""{"""),format.raw/*506.57*/("""
		        		"""),format.raw/*507.13*/("""UserIds = UserIds + myOption.options[j].value + ",";
		            """),format.raw/*508.15*/("""}"""),format.raw/*508.16*/("""
		        """),format.raw/*509.11*/("""}"""),format.raw/*509.12*/("""
		    """),format.raw/*510.7*/("""}"""),format.raw/*510.8*/("""
		    	"""),format.raw/*511.8*/("""if(UserIds.length > 0)"""),format.raw/*511.30*/("""{"""),format.raw/*511.31*/("""
		        	"""),format.raw/*512.12*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
		        	values[7]=UserIds;
		    	"""),format.raw/*514.8*/("""}"""),format.raw/*514.9*/("""else"""),format.raw/*514.13*/("""{"""),format.raw/*514.14*/("""
		    		"""),format.raw/*515.9*/("""UserIds="Select";
		    		values[7]=UserIds;
		    	"""),format.raw/*517.8*/("""}"""),format.raw/*517.9*/("""
		    
		    """),format.raw/*519.7*/("""}"""),format.raw/*519.8*/("""else"""),format.raw/*519.12*/("""{"""),format.raw/*519.13*/("""
		    	"""),format.raw/*520.8*/("""values[i] = v;
			    """),format.raw/*521.8*/("""}"""),format.raw/*521.9*/("""
	
			       
		  
		    """),format.raw/*525.7*/("""}"""),format.raw/*525.8*/("""		    
	    
	    	"""),format.raw/*527.7*/("""console.log(JSON.stringify(values));	   	
	  	  //clear global search values
	 	  //table.api().search('');
	 	  $(this).data('columnIndex');
	 	 //var table = $('#nonContactsServerDataMR').dataTable();
	 	 if(values.length>0) """),format.raw/*532.25*/("""{"""),format.raw/*532.26*/("""
			 """),format.raw/*533.5*/("""for(var i=0;i<values.length;i++)"""),format.raw/*533.37*/("""{"""),format.raw/*533.38*/("""
				 """),format.raw/*534.6*/("""console.log("values[i] : "+values[i]);	
			    table.api().columns(i).search(values[i]);
			 """),format.raw/*536.5*/("""}"""),format.raw/*536.6*/("""
			 """),format.raw/*537.5*/("""table.api().draw(); 
		"""),format.raw/*538.3*/("""}"""),format.raw/*538.4*/("""
	 	
		 	  
	"""),format.raw/*541.2*/("""}"""),format.raw/*541.3*/(""");
	
	
"""),format.raw/*544.1*/("""}"""),format.raw/*544.2*/("""
"""),format.raw/*545.1*/("""</script>

 <script type="text/javascript">
	window.onload = InsuranceassignedInteractionDataMR();
</script>
    """))
      }
    }
  }

  def render(listCity:List[Location],dealerName:String,user:String,allCREHash:Map[Long, String],campaignListInsurance:List[Campaign],serviceTypeList:List[ServiceTypes],dispoList:List[CallDispositionData]): play.twirl.api.HtmlFormat.Appendable = apply(listCity,dealerName,user,allCREHash,campaignListInsurance,serviceTypeList,dispoList)

  def f:((List[Location],String,String,Map[Long, String],List[Campaign],List[ServiceTypes],List[CallDispositionData]) => play.twirl.api.HtmlFormat.Appendable) = (listCity,dealerName,user,allCREHash,campaignListInsurance,serviceTypeList,dispoList) => apply(listCity,dealerName,user,allCREHash,campaignListInsurance,serviceTypeList,dispoList)

  def ref: this.type = this

}


}

/**/
object InsuranceCallLogPageCREManager extends InsuranceCallLogPageCREManager_Scope0.InsuranceCallLogPageCREManager
              /*
                  -- GENERATED --
                  DATE: Thu Dec 28 13:53:08 IST 2017
                  SOURCE: D:/CRMNEW/28th Dec/crm/app/views/InsuranceCallLogPageCREManager.scala.html
                  HASH: 9d79b5dd8165ce1e66f0ce420b6ad1fc6bc90611
                  MATRIX: 895->1|1184->194|1212->197|1270->247|1309->249|1344->258|1391->278|1419->279|1455->289|1578->385|1606->386|1635->388|3379->2106|3416->2127|3455->2128|3549->2194|3592->2210|3605->2214|3636->2224|3666->2227|3679->2231|3710->2241|3773->2273|3819->2291|4931->3377|4988->3418|5027->3419|5091->3455|5134->3471|5156->3484|5195->3502|5225->3505|5247->3518|5287->3536|5361->3579|5432->3623|7402->5565|7442->5588|7482->5589|7515->5594|7559->5610|7574->5615|7613->5632|7644->5635|7659->5640|7698->5657|7748->5676|7832->5732|17858->15727|17891->15732|18001->15813|18031->15814|18065->15820|18944->16670|18974->16671|19008->16677|19069->16709|19099->16710|19137->16720|19211->16765|19241->16766|19277->16774|19407->16876|19437->16877|19471->16883|19500->16884|19530->16886|19559->16887|19589->16889|19640->16911|19670->16912|19705->16919|19786->16972|19815->16973|19845->16975|19877->16979|19906->16980|19936->16982|19983->17001|20012->17002|20046->17008|20237->17170|20267->17171|20305->17181|20621->17468|20651->17469|20696->17485|20803->17563|20833->17564|20867->17570|20896->17571|20925->17572|21058->17676|21088->17677|21124->17685|21228->17761|21257->17762|21299->17776|21328->17777|21366->17787|21460->17852|21490->17853|21521->17856|22368->18674|22398->18675|22433->18682|22494->18714|22524->18715|22564->18726|22638->18771|22668->18772|22705->18781|22797->18844|22827->18845|22862->18852|22891->18853|22922->18856|22951->18857|22982->18860|23033->18882|23063->18883|23099->18891|23181->18945|23210->18946|23241->18949|23273->18953|23302->18954|23333->18957|23381->18977|23410->18978|23445->18985|23663->19174|23693->19175|23733->19186|24058->19482|24088->19483|24133->19499|24240->19577|24270->19578|24305->19585|24334->19586|24363->19587|24438->19634|24467->19635|24503->19643|24592->19703|24622->19704|24653->19707|25518->20543|25548->20544|25582->20550|25643->20582|25673->20583|25711->20593|25781->20634|25811->20635|25847->20643|25937->20705|25967->20706|26001->20712|26030->20713|26060->20715|26089->20716|26120->20719|26171->20741|26201->20742|26236->20749|26318->20803|26347->20804|26378->20807|26410->20811|26439->20812|26471->20816|26519->20836|26548->20837|26582->20843|26790->21022|26820->21023|26858->21033|27176->21322|27206->21323|27251->21339|27358->21417|27388->21418|27422->21424|27451->21425|27480->21426|27556->21474|27585->21475|27627->21489|27736->21569|27766->21570|27797->21573|27882->21629|27912->21630|27947->21637|28187->21848|28217->21849|28261->21865|28300->21875|28330->21876|28370->21888|28574->22063|28604->22064|28645->22076|28706->22108|28736->22109|28781->22125|28851->22166|28881->22167|28924->22181|29021->22249|29051->22250|29092->22262|29122->22263|29158->22271|29187->22272|29224->22281|29275->22303|29305->22304|29347->22317|29466->22408|29495->22409|29528->22413|29558->22414|29596->22424|29678->22478|29707->22479|29751->22495|29780->22496|29813->22500|29843->22501|29880->22510|29931->22533|29960->22534|30017->22563|30046->22564|30095->22585|30356->22817|30386->22818|30420->22824|30481->22856|30511->22857|30546->22864|30669->22959|30698->22960|30732->22966|30784->22990|30813->22991|30857->23007|30886->23008|30924->23018|30953->23019|30983->23021
                  LINES: 27->1|32->1|33->2|33->2|33->2|37->6|38->7|38->7|39->8|42->11|42->11|43->12|70->39|70->39|70->39|71->40|71->40|71->40|71->40|71->40|71->40|71->40|72->41|73->42|103->72|103->72|103->72|104->73|104->73|104->73|104->73|104->73|104->73|104->73|105->74|107->76|149->118|149->118|149->118|150->119|150->119|150->119|150->119|150->119|150->119|150->119|152->121|155->124|340->309|342->311|344->313|344->313|345->314|361->330|361->330|362->331|362->331|362->331|363->332|363->332|363->332|364->333|366->335|366->335|367->336|367->336|368->337|368->337|369->338|369->338|369->338|370->339|371->340|371->340|372->341|372->341|372->341|373->342|374->343|374->343|375->344|378->347|378->347|379->348|388->357|388->357|389->358|391->360|391->360|392->361|392->361|392->361|396->365|396->365|397->366|399->368|399->368|401->370|401->370|406->375|406->375|406->375|407->376|422->391|422->391|423->392|423->392|423->392|424->393|424->393|424->393|425->394|426->395|426->395|427->396|427->396|428->397|428->397|429->398|429->398|429->398|430->399|431->400|431->400|432->401|432->401|432->401|433->402|434->403|434->403|435->404|438->407|438->407|439->408|448->417|448->417|449->418|451->420|451->420|452->421|452->421|452->421|456->425|456->425|460->429|460->429|460->429|461->430|477->446|477->446|478->447|478->447|478->447|479->448|479->448|479->448|480->449|481->450|481->450|482->451|482->451|483->452|483->452|484->453|484->453|484->453|485->454|486->455|486->455|487->456|487->456|487->456|488->457|489->458|489->458|490->459|492->461|492->461|493->462|502->471|502->471|503->472|505->474|505->474|506->475|506->475|506->475|510->479|510->479|517->486|519->488|519->488|520->489|521->490|521->490|522->491|528->497|528->497|530->499|530->499|530->499|532->501|535->504|535->504|536->505|536->505|536->505|537->506|537->506|537->506|538->507|539->508|539->508|540->509|540->509|541->510|541->510|542->511|542->511|542->511|543->512|545->514|545->514|545->514|545->514|546->515|548->517|548->517|550->519|550->519|550->519|550->519|551->520|552->521|552->521|556->525|556->525|558->527|563->532|563->532|564->533|564->533|564->533|565->534|567->536|567->536|568->537|569->538|569->538|572->541|572->541|575->544|575->544|576->545
                  -- GENERATED --
              */
          