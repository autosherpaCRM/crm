
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object viewReportCRE_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class viewReportCRE extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template7[List[String],controllers.webmodels.BookedLineChartData,controllers.webmodels.PieChartCallTypeData,String,String,String,Form[WyzUser],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(ajaxData:List[String],bookeddata:controllers.webmodels.BookedLineChartData,returnData:controllers.webmodels.PieChartCallTypeData,dealercode:String,dealerName:String,userName:String,form:Form[WyzUser]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
import models._
import helper._
import scala._

Seq[Any](format.raw/*1.203*/("""
"""),_display_(/*5.2*/mainPageCRE("AutoSherpaCRM",userName,dealerName,dealercode,"")/*5.64*/ {_display_(Seq[Any](format.raw/*5.66*/("""
"""),format.raw/*6.1*/("""<input type="hidden" id="piechartdata" value='"""),_display_(/*6.48*/returnData/*6.58*/.missedCallCount),format.raw/*6.74*/("""'>
<input type="hidden" id="piechartdata1" value='"""),_display_(/*7.49*/returnData/*7.59*/.outgoingCallCount),format.raw/*7.77*/("""'>
<input type="hidden" id="piechartdata2" value='"""),_display_(/*8.49*/returnData/*8.59*/.incomingCallCount),format.raw/*8.77*/("""'>

<input type="hidden" id="areachartdata1" value='"""),_display_(/*10.50*/bookeddata/*10.60*/.callList),format.raw/*10.69*/("""'>
<input type="hidden" id="areachartdata2" value='"""),_display_(/*11.50*/bookeddata/*11.60*/.bookedList),format.raw/*11.71*/("""'>

<input type="hidden" id="indexBox1" value='"""),_display_(/*13.45*/ajaxData),format.raw/*13.53*/("""'>


<div class="panel panel-default">
  <div class="panel-heading">Chart Reports</div>
  <div class="panel-body">
    <div class="row">
      <!-- <div class="col-md-5"> 
        
        <div class="box box-danger">
          <div class="box-header with-border">
            <h3 class="box-title">Pi Chart</h3>
          </div>
          <div class="box-body">
            <canvas id="pieChartForCRE" style="height:250px"></canvas>
          </div>
        </div>
      </div> -->
      <div class="col-md-8"> 
        <!-- AREA CHART -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Calls made vs Service Booked</h3>
          </div>
          <div class="box-body">
            <div class="chart">
              <canvas id="areaChartForCRE" style="height:250px"></canvas>
            </div>
          </div>
        </div>
      </div>
   <!--  <div class="col-md-2"> 
       
        <div class="box box-primary">
          <div class="panel panel-default">
            <div class="panel-heading"> <a href="#" class="pull-right"> <em class="icon-plus text-muted"></em> </a>Contacts</div>
            <div class="list-group"> 
             
              <a href="#" class="media p mt0 list-group-item" > <span class="pull-right"> <span class="circle circle-success circle-lg"></span> </span> <span class="media-body"> <span class="media-heading"> <strong>"""),_display_(/*51.217*/{userName}),format.raw/*51.227*/("""</strong> <br>
              <small class="text-muted">Agent</small> </span> </span> </a> 
             
              <a href="#" class="media p mt0 list-group-item text-center text-muted">View all contacts</a> </div>
          </div>
        </div>
      </div> -->
    </div>
  </div>
</div>
""")))}),format.raw/*61.2*/("""
 """),format.raw/*62.2*/("""<script type="text/javascript">
 window.onload = ajaxRequestPieChartForCRE; 
</script>"""))
      }
    }
  }

  def render(ajaxData:List[String],bookeddata:controllers.webmodels.BookedLineChartData,returnData:controllers.webmodels.PieChartCallTypeData,dealercode:String,dealerName:String,userName:String,form:Form[WyzUser]): play.twirl.api.HtmlFormat.Appendable = apply(ajaxData,bookeddata,returnData,dealercode,dealerName,userName,form)

  def f:((List[String],controllers.webmodels.BookedLineChartData,controllers.webmodels.PieChartCallTypeData,String,String,String,Form[WyzUser]) => play.twirl.api.HtmlFormat.Appendable) = (ajaxData,bookeddata,returnData,dealercode,dealerName,userName,form) => apply(ajaxData,bookeddata,returnData,dealercode,dealerName,userName,form)

  def ref: this.type = this

}


}

/**/
object viewReportCRE extends viewReportCRE_Scope0.viewReportCRE
              /*
                  -- GENERATED --
                  DATE: Thu Dec 28 12:24:31 IST 2017
                  SOURCE: D:/CRMNEW/28th Dec/crm/app/views/viewReportCRE.scala.html
                  HASH: a9211aa983dbe02ec8bcbc70675a2e2ec69051f5
                  MATRIX: 887->1|1230->202|1258->258|1328->320|1367->322|1395->324|1468->371|1486->381|1522->397|1600->449|1618->459|1656->477|1734->529|1752->539|1790->557|1872->612|1891->622|1921->631|2001->684|2020->694|2052->705|2129->755|2158->763|3659->2236|3691->2246|4027->2552|4057->2555
                  LINES: 27->1|34->1|35->5|35->5|35->5|36->6|36->6|36->6|36->6|37->7|37->7|37->7|38->8|38->8|38->8|40->10|40->10|40->10|41->11|41->11|41->11|43->13|43->13|81->51|81->51|91->61|92->62
                  -- GENERATED --
              */
          