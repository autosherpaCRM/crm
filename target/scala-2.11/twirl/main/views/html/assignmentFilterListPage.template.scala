
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object assignmentFilterListPage_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class assignmentFilterListPage extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template8[List[String],String,String,List[Campaign],List[Location],List[Campaign],List[Campaign],List[ServiceTypes],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(allCRE:List[String],dealerName:String,user:String,campaign_list:List[Campaign],locationList:List[Location],campaignList:List[Campaign],campaignListPSF:List[Campaign],servicetypes:List[ServiceTypes]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.201*/("""

"""),_display_(/*4.2*/mainPageCREManger("AutoSherpaCRM",user,dealerName)/*4.52*/ {_display_(Seq[Any](format.raw/*4.54*/("""


"""),format.raw/*7.1*/("""<div class="panel panel-primary">
	<div class="panel-heading">
		<strong>Call Allocation | To Assign Calls</strong>
	</div>
	<div class="panel-body">
		<div class="row">


			<div class="col-sm-3 campaignTypeDiv">
				<div class="form-group">
					<label>Assignment Type :</label> <select class="form-control"
						name="campaignType" id="campaignTypeData"> 
						<option value='0'>--Select--</option>
						"""),_display_(/*20.8*/for(list_camp	<- campaign_list) yield /*20.39*/{_display_(Seq[Any](format.raw/*20.40*/("""
						"""),format.raw/*21.7*/("""<option value=""""),_display_(/*21.23*/list_camp/*21.32*/.id),format.raw/*21.35*/("""">"""),_display_(/*21.38*/list_camp/*21.47*/.campaignType),format.raw/*21.60*/("""</option> """)))}),format.raw/*21.71*/("""
					"""),format.raw/*22.6*/("""</select>
				</div>
			</div>


			<div class="col-md-3 campaignTypePSF" style="display: none;">
				<label>Select PSF Type :</label> <select id="campaignNamePSF"
					name="campaignNamePSF" class="form-control">
					<option value='0'>--Select--</option> """),_display_(/*30.45*/for(listcamp <-
					campaignListPSF) yield /*31.22*/{_display_(Seq[Any](format.raw/*31.23*/("""
					"""),format.raw/*32.6*/("""<option value=""""),_display_(/*32.22*/listcamp/*32.30*/.id),format.raw/*32.33*/("""">"""),_display_(/*32.36*/listcamp/*32.44*/.campaignName),format.raw/*32.57*/("""</option> """)))}),format.raw/*32.68*/("""
				"""),format.raw/*33.5*/("""</select>
			</div>
			<div class="col-md-3 selectcampaign" style="display: none">
				<div class="form-group">
					<label for="">Select Campaign Name</label> <select
						class="form-control" id="campaignName" name="campaignName">
						<option value='0'>--Select--</option> """),_display_(/*39.46*/for(list_camp <-
						campaignList) yield /*40.20*/{_display_(Seq[Any](format.raw/*40.21*/("""
						"""),format.raw/*41.7*/("""<option value=""""),_display_(/*41.23*/list_camp/*41.32*/.id),format.raw/*41.35*/("""">"""),_display_(/*41.38*/list_camp/*41.47*/.campaignName),format.raw/*41.60*/("""</option> """)))}),format.raw/*41.71*/("""


					"""),format.raw/*44.6*/("""</select>

				</div>
			</div>
			
			<div class="col-sm-3 fromDiv" >
				<div class="form-group">
					<label>From Date :</label> <input type="text-primary"
						class="datepicker form-control" placeholder="Enter From Date" id="singleData" name="singleData"
						readonly>
				</div>
			</div>


			<div class="col-sm-3 toDiv" >
				<div class="form-group">
					<label>To Date :</label> <input type="text-primary"
						class="datepicker form-control" placeholder="Enter To Date" id="tranferData"
						name="tranferData" readonly>
				</div>
			</div>		
			
			

	
		
		<div class="col-sm-3 model" style="display: none;">
				<div class="form-group">
					<label>Model:</label>
					
					<select	class="form-control" id="model"
						name="model">
					<option value="">SELECT</option>
					<option value="1">Commercial</option>
					<option value="0">Non-Commercial</option>
				</select>
				
				</div>
			</div>

			<div class="col-sm-3 custDiv" >
				<div class="form-group">
					<label>Customer Category :</label> <select class="form-control"
						id="cust_category" name="cust_category">
						<option value="All">All</option>
						<option value="A+">A+</option>
						<option value="B+">B+</option>

					</select>
				</div>
			</div>

			<div class="col-sm-3 servDiv" >
				<div class="form-group" >
					<label>Service Type :</label> <select class="form-control"
						id="service_type" name="service_type">
						<option value="All">All</option> """),_display_(/*100.41*/for(servTyp <- servicetypes) yield /*100.69*/{_display_(Seq[Any](format.raw/*100.70*/("""

						"""),format.raw/*102.7*/("""<option value=""""),_display_(/*102.23*/servTyp/*102.30*/.getServiceTypeName()),format.raw/*102.51*/("""">"""),_display_(/*102.54*/servTyp/*102.61*/.getServiceTypeName()),format.raw/*102.82*/("""</option>

						""")))}),format.raw/*104.8*/("""
					"""),format.raw/*105.6*/("""</select>
				</div>
			</div>


			<div class="col-md-3 locationSelect">

				<label for="cityName">Select City</label> <select
					class="form-control" id="city" name="cityName"
					onchange="ajaxCallToLoadWorkShopByCity();">
					<option value='select'>--Select City--</option>
					"""),_display_(/*116.7*/for(location_list<-locationList) yield /*116.39*/{_display_(Seq[Any](format.raw/*116.40*/("""
					"""),format.raw/*117.6*/("""<option value=""""),_display_(/*117.22*/location_list/*117.35*/.getName()),format.raw/*117.45*/("""">"""),_display_(/*117.48*/location_list/*117.61*/.getName()),format.raw/*117.71*/("""</option>
					""")))}),format.raw/*118.7*/("""
				"""),format.raw/*119.5*/("""</select>


			</div>
			
			</div>
			<div class="row">
			
			<div class="col-md-3 workshopSel">

				<label for="workshopId">Select Workshop</label> 
				<select	class="form-control" id="workshop" name="workshopId">
					<option value=""></option>
				</select>

			</div>

			<div class="col-sm-3 viewAssDiv">
				<label></label> 
					<button type="submit" id="" name="selectedFile" class="btn btn-sm btn-primary viewAssignCalls btn-block" value="viewAssignCalls">View
						Calls</button>

				
			</div>
			
			<div class="col-md-3 selectcre" style="display: none;">
			<label for="data">Select CRE's :</label> 
			<div class="form-group">
				
				<select multiple="multiple" id="data[]"
					name="data[]" class="selectpicker form-control" required="required">
					"""),_display_(/*150.7*/for(data1 <- allCRE) yield /*150.27*/{_display_(Seq[Any](format.raw/*150.28*/("""
					"""),format.raw/*151.6*/("""<option value=""""),_display_(/*151.22*/data1),format.raw/*151.27*/("""">"""),_display_(/*151.30*/data1),format.raw/*151.35*/("""</option> 
					""")))}),format.raw/*152.7*/("""

				"""),format.raw/*154.5*/("""</select>
				</div>
		
			</div>


			<div class="col-sm-3 assgnBtn" style="display: none;">
				
					<label></label>
					<button type="submit" class="btn btn-sm btn-primary assignCall btn-block"
						id="" name="selectedFile"
						value="assignCalls">Assign Calls</button>

				
			</div>

		</div>
	</div>


</div>


<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-info">
			<div class="panel-heading">List Of Calls To Assign</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="dataTable_wrapper">
					<table class="table table-striped table-bordered table-hover"
						id="assignedTable">
						<thead>
							<tr>

								<th>Customer Name</th>
								<th>Mobile Number</th>
								<th>Vehicle RegNo.</th>
								<th>Model</th>
								<th>Variant</th>
								<th>Next Service Date</th>


							</tr>
						</thead>
						<tbody>

						</tbody>
					</table>
				</div>
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>

""")))}),format.raw/*212.2*/("""


"""),format.raw/*215.1*/("""<script>
        $(document).ready(function () """),format.raw/*216.39*/("""{"""),format.raw/*216.40*/("""


        	"""),format.raw/*219.10*/("""$('.viewAssignCalls').click(function()"""),format.raw/*219.48*/("""{"""),format.raw/*219.49*/("""
            	
                """),format.raw/*221.17*/("""var city = $("#city").val();
                var workshop = $("#workshop").val();
                var fromDate = $("#singleData").val();
                var toDate = $("#tranferData").val();
                var servicetype = $("#service_type").val();
                var customerCatgy = $("#cust_category").val();
                var campaignTypeIs = $("#campaignTypeData").val();

                var campName = $("#campaignName").val();
                var psfName = $("#campaignNamePSF").val();
                var modelname = $("#model").val();
               

                if (fromDate != ''  && toDate != '' && city != 'select' && workshop != 'select') """),format.raw/*234.98*/("""{"""),format.raw/*234.99*/("""

                	"""),format.raw/*236.18*/("""$.blockUI();

                     var urlLink = "/CREManager/assignedCallsList"
                    	    
                    	    $('#assignedTable').dataTable( """),format.raw/*240.57*/("""{"""),format.raw/*240.58*/("""
                    	     """),format.raw/*241.27*/(""""bDestroy": true,
                    	     "processing": true,
                    	     "serverSide": true,
                    	     "scrollY": 300,
                    	     "paging": true,
                    	     "searching": false,
                    	     "ordering":false,
                    	     "ajax": """),format.raw/*248.35*/("""{"""),format.raw/*248.36*/("""
                    		        """),format.raw/*249.31*/("""'type': 'POST',
                    		        'url': urlLink,
                    		        'data': """),format.raw/*251.39*/("""{"""),format.raw/*251.40*/("""
                    		        	"""),format.raw/*252.32*/("""city: ''+city,
                    		        	workshop:''+workshop,
                    		        	fromDate:''+fromDate,
                    		        	toDate:''+toDate,
                    		        	customerCatgy:''+customerCatgy,
                    		        	servicetype:''+servicetype,
                    		        	campaignTypeIs:''+campaignTypeIs,
                    		        	campName:''+campName,
                    		        	psfName:''+psfName,
                    		        	modelname:''+modelname
                    		        	
                    		        	
                    		        """),format.raw/*264.31*/("""}"""),format.raw/*264.32*/("""
                        	        
                    	     """),format.raw/*266.27*/("""}"""),format.raw/*266.28*/("""
                    	 """),format.raw/*267.23*/("""}"""),format.raw/*267.24*/(""" """),format.raw/*267.25*/(""");
                     $.unblockUI();
                     $('.assgnBtn').css("display","block");
                     $('.selectcre').css("display","block");
                     
                    
                    return true;
                """),format.raw/*274.17*/("""}"""),format.raw/*274.18*/(""" """),format.raw/*274.19*/("""else """),format.raw/*274.24*/("""{"""),format.raw/*274.25*/("""
                    
                       """),format.raw/*276.24*/("""if (city == 'select' || city == '') """),format.raw/*276.60*/("""{"""),format.raw/*276.61*/("""

                        """),format.raw/*278.25*/("""Lobibox.notify('warning', """),format.raw/*278.51*/("""{"""),format.raw/*278.52*/("""
                            """),format.raw/*279.29*/("""msg: "Please select the city"
                        """),format.raw/*280.25*/("""}"""),format.raw/*280.26*/(""");
                        return false;

                    """),format.raw/*283.21*/("""}"""),format.raw/*283.22*/("""
                    """),format.raw/*284.21*/("""if (workshop == 'select' || workshop == '') """),format.raw/*284.65*/("""{"""),format.raw/*284.66*/("""

                        """),format.raw/*286.25*/("""Lobibox.notify('warning', """),format.raw/*286.51*/("""{"""),format.raw/*286.52*/("""
                            """),format.raw/*287.29*/("""msg: "Please select the workshop"
                        """),format.raw/*288.25*/("""}"""),format.raw/*288.26*/(""");
                        return false;

                    """),format.raw/*291.21*/("""}"""),format.raw/*291.22*/("""


                    """),format.raw/*294.21*/("""if (fromDate == '') """),format.raw/*294.41*/("""{"""),format.raw/*294.42*/("""

                        """),format.raw/*296.25*/("""Lobibox.notify('warning', """),format.raw/*296.51*/("""{"""),format.raw/*296.52*/("""
                            """),format.raw/*297.29*/("""msg: "Please enter the start date"
                        """),format.raw/*298.25*/("""}"""),format.raw/*298.26*/(""");
                        return false;

                    """),format.raw/*301.21*/("""}"""),format.raw/*301.22*/("""
                    """),format.raw/*302.21*/("""if (toDate == '') """),format.raw/*302.39*/("""{"""),format.raw/*302.40*/("""

                        """),format.raw/*304.25*/("""Lobibox.notify('warning', """),format.raw/*304.51*/("""{"""),format.raw/*304.52*/("""
                            """),format.raw/*305.29*/("""msg: "Please enter the End date"
                        """),format.raw/*306.25*/("""}"""),format.raw/*306.26*/(""");
                        return false;

                    """),format.raw/*309.21*/("""}"""),format.raw/*309.22*/("""                    
                 
                """),format.raw/*311.17*/("""}"""),format.raw/*311.18*/("""
            """),format.raw/*312.13*/("""}"""),format.raw/*312.14*/(""");


        	$('.assignCall').click(function()"""),format.raw/*315.43*/("""{"""),format.raw/*315.44*/("""

        		 """),format.raw/*317.12*/("""$.blockUI();
            	

        		 var city = $("#city").val();
                 var workshop = $("#workshop").val();
                 var fromDate = $("#singleData").val();
                 var toDate = $("#tranferData").val();
                 var servicetype = $("#service_type").val();
                 var customerCatgy = $("#cust_category").val();
                 var campaignTypeIs = $("#campaignTypeData").val();

                 var campName = $("#campaignName").val();
                 var psfName = $("#campaignNamePSF").val();
                 var modelname =$("#model").val();
                 var selected_cres;
             	var x = document.getElementById("data[]");
             	var j = 0;
             	for (var i = 0; i < x.options.length; i++) """),format.raw/*334.58*/("""{"""),format.raw/*334.59*/("""
             		"""),format.raw/*335.16*/("""if (x.options[i].selected == true) """),format.raw/*335.51*/("""{"""),format.raw/*335.52*/("""
             			"""),format.raw/*336.17*/("""if (j == 0)
             				selected_cres = x.options[i].value;
             			else
             				selected_cres = selected_cres + ","
             						+ x.options[i].value;

             			//console.log(selected_cres);
             			j++;
             		"""),format.raw/*344.16*/("""}"""),format.raw/*344.17*/("""
             	"""),format.raw/*345.15*/("""}"""),format.raw/*345.16*/("""


             	"""),format.raw/*348.15*/("""var urlLink = "/CREManager/assignedCallsListToUser"
            	    
             		$.ajax("""),format.raw/*350.23*/("""{"""),format.raw/*350.24*/("""
            	        """),format.raw/*351.22*/("""type: 'POST',	       
            	        url: urlLink,
            	        data: """),format.raw/*353.28*/("""{"""),format.raw/*353.29*/("""

            	        	"""),format.raw/*355.23*/("""city: ''+city,
        		        	workshop:''+workshop,
        		        	fromDate:''+fromDate,
        		        	toDate:''+toDate,
        		        	customerCatgy:''+customerCatgy,
        		        	servicetype:''+servicetype,
        		        	campaignTypeIs:''+campaignTypeIs,
        		        	campName:''+campName,
        		        	psfName:''+psfName,            	        	
            	        	selected_cres:''+selected_cres,            	        	
            	        	modelname:''+modelname
            	        	
            	        """),format.raw/*367.22*/("""}"""),format.raw/*367.23*/(""",
            	        success: function (json) """),format.raw/*368.47*/("""{"""),format.raw/*368.48*/("""
            	        	   """),format.raw/*369.26*/("""$.unblockUI();
            	        	window.location='assignmentFilterPage';
            	        """),format.raw/*371.22*/("""}"""),format.raw/*371.23*/(""",
            	        error: function () """),format.raw/*372.41*/("""{"""),format.raw/*372.42*/("""
            	        	
            	        	
            	        """),format.raw/*375.22*/("""}"""),format.raw/*375.23*/("""
            	       
            	    """),format.raw/*377.18*/("""}"""),format.raw/*377.19*/(""");
             	

        """),format.raw/*380.9*/("""}"""),format.raw/*380.10*/(""");

        """),format.raw/*382.9*/("""}"""),format.raw/*382.10*/(""");

    </script>
"""))
      }
    }
  }

  def render(allCRE:List[String],dealerName:String,user:String,campaign_list:List[Campaign],locationList:List[Location],campaignList:List[Campaign],campaignListPSF:List[Campaign],servicetypes:List[ServiceTypes]): play.twirl.api.HtmlFormat.Appendable = apply(allCRE,dealerName,user,campaign_list,locationList,campaignList,campaignListPSF,servicetypes)

  def f:((List[String],String,String,List[Campaign],List[Location],List[Campaign],List[Campaign],List[ServiceTypes]) => play.twirl.api.HtmlFormat.Appendable) = (allCRE,dealerName,user,campaign_list,locationList,campaignList,campaignListPSF,servicetypes) => apply(allCRE,dealerName,user,campaign_list,locationList,campaignList,campaignListPSF,servicetypes)

  def ref: this.type = this

}


}

/**/
object assignmentFilterListPage extends assignmentFilterListPage_Scope0.assignmentFilterListPage
              /*
                  -- GENERATED --
                  DATE: Thu Dec 28 15:40:26 IST 2017
                  SOURCE: D:/CRMNEW/28th Dec/crm/app/views/assignmentFilterListPage.scala.html
                  HASH: ace533aa2101ef2584badc829bd8096892067ad7
                  MATRIX: 882->3|1177->202|1207->207|1265->257|1304->259|1336->265|1786->689|1833->720|1872->721|1907->729|1950->745|1968->754|1992->757|2022->760|2040->769|2074->782|2116->793|2150->800|2443->1066|2497->1104|2536->1105|2570->1112|2613->1128|2630->1136|2654->1139|2684->1142|2701->1150|2735->1163|2777->1174|2810->1180|3122->1465|3175->1502|3214->1503|3249->1511|3292->1527|3310->1536|3334->1539|3364->1542|3382->1551|3416->1564|3458->1575|3496->1586|5049->3111|5094->3139|5134->3140|5172->3150|5216->3166|5233->3173|5276->3194|5307->3197|5324->3204|5367->3225|5418->3245|5453->3252|5780->3552|5829->3584|5869->3585|5904->3592|5948->3608|5971->3621|6003->3631|6034->3634|6057->3647|6089->3657|6137->3674|6171->3680|7005->4487|7042->4507|7082->4508|7117->4515|7161->4531|7188->4536|7219->4539|7246->4544|7295->4562|7331->4570|8456->5664|8490->5670|8567->5718|8597->5719|8641->5734|8708->5772|8738->5773|8800->5806|9505->6482|9535->6483|9585->6504|9781->6671|9811->6672|9868->6700|10222->7025|10252->7026|10313->7058|10444->7160|10474->7161|10536->7194|11186->7815|11216->7816|11308->7879|11338->7880|11391->7904|11421->7905|11451->7906|11739->8165|11769->8166|11799->8167|11833->8172|11863->8173|11939->8220|12004->8256|12034->8257|12091->8285|12146->8311|12176->8312|12235->8342|12319->8397|12349->8398|12443->8463|12473->8464|12524->8486|12597->8530|12627->8531|12684->8559|12739->8585|12769->8586|12828->8616|12916->8675|12946->8676|13040->8741|13070->8742|13125->8768|13174->8788|13204->8789|13261->8817|13316->8843|13346->8844|13405->8874|13494->8934|13524->8935|13618->9000|13648->9001|13699->9023|13746->9041|13776->9042|13833->9070|13888->9096|13918->9097|13977->9127|14064->9185|14094->9186|14188->9251|14218->9252|14304->9309|14334->9310|14377->9324|14407->9325|14486->9375|14516->9376|14560->9391|15377->10179|15407->10180|15453->10197|15517->10232|15547->10233|15594->10251|15894->10522|15924->10523|15969->10539|15999->10540|16048->10560|16171->10654|16201->10655|16253->10678|16368->10764|16398->10765|16453->10791|17046->11355|17076->11356|17154->11405|17184->11406|17240->11433|17369->11533|17399->11534|17471->11577|17501->11578|17601->11649|17631->11650|17701->11691|17731->11692|17789->11722|17819->11723|17861->11737|17891->11738
                  LINES: 27->2|32->2|34->4|34->4|34->4|37->7|50->20|50->20|50->20|51->21|51->21|51->21|51->21|51->21|51->21|51->21|51->21|52->22|60->30|61->31|61->31|62->32|62->32|62->32|62->32|62->32|62->32|62->32|62->32|63->33|69->39|70->40|70->40|71->41|71->41|71->41|71->41|71->41|71->41|71->41|71->41|74->44|130->100|130->100|130->100|132->102|132->102|132->102|132->102|132->102|132->102|132->102|134->104|135->105|146->116|146->116|146->116|147->117|147->117|147->117|147->117|147->117|147->117|147->117|148->118|149->119|180->150|180->150|180->150|181->151|181->151|181->151|181->151|181->151|182->152|184->154|242->212|245->215|246->216|246->216|249->219|249->219|249->219|251->221|264->234|264->234|266->236|270->240|270->240|271->241|278->248|278->248|279->249|281->251|281->251|282->252|294->264|294->264|296->266|296->266|297->267|297->267|297->267|304->274|304->274|304->274|304->274|304->274|306->276|306->276|306->276|308->278|308->278|308->278|309->279|310->280|310->280|313->283|313->283|314->284|314->284|314->284|316->286|316->286|316->286|317->287|318->288|318->288|321->291|321->291|324->294|324->294|324->294|326->296|326->296|326->296|327->297|328->298|328->298|331->301|331->301|332->302|332->302|332->302|334->304|334->304|334->304|335->305|336->306|336->306|339->309|339->309|341->311|341->311|342->312|342->312|345->315|345->315|347->317|364->334|364->334|365->335|365->335|365->335|366->336|374->344|374->344|375->345|375->345|378->348|380->350|380->350|381->351|383->353|383->353|385->355|397->367|397->367|398->368|398->368|399->369|401->371|401->371|402->372|402->372|405->375|405->375|407->377|407->377|410->380|410->380|412->382|412->382
                  -- GENERATED --
              */
          