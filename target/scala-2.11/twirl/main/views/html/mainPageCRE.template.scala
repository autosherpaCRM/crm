
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object mainPageCRE_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class mainPageCRE extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template6[String,String,String,String,String,Html,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(title: String,userName : String,dealerName:String,dealercode:String,OEM:String)(content: Html):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.97*/("""

"""),format.raw/*3.1*/("""<!DOCTYPE html>
<html>
<head>



<title>"""),_display_(/*9.9*/title),format.raw/*9.14*/("""</title>
<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
<meta http-equiv="cache-control" content="max-age=0" >
<meta http-equiv="expires" content="0" >
<meta http-equiv="pragma" content="no-cache" >
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

<link rel="shortcut icon" type="image/png" href=""""),_display_(/*16.51*/routes/*16.57*/.Assets.at("images/favicon1.ico")),format.raw/*16.90*/("""">
<!--<script src=""""),_display_(/*17.19*/routes/*17.25*/.Assets.at("javascripts/jquery-1.7.1.min.js")),format.raw/*17.70*/("""" type="text/javascript"></script>-->
<script src=""""),_display_(/*18.15*/routes/*18.21*/.Assets.at("javascripts/jquery.js")),format.raw/*18.56*/("""" type="text/javascript"></script> 
<link rel="stylesheet" href=""""),_display_(/*19.31*/routes/*19.37*/.Assets.at("css/jquery-ui.min.css")),format.raw/*19.72*/("""">
<link rel="stylesheet" href=""""),_display_(/*20.31*/routes/*20.37*/.Assets.at("css/bootstrap.min.css")),format.raw/*20.72*/("""">
<link rel="stylesheet" href=""""),_display_(/*21.31*/routes/*21.37*/.Assets.at("css/skin-red.css")),format.raw/*21.67*/("""">
<link rel="stylesheet" href=""""),_display_(/*22.31*/routes/*22.37*/.Assets.at("css/AdminLTE.min.css")),format.raw/*22.71*/("""">
<link rel="stylesheet" href=""""),_display_(/*23.31*/routes/*23.37*/.Assets.at("css/dataTables.bootstrap.css")),format.raw/*23.79*/("""">
<link rel="stylesheet" href=""""),_display_(/*24.31*/routes/*24.37*/.Assets.at("css/dataTables.responsive.css")),format.raw/*24.80*/("""">
<link rel="stylesheet" href=""""),_display_(/*25.31*/routes/*25.37*/.Assets.at("css/font-awesome.min.css")),format.raw/*25.75*/("""">
<link rel="stylesheet" href=""""),_display_(/*26.31*/routes/*26.37*/.Assets.at("css/CutomizedNewIconCSS.css")),format.raw/*26.78*/("""">
<link rel="stylesheet" href=""""),_display_(/*27.31*/routes/*27.37*/.Assets.at("css/ionicons.min.css")),format.raw/*27.71*/("""">
<link rel="stylesheet" href=""""),_display_(/*28.31*/routes/*28.37*/.Assets.at("css/animate.css")),format.raw/*28.66*/("""">
<link rel="stylesheet" href=""""),_display_(/*29.31*/routes/*29.37*/.Assets.at("css/lobibox.min.css")),format.raw/*29.70*/("""">
<link rel="stylesheet" href=""""),_display_(/*30.31*/routes/*30.37*/.Assets.at("css/Wyz_css.css")),format.raw/*30.66*/("""">
<link rel="stylesheet" href=""""),_display_(/*31.31*/routes/*31.37*/.Assets.at("css/multiselect.css")),format.raw/*31.70*/("""">
<link rel="stylesheet" href=""""),_display_(/*32.31*/routes/*32.37*/.Assets.at("css/bootstrap-datepicker.min.css")),format.raw/*32.83*/("""">
<!-- <link rel="stylesheet" href=""""),_display_(/*33.36*/routes/*33.42*/.Assets.at("css/sidenavi-left.css")),format.raw/*33.77*/(""""> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.2.3/jquery-confirm.min.css">
<link rel="stylesheet" href=""""),_display_(/*35.31*/routes/*35.37*/.Assets.at("css/jquery.fileupload.css")),format.raw/*35.76*/("""">



<script type="text/javascript">
        function noBack()
         """),format.raw/*41.10*/("""{"""),format.raw/*41.11*/("""
             """),format.raw/*42.14*/("""window.history.forward();
         """),format.raw/*43.10*/("""}"""),format.raw/*43.11*/("""
        """),format.raw/*44.9*/("""noBack();
        window.onload = noBack;
       window.onpageshow = function(evt) """),format.raw/*46.42*/("""{"""),format.raw/*46.43*/(""" """),format.raw/*46.44*/("""if (evt.persisted) noBack() """),format.raw/*46.72*/("""}"""),format.raw/*46.73*/(""" 
        """),format.raw/*47.9*/("""window.onunload = function() """),format.raw/*47.38*/("""{"""),format.raw/*47.39*/(""" """),format.raw/*47.40*/("""void (0) """),format.raw/*47.49*/("""}"""),format.raw/*47.50*/("""
    """),format.raw/*48.5*/("""</script>
    
</head>


<body class="hold-transition skin-red sidebar-mini sidebar-collapse">
<div class="wrapper" id="CREPage"> 
<div id="throbber" style="display:none;">
<img src=""""),_display_(/*56.12*/routes/*56.18*/.Assets.at("images/loading_black.gif")),format.raw/*56.56*/(""""/>
</div>
<!-- Main Header -->
<header class="main-header"> 

<!-- Logo --> 
<a href="/CRE/getDispositionPageOfTab" class="logo"> <span class="logo-mini"><b>CRM</b></span> <span> <img src=""""),_display_(/*62.114*/routes/*62.120*/.Assets.at("images/dealercustomerconnectlogoblack2.png")),format.raw/*62.176*/("""" width="200" height="44" style="margin-left: -20px;"/></span> </a> 

<!-- Header Navbar -->
<nav class="navbar navbar-static-top" role="navigation"> 
  <!-- Sidebar toggle button--> 
  <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button"> <span class="sr-only">Toggle navigation</span> </a>
  
  <div class="navbar-custom-menu">
 <ul class="nav navbar-nav">
 
 <li class="dropdown" data-toggle="tooltip" data-placement="bottom" title="follow Up Count">
<a class="dropdown-toggle count-info" href="/CRE/listTodaysFollowUp" >
<i class="fa fa-bell fa-fw"></i><span class="label label-success" id="followUpCountOfToday"></span>
</a>
 </li>
 <li style="text-transform:uppercase;"><a href="#"><i class="fa fa-user-circle"></i><span class="hidden-xs hidden-sm">"""),_display_(/*77.120*/OEM),format.raw/*77.123*/("""<span></a></li> 
 <li style="text-transform:uppercase;"><a href="#"><i class="fa fa-user fa-fw"></i><span class="hidden-xs hidden-sm">"""),_display_(/*78.119*/userName),format.raw/*78.127*/("""<span></a></li> 
 <li style="text-transform:uppercase;"><a href="#"><i class="fa fa-user-circle-o fa-fw"></i><span class="hidden-xs hidden-sm">"""),_display_(/*79.128*/dealerName),format.raw/*79.138*/("""<span></a></li>
  <li><a href="/CRE/changepasswordcre"><i class="fa fa-unlock-alt fa-fw"></i><span class="hidden-xs hidden-sm">CHANGE PASSWORD<span></a></li>      
<li><a href="/logoutUser" onclick="noBack()"><i class="fa fa-sign-out fa-fw"></i><span class="hidden-xs hidden-sm">LOGOUT<span></a></li>              

 
</ul>
  </div>
</nav>
</header>
<aside class="main-sidebar">
<section class="sidebar">
  <ul class="sidebar-menu">
<li class="treeview"> <a href="#"><i class="fa icon-set-alarm"></i> <span>Service Reminder<span class="fa arrow"></span></span> <i class="fa fa-angle-left pull-right"></i></a>
  <ul class="treeview-menu">
  <!-- <li><a href="/"""),_display_(/*93.23*/dealercode),format.raw/*93.33*/("""/CRE/viewcallList"><i class="fa fa-phone-square"></i>View Call Log</a></li> -->
 
   <li><a href="/CRE/getDispositionPageOfTab"><i class="fa fa-phone-square"></i>Call Log</a></li>

  
 </ul>
 
</li>

 <li class="treeview"> <a href="#"><i class="fa fa-users"></i> <span>Customer<span class="fa arrow"></span></span> <i class="fa fa-angle-left pull-right"></i></a>
  <ul class="treeview-menu">
  <li><a href="/addCustomer"><i class="fa fa-plus-square"></i>Add Customer</a></li>
   <li><a href="/searchByCustomer"><i class="fa fa-search"></i>Search Customer</a></li>
   </ul>
 
</li>
  



 <li class="treeview"> <a href="#"><i class="fa fa-list-alt"></i> <span>Complaint<span class="fa arrow"></span></span> <i class="fa fa-angle-left pull-right"></i></a>
  <ul class="treeview-menu">
  <li><a href="/complaints"><i class="fa fa-pencil-square-o"></i>Add Complaint</a></li>
  <li><a href="/complaintResolution"><i class="fa fa-check-square"></i>Resolve Complaint</a></li>
  <li><a href="/viewAllComplaints"><i class="fa fa-list"></i>View All Complaint</a></li>
</ul>
 
</li>
 <li class="treeview"> <a href="#"><i class="fa icon-girl-talking"></i> <span>PSF<span class="fa arrow"></span></span> <i class="fa fa-angle-left pull-right"></i></a>
 
 """),_display_(/*123.3*/{if(OEM=="HYUNDAI"){
 	
 	<ul class="treeview-menu" id="psfdata">
 <li data-id="psf3rdday"><a href="/CRE/PSF3RDList"><i class="fa fa-phone"></i>PSF 3RD Day</a></li>
 <li data-id="PSF15thDay"><a href="/CRE/PSF15List"><i class="fa fa-phone"></i>PSF 15th Day</a></li>
 
 
</ul>
 
 
}else if(OEM=="MARUTHI"){

 <ul class="treeview-menu" id="psfdata">
 <li data-id="PSF6thDay"><a href="/CRE/PSFList"><i class="fa fa-phone"></i>PSF 6th Day</a></li>
 <li data-id="PSF15thDay"><a href="/CRE/PSF15List"><i class="fa fa-phone"></i>PSF 15th Day</a></li>
 <li data-id="PSF30thDay"><a href="/CRE/PSF30List"><i class="fa fa-phone"></i>PSF 30th Day</a></li>
 
</ul>

}else{

 <ul class="treeview-menu" id="psfdata">
 <li data-id="PSF6thDay"><a href="/CRE/PSFNextDayList"><i class="fa fa-phone"></i>PSF Next Day</a></li>
 <li data-id="PSF15thDay"><a href="/CRE/PSF4thDayList"><i class="fa fa-phone"></i>PSF 4th Day</a></li>
 
 
</ul>

}}),format.raw/*151.3*/("""
 """),format.raw/*152.2*/("""</li>
 
  <li class="treeview"> <a href="#"><i class="fa icon-insurance2"></i> <span>Insurance<span class="fa arrow"></span></span> <i class="fa fa-angle-left pull-right"></i></a>
  <ul class="treeview-menu">
 
  <li><a href="/CRE/insuranceAssignedList"><i class="fa fa-phone"></i>Call Log</a></li>
  
</ul>
 
</li>

<li class="treeview"> <a href="#"><i class="fa icon-sms5"></i> <span>Bulk SMS<span class="fa arrow"></span></span> <i class="fa fa-angle-left pull-right"></i></a>
  <ul class="treeview-menu"> 
  
   <li><a href="/SMSTemplateBySuperAdmin"><i class="fa icon-sms3"></i>Send Bulk SMS</a></li>
 	 </ul>
</li>
 
 
 
  </ul>      <!-- /.sidebar-menu --> 
</section>   <!-- /.sidebar --> 
</aside>

<div id="divfupNote" style="display:none">
<table id="tblfubNote" style="display:none"></table>
</div>
<div id="divforfollowuptemp" style="display:none">
<table id="tblforfollowuptemp" style="display:none"></table>
</div>
<div id="divforfollowuptempinsurance" style="display:none">
<table id="tblforfollowuptempinsurance" style="display:none"></table>
</div>


<div class="content-wrapper">
<section class="content">
   
  """),_display_(/*190.4*/content),format.raw/*190.11*/("""
  """),format.raw/*191.3*/("""</section>
</div>
<!--    <footer class="main-footer">
<div class="pull-right hidden-xs"> ADCC </div>
<strong>WyzMindz &copy; 2016 <a href="http://www.wyzmindz.com/">Company</a>.</strong> All rights reserved.
 </footer> -->
</div>
<script src=""""),_display_(/*198.15*/routes/*198.21*/.Assets.at("javascripts/jquery.js")),format.raw/*198.56*/("""" type="text/javascript"></script> 
<script src=""""),_display_(/*199.15*/routes/*199.21*/.Assets.at("javascripts/jquery-ui.min.js")),format.raw/*199.63*/("""" type="text/javascript"></script> 
<script src=""""),_display_(/*200.15*/routes/*200.21*/.Assets.at("javascripts/bootstrap.min.js")),format.raw/*200.63*/("""" type="text/javascript"></script>  
<script src=""""),_display_(/*201.15*/routes/*201.21*/.Assets.at("javascripts/app.js")),format.raw/*201.53*/("""" type="text/javascript"></script> 
<script src=""""),_display_(/*202.15*/routes/*202.21*/.Assets.at("javascripts/jquery.blockUI.js")),format.raw/*202.64*/("""" type="text/javascript"></script>
<script src=""""),_display_(/*203.15*/routes/*203.21*/.Assets.at("javascripts/multiselect.js")),format.raw/*203.61*/("""" type="text/javascript"></script>

<script src=""""),_display_(/*205.15*/routes/*205.21*/.Assets.at("javascripts/Chart.min.js")),format.raw/*205.59*/("""" type="text/javascript"></script> 

<script src=""""),_display_(/*207.15*/routes/*207.21*/.Assets.at("javascripts/fastclick.min.js")),format.raw/*207.63*/("""" type="text/javascript"></script>

<script src=""""),_display_(/*209.15*/routes/*209.21*/.Assets.at("javascripts/lobibox.js")),format.raw/*209.57*/("""" type="text/javascript"></script>
<script src=""""),_display_(/*210.15*/routes/*210.21*/.Assets.at("javascripts/callDisposition.js")),format.raw/*210.65*/("""" type="text/javascript"></script>
<script src=""""),_display_(/*211.15*/routes/*211.21*/.Assets.at("javascripts/ajaxCallScripts.js")),format.raw/*211.65*/("""" type="text/javascript"></script>
<script src=""""),_display_(/*212.15*/routes/*212.21*/.Assets.at("javascripts/Wyz_Scripts.js")),format.raw/*212.61*/("""" type="text/javascript"></script> 
<script src=""""),_display_(/*213.15*/routes/*213.21*/.Assets.at("javascripts/datatables.min.js")),format.raw/*213.64*/("""" type="text/javascript"></script>
<script src=""""),_display_(/*214.15*/routes/*214.21*/.Assets.at("javascripts/servicesBookedScripts.js")),format.raw/*214.71*/("""" type="text/javascript"></script>
<script src=""""),_display_(/*215.15*/routes/*215.21*/.Assets.at("javascripts/jquery.ui.position.min.js")),format.raw/*215.72*/("""" type="text/javascript"></script> 
<script src=""""),_display_(/*216.15*/routes/*216.21*/.Assets.at("javascripts/jquery.ui.timepicker.js")),format.raw/*216.70*/("""" type="text/javascript"></script> 
<script src=""""),_display_(/*217.15*/routes/*217.21*/.Assets.at("javascripts/jquery.bootstrap.wizard.js")),format.raw/*217.73*/("""" type="text/javascript"></script> 
<script src=""""),_display_(/*218.15*/routes/*218.21*/.Assets.at("javascripts/wizard.js")),format.raw/*218.56*/("""" type="text/javascript"></script> 

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.2.3/jquery-confirm.min.js"></script>


</body>
</html>"""))
      }
    }
  }

  def render(title:String,userName:String,dealerName:String,dealercode:String,OEM:String,content:Html): play.twirl.api.HtmlFormat.Appendable = apply(title,userName,dealerName,dealercode,OEM)(content)

  def f:((String,String,String,String,String) => (Html) => play.twirl.api.HtmlFormat.Appendable) = (title,userName,dealerName,dealercode,OEM) => (content) => apply(title,userName,dealerName,dealercode,OEM)(content)

  def ref: this.type = this

}


}

/**/
object mainPageCRE extends mainPageCRE_Scope0.mainPageCRE
              /*
                  -- GENERATED --
                  DATE: Thu Dec 28 14:51:59 IST 2017
                  SOURCE: D:/CRMNEW/28th Dec/crm/app/views/mainPageCRE.scala.html
                  HASH: 91e5657ab9286f6a2b6a4de00b24ac476a3ab9ff
                  MATRIX: 790->1|980->96|1010->100|1082->147|1107->152|1501->519|1516->525|1570->558|1619->580|1634->586|1700->631|1780->684|1795->690|1851->725|1945->792|1960->798|2016->833|2077->867|2092->873|2148->908|2209->942|2224->948|2275->978|2336->1012|2351->1018|2406->1052|2467->1086|2482->1092|2545->1134|2606->1168|2621->1174|2685->1217|2746->1251|2761->1257|2820->1295|2881->1329|2896->1335|2958->1376|3019->1410|3034->1416|3089->1450|3150->1484|3165->1490|3215->1519|3276->1553|3291->1559|3345->1592|3406->1626|3421->1632|3471->1661|3532->1695|3547->1701|3601->1734|3662->1768|3677->1774|3744->1820|3810->1859|3825->1865|3881->1900|4061->2053|4076->2059|4136->2098|4243->2177|4272->2178|4315->2193|4379->2229|4408->2230|4445->2240|4558->2325|4587->2326|4616->2327|4672->2355|4701->2356|4739->2367|4796->2396|4825->2397|4854->2398|4891->2407|4920->2408|4953->2414|5172->2606|5187->2612|5246->2650|5471->2847|5487->2853|5565->2909|6384->3700|6409->3703|6573->3839|6603->3847|6776->3992|6808->4002|7509->4676|7540->4686|8840->5959|9810->6908|9841->6911|11038->8081|11067->8088|11099->8092|11379->8344|11395->8350|11452->8385|11531->8436|11547->8442|11611->8484|11690->8535|11706->8541|11770->8583|11850->8635|11866->8641|11920->8673|11999->8724|12015->8730|12080->8773|12158->8823|12174->8829|12236->8869|12316->8921|12332->8927|12392->8965|12473->9018|12489->9024|12553->9066|12633->9118|12649->9124|12707->9160|12785->9210|12801->9216|12867->9260|12945->9310|12961->9316|13027->9360|13105->9410|13121->9416|13183->9456|13262->9507|13278->9513|13343->9556|13421->9606|13437->9612|13509->9662|13587->9712|13603->9718|13676->9769|13755->9820|13771->9826|13842->9875|13921->9926|13937->9932|14011->9984|14090->10035|14106->10041|14163->10076
                  LINES: 27->1|32->1|34->3|40->9|40->9|47->16|47->16|47->16|48->17|48->17|48->17|49->18|49->18|49->18|50->19|50->19|50->19|51->20|51->20|51->20|52->21|52->21|52->21|53->22|53->22|53->22|54->23|54->23|54->23|55->24|55->24|55->24|56->25|56->25|56->25|57->26|57->26|57->26|58->27|58->27|58->27|59->28|59->28|59->28|60->29|60->29|60->29|61->30|61->30|61->30|62->31|62->31|62->31|63->32|63->32|63->32|64->33|64->33|64->33|66->35|66->35|66->35|72->41|72->41|73->42|74->43|74->43|75->44|77->46|77->46|77->46|77->46|77->46|78->47|78->47|78->47|78->47|78->47|78->47|79->48|87->56|87->56|87->56|93->62|93->62|93->62|108->77|108->77|109->78|109->78|110->79|110->79|124->93|124->93|154->123|182->151|183->152|221->190|221->190|222->191|229->198|229->198|229->198|230->199|230->199|230->199|231->200|231->200|231->200|232->201|232->201|232->201|233->202|233->202|233->202|234->203|234->203|234->203|236->205|236->205|236->205|238->207|238->207|238->207|240->209|240->209|240->209|241->210|241->210|241->210|242->211|242->211|242->211|243->212|243->212|243->212|244->213|244->213|244->213|245->214|245->214|245->214|246->215|246->215|246->215|247->216|247->216|247->216|248->217|248->217|248->217|249->218|249->218|249->218
                  -- GENERATED --
              */
          