
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object callSummaryTableOfCREManger_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class callSummaryTableOfCREManger extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template5[Boolean,List[String],String,String,List[CallSummary],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*3.2*/(isDownload:Boolean,ajaxData:List[String],dealerName:String,user:String,getAllCounts:List[CallSummary]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*3.105*/("""

"""),_display_(/*5.2*/mainPageCREManger("AutoSherpaCRM",user,dealerName)/*5.52*/ {_display_(Seq[Any](format.raw/*5.54*/("""

"""),format.raw/*7.1*/("""<input type="hidden" id="indexBox1" value='"""),_display_(/*7.45*/ajaxData),format.raw/*7.53*/("""'>  
    
    
    <div class="panel panel-info">
                        <div class="panel-heading">                        
                         
                         CALL SUMMARY
                         
                          """),_display_(/*15.28*/if(isDownload == true)/*15.50*/{_display_(Seq[Any](format.raw/*15.51*/("""
       """),format.raw/*16.8*/("""<a href="/CREManager/downloadCallHistory" class="btn btn-primary btn-xs pull-right"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Download</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
       """)))}),format.raw/*17.9*/("""
                         
                        """),format.raw/*19.25*/("""</div>
                        
                         <div class="panel-body">
                            <div class="dataTable_wrapper">                            
                            <div style="overflow-x: auto">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example37">
                                    <thead>
                                 <tr> 
                                	                            	
                                    <th>AgentId</th>
                                    <th>Allocated Calls</th>
                                    <th>Pending Calls</th>
                                    <th>Contacts</th>
                                    <th>Service Booked</th>
                                    <th>FollowUp</th>
                                    <th>Service NotRequired</th>
                                    <th>Non Contacts</th>
                                    <th>Ringing NoResponse</th>
                                    <th>Busy</th>
                                    <th>Switched Off</th>
                                    <th>Not Reachable</th>     
                                     <th>Wrong Number</th>                                
                                    
                                    
                                                                                                      
                                    
                                 </tr>
                              </thead>
                                <tbody>
                                    
                                 """),_display_(/*49.35*/for(post <- getAllCounts) yield /*49.60*/ {_display_(Seq[Any](format.raw/*49.62*/("""                                 	
                               """),format.raw/*50.32*/("""<tr>                               
                              	<td>"""),_display_(/*51.37*/post/*51.41*/.agentName),format.raw/*51.51*/("""</td>
                              	<td>"""),_display_(/*52.37*/post/*52.41*/.allocatedCallsCount),format.raw/*52.61*/("""</td>
                              	<td>"""),_display_(/*53.37*/post/*53.41*/.pendingCallsCount),format.raw/*53.59*/("""</td>
                              	<td>"""),_display_(/*54.37*/post/*54.41*/.contactsCallsCount),format.raw/*54.60*/("""</td>
                              	<td>"""),_display_(/*55.37*/post/*55.41*/.serviceBookedCount),format.raw/*55.60*/("""</td>
                              	<td>"""),_display_(/*56.37*/post/*56.41*/.followUpRequiredCount),format.raw/*56.63*/("""</td>
                              	<td>"""),_display_(/*57.37*/post/*57.41*/.serviceNotRequiredCount),format.raw/*57.65*/("""</td>
                              	<td>"""),_display_(/*58.37*/post/*58.41*/.noncontactsCallsCount),format.raw/*58.63*/("""</td>
                              	<td>"""),_display_(/*59.37*/post/*59.41*/.ringingNoResponseCount),format.raw/*59.64*/("""</td>
                              	<td>"""),_display_(/*60.37*/post/*60.41*/.busyCount),format.raw/*60.51*/("""</td>
                              	<td>"""),_display_(/*61.37*/post/*61.41*/.switchedOffCount),format.raw/*61.58*/("""</td>
                              	<td>"""),_display_(/*62.37*/post/*62.41*/.notReachableCount),format.raw/*62.59*/("""</td>
                              	<td>"""),_display_(/*63.37*/post/*63.41*/.wrongNumberCount),format.raw/*63.58*/("""</td>
                              		                                                                     
                         		 </tr>
                                      
                                     """)))}),format.raw/*67.39*/("""
                             
                                   
                                """),format.raw/*70.33*/("""</tbody>
                               
                            </table>
                                </div>
                            </div>
                           
                            
                        </div>
                       
                    </div>
    
  
""")))}),format.raw/*82.2*/("""

"""),format.raw/*84.1*/("""<script type="text/javascript">
window.onload = ajaxRequestDataIndexPageCREMAn;
</script> """))
      }
    }
  }

  def render(isDownload:Boolean,ajaxData:List[String],dealerName:String,user:String,getAllCounts:List[CallSummary]): play.twirl.api.HtmlFormat.Appendable = apply(isDownload,ajaxData,dealerName,user,getAllCounts)

  def f:((Boolean,List[String],String,String,List[CallSummary]) => play.twirl.api.HtmlFormat.Appendable) = (isDownload,ajaxData,dealerName,user,getAllCounts) => apply(isDownload,ajaxData,dealerName,user,getAllCounts)

  def ref: this.type = this

}


}

/**/
object callSummaryTableOfCREManger extends callSummaryTableOfCREManger_Scope0.callSummaryTableOfCREManger
              /*
                  -- GENERATED --
                  DATE: Thu Dec 28 12:24:27 IST 2017
                  SOURCE: D:/CRMNEW/28th Dec/crm/app/views/callSummaryTableOfCREManger.scala.html
                  HASH: 9688cf8e2b85766fcb23de5e73e6c84f7296872f
                  MATRIX: 835->5|1034->108|1064->113|1122->163|1161->165|1191->169|1261->213|1289->221|1567->472|1598->494|1637->495|1673->504|1906->707|1987->760|3734->2480|3775->2505|3815->2507|3910->2574|4010->2647|4023->2651|4054->2661|4124->2704|4137->2708|4178->2728|4248->2771|4261->2775|4300->2793|4370->2836|4383->2840|4423->2859|4493->2902|4506->2906|4546->2925|4616->2968|4629->2972|4672->2994|4742->3037|4755->3041|4800->3065|4870->3108|4883->3112|4926->3134|4996->3177|5009->3181|5053->3204|5123->3247|5136->3251|5167->3261|5237->3304|5250->3308|5288->3325|5358->3368|5371->3372|5410->3390|5480->3433|5493->3437|5531->3454|5785->3677|5915->3779|6257->4091|6288->4095
                  LINES: 27->3|32->3|34->5|34->5|34->5|36->7|36->7|36->7|44->15|44->15|44->15|45->16|46->17|48->19|78->49|78->49|78->49|79->50|80->51|80->51|80->51|81->52|81->52|81->52|82->53|82->53|82->53|83->54|83->54|83->54|84->55|84->55|84->55|85->56|85->56|85->56|86->57|86->57|86->57|87->58|87->58|87->58|88->59|88->59|88->59|89->60|89->60|89->60|90->61|90->61|90->61|91->62|91->62|91->62|92->63|92->63|92->63|96->67|99->70|111->82|113->84
                  -- GENERATED --
              */
          