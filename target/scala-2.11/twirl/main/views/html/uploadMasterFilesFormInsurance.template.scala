
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object uploadMasterFilesFormInsurance_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class uploadMasterFilesFormInsurance extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template7[List[Integer],String,String,List[Campaign],List[Location],String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(resultdata:List[Integer],uploadId:String,uploadType:String,campaignList:List[Campaign],locationList:List[Location],dealerCode:String,userName:String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.152*/("""
"""),_display_(/*2.2*/mainPageCREManger("AutoSherpaCRM",userName,dealerCode)/*2.56*/ {_display_(Seq[Any](format.raw/*2.58*/("""

"""),_display_(/*4.2*/helper/*4.8*/.form(action = routes.UploadExcelController.uploadExcelData(), 'enctype -> "multipart/form-data")/*4.105*/ {_display_(Seq[Any](format.raw/*4.107*/("""


"""),format.raw/*7.1*/("""<input type="hidden" value=""""),_display_(/*7.30*/uploadId),format.raw/*7.38*/("""">


<div class="panel panel-info">
    <div class="panel-heading"><strong>Master Data Upload</strong></div>
    <div class="panel-body">

        <div class="row">
            <div class="col-lg-12">

                <div class="panel panel-primary">
                    <div class="panel-heading"><b>Select file from your computer</b></div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-lg-3">
                                <label>Select File Format:</label>
                                <select name="dataType" id="upload_form_name" class="form-control">
                                     <option value= "Insurance">Insurance</option>

                                </select> 
                            </div>
                            <div class="col-md-3" >
                                <div class="form-group">
                                    <label for="campaignName">Campaign Name</label>
                                    <select name="campaignName" id="campaignName" class="form-control">
                                        <option value='select'>--Select Campaign--</option>
                                        """),_display_(/*34.42*/for(campaign_list<-campaignList) yield /*34.74*/{_display_(Seq[Any](format.raw/*34.75*/("""
                                        """),format.raw/*35.41*/("""<option value=""""),_display_(/*35.57*/campaign_list/*35.70*/.getCampaignName()),format.raw/*35.88*/("""">"""),_display_(/*35.91*/campaign_list/*35.104*/.getCampaignName()),format.raw/*35.122*/("""</option>
                                        """)))}),format.raw/*36.42*/("""
                                """),format.raw/*37.33*/("""</select> 
                                    
                                </div>				 
                            </div>
                            <div class="col-lg-3" style="display:none" >  
                                <label>Sheet Name:</label>                                           
                                <input type="text" class="form-control" name="sheetName" id="sheetName" value="Sheet1">
                            </div>

                            <div class="col-lg-3">
                                <label>&nbsp;</label>  
                                <input type="file" name="uploadfiles" id="uploadfiles" >  
                                <span id="lblError" style="color: red;"></span>
                            </div>

                        </div>
                        <div class="row"> 
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="cityName">Select City</label>
                                    <select class="form-control" id="city" name="name" onchange="ajaxCallToLoadWorkShopByCity();">	
                                        <option value='select'>--Select City--</option>
                                        """),_display_(/*59.42*/for(location_list<-locationList) yield /*59.74*/{_display_(Seq[Any](format.raw/*59.75*/("""
                                        """),format.raw/*60.41*/("""<option value=""""),_display_(/*60.57*/location_list/*60.70*/.getName()),format.raw/*60.80*/("""">"""),_display_(/*60.83*/location_list/*60.96*/.getName()),format.raw/*60.106*/("""</option>
                                        """)))}),format.raw/*61.42*/("""
                                    """),format.raw/*62.37*/("""</select>

                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="workshopId">Select Location</label>
                                    <select class="form-control" id="workshop" name="id" >					  
                                        <option value=""></option>
                                    </select>
                                </div>				 
                            </div>
                            <div class="col-md-3 campaignStartdate">
                                <div class="form-group">
                                    <label for="seldate">Select From Date</label>
                                    <input type="text" class="form-control datepicker"  id="campaignStartdate" name="campaignStartdate" readonly>			  
                                </div>				 
                            </div>
                            <div class="col-md-3 campaignExpiry">
                                <div class="form-group">
                                    <label for="seldateExp">Select Expiry Date</label>
                                    <input type="text" class="form-control datepicker" id="campaignExpiry" name="campaignExpiry" readonly>
                                </div>				 
                            </div>
                        </div>
                        <input type="submit" class="btn btn-primary pull-right upload-submitExcel" name="submit" id="uploadFileSubmit" value="Submit" >
                      
                      
                      """),_display_(/*90.24*/if(uploadId!="0")/*90.41*/{_display_(Seq[Any](format.raw/*90.42*/("""
                        
                        
                                """),format.raw/*93.33*/("""<div class="form-group">
                                    <label></label>
                                    """),_display_(/*95.38*/{if(resultdata(0)==0){
                                    
                                     <span style="color:red">Data Not Uploaded</span>
                                     }else{
                                     
                                     <span style="color:green">Data Uploaded Successfully</span>
                                      <p>Total Record are {resultdata(1)} and records uploded successfully is {resultdata(0)}</p>
                                   
                                     }}),format.raw/*103.40*/("""
                                     
                                     
                                  """),format.raw/*106.35*/("""<a href="/CREManager/downloadExcelSheet/"""),_display_(/*106.76*/uploadId),format.raw/*106.84*/("""/"""),_display_(/*106.86*/uploadType),format.raw/*106.96*/("""" >Click Here To Download Error File</a> 
                                   
                                </div>				 
                           
                            """)))}),format.raw/*110.30*/("""
                            
                            
                              
                    """),format.raw/*114.21*/("""</div>
                    
                     

                </div>
                
                

                <input type="checkbox" style="display:none" name="useworker" ><br />
                <input type="checkbox" style="display:none" name="xferable" checked><br />
                <input type="checkbox" style="display:none" name="userabs" checked><br />
                <div class="hr-line-dashed"></div>
                <div class="ibox-content">			
                    <div class="table-responsive">
                        <table class="table table-bordered header-fixed table-responsive" id="tableData" style="width:80%">
                            <thead>

                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div> 
                    <br />

                </div>

            </div>	
            
           
                            

        </div>
        <!--<button type="submit" class="btn btn-primary" name="submit" onclick="ajaxUploadData()" >Submit</button>-->


    </div>
    <script>
        $(document).ready(function () """),format.raw/*151.39*/("""{"""),format.raw/*151.40*/("""
            """),format.raw/*152.13*/("""$.fn.checkFileType = function (options) """),format.raw/*152.53*/("""{"""),format.raw/*152.54*/("""
                """),format.raw/*153.17*/("""var defaults = """),format.raw/*153.32*/("""{"""),format.raw/*153.33*/("""
                    """),format.raw/*154.21*/("""allowedExtensions: [],
                    success: function () """),format.raw/*155.42*/("""{"""),format.raw/*155.43*/("""}"""),format.raw/*155.44*/(""",
                    error: function () """),format.raw/*156.40*/("""{"""),format.raw/*156.41*/("""}"""),format.raw/*156.42*/("""
                """),format.raw/*157.17*/("""}"""),format.raw/*157.18*/(""";
                options = $.extend(defaults, options);

                return this.each(function () """),format.raw/*160.46*/("""{"""),format.raw/*160.47*/("""

                    """),format.raw/*162.21*/("""$(this).on('change', function () """),format.raw/*162.54*/("""{"""),format.raw/*162.55*/("""
                        """),format.raw/*163.25*/("""var value = $(this).val(),
                                file = value.toLowerCase(),
                                extension = file.substring(file.lastIndexOf('.') + 1);

                        if ($.inArray(extension, options.allowedExtensions) == -1) """),format.raw/*167.84*/("""{"""),format.raw/*167.85*/("""
                            """),format.raw/*168.29*/("""options.error();
                            $(this).focus();
                        """),format.raw/*170.25*/("""}"""),format.raw/*170.26*/(""" """),format.raw/*170.27*/("""else """),format.raw/*170.32*/("""{"""),format.raw/*170.33*/("""
                            """),format.raw/*171.29*/("""options.success();

                        """),format.raw/*173.25*/("""}"""),format.raw/*173.26*/("""

                    """),format.raw/*175.21*/("""}"""),format.raw/*175.22*/(""");

                """),format.raw/*177.17*/("""}"""),format.raw/*177.18*/(""");
            """),format.raw/*178.13*/("""}"""),format.raw/*178.14*/(""";
            $('#uploadfiles').checkFileType("""),format.raw/*179.45*/("""{"""),format.raw/*179.46*/("""
                """),format.raw/*180.17*/("""allowedExtensions: ['xlsx'],
                error: function () """),format.raw/*181.36*/("""{"""),format.raw/*181.37*/("""
                    """),format.raw/*182.21*/("""Lobibox.alert('warning', """),format.raw/*182.46*/("""{"""),format.raw/*182.47*/("""
                        """),format.raw/*183.25*/("""msg: "Please select XLSX file only"
                    """),format.raw/*184.21*/("""}"""),format.raw/*184.22*/(""");
                """),format.raw/*185.17*/("""}"""),format.raw/*185.18*/("""
            """),format.raw/*186.13*/("""}"""),format.raw/*186.14*/(""");
            $("#uploadFileSubmit").click(function () """),format.raw/*187.54*/("""{"""),format.raw/*187.55*/("""
                """),format.raw/*188.17*/("""var uploadName = $("#upload_form_name").val();
                var sheetName = $("#sheetName").val();
                var city = $("#city").val();
                var workshop = $("#workshop").val();
                var uploadfiles = $("#uploadfiles").val();
				console.log(workshop);
				console.log(city);
                if (uploadName != 'select' && sheetName != '' && uploadfiles != '' && city != 'select' && workshop != '0') """),format.raw/*195.124*/("""{"""),format.raw/*195.125*/("""
                   
                    
                    """),format.raw/*198.21*/("""return true;
                """),format.raw/*199.17*/("""}"""),format.raw/*199.18*/(""" """),format.raw/*199.19*/("""else """),format.raw/*199.24*/("""{"""),format.raw/*199.25*/("""

                    """),format.raw/*201.21*/("""if (uploadName == 'select' || uploadName == '') """),format.raw/*201.69*/("""{"""),format.raw/*201.70*/("""

                        """),format.raw/*203.25*/("""Lobibox.notify('warning', """),format.raw/*203.51*/("""{"""),format.raw/*203.52*/("""
                            """),format.raw/*204.29*/("""msg: "Please select the type of file"
                        """),format.raw/*205.25*/("""}"""),format.raw/*205.26*/(""");
                        return false;

                    """),format.raw/*208.21*/("""}"""),format.raw/*208.22*/("""
                    """),format.raw/*209.21*/("""if (sheetName == '') """),format.raw/*209.42*/("""{"""),format.raw/*209.43*/("""

                        """),format.raw/*211.25*/("""Lobibox.notify('warning', """),format.raw/*211.51*/("""{"""),format.raw/*211.52*/("""
                            """),format.raw/*212.29*/("""msg: "Please enter the sheet name"
                        """),format.raw/*213.25*/("""}"""),format.raw/*213.26*/(""");
                        return false;

                    """),format.raw/*216.21*/("""}"""),format.raw/*216.22*/("""
                    """),format.raw/*217.21*/("""if (uploadfiles == '') """),format.raw/*217.44*/("""{"""),format.raw/*217.45*/("""

                        """),format.raw/*219.25*/("""Lobibox.notify('warning', """),format.raw/*219.51*/("""{"""),format.raw/*219.52*/("""
                            """),format.raw/*220.29*/("""msg: "Please choose the file"
                        """),format.raw/*221.25*/("""}"""),format.raw/*221.26*/(""");
                        return false;

                    """),format.raw/*224.21*/("""}"""),format.raw/*224.22*/("""
                    """),format.raw/*225.21*/("""if (city == 'select' || city == '') """),format.raw/*225.57*/("""{"""),format.raw/*225.58*/("""

                        """),format.raw/*227.25*/("""Lobibox.notify('warning', """),format.raw/*227.51*/("""{"""),format.raw/*227.52*/("""
                            """),format.raw/*228.29*/("""msg: "Please select the city"
                        """),format.raw/*229.25*/("""}"""),format.raw/*229.26*/(""");
                        return false;

                    """),format.raw/*232.21*/("""}"""),format.raw/*232.22*/("""
                    """),format.raw/*233.21*/("""if (workshop == '0' || workshop == '') """),format.raw/*233.60*/("""{"""),format.raw/*233.61*/("""

                        """),format.raw/*235.25*/("""Lobibox.notify('warning', """),format.raw/*235.51*/("""{"""),format.raw/*235.52*/("""
                            """),format.raw/*236.29*/("""msg: "Please select the workshop"
                        """),format.raw/*237.25*/("""}"""),format.raw/*237.26*/(""");
                        return false;

                    """),format.raw/*240.21*/("""}"""),format.raw/*240.22*/("""
                """),format.raw/*241.17*/("""}"""),format.raw/*241.18*/("""
            """),format.raw/*242.13*/("""}"""),format.raw/*242.14*/(""");

        """),format.raw/*244.9*/("""}"""),format.raw/*244.10*/(""");

    </script>


    """)))}),format.raw/*249.6*/("""
    """)))}))
      }
    }
  }

  def render(resultdata:List[Integer],uploadId:String,uploadType:String,campaignList:List[Campaign],locationList:List[Location],dealerCode:String,userName:String): play.twirl.api.HtmlFormat.Appendable = apply(resultdata,uploadId,uploadType,campaignList,locationList,dealerCode,userName)

  def f:((List[Integer],String,String,List[Campaign],List[Location],String,String) => play.twirl.api.HtmlFormat.Appendable) = (resultdata,uploadId,uploadType,campaignList,locationList,dealerCode,userName) => apply(resultdata,uploadId,uploadType,campaignList,locationList,dealerCode,userName)

  def ref: this.type = this

}


}

/**/
object uploadMasterFilesFormInsurance extends uploadMasterFilesFormInsurance_Scope0.uploadMasterFilesFormInsurance
              /*
                  -- GENERATED --
                  DATE: Thu Dec 28 12:24:31 IST 2017
                  SOURCE: D:/CRMNEW/28th Dec/crm/app/views/uploadMasterFilesFormInsurance.scala.html
                  HASH: 978c7ec742ed247047287971059890d06d3114f4
                  MATRIX: 860->1|1106->151|1134->154|1196->208|1235->210|1265->215|1278->221|1384->318|1424->320|1456->326|1511->355|1539->363|2834->1631|2882->1663|2921->1664|2991->1706|3034->1722|3056->1735|3095->1753|3125->1756|3148->1769|3188->1787|3271->1839|3333->1873|4677->3190|4725->3222|4764->3223|4834->3265|4877->3281|4899->3294|4930->3304|4960->3307|4982->3320|5014->3330|5097->3382|5163->3420|6903->5133|6929->5150|6968->5151|7082->5237|7225->5353|7785->5891|7928->6005|7997->6046|8027->6054|8057->6056|8089->6066|8304->6249|8447->6363|9693->7580|9723->7581|9766->7595|9835->7635|9865->7636|9912->7654|9956->7669|9986->7670|10037->7692|10131->7757|10161->7758|10191->7759|10262->7801|10292->7802|10322->7803|10369->7821|10399->7822|10534->7928|10564->7929|10617->7953|10679->7986|10709->7987|10764->8013|11055->8275|11085->8276|11144->8306|11261->8394|11291->8395|11321->8396|11355->8401|11385->8402|11444->8432|11519->8478|11549->8479|11602->8503|11632->8504|11683->8526|11713->8527|11758->8543|11788->8544|11864->8591|11894->8592|11941->8610|12035->8675|12065->8676|12116->8698|12170->8723|12200->8724|12255->8750|12341->8807|12371->8808|12420->8828|12450->8829|12493->8843|12523->8844|12609->8901|12639->8902|12686->8920|13155->9359|13186->9360|13280->9425|13339->9455|13369->9456|13399->9457|13433->9462|13463->9463|13516->9487|13593->9535|13623->9536|13680->9564|13735->9590|13765->9591|13824->9621|13916->9684|13946->9685|14040->9750|14070->9751|14121->9773|14171->9794|14201->9795|14258->9823|14313->9849|14343->9850|14402->9880|14491->9940|14521->9941|14615->10006|14645->10007|14696->10029|14748->10052|14778->10053|14835->10081|14890->10107|14920->10108|14979->10138|15063->10193|15093->10194|15187->10259|15217->10260|15268->10282|15333->10318|15363->10319|15420->10347|15475->10373|15505->10374|15564->10404|15648->10459|15678->10460|15772->10525|15802->10526|15853->10548|15921->10587|15951->10588|16008->10616|16063->10642|16093->10643|16152->10673|16240->10732|16270->10733|16364->10798|16394->10799|16441->10817|16471->10818|16514->10832|16544->10833|16586->10847|16616->10848|16677->10878
                  LINES: 27->1|32->1|33->2|33->2|33->2|35->4|35->4|35->4|35->4|38->7|38->7|38->7|65->34|65->34|65->34|66->35|66->35|66->35|66->35|66->35|66->35|66->35|67->36|68->37|90->59|90->59|90->59|91->60|91->60|91->60|91->60|91->60|91->60|91->60|92->61|93->62|121->90|121->90|121->90|124->93|126->95|134->103|137->106|137->106|137->106|137->106|137->106|141->110|145->114|182->151|182->151|183->152|183->152|183->152|184->153|184->153|184->153|185->154|186->155|186->155|186->155|187->156|187->156|187->156|188->157|188->157|191->160|191->160|193->162|193->162|193->162|194->163|198->167|198->167|199->168|201->170|201->170|201->170|201->170|201->170|202->171|204->173|204->173|206->175|206->175|208->177|208->177|209->178|209->178|210->179|210->179|211->180|212->181|212->181|213->182|213->182|213->182|214->183|215->184|215->184|216->185|216->185|217->186|217->186|218->187|218->187|219->188|226->195|226->195|229->198|230->199|230->199|230->199|230->199|230->199|232->201|232->201|232->201|234->203|234->203|234->203|235->204|236->205|236->205|239->208|239->208|240->209|240->209|240->209|242->211|242->211|242->211|243->212|244->213|244->213|247->216|247->216|248->217|248->217|248->217|250->219|250->219|250->219|251->220|252->221|252->221|255->224|255->224|256->225|256->225|256->225|258->227|258->227|258->227|259->228|260->229|260->229|263->232|263->232|264->233|264->233|264->233|266->235|266->235|266->235|267->236|268->237|268->237|271->240|271->240|272->241|272->241|273->242|273->242|275->244|275->244|280->249
                  -- GENERATED --
              */
          