
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object serviceInBound_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class serviceInBound extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template7[List[Workshop],List[ServiceTypes],Customer,Vehicle,List[Location],CallInteraction,WyzUser,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(workshopList:List[Workshop],servicetypeList:List[ServiceTypes],customerData:Customer,vehicleData:Vehicle,locationList:List[Location],interOfCall:CallInteraction,userData:WyzUser):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.181*/("""
 """),format.raw/*3.2*/("""<div class="col-md-12" id="InBoundDiv" style="display:none;">
			<div class="row animated  bounceInRight" id="LeadSourceHideIn">
			<div class="col-md-3">
			  <div class="form-group">
				<label for=""><b>Lead Source</b></label>
				<select class="form-control" name="inBoundLeadSource" id="inBoundLeadSourceSelectVal" >
					<option value=0>Select</option>
				  <option value="Phone">Phone</option>
				  <option value="Walk In">Walk In</option>
				  <option value="Website">Website</option>
				  <option value="E-mail">E-mail</option>
				  <option value="Reference">Reference</option>
				  <option value="Inbound Call">Inbound Call</option>
				  <option value="Body Shop">Body Shop</option>
				  <option value="Others">Others</option>
				</select>
			  </div>
			</div>
			
			<div class="col-md-3">
			  <div class="form-group">
				<label for=""><b>Service Required</b></label>
				<select class="form-control" name="serviceTypeAltId" id="serviceTypeAltIdDataVal" >
					<option value=0>Select</option>
						"""),_display_(/*27.8*/for(serlist <- servicetypeList) yield /*27.39*/{_display_(Seq[Any](format.raw/*27.40*/("""
					
					"""),_display_(/*29.7*/{if(serlist.serviceTypeName ==(if(interOfCall.getServiceBooked()!=null){interOfCall.getServiceBooked().getServiceBookedType()}))
					{
									
									<option value={serlist.id.toString()} selected="selected">{serlist.serviceTypeName}</option>
									}else {
										<option value={serlist.id.toString()} >{serlist.serviceTypeName}</option>
										}
									}),format.raw/*36.11*/("""
                              
									""")))}),format.raw/*38.11*/("""
				"""),format.raw/*39.5*/("""</select>
			  </div>
			</div>
				
			
			
		</div>
		<div class="col-md-3 animated  bounceInRight BookMyServiceIn" id="BookMyServiceIn">
			  <div class="form-group">
				
			<div class="checkbox-inline" style="display:none" id="booksMyServiceDisplayBlock">
			<label>
			  <input type="checkbox" name="contactType" class="InCallServiceBook" value="Book My Service" id="InCallServiceBook">
			  Book My Service </label>
		  </div>
	
			  </div>
		</div>
	<div style="display:none;" class="animated  bounceInRight" id="InCallserviceBookDiv">
		  <div class="col-md-12">
			  <div class="row">
				<div class="col-md-3">
				  <div class="form-group">
					<label for="">Select Vehicle</label>
					<select class="form-control" id="vehicleIn" name="vehicleIn">
					  """),_display_(/*64.9*/for(post <- customerData.getVehicles()) yield /*64.48*/{_display_(Seq[Any](format.raw/*64.49*/("""
                              
                              """),format.raw/*66.31*/("""<option value=""""),_display_(/*66.47*/post/*66.51*/.vehicle_id),format.raw/*66.62*/("""">"""),_display_(/*66.65*/post/*66.69*/.vehicleRegNo),format.raw/*66.82*/("""</option>
                              
					""")))}),format.raw/*68.7*/("""
					  
							
						
					"""),format.raw/*72.6*/("""</select>
				  </div>
				  </div>
				  <div class="col-md-3">
				  <div class="form-group">
					<label for="">Select City</label>
					<select class="form-control" id="cityIn" name="cityIn" onchange="ajaxCallToLoadWorkShopByCityIn();">
					 """),_display_(/*79.8*/for(loc <- locationList) yield /*79.32*/{_display_(Seq[Any](format.raw/*79.33*/("""
					
					"""),_display_(/*81.7*/{if(loc.getName() ==userData.getLocation().getName())
					{
									
									<option value={userData.getLocation().getName()} selected="selected">{userData.getLocation().getName()}</option>
									}else {
										<option value={loc.getName} >{loc.getName}</option>
										}
									}),format.raw/*88.11*/("""
                              
									""")))}),format.raw/*90.11*/(""" 
					"""),format.raw/*91.6*/("""</select>
				  </div>
				</div>
						
				<div class="col-md-3">
				  <label for="">Workshop</label>
				 <select class="form-control" id="workshopIn" name="workshopIn" onchange="ajaxAutoSASelectionList('workshopIn','date123456','serviceAdvisorIdIn');  advisorBasedOnWorkshopSelectionIn(); driverbasedOnWorkshopSelectionIn();">
						<option value=0>--Select--</option>
						"""),_display_(/*99.8*/for(worklist <- workshopList) yield /*99.37*/{_display_(Seq[Any](format.raw/*99.38*/("""
					
					"""),_display_(/*101.7*/{if(worklist.workshopName ==(if(interOfCall.getServiceBooked()!=null){interOfCall.getServiceBooked().getWorkshop().getWorkshopName()})){
									
									<option value={worklist.id.toString()} selected="selected">{worklist.workshopName}</option>
									}else {
										<option value={worklist.id.toString()} >{worklist.workshopName}</option>
										}
									}),format.raw/*107.11*/("""
                              
                             
                              
									""")))}),format.raw/*111.11*/("""
				 """),format.raw/*112.6*/("""</select>
				</div>
				 <div class="col-md-3">
				  <div class="form-group">
					<label class="control-label" for="inputGroup">Select Date</label>
					<div class="input-group">
					  <input type="text" name="serviceScheduledDateIn" class="form-control" id="date123456" onchange="ajaxAutoSASelectionList('workshopIn','date123456','serviceAdvisorIdIn');" value=""""),_display_(/*118.186*/{if(interOfCall.getServiceBooked()!=null){interOfCall.getServiceBooked().getServiceScheduledDateStrSqlFor()}}),format.raw/*118.295*/(""""readonly/>
					  <span class="input-group-addon" data-toggle="modal" data-target="#sa_modal" onclick="ajaxCallToLoadWorkshopSummaryIn();"> <i style="font-size:19px; color:Red;" class="fa">&#xf274;</i> </span> </div>
				  </div>
				</div>
				</div>
			 </div>
				
		   <div class="col-md-12">
			  <div class="row">
			 
				<div class="col-md-3">
				<div class="form-group">
				  <label for="serviceScheduledTimeIn">Select Time:</label>
				  <input type="text" name="serviceScheduledTimeIn" class="form-control timePickRange7to19" id="CommittedTimesIn" value=""""),_display_(/*131.126*/{if(interOfCall.getServiceBooked()!=null){interOfCall.getServiceBooked().getServiceScheduledTimeStrSqlFor()}}),format.raw/*131.235*/(""""  readonly>
				 
				</div>
		 </div>
		  <div class="col-md-3">
				  <div class="form-group">
				  <label>&nbsp;</label><br/>
					<button type="button" class="btn btn-primary btn-block" onClick="return ajaxAutoSASelection('workshopIn','date123456','serviceAdvisorIdIn')">Recommend SA</button>
					  
				  </div>
				</div>
				<div class="col-md-3">
				  <div class="form-group">
					<label for="">Assigned To(SA):</label>
					<select class="form-control" id="serviceAdvisorIdIn" name="serviceAdvisorIdIn" onChange="ajaxAutoSAManualchange('workshopIn','date123456','serviceAdvisorIdIn');">
					  """),_display_(/*146.9*/{if(interOfCall.getServiceBooked()!=null){
					
					<option value={interOfCall.getServiceBooked().getServiceAdvisor().getAdvisorId().toString()} selected="selected">{interOfCall.getServiceBooked().getServiceAdvisor().getAdvisorName()}</option>
					
					}}),format.raw/*150.8*/("""
					  """),format.raw/*151.8*/("""</select>
				  </div>
				</div>
			  </div>
			
			
			<div class="pull-right">
				<button type="button" class="btn btn-primary" id="BackToCustomerMainInBound">Back</button>
				<button type="button" class="btn btn-primary" id="nextToCustomerDriveIn">Next</button>
				
			</div>
			</div>
			</div>
			<div style="display:none;" class="animated  bounceInRight" id="CustomerDriveInDivIn">
			<div class="col-md-12">
			<label>Choose one of the below option:</label>
		<div class="radio">
		  <label>
			<input type="radio" name="typeOfPickupIn" value="Customer Drive-In" id="CustomerDriveInIDIn" >
			Customer Drive-In </label>
		</div>		
		
		<div class="radio">
		  <label>
			<input type="radio" name="typeOfPickupIn" value="true" id="PickupDropRequiredIn" >
			Pickup Drop Required</label>
		</div>
		<div class="radio">
		  <label>
			<input type="radio" name="typeOfPickupIn" value="Road Side Assistance" id="RoadSideAssi" >
			Road Side Assistance</label>
		</div>
		<div class="animated  bounceInRight" id="MSSSelectDivIn" style="display:none;">
				
				<div class="col-md-3">
				  <div class="form-group">
					<label for="">Select Address</label>
					<select class="form-control" id="AddressMSS" name="pickUpAddressIn">
					 """),_display_(/*189.8*/for( addre_list <- customerData.getAddresses()) yield /*189.55*/{_display_(Seq[Any](format.raw/*189.56*/("""
											
									"""),_display_(/*191.11*/{if(addre_list.getConcatenatedAdress() !=null)
									
									<option value={addre_list.getConcatenatedAdress()}>{addre_list.getConcatenatedAdress()}</option>
									else 
										<option value={addre_list.getConcatenatedAddressWith()}>{addre_list.getConcatenatedAddressWith()}</option>
										}),format.raw/*196.12*/("""
									""")))}),format.raw/*197.11*/("""
					"""),format.raw/*198.6*/("""</select>
				  </div>
				  </div>
				   <div class="col-md-3 animated  bounceInRight">
				   <div class="form-group">
				   <br>
				   <button type="button" class="btn btn-success" data-toggle="modal" data-target="#AddNewAddressPopup">Add New Address</button>
              <button type="button" class="btn btn-success" style="display:none;" id="AssignBtnBkreviewIn" data-toggle="modal" data-target="#DriverAllcationPOPUp" onclick="ajaxAssignBtnBkreview('workshopIn','date123456');">Assign Driver</button>
					</div>				   
				  </div>
			  </div>
				<div class="row">
				<div class="col-md-3">
							  <div class="form-group">
								<label for="memarksPickUpADD">Remarks</label>
								<textarea type="text" name="remarksList[0]" class="form-control" id="memarksPickUpADD"></textarea>
							  </div>
							</div>
			  </div>
	  <div class="pull-right">
	  <button type="button" class="btn btn-primary" id="BackToCunstomerDriveIn">Back</button>
	   <button type="button" class="btn btn-primary" id="NextToLeadInbound">Next</button>
	  </div>
	</div>
		  </div>
		  <!--end-->
		
		 
		
	  </div>			   
<!------------inBound Upsel Opportunity------------------>
		 <div class="col-md-12 animated  bounceInRight" style="display:none;" id="finalDiv1Inbound">
			<label>Capture Lead : Is there an Upsell Opportunity.</label>
		   <br>
		   
			<div class="radio-inline">
			  <label>
				<input type="radio" name="LeadYesIn" onclick="loadLeadBasedOnLocation();" value="Capture Lead YesIn" id="LeadYesIDIn" >
				Yes</label>
			</div>
			<div class="radio-inline">
			  <label>
				<input type="radio" name="LeadYesIn" value="Capture Lead NoIn" id="LeadNoIDIn" >
				No</label>
			</div>
			<div class="row animated  bounceInRight" style="display:none;" id="LeadDivIn">
			  <div class="col-md-6">
			  
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="LeadClassIn myCheckbox" name="upsellLead[14].upsellId" value="1" name="LeadClassIn"  id="InsuranceIDCheckIn" onClick="loadLeadBasedOnUserLocation('InsuranceIDCheckIn','insuranceLead2')">
					Insurance</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="InsuranceSelectIn">
				  <div class="col-md-12">
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[14].taggedTo" id="insuranceLead2">
								<option >Select</option>
						</select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <input class="form-control" rows="1" id="comments8" name="upsellLead[14].upsellComments">
					</div>
				   </div>
				</div>
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="LeadClassIn myCheckbox" name="upsellLead[14].upsellId" value="2" name="MaxicareIn"  id="MaxicareIDCheckIn" onClick="loadLeadBasedOnUserLocation('MaxicareIDCheckIn','MaxicareLead2')">
					Warranty / EW</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="MaxicareSelectIn">
				  <div class="col-md-12">
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[14].taggedTo" id="MaxicareLead2">
								<option >Select</option>
						</select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <input class="form-control" rows="1" id="comments8" name="upsellLead[14].upsellComments">
					</div>
				   </div>
				</div>
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="LeadClassIn myCheckbox" name="upsellLead[15].upsellId" value="4" id="ShieldIn" onClick="loadLeadBasedOnUserLocation('ShieldIn','ShieldLead2')">
					Re-Finance / New Car Finance</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="ShieldSelectIn">
					<div class="col-md-12">
					<div class="col-md-6">
					  <label for="">Tag to</label>
					 <select class="form-control" name="upsellLead[15].taggedTo" id="ShieldLead2">
								<option >Select</option>
							  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments9" name="upsellLead[15].upsellComments"></textarea>
					</div>
				   </div>
				 
				</div>
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="VASIn myCheckbox" name="upsellLead[16].upsellId" value="3" id="VASIDIn" onClick="loadLeadBasedOnUserLocation('VASIDIn','vASLead2')"/>
					VAS</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="VASTagToSelectIn">
				   <div class="col-md-12">
					<div class="col-md-6">
					  <label for="">Tag to</label>
					 <select class="form-control" name="upsellLead[16].taggedTo" id="vASLead2">
								<option >Select</option>
							  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments10" name="upsellLead[16].upsellComments"></textarea>
					</div>
				   </div>
				  
				</div>
				
				</div>
				 <div class="col-md-6">
				
				<div class="checkbox">
				  <label>
					<input type="checkbox" class="EXCHANGEIn myCheckbox" name="upsellLead[19].upsellId" value="6" id="EXCHANGEIDIn" onClick="loadLeadBasedOnUserLocation('EXCHANGEIDIn','buyNewCarLead2')"/>
					Buy New Car / Exchange</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="EXCHANGEIDSelectIn">
				   <div class="col-md-12">
					<div class="col-md-6">
					  <label for="">Tag to</label>
					  <select class="form-control" name="upsellLead[19].taggedTo" id="buyNewCarLead2">
								<option >Select</option>
							  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments13" name="upsellLead[19].upsellComments"></textarea>
					</div>
				   </div>
				</div>
				  <div class="checkbox">
				  <label>
					<input type="checkbox" class="UsedCarIn myCheckbox" name="upsellLead[20].upsellId" value="9" id="UsedCarIDIn" onClick="loadLeadBasedOnUserLocation('UsedCarIDIn','usedCarLead2')"/>
					Used Car</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="UsedCarSelectIn">
				   <div class="col-md-12">
					<div class="col-md-6">
					  <label for="">Tag to</label>
					 <select class="form-control" name="upsellLead[20].taggedTo" id="usedCarLead2">
								<option >Select</option>
							  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments14" name="upsellLead[20].upsellComments"></textarea>
					</div>
				   </div>
			</div>
			<div class="checkbox">
				  <label>
					<input type="checkbox" class="UsedCarIn myCheckbox" name="upsellLead[20].upsellId" value="5" id="RoadSideAssiIDIn" onClick="loadLeadBasedOnUserLocation('RoadSideAssiIDIn','RoadSideAssiLead2')"/>
					Sell Old Car</label>
				</div>
				<div class="row animated  bounceInRight" style="display:none;" id="RoadSideAssiSelectIn">
				   <div class="col-md-12">
					<div class="col-md-6">
					  <label for="">Tag to</label>
					 <select class="form-control" name="upsellLead[20].taggedTo" id="RoadSideAssiLead2">
								<option >Select</option>
							  </select>
					   </div>
					   <div class="col-md-6">
					  <label for="comments">Remarks</label>
					  <textarea class="form-control" rows="1" id="comments14" name="upsellLead[20].upsellComments"></textarea>
					</div>
				   </div>
			</div>
		   
			  </div>
			</div>
			  <div class="pull-right" >
			<button type="button" class="btn btn-primary" id="BackToLeadIn">Back</button>
			  <button type="button" class="btn btn-primary" value="" id="NextToCustFeedBackIn"/>Next</button>
			</div>
			</div>
			
			
			<!--Complaints Inbound -->
			
			<div class="col-md-12 animated  bounceInRight" id="CustFeedBackIn" style="display:none;">
				 
				  <div class="col-md-12">
					<label>Does Customer has a feedback / compliant ?</label>
					<br>
					<div class="radio-inline">
					  <label for="feedbackYesIn">
						<input type="radio" name="userfeedbackIn" onclick="loadLeadBasedOnLocationDepartment();" value="feedback YesIn" id="feedbackYesIn" >
						Yes</label>
					</div>
					<div class="radio-inline">
					  <label for="feedbackNoIn">
						<input type="radio" name="userfeedbackIn" value="feedback NoIn" id="feedbackNoIn" >
						No</label> 
					</div>
				  </div>
			   
				<div class="row animated  bounceInRight" style="display:none;" id="feedbackDIVIn"><br>
				 <div class="col-md-12">
				  <div class="col-md-3">
					<label for="">Select Department<i class="lblStar">*</i></label>
					<select class="form-control selected_department" name="departmentForFB2" id="selected_department2" onchange="ajaxLeadTagByDepartmentInbound();">
						 <option value="0">select</option>
						 
							</select>
				  </div>
				  <div class="col-md-3">
					<label for="">Tag to</label>
					<select class="form-control" id="LeadTagsByLocation2" name="complaintOrFB_TagTo2">
					"""),_display_(/*426.7*/{if(interOfCall.getSrdisposition()!=null){
					
					<option value={interOfCall.getSrdisposition().getComplaintOrFB_TagTo()} selected="selected">{interOfCall.getSrdisposition().getComplaintOrFB_TagTo()}</option>
					
					}}),format.raw/*430.8*/("""		  
					"""),format.raw/*431.6*/("""</select>
				  </div>
				
				  <div class="col-md-3">
					<label for="comments">Feedback/Comment</label>
					<textarea class="form-control" rows="1" id="commentsDSA" name="remarksOfFB2"></textarea>
				  </div>
				  <br>
				</div>
				  </div>
		<div class="pull-right" >
			<button type="button" class="btn btn-primary" id="BackToUpsellIn">Back</button>
			  <button type="button" class="btn btn-primary" value="" id="NextToInBoundLastQuest">Next</button>
			</div>
		 
		  </div>
		  <div class="row animated  bounceInRight" id="LastQuestionInbound" style="display:none;">
						  <div class="col-md-12">
						  <div class="col-md-12">
							<label>May I please take a moment to update your contact information? This will help us serve you better in future.</label>
							<br>
							<div class="radio-inline">
							  <label for="">
								<input type="radio" name="radio7" value="" id=""  data-toggle="modal" data-target="#addBtn" >
								Yes</label>
							</div>
							<div class="radio-inline">
							  <label for="">
								<input type="radio" name="radio7" value="" id="CustomerNo">
								No</label> 
							</div>
						  </div>
						
						  <div class="pull-right" >
					<button type="button" class="btn btn-primary " id="BackToToInboindComplaint">Back</button>
					<button type="submit" class="btn btn-primary" value="inBoundCallSubmit" id="ToInboindComplaintSubmit" name="typeOfsubmit"/>Submit</button>
					</div>
						  </div>
		    </div>
		  
				  
		  
		  
		 <!--EndLead-->
		 <!--EndLead-->"""))
      }
    }
  }

  def render(workshopList:List[Workshop],servicetypeList:List[ServiceTypes],customerData:Customer,vehicleData:Vehicle,locationList:List[Location],interOfCall:CallInteraction,userData:WyzUser): play.twirl.api.HtmlFormat.Appendable = apply(workshopList,servicetypeList,customerData,vehicleData,locationList,interOfCall,userData)

  def f:((List[Workshop],List[ServiceTypes],Customer,Vehicle,List[Location],CallInteraction,WyzUser) => play.twirl.api.HtmlFormat.Appendable) = (workshopList,servicetypeList,customerData,vehicleData,locationList,interOfCall,userData) => apply(workshopList,servicetypeList,customerData,vehicleData,locationList,interOfCall,userData)

  def ref: this.type = this

}


}

/**/
object serviceInBound extends serviceInBound_Scope0.serviceInBound
              /*
                  -- GENERATED --
                  DATE: Thu Dec 28 12:24:30 IST 2017
                  SOURCE: D:/CRMNEW/28th Dec/crm/app/views/serviceInBound.scala.html
                  HASH: 3f5b542c53f9d39bde0e75a0f63d6a39fe18347f
                  MATRIX: 846->3|1121->182|1150->185|2223->1232|2270->1263|2309->1264|2350->1279|2749->1657|2824->1701|2857->1707|3678->2502|3733->2541|3772->2542|3864->2606|3907->2622|3920->2626|3952->2637|3982->2640|3995->2644|4029->2657|4108->2706|4168->2739|4449->2994|4489->3018|4528->3019|4569->3034|4891->3335|4966->3379|5001->3387|5416->3776|5461->3805|5500->3806|5542->3821|5940->4197|6079->4304|6114->4311|6516->4684|6648->4793|7260->5376|7392->5485|8043->6109|8327->6372|8364->6381|9672->7662|9736->7709|9776->7710|9829->7735|10162->8046|10206->8058|10241->8065|19748->17545|19999->17775|20038->17786
                  LINES: 27->2|32->2|33->3|57->27|57->27|57->27|59->29|66->36|68->38|69->39|94->64|94->64|94->64|96->66|96->66|96->66|96->66|96->66|96->66|96->66|98->68|102->72|109->79|109->79|109->79|111->81|118->88|120->90|121->91|129->99|129->99|129->99|131->101|137->107|141->111|142->112|148->118|148->118|161->131|161->131|176->146|180->150|181->151|219->189|219->189|219->189|221->191|226->196|227->197|228->198|456->426|460->430|461->431
                  -- GENERATED --
              */
          