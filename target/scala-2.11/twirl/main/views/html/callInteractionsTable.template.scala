
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object callInteractionsTable_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class callInteractionsTable extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template7[String,String,String,String,List[String],List[CallDispositionData],List[String],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(fromDateValue:String,toDateValue:String,user:String,dealerName:String,allCRE:List[String],allDisposition:List[CallDispositionData],locations:List[String]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.157*/("""

"""),_display_(/*4.2*/mainPageCREManger("AutoSherpaCRM",user,dealerName)/*4.52*/ {_display_(Seq[Any](format.raw/*4.54*/("""

"""),format.raw/*6.1*/("""<input type="hidden" name="fromDateToAss" value=""""),_display_(/*6.51*/fromDateValue),format.raw/*6.64*/("""">
<input type="hidden" name="toDateToAss" value=""""),_display_(/*7.49*/toDateValue),format.raw/*7.60*/("""">
<div class="panel panel-primary">
        <div class="panel-heading"><strong>Select Date Range To view Call Interactions</strong></div>
        <div class="panel-body"> 
             """),_display_(/*11.15*/helper/*11.21*/.form(action = routes.AllCallInteractionController.downloadCallHistoryReport)/*11.98*/ {_display_(Seq[Any](format.raw/*11.100*/("""
  """),format.raw/*12.3*/("""<div class="row">
  <div class="col-md-2">   
      <div class="form-group">
        
<label>Call Date From :<span style="color:red">*</span></label>    
	   
		<input type="text" class="form-control datepickerFilter" placeholder="Enter Call Date From" id="frombilldaterange" name="frombilldaterange" readonly>
      </div>
      </div>
  
   <div class="col-md-2">   
      <div class="form-group">   
       <label>Call Date To :<span style="color:red">*</span></label>    
					<input type="text" class="form-control datepickerFilter" placeholder="Enter Call Date To"  id="tobilldaterange" name="tobilldaterange" readonly>
     	</div>
      </div>

   
       
       <div class="col-md-2">
   
                    <label>Select Location :</label>
                     <div class="form-group">
    		 	<select  multiple="multiple"  id="location"  name="locations[]" class="selectpicker form-control" onchange="ajaxCallToLoadWorkShopByCityCallhistory();" > 
    					                      			   
                     	"""),_display_(/*37.24*/for(data3 <- locations) yield /*37.47*/{_display_(Seq[Any](format.raw/*37.48*/("""                       	                         
                    
                        """),format.raw/*39.25*/("""<option value=""""),_display_(/*39.41*/data3),format.raw/*39.46*/("""">"""),_display_(/*39.49*/data3),format.raw/*39.54*/("""</option>
                      				
                      	""")))}),format.raw/*41.25*/("""			 	
                    	"""),format.raw/*42.22*/("""</select>	
           
      </div>
       </div>
       
       <div class="col-md-2">
   
                    <label>Select Workshops :</label>
                     <div class="form-group">
    		 	<select  multiple="multiple"  id="workshopname"  name="workshopname[]" class="selectpicker form-control"  onchange="ajaxCallHistoryReports();"  > 
    					                      			    
                     				 	
                    	</select>
                    			
           
      </div>
       </div>
       <div class="col-md-2">
                    <label>Select Dispositions :</label>
                     <div class="form-group">
    			<select  multiple="multiple" id="all_dispositions" name="all_dispositions[]" class="selectpicker form-control" >
    					                       			    
                     	"""),_display_(/*64.24*/for(data2 <- allDisposition) yield /*64.52*/{_display_(Seq[Any](format.raw/*64.53*/("""                      	                         
                    
                        """),format.raw/*66.25*/("""<option value=""""),_display_(/*66.41*/data2/*66.46*/.getId()),format.raw/*66.54*/("""">"""),_display_(/*66.57*/data2/*66.62*/.getDisposition()),format.raw/*66.79*/("""</option>
                      				
                      	""")))}),format.raw/*68.25*/("""			 	
                      					
                	"""),format.raw/*70.18*/("""</select>
                				
            </div>
             </div>
        <div class="col-md-2">
   
                    <label>Select CRE's :</label>
                     <div class="form-group">
              <select class="selectpicker form-control"  id="crename" name="crename[]" multiple>
    					                      			    
                     				 	
                    	</select>
           
      </div>
       </div>
</div>
<div class="col-md-12">
  <div class="form-group pull-right">
  		<button type="button" name="selectedFile" class="btn btn-sm btn-primary viewCallsforReports" value="viewCalls"><i class="fa fa-list" aria-hidden="true"></i>&nbsp;View Calls</button>		     	
  		 <button type="submit" name="downloadFile" class="btn btn-sm btn-warning"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;Download</button>
		</div>
</div> 
  </div>
 
  """)))}),format.raw/*94.4*/("""
  """),format.raw/*95.3*/("""</div>      
<span type="label" id="viewCallsERR" style="color:red"></span>
<div class="row">
      <div class="col-lg-12">
        <div class="panel panel-primary">
          <div class="panel-heading"><b> List Of  Call Interactions</b>
          </div>
          <div class="panel-body" style="overflow:auto">
          <table class="table table-striped table-bordered table-hover"
						id="datatableCallIntraction">
						<thead>
							<tr>

								<th>Location</th> 
                                    <th>CRE Name</th> 
                                    <th>Call Time</th>                                   
                                    <th>Call Date</th>
                                    <th>Preferred ContactNo</th>
                                    <th>Call Type</th>
                                    <th>Customer Name</th>
                                    <th>DOB</th>
                                    <th>Office Address</th>
                                    <th>Residential Address</th>
                                    <th>Permanent Address</th>
                                    <th>Email</th>
                                    <th>Customer Category</th>
                                    <th>Customer City</th>
                                    <th>Call Duration</th>
                                    <th>Customer Remarks</th>
                                    <th>Vehicle RegNo</th>
                                    <th>Chassis No</th>
                                    <th>Model</th>
                                    <th>Fuel Type</th>
                                    <th>Variant</th>
                                    <th>Last Service Date</th>
                                    <th>Last Service Type</th>
                                    <th>Next Service Date</th>
                                    <th>Next Service Type</th>
                                    <th>Forecast Logic</th>
                                    <th>Previous Disposition</th>
                                    <th>Primary Disposition</th>
                                    <th>Secondary Disposition</th>
                                    <th>Tertiary Disposition</th>
                                    <th>Service Type</th>
                                    <th>PSF Status</th>
                                    <th>Calling data type</th>
                                    <th>Type of Pickup</th>
                                    <th>Time of PickUp</th>
                                    <th>Up to</th>
                                    <th>Driver</th>
                                    <th>Service Advisor</th>
                                    <th>Upsell Type</th>
                                    <th>Assigned Date</th>
                                    <th>Is Call initiated</th>


							</tr>
						</thead>
						<tbody>

						</tbody>
					</table>
                     
          </div>
          <!-- /.panel-body --> 
        </div>
        <!-- /.panel --> 
      </div>
      <!-- /.col-lg-12 --> 
    </div>
""")))}),format.raw/*165.2*/("""
	
"""),format.raw/*167.1*/("""<script>
$(document).ready(function () """),format.raw/*168.31*/("""{"""),format.raw/*168.32*/("""
	"""),format.raw/*169.2*/("""$('.viewCallsforReports').click(function()"""),format.raw/*169.44*/("""{"""),format.raw/*169.45*/("""
    	
        """),format.raw/*171.9*/("""var fromDate     = $("#frombilldaterange").val();
        var toDate       = $("#tobilldaterange").val();
       
              
        var UserIds="",j,k;
        var dispositions="";
        var location ="";
        var workshop = "";
        
        myOption = document.getElementById('crename');
        console.log("here"+myOption);		        
    for (j=0;j<myOption.options.length;j++)"""),format.raw/*182.44*/("""{"""),format.raw/*182.45*/("""
        """),format.raw/*183.9*/("""if(myOption.options[j].selected)"""),format.raw/*183.41*/("""{"""),format.raw/*183.42*/("""
            """),format.raw/*184.13*/("""if(myOption.options[j].value != "Select")"""),format.raw/*184.54*/("""{"""),format.raw/*184.55*/("""
        		"""),format.raw/*185.11*/("""UserIds = UserIds + myOption.options[j].value + ",";
            """),format.raw/*186.13*/("""}"""),format.raw/*186.14*/("""else"""),format.raw/*186.18*/("""{"""),format.raw/*186.19*/("""
				"""),format.raw/*187.5*/("""UserIds ="";
            """),format.raw/*188.13*/("""}"""),format.raw/*188.14*/("""
        """),format.raw/*189.9*/("""}"""),format.raw/*189.10*/("""
    """),format.raw/*190.5*/("""}"""),format.raw/*190.6*/("""
    	"""),format.raw/*191.6*/("""if(UserIds.length > 0)"""),format.raw/*191.28*/("""{"""),format.raw/*191.29*/("""
        	"""),format.raw/*192.10*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
        	
    	"""),format.raw/*194.6*/("""}"""),format.raw/*194.7*/("""else"""),format.raw/*194.11*/("""{"""),format.raw/*194.12*/("""
    		"""),format.raw/*195.7*/("""UserIds="";
    	"""),format.raw/*196.6*/("""}"""),format.raw/*196.7*/("""

    	"""),format.raw/*198.6*/("""mydispo = document.getElementById('all_dispositions');
        console.log("here"+mydispo);
    	 for (j=0;j<mydispo.options.length;j++)"""),format.raw/*200.45*/("""{"""),format.raw/*200.46*/("""
    	        """),format.raw/*201.14*/("""if(mydispo.options[j].selected)"""),format.raw/*201.45*/("""{"""),format.raw/*201.46*/("""
    	            """),format.raw/*202.18*/("""if(mydispo.options[j].value != "Select")"""),format.raw/*202.58*/("""{"""),format.raw/*202.59*/("""
    	        		"""),format.raw/*203.16*/("""dispositions = dispositions + mydispo.options[j].value + ",";
    	            """),format.raw/*204.18*/("""}"""),format.raw/*204.19*/("""else"""),format.raw/*204.23*/("""{"""),format.raw/*204.24*/("""
						"""),format.raw/*205.7*/("""dispositions ="";

    	            """),format.raw/*207.18*/("""}"""),format.raw/*207.19*/("""
        	            
    	        """),format.raw/*209.14*/("""}"""),format.raw/*209.15*/("""
    	    """),format.raw/*210.10*/("""}"""),format.raw/*210.11*/("""	   	
    	 """),format.raw/*211.7*/("""if(dispositions.length > 0)"""),format.raw/*211.34*/("""{"""),format.raw/*211.35*/("""
    		 """),format.raw/*212.8*/("""dispositions = dispositions.substring(0, dispositions.length - 1);
         	
     	"""),format.raw/*214.7*/("""}"""),format.raw/*214.8*/("""else"""),format.raw/*214.12*/("""{"""),format.raw/*214.13*/("""
     		"""),format.raw/*215.8*/("""dispositions="";
     	"""),format.raw/*216.7*/("""}"""),format.raw/*216.8*/("""


    	 """),format.raw/*219.7*/("""myloc = document.getElementById('all_dispositions');
         console.log("here"+myloc);
     	 for (j=0;j<myloc.options.length;j++)"""),format.raw/*221.44*/("""{"""),format.raw/*221.45*/("""
     	        """),format.raw/*222.15*/("""if(myloc.options[j].selected)"""),format.raw/*222.44*/("""{"""),format.raw/*222.45*/("""
     	            """),format.raw/*223.19*/("""if(myloc.options[j].value != "Select")"""),format.raw/*223.57*/("""{"""),format.raw/*223.58*/("""
     	        		"""),format.raw/*224.17*/("""locations = location + myloc.options[j].value + ",";
     	            """),format.raw/*225.19*/("""}"""),format.raw/*225.20*/("""else"""),format.raw/*225.24*/("""{"""),format.raw/*225.25*/("""
						"""),format.raw/*226.7*/("""locations ="";
     	            """),format.raw/*227.19*/("""}"""),format.raw/*227.20*/("""
     	        """),format.raw/*228.15*/("""}"""),format.raw/*228.16*/("""
     	    """),format.raw/*229.11*/("""}"""),format.raw/*229.12*/("""	   	
     	 """),format.raw/*230.8*/("""if(location.length > 0)"""),format.raw/*230.31*/("""{"""),format.raw/*230.32*/("""
     		 """),format.raw/*231.9*/("""locations = location.substring(0, myloc.length - 1);
          	
      	"""),format.raw/*233.8*/("""}"""),format.raw/*233.9*/("""else"""),format.raw/*233.13*/("""{"""),format.raw/*233.14*/("""
      		"""),format.raw/*234.9*/("""locations="";
      	"""),format.raw/*235.8*/("""}"""),format.raw/*235.9*/("""

     	 """),format.raw/*237.8*/("""myworkshop = document.getElementById('workshopname');
         console.log("here"+myworkshop);
     	 for (j=0;j<myworkshop.options.length;j++)"""),format.raw/*239.49*/("""{"""),format.raw/*239.50*/("""
     	        """),format.raw/*240.15*/("""if(myworkshop.options[j].selected)"""),format.raw/*240.49*/("""{"""),format.raw/*240.50*/("""
     	            """),format.raw/*241.19*/("""if(myworkshop.options[j].value != "Select")"""),format.raw/*241.62*/("""{"""),format.raw/*241.63*/("""
     	        		"""),format.raw/*242.17*/("""workshop = workshop + myworkshop.options[j].value + ",";
     	        		
     	            """),format.raw/*244.19*/("""}"""),format.raw/*244.20*/("""else"""),format.raw/*244.24*/("""{"""),format.raw/*244.25*/("""
     	        		"""),format.raw/*245.17*/("""workshop = "";
         	            
     	            """),format.raw/*247.19*/("""}"""),format.raw/*247.20*/("""
     	        """),format.raw/*248.15*/("""}"""),format.raw/*248.16*/("""
     	    """),format.raw/*249.11*/("""}"""),format.raw/*249.12*/("""	   	
     	 """),format.raw/*250.8*/("""if(workshop.length > 0)"""),format.raw/*250.31*/("""{"""),format.raw/*250.32*/("""
     		 """),format.raw/*251.9*/("""workshop = workshop.substring(0, myworkshop.length - 1);
          	
      	"""),format.raw/*253.8*/("""}"""),format.raw/*253.9*/("""else"""),format.raw/*253.13*/("""{"""),format.raw/*253.14*/("""
      		"""),format.raw/*254.9*/("""workshop="";
      	"""),format.raw/*255.8*/("""}"""),format.raw/*255.9*/("""
     	"""),format.raw/*256.7*/("""var urlLink = "/CREManager/getAllCallHistoryData"
    	    
    	    $('#datatableCallIntraction').dataTable( """),format.raw/*258.51*/("""{"""),format.raw/*258.52*/("""
    	     """),format.raw/*259.11*/(""""bDestroy": true,
    	     "processing": true,
    	     "serverSide": true,
    	      "scrollY": 400,
 	        "scrollX": true,
    	     "paging": true,
    	     "searching": false,
    	     "ordering":false,
    	     "ajax": """),format.raw/*267.19*/("""{"""),format.raw/*267.20*/("""
    		        """),format.raw/*268.15*/("""'type': 'POST',
    		        'url': urlLink,
    		        'data': """),format.raw/*270.23*/("""{"""),format.raw/*270.24*/("""    		        	
    		        	"""),format.raw/*271.16*/("""fromDate:''+fromDate,
    		        	toDate:''+toDate ,
    		        	UserIds:''+UserIds,
    		        	dispositions:''+dispositions,
    		        	locations:''+locations,
    		        	workshop:''+workshop,	        	
    		        	
    		        	
    		        """),format.raw/*279.15*/("""}"""),format.raw/*279.16*/("""
        	        
    	     """),format.raw/*281.11*/("""}"""),format.raw/*281.12*/(""",
    	     "fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*282.67*/("""{"""),format.raw/*282.68*/("""
                 """),format.raw/*283.18*/("""$('td', nRow).attr('nowrap','nowrap');
                 return nRow;
                 """),format.raw/*285.18*/("""}"""),format.raw/*285.19*/("""
    	 """),format.raw/*286.7*/("""}"""),format.raw/*286.8*/(""" """),format.raw/*286.9*/(""");


    """),format.raw/*289.5*/("""}"""),format.raw/*289.6*/(""");
"""),format.raw/*290.1*/("""}"""),format.raw/*290.2*/(""");



</script>
"""))
      }
    }
  }

  def render(fromDateValue:String,toDateValue:String,user:String,dealerName:String,allCRE:List[String],allDisposition:List[CallDispositionData],locations:List[String]): play.twirl.api.HtmlFormat.Appendable = apply(fromDateValue,toDateValue,user,dealerName,allCRE,allDisposition,locations)

  def f:((String,String,String,String,List[String],List[CallDispositionData],List[String]) => play.twirl.api.HtmlFormat.Appendable) = (fromDateValue,toDateValue,user,dealerName,allCRE,allDisposition,locations) => apply(fromDateValue,toDateValue,user,dealerName,allCRE,allDisposition,locations)

  def ref: this.type = this

}


}

/**/
object callInteractionsTable extends callInteractionsTable_Scope0.callInteractionsTable
              /*
                  -- GENERATED --
                  DATE: Thu Dec 28 15:54:30 IST 2017
                  SOURCE: D:/CRMNEW/28th Dec/crm/app/views/callInteractionsTable.scala.html
                  HASH: 29058c9ecb32aecee0bc4101fefb1f141d10ff9a
                  MATRIX: 850->3|1101->158|1131->163|1189->213|1228->215|1258->219|1334->269|1367->282|1445->334|1476->345|1694->536|1709->542|1795->619|1836->621|1867->625|2941->1672|2980->1695|3019->1696|3144->1793|3187->1809|3213->1814|3243->1817|3269->1822|3363->1885|3419->1913|4292->2759|4336->2787|4375->2788|4499->2884|4542->2900|4556->2905|4585->2913|4615->2916|4629->2921|4667->2938|4761->3001|4842->3054|5776->3958|5807->3962|9022->7146|9055->7151|9124->7191|9154->7192|9185->7195|9256->7237|9286->7238|9331->7255|9765->7660|9795->7661|9833->7671|9894->7703|9924->7704|9967->7718|10037->7759|10067->7760|10108->7772|10203->7838|10233->7839|10266->7843|10296->7844|10330->7850|10385->7876|10415->7877|10453->7887|10483->7888|10517->7894|10546->7895|10581->7902|10632->7924|10662->7925|10702->7936|10799->8005|10828->8006|10861->8010|10891->8011|10927->8019|10973->8037|11002->8038|11039->8047|11206->8185|11236->8186|11280->8201|11340->8232|11370->8233|11418->8252|11487->8292|11517->8293|11563->8310|11672->8390|11702->8391|11735->8395|11765->8396|11801->8404|11868->8442|11898->8443|11965->8481|11995->8482|12035->8493|12065->8494|12106->8507|12162->8534|12192->8535|12229->8544|12343->8630|12372->8631|12405->8635|12435->8636|12472->8645|12524->8669|12553->8670|12593->8682|12756->8816|12786->8817|12831->8833|12889->8862|12919->8863|12968->8883|13035->8921|13065->8922|13112->8940|13213->9012|13243->9013|13276->9017|13306->9018|13342->9026|13405->9060|13435->9061|13480->9077|13510->9078|13551->9090|13581->9091|13623->9105|13675->9128|13705->9129|13743->9139|13845->9213|13874->9214|13907->9218|13937->9219|13975->9229|14025->9251|14054->9252|14093->9263|14267->9408|14297->9409|14342->9425|14405->9459|14435->9460|14484->9480|14556->9523|14586->9524|14633->9542|14756->9636|14786->9637|14819->9641|14849->9642|14896->9660|14983->9718|15013->9719|15058->9735|15088->9736|15129->9748|15159->9749|15201->9763|15253->9786|15283->9787|15321->9797|15427->9875|15456->9876|15489->9880|15519->9881|15557->9891|15606->9912|15635->9913|15671->9921|15812->10033|15842->10034|15883->10046|16154->10288|16184->10289|16229->10305|16328->10375|16358->10376|16419->10408|16724->10684|16754->10685|16814->10716|16844->10717|16942->10786|16972->10787|17020->10806|17137->10894|17167->10895|17203->10903|17232->10904|17261->10905|17301->10917|17330->10918|17362->10922|17391->10923
                  LINES: 27->2|32->2|34->4|34->4|34->4|36->6|36->6|36->6|37->7|37->7|41->11|41->11|41->11|41->11|42->12|67->37|67->37|67->37|69->39|69->39|69->39|69->39|69->39|71->41|72->42|94->64|94->64|94->64|96->66|96->66|96->66|96->66|96->66|96->66|96->66|98->68|100->70|124->94|125->95|195->165|197->167|198->168|198->168|199->169|199->169|199->169|201->171|212->182|212->182|213->183|213->183|213->183|214->184|214->184|214->184|215->185|216->186|216->186|216->186|216->186|217->187|218->188|218->188|219->189|219->189|220->190|220->190|221->191|221->191|221->191|222->192|224->194|224->194|224->194|224->194|225->195|226->196|226->196|228->198|230->200|230->200|231->201|231->201|231->201|232->202|232->202|232->202|233->203|234->204|234->204|234->204|234->204|235->205|237->207|237->207|239->209|239->209|240->210|240->210|241->211|241->211|241->211|242->212|244->214|244->214|244->214|244->214|245->215|246->216|246->216|249->219|251->221|251->221|252->222|252->222|252->222|253->223|253->223|253->223|254->224|255->225|255->225|255->225|255->225|256->226|257->227|257->227|258->228|258->228|259->229|259->229|260->230|260->230|260->230|261->231|263->233|263->233|263->233|263->233|264->234|265->235|265->235|267->237|269->239|269->239|270->240|270->240|270->240|271->241|271->241|271->241|272->242|274->244|274->244|274->244|274->244|275->245|277->247|277->247|278->248|278->248|279->249|279->249|280->250|280->250|280->250|281->251|283->253|283->253|283->253|283->253|284->254|285->255|285->255|286->256|288->258|288->258|289->259|297->267|297->267|298->268|300->270|300->270|301->271|309->279|309->279|311->281|311->281|312->282|312->282|313->283|315->285|315->285|316->286|316->286|316->286|319->289|319->289|320->290|320->290
                  -- GENERATED --
              */
          