
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object uploadDateWiseForm_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class uploadDateWiseForm extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template8[HashMap[String, String],String,String,Date,Date,String,String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(alltypeOfReport:HashMap[String,String],uploadId:String,uploadType:String,minDate:Date,maxDate:Date,dealercode:String,dealerName:String,userName:String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.154*/("""
"""),_display_(/*2.2*/mainPageCREManger("AutoSherpaCRM",userName,dealerName)/*2.56*/ {_display_(Seq[Any](format.raw/*2.58*/("""
	
	"""),format.raw/*4.2*/("""<input type="hidden" id="reportTypeId" value=""""),_display_(/*4.49*/uploadType),format.raw/*4.59*/("""">
	<input type="hidden" id="reportUploadId" value=""""),_display_(/*5.51*/uploadId),format.raw/*5.59*/("""">
	
	<div class="panel panel-info">
   
    <div class="panel-body"> 
	
	 <div class="row">
	<div class="col-md-12" id="" style="">   
							<div class="col-lg-2">
							<label>Select File Type :</label>
							<div class="form-group">
                                
                                <select id="dataFileType" class="form-control selectpicker" value=""""),_display_(/*17.101*/uploadType),format.raw/*17.111*/("""" multiple>                                                                                                        
                                   <!--  <option value= "job_card">Job Card Status</option>
                                    <option value= "bill">Work Bills</option>
                                    <option value= "SMR" selected="selected">SMR</option>
                                    <option value= "campaign">Campaign</option>-->
                                    
                                    """),_display_(/*23.38*/for(key <- alltypeOfReport.keySet()) yield /*23.74*/{_display_(Seq[Any](format.raw/*23.75*/(""" 
                                    
                                    """),_display_(/*25.38*/{if(key == uploadType){
									
									<option value={key} selected="selected">{alltypeOfReport.get(key)}</option>
									}
									else {
										<option value={key} >{alltypeOfReport.get(key)}</option>
										}}),format.raw/*31.13*/("""
					  
                    
                    """)))}),format.raw/*34.22*/("""

                                """),format.raw/*36.33*/("""</select> 
                            </div>
                            </div>
                            
                            <div class="col-lg-2">
                            <div class="form-group">
                                <label>From Date :</label>
                                <input type='text' id="fromDate" placeholder="Enter From Date" class="form-control datepicker" value=""""),_display_(/*43.136*/minDate),format.raw/*43.143*/("""" required/>                               
                            </div>
                            </div>
                            
                             <div class="col-lg-2">
                             <div class="form-group">
                                <label>To Date :</label>
                                <input type='text' id="toDate" placeholder="Enter To Date" class="form-control range1datepicker" value=""""),_display_(/*50.138*/maxDate),format.raw/*50.145*/("""" required/>                               
                            </div>
                            </div>
                             
				            <div class="col-lg-3">
				            <div class="form-group">
				            <lable>&nbsp;</lable><br/>
				              <button type='button' class="btn btn-info" onclick="ajaxCallForUploadExcelReport();" >View</button>
				              </div>
				            </div>
            
                            
                            
                            </div>
                            </div>
                            </div>
                            </div>
	
	

<div class="panel panel-info">
    <div class="panel-heading"><strong>Upload Summary Table</strong></div>
    <div class="panel-body"> 
 


	  <div class="col-md-12" id="" style="">
	  <div class="box box-info">
      
            <!-- /.box-header -->
			
            <div class="box-body">
              
                <table id="uploadReport" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Report Name</th>
                <th>Recorded Date</th>
               	<th>City</th>
				<th>Location</th>				
				<th>Total Records</th>
				<th>Records Approved</th>
				<th>Records Rejected</th>
				<th>Uploaded File Name</th>
				<th>Upload by User</th>
				<th>Upload Date</th>
				<th>Upload Time</th>
				<th>Upload ID</th>
				
            </tr>
        </thead>
 
        <tbody>                   
                  </tbody>
                </table>
             
              <!-- /.table-responsive -->
            </div>
           
          </div>
		  </div>

        


    </div>
 </div>


    
	
    """)))}),format.raw/*121.6*/("""
    """),format.raw/*122.5*/("""<script src=""""),_display_(/*122.19*/routes/*122.25*/.Assets.at("javascripts/uploadReportAjax.js")),format.raw/*122.70*/("""" type="text/javascript"></script>    
    
	<script>
		   var reportFromUpload=document.getElementById("reportTypeId").value;

			if(reportFromUpload!="0")"""),format.raw/*127.29*/("""{"""),format.raw/*127.30*/("""
		   
		  """),format.raw/*129.5*/("""// alert("reportFromUpload not  zero: "+reportFromUpload);
		   $("#dataFileType").val(reportFromUpload);
		   window.onload = ajaxCallForUploadExcelReport;		   
			"""),format.raw/*132.4*/("""}"""),format.raw/*132.5*/("""

	"""),format.raw/*134.2*/("""</script>
    
 
	"""))
      }
    }
  }

  def render(alltypeOfReport:HashMap[String, String],uploadId:String,uploadType:String,minDate:Date,maxDate:Date,dealercode:String,dealerName:String,userName:String): play.twirl.api.HtmlFormat.Appendable = apply(alltypeOfReport,uploadId,uploadType,minDate,maxDate,dealercode,dealerName,userName)

  def f:((HashMap[String, String],String,String,Date,Date,String,String,String) => play.twirl.api.HtmlFormat.Appendable) = (alltypeOfReport,uploadId,uploadType,minDate,maxDate,dealercode,dealerName,userName) => apply(alltypeOfReport,uploadId,uploadType,minDate,maxDate,dealercode,dealerName,userName)

  def ref: this.type = this

}


}

/**/
object uploadDateWiseForm extends uploadDateWiseForm_Scope0.uploadDateWiseForm
              /*
                  -- GENERATED --
                  DATE: Thu Dec 28 15:54:31 IST 2017
                  SOURCE: D:/CRMNEW/28th Dec/crm/app/views/uploadDateWiseForm.scala.html
                  HASH: a2d176e7589605bf5db639fefab82fc9112902bb
                  MATRIX: 833->1|1081->153|1109->156|1171->210|1210->212|1242->218|1315->265|1345->275|1425->329|1453->337|1865->721|1897->731|2463->1270|2515->1306|2554->1307|2659->1385|2911->1616|2996->1670|3060->1706|3503->2121|3532->2128|4010->2578|4039->2585|5868->4383|5902->4389|5944->4403|5960->4409|6027->4454|6217->4615|6247->4616|6288->4629|6484->4797|6513->4798|6546->4803
                  LINES: 27->1|32->1|33->2|33->2|33->2|35->4|35->4|35->4|36->5|36->5|48->17|48->17|54->23|54->23|54->23|56->25|62->31|65->34|67->36|74->43|74->43|81->50|81->50|152->121|153->122|153->122|153->122|153->122|158->127|158->127|160->129|163->132|163->132|165->134
                  -- GENERATED --
              */
          