
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object commonDispositionPage_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class commonDispositionPage extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template20[String,List[String],Insurance,Long,String,String,String,Customer,Vehicle,List[Location],WyzUser,Service,List[SMSTemplate],List[String],List[ServiceTypes],CallInteraction,List[SpecialOfferMaster],String,Html,Html,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(oem:String,statesList:List[String],latestinsurance:Insurance,uniqueid:Long,dealercode:String,dealerName:String,userName:String,customerData:Customer,vehicleData:Vehicle,locationList:List[Location],userData:WyzUser,latestService:Service,smsTemplates:List[SMSTemplate],complaintOFCust:List[String],servicetypeList:List[ServiceTypes],interOfCall:CallInteraction,offersList:List[SpecialOfferMaster],typeDispo:String,dispoOut:Html,dispoInBound:Html):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.447*/("""

"""),_display_(/*3.2*/mainPageCRE("AutoSherpaCRM",userName,dealerName,dealercode,oem)/*3.65*/ {_display_(Seq[Any](format.raw/*3.67*/("""
"""),format.raw/*4.1*/("""<style>
	.ColorGreen"""),format.raw/*5.13*/("""{"""),format.raw/*5.14*/("""
		"""),format.raw/*6.3*/("""background-color:green;
	"""),format.raw/*7.2*/("""}"""),format.raw/*7.3*/("""
	
	"""),format.raw/*9.2*/(""".ColorRed"""),format.raw/*9.11*/("""{"""),format.raw/*9.12*/("""
		"""),format.raw/*10.3*/("""background-color:red;
	"""),format.raw/*11.2*/("""}"""),format.raw/*11.3*/("""
"""),format.raw/*12.1*/("""</style>

"""),_display_(/*14.2*/helper/*14.8*/.form(action=routes.InsuranceController.postCommonCallDispositionForm())/*14.80*/{_display_(Seq[Any](format.raw/*14.81*/("""
"""),format.raw/*15.1*/("""<link rel="stylesheet" href=""""),_display_(/*15.31*/routes/*15.37*/.Assets.at("css/cre.css")),format.raw/*15.62*/("""">
<input type="hidden" id="dndCust" value=""""),_display_(/*16.43*/{customerData.isDoNotDisturb()}),format.raw/*16.74*/("""">
<input type="hidden" id="typeOfDispoPageView" name="typeOfDispoPageView" value=""""),_display_(/*17.82*/typeDispo),format.raw/*17.91*/("""">
<input type="hidden" id="pFlag" name="pFlag" value=""""),_display_(/*18.54*/{if(customerData.getPreferredAdress()!=null){customerData.getPreferredAdress().getAddressType()}}),format.raw/*18.151*/("""">
<input type="hidden" name="customer_Id" id="customer_Id" value=""""),_display_(/*19.66*/{customerData.getId()}),format.raw/*19.88*/("""">
<input type="hidden" id="wyzUser_Id" name="wyzUser_Id" value=""""),_display_(/*20.64*/{userData.getId()}),format.raw/*20.82*/("""">
<input type="hidden" id="vehical_Id" name="vehicalId" value=""""),_display_(/*21.63*/{vehicleData.getVehicle_id()}),format.raw/*21.92*/("""">
<input type="hidden" name="uniqueidForCallSync" value=""""),_display_(/*22.57*/uniqueid),format.raw/*22.65*/("""">
<!-- <input type="hidden" id="workshopIdis" value=""""),_display_(/*23.53*/{if(latestService.getWorkshop()!=null){latestService.getWorkshop().getId()}}),format.raw/*23.129*/(""""> -->
<input type="hidden" id="workshopIdis" value="0">

<input type="hidden" id="location_Id" value=""""),_display_(/*26.47*/{userData.getLocation().getCityId()}),format.raw/*26.83*/("""">

<input type="hidden" name="callInteractionId" value=""""),_display_(/*28.55*/{interOfCall.getId()}),format.raw/*28.76*/("""">

<input type="hidden" id="srdispo_id" value=""""),_display_(/*30.46*/{if(interOfCall.getSrdisposition()!=null){interOfCall.getSrdisposition().getId()}}),format.raw/*30.128*/("""">
<input type="hidden" id="servicebook_id" name="serviceBookId" value=""""),_display_(/*31.71*/{if(interOfCall.getServiceBooked()!=null){interOfCall.getServiceBooked().getServiceBookedId()}}),format.raw/*31.166*/("""">
<input type="hidden" id="selectedFBComp" value=""""),_display_(/*32.50*/{if(interOfCall.getSrdisposition()!=null){interOfCall.getSrdisposition().getComplaintOrFB_TagTo()}}),format.raw/*32.149*/("""">			  
<input type="hidden" id="enteredRMFB" value=""""),_display_(/*33.47*/{if(interOfCall.getSrdisposition()!=null){interOfCall.getSrdisposition().getRemarksOfFB()}}),format.raw/*33.138*/("""">			  
<input type="hidden" id="depForFB" value=""""),_display_(/*34.44*/{if(interOfCall.getSrdisposition()!=null){interOfCall.getSrdisposition().getDepartmentForFB()}}),format.raw/*34.139*/("""">			  
<input type="hidden" id="selectedUpselOpp" value=""""),_display_(/*35.52*/{if(interOfCall.getSrdisposition()!=null){interOfCall.getSrdisposition().getUpsellCount()}}),format.raw/*35.143*/("""">
<input type="hidden" id="selectedModeOfCont" value=""""),_display_(/*36.54*/{if(interOfCall.getServiceBooked()!=null){interOfCall.getServiceBooked().getTypeOfPickup()}}),format.raw/*36.146*/("""">

	

			

<div class="row">
<!-- 4User div Starts -->
	<div class="col-md-4">
			  <div id="panelDemo10" class="panel panel-info custinfoData">
      <div class="panel-heading" style="text-align:center; padding: 5px;"> <b><i class="fa fa-user"></i>&nbsp;
        
        """),_display_(/*48.10*/{if(customerData.getCustomerName()!=null){
        
        customerData.getCustomerName();
        
        }else{
        
        customerData.getConcatinatedName();							
        
        }}),format.raw/*56.11*/(""" """),format.raw/*56.12*/("""</b><br />
        <b><i class="fa fa-user-circle">
		<input type="text" id="driverIdUpdate" style="border:none;background-color:#1797be;" value=""""),_display_(/*58.96*/{if(customerData.getUserDriver()!=null){
        
        customerData.getUserDriver();
        
        }}),format.raw/*62.11*/("""" readonly></i>
         </b></div>
      <div class="panel-body">
        <div class="row">
          <div class="col-sm-6 phoneselect">
            <div class="col-xs-3">
             <!--  <input type="button"  class="btn btn-primary" value=""""),_display_(/*68.74*/{customerData.getPreferredPhone().getPhoneNumber()}),format.raw/*68.125*/("""" id="ddl_phone_no" name="ddl_phone_no" style="cursor:default ;width: 150px;margin-left: -137px;font-size: medium;"> -->
			 <select class="btn btn-primary" id="ddl_phone_no" name="ddl_phone_no" style="cursor:default ;width: 150px;margin-left: -137px;font-size: medium;">
									"""),_display_(/*70.11*/for( phoneList <- customerData.getPhones()) yield /*70.54*/{_display_(Seq[Any](format.raw/*70.55*/("""
											
									"""),_display_(/*72.11*/{if(phoneList.isIsPreferredPhone ==true){
									
									if(phoneList.getPhoneNumber()!=null){
									<option value={phoneList.getPhoneNumber()} selected="selected">{phoneList.getPhoneNumber()}</option>
									}}else{ 
									
										if(phoneList.getPhoneNumber()!=null){
										<option value={phoneList.getPhoneNumber()} >{phoneList.getPhoneNumber()}</option>
										}}}),format.raw/*80.14*/("""
									""")))}),format.raw/*81.11*/("""						
								 """),format.raw/*82.10*/("""</select>
            </div>
            <button type="button" class="btn btn-success phoneinfobtn"  data-target="#addBtn" data-toggle="modal" data-backdrop="static" data-keyboard="false"><i class="fa fa-pencil-square-o fa-lg"></i></button>
            """),_display_(/*85.14*/if(typeDispo=="insurance")/*85.40*/{_display_(Seq[Any](format.raw/*85.41*/("""
            """),format.raw/*86.13*/("""<button type="button" class="btn btn-primary" id="" data-target="#InsuPremiumPopup" data-toggle="modal" data-backdrop="static" data-keyboard="false" onClick="ajaxODPercentage();"><i class="fa fa-calculator fa-lg"></i></button>
            """)))}),format.raw/*87.14*/("""
"""),format.raw/*88.1*/("""<button type="button" style="Display:none" id="DNDbtn" class="btn btn-danger">DND</button>			
            <!--<button type="button" class="btn btn-success test" id="btnDnd" data-target="#DNDConfirm" data-toggle="modal" data-backdrop="static" data-keyboard="false">DND</button>--> 
            
          </div>
          <div class="col-sm-12" style="margin-top: 10px;">
            <div class="col-xs-4">
              <input type="hidden" id="isCallinitaited" name="isCallinitaited" value="NotDailed">
              <input type="hidden" id="pref_number_callini" value=""""),_display_(/*95.69*/{customerData.getPreferredPhone().getPhone_Id()}),format.raw/*95.117*/("""">
              <button type="button" class="btn btn-primary btn-lg" onclick ="callfunctionFromPage('"""),_display_(/*96.101*/{customerData.getPreferredPhone().getPhone_Id()}),format.raw/*96.149*/("""','"""),_display_(/*96.153*/uniqueid),format.raw/*96.161*/("""','"""),_display_(/*96.165*/customerData/*96.177*/.getId()),format.raw/*96.185*/("""');"  style="border-radius: 29px;" ><i class="fa fa-phone-square fa-2x" aria-hidden="true" title="Make Call" ></i></button>
            </div>
            <div class="col-xs-4">
              <button type="button" class="btn btn-primary btn-lg " style="border-radius: 29px;"><i class="fa fa-envelope-square fa-2x" aria-hidden="true"></i></button>
            </div>
            <div class="col-xs-4">
              <button type="button" data-toggle="modal" data-target="#smsPopuId" data-backdrop="static" data-keyboard="false" class="btn btn-primary  btn-lg" style="border-radius: 29px;"><i class="fa fa-comment fa-2x" title="SMS" ></i></button>
            </div>
          </div>
          <div class="col-sm-12" style="margin-top: 6px;">
            <div class="row">
              <div class="col-xs-12" > <span><b><u>Email:</u>&nbsp; </b></span> <span style="font-size:15px" id="ddl_email"> """),_display_(/*107.127*/{if(customerData.getPreferredEmail()!=null){
                customerData.getPreferredEmail().getEmailAddress()
                }}),format.raw/*109.19*/(""" """),format.raw/*109.20*/("""</span> </div>
            </div>
            <div class="row">
              <div class="col-xs-6"> <span><b><u>DOA:</u>&nbsp; </b></span> <span>"""),_display_(/*112.84*/{customerData.getAnniversaryDateStr()}),format.raw/*112.122*/("""</span> </div>
              <div class="col-xs-6" > <span><b><u>DOB:</u>&nbsp; </b></span> <span>"""),_display_(/*113.85*/{customerData.getDobStr()}),format.raw/*113.111*/("""</span> </div>
            </div>
          </div>
          <div class="col-sm-12">
            <div class="row">
              <div class="col-xs-12"> <span><b><u>Address:</u>&nbsp; </b></span> <span class="addressBlog" id="prefAdressUpdate"> """),_display_(/*118.132*/{if(customerData.getPreferredAdress()!=null){
                if(customerData.getPreferredAdress().getConcatenatedAdress()!=null){
                customerData.getPreferredAdress().getConcatenatedAdress();
                }else{
                if(customerData.getConcatinatedPrefAddree()!=null){
                customerData.getConcatinatedPrefAddree();
                }    
                }
				}}),format.raw/*126.7*/(""" """),format.raw/*126.8*/("""</span> </div>
            </div>
          </div>
          <div class="col-xs-12">
            <div class="row">
              <div class="col-xs-6"> <b><u>Pref Time:</u>&nbsp;</b><span>"""),_display_(/*131.75*/{customerData.getPreferred_time_start()}),format.raw/*131.115*/(""" """),format.raw/*131.116*/("""To """),_display_(/*131.120*/{customerData.getPreferred_time_end()}),format.raw/*131.158*/("""</span> </div>
              <div class="col-xs-6"> <b><u>Pref Contact:</u>&nbsp;</b> <span>"""),_display_(/*132.79*/{customerData.getMode_of_contact()}),format.raw/*132.114*/("""</span> </div>
            </div>
          </div>
		  <div class="col-xs-12">
            <div class="row">
              <div class="col-xs-12"> <b><u>Pref Day:</u>&nbsp;</b> <span>"""),_display_(/*137.76*/{customerData.getPreferred_day()}),format.raw/*137.109*/("""</span> </div>
              
            </div>
          </div>
          <!--button-->
          <div class="col-sm-12">
            <div class="row">
              <div class="col-sm-6"> """),_display_(/*144.39*/if(customerData.getSegment()!=null)/*144.74*/{_display_(Seq[Any](format.raw/*144.75*/("""
                
                """),_display_(/*146.18*/for(segmentIterate <- customerData.getSegment()) yield /*146.66*/{_display_(Seq[Any](format.raw/*146.67*/("""
                """),format.raw/*147.17*/("""<button type="button" class="btn btn-info btn-flat btn-block loya" style="cursor:default;"><b style="font-size:15px"> """),_display_(/*147.136*/if(segmentIterate.getName()!="" && segmentIterate.getName()!=null)/*147.202*/{_display_(Seq[Any](format.raw/*147.203*/("""
                """),_display_(/*148.18*/segmentIterate/*148.32*/.getName()),format.raw/*148.42*/("""
                """)))}/*149.18*/else/*149.22*/{_display_(Seq[Any](format.raw/*149.23*/("""
                """),format.raw/*150.17*/("""NA
                """)))}),format.raw/*151.18*/(""" """),format.raw/*151.19*/("""</b></button>
                """)))}),format.raw/*152.18*/("""
                """)))}),format.raw/*153.18*/("""
				"""),format.raw/*154.5*/("""</div>
            </div>
          </div>
        </div>
      </div>
    </div>
			   </div>
		  <div class="col-md-8" style="padding: 0px;">
							<div class="col-sm-4" style="padding: 0px;">
			<!-- START panel-->
					<div id="panelDemo10" class="panel panel-info">
					<div class="panel-heading"><b>VEHICLE&nbsp;INFO</b><img src=""""),_display_(/*165.68*/routes/*165.74*/.Assets.at("images/car.png")),format.raw/*165.102*/("""" class="pull-right" style="width:28px;"></img></div>
						<div class="panel-body" style="height: 110px;overflow: auto;padding: 5px;">
						"""),_display_(/*167.8*/if(vehicleData!=null)/*167.29*/{_display_(Seq[Any](format.raw/*167.30*/("""
		
						
						"""),format.raw/*170.7*/("""<span><b>"""),_display_(/*170.17*/{vehicleData.getChassisNo()}),format.raw/*170.45*/("""</b></span>&nbsp;|&nbsp;<span><b>"""),_display_(/*170.79*/{vehicleData.getVehicleRegNo()}),format.raw/*170.110*/("""</b></span>&nbsp;|&nbsp;<span><b>"""),_display_(/*170.144*/{vehicleData.getModel()}),format.raw/*170.168*/("""</b></span></br>
						<span><b>"""),_display_(/*171.17*/{vehicleData.getVariant()}),format.raw/*171.43*/("""</b>&nbsp;|&nbsp;<b>"""),_display_(/*171.64*/{vehicleData.getColor()}),format.raw/*171.88*/("""</b></span></br>
						<span>MILEAGE(Kms):&nbsp;<b>"""),_display_(/*172.36*/{latestService.getServiceOdometerReading()}),format.raw/*172.79*/("""</b></span></br>
						<span>SALE&nbsp;DATE:&nbsp;<b class="servi" style="background-color: #f05050; color: #fff;border-radius: 3px;   font-size: 13px;border-color: transparent;padding:2px">"""),_display_(/*173.175*/{vehicleData.getSaleDateStr()}),format.raw/*173.205*/("""</b></b></span>
										
						
				""")))}),format.raw/*176.6*/("""
						"""),format.raw/*177.7*/("""</div>
									</div>
									<!-- END panel-->
							</div>
<div class="col-sm-4" style="padding: 0px;">
<!-- START panel-->
<div id="panelDemo10" class="panel panel-info">
<div class="panel-heading specialoffer"><b>NEXT&nbsp;SERVICE&nbsp;DUE</b>
<!-- <div class="wrapper">
       <div class="ribbon-wrapper-green"><div class="ribbon-green"  data-toggle="modal" data-target="#offer" data-backdrop="static" data-keyboard="false">Special <br/> Offer</div></div>
</div> -->

<div class="pull-right">
<img src="/assets/images/specialoffer.png"  style="width: 80px; height: 80px; margin-top: -11px; margin-right: -17px;" data-toggle="modal" data-target="#offer" data-backdrop="static" data-keyboard="false">
</div>
</div>
<div class="panel-body" style="height: 110px;overflow-x:hidden;padding: 5px;">
	
		
						

<span><b class="servi" style="background-color: #f05050; color: #fff;border-radius: 3px;font-size: 13px;border-color: transparent;padding:2px">"""),_display_(/*198.145*/{vehicleData.getNextServicedateStr()}),format.raw/*198.182*/(""" """),format.raw/*198.183*/("""</b>&nbsp;|&nbsp;<b>"""),_display_(/*198.204*/{vehicleData.getNextServicetype()}),format.raw/*198.238*/("""</b></span><br>

<span>FORECAST&nbsp;LOGIC: &nbsp;<b>"""),_display_(/*200.38*/{vehicleData.getForecastLogic()}),format.raw/*200.70*/("""</b></span><br>
<span>AVG&nbsp;RUNNING/MONTH(Kms):&nbsp;<b></b> </span></br>
<span>NO&nbsp;SHOW(Months):&nbsp;<b>"""),_display_(/*202.38*/{vehicleData.getRunningBetweenvisits()}),format.raw/*202.77*/("""</b> </span>



</div>
</div>

</div>
							
<div class="col-sm-4" style="padding: 0px;">
<!-- START panel-->
<div id="panelDemo10" class="panel panel-info">
<div class="panel-heading"><b>LAST&nbsp;SERVICE&nbsp;STATUS</b><img src=""""),_display_(/*214.75*/routes/*214.81*/.Assets.at("images/car-insurance.png")),format.raw/*214.119*/("""" class="pull-right" style="width:28px;"></img></div>
<div class="panel-body" style="height: 110px;overflow: auto;padding: 5px;">
"""),_display_(/*216.2*/if(vehicleData!=null)/*216.23*/{_display_(Seq[Any](format.raw/*216.24*/("""
"""),format.raw/*217.1*/("""<span><b style="background-color: #f05050; color: #fff;border-radius: 3px;   font-size: 13px;border-color: transparent;padding:2px">"""),_display_(/*217.134*/latestService/*217.147*/.getLastServiceDateStr()),format.raw/*217.171*/("""</b> &nbsp;|&nbsp;<b>"""),_display_(/*217.193*/latestService/*217.206*/.getLastServiceType()),format.raw/*217.227*/("""</b></span><br>
<span>WORKSHOP:&nbsp;<b>
"""),_display_(/*219.2*/{if(latestService.getWorkshop()!=null){
latestService.getWorkshop().getWorkshopName()
}
}),format.raw/*222.2*/("""
""")))}),format.raw/*223.2*/("""

"""),format.raw/*225.1*/("""</b></span><br>
<span>PSF STATUS :&nbsp;<b>"""),_display_(/*226.29*/latestService/*226.42*/.getLastPSFStatus()),format.raw/*226.61*/("""</b></span><br>
<span>PSF DATE :&nbsp;<b></b></span>
</div>

</div>
<!-- END panel-->
</div>

<div class="col-sm-4" style="padding: 0px;">
<!-- START panel-->
<div id="panelDemo10" class="panel panel-info">
<div class="panel-heading"><b>INSURANCE</b><img src=""""),_display_(/*237.55*/routes/*237.61*/.Assets.at("images/truck.png")),format.raw/*237.91*/("""" class="pull-right" style="width:28px;"></img></div>
<div class="panel-body" style="height: 110px;overflow: auto;padding: 5px;">

		

<span>EXPIRES&nbsp;ON:&nbsp;<b class="servi" style="background-color: #f05050;color: #fff;border-radius: 3px;   font-size: 13px;border-color: transparent;">"""),_display_(/*242.158*/{latestinsurance.getPolicyDueDateStr()}),format.raw/*242.197*/("""</b></span><br>
<span style="font-size:13px">NEW&nbsp;INDIA&nbsp;ASSURANCE:&nbsp; <b>"""),_display_(/*243.71*/{latestinsurance.getInsuranceCompanyName()}),format.raw/*243.114*/("""</b></span><br>
<span style="font-size:13px">POLICY&nbsp;NO:&nbsp;<b>"""),_display_(/*244.55*/{latestinsurance.getPolicyNo()}),format.raw/*244.86*/("""</b></span><br>
<span style="font-size:13px">PREMIUM(Rs.):&nbsp;<b>"""),_display_(/*245.53*/{latestinsurance.getPremiumAmountAfterTax()}),format.raw/*245.97*/("""</b></span>


</div>

</div>
<!-- END panel-->
</div>
<div class="col-sm-4" style="padding: 0px;">
<!-- START panel-->
<div id="panelDemo10" class="panel panel-info">
<div class="panel-heading"><b>OTHER&nbsp;UPSELL</b><img src=""""),_display_(/*256.63*/routes/*256.69*/.Assets.at("images/warranty-certificate.png")),format.raw/*256.114*/("""" class="pull-right" style="width:28px;"></img></div>
<div class="panel-body" style="height: 110px;overflow: auto;padding: 5px;">
<span>EW&nbsp;DUE:&nbsp;<b class="servi" style="background-color: #f05050;color: #fff;border-radius: 3px; font-size: 13px;
border-color: transparent;">"""),_display_(/*259.30*/{vehicleData.getExtendedWarentyDue()}),format.raw/*259.67*/("""</b></span><br>
<span>WARRANTY&nbsp;DUE:&nbsp;<b class="servi" style="font-size: 13px;"></b></span><br>
<span style="font-size: 13px;">EXCHANGE:&nbsp;<b>"""),_display_(/*261.51*/{vehicleData.getExchange()}),format.raw/*261.78*/("""</b></span></br>
<span>&nbsp;</span>


</div>

</div>
<!-- END panel-->
</div>
<div class="col-sm-4" style="padding: 0px;">
<!-- START panel-->
<div id="panelDemo10" class="panel panel-info">
<div class="panel-heading"><b>COMPLAINTS</b><img src=""""),_display_(/*273.56*/routes/*273.62*/.Assets.at("images/businessman.png")),format.raw/*273.98*/("""" class="pull-right" style="width:28px;"></img></div>
<div class="panel-body" style="height: 110px;overflow: auto;padding: 5px;">
<span>OPEN:<b class="servi" style="background-color: #f05050; margin-left: 15px; color: #fff;border-radius: 3px; font-size: 17px;    border-color: transparent;">"""),_display_(/*275.163*/complaintOFCust/*275.178*/.get(0)),format.raw/*275.185*/("""</b></span><br>
<span> CLOSED:<b class="servi" style="background-color: #27c24c; color: #fff;border-radius: 3px;     font-size: 13px;
border-color: transparent;">"""),_display_(/*277.30*/complaintOFCust/*277.45*/.get(1)),format.raw/*277.52*/("""</b></span><br>
<span style="font-size: 14PX;">LAST&nbsp;RAISED&nbsp;ON:&nbsp;<b>"""),_display_(/*278.67*/complaintOFCust/*278.82*/.get(2)),format.raw/*278.89*/("""</b></span><br>
<span style="font-size: 13px;">RESOLVED&nbsp;BY:&nbsp;<b>"""),_display_(/*279.59*/complaintOFCust/*279.74*/.get(3)),format.raw/*279.81*/("""</b>&nbsp;</span>

</div>

</div>
<!-- END panel-->
</div>
			</div>
   <div class="modal fade" id="addBtn" role="dialog">
								<div class="modal-dialog modal-lg">

									<!-- Modal content-->
									<div class="modal-content">
										<div class="modal-header" style="padding:1px;text-align:center;color:red;">
											<button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*293.81*/routes/*293.87*/.Assets.at("images/close1.png")),format.raw/*293.118*/(""""></button>
											<h5 class="modal-title">UPDATE CUSTOMER INFORMATION</h5>
										</div>
										
								 <div class="modal-body">
					 <div class="row">
						  <div class="col-sm-12" style="margin-top: 5px;">
								<div class="col-sm-3">
								  <label><b>Owner:</b></label>
								  <input type="text" class="form-control textOnlyAccepted" id="customerNameEdit" value=""""),_display_(/*302.98*/customerData/*302.110*/.getCustomerName()),format.raw/*302.128*/("""" name="" readonly>
								  <br/>
								</div>
								
								<div class="col-sm-3">
								  <label><b>User/Driver:</b></label>
								  <input type="text" class="form-control textOnlyAccepted" id="driverNameEdit" value=""""),_display_(/*308.96*/customerData/*308.108*/.getUserDriver()),format.raw/*308.124*/("""" name="">
								  <br/>
								</div>
								<div class="col-sm-3">
								  <label><b>DND</b></label><br />
								  
								<label class="radio-inline"><input type="radio" name="doNotDisturb" id="dndcheckbox1" value="true">YES</label>
								<label class="radio-inline"><input type="radio" name="doNotDisturb" id="dndcheckbox2" value="false" checked>NO</label>
								 
								</div>
								
								
							  </div>
							  </br>
							  <div class="col-sm-12">
								<div class="col-sm-3">
								  <label><b>Permanent Address:</b></label>
								  <textarea class="form-control permanentAddress" rows="2" id="permanentAddress" name="permanentAddress" readonly >"""),_display_(/*325.125*/{if(customerData.getPermanentAddressData()!=null){
	  
	  customerData.getPermanentAddressData().getAddressLine1();
	  
	  }}),format.raw/*329.6*/("""_
	  """),_display_(/*330.5*/{if(customerData.getPermanentAddressData()!=null){
	  
	  customerData.getPermanentAddressData().getAddressLine2();
	  
	  }}),format.raw/*334.6*/("""_
	  """),_display_(/*335.5*/{if(customerData.getPermanentAddressData()!=null){
	  
	  customerData.getPermanentAddressData().getAddressLine3();
	  
	  }}),format.raw/*339.6*/("""</textarea>
								
								</div>
								<div class="col-sm-1">
								  <br/>
								  <button type="button" class="btn btn-success peditAddressmodal" data-toggle="modal" data-target="#driver_modal1" style="margin-top: 5px;">Add</button>
								</div>
								<div class="col-md-3">
								  <label><b>Residence Address:</b></label>
								  <textarea class="form-control" rows="2" id="residenceAddress" name="residenceAddress" readonly>"""),_display_(/*348.107*/{if(customerData.getResidenceAddressData()!=null){
	  
	  customerData.getResidenceAddressData().getAddressLine1();
	  
	  }}),format.raw/*352.6*/("""_
	  """),_display_(/*353.5*/{if(customerData.getResidenceAddressData()!=null){
	  
	  customerData.getResidenceAddressData().getAddressLine2();
	  
	  }}),format.raw/*357.6*/("""_
	  """),_display_(/*358.5*/{if(customerData.getResidenceAddressData()!=null){
	  
	  customerData.getResidenceAddressData().getAddressLine3();
	  
	  }}),format.raw/*362.6*/("""</textarea>
								</div>
								<div class="col-md-1">
								  <br/>
								  <button type="button" class="btn btn-success reditAddressmodal"  data-toggle="modal" data-target="#driver_modal2">Add</button>
								</div>
								<div class="col-md-3">
								  <label><b>Office Address:</b></label>
								  <textarea class="form-control" rows="2" id="officeAddress" name="officeAddress" readonly >"""),_display_(/*370.102*/{if(customerData.getOfficeAddressData()!=null){
	  
	  customerData.getOfficeAddressData().getAddressLine1();
	  
	  }}),format.raw/*374.6*/("""_
	  """),_display_(/*375.5*/{if(customerData.getOfficeAddressData()!=null){
	  
	  customerData.getOfficeAddressData().getAddressLine2();
	  
	  }}),format.raw/*379.6*/("""_
	  """),_display_(/*380.5*/{if(customerData.getOfficeAddressData()!=null){
	  
	  customerData.getOfficeAddressData().getAddressLine3();
	  
	  }}),format.raw/*384.6*/("""</textarea>
								</div>
								<div class="col-md-1">
								  <br/>
								  <button type="button" class="btn btn-success oeditAddressmodal" data-toggle="modal" data-target="#driver_modal3" >Add</button>
								</div>
							  </div>
							  <hr>
							  <div class="col-sm-12">
								<h5 style="color:Red;"><b>PREFERRED COMMUNICATION ADDRESS</b></h5>
								<div class="col-md-4">
								  <input type="radio"  id="checkbox1" name="ADDRESS" value="0">
								  Permanent Address</div>
								<div class="col-md-4">
								  <input type="radio"  id="checkbox2" name="ADDRESS" value="2" >
								  Residence Address</div>
								<div class="col-md-4">
								  <input type="radio"  id="checkbox3" name="ADDRESS" value="1" >
								  Office Address</div>
							  </div>
							   <hr>
							  <div class="col-sm-12" style="margin-top: 5px;">
								<div class="col-sm-3">
								  <label><b>Preferred Email:</b></label>
								  <select class="form-control" id="email" name="email">
									"""),_display_(/*409.11*/for( emailList <- customerData.getEmails()) yield /*409.54*/{_display_(Seq[Any](format.raw/*409.55*/("""
											
									"""),_display_(/*411.11*/{if(emailList.isIsPreferredEmail() ==true)
									
									<option value={emailList.getEmailAddress()} selected="selected">{emailList.getEmailAddress()}</option>
									else 
										<option value={emailList.getEmailAddress()} >{emailList.getEmailAddress()}</option>
										}),format.raw/*416.12*/("""
									""")))}),format.raw/*417.11*/("""						
								 """),format.raw/*418.10*/("""</select>
								</div>
								<div class="col-xs-1">
								  <br/>
								  <button type="button" class="btn btn-success oeditEmailmodal" data-toggle="modal" data-target="#driver_modal4" style="margin-top: 5px;">Add</button>
								</div>
								<div class="col-sm-3">
								  <label><b>Date OF Birth:</b></label>
								  <input type="text" class="datepicMYDropDown form-control" id="dob" value=""""),_display_(/*426.86*/{customerData.getDob()}),format.raw/*426.109*/("""" name="dob" readonly>
								</div>
								<div class="col-md-3">
								  <label><b>Date of Anniversary</b></label>
								  <input type="text" class="datepicMYDropDown form-control" id="anniversary_date" value=""""),_display_(/*430.99*/{customerData.getAnniversary_date()}),format.raw/*430.135*/("""" name="anniversary_date" readonly>
								</div>
							  </div>
							   <hr>
							  <div class="col-sm-12" style="margin-top: 5px;">
								<h5 style="color:Red;"><b>MANAGE PREFERENCES</b></h5>
								<div class="col-sm-3">
								  <label><b>Preferred Contact #:</b></label>
								  <select class="form-control" id="preffered_contact_num" name="preffered_contact_num">
									"""),_display_(/*439.11*/for( phoneList <- customerData.getPhones()) yield /*439.54*/{_display_(Seq[Any](format.raw/*439.55*/("""
											
									"""),_display_(/*441.11*/{if(phoneList.isIsPreferredPhone ==true)
									
									<option value={phoneList.phone_Id.toString()} selected="selected">{phoneList.getPhoneNumber()}</option>
									else 
										<option value={phoneList.phone_Id.toString()} >{phoneList.getPhoneNumber()}</option>
										}),format.raw/*446.12*/("""
									""")))}),format.raw/*447.11*/("""						
								 """),format.raw/*448.10*/("""</select>
								  
								</div>
								<div class="col-sm-1">
								<br />
								 <input type="button"  class="btn btn-success"  id="tanniversary_edit" data-toggle="modal" data-target="#myModalAddPhone" value="Add">
										   </div>
								<div class="col-md-3">
								  <label><b>Preferred Mode of Contact:</b></label>
								  <div class="col-sm-12" id="modeOfCon"  style="display:none">
									<select class="form-control" id="preffered_mode_contact" name="preffered_mode_contact" multiple="multiple" >
									  <option value="Email">Email</option>
									  <option value="Phone">Phone</option>
									  <option value="SMS">SMS</option>
									</select>
								  </div>
								  <input type="text" class="form-control" id="tpreffered_mode_contact"  value=""""),_display_(/*464.89*/{customerData.getMode_of_contact()}),format.raw/*464.124*/("""" readonly>
								</div>
								<div class="col-md-1">
								<br />
								  <input type="button"  class="btn btn-success"  id="tpreffered_mode_contact_edit" value="Add">
								  
								</div>
								<div class="col-md-3">
								  <label><b>Preferred Day to Contact:</b></label>
								  <div class="col-sm-12" id="daysWeek" style="display:none">
									<select class="form-control" id="preffered_day_contact" name="preffered_day_contact" multiple="multiple">
									  <option value="Sunday">Sunday</option>
									  <option value="Monday">Monday</option>
									  <option value="Tuesday">Tuesday</option>
									  <option value="Wednesday">Wednesday</option>
									  <option value="Thursday">Thursday</option>
									  <option value="Friday">Friday</option>
									  <option value="Saturday">Saturday</option>
									</select>
								  </div>
								  <input type="text" class="form-control" id="tpreffered_day_contact"  value=""""),_display_(/*484.88*/{customerData.getPreferred_day()}),format.raw/*484.121*/("""" readonly>
								</div>
								<div class="col-md-1">
								<br />
								  <input type="button"  class="btn btn-success"  id="tpreffered_day_contact_edit" value="Add">
								</div>
							  </div>
							  <br>
							   <hr>
							  <div class="col-sm-12" style="margin-top: 5px;">
								<div class="col-sm-3">
								  <label><b>Preferred Time Of Contact(Start Time):</b></label>
								  <input type="text" class="form-control single-input" id="dateStarttime" name="dateStarttime" value=""""),_display_(/*496.112*/{customerData.getPreferred_time_start()}),format.raw/*496.152*/("""" readonly>
								</div>
								<div class="col-md-3">
								  <label><b>Preferred Time Of Contact(End Time):</b></label>
								  <input type="text" class="form-control single-input" id="dateEndtime" name="dateEndtime" value=""""),_display_(/*500.108*/{customerData.getPreferred_time_end()}),format.raw/*500.146*/("""" readonly>
								</div>
								<div class="col-sm-6">
								  <label><b>Comments:(250 Characters Only)</b></label>
								  <textarea type="text" class="form-control" id="custEditComment" rows="2" maxlength="250" name="custEditComment" value=""""),_display_(/*504.131*/{customerData.getComments()}),format.raw/*504.159*/("""">"""),_display_(/*504.162*/{customerData.getComments()}),format.raw/*504.190*/("""</textarea>
								</div>
							  </div>
							 
							</div>
				</div>

										<div class="modal-footer">
											<button type="button" id="pn_save" class="btn btn-success" onClick="ajaxCallForAddcustomerinfo();" data-dismiss="modal">Save</button>

										</div>
									</div>

								</div>
							</div>
		  
			  
		  <!-- 4User Div End -->
   


</div>

<!--Shasi dispositio-->
<div class="row">
<div class="col-lg-12 disposit">
<div class="panel panel-primary">
<div class="panel-heading " align="center"; style="background-color:#1797be;" ><img src=""""),_display_(/*531.91*/routes/*531.97*/.Assets.at("images/phone1.png")),format.raw/*531.128*/("""" style="width:17px"/>&nbsp;<b>DISPOSITION FORM</b> 
</div>
<div class="panel-body disposit">
<div class="row animated  bounceInRight">

<div class="col-sm-12" id="SMRInteractionFirst" >
		  <label for=""><h4>Select The Mode of Interaction :-&nbsp;&nbsp;</h4></label>
			 
			  <div class="radio-inline">
				<label>
				  <input type="radio" name="InOutCallName" value="OutCall" checked id="OutGoingID">
				  Outbound Call </label>
			  </div>
			  <div class="radio-inline">
				<label>
				  <input type="radio" name="InOutCallName" value="InCall" id="" >
				  Inbound Call</label>
			  </div>
		</div>
		<div>
		
		"""),_display_(/*552.4*/dispoInBound),format.raw/*552.16*/("""
		
		 """),format.raw/*554.4*/("""</div>
	  <div class="col-md-12 animated  bounceInRight" id="OutBoundDiv" style="display:none;">
		<div class="form-group">
		<div id="DidYouTalkDiv">
		  <label for=""><b>Did you talk to the customer ?</b></label>
		  <br>
		  <div class="radio-inline">
			<label>
			  <input type="radio" name="typeOfDisposition" value="Contact" id="SpeakYes">
			  Yes </label>
		  </div>
		  <div class="radio-inline">
			<label>
			  <input type="radio" name="typeOfDisposition" value="NonContact" id="SpeakNo" >
			  No</label>
		  </div>
		 </div>
		  <!--No-->
		  
		  <div style="display:none;" class="animated  bounceInRight" id="NotSpeachDiv"><br>
			<label for=""><b>Choose reason for not being able to talk to customer:</b></label>
			<br>
			<div class="radio">
			  <label>
				<input type="radio" name="disposition" value="Ringing No response" id="">
				Ringing No response </label>
			</div>
			<div class="radio">
			  <label>
				<input type="radio" name="disposition" value="Wrong Number" id="">
				Wrong Number</label>
			</div>
			<div class="radio">
			  <label>
				<input type="radio" name="disposition" value="Busy" id="" >
				Busy</label>
			</div>
			<div class="radio">
			  <label>
				<input type="radio" name="disposition" value="Switched Off / Unreachable" id="" >
				Switched Off / Unreachable</label>
			</div>
			<div class="radio">
			  <label>
				<input type="radio" name="disposition" value="Invalid Number" id="" >
				Invalid Number</label>
			</div>
			<div class="radio">
			  <label>
				<input type="radio" name="disposition" value="NoOther" id="NOOthersCheck" >
				Other</label>
			</div>
			
			
			<div class="row animated  bounceInRight" style="display:none;"  id="NoOthers">
			<div class="col-md-3">
			<label><b>Please Specify</b></label>
			<textarea type="text" class="form-control NoOthersText1" rows="1" name="other"></textarea>
			</div>
			</div>
			<div class="pull-right">
					<button type="button" class="btn btn-info" id="DidUTalkNO" style="background-color:#1797BE">Back</button>						
					<button type="submit" class="btn btn-info" name="typeOfsubmit" style="background-color:#1797BE" id="nonContactValidation" value="nonContact">Submit</button>
			</div>
		  </div>
		  <!--Book My Service-->
		  <div style="display:none;" class="animated  bounceInRight whatDidCustSayDiv" id="WhatdidtheCustomersayDIV" >
		  <div id="whatDidCustSayDiv">
			<label for=""><b>What did the customer say?</b></label>
			<br>
			
		"""),_display_(/*625.4*/if(typeDispo=="insurance")/*625.30*/{_display_(Seq[Any](format.raw/*625.31*/("""	
			
			"""),format.raw/*627.4*/("""<div class="radio-inline">
			  <label>
				<input type="radio" name="disposition" value="Book Appointment" id="BookMyAppointment" >
				Book Appointment </label>
			</div>
			<div class="radio-inline">
			  <label>
				<input type="radio" name="disposition" value="Renewal Not Required" id="RenewalNotRequired" >
				Renewal Not Required</label>
			</div>
			
		
			""")))}),format.raw/*639.5*/("""

			"""),_display_(/*641.5*/if(interOfCall.getServiceBooked()==null && typeDispo != "insurance")/*641.73*/{_display_(Seq[Any](format.raw/*641.74*/(""" 
						
			"""),format.raw/*643.4*/("""<div class="radio-inline">
			  <label>
				<input type="radio" name="disposition" value="Book My Service" id="BookMyService" >
				Book My Service </label>
			</div>
			
			<div class="radio-inline">
			  <label>
				<input type="radio" name="disposition" value="Service Not Required" id="ServiceNotRequired" >
				Service Not Required</label>
			</div>
				
		""")))}),format.raw/*655.4*/("""	"""),_display_(/*655.6*/if(interOfCall.getServiceBooked() !=null && typeDispo != "insurance")/*655.75*/{_display_(Seq[Any](format.raw/*655.76*/(""" 
		"""),format.raw/*656.3*/("""<div class="radio-inline" >
			  <label>
				<input type="radio" name="disposition" value="Confirmed" id="ConfirmedId" >
				Confirmed </label>
			</div>
			
		<div class="radio-inline">
			  <label>
				<input type="radio" name="disposition" value="Book My Service" id="BookMyService" >
				Reschedule </label>
			</div>
				
			
			<div class="radio-inline">
			  <label>
				<input type="radio" name="disposition" value="Cancelled" id="CancelId" >
				Cancel</label>
			</div>
		""")))}),format.raw/*674.4*/("""
		"""),format.raw/*675.3*/("""<div class="radio-inline">
			  <label>
				<input type="radio" name="disposition" value="Call Me Later" id="CallMeLatter" >
				Call Me Later</label>
			</div>
			
		
		

		
			</div>
		  </div>
		  <div>

		  
		   """),_display_(/*690.7*/dispoOut),format.raw/*690.15*/(""" 		  
		
		  """),format.raw/*692.5*/("""<!--call me latter-->
		 </div>
		  </div>
		
		  </div>
		 </div>
		 </div>
		 </div>
		 </div>
		 <!-----------INTERACTIONH HISTORY--------->
		 <div class="col-lg-12">
	<div class="panel panel-primary">
	<div class="panel-heading " align="center"; style="background-color:#1797be;" data-toggle="collapse" data-parent="#accordion" data-target="#collapseOne3"><img src=""""),_display_(/*704.167*/routes/*704.173*/.Assets.at("images/INTERACTION HISTORY.png")),format.raw/*704.217*/("""" style="width:17px"/>&nbsp;<b>INTERACTION HISTORY</b> 
</div>

  <div id="collapseOne3" class="panel-collapse collapse in">
		<div class="panel-body">
	  <div class="panel with-nav-tabs">
			<div class="panel-heading clearfix">
		  <div>
				<ul class="nav nav-tabs">
			  <li class="expand"><a href="#tab1primary" data-toggle="tab" onclick="callHistoryByVehicle();">PREVIOUS COMMENTS</a></li>
			  <li class="expand"><a href="#tab2primary" data-toggle="tab" onclick="callHistoryByVehicle();">DISPOSITIONS</a></li>
			  <li class="expand"><a href="#tab3primary" data-toggle="tab" onclick="smsHistoryOfUser();">SMS</a></li>
			  <li class="expand"><a href="#tab4primary" data-toggle="tab">E-Mail</a></li>
			  <li class="expand"><a href="#tab5primary" data-toggle="tab" onclick="complaintsOFVehicle();">COMPLAINTS</a></li>
			 """),_display_(/*718.6*/if(typeDispo=="insurance")/*718.32*/{_display_(Seq[Any](format.raw/*718.33*/(""" 
			  """),format.raw/*719.6*/("""<li class="expand"><a href="#tab6primary" data-toggle="tab" onclick="insuranceHistoryOfCustomer();">APPOINTMENT HISTORY</a></li>
			 """)))}),format.raw/*720.6*/(""" 
			"""),format.raw/*721.4*/("""</ul>
			  </div>
		</div>
			<div class="panel-body">
		  <div class="tab-content">
				<div class="tab-pane fade in active" id="tab1primary">
			  <div class="row">
					<div class="col-sm-12">
				  <div class="form-group">
						<table class="table table-bordered table-responsive table-user-information" id="previousCommentTable">
					  <thead>
							<tr>
						  <th></th>
						  <th></th>						  
						 
						</tr>
						  </thead>
					  <tbody>
						  
					 
						</tbody>
					  
					  
					</table>
					  </div>
				</div>
				  </div>
			</div>
				<div class="tab-pane fade" id="tab2primary">
			  <div class="row">
					<div class="col-sm-12">
				  <div class="form-group">
						<table class="table table-bordered table-responsive" id="dispositionHistory">
					  <thead>
							<tr>
						  <th>Date</th>
						  <th>Time</th>
						  <th>Call Type</th>
						  <th>Disposition</th>
						  <th>Next FollowUpDate</th>
						  <th>Next FollowUpTime</th>
						  <th>CRE ID</th>
						 
						</tr>
						  </thead>
					  <tbody>
						  
					 
						</tbody>
					  
					</table>
					  </div>
				</div>
				  </div>
			</div>
			<div class="tab-pane fade" id="tab3primary">
			  <div class="row">
				<div class="col-sm-12">
				
						<table class="table table-bordered table-responsive" id="sms_history">
						 <thead>
							<tr>
						  <th>DATE</th>
						  <th>TIME</th>
						  <th>SOURCE AUTO/CRE ID</th>
						  <th>SCENARIO</th>
						  <th>MESSAGE</th>
						  <th>STATUS</th>
						 
						</tr>
						  </thead>
					  <tbody>
							
						</tbody>
					  
					</table>
					
				</div>
				
				</div>
			</div>
			<div class="tab-pane fade" id="tab4primary">
			  <div class="row">
					<div class="col-sm-12">
				  <div class="form-group">
						<table class="table table-bordered table-responsive table-user-information">
					  <tbody>
							
						</tbody>
					  
					</table>
					  </div>
				</div>
				  </div>
			</div>
			<div class="tab-pane fade" id="tab5primary">
			  <div class="row">
					<div class="col-sm-12">
				  <div class="form-group">
						<table class="table table-bordered table-responsive table-user-information" id="complaint_history">
					  <thead>
							<tr>
						  <th>COMPLAINT No.</th>
						  <th>ISSUE DATE</th>
						  <th>SOURCE</th>
						  <th>FUNCTION</th>
						  <th>CATEGORY</th>
						  <th>STATUS</th>
						 
						</tr>
						  </thead>
					  <tbody>
							
						</tbody>
					  
					</table>
					  </div>
				</div>
				  </div>
			</div>
			
			<div class="tab-pane fade" id="tab6primary">
			  <div class="row">
					<div class="col-sm-12">
				  <div class="form-group">
						<table class="table table-bordered table-responsive table-user-information" id="insurance_history">
					  <thead>
							<tr>
						  <th>INSURANCE COMPANY</th>
						  <th>IDV</th>
						  <th>OD %</th>
						  <th>OD (Rs.)</th>
						  <th>NCB %</th>
						  <th>NCB (Rs.)</th>
						  <th>DISCOUNT%</th>
						  <th>OD PREMIUM</th>
						  <th>LIABILITY PREMIUM</th>
						  <th>ADD-ON PREMIUM</th>
						  <th>PREMIUM (Before Tax)</th>
						  <th>TAX %</th>
						  <th>PREMIUM (After Tax)</th>
						</tr>
						  </thead>
					  <tbody>
							
						</tbody>
					  
					</table>
					  </div>
				</div>
				  </div>
			</div>
			  </div>
		</div>
		  </div>
	</div>
	  </div>
</div>
  </div>
		 </div>
		






		
		




<!--sashi model-->
<div class="modal fade" id="driver_modal1" role="dialog">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*899.72*/routes/*899.78*/.Assets.at("images/close1.png")),format.raw/*899.109*/(""""></button>
		<h4 class="modal-title">Permanent Address</h4>
	  </div>
	  <div class="modal-body">
		<table>
		  <tr>
			<td>Address Line1:</td>
			<td><textarea class="form-control paddr_line1" rows="1"   placeholder="Please enter Company Name/Flat No & Building name." ></textarea>
			  <br/></td>
		 
			<td>Address Line2:</td>
			<td><textarea class="form-control paddr_line2" rows="1"   placeholder="Please enter Road number/ Road name."></textarea>
			  <br/></td>
			  <td>Address Line3:</td>
			<td><textarea class="form-control paddr_line3" rows="1"  placeholder="Please enter Road number/ Road name."></textarea>
			  <br/></td>
		  </tr>
		  <tr>
		  
		  	<td>State</td>
			<td><select  class="form-control paddr_line5" id="pstateId" onchange="getCityByStateSelection('pstateId','paddrrCity');">
			<option value="--SELECT--">--SELECT--</option>
			"""),_display_(/*921.5*/for(states <- statesList) yield /*921.30*/{_display_(Seq[Any](format.raw/*921.31*/("""
           
          """),format.raw/*923.11*/("""<option value=""""),_display_(/*923.27*/states),format.raw/*923.33*/("""">"""),_display_(/*923.36*/states),format.raw/*923.42*/("""</option>
                              
	  """)))}),format.raw/*925.5*/("""
			
			"""),format.raw/*927.4*/("""</select>
			<br/></td>
		  
			<td>City</td>			
		  <td><select  class="form-control paddr_line4" id="paddrrCity">
			                    
	  </select><br/></td>
			</tr>

		  <tr>
			<td>PinCode</td>			
			<td><input type="text"  class="form-control paddr_line6 numberOnly" id="ppincode" maxlength="6" rows="1" value=""""),_display_(/*938.117*/{if( customerData.getPermanentAddressData()!=null){ customerData.getPermanentAddressData().getPincode()} }),format.raw/*938.223*/("""" placeholder="Please enter PinCode"/><br/></td>
		  
			<td>Country</td>
			<td><input type="text"  class="form-control paddr_line7" rows="1" value="India"  placeholder="Please enter Country" readonly/><br/></td>
		  </tr>
		</table>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-primary paddr_submit" data-dismiss="modal">Submit</button>
	  </div>
	</div>
  </div>
</div>
<div class="modal fade" id="driver_modal2" role="dialog">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*955.72*/routes/*955.78*/.Assets.at("images/close1.png")),format.raw/*955.109*/(""""></button>
		<h4 class="modal-title">Residence Address</h4>
	  </div>
	  <div class="modal-body">
		<table>
		  <tr>
			<td>Address Line1:</td>
			<td><textarea class="form-control raddr_line1" rows="1"  name="raddressline1" placeholder="Please enter Company Name/Flat No & Building name." ></textarea>
			  <br/></td>
		
			<td>Address Line2:</td>
			<td><textarea class="form-control raddr_line2" rows="1"  name="raddressline2" placeholder="Please enter Road number/ Road name."></textarea>
			  <br/></td>
			  
			 <td>Address Line3:</td>
			<td><textarea class="form-control raddr_line3" rows="1"  name="raddressline3" placeholder="Please enter Road number/ Road name."></textarea>
			  <br/></td> 
		  </tr>
		   <tr>
		   
		   <td>State</td>			
		 <td><select  class="form-control raddr_line5" id="rstateId" onchange="getCityByStateSelection('rstateId','raddrrCity');">
			<option value="--SELECT--">--SELECT--</option>
			"""),_display_(/*978.5*/for(states <- statesList) yield /*978.30*/{_display_(Seq[Any](format.raw/*978.31*/("""
           
          """),format.raw/*980.11*/("""<option value=""""),_display_(/*980.27*/states),format.raw/*980.33*/("""">"""),_display_(/*980.36*/states),format.raw/*980.42*/("""</option>
                              
	  """)))}),format.raw/*982.5*/("""
			
			"""),format.raw/*984.4*/("""</select>
			<br/></td>
			<td>City</td>
			
		    <td><select  class="form-control raddr_line4" id="raddrrCity">
			                    
	  </select><br/></td>
			 </tr>

		  <tr>
			<td>PinCode</td>
			<td><input type="text"  class="form-control raddr_line6 numberOnly" rows="1" maxlength="6" value=""""),_display_(/*995.103*/{if( customerData.getResidenceAddressData()!=null){ customerData.getResidenceAddressData().getPincode()} }),format.raw/*995.209*/(""""placeholder="Please enter PinCode"/><br/></td>
		  
			<td>Country</td>
			<td><input type="text"  class="form-control raddr_line7" rows="1" value="India" placeholder="Please enter Country" readonly/><br/></td>
		  </tr>
		
		</table>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-primary raddr_submit" data-dismiss="modal">Submit</button>
	  </div>
	</div>
  </div>
</div>
<div class="modal fade" id="driver_modal3" role="dialog">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1013.72*/routes/*1013.78*/.Assets.at("images/close1.png")),format.raw/*1013.109*/(""""></button>
		<h4 class="modal-title">Office Address</h4>
	  </div>
	  <div class="modal-body">
		<table>
		  <tr>
			<td>Address Line1:</td>
			<td><textarea class="form-control oaddr_line1" rows="2"  placeholder="Please enter Company Name/Flat No & Building name." name="oaddressline1">"""),_display_(/*1020.148*/{if( customerData.getOfficeAddressData()!=null){ customerData.getOfficeAddressData().getAddressLine1()} }),format.raw/*1020.253*/("""</textarea>
			  <br/></td>
		  
			<td>Address Line2:</td>
			<td><textarea class="form-control oaddr_line2" rows="2" placeholder="Please enter Road number/ Road name." name="oaddressline2">"""),_display_(/*1024.133*/{if( customerData.getOfficeAddressData()!=null){ customerData.getOfficeAddressData().getAddressLine2()} }),format.raw/*1024.238*/("""</textarea>
			  <br/></td>
			<td>Address Line3:</td>
			<td><textarea class="form-control oaddr_line3" rows="2" placeholder="Please enter Road number/ Road name." name="oaddressline3">"""),_display_(/*1027.133*/{if( customerData.getOfficeAddressData()!=null){ customerData.getOfficeAddressData().getAddressLine3()} }),format.raw/*1027.238*/("""</textarea>
			  <br/></td>
		  </tr>
		  <tr>
		  
		  <td>State</td>
			
		 <td><select  class="form-control oaddr_line4" id="paddr_line4" name="paddressline4"  onchange="getCityByStateSelection('paddr_line4','paddr_line7');">
			<option value="--SELECT--">--SELECT--</option>
			"""),_display_(/*1036.5*/for(states <- statesList) yield /*1036.30*/{_display_(Seq[Any](format.raw/*1036.31*/("""
           
          """),format.raw/*1038.11*/("""<option value=""""),_display_(/*1038.27*/states),format.raw/*1038.33*/("""">"""),_display_(/*1038.36*/states),format.raw/*1038.42*/("""</option>
                              
	  """)))}),format.raw/*1040.5*/("""
			
			"""),format.raw/*1042.4*/("""</select>
			<br/></td>
			<td>City</td>			
		  <td><select  class="form-control oaddr_line7"  id="paddr_line7" name="paddressline3">			                    
	  </select><br/></td>
			 </tr>

		  <tr>
			<td>PinCode</td>
			<td><input type="text"  class="form-control oaddr_line5 numberOnly" maxlength="6" id="paddr_line5" rows="1" value=""""),_display_(/*1051.120*/{if( customerData.getOfficeAddressData()!=null){ customerData.getOfficeAddressData().getPincode()} }),format.raw/*1051.220*/("""" name="paddressline5" placeholder="Please enter PinCode"/><br/></td>
		  
			<td>Country</td>
			<td><input type="text"  class="form-control oaddr_line6" id="paddr_line6" rows="1" value="India" name="paddressline6" placeholder="Please enter Country" readonly/><br/></td>
		  </tr>
		</table>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-primary oaddr_submit" data-dismiss="modal">Submit</button>
	  </div>
	</div>
  </div>
</div>

<div class="modal fade" id="driver_modal4" role="dialog">
    <div class="modal-dialog modal-xs">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1071.80*/routes/*1071.86*/.Assets.at("images/close1.png")),format.raw/*1071.117*/(""""></button>
          <h4 class="modal-title">ADD EMAIL</h4>
        </div>
		
		
        <div class="modal-body">
		<div class="row">
        <div class="col-xs-12">
        <div class="col-xs-4">
        <input type="text" name="myEmailNum" class="form-control email"  maxlength="30" min="5" max="30" id="myEmailNum" >
      </div>
        </div>
        </div>
		</div>
        <div class="modal-footer">
   <button type="button" class="btn btn-default" data-dismiss="modal" id="saveEmailCust">SAVE</button>
        </div>
      </div>
      
    </div>
  </div>		  
<div id="smallModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
		<h4 class="modal-title">Message</h4>
	  </div>
	  <div class="modal-body">
		<p>This feature is not enabled for """),_display_(/*1100.39*/userName),format.raw/*1100.47*/(""".<br>
		  Contact Administrator.</p>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
	  </div>
	</div>
  </div>
</div>
<div id="smallModal1" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
		<h4 class="modal-title">E-Mail</h4>
	  </div>
	  <div class="modal-body">
		<p>This feature is not enabled for """),_display_(/*1117.39*/userName),format.raw/*1117.47*/(""" """),format.raw/*1117.48*/(""".<br>
		  Contact Administrator.</p>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
	  </div>
	</div>
  </div>
</div>
<div class="modal fade" id="sa_modal" role="dialog">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1130.72*/routes/*1130.78*/.Assets.at("images/close1.png")),format.raw/*1130.109*/(""""></button>
		<h4 class="modal-title">CHECK SMR LOAD</h4>
		<br>
		<h5 class="modal-title">Auto Service</h5>
	  </div>
	  <div class="modal-body">
		<table border="1" class="table table-striped table-bordered table-hover" id="dataTables-example60">
		  <thead>
			<tr>
			  
			  <th>DATE</th>
			  <th>SMR BOOKED</th>
			  <th>CAPACITY</th>
			</tr>
		  </thead>
		  <tbody>    
			
			
			
		  </tbody>
		</table>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
	  </div>
	</div>
  </div>
</div>
<div class="modal fade" id="sa_modal1" role="dialog">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1162.72*/routes/*1162.78*/.Assets.at("images/close1.png")),format.raw/*1162.109*/(""""></button>
		<h4 class="modal-title">CHECK DRIVER'S SCHEDULE</h4>
		<br>
		<h5 class="modal-title">Auto Service</h5>
	  </div>
	  <div class="modal-body">
		<table border="1"  class="table table-striped table-bordered table-hover" id="">
		  <thead>
			<tr>
			  <th>PICK-UP-BY</th>
			  <th>FROM TIME</th>
			  <th>TO TIME</th>
			  
			</tr>
		  </thead>
		  <tbody>
			
			
		  </tbody>
		</table>
	  </div>
	  <div class="row">
		<div class="col-md-10 col-md-offset-1" style="background-color: #ddd;">
		 
		</div>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
	  </div>
	</div>
  </div>
</div>
<div class="modal fade" id="workshop_modal" role="dialog">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1198.72*/routes/*1198.78*/.Assets.at("images/close1.png")),format.raw/*1198.109*/(""""></button>
		<h4 class="modal-title">Workshop Details</h4>
	  </div>
	  <div class="modal-body">
		<table class="table table-striped table-bordered table-hover" id="">
		  <thead>
			<tr>
			  <th>Workshop Name</th>
			  <th>Service Booked Date</th>
			</tr>
		  </thead>
		  <tbody>
		  </tbody>
		</table>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	  </div>
	</div>
  </div>
</div>


<!-- Address Popup Model -->
<div class="modal fade" id="AddNewAddressPopup" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1228.72*/routes/*1228.78*/.Assets.at("images/close1.png")),format.raw/*1228.109*/(""""></button>
  <h4 class="modal-title"><b style="text-align:center; color:Red;">Add New Address</b></h4>
</div>
<div class="modal-body">
 
  <div class="row" id="AddAddressDiv">
   <div class="col-md-12">
<div class="col-md-6">
  <div class="form-group">
<label for="comments"><b>Add Address1<b></label>
<textarea class="form-control" rows="1" id="AddAddress1All" name="address1New" ></textarea>
  </div>
</div>
<div class="col-md-6">
  <div class="form-group">
<label for="comments"><b>Add Address2<b></label>
<textarea class="form-control" rows="1" id="AddAddress2All" name="address2New" ></textarea>
  </div>
</div>

<div class="col-md-6">
  <div class="form-group">
<label for="comments"><b>State<b></label>
<select class="form-control" name="stateNew" id="AddAddrMMSState" onchange="getCityByStateSelection('AddAddrMMSState','cityInputPopup3');">
  <option value="0">--SELECT--</option>
"""),_display_(/*1253.2*/for(states <- statesList) yield /*1253.27*/{_display_(Seq[Any](format.raw/*1253.28*/("""
           
          """),format.raw/*1255.11*/("""<option value=""""),_display_(/*1255.27*/states),format.raw/*1255.33*/("""">"""),_display_(/*1255.36*/states),format.raw/*1255.42*/("""</option>
                              
  """)))}),format.raw/*1257.4*/("""
"""),format.raw/*1258.1*/("""</select>
  </div>
</div>

<div class="col-md-6">
  <div class="form-group">
<label for="comments"><b>City<b></label>
<select class="form-control" name="cityNew" placeholder="City" id="cityInputPopup3">	
                                        
                                    </select>
  </div>
</div>
<div class="col-md-6">
  <div class="form-group">
<label for="comments"><b>PinCode<b></label>
<input type="text" class="form-control numberOnly" name="pincodeNew" value="0" id="PinCode1MMS" maxlength="6" />
  </div>
</div>

</div>
   
  </div>
  

<div class="modal-footer">
  <button type="button" class="btn btn-success" id="AddAddrMMSSave" data-dismiss="modal">Save</button>
</div>
</div>

</div>
</div>
</div>  <!-- end popup -->  <!-- end popup -->


<!---bottom fixed popup----->
<div class="modal fade" id="bottonFixedId" role="dialog">
<div class="modal-dialog">

  <!-- Modal content-->
  <div class="modal-content">
	<div class="modal-header">
	  <button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1299.73*/routes/*1299.79*/.Assets.at("images/close1.png")),format.raw/*1299.110*/(""""></button>
	  <h4 class="modal-title">Call Script</h4>
	</div>
	<div class="modal-body">
	  <p>Hi Good Morning!. </p>
	</div>
	<div class="modal-footer">
	  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	</div>
  </div>
  
</div>
</div>



<div class="modal fade" id="offer" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="background-color:#1797be;color:#ffffff;text-align:center">
          <button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1321.80*/routes/*1321.86*/.Assets.at("images/close1.png")),format.raw/*1321.117*/(""""></button>
          <h4 class="modal-title">Select Offer For """),_display_(/*1322.53*/if(vehicleData!=null)/*1322.74*/{_display_(Seq[Any](format.raw/*1322.75*/("""
		"""),format.raw/*1323.3*/("""<b>  """),_display_(/*1323.9*/{vehicleData.getVehicleRegNo()}),format.raw/*1323.40*/("""</b>
		  """)))}),format.raw/*1324.6*/(""" """),format.raw/*1324.7*/("""</h4>
        </div>
        <div class="modal-body ofeerData">
          <table class="table table-striped">
    <thead class="makefixed">
      <tr>
	  <th>#</th>
        <th>OFFER&nbsp;CODE</th>
        <th>OFFER&nbsp;DETAIL</th>
        <th>CONDITION</th>
		<th>BENEFIT</th>
        <th>VALIDITY</th>
      </tr>
    </thead>
   <tbody>
     """),_display_(/*1339.7*/for(offer_list <- offersList) yield /*1339.36*/{_display_(Seq[Any](format.raw/*1339.37*/("""
           """),format.raw/*1340.12*/("""<tr id="row12">
        <td><input type="radio" class="case" name="case[]" value=""""),_display_(/*1341.68*/offer_list/*1341.78*/.id),format.raw/*1341.81*/("""" ></td>
		 <td>"""),_display_(/*1342.9*/offer_list/*1342.19*/.offerCode),format.raw/*1342.29*/("""</td>
        <td>"""),_display_(/*1343.14*/offer_list/*1343.24*/.offerDetails),format.raw/*1343.37*/("""</td>
        <td>"""),_display_(/*1344.14*/offer_list/*1344.24*/.offerConditions),format.raw/*1344.40*/("""</td>
		  <td>"""),_display_(/*1345.10*/offer_list/*1345.20*/.offerBenefit),format.raw/*1345.33*/("""</td>
        <td>"""),_display_(/*1346.14*/offer_list/*1346.24*/.offerValidity),format.raw/*1346.38*/("""</td>
      </tr>
	  """)))}),format.raw/*1348.5*/("""
    """),format.raw/*1349.5*/("""</tbody>
  </table>
      </div>
        <div class="modal-footer" style="padding: 7px;">
          <button type="button" class="btn btn-success btn-xs" id="applyOffers">Apply&nbsp;Offer</button>
        </div>
		<div id="offerSelected">
		<b style="margin-left:6px;">OFFER SELECTED</b>
<div class="modal-footer" style="border: 2px solid #1797be;margin-left: 6px;margin-right: 5px;">
<div class="col-sm-10 pull-left">
<span id="selectChkVal" style="text-align:left"></span>
<input type="hidden" id="offerSelectedId" name="offerSelId" value=0>

</div>
<div class="col-sm-2 pull-right">
          <button type="button" class="btn btn-danger btn-xs removeOffer" >REMOVE OFFER</button>
		  
		  </div>
		  
		 
        </div>
		
		</br>
      </div>
	  <div style="text-align:center">
	   <button type="button" class="btn btn-success saveOffer" data-dismiss="modal" style="margin-top:-24px;">SAVE</button><br />
	   </div>
      </div>
    </div>
  </div>
  
  
  <div class="modal fade" id="DNDConfirm" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1387.80*/routes/*1387.86*/.Assets.at("images/close1.png")),format.raw/*1387.117*/(""""></button>
          <h4 class="modal-title" style="text-align:center;">Do Not Disturb(DND) Tagging</h4>
        </div>
        <div class="modal-body">
          <b>Do you want to tag the customer """),_display_(/*1391.47*/customerData/*1391.59*/.getCustomerName()),format.raw/*1391.77*/("""  """),format.raw/*1391.79*/("""as �Do Not Disturb /Call� ?<br> Click �YES� to confirm</b><br><br>
		  
  <div class="radio-inline">
      <label><input type="radio" name="RadioDND" id="RadioDNDYes" value="makeDND">YES</label>
    </div>
    <div class="radio-inline">
      <label><input type="radio" name="RadioDND" id="RadioDNDNo" value="removeDND">NO</label>
    </div>
        </div>
        <div class="modal-footer" style="text-align:center;" >
         
<button type="button" class="btn btn-success" id="submitDndBtn" data-dismiss="modal"
>submit</button>		  
        </div>
      </div>
      
    </div>
  </div>
  
     <div class="modal fade" id="smsPopuId" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="text-align:center;background-color: #106A86;color: #fff;">
          <button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1416.80*/routes/*1416.86*/.Assets.at("images/close1.png")),format.raw/*1416.117*/(""""></button>
          <h4 class="modal-title" ><b>SMS OPTIONS<b></h4>
        </div>
        <div class="">
   <ul class="nav nav-pills nav-stacked col-md-3">
   """),_display_(/*1421.5*/for(smsTemplate<-smsTemplates) yield /*1421.35*/{_display_(Seq[Any](format.raw/*1421.36*/("""
   		"""),format.raw/*1422.6*/("""<li><a class="clssTemplType" id="typAnch_"""),_display_(/*1422.48*/{smsTemplate.getSmsId()}),format.raw/*1422.72*/("""" href="#tab_"""),_display_(/*1422.86*/{smsTemplate.getSmsId()}),format.raw/*1422.110*/("""" data-toggle="pill">"""),_display_(/*1422.132*/{smsTemplate.getSmsType()}),format.raw/*1422.158*/("""</a></li>
   	""")))}),format.raw/*1423.6*/("""
 
"""),format.raw/*1425.1*/("""</ul>
<div class="tab-content col-md-9 smstabclass">
<input type="hidden" id="smsrequestReference" value=""""),_display_(/*1427.55*/{vehicleData.getVehicle_id()}),format.raw/*1427.84*/("""" />
  """),_display_(/*1428.4*/for(smsTemplate<-smsTemplates) yield /*1428.34*/{_display_(Seq[Any](format.raw/*1428.35*/("""
  	

        """),format.raw/*1431.9*/("""<div class="tab-pane """),_display_(/*1431.31*/if(smsTemplate.getSmsId()==1)/*1431.60*/{_display_(Seq[Any](format.raw/*1431.61*/("""active""")))}),format.raw/*1431.68*/("""" id="tab_"""),_display_(/*1431.79*/{smsTemplate.getSmsId()}),format.raw/*1431.103*/("""">
        <input type="hidden" id="typ_"""),_display_(/*1432.39*/{smsTemplate.getSmsId()}),format.raw/*1432.63*/(""""  />
             <h4><u>"""),_display_(/*1433.22*/{smsTemplate.getSmsType()}),format.raw/*1433.48*/("""</u></h4>
			 <textarea name="text" rows="5" cols="45" id="txt_"""),_display_(/*1434.55*/{smsTemplate.getSmsId()}),format.raw/*1434.79*/("""" class="cl_"""),_display_(/*1434.92*/{smsTemplate.getSmsId()}),format.raw/*1434.116*/("""" disabled="disabled" style="border:none; background-color:#fff;">"""),_display_(/*1434.183*/{smsTemplate.getSmsTemplate()}),format.raw/*1434.213*/("""</textarea>
<br/>
		<input type="button" name="set_Value" id="ed_"""),_display_(/*1436.49*/{smsTemplate.getSmsId()}),format.raw/*1436.73*/("""" class="btn btn-info cmmnbtnclass"  value="Edit"  >
     

        </div>
 
 """)))}),format.raw/*1441.3*/("""
 """),format.raw/*1442.2*/("""</div>	
<!-- tab content -->
        </div>
        <div class="modal-footer">
          <button type="button" id="smsSendbtn" class="btn btn-success" data-dismiss="modal">SEND SMS</button>
        </div>
      </div>
    </div>
  </div>
 

  <!-- Modal -->
  
  
  <div class="modal fade" id="myModalAddPhone" role="dialog">
    <div class="modal-dialog modal-xs">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1462.80*/routes/*1462.86*/.Assets.at("images/close1.png")),format.raw/*1462.117*/(""""></button>
          <h4 class="modal-title">ADD PHONE NUMBER</h4>
        </div>
		
		
        <div class="modal-body">
		<div class="row">
        <div class="col-xs-12">
        <div class="col-xs-4">
        <input type="text" name="myPhoneNum" class="form-control numberOnly" maxlength="10" min="10" max="10" id="myPhoneNum" >
      </div>
        </div>
        </div>
		</div>
        <div class="modal-footer">
   <button type="button" class="btn btn-default" data-dismiss="modal" id="savePhoneCust">SAVE</button>
        </div>
      </div>
      
    </div>
  </div>
   <!--------------------------------Side bar popup start------------------------------------------>
  <div id="fixedsocial">
    <div class="facebookflat" data-toggle="modal" data-target="#myModal1" >
	<!-- <img src='"""),_display_(/*1486.18*/routes/*1486.24*/.Assets.at("images/car.png")),format.raw/*1486.52*/("""' style="width: 50px;" /> -->
	<img src='"""),_display_(/*1487.13*/routes/*1487.19*/.Assets.at("images/sports-car.jpg")),format.raw/*1487.54*/("""' style="width: 50px; border-radius: 5px;" data-toggle="tooltip" data-placement="left" title="Vehicle" />
	</div>
    <div class="twitterflat" data-toggle="modal" data-target="#myModal2" >
	<img src='"""),_display_(/*1490.13*/routes/*1490.19*/.Assets.at("images/service.jpg")),format.raw/*1490.51*/("""' style="width: 50px;border-radius: 5px;" data-toggle="tooltip" data-placement="left" title="Service" onclick="ajaxCallServiceLoadOfCustomer();"/>
	<!-- <img src='"""),_display_(/*1491.18*/routes/*1491.24*/.Assets.at("images/car-insurance.png")),format.raw/*1491.62*/("""' style="width: 50px;" /> -->
	</div> 
     <div class="twitterflat1" data-toggle="modal" data-target="#myModal3" >
	 <img src='"""),_display_(/*1494.14*/routes/*1494.20*/.Assets.at("images/car-insu.jpg")),format.raw/*1494.53*/("""' style="width: 50px; border-radius: 5px;" data-toggle="tooltip" data-placement="left" title="Insurance"/> 
	 <!-- <img src='"""),_display_(/*1495.19*/routes/*1495.25*/.Assets.at("images/INSURANCE1.png")),format.raw/*1495.60*/("""' style="width: 50px;" /> --> 
	 </div> 
      
</div>
<div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding: 5px; text-align: center;">
          <button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1505.80*/routes/*1505.86*/.Assets.at("images/close1.png")),format.raw/*1505.117*/(""""></button>
          <h4 class="modal-title">VEHICLE INFORMATION</h4>
        </div>
        <div class="modal-body"  style="padding: 5px;">
		<div class="table table-responsive">
          <table id="example700" class = "table table-bordered">
 
  <thead class="makefixed">
				   <tr>
				  <th >Model</th>
			   <th>Variant</th>
				<th >Color</th>
			   <th >Registration&nbsp;No</th>
				<th>Engine&nbsp;No</th>
				<th >Chassis&nbsp;No</th>
				  <th >Sale&nbsp;Date</th>
				<th >Fuel&nbsp;Type</th>
				</tr>
			  </thead>
			  <tbody>
			   
			"""),_display_(/*1526.5*/for(post <- customerData.getVehicles()) yield /*1526.44*/ {_display_(Seq[Any](format.raw/*1526.46*/("""
			"""),format.raw/*1527.4*/("""<tr>
			  <td><input type="text-primary" id="model" value=""""),_display_(/*1528.56*/post/*1528.60*/.getModel()),format.raw/*1528.71*/(""""  style="border:none;text-align:center" readonly></td>
			  <td><input type="text-primary" id="variant" value=""""),_display_(/*1529.58*/post/*1529.62*/.getVariant()),format.raw/*1529.75*/("""" style="border:none;text-align:center" readonly></td>
			  <td><input type="text-primary" id="color" value=""""),_display_(/*1530.56*/post/*1530.60*/.getColor()),format.raw/*1530.71*/("""" style="border:none;text-align:center" readonly></td>
			  <td>
			  <div class="col-sm-12">
			  <div class="col-sm-8">
			  <input type="text-primary" class="vehicalRegNo" id="vehicalRegNo" value=""""),_display_(/*1534.80*/post/*1534.84*/.getVehicleRegNo()),format.raw/*1534.102*/("""" style="border:none;margin-right: 15px;" readonly>
				</p>
				</div>
				<div class="col-sm-2">
				  <!-- <input type="button" value="+"  id="editRegistrationno" style="color: rgb(255, 255, 255); background-color: rgba(0, 166, 90, 0.85098);"> -->
				  
				  <input type="button" value="+"  id="" style="color: rgb(255, 255, 255); background-color: rgba(0, 166, 90, 0.85098);">
				 </div>
				 <div class="col-sm-2">
				  <!-- <input type="button" class="btn btn-success btn-xs" value="Add" id="updateRegistrationno" onmouseenter="ajaxAddRegistrationno();" style="display:none;"> -->
				  
				</div>
			  </div></td>
			  <td> 
			  <div class="col-sm-12">
			   <div class="col-sm-8">
			  <input type="text-primary" id="engineNo" value=""""),_display_(/*1550.55*/post/*1550.59*/.getEngineNo()),format.raw/*1550.73*/("""" style="border:none;margin-right: 15px;" readonly>
				</p>
				  </div>
			<!-- <div class="col-sm-2">
				  <input type="button" value="+" id="editEngineno" style="color: rgb(255, 255, 255); background-color: rgba(0, 166, 90, 0.85098);">
				</div>  <div class="col-sm-2">
				  <input type="button" class="btn btn-success btn-xs" value="Add" id="updateEngineno" onmouseenter="ajaxAddEngineno();" style="display:none;">
				</div>-->
				</div></td>
			  <td><div class="col-sm-12 ">
			   <div class="col-sm-8">
			  <input type="text-primary" id="chassisNo" value=""""),_display_(/*1561.56*/post/*1561.60*/.getChassisNo()),format.raw/*1561.75*/("""" style="border:none;margin-right: 15px;" readonly>
				 </div>
			<!--	<div class="col-sm-2">
				  <input type="button" value="+" id="editChassisno" style="color: rgb(255, 255, 255); background-color: rgba(0, 166, 90, 0.85098);">
				  </div>  
				  <div class="col-sm-2">  
				  <input type="button" class="btn btn-success btn-xs" value="Add" id="updateChassisno" onmouseenter="ajaxAddChassisno();" style="display:none;">
				</div>-->
				</div></td>
			    <td><input type="text-primary" id="saleDate" value=""""),_display_(/*1570.61*/post/*1570.65*/.getSaleDateStr()),format.raw/*1570.82*/("""" style="border:none;text-align:center" readonly></td>
			  <td><input type="text-primary" id="fuelType" value=""""),_display_(/*1571.59*/post/*1571.63*/.getFuelType()),format.raw/*1571.77*/("""" style="border:none;text-align:center" readonly></td>
			</tr>			""")))}),format.raw/*1572.13*/("""
				"""),format.raw/*1573.5*/("""</tbody>
</table>
</div>
        </div>
     
      </div>
      
    </div>
  </div>
  
  <!-- Modal -->
  <div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog modal-lg" style="width:1200px;">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding: 5px; text-align: center;">
          <button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1590.80*/routes/*1590.86*/.Assets.at("images/close1.png")),format.raw/*1590.117*/(""""></button>
          <h4 class="modal-title">SERVICE INFORMATION</h4>
        </div>
        <div class="modal-body"  style="padding: 5px;">
		<div class="table table-responsive">
          <table id="example800" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
 
   <thead class="makefixed">
                <tr>
						<th>Repair Order&nbsp;Date</th>
						<th>Repair Order&nbsp;No.</th>
						<th>Bill&nbsp;Date</th>
						<th>Kilometer</th>
						<th>Model</th>
						<th>Registration No.</th>
						<th>Service Type</th>
						<th>Menu Code Description</th>
						<th>Service Advisor Name</th>
						<th>Customer Name</th>
						<th>Group (Category)</th>
						<th>Part Amount</th>
						<th>Labour Amount</th>
						<th>Total Amount</th> 
					</tr>
                  </thead>
				   <tbody>
                                   
                    </tbody>
			  
</table>

</div>
        </div>
     
      </div>
      
    </div>
  </div>
  
  <!-- Modal -->
  <div class="modal fade" id="myModal3" role="dialog">
    <div class="modal-dialog modal-lg" style="width:1200px;">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding: 5px; text-align: center;">
          <button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1636.80*/routes/*1636.86*/.Assets.at("images/close1.png")),format.raw/*1636.117*/(""""></button>
          <h4 class="modal-title">INSURANCE INFORMATION</h4>
        </div>
        <div class="modal-body"  style="padding: 5px;">
		<div class="table table-responsive">
          <table id="" class = "table table-bordered">
	 
	   <thead class="makefixed">
					  <tr>
					<th>Policy Issue Date</th>
					<th>Policy Number</th>
					<th>Policy type</th>
					<th>Class</th>
					<th>Add-On</th>
					<th>Due Date</th>
					<th>Gross Premium</th>
					<th>IDV</th>
					<th>OD(%)</th>
					<th>Basic OD</th>
					<th>NCB(%)</th>
					<th>NCB Value</th>
					<th>Discount</th>
					<th>OD Premium</th>
					<th>TP Premium</th>
					<th>PA Premium</th>
					<th>Legal Liability</th>
					<th>Net Liability</th>
					<th>Add-On Premium</th>
					<th>Other Premium</th>
					<th>Net Premium</th>
					<th>Tax</th>
					<!--<th>Gross Premium</th>
					<th>Coverage&nbsp;Period</th>
					<th>Policy&nbsp;Due&nbsp;Date</th>
					<th>Insurance&nbsp;Company</th>
					<th>NCB(%age)</th>
					<th>NCB&nbsp;Amount(Rs.)</th>
					<th>PREMIUM</th>
					<th>Service&nbsp;Tax</th>
					<th>Total&nbsp;Premium</th>
					<th>Last&nbsp;Renewal&nbsp;By</th>-->
					
					</tr>
				  </thead>
				<tbody>
				"""),_display_(/*1681.6*/for(insuranceData <-customerData.getInsurances()) yield /*1681.55*/{_display_(Seq[Any](format.raw/*1681.56*/("""
				 
				  """),format.raw/*1683.7*/("""<tr>
				 <!--  <td><a data-toggle="modal" data-target="#otherdetails" ><i class="fa fa-info-circle" data-toggel="tooltip" title="Other Details" style="font-size:30px;color:red;"></i></a></td> -->
					<td>"""),_display_(/*1685.11*/insuranceData/*1685.24*/.getPolicyDueDateStr()),format.raw/*1685.46*/("""</td>
					<td>"""),_display_(/*1686.11*/insuranceData/*1686.24*/.getPolicyNo()),format.raw/*1686.38*/("""</td>
					<td>"""),_display_(/*1687.11*/insuranceData/*1687.24*/.getInsuranceCompanyName()),format.raw/*1687.50*/("""</td>
					<td>"""),_display_(/*1688.11*/insuranceData/*1688.24*/.getPolicyType()),format.raw/*1688.40*/("""</td>
					<td>"""),_display_(/*1689.11*/insuranceData/*1689.24*/.getClassType()),format.raw/*1689.39*/("""</td>
					<td>"""),_display_(/*1690.11*/insuranceData/*1690.24*/.getAddOn()),format.raw/*1690.35*/("""</td>
					<td>"""),_display_(/*1691.11*/insuranceData/*1691.24*/.getPolicyDueDate()),format.raw/*1691.43*/("""</td>
					<td>"""),_display_(/*1692.11*/insuranceData/*1692.24*/.getGrossPremium()),format.raw/*1692.42*/("""</td>
					<td>"""),_display_(/*1693.11*/insuranceData/*1693.24*/.getIdv()),format.raw/*1693.33*/("""</td>
					<td>"""),_display_(/*1694.11*/insuranceData/*1694.24*/.getOdPercentage()),format.raw/*1694.42*/("""</td>
					 <td>"""),_display_(/*1695.12*/insuranceData/*1695.25*/.getNcBPercentage()),format.raw/*1695.44*/("""</td>
					<td>"""),_display_(/*1696.11*/insuranceData/*1696.24*/.getNcBAmountStr()),format.raw/*1696.42*/("""</td>
					<td>"""),_display_(/*1697.11*/insuranceData/*1697.24*/.getDiscountPercentage()),format.raw/*1697.48*/("""</td>
					<td>"""),_display_(/*1698.11*/insuranceData/*1698.24*/.getODpremium()),format.raw/*1698.39*/("""</td>
					<td>"""),_display_(/*1699.11*/insuranceData/*1699.24*/.getaPPremium()),format.raw/*1699.39*/("""</td>
					<td>"""),_display_(/*1700.11*/insuranceData/*1700.24*/.getpAPremium()),format.raw/*1700.39*/("""</td>
					<td>"""),_display_(/*1701.11*/insuranceData/*1701.24*/.getLegalLiability()),format.raw/*1701.44*/("""</td>
					<td>"""),_display_(/*1702.11*/insuranceData/*1702.24*/.getNetLiability()),format.raw/*1702.42*/("""</td>
					<td>"""),_display_(/*1703.11*/insuranceData/*1703.24*/.getAdd_ON_Premium()),format.raw/*1703.44*/("""</td>
					<td>"""),_display_(/*1704.11*/insuranceData/*1704.24*/.getOtherPremium()),format.raw/*1704.42*/("""</td>
					<td>"""),_display_(/*1705.11*/insuranceData/*1705.24*/.getPremiumAmountStr()),format.raw/*1705.46*/("""</td>
					<td>"""),_display_(/*1706.11*/insuranceData/*1706.24*/.getServiceTax()),format.raw/*1706.40*/("""</td>
				  </tr>
				  """)))}),format.raw/*1708.8*/("""
					"""),format.raw/*1709.6*/("""</tbody> 
	</table>
        </div>
		</div>
       
      </div>
      
    </div>
  </div>
  <!------------Service Popup Model---------->
  <div class="modal fade" id="myModalForInfo" role="dialog">
    <div class="modal-dialog modal-lg" style="width:1200px;">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="background-color:#1797BE;text-align:center;">
          <button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1725.80*/routes/*1725.86*/.Assets.at("images/close1.png")),format.raw/*1725.117*/(""""></button>
          <h4 class="modal-title heading" style="color:#fff"><b>SERVICE HISTORY -<<VEH No.>> - <<SERVICE DATE>><b> </h4>
        </div>
        <div class="modal-body">
         

   <div class="row" style="border:2px solid #1797BE;margin-right: -8px;margin-left: -6px;">
                <div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                    <tr>
                      <td><strong>CUSTOMER ID</strong></td>
                      <td class="text-primary"> 1311935638 </td>
                    </tr>
					
					<tr>
                      <td><strong>S/ADVISOR</strong></td>
                      <td class="text-primary"> </td>
                    </tr>
					<tr>
                      <td><strong>MILEAGE (Kms)</strong></td>
                      <td class="text-primary">34233</td>
                    </tr>
					  
					 <tr>
                      <td><strong>PART DISC</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
                   <tr>
                      <td><strong>PARTS BILL</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
                    </tbody>
                    </table>
                </div>
                <div class="col-md-4">
                 <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                 
					<tr>
                      <td><strong> JOBCARD No.</strong></td>
                      <td class="text-primary">JC13010045</td>
                    </tr>
				
					<tr>
                      <td><strong>TECHNICIAN</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
					<tr>
                      <td><strong>Bill No.</strong></td>
                      <td class="text-primary">BR13010474</td>
                    </tr>
					 
					
					<tr>
                      <td><strong>LAB DISC</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					 <tr>
                      <td><strong>LAB BILL</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
                   
                  </tbody>
                </table>
                </div>
                 <div class="col-md-4">
                 <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                
					<tr>
                      <td><strong>W/SHOP</strong></td>
                      <td class="text-primary">UNIV RD</td>
                    </tr>
					
					
					<tr>
                      <td><strong>S/TYPE</strong></td>
                      <td class="text-primary">FR3</td>
                    </tr>
					<tr>
                      <td><strong>Bill Date</strong></td>
                      <td class="text-primary">10/27/2015</td>
                    </tr>
					
					
					 <tr>
                      <td><strong>DISCOUNT</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
                  
					 <tr>
                      <td><strong>BILL AMOUNT</strong></td>
                      <td class="text-primary">656577</td>
                    </tr>
                  </tbody>
                </table>
                </div>
	</div>
			<div class="row" style="border:2px solid #1797BE;margin-top: 5px;margin-right: -8px;margin-left: -6px;">
	   
		
			<h4 style="text-align:center;"><b>REMARKS<b></h4>
    			<div class="col-lg-4" style="text-align: center;">
  			       <div class="form-group">
     			    <label for="">DEFECT DETAILS</label>
     			     <textarea type="" class="form-control" rows="3" id=""  readonly></textarea>
 			     </div>
		 </div> 
  		    <div class="col-lg-4" style="    text-align: center;">
  			<div class="form-group">
     			 <label for="">LABOUR DETAILS</label>
     			 <textarea type="" class="form-control" id=""  rows="3" readonly></textarea>
 			 </div>
		 </div>
       
  		<div class="col-lg-4" style="    text-align: center;">
  			<div class="form-group">
     			 <label for="">JC REMARKS</label>
     			  <textarea type="" class="form-control" id=""  rows="3" readonly></textarea>
 			 </div>
		 </div>
		 
 

</div>

<div class="row" style="border:2px solid #1797BE;    margin-right: -8px;
    margin-left: -6px;    margin-top: 5px;">
<div class="row">
                <div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                    <tr>
                      <td><strong>3rd DAY PSF</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
					
					
                    </tbody>
                    </table>
                </div>
				<div class="col-md-4" >
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                  
					<tr>
                      <td><strong>DATE</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
					
                    </tbody>
                    </table>
                </div>
				<div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                   
					
					<tr>
                      <td><strong>DONE BY</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
                    </tbody>
                    </table>
                </div>
				</div>
				
				
<div class="row">
                <div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                    <tr>
                      <td><strong>6th DAY PSF</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
					
					
                    </tbody>
                    </table>
                </div>
				<div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                  
					<tr>
                      <td><strong>DATE</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
					
                    </tbody>
                    </table>
                </div>
				<div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                   
					
					<tr>
                      <td><strong>DONE BY</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
                    </tbody>
                    </table>
                </div>
				</div>
				
<div class="row">
                <div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                    <tr>
                      <td><strong>30th DAY PSF</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
					
					
                    </tbody>
                    </table>
                </div>
				<div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                  
					<tr>
                      <td><strong>DATE</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
					
                    </tbody>
                    </table>
                </div>
				<div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                   
					
					<tr>
                      <td><strong>DONE BY</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
                    </tbody>
                    </table>
                </div>
				</div>
</div>
        </div>
      
      </div>
      
    </div>
  </div>
	 <!------------------Calculator modal Popup------------------------->
   <!------------------Calculator modal Popup------------------------->
  <div class="modal fade" id="InsuPremiumPopup" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding: 10px; color: red">
		<button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1996.72*/routes/*1996.78*/.Assets.at("images/close1.png")),format.raw/*1996.109*/(""""></button>
         
          <p class="modal-title" style="text-align:center;"><b>INSURANCE PREMIUM CALCULATOR</b></p>
        </div>
        <div class="modal-body" style="padding-bottom: 0px;">
       
        
        <b style="color:red;"><u>VEHICLE DETAILS:</u></b>
         <div class="row">
      
      <div class="col-xs-2">
       <div class="form-group">
        <label>Cubic&nbsp;Capacity(CC)</label>
         <select class="form-control input-sm" name="cubicCapacity" id="cubicCapacityId" onchange="ajaxODPercentage();">
		 <option value="1001 -1500 CC">1001 -1500 CC</option>
          <option value="<=1000 CC"><=1000 CC</option>
          <option value=">=1501 CC"> >=1501 CC</option>
        </select>
      </div>
      </div>
       <div class="col-xs-2">
        <div class="form-group">
        <label>Vehicle&nbsp;Age</label>
         <select class="form-control input-sm" name="vehicleAge" id="vehicleAgeId" onchange="ajaxODPercentage();">
          <option value="<= 5 Years"><= 5 Years</option>
          <option value="5-10 Years">5-10 Years</option>
          <option value=">10 Years"> >10 Years </option>
        </select>
      </div>
      </div>
      <div class="col-xs-2">
       <div class="form-group">
        <label>City</label>
         <select class="form-control input-sm" name="vehicleCity">
          <option value="Rest of India">Rest of India</option>
          <option value="Ahmedabad">Ahmedabad</option>
          <option value="Bangalore">Bangalore</option>
          <option value="Chennai">Chennai</option>
          <option value="Hyderabad">Hyderabad</option>
          <option value="Kolkata">Kolkata</option>
          <option value="Mumbai">Mumbai</option>
          <option value="New Delhi">New Delhi</option>
          <option value="Pune">Pune</option>
             
        </select>
      </div>
      </div>
      <div class="col-xs-2">
       <div class="form-group">
        <label>Zone</label>
         <select class="form-control input-sm" name="vehicleZone" id="zoneId" onchange="ajaxODPercentage();">
		 <option value="Zone B">Zone B</option>
         <option value="Zone A">Zone A</option>
           </select>
         </div>
         </div>

      <div class="col-xs-2">
       <div class="form-group">
        <label>Ex&nbsp;Showroom&nbsp;Price</label>
         <input type="text" class="form-control input-sm numberOnly" value="0" name="exShowroomPrice">
         </div>
         </div>
      
        </div>
         <b style="color:red;"><u>OWN DAMAGE PREMIUM (I):</u></b>
<div class="row">
         <div class="col-xs-2">
      <div class="form-group">
        <label>IDV</label>
        <input class="form-control input-sm numberOnly" id="idvId" value=""""),_display_(/*2066.76*/{if(interOfCall.getVehicle()!=null){if(interOfCall.getVehicle().getCurrentInsurance()!=null){interOfCall.getVehicle().getCurrentInsurance().getIdv()}else{0}}else{0}}),format.raw/*2066.241*/("""" name="idv" type="text">
     </div>
     </div>
       <div class="col-xs-2">
       <div class="form-group">
        <label>OD(%)</label>
         <input class="form-control input-sm" id="odId" name="odPercentage" value=""""),_display_(/*2072.85*/{if(interOfCall.getVehicle()!=null){if(interOfCall.getVehicle().getCurrentInsurance()!=null){interOfCall.getVehicle().getCurrentInsurance().getOdPercentage()}else{0}}else{0}}),format.raw/*2072.259*/("""" type="text" readonly>
      </div>
      </div>
      <div class="col-xs-2">
      <div class="form-group">
        <label>Basic OD</label>
         <input class="form-control input-sm" id="basicODId" name="odAmount" value=""""),_display_(/*2078.86*/{if(interOfCall.getVehicle()!=null){if(interOfCall.getVehicle().getCurrentInsurance()!=null){interOfCall.getVehicle().getCurrentInsurance().getOdAmount()}else{0}}else{0}}),format.raw/*2078.256*/("""" type="text" readonly>
      </div>
      </div>
      <div class="col-xs-2">
      <div class="form-group">
        <label>NCB(%)</label>
         <input class="form-control input-sm" id="ncbPercenId" name="ncBPercentage" value=""""),_display_(/*2084.93*/{if(interOfCall.getVehicle()!=null){if(interOfCall.getVehicle().getCurrentInsurance()!=null){interOfCall.getVehicle().getCurrentInsurance().getNcBPercentage()}else{0}}else{0}}),format.raw/*2084.268*/("""" type="text">
      </div>
      </div>
      <div class="col-xs-2">
      <div class="form-group">
        <label>NCB Value</label>
         <input class="form-control input-sm" id="ncbValueId"  name="ncBAmount" value=""""),_display_(/*2090.89*/{if(interOfCall.getVehicle()!=null){if(interOfCall.getVehicle().getCurrentInsurance()!=null){interOfCall.getVehicle().getCurrentInsurance().getNcBAmount()}else{0}}else{0}}),format.raw/*2090.260*/("""" type="text" readonly>
      </div>
      </div>
	  </div>
 <div class="row">
      <div class="col-xs-2">
      <div class="form-group">
        <label>OD Premium</label>
         <input class="form-control input-sm" id="odPremiumId" name="ODpremium" value=""""),_display_(/*2098.89*/{if(interOfCall.getVehicle()!=null){if(interOfCall.getVehicle().getCurrentInsurance()!=null){interOfCall.getVehicle().getCurrentInsurance().getODpremium()}else{0}}else{0}}),format.raw/*2098.260*/("""" type="text" readonly>
      </div>
      </div>
      <div class="col-xs-2">
      <div class="form-group">
        <label>Commercial&nbsp;Disc(%)</label>
         <input class="form-control input-sm" id="commercialDiscId" value=""""),_display_(/*2104.77*/{if(interOfCall.getVehicle()!=null){if(interOfCall.getVehicle().getCurrentInsurance()!=null){interOfCall.getVehicle().getCurrentInsurance().getDiscountPercentage()}else{0}}else{0}}),format.raw/*2104.257*/("""" name="discountPercentage" type="text">
      </div>
      </div>
      <div class="col-xs-2">
      <div class="form-group">
        <label>Disc Value </label>
         <input class="form-control input-sm" id="discValueId" name="discValue" value=""""),_display_(/*2110.89*/{if(interOfCall.getVehicle()!=null){if(interOfCall.getVehicle().getCurrentInsurance()!=null){interOfCall.getVehicle().getCurrentInsurance().getDiscValue()}else{0}}else{0}}),format.raw/*2110.260*/("""" type="text" readonly>
      </div>
      </div>
      <div class="col-xs-2">
      <div class="form-group">
         <label>Total&nbsp;OD&nbsp;Premium</label>
         <input class="form-control input-sm" id="totalODPremiumId" name="totalODpremium" value=""""),_display_(/*2116.99*/{if(interOfCall.getVehicle()!=null){if(interOfCall.getVehicle().getCurrentInsurance()!=null){interOfCall.getVehicle().getCurrentInsurance().getTotalODpremium()}else{0}}else{0}}),format.raw/*2116.275*/("""" type="text" readonly>
      </div>
      </div>
    
    
        </div>
        <b style="color:red;"><u>LIABILITY & BENEFIT (II):</u></b>
		<b style="color:red;margin-left: 272px;"><u>ADD ON COVERS (III):</u></b>
         <div class="row">
      
      <div class="col-xs-2">
       <div class="form-group">
        <label>3rd&nbsp;Party&nbsp;Premium</label>
         <input type="text" class="form-control input-sm" name="" id="thirdPartyPremId" readonly>
      </div>
      </div>
       <div class="col-xs-2">
        <div class="form-group">
        <label>PA&nbsp;Premium</label>
         <input type="text" class="form-control input-sm" name="" value="100" readonly>
      </div>
      </div>
      <div class="col-xs-2">
       <div class="form-group">
          <label>Total&nbsp;Premium</label>
         <input type="text" class="form-control input-sm" name="" id="totalPremiumID" readonly>
      </div>
      </div>
	  
	    <div class="col-xs-2">
       <div class="form-group">
        <label>Add&nbsp;On&nbsp;Cover(%)</label>
         <input type="text" class="form-control input-sm" value=""""),_display_(/*2148.67*/{if(interOfCall.getVehicle()!=null){if(interOfCall.getVehicle().getCurrentInsurance()!=null){interOfCall.getVehicle().getCurrentInsurance().getAdd_ON_CoverPercentage()}else{0}}else{0}}),format.raw/*2148.251*/("""" name="add_ON_CoverPercentage" id="add_ON_CoverPercentageId" >
      </div>
      </div>
       <div class="col-xs-2">
        <div class="form-group">
        <label>Add&nbsp;On&nbsp;Premium </label>
         <input type="text" class="form-control input-sm" name="add_ON_Premium" value=""""),_display_(/*2154.89*/{if(interOfCall.getVehicle()!=null){if(interOfCall.getVehicle().getCurrentInsurance()!=null){interOfCall.getVehicle().getCurrentInsurance().getAdd_ON_Premium()}else{0}}else{0}}),format.raw/*2154.265*/("""" id="addOnPremiumId">
      </div>
      </div>
      
      
        </div>
        
	    <b style="color:red;"><u>PACKAGE PREMIUM (I + II + III):</u></b>
         <div class="row">
     
      <div class="col-xs-2">
        <div class="form-group">
         <label>Total&nbsp;Pkg&nbsp;Premium</label>
         <input type="text" class="form-control input-sm" value=""""),_display_(/*2167.67*/{if(interOfCall.getVehicle()!=null){if(interOfCall.getVehicle().getCurrentInsurance()!=null){interOfCall.getVehicle().getCurrentInsurance().getPremiumAmountBeforeTax()}else{0}}else{0}}),format.raw/*2167.251*/("""" name="premiumAmountBeforeTax" id="totalPackagePremiumId" readonly>
      </div>
      </div>
      <div class="col-xs-2">
        <div class="form-group">
        <label>Service&nbsp;Tax(%)</label>
         <input type="text" class="form-control input-sm" name="serviceTax" value="15" readonly>
      </div>
      </div>
      <div class="col-xs-2">
        <div class="form-group">
        <label>Final Premium</label>
         <input type="text" class="form-control input-sm" value=""""),_display_(/*2179.67*/{if(interOfCall.getVehicle()!=null){if(interOfCall.getVehicle().getCurrentInsurance()!=null){interOfCall.getVehicle().getCurrentInsurance().getPremiumAmountAfterTax()}else{0}}else{0}}),format.raw/*2179.250*/("""" name="premiumAmountAfterTax" id="finalPremiumId" readonly style="background-color: #8BC34A; color:#fff; font-size: 18px;">
      </div>
      </div>
       </div>
</div>
      
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal" id="saveFinalPremium">Save</button>
        </div>
      </div>
      </div>
      
    </div>
	
  <!--------------------------------Side bar popup end------------------------------------------>
   <!--Popup For Driver Allocation-->
<div class="modal fade" id="DriverAllcationPOPUp" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
	   <div class="modal-header" style="text-align:center">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Driver Allocation</h4>																													
        </div>
             <div class="modal-body" style="padding:4px;">
  <div class="panel panel-primary" >
	<div class="panel-heading" style="padding:0px;"> </div>
	<div class="panel-body" style="height: 500px; overflow: auto; ">
			<div class="col-md-12">
			
			<div class="form-inline">
  <div class="form-group">
<label for="comments"><b>CHANGE SERVICE BOOKING DATE :<b></label>
<input type="text" class="form-control datepickerchange" id="changeserviceBookingDate" onchange="ajaxAssignBtnBkreview('workshop','date12345');"   readonly>
  </div>
  </div>
<br />
</div>

              <div class="col-md-6">
                <table class="table table-responsive">
                  <tbody>
                    <tr>
                      <td><strong> New Booking Date: </strong></td>
                      <td class="text-primary" id="newbookingdate"></td>
                    </tr>
                     <tr>
                      <td><strong> New Driver Name:</strong></td>
                      <td class="text-primary" id="newDriver"></td>
                    </tr>
					<tr>
                      <td><strong>New Time Slot:</strong></td>
                      <td class="text-primary" id="newTimeSlot"></td>
                    </tr>
                  </tbody>
                </table>
                </div>
              
	 <div>
        <input type="hidden" id="tempValue" value="0">
        <input type="hidden" id="tempIncreValue" value="0">
        <input type="hidden" name="time_From" id="startValue" value="0">
        <input type="hidden" name="time_To" id="endValue" value="0">
        <input type="hidden" name="driverId" id="driverValue" value="0">
      </div>
  <table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" id="tableID"  style="cursor: pointer;">
    <thead>  
        
    </thead>
    <tbody> 
           
    </tbody>
  </table>
  <button type="button" id="allcateSubmit" class="btn btn-primary pull-right" data-dismiss="modal">Submit</button>
</div>
</div>
 </div>
       </div>
      
    </div>
  </div>
  <!-- Hidden div with details for Auto selection of SA  -->
  <div style="display:none">
  	<input type="hidden" id="preSaDetails"  />
					<input type="hidden" id="newSaDetails"  />
					<select class="form-control" id="serviceAdvisorTemp"  name="serviceAdvisorTemp" style="display:none">
						<option value="0">--Select--</option>
					</select>
  </div>
  
  <div style="display:none">
  	<input type="hidden" id="preSaDetailsIns"  />
					<input type="hidden" id="newSaDetailsIns"  />
					<select class="form-control" id="serviceAdvisorTempIns"  name="serviceAdvisorTempIns" style="display:none">
						<option value="0">--Select--</option>
					</select>
  </div>

""")))}),format.raw/*2278.2*/("""
"""),format.raw/*2279.1*/("""<script src=""""),_display_(/*2279.15*/routes/*2279.21*/.Assets.at("javascripts/commondisposition.js")),format.raw/*2279.67*/("""" type="text/javascript"></script>
""")))}),format.raw/*2280.2*/("""
"""))
      }
    }
  }

  def render(oem:String,statesList:List[String],latestinsurance:Insurance,uniqueid:Long,dealercode:String,dealerName:String,userName:String,customerData:Customer,vehicleData:Vehicle,locationList:List[Location],userData:WyzUser,latestService:Service,smsTemplates:List[SMSTemplate],complaintOFCust:List[String],servicetypeList:List[ServiceTypes],interOfCall:CallInteraction,offersList:List[SpecialOfferMaster],typeDispo:String,dispoOut:Html,dispoInBound:Html): play.twirl.api.HtmlFormat.Appendable = apply(oem,statesList,latestinsurance,uniqueid,dealercode,dealerName,userName,customerData,vehicleData,locationList,userData,latestService,smsTemplates,complaintOFCust,servicetypeList,interOfCall,offersList,typeDispo,dispoOut,dispoInBound)

  def f:((String,List[String],Insurance,Long,String,String,String,Customer,Vehicle,List[Location],WyzUser,Service,List[SMSTemplate],List[String],List[ServiceTypes],CallInteraction,List[SpecialOfferMaster],String,Html,Html) => play.twirl.api.HtmlFormat.Appendable) = (oem,statesList,latestinsurance,uniqueid,dealercode,dealerName,userName,customerData,vehicleData,locationList,userData,latestService,smsTemplates,complaintOFCust,servicetypeList,interOfCall,offersList,typeDispo,dispoOut,dispoInBound) => apply(oem,statesList,latestinsurance,uniqueid,dealercode,dealerName,userName,customerData,vehicleData,locationList,userData,latestService,smsTemplates,complaintOFCust,servicetypeList,interOfCall,offersList,typeDispo,dispoOut,dispoInBound)

  def ref: this.type = this

}


}

/**/
object commonDispositionPage extends commonDispositionPage_Scope0.commonDispositionPage
              /*
                  -- GENERATED --
                  DATE: Thu Dec 28 12:24:27 IST 2017
                  SOURCE: D:/CRMNEW/28th Dec/crm/app/views/commonDispositionPage.scala.html
                  HASH: de35a8f176bf520fd230f95589f2af73c658669a
                  MATRIX: 983->1|1524->446|1554->451|1625->514|1664->516|1692->518|1740->539|1768->540|1798->544|1850->570|1877->571|1909->577|1945->586|1973->587|2004->591|2055->615|2083->616|2112->618|2151->631|2165->637|2246->709|2285->710|2314->712|2371->742|2386->748|2432->773|2505->819|2557->850|2669->935|2699->944|2783->1001|2902->1098|2998->1167|3041->1189|3135->1256|3174->1274|3267->1340|3317->1369|3404->1429|3433->1437|3516->1493|3614->1569|3748->1676|3805->1712|3892->1772|3934->1793|4012->1844|4116->1926|4217->2000|4334->2095|4414->2148|4535->2247|4617->2302|4730->2393|4809->2445|4926->2540|5013->2600|5126->2691|5210->2748|5324->2840|5638->3127|5862->3330|5891->3331|6067->3480|6199->3591|6478->3843|6551->3894|6862->4178|6921->4221|6960->4222|7012->4247|7431->4645|7474->4657|7519->4674|7803->4931|7838->4957|7877->4958|7919->4972|8191->5213|8220->5215|8826->5794|8896->5842|9028->5946|9098->5994|9130->5998|9160->6006|9192->6010|9214->6022|9244->6030|10181->6938|10335->7070|10365->7071|10543->7221|10604->7259|10732->7359|10781->7385|11061->7636|11491->8045|11520->8046|11742->8240|11805->8280|11836->8281|11869->8285|11930->8323|12052->8417|12110->8452|12327->8641|12383->8674|12610->8873|12655->8908|12695->8909|12760->8946|12825->8994|12865->8995|12912->9013|13060->9132|13137->9198|13178->9199|13225->9218|13249->9232|13281->9242|13320->9261|13334->9265|13374->9266|13421->9284|13474->9305|13504->9306|13568->9338|13619->9357|13653->9363|14031->9713|14047->9719|14098->9747|14270->9892|14301->9913|14341->9914|14389->9934|14427->9944|14477->9972|14539->10006|14593->10037|14656->10071|14703->10095|14765->10129|14813->10155|14862->10176|14908->10200|14989->10253|15054->10296|15275->10488|15328->10518|15401->10560|15437->10568|16444->11546|16504->11583|16535->11584|16585->11605|16642->11639|16726->11695|16780->11727|16924->11843|16985->11882|17258->12127|17274->12133|17335->12171|17495->12304|17526->12325|17566->12326|17596->12328|17758->12461|17782->12474|17829->12498|17880->12520|17904->12533|17948->12554|18019->12598|18132->12690|18166->12693|18198->12697|18271->12742|18294->12755|18335->12774|18635->13046|18651->13052|18703->13082|19029->13379|19091->13418|19206->13505|19272->13548|19371->13619|19424->13650|19521->13719|19587->13763|19855->14003|19871->14009|19939->14054|20252->14339|20311->14376|20495->14532|20544->14559|20831->14818|20847->14824|20905->14860|21228->15154|21254->15169|21284->15176|21477->15341|21502->15356|21531->15363|21642->15446|21667->15461|21696->15468|21799->15543|21824->15558|21853->15565|22297->15981|22313->15987|22367->16018|22794->16417|22817->16429|22858->16447|23123->16684|23146->16696|23185->16712|23916->17414|24066->17543|24100->17550|24250->17679|24284->17686|24434->17815|24919->18271|25069->18400|25103->18407|25253->18536|25287->18543|25437->18672|25880->19086|26024->19209|26058->19216|26202->19339|26236->19346|26380->19469|27450->20511|27510->20554|27550->20555|27603->20580|27915->20870|27959->20882|28005->20899|28450->21316|28496->21339|28747->21562|28806->21598|29235->21999|29295->22042|29335->22043|29388->22068|29700->22358|29744->22370|29790->22387|30622->23191|30680->23226|31693->24211|31749->24244|32297->24763|32360->24803|32627->25041|32688->25079|32972->25334|33023->25362|33055->25365|33106->25393|33736->25995|33752->26001|33806->26032|34476->26675|34510->26687|34547->26696|37116->29238|37152->29264|37192->29265|37231->29276|37642->29656|37677->29664|37755->29732|37795->29733|37837->29747|38243->30122|38272->30124|38351->30193|38391->30194|38424->30199|38956->30700|38988->30704|39248->30937|39278->30945|39321->30960|39734->31344|39751->31350|39818->31394|40687->32236|40723->32262|40763->32263|40799->32271|40965->32406|40999->32412|44875->36260|44891->36266|44945->36297|45856->37181|45898->37206|45938->37207|45992->37232|46036->37248|46064->37254|46095->37257|46123->37263|46201->37310|46239->37320|46600->37652|46729->37758|47407->38408|47423->38414|47477->38445|48460->39401|48502->39426|48542->39427|48596->39452|48640->39468|48668->39474|48699->39477|48727->39483|48805->39530|48843->39540|49186->39854|49315->39960|49996->40612|50013->40618|50068->40649|50394->40945|50523->41050|50749->41246|50878->41351|51098->41541|51227->41646|51547->41938|51590->41963|51631->41964|51686->41989|51731->42005|51760->42011|51792->42014|51821->42020|51900->42067|51939->42077|52317->42425|52441->42525|53235->43290|53252->43296|53307->43327|54305->44296|54336->44304|54944->44883|54975->44891|55006->44892|55461->45318|55478->45324|55533->45355|56389->46182|56406->46188|56461->46219|57430->47159|57447->47165|57502->47196|58298->47963|58315->47969|58370->48000|59315->48917|59358->48942|59399->48943|59454->48968|59499->48984|59528->48990|59560->48993|59589->48999|59667->49045|59698->49047|60801->50121|60818->50127|60873->50158|61528->50784|61545->50790|61600->50821|61694->50886|61726->50907|61767->50908|61800->50912|61834->50918|61888->50949|61931->50960|61961->50961|62351->51323|62398->51352|62439->51353|62482->51366|62595->51450|62616->51460|62642->51463|62688->51481|62709->51491|62742->51501|62791->51521|62812->51531|62848->51544|62897->51564|62918->51574|62957->51590|63002->51606|63023->51616|63059->51629|63108->51649|63129->51659|63166->51673|63222->51697|63257->51703|64551->52968|64568->52974|64623->53005|64856->53209|64879->53221|64920->53239|64952->53241|65936->54196|65953->54202|66008->54233|66204->54401|66252->54431|66293->54432|66329->54439|66400->54481|66447->54505|66490->54519|66538->54543|66590->54565|66640->54591|66688->54607|66722->54612|66860->54721|66912->54750|66949->54759|66997->54789|67038->54790|67084->54807|67135->54829|67175->54858|67216->54859|67256->54866|67296->54877|67344->54901|67415->54943|67462->54967|67519->54995|67568->55021|67662->55086|67709->55110|67751->55123|67799->55147|67896->55214|67950->55244|68047->55312|68094->55336|68210->55420|68242->55423|68838->55990|68855->55996|68910->56027|69760->56848|69777->56854|69828->56882|69900->56925|69917->56931|69975->56966|70208->57170|70225->57176|70280->57208|70474->57373|70491->57379|70552->57417|70713->57549|70730->57555|70786->57588|70942->57715|70959->57721|71017->57756|71427->58137|71444->58143|71499->58174|72104->58751|72161->58790|72203->58792|72237->58797|72327->58858|72342->58862|72376->58873|72519->58987|72534->58991|72570->59004|72710->59115|72725->59119|72759->59130|72993->59335|73008->59339|73050->59357|73844->60122|73859->60126|73896->60140|74507->60722|74522->60726|74560->60741|75115->61267|75130->61271|75170->61288|75313->61402|75328->61406|75365->61420|75466->61488|75501->61494|75993->61957|76010->61963|76065->61994|77492->63392|77509->63398|77564->63429|78842->64679|78909->64728|78950->64729|78994->64744|79232->64953|79256->64966|79301->64988|79347->65005|79371->65018|79408->65032|79454->65049|79478->65062|79527->65088|79573->65105|79597->65118|79636->65134|79682->65151|79706->65164|79744->65179|79790->65196|79814->65209|79848->65220|79894->65237|79918->65250|79960->65269|80006->65286|80030->65299|80071->65317|80117->65334|80141->65347|80173->65356|80219->65373|80243->65386|80284->65404|80331->65422|80355->65435|80397->65454|80443->65471|80467->65484|80508->65502|80554->65519|80578->65532|80625->65556|80671->65573|80695->65586|80733->65601|80779->65618|80803->65631|80841->65646|80887->65663|80911->65676|80949->65691|80995->65708|81019->65721|81062->65741|81108->65758|81132->65771|81173->65789|81219->65806|81243->65819|81286->65839|81332->65856|81356->65869|81397->65887|81443->65904|81467->65917|81512->65939|81558->65956|81582->65969|81621->65985|81680->66012|81716->66019|82256->66530|82273->66536|82328->66567|91487->75697|91504->75703|91559->75734|94393->78539|94582->78704|94842->78935|95040->79109|95302->79342|95496->79512|95763->79750|95962->79925|96219->80153|96414->80324|96712->80593|96907->80764|97175->81003|97379->81183|97664->81439|97859->81610|98153->81875|98353->82051|99523->83192|99731->83376|100056->83672|100256->83848|100668->84231|100876->84415|101405->84915|101612->85098|105428->88882|105459->88884|105502->88898|105519->88904|105588->88950|105657->88987
                  LINES: 27->1|32->1|34->3|34->3|34->3|35->4|36->5|36->5|37->6|38->7|38->7|40->9|40->9|40->9|41->10|42->11|42->11|43->12|45->14|45->14|45->14|45->14|46->15|46->15|46->15|46->15|47->16|47->16|48->17|48->17|49->18|49->18|50->19|50->19|51->20|51->20|52->21|52->21|53->22|53->22|54->23|54->23|57->26|57->26|59->28|59->28|61->30|61->30|62->31|62->31|63->32|63->32|64->33|64->33|65->34|65->34|66->35|66->35|67->36|67->36|79->48|87->56|87->56|89->58|93->62|99->68|99->68|101->70|101->70|101->70|103->72|111->80|112->81|113->82|116->85|116->85|116->85|117->86|118->87|119->88|126->95|126->95|127->96|127->96|127->96|127->96|127->96|127->96|127->96|138->107|140->109|140->109|143->112|143->112|144->113|144->113|149->118|157->126|157->126|162->131|162->131|162->131|162->131|162->131|163->132|163->132|168->137|168->137|175->144|175->144|175->144|177->146|177->146|177->146|178->147|178->147|178->147|178->147|179->148|179->148|179->148|180->149|180->149|180->149|181->150|182->151|182->151|183->152|184->153|185->154|196->165|196->165|196->165|198->167|198->167|198->167|201->170|201->170|201->170|201->170|201->170|201->170|201->170|202->171|202->171|202->171|202->171|203->172|203->172|204->173|204->173|207->176|208->177|229->198|229->198|229->198|229->198|229->198|231->200|231->200|233->202|233->202|245->214|245->214|245->214|247->216|247->216|247->216|248->217|248->217|248->217|248->217|248->217|248->217|248->217|250->219|253->222|254->223|256->225|257->226|257->226|257->226|268->237|268->237|268->237|273->242|273->242|274->243|274->243|275->244|275->244|276->245|276->245|287->256|287->256|287->256|290->259|290->259|292->261|292->261|304->273|304->273|304->273|306->275|306->275|306->275|308->277|308->277|308->277|309->278|309->278|309->278|310->279|310->279|310->279|324->293|324->293|324->293|333->302|333->302|333->302|339->308|339->308|339->308|356->325|360->329|361->330|365->334|366->335|370->339|379->348|383->352|384->353|388->357|389->358|393->362|401->370|405->374|406->375|410->379|411->380|415->384|440->409|440->409|440->409|442->411|447->416|448->417|449->418|457->426|457->426|461->430|461->430|470->439|470->439|470->439|472->441|477->446|478->447|479->448|495->464|495->464|515->484|515->484|527->496|527->496|531->500|531->500|535->504|535->504|535->504|535->504|562->531|562->531|562->531|583->552|583->552|585->554|656->625|656->625|656->625|658->627|670->639|672->641|672->641|672->641|674->643|686->655|686->655|686->655|686->655|687->656|705->674|706->675|721->690|721->690|723->692|735->704|735->704|735->704|749->718|749->718|749->718|750->719|751->720|752->721|930->899|930->899|930->899|952->921|952->921|952->921|954->923|954->923|954->923|954->923|954->923|956->925|958->927|969->938|969->938|986->955|986->955|986->955|1009->978|1009->978|1009->978|1011->980|1011->980|1011->980|1011->980|1011->980|1013->982|1015->984|1026->995|1026->995|1044->1013|1044->1013|1044->1013|1051->1020|1051->1020|1055->1024|1055->1024|1058->1027|1058->1027|1067->1036|1067->1036|1067->1036|1069->1038|1069->1038|1069->1038|1069->1038|1069->1038|1071->1040|1073->1042|1082->1051|1082->1051|1102->1071|1102->1071|1102->1071|1131->1100|1131->1100|1148->1117|1148->1117|1148->1117|1161->1130|1161->1130|1161->1130|1193->1162|1193->1162|1193->1162|1229->1198|1229->1198|1229->1198|1259->1228|1259->1228|1259->1228|1284->1253|1284->1253|1284->1253|1286->1255|1286->1255|1286->1255|1286->1255|1286->1255|1288->1257|1289->1258|1330->1299|1330->1299|1330->1299|1352->1321|1352->1321|1352->1321|1353->1322|1353->1322|1353->1322|1354->1323|1354->1323|1354->1323|1355->1324|1355->1324|1370->1339|1370->1339|1370->1339|1371->1340|1372->1341|1372->1341|1372->1341|1373->1342|1373->1342|1373->1342|1374->1343|1374->1343|1374->1343|1375->1344|1375->1344|1375->1344|1376->1345|1376->1345|1376->1345|1377->1346|1377->1346|1377->1346|1379->1348|1380->1349|1418->1387|1418->1387|1418->1387|1422->1391|1422->1391|1422->1391|1422->1391|1447->1416|1447->1416|1447->1416|1452->1421|1452->1421|1452->1421|1453->1422|1453->1422|1453->1422|1453->1422|1453->1422|1453->1422|1453->1422|1454->1423|1456->1425|1458->1427|1458->1427|1459->1428|1459->1428|1459->1428|1462->1431|1462->1431|1462->1431|1462->1431|1462->1431|1462->1431|1462->1431|1463->1432|1463->1432|1464->1433|1464->1433|1465->1434|1465->1434|1465->1434|1465->1434|1465->1434|1465->1434|1467->1436|1467->1436|1472->1441|1473->1442|1493->1462|1493->1462|1493->1462|1517->1486|1517->1486|1517->1486|1518->1487|1518->1487|1518->1487|1521->1490|1521->1490|1521->1490|1522->1491|1522->1491|1522->1491|1525->1494|1525->1494|1525->1494|1526->1495|1526->1495|1526->1495|1536->1505|1536->1505|1536->1505|1557->1526|1557->1526|1557->1526|1558->1527|1559->1528|1559->1528|1559->1528|1560->1529|1560->1529|1560->1529|1561->1530|1561->1530|1561->1530|1565->1534|1565->1534|1565->1534|1581->1550|1581->1550|1581->1550|1592->1561|1592->1561|1592->1561|1601->1570|1601->1570|1601->1570|1602->1571|1602->1571|1602->1571|1603->1572|1604->1573|1621->1590|1621->1590|1621->1590|1667->1636|1667->1636|1667->1636|1712->1681|1712->1681|1712->1681|1714->1683|1716->1685|1716->1685|1716->1685|1717->1686|1717->1686|1717->1686|1718->1687|1718->1687|1718->1687|1719->1688|1719->1688|1719->1688|1720->1689|1720->1689|1720->1689|1721->1690|1721->1690|1721->1690|1722->1691|1722->1691|1722->1691|1723->1692|1723->1692|1723->1692|1724->1693|1724->1693|1724->1693|1725->1694|1725->1694|1725->1694|1726->1695|1726->1695|1726->1695|1727->1696|1727->1696|1727->1696|1728->1697|1728->1697|1728->1697|1729->1698|1729->1698|1729->1698|1730->1699|1730->1699|1730->1699|1731->1700|1731->1700|1731->1700|1732->1701|1732->1701|1732->1701|1733->1702|1733->1702|1733->1702|1734->1703|1734->1703|1734->1703|1735->1704|1735->1704|1735->1704|1736->1705|1736->1705|1736->1705|1737->1706|1737->1706|1737->1706|1739->1708|1740->1709|1756->1725|1756->1725|1756->1725|2027->1996|2027->1996|2027->1996|2097->2066|2097->2066|2103->2072|2103->2072|2109->2078|2109->2078|2115->2084|2115->2084|2121->2090|2121->2090|2129->2098|2129->2098|2135->2104|2135->2104|2141->2110|2141->2110|2147->2116|2147->2116|2179->2148|2179->2148|2185->2154|2185->2154|2198->2167|2198->2167|2210->2179|2210->2179|2309->2278|2310->2279|2310->2279|2310->2279|2310->2279|2311->2280
                  -- GENERATED --
              */
          