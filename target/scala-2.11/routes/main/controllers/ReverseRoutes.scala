
// @GENERATOR:play-routes-compiler
// @SOURCE:D:/CRMNEW/28th Dec/crm/conf/routes
// @DATE:Thu Dec 28 12:24:22 IST 2017

import play.api.mvc.{ QueryStringBindable, PathBindable, Call, JavascriptLiteral }
import play.core.routing.{ HandlerDef, ReverseRouteContext, queryString, dynamicString }


import _root_.controllers.Assets.Asset
import _root_.play.libs.F

// @LINE:7
package controllers {

  // @LINE:168
  class ReverseAssets(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:168
    def at(file:String): Call = {
      implicit val _rrc = new ReverseRouteContext(Map(("path", "/public")))
      Call("GET", _prefix + { _defaultPrefix } + "assets/" + implicitly[PathBindable[String]].unbind("file", file))
    }
  
  }

  // @LINE:35
  class ReverseDealerController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:35
    def adddel(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "superAdmin/addDealer")
    }
  
    // @LINE:528
    def getOEMOfDealer(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/getOEMOfDealer")
    }
  
    // @LINE:41
    def geteditDealer(did:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "superAdmin/dealer/edit/" + implicitly[PathBindable[Long]].unbind("did", did))
    }
  
    // @LINE:39
    def deleteDealerData(did:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "superAdmin/dealer/delete/" + implicitly[PathBindable[Long]].unbind("did", did))
    }
  
    // @LINE:42
    def postEditDealer(did:Long): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "superAdmin/edit/" + implicitly[PathBindable[Long]].unbind("did", did))
    }
  
    // @LINE:38
    def dealerInformation(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "superAdmin/dealerList")
    }
  
    // @LINE:36
    def adddealerForm(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "superAdmin/addDealer")
    }
  
  }

  // @LINE:630
  class ReverseCallRecordingHistoryController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:634
    def callRecordingDataInsurance(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/recordingDataListInsurance")
    }
  
    // @LINE:639
    def otherCallRecordingView(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/otherCallRecording")
    }
  
    // @LINE:633
    def callRecordingViewInsurance(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/callRecordingInsurance")
    }
  
    // @LINE:630
    def callRecordingView(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/callRecording")
    }
  
    // @LINE:631
    def callRecordingData(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/recordingDataList")
    }
  
    // @LINE:637
    def callRecordingDataPSF(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/recordingDataListPSF")
    }
  
    // @LINE:636
    def callRecordingViewPSF(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/callRecordingPSF")
    }
  
    // @LINE:640
    def otherCallRecordingData(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/otherRecordingDataList")
    }
  
  }

  // @LINE:621
  class ReverseAudioStreamController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:621
    def header(interactionId:String): Call = {
      import ReverseRouteContext.empty
      Call("HEAD", _prefix + { _defaultPrefix } + "CRE/audiostream/" + implicitly[PathBindable[String]].unbind("interactionId", dynamicString(interactionId)))
    }
  
    // @LINE:622
    def streamAudio(interactionId:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/audiostream/" + implicitly[PathBindable[String]].unbind("interactionId", dynamicString(interactionId)))
    }
  
  }

  // @LINE:186
  class ReverseCustomerScheduledController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:186
    def readCustomersFromCSV(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "customers/readCustomersFromCSV")
    }
  
    // @LINE:188
    def addcustomerInfo(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "CREManager/addcustomer")
    }
  
  }

  // @LINE:10
  class ReverseWyzUserController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:63
    def viewReportCREManager(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/viewReport")
    }
  
    // @LINE:28
    def userInformation(): Call = {
    
      () match {
      
        // @LINE:28
        case ()  =>
          import ReverseRouteContext.empty
          Call("GET", _prefix + { _defaultPrefix } + "superAdmin/userList")
      
      }
    
    }
  
    // @LINE:161
    def indexPageSalesManager(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "SalesManager")
    }
  
    // @LINE:58
    def loginfo(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "superAdmin/loginformation")
    }
  
    // @LINE:395
    def changepasswordServiceAdvisor(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "ServiceAdvisor/changepassword")
    }
  
    // @LINE:57
    def changepasswordeditingsuperAdmin(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "superAdmin/passwordchangesuperAdmin")
    }
  
    // @LINE:26
    def addApplicationUser(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "superAdmin/addUser")
    }
  
    // @LINE:56
    def changepasswordsuperAdmin(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "superAdmin/changepasswordsuperAdmin")
    }
  
    // @LINE:104
    def indexPageCRE(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE")
    }
  
    // @LINE:110
    def changepasswordCRE(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/changepasswordcre")
    }
  
    // @LINE:31
    def geteditUser(id:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "superAdmin/user/edit/" + implicitly[PathBindable[Long]].unbind("id", id))
    }
  
    // @LINE:25
    def addapp(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "superAdmin/addUser")
    }
  
    // @LINE:18
    def logoutParticularUser(userIs:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "logoutParticularUser/" + implicitly[PathBindable[String]].unbind("userIs", dynamicString(userIs)))
    }
  
    // @LINE:33
    def deleteUserData(id:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "superAdmin/user/delete/" + implicitly[PathBindable[Long]].unbind("id", id))
    }
  
    // @LINE:396
    def changepasswordeditingServiceAdvisor(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "ServiceAdvisor/passwordchange")
    }
  
    // @LINE:11
    def logout(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "logoutUser")
    }
  
    // @LINE:400
    def indexPageOthers(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "OthersLogins")
    }
  
    // @LINE:393
    def indexPageServiceAdvisor(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "ServiceAdvisor")
    }
  
    // @LINE:163
    def viewReport(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "SalesManager/viewReport")
    }
  
    // @LINE:10
    def changePassword(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "changePassword")
    }
  
    // @LINE:66
    def changepassword(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/changepassword")
    }
  
    // @LINE:67
    def changepasswordediting(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "CREManager/passwordchange")
    }
  
    // @LINE:111
    def changepasswordeditingCRE(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "CRE/passwordchangecre")
    }
  
    // @LINE:62
    def indexPageCREManager(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager")
    }
  
    // @LINE:22
    def index(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "superAdmin/index")
    }
  
    // @LINE:32
    def postEditUser(id:Long): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "superAdmin/user/edit/" + implicitly[PathBindable[Long]].unbind("id", id))
    }
  
  }

  // @LINE:215
  class ReverseSMSTemplateController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:600
    def postSMSTemplate(messageTemplate:String, msgAPI:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "postSMSTemplate/" + implicitly[PathBindable[String]].unbind("messageTemplate", dynamicString(messageTemplate)) + "/" + implicitly[PathBindable[String]].unbind("msgAPI", dynamicString(msgAPI)))
    }
  
    // @LINE:599
    def SMSTemplateBySuperAdmin(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "SMSTemplateBySuperAdmin")
    }
  
    // @LINE:603
    def getSelectedUserListforDatatable(selectedUsers:String, setFlag:Boolean, savedsearchname:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "getSelectedUserListforDatatable/" + implicitly[PathBindable[String]].unbind("selectedUsers", dynamicString(selectedUsers)) + "/" + implicitly[PathBindable[Boolean]].unbind("setFlag", setFlag) + "/" + implicitly[PathBindable[String]].unbind("savedsearchname", dynamicString(savedsearchname)))
    }
  
    // @LINE:215
    def checkIfSearchNameExists(searchnamevalue:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "checkIfSearchNameExists/" + implicitly[PathBindable[String]].unbind("searchnamevalue", dynamicString(searchnamevalue)))
    }
  
    // @LINE:602
    def postSMSBulk(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "postSMSBulk")
    }
  
    // @LINE:601
    def getCustomersListBySavedName(savedsearchname:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "getCustomersListBySavedName/" + implicitly[PathBindable[String]].unbind("savedsearchname", dynamicString(savedsearchname)))
    }
  
  }

  // @LINE:625
  class ReverseAllAudioConverter(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:625
    def convertAllFiles(startId:Long, endId:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/audioconverter/" + implicitly[PathBindable[Long]].unbind("startId", startId) + "/" + implicitly[PathBindable[Long]].unbind("endId", endId))
    }
  
  }

  // @LINE:464
  class ReverseServiceAdvisorController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:464
    def getServiceAdvisorComplaints(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "ServiceAdvisor/resolveComplaint")
    }
  
  }

  // @LINE:589
  class ReverseSuperAdminController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:589
    def LocationBySuperAdmin(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "LocationBySuperAdmins")
    }
  
    // @LINE:590
    def postLocationBySuperAdmin(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "postLocationBySuperAdmins")
    }
  
    // @LINE:594
    def postUserBySuperAdmin(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "postUserBySuperAdmins")
    }
  
    // @LINE:593
    def UsersBySuperAdmin(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "UsersBySuperAdmins")
    }
  
    // @LINE:595
    def checkIfUserExists(uname:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "checkExistingUser/" + implicitly[PathBindable[String]].unbind("uname", dynamicString(uname)))
    }
  
  }

  // @LINE:149
  class ReversePSFController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:433
    def ajaxCallForAppointmentPSFData(psfDay:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "ajaxCallForAppointmentPSFData/" + implicitly[PathBindable[String]].unbind("psfDay", dynamicString(psfDay)))
    }
  
    // @LINE:428
    def ajaxCallForIncompletedSurveyPSFData(psfDay:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "ajaxCallForIncompletedSurveyPSFData/" + implicitly[PathBindable[String]].unbind("psfDay", dynamicString(psfDay)))
    }
  
    // @LINE:429
    def ajaxCallForCompletedSurveyPSFData(psfDay:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "ajaxCallForCompletedSurveyPSFData/" + implicitly[PathBindable[String]].unbind("psfDay", dynamicString(psfDay)))
    }
  
    // @LINE:416
    def getPSF30List(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/PSF30List")
    }
  
    // @LINE:420
    def getPSFNextDayList(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/PSFNextDayList")
    }
  
    // @LINE:418
    def getPSF3rdDayList(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/PSF3RDList")
    }
  
    // @LINE:432
    def ajaxCallForDroppedCallsPSFData(psfDay:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "ajaxCallForDroppedCallsPSFData/" + implicitly[PathBindable[String]].unbind("psfDay", dynamicString(psfDay)))
    }
  
    // @LINE:431
    def ajaxCallForNonContactsPSFData(psfDay:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "ajaxCallForNonContactsPSFData/" + implicitly[PathBindable[String]].unbind("psfDay", dynamicString(psfDay)))
    }
  
    // @LINE:430
    def ajaxCallForDissatisfiedPSFData(psfDay:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "ajaxCallForDissatisfiedPSFData/" + implicitly[PathBindable[String]].unbind("psfDay", dynamicString(psfDay)))
    }
  
    // @LINE:496
    def postCommonPSFDispositionPage(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "CRE/psfDispo")
    }
  
    // @LINE:427
    def ajaxCallForFollowUpRequiredPSFData(psfDay:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "ajaxCallForFollowUpRequiredPSFData/" + implicitly[PathBindable[String]].unbind("psfDay", dynamicString(psfDay)))
    }
  
    // @LINE:149
    def getPSFFollowUpNotificationToday(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/ajax/getPSFFollowupNotificationOfToday")
    }
  
    // @LINE:414
    def getPSF15List(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/PSF15List")
    }
  
    // @LINE:426
    def assignedInteractionPSFData(name:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "assignedInteractionTablePSFData/" + implicitly[PathBindable[String]].unbind("name", dynamicString(name)))
    }
  
    // @LINE:495
    def getCommonPSFDispositionPage(cid:Long, vehicle_id:Long, interactionid:Long, dispositionHistory:Long, typeOfPSF:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/psfDispo/" + implicitly[PathBindable[Long]].unbind("cid", cid) + "/" + implicitly[PathBindable[Long]].unbind("vehicle_id", vehicle_id) + "/" + implicitly[PathBindable[Long]].unbind("interactionid", interactionid) + "/" + implicitly[PathBindable[Long]].unbind("dispositionHistory", dispositionHistory) + "/" + implicitly[PathBindable[Long]].unbind("typeOfPSF", typeOfPSF))
    }
  
    // @LINE:422
    def getPSF4thDayList(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/PSF4thDayList")
    }
  
    // @LINE:412
    def getPSFList(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/PSFList")
    }
  
  }

  // @LINE:549
  class ReverseSMSandEmailController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:549
    def startTriggerSMS(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "SMSTriggger")
    }
  
  }

  // @LINE:532
  class ReverseFileUploadController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:532
    def uploadPage(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "fileuploadPage")
    }
  
    // @LINE:547
    def uploadReportPage(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "uploadReportPage")
    }
  
    // @LINE:534
    def headRequest(): Call = {
      import ReverseRouteContext.empty
      Call("HEAD", _prefix + { _defaultPrefix } + "postfile")
    }
  
    // @LINE:537
    def downloadFile(id:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "downloadFile/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)))
    }
  
    // @LINE:533
    def startUpload(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "postfile")
    }
  
    // @LINE:542
    def getUploadsList(utype:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "uploadList/" + implicitly[PathBindable[String]].unbind("utype", dynamicString(utype)))
    }
  
    // @LINE:538
    def downloadErrors(id:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "downloadErrors/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)))
    }
  
    // @LINE:545
    def getExistingFiles(utype:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "getExistingFiles/" + implicitly[PathBindable[String]].unbind("utype", dynamicString(utype)))
    }
  
    // @LINE:543
    def getUploadsListById(upId:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "uploadListById/" + implicitly[PathBindable[Long]].unbind("upId", upId))
    }
  
    // @LINE:536
    def deleteFile(id:String): Call = {
      import ReverseRouteContext.empty
      Call("DELETE", _prefix + { _defaultPrefix } + "deletefile/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)))
    }
  
  }

  // @LINE:474
  class ReverseAutoSelInsuranceAgentController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:476
    def ajaxupdateInsuAgentChange(date:String, preSaDetails:String, newSaDetails:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/callDispositionPageIns/" + implicitly[PathBindable[String]].unbind("date", dynamicString(date)) + "/" + implicitly[PathBindable[String]].unbind("preSaDetails", dynamicString(preSaDetails)) + "/" + implicitly[PathBindable[String]].unbind("newSaDetails", dynamicString(newSaDetails)))
    }
  
    // @LINE:474
    def ajaxAutoInsurAgentSelection(date:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/callDispositionPageIns/" + implicitly[PathBindable[String]].unbind("date", dynamicString(date)))
    }
  
    // @LINE:475
    def ajaxAutoInsurAgentSelectionList(saDetails:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/callDispositionPageIns/" + implicitly[PathBindable[String]].unbind("saDetails", dynamicString(saDetails)))
    }
  
  }

  // @LINE:95
  class ReverseCallInteractionController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:277
    def ajaxCallForAddcustomerinfo(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "CRE/saveaddcustomerinfo")
    }
  
    // @LINE:238
    def get_required_fields(selected_part:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "uploadFormat/MapMasterFormat/" + implicitly[PathBindable[String]].unbind("selected_part", dynamicString(selected_part)))
    }
  
    // @LINE:362
    def getServiceDataOfCustomer(customerId:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/serviceDataOfCustomer/" + implicitly[PathBindable[Long]].unbind("customerId", customerId))
    }
  
    // @LINE:349
    def postUpdateRangeOfUnavailabiltyOfUsers(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "CREManager/updateRangeOfUnavailabilty")
    }
  
    // @LINE:369
    def getComplaintHistoryVeh(vehicalId:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/getComplaintHistoryOfvehicalId/" + implicitly[PathBindable[Long]].unbind("vehicalId", vehicalId))
    }
  
    // @LINE:351
    def getRoasterTable(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/todaysRoaster")
    }
  
    // @LINE:344
    def getComplaintHistoryAll(complaintNumber:String, vehregnumber:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/getComplaintHistoryAll/" + implicitly[PathBindable[String]].unbind("complaintNumber", dynamicString(complaintNumber)) + "/" + implicitly[PathBindable[String]].unbind("vehregnumber", dynamicString(vehregnumber)))
    }
  
    // @LINE:225
    def getSelectedAssignCalls(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "CREManager/assignCall")
    }
  
    // @LINE:207
    def assigningCallsToCRE(selectAgent:String, fromData:String, toDate:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/assignCalls/" + implicitly[PathBindable[String]].unbind("selectAgent", dynamicString(selectAgent)) + "/" + implicitly[PathBindable[String]].unbind("fromData", dynamicString(fromData)) + "/" + implicitly[PathBindable[String]].unbind("toDate", dynamicString(toDate)))
    }
  
    // @LINE:564
    def getFunctionsListByLoc(selectedCity:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/listFunctionsByLocation/" + implicitly[PathBindable[String]].unbind("selectedCity", dynamicString(selectedCity)))
    }
  
    // @LINE:282
    def ajaxAddRegistrationno(vehicalRegNo:String, customer_id:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/saveaddcustomervregistrationno/" + implicitly[PathBindable[String]].unbind("vehicalRegNo", dynamicString(vehicalRegNo)) + "/" + implicitly[PathBindable[Long]].unbind("customer_id", customer_id))
    }
  
    // @LINE:220
    def postAddCustomer(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "postAddCustomer")
    }
  
    // @LINE:553
    def getCRESByWorkshops(workshops:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "getCRESByWorkshop/" + implicitly[PathBindable[String]].unbind("workshops", dynamicString(workshops)))
    }
  
    // @LINE:97
    def getServiceBookedDataCREMan(selectedAgent:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/getServiceBookedDataCREMan/" + implicitly[PathBindable[String]].unbind("selectedAgent", dynamicString(selectedAgent)))
    }
  
    // @LINE:285
    def saveaddcustomermobno(custMobNo:String, wyzUser_id:Long, customer_Id:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/saveaddcustomermobno/" + implicitly[PathBindable[String]].unbind("custMobNo", dynamicString(custMobNo)) + "/" + implicitly[PathBindable[Long]].unbind("wyzUser_id", wyzUser_id) + "/" + implicitly[PathBindable[Long]].unbind("customer_Id", customer_Id))
    }
  
    // @LINE:366
    def getLeadByUserLocation(userLocation:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/leadBasedOnLocation/" + implicitly[PathBindable[Long]].unbind("userLocation", userLocation))
    }
  
    // @LINE:329
    def updateComplaintsResolution(complaintNum:String, reasonFor:String, complaintStatus:String, customerStatus:String, actionTaken:String, resolutionBy:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/updateComplaintsResolution/" + implicitly[PathBindable[String]].unbind("complaintNum", dynamicString(complaintNum)) + "/" + implicitly[PathBindable[String]].unbind("reasonFor", dynamicString(reasonFor)) + "/" + implicitly[PathBindable[String]].unbind("complaintStatus", dynamicString(complaintStatus)) + "/" + implicitly[PathBindable[String]].unbind("customerStatus", dynamicString(customerStatus)) + "/" + implicitly[PathBindable[String]].unbind("actionTaken", dynamicString(actionTaken)) + "/" + implicitly[PathBindable[String]].unbind("resolutionBy", dynamicString(resolutionBy)))
    }
  
    // @LINE:370
    def getSMSHistoryOfCustomer(customerId:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/getSMSHistoryOfCustomerId/" + implicitly[PathBindable[Long]].unbind("customerId", customerId))
    }
  
    // @LINE:96
    def getFollowUpRequiredDataCREMan(selectedAgent:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/getFollowUpRequiredDataCREMan/" + implicitly[PathBindable[String]].unbind("selectedAgent", dynamicString(selectedAgent)))
    }
  
    // @LINE:95
    def getAssignedCallsOfCREManager(selectedAgent:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/getAssignedCallsOfCREManager/" + implicitly[PathBindable[String]].unbind("selectedAgent", dynamicString(selectedAgent)))
    }
  
    // @LINE:459
    def getCRESListBasedOnWorkshop(workshopId:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/listCREsByWorkshop/" + implicitly[PathBindable[Long]].unbind("workshopId", workshopId))
    }
  
    // @LINE:352
    def toUpdateRoasterUnAvailablity(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "CREManager/todaysRoaster")
    }
  
    // @LINE:310
    def getDriverServiceBooked(driverId:Long, userId:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/getDriverServices/" + implicitly[PathBindable[Long]].unbind("driverId", driverId) + "/" + implicitly[PathBindable[Long]].unbind("userId", userId))
    }
  
    // @LINE:367
    def getLeadTagByDepartment(userLocation:Long, departmentName:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/leadBasedOnDepartment/" + implicitly[PathBindable[Long]].unbind("userLocation", userLocation) + "/" + implicitly[PathBindable[Long]].unbind("departmentName", departmentName))
    }
  
    // @LINE:276
    def ajaxAddPhoneNumber(phone_no:String, customer_id:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/addphone/" + implicitly[PathBindable[String]].unbind("phone_no", dynamicString(phone_no)) + "/" + implicitly[PathBindable[Long]].unbind("customer_id", customer_id))
    }
  
    // @LINE:229
    def getReAssignmentCalls(selectAgent:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/getReAssignmentCalls/" + implicitly[PathBindable[String]].unbind("selectAgent", dynamicString(selectAgent)))
    }
  
    // @LINE:237
    def upload_Excel_Sheet(uploadId:String, uploadType:String, resultData:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/uploadExcelSheet/" + implicitly[PathBindable[String]].unbind("uploadId", dynamicString(uploadId)) + "/" + implicitly[PathBindable[String]].unbind("uploadType", dynamicString(uploadType)) + "/" + implicitly[PathBindable[String]].unbind("resultData", dynamicString(resultData)))
    }
  
    // @LINE:565
    def getusersByFuncandLocation(city:String, function:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/listUsersByFuncandLoc/" + implicitly[PathBindable[String]].unbind("city", dynamicString(city)) + "/" + implicitly[PathBindable[String]].unbind("function", dynamicString(function)))
    }
  
    // @LINE:315
    def getListWorkshopByLocation(selectedCity:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/listWorkshops/" + implicitly[PathBindable[String]].unbind("selectedCity", dynamicString(selectedCity)))
    }
  
    // @LINE:213
    def addCustomerCRE(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "addCustomer")
    }
  
    // @LINE:98
    def getServiceNotRequiredDataCREMan(selectedAgent:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/getServiceNotRequiredDataCREMan/" + implicitly[PathBindable[String]].unbind("selectedAgent", dynamicString(selectedAgent)))
    }
  
    // @LINE:357
    def getRosterDataByUser(selectAgent:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/loadRosterData/" + implicitly[PathBindable[String]].unbind("selectAgent", dynamicString(selectAgent)))
    }
  
    // @LINE:253
    def postComplaints(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "postComplaints")
    }
  
    // @LINE:561
    def viewAllComplaintsReadOnlyAccess(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "viewAllComplaints")
    }
  
    // @LINE:359
    def deleteUnavialbility(id:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/deleteUnavialbility/" + implicitly[PathBindable[Long]].unbind("id", id))
    }
  
    // @LINE:230
    def reAssigningCallsofselectCRE(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "CREManager/changeAssignment")
    }
  
    // @LINE:343
    def getComplaintsDataByFilter(filterData:String, varLoc:String, varfunc:String, varraisedDate:String, varendDate:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/getComplaintsDataByFilter/" + implicitly[PathBindable[String]].unbind("filterData", dynamicString(filterData)) + "/" + implicitly[PathBindable[String]].unbind("varLoc", dynamicString(varLoc)) + "/" + implicitly[PathBindable[String]].unbind("varfunc", dynamicString(varfunc)) + "/" + implicitly[PathBindable[String]].unbind("varraisedDate", dynamicString(varraisedDate)) + "/" + implicitly[PathBindable[String]].unbind("varendDate", dynamicString(varendDate)))
    }
  
    // @LINE:451
    def getExistingVehicleRegCount(vehicleReg:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/vehicleRegNoExist/" + implicitly[PathBindable[String]].unbind("vehicleReg", dynamicString(vehicleReg)))
    }
  
    // @LINE:258
    def postComplaintsByManager(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "postComplaintsByManager")
    }
  
    // @LINE:551
    def getWorkshopsByLocations(locations:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "getworkshopByLocation/" + implicitly[PathBindable[String]].unbind("locations", dynamicString(locations)))
    }
  
    // @LINE:99
    def getNonContactsDataCREMan(selectedAgent:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/getNonContactsDataCREMan/" + implicitly[PathBindable[String]].unbind("selectedAgent", dynamicString(selectedAgent)))
    }
  
    // @LINE:281
    def ajaxAddChassisno(chassisNo:String, customer_id:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/saveaddcustomerchassisno/" + implicitly[PathBindable[String]].unbind("chassisNo", dynamicString(chassisNo)) + "/" + implicitly[PathBindable[Long]].unbind("customer_id", customer_id))
    }
  
    // @LINE:326
    def ajaxcomplaintNumber(complaintNum:String, veh_num:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/searchcomplaintNum/" + implicitly[PathBindable[String]].unbind("complaintNum", dynamicString(complaintNum)) + "/" + implicitly[PathBindable[String]].unbind("veh_num", dynamicString(veh_num)))
    }
  
    // @LINE:354
    def rosterOfUnavailabiltyByUser(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/rosterOfUnavailabiltyByUser")
    }
  
    // @LINE:458
    def getDriverListBasedOnworkshop(workshopId:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/listDrivers/" + implicitly[PathBindable[Long]].unbind("workshopId", workshopId))
    }
  
    // @LINE:363
    def getCallHistoryOfvehicalId(vehicalId:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/getCallHistoryOfvehicalId/" + implicitly[PathBindable[Long]].unbind("vehicalId", vehicalId))
    }
  
    // @LINE:201
    def getCustomerByInteractionId(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "getCustomerByInteractionId")
    }
  
    // @LINE:283
    def ajaxAddEngineno(engineNo:String, customer_id:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/saveaddcustomerengineno/" + implicitly[PathBindable[String]].unbind("engineNo", dynamicString(engineNo)) + "/" + implicitly[PathBindable[Long]].unbind("customer_id", customer_id))
    }
  
    // @LINE:333
    def complaintAssignment(complaintNum:String, veh_num:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/complaintAssignment/" + implicitly[PathBindable[String]].unbind("complaintNum", dynamicString(complaintNum)) + "/" + implicitly[PathBindable[String]].unbind("veh_num", dynamicString(veh_num)))
    }
  
    // @LINE:235
    def upload_file_Format_submit(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "CREManager/uploadExcelData")
    }
  
    // @LINE:239
    def ajax_transaction_data_upload(selected_format:String): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "master/excelUpload/" + implicitly[PathBindable[String]].unbind("selected_format", dynamicString(selected_format)))
    }
  
    // @LINE:228
    def toChangeAssigment(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/changeAssignment")
    }
  
    // @LINE:227
    def getAssignedList(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/getAssignList")
    }
  
    // @LINE:355
    def addRosterOfUserByAjax(selectAgent:String, fromDate:String, toDate:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/addRosterOfUserByAjax/" + implicitly[PathBindable[String]].unbind("selectAgent", dynamicString(selectAgent)) + "/" + implicitly[PathBindable[String]].unbind("fromDate", dynamicString(fromDate)) + "/" + implicitly[PathBindable[String]].unbind("toDate", dynamicString(toDate)))
    }
  
    // @LINE:556
    def getCitiesByState(selState:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "getCityByStates/" + implicitly[PathBindable[String]].unbind("selState", dynamicString(selState)))
    }
  
    // @LINE:286
    def saveaddcustomerEmail(custEmail:String, wyzUser_id:Long, customer_Id:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/saveaddcustomerEmail/" + implicitly[PathBindable[String]].unbind("custEmail", dynamicString(custEmail)) + "/" + implicitly[PathBindable[Long]].unbind("wyzUser_id", wyzUser_id) + "/" + implicitly[PathBindable[Long]].unbind("customer_Id", customer_Id))
    }
  
    // @LINE:138
    def getServiceBookedData(selectAgent:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/getServiceBookedData/" + implicitly[PathBindable[String]].unbind("selectAgent", dynamicString(selectAgent)))
    }
  
    // @LINE:137
    def getFollowUpRequiredData(selectAgent:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/getFollowUpRequiredData/" + implicitly[PathBindable[String]].unbind("selectAgent", dynamicString(selectAgent)))
    }
  
    // @LINE:205
    def getAssignedInteraction(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/UploadAllCalls")
    }
  
    // @LINE:302
    def getWorkShopServiceBooked(workId:Long, userId:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/getWorkshopServices/" + implicitly[PathBindable[Long]].unbind("workId", workId) + "/" + implicitly[PathBindable[Long]].unbind("userId", userId))
    }
  
    // @LINE:340
    def downloadExcelFile(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "CREManager/viewComplaints1")
    }
  
    // @LINE:457
    def sendCustomSMSAjax(rerenceNumber:Long): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "CRE/sendSMS/" + implicitly[PathBindable[Long]].unbind("rerenceNumber", rerenceNumber))
    }
  
    // @LINE:224
    def getPageForAssigningCalls(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/assignCall")
    }
  
    // @LINE:331
    def updateComplaintsResolutionClosed(complaintNumClosed:String, reasonForClosed:String, complaintStatusClosed:String, customerStatusClosed:String, actionTakenClosed:String, resolutionByClosed:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/updateComplaintsResolutionClosed/" + implicitly[PathBindable[String]].unbind("complaintNumClosed", dynamicString(complaintNumClosed)) + "/" + implicitly[PathBindable[String]].unbind("reasonForClosed", dynamicString(reasonForClosed)) + "/" + implicitly[PathBindable[String]].unbind("complaintStatusClosed", dynamicString(complaintStatusClosed)) + "/" + implicitly[PathBindable[String]].unbind("customerStatusClosed", dynamicString(customerStatusClosed)) + "/" + implicitly[PathBindable[String]].unbind("actionTakenClosed", dynamicString(actionTakenClosed)) + "/" + implicitly[PathBindable[String]].unbind("resolutionByClosed", dynamicString(resolutionByClosed)))
    }
  
    // @LINE:255
    def assignComplaints(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/assignComplaints")
    }
  
    // @LINE:208
    def getAssignedCallsOfUser(selectAgent:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/getAssignedCalls/" + implicitly[PathBindable[String]].unbind("selectAgent", dynamicString(selectAgent)))
    }
  
    // @LINE:139
    def getNonContactsData(selectAgent:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/getNonContactsData/" + implicitly[PathBindable[String]].unbind("selectAgent", dynamicString(selectAgent)))
    }
  
    // @LINE:364
    def getServiceAdvisorOfWorkshop(workshopId:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/listServiceAdvisors/" + implicitly[PathBindable[Long]].unbind("workshopId", workshopId))
    }
  
    // @LINE:236
    def ajax_master_data_upload_fileFormat(selected_part:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "uploadFormat/getFormElement/" + implicitly[PathBindable[String]].unbind("selected_part", dynamicString(selected_part)))
    }
  
    // @LINE:252
    def addComplaints(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "complaints")
    }
  
    // @LINE:311
    def reAssignAgentUpdate(rowId:Long, wyzUserId:Long, post_id:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/reAssignAgent/" + implicitly[PathBindable[Long]].unbind("rowId", rowId) + "/" + implicitly[PathBindable[Long]].unbind("wyzUserId", wyzUserId) + "/" + implicitly[PathBindable[Long]].unbind("post_id", post_id))
    }
  
    // @LINE:244
    def getExcelColumnsOFUploadFormat(upload_format:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "getExcelColumns/" + implicitly[PathBindable[String]].unbind("upload_format", dynamicString(upload_format)))
    }
  
    // @LINE:306
    def getSerAdvServiceBooked(serviceAdvisorId:Long, userId:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/getServiceAdvisorServices/" + implicitly[PathBindable[Long]].unbind("serviceAdvisorId", serviceAdvisorId) + "/" + implicitly[PathBindable[Long]].unbind("userId", userId))
    }
  
    // @LINE:330
    def updateComplaintsResolutionByManager(complaintNum:String, reasonFor:String, complaintStatus:String, customerStatus:String, actionTaken:String, resolutionBy:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/updateComplaintsResolutionByManager/" + implicitly[PathBindable[String]].unbind("complaintNum", dynamicString(complaintNum)) + "/" + implicitly[PathBindable[String]].unbind("reasonFor", dynamicString(reasonFor)) + "/" + implicitly[PathBindable[String]].unbind("complaintStatus", dynamicString(complaintStatus)) + "/" + implicitly[PathBindable[String]].unbind("customerStatus", dynamicString(customerStatus)) + "/" + implicitly[PathBindable[String]].unbind("actionTaken", dynamicString(actionTaken)) + "/" + implicitly[PathBindable[String]].unbind("resolutionBy", dynamicString(resolutionBy)))
    }
  
    // @LINE:206
    def toUploadInteractionsByCREManager(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "CREManager/UploadAllCalls")
    }
  
    // @LINE:365
    def getWorkshopListName(workshopId:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/listWorkshopsIfExisting/" + implicitly[PathBindable[Long]].unbind("workshopId", workshopId))
    }
  
    // @LINE:325
    def complaintsResolution(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "complaintResolution")
    }
  
    // @LINE:257
    def addComplaintsByManager(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "complaintsByManager")
    }
  
    // @LINE:100
    def getDroppedCallsDataCREMan(selectedAgent:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/getDroppedCallsDataCREMan/" + implicitly[PathBindable[String]].unbind("selectedAgent", dynamicString(selectedAgent)))
    }
  
    // @LINE:256
    def updateComplaints(id:Long, comments:String, selected_value:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/updateComplaints/" + implicitly[PathBindable[Long]].unbind("id", id) + "/" + implicitly[PathBindable[String]].unbind("comments", dynamicString(comments)) + "/" + implicitly[PathBindable[String]].unbind("selected_value", dynamicString(selected_value)))
    }
  
    // @LINE:336
    def saveaddComplaintAssignModile(complaintNum:String, city:String, workshop:String, functions:String, ownership:String, priority:String, esclation1:String, esclation2:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/saveaddComplaintAssignModile/" + implicitly[PathBindable[String]].unbind("complaintNum", dynamicString(complaintNum)) + "/" + implicitly[PathBindable[String]].unbind("city", dynamicString(city)) + "/" + implicitly[PathBindable[String]].unbind("workshop", dynamicString(workshop)) + "/" + implicitly[PathBindable[String]].unbind("functions", dynamicString(functions)) + "/" + implicitly[PathBindable[String]].unbind("ownership", dynamicString(ownership)) + "/" + implicitly[PathBindable[String]].unbind("priority", dynamicString(priority)) + "/" + implicitly[PathBindable[String]].unbind("esclation1", dynamicString(esclation1)) + "/" + implicitly[PathBindable[String]].unbind("esclation2", dynamicString(esclation2)))
    }
  
    // @LINE:348
    def updateRangeOfUnavailabiltyOfUsers(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/updateRangeOfUnavailabilty")
    }
  
    // @LINE:337
    def viewAllComplaints1(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/viewComplaints1")
    }
  
    // @LINE:234
    def upload_file_Format_show(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/uploadExcelData")
    }
  
    // @LINE:368
    def getTagNameByUpselLeadType(userLocation:Long, upselIDTag:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/upselLeadIdTag/" + implicitly[PathBindable[Long]].unbind("userLocation", userLocation) + "/" + implicitly[PathBindable[Long]].unbind("upselIDTag", upselIDTag))
    }
  
    // @LINE:460
    def getCREListBasedOnWorkshopCallHistory(workshopId:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/listCREsByWorkshopcallhistory/" + implicitly[PathBindable[Long]].unbind("workshopId", workshopId))
    }
  
    // @LINE:212
    def searchByCustomer(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "searchByCustomer")
    }
  
    // @LINE:563
    def getListWorkshopByLocationById(selectedCity:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/listWorkshopsByID/" + implicitly[PathBindable[Long]].unbind("selectedCity", selectedCity))
    }
  
    // @LINE:327
    def ajaxcomplaintNumberClosed(complaintNumClosed:String, veh_numclosed:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/searchcomplaintNumClosed/" + implicitly[PathBindable[String]].unbind("complaintNumClosed", dynamicString(complaintNumClosed)) + "/" + implicitly[PathBindable[String]].unbind("veh_numclosed", dynamicString(veh_numclosed)))
    }
  
    // @LINE:222
    def getMediaFileCallInteractions(id:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "getMediaFile/" + implicitly[PathBindable[Long]].unbind("id", id))
    }
  
    // @LINE:243
    def getDownloadExcelFormat(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/downloadExcelFormat")
    }
  
    // @LINE:320
    def getWorkshopSummaryDetails(selectedWorkshop:Long, schDate:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/listWorkshopSummary/" + implicitly[PathBindable[Long]].unbind("selectedWorkshop", selectedWorkshop) + "/" + implicitly[PathBindable[String]].unbind("schDate", dynamicString(schDate)))
    }
  
    // @LINE:140
    def getDroppedCallsData(selectAgent:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/getDroppedCallsData/" + implicitly[PathBindable[String]].unbind("selectAgent", dynamicString(selectAgent)))
    }
  
    // @LINE:141
    def getServiceNotRequiredData(selectAgent:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/getServiceNotRequiredData/" + implicitly[PathBindable[String]].unbind("selectAgent", dynamicString(selectAgent)))
    }
  
    // @LINE:324
    def ajaxsearchVehicle(veh_number:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/searchvehRegNo/" + implicitly[PathBindable[String]].unbind("veh_number", dynamicString(veh_number)))
    }
  
    // @LINE:358
    def updateRosterOfUserByAjaxVal(selectAgent:String, From_Date:String, To_Date:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/updateRosterOfUserByAjaxData/" + implicitly[PathBindable[String]].unbind("selectAgent", dynamicString(selectAgent)) + "/" + implicitly[PathBindable[String]].unbind("From_Date", dynamicString(From_Date)) + "/" + implicitly[PathBindable[String]].unbind("To_Date", dynamicString(To_Date)))
    }
  
  }

  // @LINE:387
  class ReverseUploadExcelController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:387
    def uploadExcelData(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "CREManager/uploadExcelPOST")
    }
  
  }

  // @LINE:88
  class ReverseCallInfoController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:147
    def getFollowUpRemainderOfMissedSchedules(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/ajax/getFollowUpRemainderOfMissedSchedules")
    }
  
    // @LINE:89
    def getPSFCallLogViewPage(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/PSFCallLogPageCREManager")
    }
  
    // @LINE:88
    def getCallDispositionBucketForCREMan(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/CallDispositionBucketForCREMan")
    }
  
    // @LINE:106
    def getcallLogEditForCRE(id:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/toeditcallLog/" + implicitly[PathBindable[Long]].unbind("id", id))
    }
  
    // @LINE:146
    def getFollowUpNotifyBeforeTime(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/ajax/getFollowUpNotificationBeforeTime")
    }
  
    // @LINE:482
    def getUpsellLeadsSeletedInLastSB(sr_int_id:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/upsellSelectedLastSB/" + implicitly[PathBindable[Long]].unbind("sr_int_id", sr_int_id))
    }
  
    // @LINE:123
    def startInitiatingOfCall(phonenumber:Long, uniqueid:Long, customerId:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/ajax/initiateCall/" + implicitly[PathBindable[Long]].unbind("phonenumber", phonenumber) + "/" + implicitly[PathBindable[Long]].unbind("uniqueid", uniqueid) + "/" + implicitly[PathBindable[Long]].unbind("customerId", customerId))
    }
  
    // @LINE:177
    def startSyncOperation(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "callInfo/startSync")
    }
  
    // @LINE:156
    def deleteCallLog(dealercode:String, id:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/deleteCall/" + implicitly[PathBindable[Long]].unbind("id", id) + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("dealercode", dealercode)))))
    }
  
    // @LINE:148
    def getFollowUpNotificationToday(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/ajax/getFollowUpNotificationOfToday")
    }
  
    // @LINE:178
    def stopSyncOperation(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "callInfo/stopSync")
    }
  
    // @LINE:584
    def getLeadNamesbyLeadId(leadId:Long, userId:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/getLeadNamesbyLeadId/" + implicitly[PathBindable[Long]].unbind("leadId", leadId) + "/" + implicitly[PathBindable[Long]].unbind("userId", userId))
    }
  
    // @LINE:157
    def deleteSchCalllog(dealercode:String, id:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/deleteSchCall/" + implicitly[PathBindable[Long]].unbind("id", id) + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("dealercode", dealercode)))))
    }
  
    // @LINE:586
    def getCheckVehicleRegExist(vehRegId:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/checkingVehicleRegNo/" + implicitly[PathBindable[String]].unbind("vehRegId", dynamicString(vehRegId)))
    }
  
    // @LINE:107
    def postcallLogeditForCRE(id:Long): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "CRE/toeditcallLog/" + implicitly[PathBindable[Long]].unbind("id", id))
    }
  
    // @LINE:90
    def getInsuranceCallLogViewPage(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/InsuranceCallLogPageCREManager")
    }
  
    // @LINE:145
    def getFollowUpTableDataOfCRE(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/listTodaysFollowUp")
    }
  
    // @LINE:182
    def todeleteFilesFromDirectory(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "delete/allFilesFromTmp")
    }
  
  }

  // @LINE:486
  class ReverseInsuranceController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:520
    def insuranceFollowUpNotificationToday(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/ajax/getInsuranceFollowupNotificationOfToday")
    }
  
    // @LINE:513
    def showRoomList(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/showRoomList")
    }
  
    // @LINE:500
    def getPageForAssigningCallsInsurance(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/assignCallInsurance")
    }
  
    // @LINE:489
    def getAssignedInsuInteraction(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "insurance/assignedInsuInteraction")
    }
  
    // @LINE:527
    def insuranceHistoryOfCustomerId(customerId:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/getInsuranceHistoryOfCustomerId/" + implicitly[PathBindable[Long]].unbind("customerId", customerId))
    }
  
    // @LINE:507
    def getNCBValueByBasicValue(ncbPercenVal:Double, basicODValue:Double): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/getNCBBYBasicODValue/" + implicitly[PathBindable[Double]].unbind("ncbPercenVal", ncbPercenVal) + "/" + implicitly[PathBindable[Double]].unbind("basicODValue", basicODValue))
    }
  
    // @LINE:506
    def getBasicODVaue(odvValue:Double, idvValue:Double): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/getBasicODValue/" + implicitly[PathBindable[Double]].unbind("odvValue", odvValue) + "/" + implicitly[PathBindable[Double]].unbind("idvValue", idvValue))
    }
  
    // @LINE:486
    def getCommonCallDispositionForm(cid:Long, vehicle_id:Long, typeDispo:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/getDispositionFormPage/" + implicitly[PathBindable[Long]].unbind("cid", cid) + "/" + implicitly[PathBindable[Long]].unbind("vehicle_id", vehicle_id) + "/" + implicitly[PathBindable[String]].unbind("typeDispo", dynamicString(typeDispo)))
    }
  
    // @LINE:515
    def downloadInsuranceErrorData(uploadId:String, uploadType:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/downloadExcelSheet/" + implicitly[PathBindable[String]].unbind("uploadId", dynamicString(uploadId)) + "/" + implicitly[PathBindable[String]].unbind("uploadType", dynamicString(uploadType)))
    }
  
    // @LINE:491
    def getNonContactDroppedInsuranceDispoData(typeOfdispo:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "insurance/nonContactedDispoInterac/" + implicitly[PathBindable[Long]].unbind("typeOfdispo", typeOfdispo))
    }
  
    // @LINE:488
    def getAllInsuranceAssignedList(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/insuranceAssignedList")
    }
  
    // @LINE:509
    def gettotalPremiumForAddOn(addOnPremiumValuePer:Double, odPremiumVaue:Double, commercialDiscPerceValue:Double, idvVal:Double, thirdPartPremiumValue:Double): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/gettotalPremiumOdAddOn/" + implicitly[PathBindable[Double]].unbind("addOnPremiumValuePer", addOnPremiumValuePer) + "/" + implicitly[PathBindable[Double]].unbind("odPremiumVaue", odPremiumVaue) + "/" + implicitly[PathBindable[Double]].unbind("commercialDiscPerceValue", commercialDiscPerceValue) + "/" + implicitly[PathBindable[Double]].unbind("idvVal", idvVal) + "/" + implicitly[PathBindable[Double]].unbind("thirdPartPremiumValue", thirdPartPremiumValue))
    }
  
    // @LINE:505
    def getODPercentage(cubicCap:String, vehAge:String, zoneid:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/getODPercentage/" + implicitly[PathBindable[String]].unbind("cubicCap", dynamicString(cubicCap)) + "/" + implicitly[PathBindable[String]].unbind("vehAge", dynamicString(vehAge)) + "/" + implicitly[PathBindable[String]].unbind("zoneid", dynamicString(zoneid)))
    }
  
    // @LINE:487
    def postCommonCallDispositionForm(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "CRE/getDispositionPageOfTab")
    }
  
    // @LINE:498
    def upload_Excel_Sheet_insurance(uploadId:String, uploadType:String, resultData:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/uploadExcelSheetInsurance/" + implicitly[PathBindable[String]].unbind("uploadId", dynamicString(uploadId)) + "/" + implicitly[PathBindable[String]].unbind("uploadType", dynamicString(uploadType)) + "/" + implicitly[PathBindable[String]].unbind("resultData", dynamicString(resultData)))
    }
  
    // @LINE:501
    def postAssignCallInsurance(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "CREManager/assignCallInsurance")
    }
  
    // @LINE:490
    def getContactedDispoFormData(typeOfdispo:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "insurance/contactedDispoInteractions/" + implicitly[PathBindable[Long]].unbind("typeOfdispo", typeOfdispo))
    }
  
    // @LINE:508
    def gettotalPremiumAndDiscValue(odPremiumVaue:Double, commercialDiscPerceValue:Double, thirdPartPremiumValue:Double): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/getDiscValueVyODPremium/" + implicitly[PathBindable[Double]].unbind("odPremiumVaue", odPremiumVaue) + "/" + implicitly[PathBindable[Double]].unbind("commercialDiscPerceValue", commercialDiscPerceValue) + "/" + implicitly[PathBindable[Double]].unbind("thirdPartPremiumValue", thirdPartPremiumValue))
    }
  
  }

  // @LINE:607
  class ReverseServiceBookController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:618
    def driverListScheduleByWorkshopId(workshopId:Long, scheduleDate:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "driverListSchedule/" + implicitly[PathBindable[Long]].unbind("workshopId", workshopId) + "/" + implicitly[PathBindable[String]].unbind("scheduleDate", dynamicString(scheduleDate)))
    }
  
    // @LINE:647
    def downloadServiceBooking(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "CREManager/downloadBooking")
    }
  
    // @LINE:615
    def reviewScheduleBooking(servicebookedId:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/reviewResheduleBooking/" + implicitly[PathBindable[Long]].unbind("servicebookedId", servicebookedId))
    }
  
    // @LINE:609
    def getServiceAdvisorListByWorkshop(workshopid:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "serviceAdvisorListByWorkshop/" + implicitly[PathBindable[Long]].unbind("workshopid", workshopid))
    }
  
    // @LINE:613
    def cancelBookingOrPickup(servicebookedId:Long, cancelId:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/cancelBooking/" + implicitly[PathBindable[Long]].unbind("servicebookedId", servicebookedId) + "/" + implicitly[PathBindable[Long]].unbind("cancelId", cancelId))
    }
  
    // @LINE:610
    def getsearchServiceBoookedList(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "searchServiceBookedList")
    }
  
    // @LINE:607
    def getReviewBookingPage(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/getReviewBookingPage")
    }
  
    // @LINE:616
    def postRescheduleBooking(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "CREManager/postRescheduleBooking")
    }
  
  }

  // @LINE:294
  class ReverseAllCallInteractionController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:299
    def downloadCallHistoryReport(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "CREManager/downloadCallHistoryReport")
    }
  
    // @LINE:298
    def getAllCallInteractions(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "CREManager/getAllCallHistoryData")
    }
  
    // @LINE:294
    def viewAllCallIntearctions(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/viewCallInteractions")
    }
  
  }

  // @LINE:71
  class ReverseSearchControllerMR(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:71
    def getPSFassignedInteractionTableDataMR(CREIds:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/PSFassignedInteractionTableDataMR/" + implicitly[PathBindable[String]].unbind("CREIds", dynamicString(CREIds)))
    }
  
    // @LINE:442
    def missedCallInteractionDataMR(CREIds:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "missedCallsServerDataTableMR/" + implicitly[PathBindable[String]].unbind("CREIds", dynamicString(CREIds)))
    }
  
    // @LINE:83
    def getInsurancenonContactsServerDataTableMR(CREIds:String, buckettype:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/InsurancenonContactsServerDataTableMR/" + implicitly[PathBindable[String]].unbind("CREIds", dynamicString(CREIds)) + "/" + implicitly[PathBindable[Long]].unbind("buckettype", buckettype))
    }
  
    // @LINE:446
    def getMediaFileMR(callId:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/downloadMediaFile/" + implicitly[PathBindable[Long]].unbind("callId", callId))
    }
  
    // @LINE:438
    def serviceBookedInteractionDataMR(CREIds:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "serviceBookedServerDataTableMR/" + implicitly[PathBindable[String]].unbind("CREIds", dynamicString(CREIds)))
    }
  
    // @LINE:72
    def getPSFfollowUpCallLogTableDataMR(CREIds:String, buckettype:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/PSFfollowUpCallLogTableDataMR/" + implicitly[PathBindable[String]].unbind("CREIds", dynamicString(CREIds)) + "/" + implicitly[PathBindable[Long]].unbind("buckettype", buckettype))
    }
  
    // @LINE:75
    def getPSFnonContactsServerDataTableMR(CREIds:String, buckettype:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/PSFnonContactsServerDataTableMR/" + implicitly[PathBindable[String]].unbind("CREIds", dynamicString(CREIds)) + "/" + implicitly[PathBindable[Long]].unbind("buckettype", buckettype))
    }
  
    // @LINE:443
    def incomingCallInteractionDataMR(CREIds:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "incomingCallsServerDataTableMR/" + implicitly[PathBindable[String]].unbind("CREIds", dynamicString(CREIds)))
    }
  
    // @LINE:437
    def followUpRequiredInteractionDataMR(CREIds:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "followUpCallLogTableDataMR/" + implicitly[PathBindable[String]].unbind("CREIds", dynamicString(CREIds)))
    }
  
    // @LINE:452
    def getDashboardCount(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/dashboardCounts")
    }
  
    // @LINE:439
    def serviceNotRequiredInteractionDataMR(CREIds:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "serviceNotRequiredServerDataTableMR/" + implicitly[PathBindable[String]].unbind("CREIds", dynamicString(CREIds)))
    }
  
    // @LINE:79
    def getInsuranceassignedInteractionTableDataMR(CREIds:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/InsuranceassignedInteractionTableDataMR/" + implicitly[PathBindable[String]].unbind("CREIds", dynamicString(CREIds)))
    }
  
    // @LINE:436
    def assignedInteractionDataMR(CREIds:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "assignedInteractionTableDataMR/" + implicitly[PathBindable[String]].unbind("CREIds", dynamicString(CREIds)))
    }
  
    // @LINE:444
    def outgoingCallsServerDataTableMR(CREIds:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "outgoingCallsServerDataTableMR/" + implicitly[PathBindable[String]].unbind("CREIds", dynamicString(CREIds)))
    }
  
    // @LINE:80
    def getInsurancefollowUpCallLogTableDataMR(CREIds:String, buckettype:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/InsurancefollowUpCallLogTableDataMR/" + implicitly[PathBindable[String]].unbind("CREIds", dynamicString(CREIds)) + "/" + implicitly[PathBindable[Long]].unbind("buckettype", buckettype))
    }
  
    // @LINE:441
    def droppedCallInteractionDataMR(CREIds:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "droppedCallsServerDataTableMR/" + implicitly[PathBindable[String]].unbind("CREIds", dynamicString(CREIds)))
    }
  
    // @LINE:440
    def nonContactsInteractionDataMR(CREIds:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "nonContactsServerDataTableMR/" + implicitly[PathBindable[String]].unbind("CREIds", dynamicString(CREIds)))
    }
  
  }

  // @LINE:468
  class ReverseAutoSelectionSAController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:470
    def ajaxupdateSaChange(workshopId:Long, date:String, preSaDetails:String, newSaDetails:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/callDispositionPage/" + implicitly[PathBindable[Long]].unbind("workshopId", workshopId) + "/" + implicitly[PathBindable[String]].unbind("date", dynamicString(date)) + "/" + implicitly[PathBindable[String]].unbind("preSaDetails", dynamicString(preSaDetails)) + "/" + implicitly[PathBindable[String]].unbind("newSaDetails", dynamicString(newSaDetails)))
    }
  
    // @LINE:469
    def ajaxAutoSASelectionList(saDetails:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/callDispositionPage/" + implicitly[PathBindable[String]].unbind("saDetails", dynamicString(saDetails)))
    }
  
    // @LINE:468
    def ajaxAutoSASelection(workshopId:Long, date:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/callDispositionPage/" + implicitly[PathBindable[Long]].unbind("workshopId", workshopId) + "/" + implicitly[PathBindable[String]].unbind("date", dynamicString(date)))
    }
  
  }

  // @LINE:7
  class ReverseApplication(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:7
    def landingPage(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix)
    }
  
    // @LINE:16
    def formIndex(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "form/index.html")
    }
  
    // @LINE:9
    def login(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "login")
    }
  
  }

  // @LINE:567
  class ReverseChangeAssignmentController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:580
    def assignListToUserSelected(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "CREManager/assignedCallsListToUser")
    }
  
    // @LINE:570
    def changeAssignedCallsToAgents(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "CREManager/assignCallToAgents")
    }
  
    // @LINE:567
    def changeassignedCalls(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/changeAssignedCalls")
    }
  
    // @LINE:568
    def postchangeassignedCalls(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "CREManager/changeAssignedCalls")
    }
  
    // @LINE:578
    def assignmentFilterList(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/assignmentFilterPage")
    }
  
    // @LINE:579
    def getAssignmentFilterListAjax(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "CREManager/assignedCallsList")
    }
  
  }

  // @LINE:524
  class ReverseInsuranceHistoryController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:524
    def uploadHistoryViewPage(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "insuranceHistoryUpload")
    }
  
    // @LINE:525
    def uploadExcelDataHistory(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "insuranceHistoryUpload")
    }
  
  }

  // @LINE:535
  class ReverseProcessUploadedFiles(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:535
    def process(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "processFile")
    }
  
    // @LINE:540
    def processFile(id:String): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "wyzprocessFile/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)))
    }
  
  }

  // @LINE:181
  class ReverseSearchController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:181
    def startSyncOfReports(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "service/startSyncReports")
    }
  
    // @LINE:453
    def getDashboardCountCRE(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/dashboardCounts")
    }
  
    // @LINE:378
    def assignedInteractionData(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "assignedInteractionTableData")
    }
  
    // @LINE:248
    def uploadHistoryReport(uploadId:String, uploadType:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/uploadDataReport/" + implicitly[PathBindable[String]].unbind("uploadId", dynamicString(uploadId)) + "/" + implicitly[PathBindable[String]].unbind("uploadType", dynamicString(uploadType)))
    }
  
    // @LINE:380
    def serviceBookedInteractionData(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "serviceBookedServerDataTable")
    }
  
    // @LINE:383
    def droppedCallInteractionData(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "droppedCallsServerDataTable")
    }
  
    // @LINE:382
    def nonContactsInteractionData(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "nonContactsServerDataTable")
    }
  
    // @LINE:214
    def searchCustomer(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "searchByCustomerv2")
    }
  
    // @LINE:249
    def uploadHistoryViewData(typeIds:String, fromDate:String, toDate:String, uploadReportId:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "uploadReport/" + implicitly[PathBindable[String]].unbind("typeIds", dynamicString(typeIds)) + "/" + implicitly[PathBindable[String]].unbind("fromDate", dynamicString(fromDate)) + "/" + implicitly[PathBindable[String]].unbind("toDate", dynamicString(toDate)) + "/" + implicitly[PathBindable[String]].unbind("uploadReportId", dynamicString(uploadReportId)))
    }
  
    // @LINE:381
    def serviceNotRequiredInteractionData(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "serviceNotRequiredServerDataTable")
    }
  
    // @LINE:379
    def followUpRequiredInteractionData(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "followUpCallLogTableData")
    }
  
  }

  // @LINE:403
  class ReverseCampaignController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:573
    def getCampaignNamesByUpload(uploadType:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "getCampaignList/" + implicitly[PathBindable[String]].unbind("uploadType", dynamicString(uploadType)))
    }
  
    // @LINE:404
    def postCampaign(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "postCampaign")
    }
  
    // @LINE:403
    def addCampaign(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "addcampaign")
    }
  
    // @LINE:518
    def addCampaignInsurance(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/addInsuranceCampaign")
    }
  
  }

  // @LINE:105
  class ReverseReportsController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:126
    def getAllAjaxRequestForCRE(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/ajax/getAjaxDataCompleteData")
    }
  
    // @LINE:133
    def getBookedListByTimeForCRE(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/ajax/getBookedListByTime")
    }
  
    // @LINE:195
    def getBookedListByTime(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "ajax/getBookedListByTime")
    }
  
    // @LINE:193
    def getScheduledCallsCountCREMan(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "ajax/getTtlSchCallsCREManager")
    }
  
    // @LINE:131
    def getServiceBookedPercentageForCRE(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/ajax/getPercServiceBookedForsCRE")
    }
  
    // @LINE:128
    def getScheduledCallsCountOfCRE(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/ajax/getTtlSchCallsCRE")
    }
  
    // @LINE:132
    def getCallTypePieForCRE(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/ajax/getCallTypePieForCRE")
    }
  
    // @LINE:130
    def getServiceBookedForCRE(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/ajax/getTtlServiceBookedForCRE")
    }
  
    // @LINE:129
    def getScheduledCallsPendingCountForCRE(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/ajax/getTtlSchCallsPendingCRE")
    }
  
    // @LINE:197
    def getCallTypePie(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "ajax/getCallTypePie")
    }
  
    // @LINE:105
    def viewReportCRE(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/viewReport")
    }
  
  }

  // @LINE:176
  class ReverseUserAuthenticator(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:179
    def updateUserAuthentication(phoneIMEINo:String, registrationId:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "updateUserAuthentication" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("phoneIMEINo", phoneIMEINo)), Some(implicitly[QueryStringBindable[String]].unbind("registrationId", registrationId)))))
    }
  
    // @LINE:176
    def authenticateUser(phoneNumber:String, phoneIMEINo:String, registrationId:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "users/authenticate/" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("phoneNumber", phoneNumber)), Some(implicitly[QueryStringBindable[String]].unbind("phoneIMEINo", phoneIMEINo)), Some(implicitly[QueryStringBindable[String]].unbind("registrationId", registrationId)))))
    }
  
  }

  // @LINE:261
  class ReverseFirebaseSyncController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:645
    def startSyncOperationInsuranceAgentHistory(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/startSyncInsuranceHistory")
    }
  
    // @LINE:271
    def stopSyncOperationPSFCallServiceAgent(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/stopSyncPSFCall")
    }
  
    // @LINE:644
    def startSyncOperationInsuranceAgent(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/startSyncInsuranceAgent")
    }
  
    // @LINE:273
    def startServiceAdvisorPSFHistorySync(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/startSyncPSFHistory")
    }
  
    // @LINE:265
    def stopSyncOperationServiceAgent(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/stopSyncServiceAgent")
    }
  
    // @LINE:267
    def stopServiceAdvisorSummaryDetailSync(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/stopSyncSummarySync")
    }
  
    // @LINE:261
    def startSyncOperationServiceAgent(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/startSyncServiceAgent")
    }
  
    // @LINE:262
    def startServiceAdvisorHistorySync(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/startSyncHistorySync")
    }
  
    // @LINE:389
    def startSyncOperationDriverPickupDropList(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "driverData")
    }
  
    // @LINE:270
    def startSyncOperationPSFCallServiceAgent(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/startSyncPSFCall")
    }
  
    // @LINE:263
    def startServiceAdvisorSummaryDetailSync(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/startSyncSummarySync")
    }
  
    // @LINE:266
    def stopServiceAdvisorHistorySync(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CREManager/stopSyncHistorySync")
    }
  
  }

  // @LINE:44
  class ReverseScheduledCallController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:44
    def startSyncForTesting(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "deleteRecordFireBase")
    }
  
    // @LINE:118
    def getCallDispositionTabPAgeCRE(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "CRE/getDispositionPageOfTab")
    }
  
  }


}
