
// @GENERATOR:play-routes-compiler
// @SOURCE:D:/CRMNEW/28th Dec/crm/conf/routes
// @DATE:Thu Dec 28 12:24:22 IST 2017

package org.pac4j.play;

import router.RoutesPrefix;

public class routes {
  
  public static final org.pac4j.play.ReverseCallbackController CallbackController = new org.pac4j.play.ReverseCallbackController(RoutesPrefix.byNamePrefix());

  public static class javascript {
    
    public static final org.pac4j.play.javascript.ReverseCallbackController CallbackController = new org.pac4j.play.javascript.ReverseCallbackController(RoutesPrefix.byNamePrefix());
  }

}
