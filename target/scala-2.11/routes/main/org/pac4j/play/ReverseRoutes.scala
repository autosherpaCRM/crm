
// @GENERATOR:play-routes-compiler
// @SOURCE:D:/CRMNEW/28th Dec/crm/conf/routes
// @DATE:Thu Dec 28 12:24:22 IST 2017

import play.api.mvc.{ QueryStringBindable, PathBindable, Call, JavascriptLiteral }
import play.core.routing.{ HandlerDef, ReverseRouteContext, queryString, dynamicString }


import _root_.controllers.Assets.Asset
import _root_.play.libs.F

// @LINE:13
package org.pac4j.play {

  // @LINE:13
  class ReverseCallbackController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:13
    def callback(): Call = {
    
      () match {
      
        // @LINE:13
        case ()  =>
          import ReverseRouteContext.empty
          Call("GET", _prefix + { _defaultPrefix } + "callback")
      
      }
    
    }
  
  }


}
