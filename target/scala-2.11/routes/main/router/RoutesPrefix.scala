
// @GENERATOR:play-routes-compiler
// @SOURCE:D:/CRMNEW/28th Dec/crm/conf/routes
// @DATE:Thu Dec 28 12:24:22 IST 2017


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
