
// @GENERATOR:play-routes-compiler
// @SOURCE:D:/CRMNEW/28th Dec/crm/conf/routes
// @DATE:Thu Dec 28 12:24:22 IST 2017

package router

import play.core.routing._
import play.core.routing.HandlerInvokerFactory._
import play.core.j._

import play.api.mvc._

import _root_.controllers.Assets.Asset
import _root_.play.libs.F

class Routes(
  override val errorHandler: play.api.http.HttpErrorHandler, 
  // @LINE:7
  Application_27: controllers.Application,
  // @LINE:10
  WyzUserController_11: controllers.WyzUserController,
  // @LINE:13
  CallbackController_30: javax.inject.Provider[org.pac4j.play.CallbackController],
  // @LINE:35
  DealerController_28: controllers.DealerController,
  // @LINE:44
  ScheduledCallController_1: controllers.ScheduledCallController,
  // @LINE:71
  SearchControllerMR_10: controllers.SearchControllerMR,
  // @LINE:88
  CallInfoController_26: controllers.CallInfoController,
  // @LINE:95
  CallInteractionController_12: controllers.CallInteractionController,
  // @LINE:105
  ReportsController_19: controllers.ReportsController,
  // @LINE:149
  PSFController_14: controllers.PSFController,
  // @LINE:168
  Assets_23: controllers.Assets,
  // @LINE:176
  UserAuthenticator_16: controllers.UserAuthenticator,
  // @LINE:181
  SearchController_0: controllers.SearchController,
  // @LINE:186
  CustomerScheduledController_15: controllers.CustomerScheduledController,
  // @LINE:215
  SMSTemplateController_2: controllers.SMSTemplateController,
  // @LINE:261
  FirebaseSyncController_6: controllers.FirebaseSyncController,
  // @LINE:294
  AllCallInteractionController_4: controllers.AllCallInteractionController,
  // @LINE:387
  UploadExcelController_24: controllers.UploadExcelController,
  // @LINE:403
  CampaignController_8: controllers.CampaignController,
  // @LINE:464
  ServiceAdvisorController_20: controllers.ServiceAdvisorController,
  // @LINE:468
  AutoSelectionSAController_29: controllers.AutoSelectionSAController,
  // @LINE:474
  AutoSelInsuranceAgentController_5: controllers.AutoSelInsuranceAgentController,
  // @LINE:486
  InsuranceController_25: controllers.InsuranceController,
  // @LINE:524
  InsuranceHistoryController_21: controllers.InsuranceHistoryController,
  // @LINE:532
  FileUploadController_7: controllers.FileUploadController,
  // @LINE:535
  ProcessUploadedFiles_31: controllers.ProcessUploadedFiles,
  // @LINE:549
  SMSandEmailController_18: controllers.SMSandEmailController,
  // @LINE:567
  ChangeAssignmentController_22: controllers.ChangeAssignmentController,
  // @LINE:589
  SuperAdminController_17: controllers.SuperAdminController,
  // @LINE:607
  ServiceBookController_32: controllers.ServiceBookController,
  // @LINE:621
  AudioStreamController_3: controllers.AudioStreamController,
  // @LINE:625
  AllAudioConverter_9: controllers.AllAudioConverter,
  // @LINE:630
  CallRecordingHistoryController_13: controllers.CallRecordingHistoryController,
  val prefix: String
) extends GeneratedRouter {

   @javax.inject.Inject()
   def this(errorHandler: play.api.http.HttpErrorHandler,
    // @LINE:7
    Application_27: controllers.Application,
    // @LINE:10
    WyzUserController_11: controllers.WyzUserController,
    // @LINE:13
    CallbackController_30: javax.inject.Provider[org.pac4j.play.CallbackController],
    // @LINE:35
    DealerController_28: controllers.DealerController,
    // @LINE:44
    ScheduledCallController_1: controllers.ScheduledCallController,
    // @LINE:71
    SearchControllerMR_10: controllers.SearchControllerMR,
    // @LINE:88
    CallInfoController_26: controllers.CallInfoController,
    // @LINE:95
    CallInteractionController_12: controllers.CallInteractionController,
    // @LINE:105
    ReportsController_19: controllers.ReportsController,
    // @LINE:149
    PSFController_14: controllers.PSFController,
    // @LINE:168
    Assets_23: controllers.Assets,
    // @LINE:176
    UserAuthenticator_16: controllers.UserAuthenticator,
    // @LINE:181
    SearchController_0: controllers.SearchController,
    // @LINE:186
    CustomerScheduledController_15: controllers.CustomerScheduledController,
    // @LINE:215
    SMSTemplateController_2: controllers.SMSTemplateController,
    // @LINE:261
    FirebaseSyncController_6: controllers.FirebaseSyncController,
    // @LINE:294
    AllCallInteractionController_4: controllers.AllCallInteractionController,
    // @LINE:387
    UploadExcelController_24: controllers.UploadExcelController,
    // @LINE:403
    CampaignController_8: controllers.CampaignController,
    // @LINE:464
    ServiceAdvisorController_20: controllers.ServiceAdvisorController,
    // @LINE:468
    AutoSelectionSAController_29: controllers.AutoSelectionSAController,
    // @LINE:474
    AutoSelInsuranceAgentController_5: controllers.AutoSelInsuranceAgentController,
    // @LINE:486
    InsuranceController_25: controllers.InsuranceController,
    // @LINE:524
    InsuranceHistoryController_21: controllers.InsuranceHistoryController,
    // @LINE:532
    FileUploadController_7: controllers.FileUploadController,
    // @LINE:535
    ProcessUploadedFiles_31: controllers.ProcessUploadedFiles,
    // @LINE:549
    SMSandEmailController_18: controllers.SMSandEmailController,
    // @LINE:567
    ChangeAssignmentController_22: controllers.ChangeAssignmentController,
    // @LINE:589
    SuperAdminController_17: controllers.SuperAdminController,
    // @LINE:607
    ServiceBookController_32: controllers.ServiceBookController,
    // @LINE:621
    AudioStreamController_3: controllers.AudioStreamController,
    // @LINE:625
    AllAudioConverter_9: controllers.AllAudioConverter,
    // @LINE:630
    CallRecordingHistoryController_13: controllers.CallRecordingHistoryController
  ) = this(errorHandler, Application_27, WyzUserController_11, CallbackController_30, DealerController_28, ScheduledCallController_1, SearchControllerMR_10, CallInfoController_26, CallInteractionController_12, ReportsController_19, PSFController_14, Assets_23, UserAuthenticator_16, SearchController_0, CustomerScheduledController_15, SMSTemplateController_2, FirebaseSyncController_6, AllCallInteractionController_4, UploadExcelController_24, CampaignController_8, ServiceAdvisorController_20, AutoSelectionSAController_29, AutoSelInsuranceAgentController_5, InsuranceController_25, InsuranceHistoryController_21, FileUploadController_7, ProcessUploadedFiles_31, SMSandEmailController_18, ChangeAssignmentController_22, SuperAdminController_17, ServiceBookController_32, AudioStreamController_3, AllAudioConverter_9, CallRecordingHistoryController_13, "/")

  import ReverseRouteContext.empty

  def withPrefix(prefix: String): Routes = {
    router.RoutesPrefix.setPrefix(prefix)
    new Routes(errorHandler, Application_27, WyzUserController_11, CallbackController_30, DealerController_28, ScheduledCallController_1, SearchControllerMR_10, CallInfoController_26, CallInteractionController_12, ReportsController_19, PSFController_14, Assets_23, UserAuthenticator_16, SearchController_0, CustomerScheduledController_15, SMSTemplateController_2, FirebaseSyncController_6, AllCallInteractionController_4, UploadExcelController_24, CampaignController_8, ServiceAdvisorController_20, AutoSelectionSAController_29, AutoSelInsuranceAgentController_5, InsuranceController_25, InsuranceHistoryController_21, FileUploadController_7, ProcessUploadedFiles_31, SMSandEmailController_18, ChangeAssignmentController_22, SuperAdminController_17, ServiceBookController_32, AudioStreamController_3, AllAudioConverter_9, CallRecordingHistoryController_13, prefix)
  }

  private[this] val defaultPrefix: String = {
    if (this.prefix.endsWith("/")) "" else "/"
  }

  def documentation = List(
    ("""GET""", this.prefix, """controllers.Application.landingPage()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """login""", """controllers.Application.login"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """changePassword""", """controllers.WyzUserController.changePassword"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """logoutUser""", """controllers.WyzUserController.logout"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """callback""", """@org.pac4j.play.CallbackController@.callback()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """callback""", """@org.pac4j.play.CallbackController@.callback()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """form/index.html""", """controllers.Application.formIndex()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """logoutParticularUser/""" + "$" + """userIs<[^/]+>""", """controllers.WyzUserController.logoutParticularUser(userIs:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """superAdmin/index""", """controllers.WyzUserController.index"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """superAdmin/addUser""", """controllers.WyzUserController.addapp"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """superAdmin/addUser""", """controllers.WyzUserController.addApplicationUser"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """superAdmin/userList""", """controllers.WyzUserController.userInformation"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """superAdmin/userList""", """controllers.WyzUserController.userInformation"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """superAdmin/user/edit/""" + "$" + """id<[^/]+>""", """controllers.WyzUserController.geteditUser(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """superAdmin/user/edit/""" + "$" + """id<[^/]+>""", """controllers.WyzUserController.postEditUser(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """superAdmin/user/delete/""" + "$" + """id<[^/]+>""", """controllers.WyzUserController.deleteUserData(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """superAdmin/addDealer""", """controllers.DealerController.adddel"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """superAdmin/addDealer""", """controllers.DealerController.adddealerForm"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """superAdmin/dealerList""", """controllers.DealerController.dealerInformation"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """superAdmin/dealer/delete/""" + "$" + """did<[^/]+>""", """controllers.DealerController.deleteDealerData(did:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """superAdmin/dealer/edit/""" + "$" + """did<[^/]+>""", """controllers.DealerController.geteditDealer(did:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """superAdmin/edit/""" + "$" + """did<[^/]+>""", """controllers.DealerController.postEditDealer(did:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """deleteRecordFireBase""", """controllers.ScheduledCallController.startSyncForTesting()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """superAdmin/changepasswordsuperAdmin""", """controllers.WyzUserController.changepasswordsuperAdmin"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """superAdmin/passwordchangesuperAdmin""", """controllers.WyzUserController.changepasswordeditingsuperAdmin()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """superAdmin/loginformation""", """controllers.WyzUserController.loginfo()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager""", """controllers.WyzUserController.indexPageCREManager()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/viewReport""", """controllers.WyzUserController.viewReportCREManager"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/changepassword""", """controllers.WyzUserController.changepassword"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/passwordchange""", """controllers.WyzUserController.changepasswordediting()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/PSFassignedInteractionTableDataMR/""" + "$" + """CREIds<[^/]+>""", """controllers.SearchControllerMR.getPSFassignedInteractionTableDataMR(CREIds:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/PSFfollowUpCallLogTableDataMR/""" + "$" + """CREIds<[^/]+>/""" + "$" + """buckettype<[^/]+>""", """controllers.SearchControllerMR.getPSFfollowUpCallLogTableDataMR(CREIds:String, buckettype:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/PSFnonContactsServerDataTableMR/""" + "$" + """CREIds<[^/]+>/""" + "$" + """buckettype<[^/]+>""", """controllers.SearchControllerMR.getPSFnonContactsServerDataTableMR(CREIds:String, buckettype:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/InsuranceassignedInteractionTableDataMR/""" + "$" + """CREIds<[^/]+>""", """controllers.SearchControllerMR.getInsuranceassignedInteractionTableDataMR(CREIds:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/InsurancefollowUpCallLogTableDataMR/""" + "$" + """CREIds<[^/]+>/""" + "$" + """buckettype<[^/]+>""", """controllers.SearchControllerMR.getInsurancefollowUpCallLogTableDataMR(CREIds:String, buckettype:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/InsurancenonContactsServerDataTableMR/""" + "$" + """CREIds<[^/]+>/""" + "$" + """buckettype<[^/]+>""", """controllers.SearchControllerMR.getInsurancenonContactsServerDataTableMR(CREIds:String, buckettype:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/CallDispositionBucketForCREMan""", """controllers.CallInfoController.getCallDispositionBucketForCREMan()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/PSFCallLogPageCREManager""", """controllers.CallInfoController.getPSFCallLogViewPage()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/InsuranceCallLogPageCREManager""", """controllers.CallInfoController.getInsuranceCallLogViewPage()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/getAssignedCallsOfCREManager/""" + "$" + """selectedAgent<[^/]+>""", """controllers.CallInteractionController.getAssignedCallsOfCREManager(selectedAgent:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/getFollowUpRequiredDataCREMan/""" + "$" + """selectedAgent<[^/]+>""", """controllers.CallInteractionController.getFollowUpRequiredDataCREMan(selectedAgent:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/getServiceBookedDataCREMan/""" + "$" + """selectedAgent<[^/]+>""", """controllers.CallInteractionController.getServiceBookedDataCREMan(selectedAgent:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/getServiceNotRequiredDataCREMan/""" + "$" + """selectedAgent<[^/]+>""", """controllers.CallInteractionController.getServiceNotRequiredDataCREMan(selectedAgent:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/getNonContactsDataCREMan/""" + "$" + """selectedAgent<[^/]+>""", """controllers.CallInteractionController.getNonContactsDataCREMan(selectedAgent:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/getDroppedCallsDataCREMan/""" + "$" + """selectedAgent<[^/]+>""", """controllers.CallInteractionController.getDroppedCallsDataCREMan(selectedAgent:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE""", """controllers.WyzUserController.indexPageCRE()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/viewReport""", """controllers.ReportsController.viewReportCRE()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/toeditcallLog/""" + "$" + """id<[^/]+>""", """controllers.CallInfoController.getcallLogEditForCRE(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/toeditcallLog/""" + "$" + """id<[^/]+>""", """controllers.CallInfoController.postcallLogeditForCRE(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/changepasswordcre""", """controllers.WyzUserController.changepasswordCRE()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/passwordchangecre""", """controllers.WyzUserController.changepasswordeditingCRE()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/getDispositionPageOfTab""", """controllers.ScheduledCallController.getCallDispositionTabPAgeCRE()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/ajax/initiateCall/""" + "$" + """phonenumber<[^/]+>/""" + "$" + """uniqueid<[^/]+>/""" + "$" + """customerId<[^/]+>""", """controllers.CallInfoController.startInitiatingOfCall(phonenumber:Long, uniqueid:Long, customerId:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/ajax/getAjaxDataCompleteData""", """controllers.ReportsController.getAllAjaxRequestForCRE()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/ajax/getTtlSchCallsCRE""", """controllers.ReportsController.getScheduledCallsCountOfCRE()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/ajax/getTtlSchCallsPendingCRE""", """controllers.ReportsController.getScheduledCallsPendingCountForCRE()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/ajax/getTtlServiceBookedForCRE""", """controllers.ReportsController.getServiceBookedForCRE()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/ajax/getPercServiceBookedForsCRE""", """controllers.ReportsController.getServiceBookedPercentageForCRE()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/ajax/getCallTypePieForCRE""", """controllers.ReportsController.getCallTypePieForCRE()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/ajax/getBookedListByTime""", """controllers.ReportsController.getBookedListByTimeForCRE()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/getFollowUpRequiredData/""" + "$" + """selectAgent<[^/]+>""", """controllers.CallInteractionController.getFollowUpRequiredData(selectAgent:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/getServiceBookedData/""" + "$" + """selectAgent<[^/]+>""", """controllers.CallInteractionController.getServiceBookedData(selectAgent:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/getNonContactsData/""" + "$" + """selectAgent<[^/]+>""", """controllers.CallInteractionController.getNonContactsData(selectAgent:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/getDroppedCallsData/""" + "$" + """selectAgent<[^/]+>""", """controllers.CallInteractionController.getDroppedCallsData(selectAgent:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/getServiceNotRequiredData/""" + "$" + """selectAgent<[^/]+>""", """controllers.CallInteractionController.getServiceNotRequiredData(selectAgent:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/listTodaysFollowUp""", """controllers.CallInfoController.getFollowUpTableDataOfCRE()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/ajax/getFollowUpNotificationBeforeTime""", """controllers.CallInfoController.getFollowUpNotifyBeforeTime()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/ajax/getFollowUpRemainderOfMissedSchedules""", """controllers.CallInfoController.getFollowUpRemainderOfMissedSchedules()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/ajax/getFollowUpNotificationOfToday""", """controllers.CallInfoController.getFollowUpNotificationToday()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/ajax/getPSFFollowupNotificationOfToday""", """controllers.PSFController.getPSFFollowUpNotificationToday()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/deleteCall/""" + "$" + """id<[^/]+>""", """controllers.CallInfoController.deleteCallLog(dealercode:String, id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/deleteSchCall/""" + "$" + """id<[^/]+>""", """controllers.CallInfoController.deleteSchCalllog(dealercode:String, id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """SalesManager""", """controllers.WyzUserController.indexPageSalesManager"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """SalesManager/viewReport""", """controllers.WyzUserController.viewReport()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """assets/""" + "$" + """file<.+>""", """controllers.Assets.at(path:String = "/public", file:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """users/authenticate/""", """controllers.UserAuthenticator.authenticateUser(phoneNumber:String, phoneIMEINo:String, registrationId:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """callInfo/startSync""", """controllers.CallInfoController.startSyncOperation()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """callInfo/stopSync""", """controllers.CallInfoController.stopSyncOperation()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """updateUserAuthentication""", """controllers.UserAuthenticator.updateUserAuthentication(phoneIMEINo:String, registrationId:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """service/startSyncReports""", """controllers.SearchController.startSyncOfReports()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """delete/allFilesFromTmp""", """controllers.CallInfoController.todeleteFilesFromDirectory()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """customers/readCustomersFromCSV""", """controllers.CustomerScheduledController.readCustomersFromCSV"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/addcustomer""", """controllers.CustomerScheduledController.addcustomerInfo"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """ajax/getTtlSchCallsCREManager""", """controllers.ReportsController.getScheduledCallsCountCREMan"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """ajax/getBookedListByTime""", """controllers.ReportsController.getBookedListByTime"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """ajax/getCallTypePie""", """controllers.ReportsController.getCallTypePie"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """getCustomerByInteractionId""", """controllers.CallInteractionController.getCustomerByInteractionId()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/UploadAllCalls""", """controllers.CallInteractionController.getAssignedInteraction()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/UploadAllCalls""", """controllers.CallInteractionController.toUploadInteractionsByCREManager()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/assignCalls/""" + "$" + """selectAgent<[^/]+>/""" + "$" + """fromData<[^/]+>/""" + "$" + """toDate<[^/]+>""", """controllers.CallInteractionController.assigningCallsToCRE(selectAgent:String, fromData:String, toDate:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/getAssignedCalls/""" + "$" + """selectAgent<[^/]+>""", """controllers.CallInteractionController.getAssignedCallsOfUser(selectAgent:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """searchByCustomer""", """controllers.CallInteractionController.searchByCustomer()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """addCustomer""", """controllers.CallInteractionController.addCustomerCRE()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """searchByCustomerv2""", """controllers.SearchController.searchCustomer()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """checkIfSearchNameExists/""" + "$" + """searchnamevalue<[^/]+>""", """controllers.SMSTemplateController.checkIfSearchNameExists(searchnamevalue:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """postAddCustomer""", """controllers.CallInteractionController.postAddCustomer()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """getMediaFile/""" + "$" + """id<[^/]+>""", """controllers.CallInteractionController.getMediaFileCallInteractions(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/assignCall""", """controllers.CallInteractionController.getPageForAssigningCalls()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/assignCall""", """controllers.CallInteractionController.getSelectedAssignCalls()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/getAssignList""", """controllers.CallInteractionController.getAssignedList()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/changeAssignment""", """controllers.CallInteractionController.toChangeAssigment()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/getReAssignmentCalls/""" + "$" + """selectAgent<[^/]+>""", """controllers.CallInteractionController.getReAssignmentCalls(selectAgent:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/changeAssignment""", """controllers.CallInteractionController.reAssigningCallsofselectCRE()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/uploadExcelData""", """controllers.CallInteractionController.upload_file_Format_show()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/uploadExcelData""", """controllers.CallInteractionController.upload_file_Format_submit()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """uploadFormat/getFormElement/""" + "$" + """selected_part<[^/]+>""", """controllers.CallInteractionController.ajax_master_data_upload_fileFormat(selected_part:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/uploadExcelSheet/""" + "$" + """uploadId<[^/]+>/""" + "$" + """uploadType<[^/]+>/""" + "$" + """resultData<[^/]+>""", """controllers.CallInteractionController.upload_Excel_Sheet(uploadId:String, uploadType:String, resultData:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """uploadFormat/MapMasterFormat/""" + "$" + """selected_part<[^/]+>""", """controllers.CallInteractionController.get_required_fields(selected_part:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """master/excelUpload/""" + "$" + """selected_format<[^/]+>""", """controllers.CallInteractionController.ajax_transaction_data_upload(selected_format:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/downloadExcelFormat""", """controllers.CallInteractionController.getDownloadExcelFormat()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """getExcelColumns/""" + "$" + """upload_format<[^/]+>""", """controllers.CallInteractionController.getExcelColumnsOFUploadFormat(upload_format:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/uploadDataReport/""" + "$" + """uploadId<[^/]+>/""" + "$" + """uploadType<[^/]+>""", """controllers.SearchController.uploadHistoryReport(uploadId:String, uploadType:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """uploadReport/""" + "$" + """typeIds<[^/]+>/""" + "$" + """fromDate<[^/]+>/""" + "$" + """toDate<[^/]+>/""" + "$" + """uploadReportId<[^/]+>""", """controllers.SearchController.uploadHistoryViewData(typeIds:String, fromDate:String, toDate:String, uploadReportId:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """complaints""", """controllers.CallInteractionController.addComplaints()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """postComplaints""", """controllers.CallInteractionController.postComplaints()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/assignComplaints""", """controllers.CallInteractionController.assignComplaints()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/updateComplaints/""" + "$" + """id<[^/]+>/""" + "$" + """comments<[^/]+>/""" + "$" + """selected_value<[^/]+>""", """controllers.CallInteractionController.updateComplaints(id:Long, comments:String, selected_value:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """complaintsByManager""", """controllers.CallInteractionController.addComplaintsByManager()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """postComplaintsByManager""", """controllers.CallInteractionController.postComplaintsByManager()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/startSyncServiceAgent""", """controllers.FirebaseSyncController.startSyncOperationServiceAgent()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/startSyncHistorySync""", """controllers.FirebaseSyncController.startServiceAdvisorHistorySync()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/startSyncSummarySync""", """controllers.FirebaseSyncController.startServiceAdvisorSummaryDetailSync()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/stopSyncServiceAgent""", """controllers.FirebaseSyncController.stopSyncOperationServiceAgent()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/stopSyncHistorySync""", """controllers.FirebaseSyncController.stopServiceAdvisorHistorySync()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/stopSyncSummarySync""", """controllers.FirebaseSyncController.stopServiceAdvisorSummaryDetailSync()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/startSyncPSFCall""", """controllers.FirebaseSyncController.startSyncOperationPSFCallServiceAgent()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/stopSyncPSFCall""", """controllers.FirebaseSyncController.stopSyncOperationPSFCallServiceAgent()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/startSyncPSFHistory""", """controllers.FirebaseSyncController.startServiceAdvisorPSFHistorySync()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/addphone/""" + "$" + """phone_no<[^/]+>/""" + "$" + """customer_id<[^/]+>""", """controllers.CallInteractionController.ajaxAddPhoneNumber(phone_no:String, customer_id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/saveaddcustomerinfo""", """controllers.CallInteractionController.ajaxCallForAddcustomerinfo()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/saveaddcustomerchassisno/""" + "$" + """chassisNo<[^/]+>/""" + "$" + """customer_id<[^/]+>""", """controllers.CallInteractionController.ajaxAddChassisno(chassisNo:String, customer_id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/saveaddcustomervregistrationno/""" + "$" + """vehicalRegNo<[^/]+>/""" + "$" + """customer_id<[^/]+>""", """controllers.CallInteractionController.ajaxAddRegistrationno(vehicalRegNo:String, customer_id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/saveaddcustomerengineno/""" + "$" + """engineNo<[^/]+>/""" + "$" + """customer_id<[^/]+>""", """controllers.CallInteractionController.ajaxAddEngineno(engineNo:String, customer_id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/saveaddcustomermobno/""" + "$" + """custMobNo<[^/]+>/""" + "$" + """wyzUser_id<[^/]+>/""" + "$" + """customer_Id<[^/]+>""", """controllers.CallInteractionController.saveaddcustomermobno(custMobNo:String, wyzUser_id:Long, customer_Id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/saveaddcustomerEmail/""" + "$" + """custEmail<[^/]+>/""" + "$" + """wyzUser_id<[^/]+>/""" + "$" + """customer_Id<[^/]+>""", """controllers.CallInteractionController.saveaddcustomerEmail(custEmail:String, wyzUser_id:Long, customer_Id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/viewCallInteractions""", """controllers.AllCallInteractionController.viewAllCallIntearctions()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/getAllCallHistoryData""", """controllers.AllCallInteractionController.getAllCallInteractions()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/downloadCallHistoryReport""", """controllers.AllCallInteractionController.downloadCallHistoryReport()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/getWorkshopServices/""" + "$" + """workId<[^/]+>/""" + "$" + """userId<[^/]+>""", """controllers.CallInteractionController.getWorkShopServiceBooked(workId:Long, userId:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/getServiceAdvisorServices/""" + "$" + """serviceAdvisorId<[^/]+>/""" + "$" + """userId<[^/]+>""", """controllers.CallInteractionController.getSerAdvServiceBooked(serviceAdvisorId:Long, userId:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/getDriverServices/""" + "$" + """driverId<[^/]+>/""" + "$" + """userId<[^/]+>""", """controllers.CallInteractionController.getDriverServiceBooked(driverId:Long, userId:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/reAssignAgent/""" + "$" + """rowId<[^/]+>/""" + "$" + """wyzUserId<[^/]+>/""" + "$" + """post_id<[^/]+>""", """controllers.CallInteractionController.reAssignAgentUpdate(rowId:Long, wyzUserId:Long, post_id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/listWorkshops/""" + "$" + """selectedCity<[^/]+>""", """controllers.CallInteractionController.getListWorkshopByLocation(selectedCity:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/listWorkshopSummary/""" + "$" + """selectedWorkshop<[^/]+>/""" + "$" + """schDate<[^/]+>""", """controllers.CallInteractionController.getWorkshopSummaryDetails(selectedWorkshop:Long, schDate:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/searchvehRegNo/""" + "$" + """veh_number<[^/]+>""", """controllers.CallInteractionController.ajaxsearchVehicle(veh_number:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """complaintResolution""", """controllers.CallInteractionController.complaintsResolution()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/searchcomplaintNum/""" + "$" + """complaintNum<[^/]+>/""" + "$" + """veh_num<[^/]+>""", """controllers.CallInteractionController.ajaxcomplaintNumber(complaintNum:String, veh_num:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/searchcomplaintNumClosed/""" + "$" + """complaintNumClosed<[^/]+>/""" + "$" + """veh_numclosed<[^/]+>""", """controllers.CallInteractionController.ajaxcomplaintNumberClosed(complaintNumClosed:String, veh_numclosed:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/updateComplaintsResolution/""" + "$" + """complaintNum<[^/]+>/""" + "$" + """reasonFor<[^/]+>/""" + "$" + """complaintStatus<[^/]+>/""" + "$" + """customerStatus<[^/]+>/""" + "$" + """actionTaken<[^/]+>/""" + "$" + """resolutionBy<[^/]+>""", """controllers.CallInteractionController.updateComplaintsResolution(complaintNum:String, reasonFor:String, complaintStatus:String, customerStatus:String, actionTaken:String, resolutionBy:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/updateComplaintsResolutionByManager/""" + "$" + """complaintNum<[^/]+>/""" + "$" + """reasonFor<[^/]+>/""" + "$" + """complaintStatus<[^/]+>/""" + "$" + """customerStatus<[^/]+>/""" + "$" + """actionTaken<[^/]+>/""" + "$" + """resolutionBy<[^/]+>""", """controllers.CallInteractionController.updateComplaintsResolutionByManager(complaintNum:String, reasonFor:String, complaintStatus:String, customerStatus:String, actionTaken:String, resolutionBy:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/updateComplaintsResolutionClosed/""" + "$" + """complaintNumClosed<[^/]+>/""" + "$" + """reasonForClosed<[^/]+>/""" + "$" + """complaintStatusClosed<[^/]+>/""" + "$" + """customerStatusClosed<[^/]+>/""" + "$" + """actionTakenClosed<[^/]+>/""" + "$" + """resolutionByClosed<[^/]+>""", """controllers.CallInteractionController.updateComplaintsResolutionClosed(complaintNumClosed:String, reasonForClosed:String, complaintStatusClosed:String, customerStatusClosed:String, actionTakenClosed:String, resolutionByClosed:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/complaintAssignment/""" + "$" + """complaintNum<[^/]+>/""" + "$" + """veh_num<[^/]+>""", """controllers.CallInteractionController.complaintAssignment(complaintNum:String, veh_num:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/saveaddComplaintAssignModile/""" + "$" + """complaintNum<[^/]+>/""" + "$" + """city<[^/]+>/""" + "$" + """workshop<[^/]+>/""" + "$" + """functions<[^/]+>/""" + "$" + """ownership<[^/]+>/""" + "$" + """priority<[^/]+>/""" + "$" + """esclation1<[^/]+>/""" + "$" + """esclation2<[^/]+>""", """controllers.CallInteractionController.saveaddComplaintAssignModile(complaintNum:String, city:String, workshop:String, functions:String, ownership:String, priority:String, esclation1:String, esclation2:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/viewComplaints1""", """controllers.CallInteractionController.viewAllComplaints1()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/viewComplaints1""", """controllers.CallInteractionController.downloadExcelFile()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/getComplaintsDataByFilter/""" + "$" + """filterData<[^/]+>/""" + "$" + """varLoc<[^/]+>/""" + "$" + """varfunc<[^/]+>/""" + "$" + """varraisedDate<[^/]+>/""" + "$" + """varendDate<[^/]+>""", """controllers.CallInteractionController.getComplaintsDataByFilter(filterData:String, varLoc:String, varfunc:String, varraisedDate:String, varendDate:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/getComplaintHistoryAll/""" + "$" + """complaintNumber<[^/]+>/""" + "$" + """vehregnumber<[^/]+>""", """controllers.CallInteractionController.getComplaintHistoryAll(complaintNumber:String, vehregnumber:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/updateRangeOfUnavailabilty""", """controllers.CallInteractionController.updateRangeOfUnavailabiltyOfUsers()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/updateRangeOfUnavailabilty""", """controllers.CallInteractionController.postUpdateRangeOfUnavailabiltyOfUsers"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/todaysRoaster""", """controllers.CallInteractionController.getRoasterTable()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/todaysRoaster""", """controllers.CallInteractionController.toUpdateRoasterUnAvailablity()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/rosterOfUnavailabiltyByUser""", """controllers.CallInteractionController.rosterOfUnavailabiltyByUser()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/addRosterOfUserByAjax/""" + "$" + """selectAgent<[^/]+>/""" + "$" + """fromDate<[^/]+>/""" + "$" + """toDate<[^/]+>""", """controllers.CallInteractionController.addRosterOfUserByAjax(selectAgent:String, fromDate:String, toDate:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/loadRosterData/""" + "$" + """selectAgent<[^/]+>""", """controllers.CallInteractionController.getRosterDataByUser(selectAgent:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/updateRosterOfUserByAjaxData/""" + "$" + """selectAgent<[^/]+>/""" + "$" + """From_Date<[^/]+>/""" + "$" + """To_Date<[^/]+>""", """controllers.CallInteractionController.updateRosterOfUserByAjaxVal(selectAgent:String, From_Date:String, To_Date:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/deleteUnavialbility/""" + "$" + """id<[^/]+>""", """controllers.CallInteractionController.deleteUnavialbility(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/serviceDataOfCustomer/""" + "$" + """customerId<[^/]+>""", """controllers.CallInteractionController.getServiceDataOfCustomer(customerId:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/getCallHistoryOfvehicalId/""" + "$" + """vehicalId<[^/]+>""", """controllers.CallInteractionController.getCallHistoryOfvehicalId(vehicalId:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/listServiceAdvisors/""" + "$" + """workshopId<[^/]+>""", """controllers.CallInteractionController.getServiceAdvisorOfWorkshop(workshopId:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/listWorkshopsIfExisting/""" + "$" + """workshopId<[^/]+>""", """controllers.CallInteractionController.getWorkshopListName(workshopId:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/leadBasedOnLocation/""" + "$" + """userLocation<[^/]+>""", """controllers.CallInteractionController.getLeadByUserLocation(userLocation:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/leadBasedOnDepartment/""" + "$" + """userLocation<[^/]+>/""" + "$" + """departmentName<[^/]+>""", """controllers.CallInteractionController.getLeadTagByDepartment(userLocation:Long, departmentName:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/upselLeadIdTag/""" + "$" + """userLocation<[^/]+>/""" + "$" + """upselIDTag<[^/]+>""", """controllers.CallInteractionController.getTagNameByUpselLeadType(userLocation:Long, upselIDTag:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/getComplaintHistoryOfvehicalId/""" + "$" + """vehicalId<[^/]+>""", """controllers.CallInteractionController.getComplaintHistoryVeh(vehicalId:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/getSMSHistoryOfCustomerId/""" + "$" + """customerId<[^/]+>""", """controllers.CallInteractionController.getSMSHistoryOfCustomer(customerId:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """assignedInteractionTableData""", """controllers.SearchController.assignedInteractionData()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """followUpCallLogTableData""", """controllers.SearchController.followUpRequiredInteractionData()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """serviceBookedServerDataTable""", """controllers.SearchController.serviceBookedInteractionData()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """serviceNotRequiredServerDataTable""", """controllers.SearchController.serviceNotRequiredInteractionData()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """nonContactsServerDataTable""", """controllers.SearchController.nonContactsInteractionData()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """droppedCallsServerDataTable""", """controllers.SearchController.droppedCallInteractionData()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/uploadExcelPOST""", """controllers.UploadExcelController.uploadExcelData()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """driverData""", """controllers.FirebaseSyncController.startSyncOperationDriverPickupDropList()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """ServiceAdvisor""", """controllers.WyzUserController.indexPageServiceAdvisor()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """ServiceAdvisor/changepassword""", """controllers.WyzUserController.changepasswordServiceAdvisor"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """ServiceAdvisor/passwordchange""", """controllers.WyzUserController.changepasswordeditingServiceAdvisor()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """OthersLogins""", """controllers.WyzUserController.indexPageOthers()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """addcampaign""", """controllers.CampaignController.addCampaign()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """postCampaign""", """controllers.CampaignController.postCampaign()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/PSFList""", """controllers.PSFController.getPSFList()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/PSF15List""", """controllers.PSFController.getPSF15List()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/PSF30List""", """controllers.PSFController.getPSF30List()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/PSF3RDList""", """controllers.PSFController.getPSF3rdDayList()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/PSFNextDayList""", """controllers.PSFController.getPSFNextDayList()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/PSF4thDayList""", """controllers.PSFController.getPSF4thDayList()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """assignedInteractionTablePSFData/""" + "$" + """name<[^/]+>""", """controllers.PSFController.assignedInteractionPSFData(name:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """ajaxCallForFollowUpRequiredPSFData/""" + "$" + """psfDay<[^/]+>""", """controllers.PSFController.ajaxCallForFollowUpRequiredPSFData(psfDay:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """ajaxCallForIncompletedSurveyPSFData/""" + "$" + """psfDay<[^/]+>""", """controllers.PSFController.ajaxCallForIncompletedSurveyPSFData(psfDay:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """ajaxCallForCompletedSurveyPSFData/""" + "$" + """psfDay<[^/]+>""", """controllers.PSFController.ajaxCallForCompletedSurveyPSFData(psfDay:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """ajaxCallForDissatisfiedPSFData/""" + "$" + """psfDay<[^/]+>""", """controllers.PSFController.ajaxCallForDissatisfiedPSFData(psfDay:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """ajaxCallForNonContactsPSFData/""" + "$" + """psfDay<[^/]+>""", """controllers.PSFController.ajaxCallForNonContactsPSFData(psfDay:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """ajaxCallForDroppedCallsPSFData/""" + "$" + """psfDay<[^/]+>""", """controllers.PSFController.ajaxCallForDroppedCallsPSFData(psfDay:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """ajaxCallForAppointmentPSFData/""" + "$" + """psfDay<[^/]+>""", """controllers.PSFController.ajaxCallForAppointmentPSFData(psfDay:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """assignedInteractionTableDataMR/""" + "$" + """CREIds<[^/]+>""", """controllers.SearchControllerMR.assignedInteractionDataMR(CREIds:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """followUpCallLogTableDataMR/""" + "$" + """CREIds<[^/]+>""", """controllers.SearchControllerMR.followUpRequiredInteractionDataMR(CREIds:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """serviceBookedServerDataTableMR/""" + "$" + """CREIds<[^/]+>""", """controllers.SearchControllerMR.serviceBookedInteractionDataMR(CREIds:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """serviceNotRequiredServerDataTableMR/""" + "$" + """CREIds<[^/]+>""", """controllers.SearchControllerMR.serviceNotRequiredInteractionDataMR(CREIds:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """nonContactsServerDataTableMR/""" + "$" + """CREIds<[^/]+>""", """controllers.SearchControllerMR.nonContactsInteractionDataMR(CREIds:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """droppedCallsServerDataTableMR/""" + "$" + """CREIds<[^/]+>""", """controllers.SearchControllerMR.droppedCallInteractionDataMR(CREIds:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """missedCallsServerDataTableMR/""" + "$" + """CREIds<[^/]+>""", """controllers.SearchControllerMR.missedCallInteractionDataMR(CREIds:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """incomingCallsServerDataTableMR/""" + "$" + """CREIds<[^/]+>""", """controllers.SearchControllerMR.incomingCallInteractionDataMR(CREIds:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """outgoingCallsServerDataTableMR/""" + "$" + """CREIds<[^/]+>""", """controllers.SearchControllerMR.outgoingCallsServerDataTableMR(CREIds:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/downloadMediaFile/""" + "$" + """callId<[^/]+>""", """controllers.SearchControllerMR.getMediaFileMR(callId:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/vehicleRegNoExist/""" + "$" + """vehicleReg<[^/]+>""", """controllers.CallInteractionController.getExistingVehicleRegCount(vehicleReg:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/dashboardCounts""", """controllers.SearchControllerMR.getDashboardCount()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/dashboardCounts""", """controllers.SearchController.getDashboardCountCRE()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/sendSMS/""" + "$" + """rerenceNumber<[^/]+>""", """controllers.CallInteractionController.sendCustomSMSAjax(rerenceNumber:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/listDrivers/""" + "$" + """workshopId<[^/]+>""", """controllers.CallInteractionController.getDriverListBasedOnworkshop(workshopId:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/listCREsByWorkshop/""" + "$" + """workshopId<[^/]+>""", """controllers.CallInteractionController.getCRESListBasedOnWorkshop(workshopId:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/listCREsByWorkshopcallhistory/""" + "$" + """workshopId<[^/]+>""", """controllers.CallInteractionController.getCREListBasedOnWorkshopCallHistory(workshopId:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """ServiceAdvisor/resolveComplaint""", """controllers.ServiceAdvisorController.getServiceAdvisorComplaints()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/callDispositionPage/""" + "$" + """workshopId<[^/]+>/""" + "$" + """date<[^/]+>""", """controllers.AutoSelectionSAController.ajaxAutoSASelection(workshopId:Long, date:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/callDispositionPage/""" + "$" + """saDetails<[^/]+>""", """controllers.AutoSelectionSAController.ajaxAutoSASelectionList(saDetails:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/callDispositionPage/""" + "$" + """workshopId<[^/]+>/""" + "$" + """date<[^/]+>/""" + "$" + """preSaDetails<[^/]+>/""" + "$" + """newSaDetails<[^/]+>""", """controllers.AutoSelectionSAController.ajaxupdateSaChange(workshopId:Long, date:String, preSaDetails:String, newSaDetails:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/callDispositionPageIns/""" + "$" + """date<[^/]+>""", """controllers.AutoSelInsuranceAgentController.ajaxAutoInsurAgentSelection(date:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/callDispositionPageIns/""" + "$" + """saDetails<[^/]+>""", """controllers.AutoSelInsuranceAgentController.ajaxAutoInsurAgentSelectionList(saDetails:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/callDispositionPageIns/""" + "$" + """date<[^/]+>/""" + "$" + """preSaDetails<[^/]+>/""" + "$" + """newSaDetails<[^/]+>""", """controllers.AutoSelInsuranceAgentController.ajaxupdateInsuAgentChange(date:String, preSaDetails:String, newSaDetails:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/upsellSelectedLastSB/""" + "$" + """sr_int_id<[^/]+>""", """controllers.CallInfoController.getUpsellLeadsSeletedInLastSB(sr_int_id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/getDispositionFormPage/""" + "$" + """cid<[^/]+>/""" + "$" + """vehicle_id<[^/]+>/""" + "$" + """typeDispo<[^/]+>""", """controllers.InsuranceController.getCommonCallDispositionForm(cid:Long, vehicle_id:Long, typeDispo:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/getDispositionPageOfTab""", """controllers.InsuranceController.postCommonCallDispositionForm()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/insuranceAssignedList""", """controllers.InsuranceController.getAllInsuranceAssignedList()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """insurance/assignedInsuInteraction""", """controllers.InsuranceController.getAssignedInsuInteraction()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """insurance/contactedDispoInteractions/""" + "$" + """typeOfdispo<[^/]+>""", """controllers.InsuranceController.getContactedDispoFormData(typeOfdispo:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """insurance/nonContactedDispoInterac/""" + "$" + """typeOfdispo<[^/]+>""", """controllers.InsuranceController.getNonContactDroppedInsuranceDispoData(typeOfdispo:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/psfDispo/""" + "$" + """cid<[^/]+>/""" + "$" + """vehicle_id<[^/]+>/""" + "$" + """interactionid<[^/]+>/""" + "$" + """dispositionHistory<[^/]+>/""" + "$" + """typeOfPSF<[^/]+>""", """controllers.PSFController.getCommonPSFDispositionPage(cid:Long, vehicle_id:Long, interactionid:Long, dispositionHistory:Long, typeOfPSF:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/psfDispo""", """controllers.PSFController.postCommonPSFDispositionPage()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/uploadExcelSheetInsurance/""" + "$" + """uploadId<[^/]+>/""" + "$" + """uploadType<[^/]+>/""" + "$" + """resultData<[^/]+>""", """controllers.InsuranceController.upload_Excel_Sheet_insurance(uploadId:String, uploadType:String, resultData:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/assignCallInsurance""", """controllers.InsuranceController.getPageForAssigningCallsInsurance()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/assignCallInsurance""", """controllers.InsuranceController.postAssignCallInsurance()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/getODPercentage/""" + "$" + """cubicCap<[^/]+>/""" + "$" + """vehAge<[^/]+>/""" + "$" + """zoneid<[^/]+>""", """controllers.InsuranceController.getODPercentage(cubicCap:String, vehAge:String, zoneid:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/getBasicODValue/""" + "$" + """odvValue<[^/]+>/""" + "$" + """idvValue<[^/]+>""", """controllers.InsuranceController.getBasicODVaue(odvValue:Double, idvValue:Double)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/getNCBBYBasicODValue/""" + "$" + """ncbPercenVal<[^/]+>/""" + "$" + """basicODValue<[^/]+>""", """controllers.InsuranceController.getNCBValueByBasicValue(ncbPercenVal:Double, basicODValue:Double)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/getDiscValueVyODPremium/""" + "$" + """odPremiumVaue<[^/]+>/""" + "$" + """commercialDiscPerceValue<[^/]+>/""" + "$" + """thirdPartPremiumValue<[^/]+>""", """controllers.InsuranceController.gettotalPremiumAndDiscValue(odPremiumVaue:Double, commercialDiscPerceValue:Double, thirdPartPremiumValue:Double)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/gettotalPremiumOdAddOn/""" + "$" + """addOnPremiumValuePer<[^/]+>/""" + "$" + """odPremiumVaue<[^/]+>/""" + "$" + """commercialDiscPerceValue<[^/]+>/""" + "$" + """idvVal<[^/]+>/""" + "$" + """thirdPartPremiumValue<[^/]+>""", """controllers.InsuranceController.gettotalPremiumForAddOn(addOnPremiumValuePer:Double, odPremiumVaue:Double, commercialDiscPerceValue:Double, idvVal:Double, thirdPartPremiumValue:Double)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/showRoomList""", """controllers.InsuranceController.showRoomList()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/downloadExcelSheet/""" + "$" + """uploadId<[^/]+>/""" + "$" + """uploadType<[^/]+>""", """controllers.InsuranceController.downloadInsuranceErrorData(uploadId:String, uploadType:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/addInsuranceCampaign""", """controllers.CampaignController.addCampaignInsurance()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/ajax/getInsuranceFollowupNotificationOfToday""", """controllers.InsuranceController.insuranceFollowUpNotificationToday()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """insuranceHistoryUpload""", """controllers.InsuranceHistoryController.uploadHistoryViewPage()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """insuranceHistoryUpload""", """controllers.InsuranceHistoryController.uploadExcelDataHistory()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/getInsuranceHistoryOfCustomerId/""" + "$" + """customerId<[^/]+>""", """controllers.InsuranceController.insuranceHistoryOfCustomerId(customerId:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/getOEMOfDealer""", """controllers.DealerController.getOEMOfDealer()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """fileuploadPage""", """controllers.FileUploadController.uploadPage()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """postfile""", """controllers.FileUploadController.startUpload()"""),
    ("""HEAD""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """postfile""", """controllers.FileUploadController.headRequest()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """processFile""", """controllers.ProcessUploadedFiles.process()"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """deletefile/""" + "$" + """id<[^/]+>""", """controllers.FileUploadController.deleteFile(id:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """downloadFile/""" + "$" + """id<[^/]+>""", """controllers.FileUploadController.downloadFile(id:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """downloadErrors/""" + "$" + """id<[^/]+>""", """controllers.FileUploadController.downloadErrors(id:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """wyzprocessFile/""" + "$" + """id<[^/]+>""", """controllers.ProcessUploadedFiles.processFile(id:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """uploadList/""" + "$" + """utype<[^/]+>""", """controllers.FileUploadController.getUploadsList(utype:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """uploadListById/""" + "$" + """upId<[^/]+>""", """controllers.FileUploadController.getUploadsListById(upId:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """getExistingFiles/""" + "$" + """utype<[^/]+>""", """controllers.FileUploadController.getExistingFiles(utype:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """uploadReportPage""", """controllers.FileUploadController.uploadReportPage()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """SMSTriggger""", """controllers.SMSandEmailController.startTriggerSMS()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """getworkshopByLocation/""" + "$" + """locations<[^/]+>""", """controllers.CallInteractionController.getWorkshopsByLocations(locations:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """getCRESByWorkshop/""" + "$" + """workshops<[^/]+>""", """controllers.CallInteractionController.getCRESByWorkshops(workshops:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """getCityByStates/""" + "$" + """selState<[^/]+>""", """controllers.CallInteractionController.getCitiesByState(selState:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """viewAllComplaints""", """controllers.CallInteractionController.viewAllComplaintsReadOnlyAccess()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/listWorkshopsByID/""" + "$" + """selectedCity<[^/]+>""", """controllers.CallInteractionController.getListWorkshopByLocationById(selectedCity:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/listFunctionsByLocation/""" + "$" + """selectedCity<[^/]+>""", """controllers.CallInteractionController.getFunctionsListByLoc(selectedCity:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/listUsersByFuncandLoc/""" + "$" + """city<[^/]+>/""" + "$" + """function<[^/]+>""", """controllers.CallInteractionController.getusersByFuncandLocation(city:String, function:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/changeAssignedCalls""", """controllers.ChangeAssignmentController.changeassignedCalls()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/changeAssignedCalls""", """controllers.ChangeAssignmentController.postchangeassignedCalls()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/assignCallToAgents""", """controllers.ChangeAssignmentController.changeAssignedCallsToAgents()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """getCampaignList/""" + "$" + """uploadType<[^/]+>""", """controllers.CampaignController.getCampaignNamesByUpload(uploadType:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/assignmentFilterPage""", """controllers.ChangeAssignmentController.assignmentFilterList()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/assignedCallsList""", """controllers.ChangeAssignmentController.getAssignmentFilterListAjax()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/assignedCallsListToUser""", """controllers.ChangeAssignmentController.assignListToUserSelected()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/getLeadNamesbyLeadId/""" + "$" + """leadId<[^/]+>/""" + "$" + """userId<[^/]+>""", """controllers.CallInfoController.getLeadNamesbyLeadId(leadId:Long, userId:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/checkingVehicleRegNo/""" + "$" + """vehRegId<[^/]+>""", """controllers.CallInfoController.getCheckVehicleRegExist(vehRegId:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """LocationBySuperAdmins""", """controllers.SuperAdminController.LocationBySuperAdmin()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """postLocationBySuperAdmins""", """controllers.SuperAdminController.postLocationBySuperAdmin()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """UsersBySuperAdmins""", """controllers.SuperAdminController.UsersBySuperAdmin()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """postUserBySuperAdmins""", """controllers.SuperAdminController.postUserBySuperAdmin()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """checkExistingUser/""" + "$" + """uname<[^/]+>""", """controllers.SuperAdminController.checkIfUserExists(uname:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """SMSTemplateBySuperAdmin""", """controllers.SMSTemplateController.SMSTemplateBySuperAdmin()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """postSMSTemplate/""" + "$" + """messageTemplate<[^/]+>/""" + "$" + """msgAPI<[^/]+>""", """controllers.SMSTemplateController.postSMSTemplate(messageTemplate:String, msgAPI:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """getCustomersListBySavedName/""" + "$" + """savedsearchname<[^/]+>""", """controllers.SMSTemplateController.getCustomersListBySavedName(savedsearchname:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """postSMSBulk""", """controllers.SMSTemplateController.postSMSBulk()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """getSelectedUserListforDatatable/""" + "$" + """selectedUsers<[^/]+>/""" + "$" + """setFlag<[^/]+>/""" + "$" + """savedsearchname<[^/]+>""", """controllers.SMSTemplateController.getSelectedUserListforDatatable(selectedUsers:String, setFlag:Boolean, savedsearchname:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/getReviewBookingPage""", """controllers.ServiceBookController.getReviewBookingPage()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """serviceAdvisorListByWorkshop/""" + "$" + """workshopid<[^/]+>""", """controllers.ServiceBookController.getServiceAdvisorListByWorkshop(workshopid:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """searchServiceBookedList""", """controllers.ServiceBookController.getsearchServiceBoookedList()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/cancelBooking/""" + "$" + """servicebookedId<[^/]+>/""" + "$" + """cancelId<[^/]+>""", """controllers.ServiceBookController.cancelBookingOrPickup(servicebookedId:Long, cancelId:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/reviewResheduleBooking/""" + "$" + """servicebookedId<[^/]+>""", """controllers.ServiceBookController.reviewScheduleBooking(servicebookedId:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/postRescheduleBooking""", """controllers.ServiceBookController.postRescheduleBooking()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """driverListSchedule/""" + "$" + """workshopId<[^/]+>/""" + "$" + """scheduleDate<[^/]+>""", """controllers.ServiceBookController.driverListScheduleByWorkshopId(workshopId:Long, scheduleDate:String)"""),
    ("""HEAD""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/audiostream/""" + "$" + """interactionId<[^/]+>""", """controllers.AudioStreamController.header(interactionId:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CRE/audiostream/""" + "$" + """interactionId<[^/]+>""", """controllers.AudioStreamController.streamAudio(interactionId:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/audioconverter/""" + "$" + """startId<[^/]+>/""" + "$" + """endId<[^/]+>""", """controllers.AllAudioConverter.convertAllFiles(startId:Long, endId:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/callRecording""", """controllers.CallRecordingHistoryController.callRecordingView()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/recordingDataList""", """controllers.CallRecordingHistoryController.callRecordingData()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/callRecordingInsurance""", """controllers.CallRecordingHistoryController.callRecordingViewInsurance()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/recordingDataListInsurance""", """controllers.CallRecordingHistoryController.callRecordingDataInsurance()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/callRecordingPSF""", """controllers.CallRecordingHistoryController.callRecordingViewPSF()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/recordingDataListPSF""", """controllers.CallRecordingHistoryController.callRecordingDataPSF()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/otherCallRecording""", """controllers.CallRecordingHistoryController.otherCallRecordingView()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/otherRecordingDataList""", """controllers.CallRecordingHistoryController.otherCallRecordingData()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/startSyncInsuranceAgent""", """controllers.FirebaseSyncController.startSyncOperationInsuranceAgent()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/startSyncInsuranceHistory""", """controllers.FirebaseSyncController.startSyncOperationInsuranceAgentHistory()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """CREManager/downloadBooking""", """controllers.ServiceBookController.downloadServiceBooking()"""),
    Nil
  ).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
    case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
    case l => s ++ l.asInstanceOf[List[(String,String,String)]]
  }}


  // @LINE:7
  private[this] lazy val controllers_Application_landingPage0_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix)))
  )
  private[this] lazy val controllers_Application_landingPage0_invoker = createInvoker(
    Application_27.landingPage(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "landingPage",
      Nil,
      "GET",
      """""",
      this.prefix + """"""
    )
  )

  // @LINE:9
  private[this] lazy val controllers_Application_login1_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("login")))
  )
  private[this] lazy val controllers_Application_login1_invoker = createInvoker(
    Application_27.login,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "login",
      Nil,
      "GET",
      """""",
      this.prefix + """login"""
    )
  )

  // @LINE:10
  private[this] lazy val controllers_WyzUserController_changePassword2_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("changePassword")))
  )
  private[this] lazy val controllers_WyzUserController_changePassword2_invoker = createInvoker(
    WyzUserController_11.changePassword,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WyzUserController",
      "changePassword",
      Nil,
      "GET",
      """""",
      this.prefix + """changePassword"""
    )
  )

  // @LINE:11
  private[this] lazy val controllers_WyzUserController_logout3_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("logoutUser")))
  )
  private[this] lazy val controllers_WyzUserController_logout3_invoker = createInvoker(
    WyzUserController_11.logout,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WyzUserController",
      "logout",
      Nil,
      "GET",
      """""",
      this.prefix + """logoutUser"""
    )
  )

  // @LINE:13
  private[this] lazy val org_pac4j_play_CallbackController_callback4_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("callback")))
  )
  private[this] lazy val org_pac4j_play_CallbackController_callback4_invoker = createInvoker(
    CallbackController_30.get.callback(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "org.pac4j.play.CallbackController",
      "callback",
      Nil,
      "GET",
      """""",
      this.prefix + """callback"""
    )
  )

  // @LINE:14
  private[this] lazy val org_pac4j_play_CallbackController_callback5_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("callback")))
  )
  private[this] lazy val org_pac4j_play_CallbackController_callback5_invoker = createInvoker(
    CallbackController_30.get.callback(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "org.pac4j.play.CallbackController",
      "callback",
      Nil,
      "POST",
      """""",
      this.prefix + """callback"""
    )
  )

  // @LINE:16
  private[this] lazy val controllers_Application_formIndex6_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("form/index.html")))
  )
  private[this] lazy val controllers_Application_formIndex6_invoker = createInvoker(
    Application_27.formIndex(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "formIndex",
      Nil,
      "GET",
      """""",
      this.prefix + """form/index.html"""
    )
  )

  // @LINE:18
  private[this] lazy val controllers_WyzUserController_logoutParticularUser7_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("logoutParticularUser/"), DynamicPart("userIs", """[^/]+""",true)))
  )
  private[this] lazy val controllers_WyzUserController_logoutParticularUser7_invoker = createInvoker(
    WyzUserController_11.logoutParticularUser(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WyzUserController",
      "logoutParticularUser",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """logoutParticularUser/""" + "$" + """userIs<[^/]+>"""
    )
  )

  // @LINE:22
  private[this] lazy val controllers_WyzUserController_index8_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("superAdmin/index")))
  )
  private[this] lazy val controllers_WyzUserController_index8_invoker = createInvoker(
    WyzUserController_11.index,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WyzUserController",
      "index",
      Nil,
      "GET",
      """Super Admin URL
GET    /superAdmin                                                                                                                                                      controllers.Application.getdealerCodeinfo
POST   /superAdmin                                                                                                                                                      controllers.Application.getDelerCodeAdmin""",
      this.prefix + """superAdmin/index"""
    )
  )

  // @LINE:25
  private[this] lazy val controllers_WyzUserController_addapp9_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("superAdmin/addUser")))
  )
  private[this] lazy val controllers_WyzUserController_addapp9_invoker = createInvoker(
    WyzUserController_11.addapp,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WyzUserController",
      "addapp",
      Nil,
      "GET",
      """""",
      this.prefix + """superAdmin/addUser"""
    )
  )

  // @LINE:26
  private[this] lazy val controllers_WyzUserController_addApplicationUser10_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("superAdmin/addUser")))
  )
  private[this] lazy val controllers_WyzUserController_addApplicationUser10_invoker = createInvoker(
    WyzUserController_11.addApplicationUser,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WyzUserController",
      "addApplicationUser",
      Nil,
      "POST",
      """""",
      this.prefix + """superAdmin/addUser"""
    )
  )

  // @LINE:28
  private[this] lazy val controllers_WyzUserController_userInformation11_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("superAdmin/userList")))
  )
  private[this] lazy val controllers_WyzUserController_userInformation11_invoker = createInvoker(
    WyzUserController_11.userInformation,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WyzUserController",
      "userInformation",
      Nil,
      "GET",
      """""",
      this.prefix + """superAdmin/userList"""
    )
  )

  // @LINE:29
  private[this] lazy val controllers_WyzUserController_userInformation12_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("superAdmin/userList")))
  )
  private[this] lazy val controllers_WyzUserController_userInformation12_invoker = createInvoker(
    WyzUserController_11.userInformation,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WyzUserController",
      "userInformation",
      Nil,
      "POST",
      """""",
      this.prefix + """superAdmin/userList"""
    )
  )

  // @LINE:31
  private[this] lazy val controllers_WyzUserController_geteditUser13_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("superAdmin/user/edit/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_WyzUserController_geteditUser13_invoker = createInvoker(
    WyzUserController_11.geteditUser(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WyzUserController",
      "geteditUser",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """superAdmin/user/edit/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:32
  private[this] lazy val controllers_WyzUserController_postEditUser14_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("superAdmin/user/edit/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_WyzUserController_postEditUser14_invoker = createInvoker(
    WyzUserController_11.postEditUser(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WyzUserController",
      "postEditUser",
      Seq(classOf[Long]),
      "POST",
      """""",
      this.prefix + """superAdmin/user/edit/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:33
  private[this] lazy val controllers_WyzUserController_deleteUserData15_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("superAdmin/user/delete/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_WyzUserController_deleteUserData15_invoker = createInvoker(
    WyzUserController_11.deleteUserData(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WyzUserController",
      "deleteUserData",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """superAdmin/user/delete/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:35
  private[this] lazy val controllers_DealerController_adddel16_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("superAdmin/addDealer")))
  )
  private[this] lazy val controllers_DealerController_adddel16_invoker = createInvoker(
    DealerController_28.adddel,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.DealerController",
      "adddel",
      Nil,
      "GET",
      """""",
      this.prefix + """superAdmin/addDealer"""
    )
  )

  // @LINE:36
  private[this] lazy val controllers_DealerController_adddealerForm17_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("superAdmin/addDealer")))
  )
  private[this] lazy val controllers_DealerController_adddealerForm17_invoker = createInvoker(
    DealerController_28.adddealerForm,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.DealerController",
      "adddealerForm",
      Nil,
      "POST",
      """""",
      this.prefix + """superAdmin/addDealer"""
    )
  )

  // @LINE:38
  private[this] lazy val controllers_DealerController_dealerInformation18_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("superAdmin/dealerList")))
  )
  private[this] lazy val controllers_DealerController_dealerInformation18_invoker = createInvoker(
    DealerController_28.dealerInformation,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.DealerController",
      "dealerInformation",
      Nil,
      "GET",
      """""",
      this.prefix + """superAdmin/dealerList"""
    )
  )

  // @LINE:39
  private[this] lazy val controllers_DealerController_deleteDealerData19_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("superAdmin/dealer/delete/"), DynamicPart("did", """[^/]+""",true)))
  )
  private[this] lazy val controllers_DealerController_deleteDealerData19_invoker = createInvoker(
    DealerController_28.deleteDealerData(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.DealerController",
      "deleteDealerData",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """superAdmin/dealer/delete/""" + "$" + """did<[^/]+>"""
    )
  )

  // @LINE:41
  private[this] lazy val controllers_DealerController_geteditDealer20_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("superAdmin/dealer/edit/"), DynamicPart("did", """[^/]+""",true)))
  )
  private[this] lazy val controllers_DealerController_geteditDealer20_invoker = createInvoker(
    DealerController_28.geteditDealer(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.DealerController",
      "geteditDealer",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """superAdmin/dealer/edit/""" + "$" + """did<[^/]+>"""
    )
  )

  // @LINE:42
  private[this] lazy val controllers_DealerController_postEditDealer21_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("superAdmin/edit/"), DynamicPart("did", """[^/]+""",true)))
  )
  private[this] lazy val controllers_DealerController_postEditDealer21_invoker = createInvoker(
    DealerController_28.postEditDealer(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.DealerController",
      "postEditDealer",
      Seq(classOf[Long]),
      "POST",
      """""",
      this.prefix + """superAdmin/edit/""" + "$" + """did<[^/]+>"""
    )
  )

  // @LINE:44
  private[this] lazy val controllers_ScheduledCallController_startSyncForTesting22_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("deleteRecordFireBase")))
  )
  private[this] lazy val controllers_ScheduledCallController_startSyncForTesting22_invoker = createInvoker(
    ScheduledCallController_1.startSyncForTesting(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ScheduledCallController",
      "startSyncForTesting",
      Nil,
      "GET",
      """""",
      this.prefix + """deleteRecordFireBase"""
    )
  )

  // @LINE:56
  private[this] lazy val controllers_WyzUserController_changepasswordsuperAdmin23_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("superAdmin/changepasswordsuperAdmin")))
  )
  private[this] lazy val controllers_WyzUserController_changepasswordsuperAdmin23_invoker = createInvoker(
    WyzUserController_11.changepasswordsuperAdmin,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WyzUserController",
      "changepasswordsuperAdmin",
      Nil,
      "GET",
      """""",
      this.prefix + """superAdmin/changepasswordsuperAdmin"""
    )
  )

  // @LINE:57
  private[this] lazy val controllers_WyzUserController_changepasswordeditingsuperAdmin24_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("superAdmin/passwordchangesuperAdmin")))
  )
  private[this] lazy val controllers_WyzUserController_changepasswordeditingsuperAdmin24_invoker = createInvoker(
    WyzUserController_11.changepasswordeditingsuperAdmin(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WyzUserController",
      "changepasswordeditingsuperAdmin",
      Nil,
      "POST",
      """""",
      this.prefix + """superAdmin/passwordchangesuperAdmin"""
    )
  )

  // @LINE:58
  private[this] lazy val controllers_WyzUserController_loginfo25_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("superAdmin/loginformation")))
  )
  private[this] lazy val controllers_WyzUserController_loginfo25_invoker = createInvoker(
    WyzUserController_11.loginfo(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WyzUserController",
      "loginfo",
      Nil,
      "GET",
      """""",
      this.prefix + """superAdmin/loginformation"""
    )
  )

  // @LINE:62
  private[this] lazy val controllers_WyzUserController_indexPageCREManager26_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager")))
  )
  private[this] lazy val controllers_WyzUserController_indexPageCREManager26_invoker = createInvoker(
    WyzUserController_11.indexPageCREManager(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WyzUserController",
      "indexPageCREManager",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager"""
    )
  )

  // @LINE:63
  private[this] lazy val controllers_WyzUserController_viewReportCREManager27_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/viewReport")))
  )
  private[this] lazy val controllers_WyzUserController_viewReportCREManager27_invoker = createInvoker(
    WyzUserController_11.viewReportCREManager,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WyzUserController",
      "viewReportCREManager",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager/viewReport"""
    )
  )

  // @LINE:66
  private[this] lazy val controllers_WyzUserController_changepassword28_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/changepassword")))
  )
  private[this] lazy val controllers_WyzUserController_changepassword28_invoker = createInvoker(
    WyzUserController_11.changepassword,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WyzUserController",
      "changepassword",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager/changepassword"""
    )
  )

  // @LINE:67
  private[this] lazy val controllers_WyzUserController_changepasswordediting29_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/passwordchange")))
  )
  private[this] lazy val controllers_WyzUserController_changepasswordediting29_invoker = createInvoker(
    WyzUserController_11.changepasswordediting(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WyzUserController",
      "changepasswordediting",
      Nil,
      "POST",
      """""",
      this.prefix + """CREManager/passwordchange"""
    )
  )

  // @LINE:71
  private[this] lazy val controllers_SearchControllerMR_getPSFassignedInteractionTableDataMR30_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/PSFassignedInteractionTableDataMR/"), DynamicPart("CREIds", """[^/]+""",true)))
  )
  private[this] lazy val controllers_SearchControllerMR_getPSFassignedInteractionTableDataMR30_invoker = createInvoker(
    SearchControllerMR_10.getPSFassignedInteractionTableDataMR(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SearchControllerMR",
      "getPSFassignedInteractionTableDataMR",
      Seq(classOf[String]),
      "GET",
      """PSF call log view Manager""",
      this.prefix + """CREManager/PSFassignedInteractionTableDataMR/""" + "$" + """CREIds<[^/]+>"""
    )
  )

  // @LINE:72
  private[this] lazy val controllers_SearchControllerMR_getPSFfollowUpCallLogTableDataMR31_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/PSFfollowUpCallLogTableDataMR/"), DynamicPart("CREIds", """[^/]+""",true), StaticPart("/"), DynamicPart("buckettype", """[^/]+""",true)))
  )
  private[this] lazy val controllers_SearchControllerMR_getPSFfollowUpCallLogTableDataMR31_invoker = createInvoker(
    SearchControllerMR_10.getPSFfollowUpCallLogTableDataMR(fakeValue[String], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SearchControllerMR",
      "getPSFfollowUpCallLogTableDataMR",
      Seq(classOf[String], classOf[Long]),
      "GET",
      """""",
      this.prefix + """CREManager/PSFfollowUpCallLogTableDataMR/""" + "$" + """CREIds<[^/]+>/""" + "$" + """buckettype<[^/]+>"""
    )
  )

  // @LINE:75
  private[this] lazy val controllers_SearchControllerMR_getPSFnonContactsServerDataTableMR32_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/PSFnonContactsServerDataTableMR/"), DynamicPart("CREIds", """[^/]+""",true), StaticPart("/"), DynamicPart("buckettype", """[^/]+""",true)))
  )
  private[this] lazy val controllers_SearchControllerMR_getPSFnonContactsServerDataTableMR32_invoker = createInvoker(
    SearchControllerMR_10.getPSFnonContactsServerDataTableMR(fakeValue[String], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SearchControllerMR",
      "getPSFnonContactsServerDataTableMR",
      Seq(classOf[String], classOf[Long]),
      "GET",
      """GET    /CREManager/PSFCompletedSurveyDataTableMR/:CREIds																											controllers.SearchControllerMR.getPSFCompletedSurveyDataTableMR(CREIds :String)
GET    /CREManager/PSFAppointmentsServerDataTableMR/:CREIds																										controllers.SearchControllerMR.getPSFAppointmentsServerDataTableMR(CREIds :String)""",
      this.prefix + """CREManager/PSFnonContactsServerDataTableMR/""" + "$" + """CREIds<[^/]+>/""" + "$" + """buckettype<[^/]+>"""
    )
  )

  // @LINE:79
  private[this] lazy val controllers_SearchControllerMR_getInsuranceassignedInteractionTableDataMR33_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/InsuranceassignedInteractionTableDataMR/"), DynamicPart("CREIds", """[^/]+""",true)))
  )
  private[this] lazy val controllers_SearchControllerMR_getInsuranceassignedInteractionTableDataMR33_invoker = createInvoker(
    SearchControllerMR_10.getInsuranceassignedInteractionTableDataMR(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SearchControllerMR",
      "getInsuranceassignedInteractionTableDataMR",
      Seq(classOf[String]),
      "GET",
      """Insurance call log view Manager""",
      this.prefix + """CREManager/InsuranceassignedInteractionTableDataMR/""" + "$" + """CREIds<[^/]+>"""
    )
  )

  // @LINE:80
  private[this] lazy val controllers_SearchControllerMR_getInsurancefollowUpCallLogTableDataMR34_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/InsurancefollowUpCallLogTableDataMR/"), DynamicPart("CREIds", """[^/]+""",true), StaticPart("/"), DynamicPart("buckettype", """[^/]+""",true)))
  )
  private[this] lazy val controllers_SearchControllerMR_getInsurancefollowUpCallLogTableDataMR34_invoker = createInvoker(
    SearchControllerMR_10.getInsurancefollowUpCallLogTableDataMR(fakeValue[String], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SearchControllerMR",
      "getInsurancefollowUpCallLogTableDataMR",
      Seq(classOf[String], classOf[Long]),
      "GET",
      """""",
      this.prefix + """CREManager/InsurancefollowUpCallLogTableDataMR/""" + "$" + """CREIds<[^/]+>/""" + "$" + """buckettype<[^/]+>"""
    )
  )

  // @LINE:83
  private[this] lazy val controllers_SearchControllerMR_getInsurancenonContactsServerDataTableMR35_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/InsurancenonContactsServerDataTableMR/"), DynamicPart("CREIds", """[^/]+""",true), StaticPart("/"), DynamicPart("buckettype", """[^/]+""",true)))
  )
  private[this] lazy val controllers_SearchControllerMR_getInsurancenonContactsServerDataTableMR35_invoker = createInvoker(
    SearchControllerMR_10.getInsurancenonContactsServerDataTableMR(fakeValue[String], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SearchControllerMR",
      "getInsurancenonContactsServerDataTableMR",
      Seq(classOf[String], classOf[Long]),
      "GET",
      """GET    /CREManager/PSFCompletedSurveyDataTableMR/:CREIds																											controllers.SearchControllerMR.getPSFCompletedSurveyDataTableMR(CREIds :String)
GET    /CREManager/PSFAppointmentsServerDataTableMR/:CREIds																										controllers.SearchControllerMR.getPSFAppointmentsServerDataTableMR(CREIds :String)""",
      this.prefix + """CREManager/InsurancenonContactsServerDataTableMR/""" + "$" + """CREIds<[^/]+>/""" + "$" + """buckettype<[^/]+>"""
    )
  )

  // @LINE:88
  private[this] lazy val controllers_CallInfoController_getCallDispositionBucketForCREMan36_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/CallDispositionBucketForCREMan")))
  )
  private[this] lazy val controllers_CallInfoController_getCallDispositionBucketForCREMan36_invoker = createInvoker(
    CallInfoController_26.getCallDispositionBucketForCREMan(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInfoController",
      "getCallDispositionBucketForCREMan",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager/CallDispositionBucketForCREMan"""
    )
  )

  // @LINE:89
  private[this] lazy val controllers_CallInfoController_getPSFCallLogViewPage37_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/PSFCallLogPageCREManager")))
  )
  private[this] lazy val controllers_CallInfoController_getPSFCallLogViewPage37_invoker = createInvoker(
    CallInfoController_26.getPSFCallLogViewPage(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInfoController",
      "getPSFCallLogViewPage",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager/PSFCallLogPageCREManager"""
    )
  )

  // @LINE:90
  private[this] lazy val controllers_CallInfoController_getInsuranceCallLogViewPage38_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/InsuranceCallLogPageCREManager")))
  )
  private[this] lazy val controllers_CallInfoController_getInsuranceCallLogViewPage38_invoker = createInvoker(
    CallInfoController_26.getInsuranceCallLogViewPage(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInfoController",
      "getInsuranceCallLogViewPage",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager/InsuranceCallLogPageCREManager"""
    )
  )

  // @LINE:95
  private[this] lazy val controllers_CallInteractionController_getAssignedCallsOfCREManager39_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/getAssignedCallsOfCREManager/"), DynamicPart("selectedAgent", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_getAssignedCallsOfCREManager39_invoker = createInvoker(
    CallInteractionController_12.getAssignedCallsOfCREManager(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getAssignedCallsOfCREManager",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """CREManager/getAssignedCallsOfCREManager/""" + "$" + """selectedAgent<[^/]+>"""
    )
  )

  // @LINE:96
  private[this] lazy val controllers_CallInteractionController_getFollowUpRequiredDataCREMan40_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/getFollowUpRequiredDataCREMan/"), DynamicPart("selectedAgent", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_getFollowUpRequiredDataCREMan40_invoker = createInvoker(
    CallInteractionController_12.getFollowUpRequiredDataCREMan(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getFollowUpRequiredDataCREMan",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """CREManager/getFollowUpRequiredDataCREMan/""" + "$" + """selectedAgent<[^/]+>"""
    )
  )

  // @LINE:97
  private[this] lazy val controllers_CallInteractionController_getServiceBookedDataCREMan41_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/getServiceBookedDataCREMan/"), DynamicPart("selectedAgent", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_getServiceBookedDataCREMan41_invoker = createInvoker(
    CallInteractionController_12.getServiceBookedDataCREMan(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getServiceBookedDataCREMan",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """CREManager/getServiceBookedDataCREMan/""" + "$" + """selectedAgent<[^/]+>"""
    )
  )

  // @LINE:98
  private[this] lazy val controllers_CallInteractionController_getServiceNotRequiredDataCREMan42_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/getServiceNotRequiredDataCREMan/"), DynamicPart("selectedAgent", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_getServiceNotRequiredDataCREMan42_invoker = createInvoker(
    CallInteractionController_12.getServiceNotRequiredDataCREMan(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getServiceNotRequiredDataCREMan",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """CREManager/getServiceNotRequiredDataCREMan/""" + "$" + """selectedAgent<[^/]+>"""
    )
  )

  // @LINE:99
  private[this] lazy val controllers_CallInteractionController_getNonContactsDataCREMan43_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/getNonContactsDataCREMan/"), DynamicPart("selectedAgent", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_getNonContactsDataCREMan43_invoker = createInvoker(
    CallInteractionController_12.getNonContactsDataCREMan(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getNonContactsDataCREMan",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """CREManager/getNonContactsDataCREMan/""" + "$" + """selectedAgent<[^/]+>"""
    )
  )

  // @LINE:100
  private[this] lazy val controllers_CallInteractionController_getDroppedCallsDataCREMan44_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/getDroppedCallsDataCREMan/"), DynamicPart("selectedAgent", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_getDroppedCallsDataCREMan44_invoker = createInvoker(
    CallInteractionController_12.getDroppedCallsDataCREMan(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getDroppedCallsDataCREMan",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """CREManager/getDroppedCallsDataCREMan/""" + "$" + """selectedAgent<[^/]+>"""
    )
  )

  // @LINE:104
  private[this] lazy val controllers_WyzUserController_indexPageCRE45_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE")))
  )
  private[this] lazy val controllers_WyzUserController_indexPageCRE45_invoker = createInvoker(
    WyzUserController_11.indexPageCRE(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WyzUserController",
      "indexPageCRE",
      Nil,
      "GET",
      """""",
      this.prefix + """CRE"""
    )
  )

  // @LINE:105
  private[this] lazy val controllers_ReportsController_viewReportCRE46_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/viewReport")))
  )
  private[this] lazy val controllers_ReportsController_viewReportCRE46_invoker = createInvoker(
    ReportsController_19.viewReportCRE(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ReportsController",
      "viewReportCRE",
      Nil,
      "GET",
      """""",
      this.prefix + """CRE/viewReport"""
    )
  )

  // @LINE:106
  private[this] lazy val controllers_CallInfoController_getcallLogEditForCRE47_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/toeditcallLog/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInfoController_getcallLogEditForCRE47_invoker = createInvoker(
    CallInfoController_26.getcallLogEditForCRE(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInfoController",
      "getcallLogEditForCRE",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """CRE/toeditcallLog/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:107
  private[this] lazy val controllers_CallInfoController_postcallLogeditForCRE48_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/toeditcallLog/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInfoController_postcallLogeditForCRE48_invoker = createInvoker(
    CallInfoController_26.postcallLogeditForCRE(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInfoController",
      "postcallLogeditForCRE",
      Seq(classOf[Long]),
      "POST",
      """""",
      this.prefix + """CRE/toeditcallLog/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:110
  private[this] lazy val controllers_WyzUserController_changepasswordCRE49_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/changepasswordcre")))
  )
  private[this] lazy val controllers_WyzUserController_changepasswordCRE49_invoker = createInvoker(
    WyzUserController_11.changepasswordCRE(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WyzUserController",
      "changepasswordCRE",
      Nil,
      "GET",
      """""",
      this.prefix + """CRE/changepasswordcre"""
    )
  )

  // @LINE:111
  private[this] lazy val controllers_WyzUserController_changepasswordeditingCRE50_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/passwordchangecre")))
  )
  private[this] lazy val controllers_WyzUserController_changepasswordeditingCRE50_invoker = createInvoker(
    WyzUserController_11.changepasswordeditingCRE(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WyzUserController",
      "changepasswordeditingCRE",
      Nil,
      "POST",
      """""",
      this.prefix + """CRE/passwordchangecre"""
    )
  )

  // @LINE:118
  private[this] lazy val controllers_ScheduledCallController_getCallDispositionTabPAgeCRE51_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/getDispositionPageOfTab")))
  )
  private[this] lazy val controllers_ScheduledCallController_getCallDispositionTabPAgeCRE51_invoker = createInvoker(
    ScheduledCallController_1.getCallDispositionTabPAgeCRE(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ScheduledCallController",
      "getCallDispositionTabPAgeCRE",
      Nil,
      "GET",
      """""",
      this.prefix + """CRE/getDispositionPageOfTab"""
    )
  )

  // @LINE:123
  private[this] lazy val controllers_CallInfoController_startInitiatingOfCall52_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/ajax/initiateCall/"), DynamicPart("phonenumber", """[^/]+""",true), StaticPart("/"), DynamicPart("uniqueid", """[^/]+""",true), StaticPart("/"), DynamicPart("customerId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInfoController_startInitiatingOfCall52_invoker = createInvoker(
    CallInfoController_26.startInitiatingOfCall(fakeValue[Long], fakeValue[Long], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInfoController",
      "startInitiatingOfCall",
      Seq(classOf[Long], classOf[Long], classOf[Long]),
      "GET",
      """GET    /:dealercode/CRE/getFollowUpCallDispositionPage/:id                                                                                                              controllers.CallInfoController.getFollowUpDispositionForm(dealercode :String,id: Long)
POST		/:dealercode/CRE/postFollowUpCallDisposition				controllers.CallInfoController.postFollowUpCallDispositionForm(dealercode :String)
POST   /CRE/getFollowUpCallDispositionPageData                                                                                                                          controllers.CallInfoController.postCallInteractionDispositionForm()""",
      this.prefix + """CRE/ajax/initiateCall/""" + "$" + """phonenumber<[^/]+>/""" + "$" + """uniqueid<[^/]+>/""" + "$" + """customerId<[^/]+>"""
    )
  )

  // @LINE:126
  private[this] lazy val controllers_ReportsController_getAllAjaxRequestForCRE53_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/ajax/getAjaxDataCompleteData")))
  )
  private[this] lazy val controllers_ReportsController_getAllAjaxRequestForCRE53_invoker = createInvoker(
    ReportsController_19.getAllAjaxRequestForCRE(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ReportsController",
      "getAllAjaxRequestForCRE",
      Nil,
      "GET",
      """""",
      this.prefix + """CRE/ajax/getAjaxDataCompleteData"""
    )
  )

  // @LINE:128
  private[this] lazy val controllers_ReportsController_getScheduledCallsCountOfCRE54_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/ajax/getTtlSchCallsCRE")))
  )
  private[this] lazy val controllers_ReportsController_getScheduledCallsCountOfCRE54_invoker = createInvoker(
    ReportsController_19.getScheduledCallsCountOfCRE(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ReportsController",
      "getScheduledCallsCountOfCRE",
      Nil,
      "GET",
      """""",
      this.prefix + """CRE/ajax/getTtlSchCallsCRE"""
    )
  )

  // @LINE:129
  private[this] lazy val controllers_ReportsController_getScheduledCallsPendingCountForCRE55_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/ajax/getTtlSchCallsPendingCRE")))
  )
  private[this] lazy val controllers_ReportsController_getScheduledCallsPendingCountForCRE55_invoker = createInvoker(
    ReportsController_19.getScheduledCallsPendingCountForCRE(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ReportsController",
      "getScheduledCallsPendingCountForCRE",
      Nil,
      "GET",
      """""",
      this.prefix + """CRE/ajax/getTtlSchCallsPendingCRE"""
    )
  )

  // @LINE:130
  private[this] lazy val controllers_ReportsController_getServiceBookedForCRE56_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/ajax/getTtlServiceBookedForCRE")))
  )
  private[this] lazy val controllers_ReportsController_getServiceBookedForCRE56_invoker = createInvoker(
    ReportsController_19.getServiceBookedForCRE(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ReportsController",
      "getServiceBookedForCRE",
      Nil,
      "GET",
      """""",
      this.prefix + """CRE/ajax/getTtlServiceBookedForCRE"""
    )
  )

  // @LINE:131
  private[this] lazy val controllers_ReportsController_getServiceBookedPercentageForCRE57_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/ajax/getPercServiceBookedForsCRE")))
  )
  private[this] lazy val controllers_ReportsController_getServiceBookedPercentageForCRE57_invoker = createInvoker(
    ReportsController_19.getServiceBookedPercentageForCRE(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ReportsController",
      "getServiceBookedPercentageForCRE",
      Nil,
      "GET",
      """""",
      this.prefix + """CRE/ajax/getPercServiceBookedForsCRE"""
    )
  )

  // @LINE:132
  private[this] lazy val controllers_ReportsController_getCallTypePieForCRE58_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/ajax/getCallTypePieForCRE")))
  )
  private[this] lazy val controllers_ReportsController_getCallTypePieForCRE58_invoker = createInvoker(
    ReportsController_19.getCallTypePieForCRE(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ReportsController",
      "getCallTypePieForCRE",
      Nil,
      "GET",
      """""",
      this.prefix + """CRE/ajax/getCallTypePieForCRE"""
    )
  )

  // @LINE:133
  private[this] lazy val controllers_ReportsController_getBookedListByTimeForCRE59_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/ajax/getBookedListByTime")))
  )
  private[this] lazy val controllers_ReportsController_getBookedListByTimeForCRE59_invoker = createInvoker(
    ReportsController_19.getBookedListByTimeForCRE(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ReportsController",
      "getBookedListByTimeForCRE",
      Nil,
      "GET",
      """""",
      this.prefix + """CRE/ajax/getBookedListByTime"""
    )
  )

  // @LINE:137
  private[this] lazy val controllers_CallInteractionController_getFollowUpRequiredData60_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/getFollowUpRequiredData/"), DynamicPart("selectAgent", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_getFollowUpRequiredData60_invoker = createInvoker(
    CallInteractionController_12.getFollowUpRequiredData(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getFollowUpRequiredData",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """CRE/getFollowUpRequiredData/""" + "$" + """selectAgent<[^/]+>"""
    )
  )

  // @LINE:138
  private[this] lazy val controllers_CallInteractionController_getServiceBookedData61_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/getServiceBookedData/"), DynamicPart("selectAgent", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_getServiceBookedData61_invoker = createInvoker(
    CallInteractionController_12.getServiceBookedData(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getServiceBookedData",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """CRE/getServiceBookedData/""" + "$" + """selectAgent<[^/]+>"""
    )
  )

  // @LINE:139
  private[this] lazy val controllers_CallInteractionController_getNonContactsData62_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/getNonContactsData/"), DynamicPart("selectAgent", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_getNonContactsData62_invoker = createInvoker(
    CallInteractionController_12.getNonContactsData(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getNonContactsData",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """CRE/getNonContactsData/""" + "$" + """selectAgent<[^/]+>"""
    )
  )

  // @LINE:140
  private[this] lazy val controllers_CallInteractionController_getDroppedCallsData63_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/getDroppedCallsData/"), DynamicPart("selectAgent", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_getDroppedCallsData63_invoker = createInvoker(
    CallInteractionController_12.getDroppedCallsData(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getDroppedCallsData",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """CRE/getDroppedCallsData/""" + "$" + """selectAgent<[^/]+>"""
    )
  )

  // @LINE:141
  private[this] lazy val controllers_CallInteractionController_getServiceNotRequiredData64_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/getServiceNotRequiredData/"), DynamicPart("selectAgent", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_getServiceNotRequiredData64_invoker = createInvoker(
    CallInteractionController_12.getServiceNotRequiredData(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getServiceNotRequiredData",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """CRE/getServiceNotRequiredData/""" + "$" + """selectAgent<[^/]+>"""
    )
  )

  // @LINE:145
  private[this] lazy val controllers_CallInfoController_getFollowUpTableDataOfCRE65_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/listTodaysFollowUp")))
  )
  private[this] lazy val controllers_CallInfoController_getFollowUpTableDataOfCRE65_invoker = createInvoker(
    CallInfoController_26.getFollowUpTableDataOfCRE(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInfoController",
      "getFollowUpTableDataOfCRE",
      Nil,
      "GET",
      """""",
      this.prefix + """CRE/listTodaysFollowUp"""
    )
  )

  // @LINE:146
  private[this] lazy val controllers_CallInfoController_getFollowUpNotifyBeforeTime66_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/ajax/getFollowUpNotificationBeforeTime")))
  )
  private[this] lazy val controllers_CallInfoController_getFollowUpNotifyBeforeTime66_invoker = createInvoker(
    CallInfoController_26.getFollowUpNotifyBeforeTime(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInfoController",
      "getFollowUpNotifyBeforeTime",
      Nil,
      "GET",
      """""",
      this.prefix + """CRE/ajax/getFollowUpNotificationBeforeTime"""
    )
  )

  // @LINE:147
  private[this] lazy val controllers_CallInfoController_getFollowUpRemainderOfMissedSchedules67_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/ajax/getFollowUpRemainderOfMissedSchedules")))
  )
  private[this] lazy val controllers_CallInfoController_getFollowUpRemainderOfMissedSchedules67_invoker = createInvoker(
    CallInfoController_26.getFollowUpRemainderOfMissedSchedules(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInfoController",
      "getFollowUpRemainderOfMissedSchedules",
      Nil,
      "GET",
      """""",
      this.prefix + """CRE/ajax/getFollowUpRemainderOfMissedSchedules"""
    )
  )

  // @LINE:148
  private[this] lazy val controllers_CallInfoController_getFollowUpNotificationToday68_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/ajax/getFollowUpNotificationOfToday")))
  )
  private[this] lazy val controllers_CallInfoController_getFollowUpNotificationToday68_invoker = createInvoker(
    CallInfoController_26.getFollowUpNotificationToday(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInfoController",
      "getFollowUpNotificationToday",
      Nil,
      "GET",
      """""",
      this.prefix + """CRE/ajax/getFollowUpNotificationOfToday"""
    )
  )

  // @LINE:149
  private[this] lazy val controllers_PSFController_getPSFFollowUpNotificationToday69_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/ajax/getPSFFollowupNotificationOfToday")))
  )
  private[this] lazy val controllers_PSFController_getPSFFollowUpNotificationToday69_invoker = createInvoker(
    PSFController_14.getPSFFollowUpNotificationToday(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PSFController",
      "getPSFFollowUpNotificationToday",
      Nil,
      "GET",
      """""",
      this.prefix + """CRE/ajax/getPSFFollowupNotificationOfToday"""
    )
  )

  // @LINE:156
  private[this] lazy val controllers_CallInfoController_deleteCallLog70_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/deleteCall/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInfoController_deleteCallLog70_invoker = createInvoker(
    CallInfoController_26.deleteCallLog(fakeValue[String], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInfoController",
      "deleteCallLog",
      Seq(classOf[String], classOf[Long]),
      "GET",
      """""",
      this.prefix + """CRE/deleteCall/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:157
  private[this] lazy val controllers_CallInfoController_deleteSchCalllog71_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/deleteSchCall/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInfoController_deleteSchCalllog71_invoker = createInvoker(
    CallInfoController_26.deleteSchCalllog(fakeValue[String], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInfoController",
      "deleteSchCalllog",
      Seq(classOf[String], classOf[Long]),
      "GET",
      """""",
      this.prefix + """CRE/deleteSchCall/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:161
  private[this] lazy val controllers_WyzUserController_indexPageSalesManager72_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("SalesManager")))
  )
  private[this] lazy val controllers_WyzUserController_indexPageSalesManager72_invoker = createInvoker(
    WyzUserController_11.indexPageSalesManager,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WyzUserController",
      "indexPageSalesManager",
      Nil,
      "GET",
      """""",
      this.prefix + """SalesManager"""
    )
  )

  // @LINE:163
  private[this] lazy val controllers_WyzUserController_viewReport73_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("SalesManager/viewReport")))
  )
  private[this] lazy val controllers_WyzUserController_viewReport73_invoker = createInvoker(
    WyzUserController_11.viewReport(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WyzUserController",
      "viewReport",
      Nil,
      "GET",
      """""",
      this.prefix + """SalesManager/viewReport"""
    )
  )

  // @LINE:168
  private[this] lazy val controllers_Assets_at74_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("assets/"), DynamicPart("file", """.+""",false)))
  )
  private[this] lazy val controllers_Assets_at74_invoker = createInvoker(
    Assets_23.at(fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Assets",
      "at",
      Seq(classOf[String], classOf[String]),
      "GET",
      """ Map static resources from the /public folder to the /assets URL path""",
      this.prefix + """assets/""" + "$" + """file<.+>"""
    )
  )

  // @LINE:176
  private[this] lazy val controllers_UserAuthenticator_authenticateUser75_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("users/authenticate/")))
  )
  private[this] lazy val controllers_UserAuthenticator_authenticateUser75_invoker = createInvoker(
    UserAuthenticator_16.authenticateUser(fakeValue[String], fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.UserAuthenticator",
      "authenticateUser",
      Seq(classOf[String], classOf[String], classOf[String]),
      "GET",
      """""",
      this.prefix + """users/authenticate/"""
    )
  )

  // @LINE:177
  private[this] lazy val controllers_CallInfoController_startSyncOperation76_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("callInfo/startSync")))
  )
  private[this] lazy val controllers_CallInfoController_startSyncOperation76_invoker = createInvoker(
    CallInfoController_26.startSyncOperation(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInfoController",
      "startSyncOperation",
      Nil,
      "GET",
      """""",
      this.prefix + """callInfo/startSync"""
    )
  )

  // @LINE:178
  private[this] lazy val controllers_CallInfoController_stopSyncOperation77_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("callInfo/stopSync")))
  )
  private[this] lazy val controllers_CallInfoController_stopSyncOperation77_invoker = createInvoker(
    CallInfoController_26.stopSyncOperation(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInfoController",
      "stopSyncOperation",
      Nil,
      "GET",
      """""",
      this.prefix + """callInfo/stopSync"""
    )
  )

  // @LINE:179
  private[this] lazy val controllers_UserAuthenticator_updateUserAuthentication78_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("updateUserAuthentication")))
  )
  private[this] lazy val controllers_UserAuthenticator_updateUserAuthentication78_invoker = createInvoker(
    UserAuthenticator_16.updateUserAuthentication(fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.UserAuthenticator",
      "updateUserAuthentication",
      Seq(classOf[String], classOf[String]),
      "GET",
      """""",
      this.prefix + """updateUserAuthentication"""
    )
  )

  // @LINE:181
  private[this] lazy val controllers_SearchController_startSyncOfReports79_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("service/startSyncReports")))
  )
  private[this] lazy val controllers_SearchController_startSyncOfReports79_invoker = createInvoker(
    SearchController_0.startSyncOfReports(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SearchController",
      "startSyncOfReports",
      Nil,
      "GET",
      """""",
      this.prefix + """service/startSyncReports"""
    )
  )

  // @LINE:182
  private[this] lazy val controllers_CallInfoController_todeleteFilesFromDirectory80_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("delete/allFilesFromTmp")))
  )
  private[this] lazy val controllers_CallInfoController_todeleteFilesFromDirectory80_invoker = createInvoker(
    CallInfoController_26.todeleteFilesFromDirectory(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInfoController",
      "todeleteFilesFromDirectory",
      Nil,
      "GET",
      """""",
      this.prefix + """delete/allFilesFromTmp"""
    )
  )

  // @LINE:186
  private[this] lazy val controllers_CustomerScheduledController_readCustomersFromCSV81_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("customers/readCustomersFromCSV")))
  )
  private[this] lazy val controllers_CustomerScheduledController_readCustomersFromCSV81_invoker = createInvoker(
    CustomerScheduledController_15.readCustomersFromCSV,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CustomerScheduledController",
      "readCustomersFromCSV",
      Nil,
      "POST",
      """""",
      this.prefix + """customers/readCustomersFromCSV"""
    )
  )

  // @LINE:188
  private[this] lazy val controllers_CustomerScheduledController_addcustomerInfo82_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/addcustomer")))
  )
  private[this] lazy val controllers_CustomerScheduledController_addcustomerInfo82_invoker = createInvoker(
    CustomerScheduledController_15.addcustomerInfo,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CustomerScheduledController",
      "addcustomerInfo",
      Nil,
      "POST",
      """""",
      this.prefix + """CREManager/addcustomer"""
    )
  )

  // @LINE:193
  private[this] lazy val controllers_ReportsController_getScheduledCallsCountCREMan83_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("ajax/getTtlSchCallsCREManager")))
  )
  private[this] lazy val controllers_ReportsController_getScheduledCallsCountCREMan83_invoker = createInvoker(
    ReportsController_19.getScheduledCallsCountCREMan,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ReportsController",
      "getScheduledCallsCountCREMan",
      Nil,
      "GET",
      """""",
      this.prefix + """ajax/getTtlSchCallsCREManager"""
    )
  )

  // @LINE:195
  private[this] lazy val controllers_ReportsController_getBookedListByTime84_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("ajax/getBookedListByTime")))
  )
  private[this] lazy val controllers_ReportsController_getBookedListByTime84_invoker = createInvoker(
    ReportsController_19.getBookedListByTime,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ReportsController",
      "getBookedListByTime",
      Nil,
      "GET",
      """""",
      this.prefix + """ajax/getBookedListByTime"""
    )
  )

  // @LINE:197
  private[this] lazy val controllers_ReportsController_getCallTypePie85_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("ajax/getCallTypePie")))
  )
  private[this] lazy val controllers_ReportsController_getCallTypePie85_invoker = createInvoker(
    ReportsController_19.getCallTypePie,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ReportsController",
      "getCallTypePie",
      Nil,
      "GET",
      """""",
      this.prefix + """ajax/getCallTypePie"""
    )
  )

  // @LINE:201
  private[this] lazy val controllers_CallInteractionController_getCustomerByInteractionId86_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("getCustomerByInteractionId")))
  )
  private[this] lazy val controllers_CallInteractionController_getCustomerByInteractionId86_invoker = createInvoker(
    CallInteractionController_12.getCustomerByInteractionId(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getCustomerByInteractionId",
      Nil,
      "GET",
      """""",
      this.prefix + """getCustomerByInteractionId"""
    )
  )

  // @LINE:205
  private[this] lazy val controllers_CallInteractionController_getAssignedInteraction87_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/UploadAllCalls")))
  )
  private[this] lazy val controllers_CallInteractionController_getAssignedInteraction87_invoker = createInvoker(
    CallInteractionController_12.getAssignedInteraction(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getAssignedInteraction",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager/UploadAllCalls"""
    )
  )

  // @LINE:206
  private[this] lazy val controllers_CallInteractionController_toUploadInteractionsByCREManager88_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/UploadAllCalls")))
  )
  private[this] lazy val controllers_CallInteractionController_toUploadInteractionsByCREManager88_invoker = createInvoker(
    CallInteractionController_12.toUploadInteractionsByCREManager(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "toUploadInteractionsByCREManager",
      Nil,
      "POST",
      """""",
      this.prefix + """CREManager/UploadAllCalls"""
    )
  )

  // @LINE:207
  private[this] lazy val controllers_CallInteractionController_assigningCallsToCRE89_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/assignCalls/"), DynamicPart("selectAgent", """[^/]+""",true), StaticPart("/"), DynamicPart("fromData", """[^/]+""",true), StaticPart("/"), DynamicPart("toDate", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_assigningCallsToCRE89_invoker = createInvoker(
    CallInteractionController_12.assigningCallsToCRE(fakeValue[String], fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "assigningCallsToCRE",
      Seq(classOf[String], classOf[String], classOf[String]),
      "GET",
      """""",
      this.prefix + """CRE/assignCalls/""" + "$" + """selectAgent<[^/]+>/""" + "$" + """fromData<[^/]+>/""" + "$" + """toDate<[^/]+>"""
    )
  )

  // @LINE:208
  private[this] lazy val controllers_CallInteractionController_getAssignedCallsOfUser90_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/getAssignedCalls/"), DynamicPart("selectAgent", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_getAssignedCallsOfUser90_invoker = createInvoker(
    CallInteractionController_12.getAssignedCallsOfUser(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getAssignedCallsOfUser",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """CRE/getAssignedCalls/""" + "$" + """selectAgent<[^/]+>"""
    )
  )

  // @LINE:212
  private[this] lazy val controllers_CallInteractionController_searchByCustomer91_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("searchByCustomer")))
  )
  private[this] lazy val controllers_CallInteractionController_searchByCustomer91_invoker = createInvoker(
    CallInteractionController_12.searchByCustomer(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "searchByCustomer",
      Nil,
      "GET",
      """search """,
      this.prefix + """searchByCustomer"""
    )
  )

  // @LINE:213
  private[this] lazy val controllers_CallInteractionController_addCustomerCRE92_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("addCustomer")))
  )
  private[this] lazy val controllers_CallInteractionController_addCustomerCRE92_invoker = createInvoker(
    CallInteractionController_12.addCustomerCRE(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "addCustomerCRE",
      Nil,
      "GET",
      """""",
      this.prefix + """addCustomer"""
    )
  )

  // @LINE:214
  private[this] lazy val controllers_SearchController_searchCustomer93_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("searchByCustomerv2")))
  )
  private[this] lazy val controllers_SearchController_searchCustomer93_invoker = createInvoker(
    SearchController_0.searchCustomer(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SearchController",
      "searchCustomer",
      Nil,
      "GET",
      """""",
      this.prefix + """searchByCustomerv2"""
    )
  )

  // @LINE:215
  private[this] lazy val controllers_SMSTemplateController_checkIfSearchNameExists94_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("checkIfSearchNameExists/"), DynamicPart("searchnamevalue", """[^/]+""",true)))
  )
  private[this] lazy val controllers_SMSTemplateController_checkIfSearchNameExists94_invoker = createInvoker(
    SMSTemplateController_2.checkIfSearchNameExists(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SMSTemplateController",
      "checkIfSearchNameExists",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """checkIfSearchNameExists/""" + "$" + """searchnamevalue<[^/]+>"""
    )
  )

  // @LINE:220
  private[this] lazy val controllers_CallInteractionController_postAddCustomer95_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("postAddCustomer")))
  )
  private[this] lazy val controllers_CallInteractionController_postAddCustomer95_invoker = createInvoker(
    CallInteractionController_12.postAddCustomer(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "postAddCustomer",
      Nil,
      "POST",
      """add customer
POST """,
      this.prefix + """postAddCustomer"""
    )
  )

  // @LINE:222
  private[this] lazy val controllers_CallInteractionController_getMediaFileCallInteractions96_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("getMediaFile/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_getMediaFileCallInteractions96_invoker = createInvoker(
    CallInteractionController_12.getMediaFileCallInteractions(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getMediaFileCallInteractions",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """getMediaFile/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:224
  private[this] lazy val controllers_CallInteractionController_getPageForAssigningCalls97_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/assignCall")))
  )
  private[this] lazy val controllers_CallInteractionController_getPageForAssigningCalls97_invoker = createInvoker(
    CallInteractionController_12.getPageForAssigningCalls(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getPageForAssigningCalls",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager/assignCall"""
    )
  )

  // @LINE:225
  private[this] lazy val controllers_CallInteractionController_getSelectedAssignCalls98_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/assignCall")))
  )
  private[this] lazy val controllers_CallInteractionController_getSelectedAssignCalls98_invoker = createInvoker(
    CallInteractionController_12.getSelectedAssignCalls(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getSelectedAssignCalls",
      Nil,
      "POST",
      """""",
      this.prefix + """CREManager/assignCall"""
    )
  )

  // @LINE:227
  private[this] lazy val controllers_CallInteractionController_getAssignedList99_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/getAssignList")))
  )
  private[this] lazy val controllers_CallInteractionController_getAssignedList99_invoker = createInvoker(
    CallInteractionController_12.getAssignedList(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getAssignedList",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager/getAssignList"""
    )
  )

  // @LINE:228
  private[this] lazy val controllers_CallInteractionController_toChangeAssigment100_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/changeAssignment")))
  )
  private[this] lazy val controllers_CallInteractionController_toChangeAssigment100_invoker = createInvoker(
    CallInteractionController_12.toChangeAssigment(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "toChangeAssigment",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager/changeAssignment"""
    )
  )

  // @LINE:229
  private[this] lazy val controllers_CallInteractionController_getReAssignmentCalls101_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/getReAssignmentCalls/"), DynamicPart("selectAgent", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_getReAssignmentCalls101_invoker = createInvoker(
    CallInteractionController_12.getReAssignmentCalls(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getReAssignmentCalls",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """CREManager/getReAssignmentCalls/""" + "$" + """selectAgent<[^/]+>"""
    )
  )

  // @LINE:230
  private[this] lazy val controllers_CallInteractionController_reAssigningCallsofselectCRE102_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/changeAssignment")))
  )
  private[this] lazy val controllers_CallInteractionController_reAssigningCallsofselectCRE102_invoker = createInvoker(
    CallInteractionController_12.reAssigningCallsofselectCRE(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "reAssigningCallsofselectCRE",
      Nil,
      "POST",
      """""",
      this.prefix + """CREManager/changeAssignment"""
    )
  )

  // @LINE:234
  private[this] lazy val controllers_CallInteractionController_upload_file_Format_show103_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/uploadExcelData")))
  )
  private[this] lazy val controllers_CallInteractionController_upload_file_Format_show103_invoker = createInvoker(
    CallInteractionController_12.upload_file_Format_show(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "upload_file_Format_show",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager/uploadExcelData"""
    )
  )

  // @LINE:235
  private[this] lazy val controllers_CallInteractionController_upload_file_Format_submit104_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/uploadExcelData")))
  )
  private[this] lazy val controllers_CallInteractionController_upload_file_Format_submit104_invoker = createInvoker(
    CallInteractionController_12.upload_file_Format_submit(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "upload_file_Format_submit",
      Nil,
      "POST",
      """""",
      this.prefix + """CREManager/uploadExcelData"""
    )
  )

  // @LINE:236
  private[this] lazy val controllers_CallInteractionController_ajax_master_data_upload_fileFormat105_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("uploadFormat/getFormElement/"), DynamicPart("selected_part", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_ajax_master_data_upload_fileFormat105_invoker = createInvoker(
    CallInteractionController_12.ajax_master_data_upload_fileFormat(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "ajax_master_data_upload_fileFormat",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """uploadFormat/getFormElement/""" + "$" + """selected_part<[^/]+>"""
    )
  )

  // @LINE:237
  private[this] lazy val controllers_CallInteractionController_upload_Excel_Sheet106_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/uploadExcelSheet/"), DynamicPart("uploadId", """[^/]+""",true), StaticPart("/"), DynamicPart("uploadType", """[^/]+""",true), StaticPart("/"), DynamicPart("resultData", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_upload_Excel_Sheet106_invoker = createInvoker(
    CallInteractionController_12.upload_Excel_Sheet(fakeValue[String], fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "upload_Excel_Sheet",
      Seq(classOf[String], classOf[String], classOf[String]),
      "GET",
      """""",
      this.prefix + """CREManager/uploadExcelSheet/""" + "$" + """uploadId<[^/]+>/""" + "$" + """uploadType<[^/]+>/""" + "$" + """resultData<[^/]+>"""
    )
  )

  // @LINE:238
  private[this] lazy val controllers_CallInteractionController_get_required_fields107_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("uploadFormat/MapMasterFormat/"), DynamicPart("selected_part", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_get_required_fields107_invoker = createInvoker(
    CallInteractionController_12.get_required_fields(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "get_required_fields",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """uploadFormat/MapMasterFormat/""" + "$" + """selected_part<[^/]+>"""
    )
  )

  // @LINE:239
  private[this] lazy val controllers_CallInteractionController_ajax_transaction_data_upload108_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("master/excelUpload/"), DynamicPart("selected_format", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_ajax_transaction_data_upload108_invoker = createInvoker(
    CallInteractionController_12.ajax_transaction_data_upload(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "ajax_transaction_data_upload",
      Seq(classOf[String]),
      "POST",
      """""",
      this.prefix + """master/excelUpload/""" + "$" + """selected_format<[^/]+>"""
    )
  )

  // @LINE:243
  private[this] lazy val controllers_CallInteractionController_getDownloadExcelFormat109_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/downloadExcelFormat")))
  )
  private[this] lazy val controllers_CallInteractionController_getDownloadExcelFormat109_invoker = createInvoker(
    CallInteractionController_12.getDownloadExcelFormat(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getDownloadExcelFormat",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager/downloadExcelFormat"""
    )
  )

  // @LINE:244
  private[this] lazy val controllers_CallInteractionController_getExcelColumnsOFUploadFormat110_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("getExcelColumns/"), DynamicPart("upload_format", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_getExcelColumnsOFUploadFormat110_invoker = createInvoker(
    CallInteractionController_12.getExcelColumnsOFUploadFormat(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getExcelColumnsOFUploadFormat",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """getExcelColumns/""" + "$" + """upload_format<[^/]+>"""
    )
  )

  // @LINE:248
  private[this] lazy val controllers_SearchController_uploadHistoryReport111_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/uploadDataReport/"), DynamicPart("uploadId", """[^/]+""",true), StaticPart("/"), DynamicPart("uploadType", """[^/]+""",true)))
  )
  private[this] lazy val controllers_SearchController_uploadHistoryReport111_invoker = createInvoker(
    SearchController_0.uploadHistoryReport(fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SearchController",
      "uploadHistoryReport",
      Seq(classOf[String], classOf[String]),
      "GET",
      """""",
      this.prefix + """CREManager/uploadDataReport/""" + "$" + """uploadId<[^/]+>/""" + "$" + """uploadType<[^/]+>"""
    )
  )

  // @LINE:249
  private[this] lazy val controllers_SearchController_uploadHistoryViewData112_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("uploadReport/"), DynamicPart("typeIds", """[^/]+""",true), StaticPart("/"), DynamicPart("fromDate", """[^/]+""",true), StaticPart("/"), DynamicPart("toDate", """[^/]+""",true), StaticPart("/"), DynamicPart("uploadReportId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_SearchController_uploadHistoryViewData112_invoker = createInvoker(
    SearchController_0.uploadHistoryViewData(fakeValue[String], fakeValue[String], fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SearchController",
      "uploadHistoryViewData",
      Seq(classOf[String], classOf[String], classOf[String], classOf[String]),
      "GET",
      """""",
      this.prefix + """uploadReport/""" + "$" + """typeIds<[^/]+>/""" + "$" + """fromDate<[^/]+>/""" + "$" + """toDate<[^/]+>/""" + "$" + """uploadReportId<[^/]+>"""
    )
  )

  // @LINE:252
  private[this] lazy val controllers_CallInteractionController_addComplaints113_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("complaints")))
  )
  private[this] lazy val controllers_CallInteractionController_addComplaints113_invoker = createInvoker(
    CallInteractionController_12.addComplaints(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "addComplaints",
      Nil,
      "GET",
      """Complaints""",
      this.prefix + """complaints"""
    )
  )

  // @LINE:253
  private[this] lazy val controllers_CallInteractionController_postComplaints114_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("postComplaints")))
  )
  private[this] lazy val controllers_CallInteractionController_postComplaints114_invoker = createInvoker(
    CallInteractionController_12.postComplaints(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "postComplaints",
      Nil,
      "POST",
      """""",
      this.prefix + """postComplaints"""
    )
  )

  // @LINE:255
  private[this] lazy val controllers_CallInteractionController_assignComplaints115_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/assignComplaints")))
  )
  private[this] lazy val controllers_CallInteractionController_assignComplaints115_invoker = createInvoker(
    CallInteractionController_12.assignComplaints(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "assignComplaints",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager/assignComplaints"""
    )
  )

  // @LINE:256
  private[this] lazy val controllers_CallInteractionController_updateComplaints116_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/updateComplaints/"), DynamicPart("id", """[^/]+""",true), StaticPart("/"), DynamicPart("comments", """[^/]+""",true), StaticPart("/"), DynamicPart("selected_value", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_updateComplaints116_invoker = createInvoker(
    CallInteractionController_12.updateComplaints(fakeValue[Long], fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "updateComplaints",
      Seq(classOf[Long], classOf[String], classOf[String]),
      "GET",
      """""",
      this.prefix + """CRE/updateComplaints/""" + "$" + """id<[^/]+>/""" + "$" + """comments<[^/]+>/""" + "$" + """selected_value<[^/]+>"""
    )
  )

  // @LINE:257
  private[this] lazy val controllers_CallInteractionController_addComplaintsByManager117_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("complaintsByManager")))
  )
  private[this] lazy val controllers_CallInteractionController_addComplaintsByManager117_invoker = createInvoker(
    CallInteractionController_12.addComplaintsByManager(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "addComplaintsByManager",
      Nil,
      "GET",
      """""",
      this.prefix + """complaintsByManager"""
    )
  )

  // @LINE:258
  private[this] lazy val controllers_CallInteractionController_postComplaintsByManager118_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("postComplaintsByManager")))
  )
  private[this] lazy val controllers_CallInteractionController_postComplaintsByManager118_invoker = createInvoker(
    CallInteractionController_12.postComplaintsByManager(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "postComplaintsByManager",
      Nil,
      "POST",
      """""",
      this.prefix + """postComplaintsByManager"""
    )
  )

  // @LINE:261
  private[this] lazy val controllers_FirebaseSyncController_startSyncOperationServiceAgent119_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/startSyncServiceAgent")))
  )
  private[this] lazy val controllers_FirebaseSyncController_startSyncOperationServiceAgent119_invoker = createInvoker(
    FirebaseSyncController_6.startSyncOperationServiceAgent(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FirebaseSyncController",
      "startSyncOperationServiceAgent",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager/startSyncServiceAgent"""
    )
  )

  // @LINE:262
  private[this] lazy val controllers_FirebaseSyncController_startServiceAdvisorHistorySync120_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/startSyncHistorySync")))
  )
  private[this] lazy val controllers_FirebaseSyncController_startServiceAdvisorHistorySync120_invoker = createInvoker(
    FirebaseSyncController_6.startServiceAdvisorHistorySync(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FirebaseSyncController",
      "startServiceAdvisorHistorySync",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager/startSyncHistorySync"""
    )
  )

  // @LINE:263
  private[this] lazy val controllers_FirebaseSyncController_startServiceAdvisorSummaryDetailSync121_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/startSyncSummarySync")))
  )
  private[this] lazy val controllers_FirebaseSyncController_startServiceAdvisorSummaryDetailSync121_invoker = createInvoker(
    FirebaseSyncController_6.startServiceAdvisorSummaryDetailSync(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FirebaseSyncController",
      "startServiceAdvisorSummaryDetailSync",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager/startSyncSummarySync"""
    )
  )

  // @LINE:265
  private[this] lazy val controllers_FirebaseSyncController_stopSyncOperationServiceAgent122_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/stopSyncServiceAgent")))
  )
  private[this] lazy val controllers_FirebaseSyncController_stopSyncOperationServiceAgent122_invoker = createInvoker(
    FirebaseSyncController_6.stopSyncOperationServiceAgent(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FirebaseSyncController",
      "stopSyncOperationServiceAgent",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager/stopSyncServiceAgent"""
    )
  )

  // @LINE:266
  private[this] lazy val controllers_FirebaseSyncController_stopServiceAdvisorHistorySync123_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/stopSyncHistorySync")))
  )
  private[this] lazy val controllers_FirebaseSyncController_stopServiceAdvisorHistorySync123_invoker = createInvoker(
    FirebaseSyncController_6.stopServiceAdvisorHistorySync(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FirebaseSyncController",
      "stopServiceAdvisorHistorySync",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager/stopSyncHistorySync"""
    )
  )

  // @LINE:267
  private[this] lazy val controllers_FirebaseSyncController_stopServiceAdvisorSummaryDetailSync124_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/stopSyncSummarySync")))
  )
  private[this] lazy val controllers_FirebaseSyncController_stopServiceAdvisorSummaryDetailSync124_invoker = createInvoker(
    FirebaseSyncController_6.stopServiceAdvisorSummaryDetailSync(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FirebaseSyncController",
      "stopServiceAdvisorSummaryDetailSync",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager/stopSyncSummarySync"""
    )
  )

  // @LINE:270
  private[this] lazy val controllers_FirebaseSyncController_startSyncOperationPSFCallServiceAgent125_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/startSyncPSFCall")))
  )
  private[this] lazy val controllers_FirebaseSyncController_startSyncOperationPSFCallServiceAgent125_invoker = createInvoker(
    FirebaseSyncController_6.startSyncOperationPSFCallServiceAgent(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FirebaseSyncController",
      "startSyncOperationPSFCallServiceAgent",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager/startSyncPSFCall"""
    )
  )

  // @LINE:271
  private[this] lazy val controllers_FirebaseSyncController_stopSyncOperationPSFCallServiceAgent126_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/stopSyncPSFCall")))
  )
  private[this] lazy val controllers_FirebaseSyncController_stopSyncOperationPSFCallServiceAgent126_invoker = createInvoker(
    FirebaseSyncController_6.stopSyncOperationPSFCallServiceAgent(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FirebaseSyncController",
      "stopSyncOperationPSFCallServiceAgent",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager/stopSyncPSFCall"""
    )
  )

  // @LINE:273
  private[this] lazy val controllers_FirebaseSyncController_startServiceAdvisorPSFHistorySync127_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/startSyncPSFHistory")))
  )
  private[this] lazy val controllers_FirebaseSyncController_startServiceAdvisorPSFHistorySync127_invoker = createInvoker(
    FirebaseSyncController_6.startServiceAdvisorPSFHistorySync(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FirebaseSyncController",
      "startServiceAdvisorPSFHistorySync",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager/startSyncPSFHistory"""
    )
  )

  // @LINE:276
  private[this] lazy val controllers_CallInteractionController_ajaxAddPhoneNumber128_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/addphone/"), DynamicPart("phone_no", """[^/]+""",true), StaticPart("/"), DynamicPart("customer_id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_ajaxAddPhoneNumber128_invoker = createInvoker(
    CallInteractionController_12.ajaxAddPhoneNumber(fakeValue[String], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "ajaxAddPhoneNumber",
      Seq(classOf[String], classOf[Long]),
      "GET",
      """ Add Phone Number""",
      this.prefix + """CRE/addphone/""" + "$" + """phone_no<[^/]+>/""" + "$" + """customer_id<[^/]+>"""
    )
  )

  // @LINE:277
  private[this] lazy val controllers_CallInteractionController_ajaxCallForAddcustomerinfo129_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/saveaddcustomerinfo")))
  )
  private[this] lazy val controllers_CallInteractionController_ajaxCallForAddcustomerinfo129_invoker = createInvoker(
    CallInteractionController_12.ajaxCallForAddcustomerinfo(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "ajaxCallForAddcustomerinfo",
      Nil,
      "POST",
      """""",
      this.prefix + """CRE/saveaddcustomerinfo"""
    )
  )

  // @LINE:281
  private[this] lazy val controllers_CallInteractionController_ajaxAddChassisno130_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/saveaddcustomerchassisno/"), DynamicPart("chassisNo", """[^/]+""",true), StaticPart("/"), DynamicPart("customer_id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_ajaxAddChassisno130_invoker = createInvoker(
    CallInteractionController_12.ajaxAddChassisno(fakeValue[String], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "ajaxAddChassisno",
      Seq(classOf[String], classOf[Long]),
      "GET",
      """""",
      this.prefix + """CRE/saveaddcustomerchassisno/""" + "$" + """chassisNo<[^/]+>/""" + "$" + """customer_id<[^/]+>"""
    )
  )

  // @LINE:282
  private[this] lazy val controllers_CallInteractionController_ajaxAddRegistrationno131_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/saveaddcustomervregistrationno/"), DynamicPart("vehicalRegNo", """[^/]+""",true), StaticPart("/"), DynamicPart("customer_id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_ajaxAddRegistrationno131_invoker = createInvoker(
    CallInteractionController_12.ajaxAddRegistrationno(fakeValue[String], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "ajaxAddRegistrationno",
      Seq(classOf[String], classOf[Long]),
      "GET",
      """""",
      this.prefix + """CRE/saveaddcustomervregistrationno/""" + "$" + """vehicalRegNo<[^/]+>/""" + "$" + """customer_id<[^/]+>"""
    )
  )

  // @LINE:283
  private[this] lazy val controllers_CallInteractionController_ajaxAddEngineno132_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/saveaddcustomerengineno/"), DynamicPart("engineNo", """[^/]+""",true), StaticPart("/"), DynamicPart("customer_id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_ajaxAddEngineno132_invoker = createInvoker(
    CallInteractionController_12.ajaxAddEngineno(fakeValue[String], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "ajaxAddEngineno",
      Seq(classOf[String], classOf[Long]),
      "GET",
      """""",
      this.prefix + """CRE/saveaddcustomerengineno/""" + "$" + """engineNo<[^/]+>/""" + "$" + """customer_id<[^/]+>"""
    )
  )

  // @LINE:285
  private[this] lazy val controllers_CallInteractionController_saveaddcustomermobno133_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/saveaddcustomermobno/"), DynamicPart("custMobNo", """[^/]+""",true), StaticPart("/"), DynamicPart("wyzUser_id", """[^/]+""",true), StaticPart("/"), DynamicPart("customer_Id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_saveaddcustomermobno133_invoker = createInvoker(
    CallInteractionController_12.saveaddcustomermobno(fakeValue[String], fakeValue[Long], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "saveaddcustomermobno",
      Seq(classOf[String], classOf[Long], classOf[Long]),
      "GET",
      """""",
      this.prefix + """CRE/saveaddcustomermobno/""" + "$" + """custMobNo<[^/]+>/""" + "$" + """wyzUser_id<[^/]+>/""" + "$" + """customer_Id<[^/]+>"""
    )
  )

  // @LINE:286
  private[this] lazy val controllers_CallInteractionController_saveaddcustomerEmail134_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/saveaddcustomerEmail/"), DynamicPart("custEmail", """[^/]+""",true), StaticPart("/"), DynamicPart("wyzUser_id", """[^/]+""",true), StaticPart("/"), DynamicPart("customer_Id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_saveaddcustomerEmail134_invoker = createInvoker(
    CallInteractionController_12.saveaddcustomerEmail(fakeValue[String], fakeValue[Long], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "saveaddcustomerEmail",
      Seq(classOf[String], classOf[Long], classOf[Long]),
      "GET",
      """""",
      this.prefix + """CRE/saveaddcustomerEmail/""" + "$" + """custEmail<[^/]+>/""" + "$" + """wyzUser_id<[^/]+>/""" + "$" + """customer_Id<[^/]+>"""
    )
  )

  // @LINE:294
  private[this] lazy val controllers_AllCallInteractionController_viewAllCallIntearctions135_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/viewCallInteractions")))
  )
  private[this] lazy val controllers_AllCallInteractionController_viewAllCallIntearctions135_invoker = createInvoker(
    AllCallInteractionController_4.viewAllCallIntearctions(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AllCallInteractionController",
      "viewAllCallIntearctions",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager/viewCallInteractions"""
    )
  )

  // @LINE:298
  private[this] lazy val controllers_AllCallInteractionController_getAllCallInteractions136_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/getAllCallHistoryData")))
  )
  private[this] lazy val controllers_AllCallInteractionController_getAllCallInteractions136_invoker = createInvoker(
    AllCallInteractionController_4.getAllCallInteractions(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AllCallInteractionController",
      "getAllCallInteractions",
      Nil,
      "POST",
      """POST   /CREManager/viewCallInteractionsDetails                                                                                                                          controllers.AllCallInteractionController.getAllCallInteractions_paging()
GET    /CREManager/downloadMediaFile                                                                                                                                    controllers.AllCallInteractionController.getRecording()
GET    /callInteractionFileDownload                                                                                                                                     controllers.AllCallInteractionController.getAllCallInteractions()""",
      this.prefix + """CREManager/getAllCallHistoryData"""
    )
  )

  // @LINE:299
  private[this] lazy val controllers_AllCallInteractionController_downloadCallHistoryReport137_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/downloadCallHistoryReport")))
  )
  private[this] lazy val controllers_AllCallInteractionController_downloadCallHistoryReport137_invoker = createInvoker(
    AllCallInteractionController_4.downloadCallHistoryReport(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AllCallInteractionController",
      "downloadCallHistoryReport",
      Nil,
      "POST",
      """""",
      this.prefix + """CREManager/downloadCallHistoryReport"""
    )
  )

  // @LINE:302
  private[this] lazy val controllers_CallInteractionController_getWorkShopServiceBooked138_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/getWorkshopServices/"), DynamicPart("workId", """[^/]+""",true), StaticPart("/"), DynamicPart("userId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_getWorkShopServiceBooked138_invoker = createInvoker(
    CallInteractionController_12.getWorkShopServiceBooked(fakeValue[Long], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getWorkShopServiceBooked",
      Seq(classOf[Long], classOf[Long]),
      "GET",
      """""",
      this.prefix + """CRE/getWorkshopServices/""" + "$" + """workId<[^/]+>/""" + "$" + """userId<[^/]+>"""
    )
  )

  // @LINE:306
  private[this] lazy val controllers_CallInteractionController_getSerAdvServiceBooked139_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/getServiceAdvisorServices/"), DynamicPart("serviceAdvisorId", """[^/]+""",true), StaticPart("/"), DynamicPart("userId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_getSerAdvServiceBooked139_invoker = createInvoker(
    CallInteractionController_12.getSerAdvServiceBooked(fakeValue[Long], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getSerAdvServiceBooked",
      Seq(classOf[Long], classOf[Long]),
      "GET",
      """""",
      this.prefix + """CRE/getServiceAdvisorServices/""" + "$" + """serviceAdvisorId<[^/]+>/""" + "$" + """userId<[^/]+>"""
    )
  )

  // @LINE:310
  private[this] lazy val controllers_CallInteractionController_getDriverServiceBooked140_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/getDriverServices/"), DynamicPart("driverId", """[^/]+""",true), StaticPart("/"), DynamicPart("userId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_getDriverServiceBooked140_invoker = createInvoker(
    CallInteractionController_12.getDriverServiceBooked(fakeValue[Long], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getDriverServiceBooked",
      Seq(classOf[Long], classOf[Long]),
      "GET",
      """""",
      this.prefix + """CRE/getDriverServices/""" + "$" + """driverId<[^/]+>/""" + "$" + """userId<[^/]+>"""
    )
  )

  // @LINE:311
  private[this] lazy val controllers_CallInteractionController_reAssignAgentUpdate141_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/reAssignAgent/"), DynamicPart("rowId", """[^/]+""",true), StaticPart("/"), DynamicPart("wyzUserId", """[^/]+""",true), StaticPart("/"), DynamicPart("post_id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_reAssignAgentUpdate141_invoker = createInvoker(
    CallInteractionController_12.reAssignAgentUpdate(fakeValue[Long], fakeValue[Long], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "reAssignAgentUpdate",
      Seq(classOf[Long], classOf[Long], classOf[Long]),
      "GET",
      """""",
      this.prefix + """CRE/reAssignAgent/""" + "$" + """rowId<[^/]+>/""" + "$" + """wyzUserId<[^/]+>/""" + "$" + """post_id<[^/]+>"""
    )
  )

  // @LINE:315
  private[this] lazy val controllers_CallInteractionController_getListWorkshopByLocation142_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/listWorkshops/"), DynamicPart("selectedCity", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_getListWorkshopByLocation142_invoker = createInvoker(
    CallInteractionController_12.getListWorkshopByLocation(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getListWorkshopByLocation",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """CRE/listWorkshops/""" + "$" + """selectedCity<[^/]+>"""
    )
  )

  // @LINE:320
  private[this] lazy val controllers_CallInteractionController_getWorkshopSummaryDetails143_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/listWorkshopSummary/"), DynamicPart("selectedWorkshop", """[^/]+""",true), StaticPart("/"), DynamicPart("schDate", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_getWorkshopSummaryDetails143_invoker = createInvoker(
    CallInteractionController_12.getWorkshopSummaryDetails(fakeValue[Long], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getWorkshopSummaryDetails",
      Seq(classOf[Long], classOf[String]),
      "GET",
      """Workshop summary by workshop id""",
      this.prefix + """CRE/listWorkshopSummary/""" + "$" + """selectedWorkshop<[^/]+>/""" + "$" + """schDate<[^/]+>"""
    )
  )

  // @LINE:324
  private[this] lazy val controllers_CallInteractionController_ajaxsearchVehicle144_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/searchvehRegNo/"), DynamicPart("veh_number", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_ajaxsearchVehicle144_invoker = createInvoker(
    CallInteractionController_12.ajaxsearchVehicle(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "ajaxsearchVehicle",
      Seq(classOf[String]),
      "GET",
      """complaints modified""",
      this.prefix + """CRE/searchvehRegNo/""" + "$" + """veh_number<[^/]+>"""
    )
  )

  // @LINE:325
  private[this] lazy val controllers_CallInteractionController_complaintsResolution145_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("complaintResolution")))
  )
  private[this] lazy val controllers_CallInteractionController_complaintsResolution145_invoker = createInvoker(
    CallInteractionController_12.complaintsResolution(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "complaintsResolution",
      Nil,
      "GET",
      """""",
      this.prefix + """complaintResolution"""
    )
  )

  // @LINE:326
  private[this] lazy val controllers_CallInteractionController_ajaxcomplaintNumber146_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/searchcomplaintNum/"), DynamicPart("complaintNum", """[^/]+""",true), StaticPart("/"), DynamicPart("veh_num", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_ajaxcomplaintNumber146_invoker = createInvoker(
    CallInteractionController_12.ajaxcomplaintNumber(fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "ajaxcomplaintNumber",
      Seq(classOf[String], classOf[String]),
      "GET",
      """""",
      this.prefix + """CRE/searchcomplaintNum/""" + "$" + """complaintNum<[^/]+>/""" + "$" + """veh_num<[^/]+>"""
    )
  )

  // @LINE:327
  private[this] lazy val controllers_CallInteractionController_ajaxcomplaintNumberClosed147_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/searchcomplaintNumClosed/"), DynamicPart("complaintNumClosed", """[^/]+""",true), StaticPart("/"), DynamicPart("veh_numclosed", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_ajaxcomplaintNumberClosed147_invoker = createInvoker(
    CallInteractionController_12.ajaxcomplaintNumberClosed(fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "ajaxcomplaintNumberClosed",
      Seq(classOf[String], classOf[String]),
      "GET",
      """""",
      this.prefix + """CRE/searchcomplaintNumClosed/""" + "$" + """complaintNumClosed<[^/]+>/""" + "$" + """veh_numclosed<[^/]+>"""
    )
  )

  // @LINE:329
  private[this] lazy val controllers_CallInteractionController_updateComplaintsResolution148_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/updateComplaintsResolution/"), DynamicPart("complaintNum", """[^/]+""",true), StaticPart("/"), DynamicPart("reasonFor", """[^/]+""",true), StaticPart("/"), DynamicPart("complaintStatus", """[^/]+""",true), StaticPart("/"), DynamicPart("customerStatus", """[^/]+""",true), StaticPart("/"), DynamicPart("actionTaken", """[^/]+""",true), StaticPart("/"), DynamicPart("resolutionBy", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_updateComplaintsResolution148_invoker = createInvoker(
    CallInteractionController_12.updateComplaintsResolution(fakeValue[String], fakeValue[String], fakeValue[String], fakeValue[String], fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "updateComplaintsResolution",
      Seq(classOf[String], classOf[String], classOf[String], classOf[String], classOf[String], classOf[String]),
      "GET",
      """""",
      this.prefix + """CRE/updateComplaintsResolution/""" + "$" + """complaintNum<[^/]+>/""" + "$" + """reasonFor<[^/]+>/""" + "$" + """complaintStatus<[^/]+>/""" + "$" + """customerStatus<[^/]+>/""" + "$" + """actionTaken<[^/]+>/""" + "$" + """resolutionBy<[^/]+>"""
    )
  )

  // @LINE:330
  private[this] lazy val controllers_CallInteractionController_updateComplaintsResolutionByManager149_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/updateComplaintsResolutionByManager/"), DynamicPart("complaintNum", """[^/]+""",true), StaticPart("/"), DynamicPart("reasonFor", """[^/]+""",true), StaticPart("/"), DynamicPart("complaintStatus", """[^/]+""",true), StaticPart("/"), DynamicPart("customerStatus", """[^/]+""",true), StaticPart("/"), DynamicPart("actionTaken", """[^/]+""",true), StaticPart("/"), DynamicPart("resolutionBy", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_updateComplaintsResolutionByManager149_invoker = createInvoker(
    CallInteractionController_12.updateComplaintsResolutionByManager(fakeValue[String], fakeValue[String], fakeValue[String], fakeValue[String], fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "updateComplaintsResolutionByManager",
      Seq(classOf[String], classOf[String], classOf[String], classOf[String], classOf[String], classOf[String]),
      "GET",
      """""",
      this.prefix + """CRE/updateComplaintsResolutionByManager/""" + "$" + """complaintNum<[^/]+>/""" + "$" + """reasonFor<[^/]+>/""" + "$" + """complaintStatus<[^/]+>/""" + "$" + """customerStatus<[^/]+>/""" + "$" + """actionTaken<[^/]+>/""" + "$" + """resolutionBy<[^/]+>"""
    )
  )

  // @LINE:331
  private[this] lazy val controllers_CallInteractionController_updateComplaintsResolutionClosed150_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/updateComplaintsResolutionClosed/"), DynamicPart("complaintNumClosed", """[^/]+""",true), StaticPart("/"), DynamicPart("reasonForClosed", """[^/]+""",true), StaticPart("/"), DynamicPart("complaintStatusClosed", """[^/]+""",true), StaticPart("/"), DynamicPart("customerStatusClosed", """[^/]+""",true), StaticPart("/"), DynamicPart("actionTakenClosed", """[^/]+""",true), StaticPart("/"), DynamicPart("resolutionByClosed", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_updateComplaintsResolutionClosed150_invoker = createInvoker(
    CallInteractionController_12.updateComplaintsResolutionClosed(fakeValue[String], fakeValue[String], fakeValue[String], fakeValue[String], fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "updateComplaintsResolutionClosed",
      Seq(classOf[String], classOf[String], classOf[String], classOf[String], classOf[String], classOf[String]),
      "GET",
      """""",
      this.prefix + """CRE/updateComplaintsResolutionClosed/""" + "$" + """complaintNumClosed<[^/]+>/""" + "$" + """reasonForClosed<[^/]+>/""" + "$" + """complaintStatusClosed<[^/]+>/""" + "$" + """customerStatusClosed<[^/]+>/""" + "$" + """actionTakenClosed<[^/]+>/""" + "$" + """resolutionByClosed<[^/]+>"""
    )
  )

  // @LINE:333
  private[this] lazy val controllers_CallInteractionController_complaintAssignment151_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/complaintAssignment/"), DynamicPart("complaintNum", """[^/]+""",true), StaticPart("/"), DynamicPart("veh_num", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_complaintAssignment151_invoker = createInvoker(
    CallInteractionController_12.complaintAssignment(fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "complaintAssignment",
      Seq(classOf[String], classOf[String]),
      "GET",
      """""",
      this.prefix + """CRE/complaintAssignment/""" + "$" + """complaintNum<[^/]+>/""" + "$" + """veh_num<[^/]+>"""
    )
  )

  // @LINE:336
  private[this] lazy val controllers_CallInteractionController_saveaddComplaintAssignModile152_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/saveaddComplaintAssignModile/"), DynamicPart("complaintNum", """[^/]+""",true), StaticPart("/"), DynamicPart("city", """[^/]+""",true), StaticPart("/"), DynamicPart("workshop", """[^/]+""",true), StaticPart("/"), DynamicPart("functions", """[^/]+""",true), StaticPart("/"), DynamicPart("ownership", """[^/]+""",true), StaticPart("/"), DynamicPart("priority", """[^/]+""",true), StaticPart("/"), DynamicPart("esclation1", """[^/]+""",true), StaticPart("/"), DynamicPart("esclation2", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_saveaddComplaintAssignModile152_invoker = createInvoker(
    CallInteractionController_12.saveaddComplaintAssignModile(fakeValue[String], fakeValue[String], fakeValue[String], fakeValue[String], fakeValue[String], fakeValue[String], fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "saveaddComplaintAssignModile",
      Seq(classOf[String], classOf[String], classOf[String], classOf[String], classOf[String], classOf[String], classOf[String], classOf[String]),
      "GET",
      """""",
      this.prefix + """CRE/saveaddComplaintAssignModile/""" + "$" + """complaintNum<[^/]+>/""" + "$" + """city<[^/]+>/""" + "$" + """workshop<[^/]+>/""" + "$" + """functions<[^/]+>/""" + "$" + """ownership<[^/]+>/""" + "$" + """priority<[^/]+>/""" + "$" + """esclation1<[^/]+>/""" + "$" + """esclation2<[^/]+>"""
    )
  )

  // @LINE:337
  private[this] lazy val controllers_CallInteractionController_viewAllComplaints1153_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/viewComplaints1")))
  )
  private[this] lazy val controllers_CallInteractionController_viewAllComplaints1153_invoker = createInvoker(
    CallInteractionController_12.viewAllComplaints1(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "viewAllComplaints1",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager/viewComplaints1"""
    )
  )

  // @LINE:340
  private[this] lazy val controllers_CallInteractionController_downloadExcelFile154_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/viewComplaints1")))
  )
  private[this] lazy val controllers_CallInteractionController_downloadExcelFile154_invoker = createInvoker(
    CallInteractionController_12.downloadExcelFile(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "downloadExcelFile",
      Nil,
      "POST",
      """Complaint download file""",
      this.prefix + """CREManager/viewComplaints1"""
    )
  )

  // @LINE:343
  private[this] lazy val controllers_CallInteractionController_getComplaintsDataByFilter155_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/getComplaintsDataByFilter/"), DynamicPart("filterData", """[^/]+""",true), StaticPart("/"), DynamicPart("varLoc", """[^/]+""",true), StaticPart("/"), DynamicPart("varfunc", """[^/]+""",true), StaticPart("/"), DynamicPart("varraisedDate", """[^/]+""",true), StaticPart("/"), DynamicPart("varendDate", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_getComplaintsDataByFilter155_invoker = createInvoker(
    CallInteractionController_12.getComplaintsDataByFilter(fakeValue[String], fakeValue[String], fakeValue[String], fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getComplaintsDataByFilter",
      Seq(classOf[String], classOf[String], classOf[String], classOf[String], classOf[String]),
      "GET",
      """feb 3rd complaints all in one Manager""",
      this.prefix + """CREManager/getComplaintsDataByFilter/""" + "$" + """filterData<[^/]+>/""" + "$" + """varLoc<[^/]+>/""" + "$" + """varfunc<[^/]+>/""" + "$" + """varraisedDate<[^/]+>/""" + "$" + """varendDate<[^/]+>"""
    )
  )

  // @LINE:344
  private[this] lazy val controllers_CallInteractionController_getComplaintHistoryAll156_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/getComplaintHistoryAll/"), DynamicPart("complaintNumber", """[^/]+""",true), StaticPart("/"), DynamicPart("vehregnumber", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_getComplaintHistoryAll156_invoker = createInvoker(
    CallInteractionController_12.getComplaintHistoryAll(fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getComplaintHistoryAll",
      Seq(classOf[String], classOf[String]),
      "GET",
      """""",
      this.prefix + """CREManager/getComplaintHistoryAll/""" + "$" + """complaintNumber<[^/]+>/""" + "$" + """vehregnumber<[^/]+>"""
    )
  )

  // @LINE:348
  private[this] lazy val controllers_CallInteractionController_updateRangeOfUnavailabiltyOfUsers157_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/updateRangeOfUnavailabilty")))
  )
  private[this] lazy val controllers_CallInteractionController_updateRangeOfUnavailabiltyOfUsers157_invoker = createInvoker(
    CallInteractionController_12.updateRangeOfUnavailabiltyOfUsers(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "updateRangeOfUnavailabiltyOfUsers",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager/updateRangeOfUnavailabilty"""
    )
  )

  // @LINE:349
  private[this] lazy val controllers_CallInteractionController_postUpdateRangeOfUnavailabiltyOfUsers158_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/updateRangeOfUnavailabilty")))
  )
  private[this] lazy val controllers_CallInteractionController_postUpdateRangeOfUnavailabiltyOfUsers158_invoker = createInvoker(
    CallInteractionController_12.postUpdateRangeOfUnavailabiltyOfUsers,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "postUpdateRangeOfUnavailabiltyOfUsers",
      Nil,
      "POST",
      """""",
      this.prefix + """CREManager/updateRangeOfUnavailabilty"""
    )
  )

  // @LINE:351
  private[this] lazy val controllers_CallInteractionController_getRoasterTable159_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/todaysRoaster")))
  )
  private[this] lazy val controllers_CallInteractionController_getRoasterTable159_invoker = createInvoker(
    CallInteractionController_12.getRoasterTable(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getRoasterTable",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager/todaysRoaster"""
    )
  )

  // @LINE:352
  private[this] lazy val controllers_CallInteractionController_toUpdateRoasterUnAvailablity160_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/todaysRoaster")))
  )
  private[this] lazy val controllers_CallInteractionController_toUpdateRoasterUnAvailablity160_invoker = createInvoker(
    CallInteractionController_12.toUpdateRoasterUnAvailablity(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "toUpdateRoasterUnAvailablity",
      Nil,
      "POST",
      """""",
      this.prefix + """CREManager/todaysRoaster"""
    )
  )

  // @LINE:354
  private[this] lazy val controllers_CallInteractionController_rosterOfUnavailabiltyByUser161_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/rosterOfUnavailabiltyByUser")))
  )
  private[this] lazy val controllers_CallInteractionController_rosterOfUnavailabiltyByUser161_invoker = createInvoker(
    CallInteractionController_12.rosterOfUnavailabiltyByUser(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "rosterOfUnavailabiltyByUser",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager/rosterOfUnavailabiltyByUser"""
    )
  )

  // @LINE:355
  private[this] lazy val controllers_CallInteractionController_addRosterOfUserByAjax162_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/addRosterOfUserByAjax/"), DynamicPart("selectAgent", """[^/]+""",true), StaticPart("/"), DynamicPart("fromDate", """[^/]+""",true), StaticPart("/"), DynamicPart("toDate", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_addRosterOfUserByAjax162_invoker = createInvoker(
    CallInteractionController_12.addRosterOfUserByAjax(fakeValue[String], fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "addRosterOfUserByAjax",
      Seq(classOf[String], classOf[String], classOf[String]),
      "GET",
      """""",
      this.prefix + """CREManager/addRosterOfUserByAjax/""" + "$" + """selectAgent<[^/]+>/""" + "$" + """fromDate<[^/]+>/""" + "$" + """toDate<[^/]+>"""
    )
  )

  // @LINE:357
  private[this] lazy val controllers_CallInteractionController_getRosterDataByUser163_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/loadRosterData/"), DynamicPart("selectAgent", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_getRosterDataByUser163_invoker = createInvoker(
    CallInteractionController_12.getRosterDataByUser(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getRosterDataByUser",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """CREManager/loadRosterData/""" + "$" + """selectAgent<[^/]+>"""
    )
  )

  // @LINE:358
  private[this] lazy val controllers_CallInteractionController_updateRosterOfUserByAjaxVal164_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/updateRosterOfUserByAjaxData/"), DynamicPart("selectAgent", """[^/]+""",true), StaticPart("/"), DynamicPart("From_Date", """[^/]+""",true), StaticPart("/"), DynamicPart("To_Date", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_updateRosterOfUserByAjaxVal164_invoker = createInvoker(
    CallInteractionController_12.updateRosterOfUserByAjaxVal(fakeValue[String], fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "updateRosterOfUserByAjaxVal",
      Seq(classOf[String], classOf[String], classOf[String]),
      "GET",
      """""",
      this.prefix + """CREManager/updateRosterOfUserByAjaxData/""" + "$" + """selectAgent<[^/]+>/""" + "$" + """From_Date<[^/]+>/""" + "$" + """To_Date<[^/]+>"""
    )
  )

  // @LINE:359
  private[this] lazy val controllers_CallInteractionController_deleteUnavialbility165_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/deleteUnavialbility/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_deleteUnavialbility165_invoker = createInvoker(
    CallInteractionController_12.deleteUnavialbility(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "deleteUnavialbility",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """CREManager/deleteUnavialbility/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:362
  private[this] lazy val controllers_CallInteractionController_getServiceDataOfCustomer166_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/serviceDataOfCustomer/"), DynamicPart("customerId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_getServiceDataOfCustomer166_invoker = createInvoker(
    CallInteractionController_12.getServiceDataOfCustomer(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getServiceDataOfCustomer",
      Seq(classOf[Long]),
      "GET",
      """Disposition page data load""",
      this.prefix + """CRE/serviceDataOfCustomer/""" + "$" + """customerId<[^/]+>"""
    )
  )

  // @LINE:363
  private[this] lazy val controllers_CallInteractionController_getCallHistoryOfvehicalId167_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/getCallHistoryOfvehicalId/"), DynamicPart("vehicalId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_getCallHistoryOfvehicalId167_invoker = createInvoker(
    CallInteractionController_12.getCallHistoryOfvehicalId(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getCallHistoryOfvehicalId",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """CRE/getCallHistoryOfvehicalId/""" + "$" + """vehicalId<[^/]+>"""
    )
  )

  // @LINE:364
  private[this] lazy val controllers_CallInteractionController_getServiceAdvisorOfWorkshop168_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/listServiceAdvisors/"), DynamicPart("workshopId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_getServiceAdvisorOfWorkshop168_invoker = createInvoker(
    CallInteractionController_12.getServiceAdvisorOfWorkshop(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getServiceAdvisorOfWorkshop",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """CRE/listServiceAdvisors/""" + "$" + """workshopId<[^/]+>"""
    )
  )

  // @LINE:365
  private[this] lazy val controllers_CallInteractionController_getWorkshopListName169_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/listWorkshopsIfExisting/"), DynamicPart("workshopId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_getWorkshopListName169_invoker = createInvoker(
    CallInteractionController_12.getWorkshopListName(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getWorkshopListName",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """CRE/listWorkshopsIfExisting/""" + "$" + """workshopId<[^/]+>"""
    )
  )

  // @LINE:366
  private[this] lazy val controllers_CallInteractionController_getLeadByUserLocation170_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/leadBasedOnLocation/"), DynamicPart("userLocation", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_getLeadByUserLocation170_invoker = createInvoker(
    CallInteractionController_12.getLeadByUserLocation(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getLeadByUserLocation",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """CRE/leadBasedOnLocation/""" + "$" + """userLocation<[^/]+>"""
    )
  )

  // @LINE:367
  private[this] lazy val controllers_CallInteractionController_getLeadTagByDepartment171_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/leadBasedOnDepartment/"), DynamicPart("userLocation", """[^/]+""",true), StaticPart("/"), DynamicPart("departmentName", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_getLeadTagByDepartment171_invoker = createInvoker(
    CallInteractionController_12.getLeadTagByDepartment(fakeValue[Long], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getLeadTagByDepartment",
      Seq(classOf[Long], classOf[Long]),
      "GET",
      """""",
      this.prefix + """CRE/leadBasedOnDepartment/""" + "$" + """userLocation<[^/]+>/""" + "$" + """departmentName<[^/]+>"""
    )
  )

  // @LINE:368
  private[this] lazy val controllers_CallInteractionController_getTagNameByUpselLeadType172_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/upselLeadIdTag/"), DynamicPart("userLocation", """[^/]+""",true), StaticPart("/"), DynamicPart("upselIDTag", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_getTagNameByUpselLeadType172_invoker = createInvoker(
    CallInteractionController_12.getTagNameByUpselLeadType(fakeValue[Long], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getTagNameByUpselLeadType",
      Seq(classOf[Long], classOf[Long]),
      "GET",
      """""",
      this.prefix + """CRE/upselLeadIdTag/""" + "$" + """userLocation<[^/]+>/""" + "$" + """upselIDTag<[^/]+>"""
    )
  )

  // @LINE:369
  private[this] lazy val controllers_CallInteractionController_getComplaintHistoryVeh173_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/getComplaintHistoryOfvehicalId/"), DynamicPart("vehicalId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_getComplaintHistoryVeh173_invoker = createInvoker(
    CallInteractionController_12.getComplaintHistoryVeh(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getComplaintHistoryVeh",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """CRE/getComplaintHistoryOfvehicalId/""" + "$" + """vehicalId<[^/]+>"""
    )
  )

  // @LINE:370
  private[this] lazy val controllers_CallInteractionController_getSMSHistoryOfCustomer174_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/getSMSHistoryOfCustomerId/"), DynamicPart("customerId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_getSMSHistoryOfCustomer174_invoker = createInvoker(
    CallInteractionController_12.getSMSHistoryOfCustomer(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getSMSHistoryOfCustomer",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """CRE/getSMSHistoryOfCustomerId/""" + "$" + """customerId<[^/]+>"""
    )
  )

  // @LINE:378
  private[this] lazy val controllers_SearchController_assignedInteractionData175_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("assignedInteractionTableData")))
  )
  private[this] lazy val controllers_SearchController_assignedInteractionData175_invoker = createInvoker(
    SearchController_0.assignedInteractionData(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SearchController",
      "assignedInteractionData",
      Nil,
      "GET",
      """""",
      this.prefix + """assignedInteractionTableData"""
    )
  )

  // @LINE:379
  private[this] lazy val controllers_SearchController_followUpRequiredInteractionData176_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("followUpCallLogTableData")))
  )
  private[this] lazy val controllers_SearchController_followUpRequiredInteractionData176_invoker = createInvoker(
    SearchController_0.followUpRequiredInteractionData(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SearchController",
      "followUpRequiredInteractionData",
      Nil,
      "GET",
      """""",
      this.prefix + """followUpCallLogTableData"""
    )
  )

  // @LINE:380
  private[this] lazy val controllers_SearchController_serviceBookedInteractionData177_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("serviceBookedServerDataTable")))
  )
  private[this] lazy val controllers_SearchController_serviceBookedInteractionData177_invoker = createInvoker(
    SearchController_0.serviceBookedInteractionData(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SearchController",
      "serviceBookedInteractionData",
      Nil,
      "GET",
      """""",
      this.prefix + """serviceBookedServerDataTable"""
    )
  )

  // @LINE:381
  private[this] lazy val controllers_SearchController_serviceNotRequiredInteractionData178_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("serviceNotRequiredServerDataTable")))
  )
  private[this] lazy val controllers_SearchController_serviceNotRequiredInteractionData178_invoker = createInvoker(
    SearchController_0.serviceNotRequiredInteractionData(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SearchController",
      "serviceNotRequiredInteractionData",
      Nil,
      "GET",
      """""",
      this.prefix + """serviceNotRequiredServerDataTable"""
    )
  )

  // @LINE:382
  private[this] lazy val controllers_SearchController_nonContactsInteractionData179_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("nonContactsServerDataTable")))
  )
  private[this] lazy val controllers_SearchController_nonContactsInteractionData179_invoker = createInvoker(
    SearchController_0.nonContactsInteractionData(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SearchController",
      "nonContactsInteractionData",
      Nil,
      "GET",
      """""",
      this.prefix + """nonContactsServerDataTable"""
    )
  )

  // @LINE:383
  private[this] lazy val controllers_SearchController_droppedCallInteractionData180_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("droppedCallsServerDataTable")))
  )
  private[this] lazy val controllers_SearchController_droppedCallInteractionData180_invoker = createInvoker(
    SearchController_0.droppedCallInteractionData(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SearchController",
      "droppedCallInteractionData",
      Nil,
      "GET",
      """""",
      this.prefix + """droppedCallsServerDataTable"""
    )
  )

  // @LINE:387
  private[this] lazy val controllers_UploadExcelController_uploadExcelData181_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/uploadExcelPOST")))
  )
  private[this] lazy val controllers_UploadExcelController_uploadExcelData181_invoker = createInvoker(
    UploadExcelController_24.uploadExcelData(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.UploadExcelController",
      "uploadExcelData",
      Nil,
      "POST",
      """""",
      this.prefix + """CREManager/uploadExcelPOST"""
    )
  )

  // @LINE:389
  private[this] lazy val controllers_FirebaseSyncController_startSyncOperationDriverPickupDropList182_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("driverData")))
  )
  private[this] lazy val controllers_FirebaseSyncController_startSyncOperationDriverPickupDropList182_invoker = createInvoker(
    FirebaseSyncController_6.startSyncOperationDriverPickupDropList(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FirebaseSyncController",
      "startSyncOperationDriverPickupDropList",
      Nil,
      "GET",
      """""",
      this.prefix + """driverData"""
    )
  )

  // @LINE:393
  private[this] lazy val controllers_WyzUserController_indexPageServiceAdvisor183_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("ServiceAdvisor")))
  )
  private[this] lazy val controllers_WyzUserController_indexPageServiceAdvisor183_invoker = createInvoker(
    WyzUserController_11.indexPageServiceAdvisor(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WyzUserController",
      "indexPageServiceAdvisor",
      Nil,
      "GET",
      """""",
      this.prefix + """ServiceAdvisor"""
    )
  )

  // @LINE:395
  private[this] lazy val controllers_WyzUserController_changepasswordServiceAdvisor184_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("ServiceAdvisor/changepassword")))
  )
  private[this] lazy val controllers_WyzUserController_changepasswordServiceAdvisor184_invoker = createInvoker(
    WyzUserController_11.changepasswordServiceAdvisor,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WyzUserController",
      "changepasswordServiceAdvisor",
      Nil,
      "GET",
      """""",
      this.prefix + """ServiceAdvisor/changepassword"""
    )
  )

  // @LINE:396
  private[this] lazy val controllers_WyzUserController_changepasswordeditingServiceAdvisor185_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("ServiceAdvisor/passwordchange")))
  )
  private[this] lazy val controllers_WyzUserController_changepasswordeditingServiceAdvisor185_invoker = createInvoker(
    WyzUserController_11.changepasswordeditingServiceAdvisor(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WyzUserController",
      "changepasswordeditingServiceAdvisor",
      Nil,
      "POST",
      """""",
      this.prefix + """ServiceAdvisor/passwordchange"""
    )
  )

  // @LINE:400
  private[this] lazy val controllers_WyzUserController_indexPageOthers186_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("OthersLogins")))
  )
  private[this] lazy val controllers_WyzUserController_indexPageOthers186_invoker = createInvoker(
    WyzUserController_11.indexPageOthers(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WyzUserController",
      "indexPageOthers",
      Nil,
      "GET",
      """""",
      this.prefix + """OthersLogins"""
    )
  )

  // @LINE:403
  private[this] lazy val controllers_CampaignController_addCampaign187_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("addcampaign")))
  )
  private[this] lazy val controllers_CampaignController_addCampaign187_invoker = createInvoker(
    CampaignController_8.addCampaign(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CampaignController",
      "addCampaign",
      Nil,
      "GET",
      """new functionality for campaign added""",
      this.prefix + """addcampaign"""
    )
  )

  // @LINE:404
  private[this] lazy val controllers_CampaignController_postCampaign188_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("postCampaign")))
  )
  private[this] lazy val controllers_CampaignController_postCampaign188_invoker = createInvoker(
    CampaignController_8.postCampaign(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CampaignController",
      "postCampaign",
      Nil,
      "POST",
      """""",
      this.prefix + """postCampaign"""
    )
  )

  // @LINE:412
  private[this] lazy val controllers_PSFController_getPSFList189_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/PSFList")))
  )
  private[this] lazy val controllers_PSFController_getPSFList189_invoker = createInvoker(
    PSFController_14.getPSFList(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PSFController",
      "getPSFList",
      Nil,
      "GET",
      """GET         /CRE             controllers.WyzUserController.indexPageServiceAdvisor()""",
      this.prefix + """CRE/PSFList"""
    )
  )

  // @LINE:414
  private[this] lazy val controllers_PSFController_getPSF15List190_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/PSF15List")))
  )
  private[this] lazy val controllers_PSFController_getPSF15List190_invoker = createInvoker(
    PSFController_14.getPSF15List(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PSFController",
      "getPSF15List",
      Nil,
      "GET",
      """""",
      this.prefix + """CRE/PSF15List"""
    )
  )

  // @LINE:416
  private[this] lazy val controllers_PSFController_getPSF30List191_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/PSF30List")))
  )
  private[this] lazy val controllers_PSFController_getPSF30List191_invoker = createInvoker(
    PSFController_14.getPSF30List(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PSFController",
      "getPSF30List",
      Nil,
      "GET",
      """""",
      this.prefix + """CRE/PSF30List"""
    )
  )

  // @LINE:418
  private[this] lazy val controllers_PSFController_getPSF3rdDayList192_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/PSF3RDList")))
  )
  private[this] lazy val controllers_PSFController_getPSF3rdDayList192_invoker = createInvoker(
    PSFController_14.getPSF3rdDayList(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PSFController",
      "getPSF3rdDayList",
      Nil,
      "GET",
      """""",
      this.prefix + """CRE/PSF3RDList"""
    )
  )

  // @LINE:420
  private[this] lazy val controllers_PSFController_getPSFNextDayList193_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/PSFNextDayList")))
  )
  private[this] lazy val controllers_PSFController_getPSFNextDayList193_invoker = createInvoker(
    PSFController_14.getPSFNextDayList(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PSFController",
      "getPSFNextDayList",
      Nil,
      "GET",
      """""",
      this.prefix + """CRE/PSFNextDayList"""
    )
  )

  // @LINE:422
  private[this] lazy val controllers_PSFController_getPSF4thDayList194_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/PSF4thDayList")))
  )
  private[this] lazy val controllers_PSFController_getPSF4thDayList194_invoker = createInvoker(
    PSFController_14.getPSF4thDayList(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PSFController",
      "getPSF4thDayList",
      Nil,
      "GET",
      """""",
      this.prefix + """CRE/PSF4thDayList"""
    )
  )

  // @LINE:426
  private[this] lazy val controllers_PSFController_assignedInteractionPSFData195_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("assignedInteractionTablePSFData/"), DynamicPart("name", """[^/]+""",true)))
  )
  private[this] lazy val controllers_PSFController_assignedInteractionPSFData195_invoker = createInvoker(
    PSFController_14.assignedInteractionPSFData(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PSFController",
      "assignedInteractionPSFData",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """assignedInteractionTablePSFData/""" + "$" + """name<[^/]+>"""
    )
  )

  // @LINE:427
  private[this] lazy val controllers_PSFController_ajaxCallForFollowUpRequiredPSFData196_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("ajaxCallForFollowUpRequiredPSFData/"), DynamicPart("psfDay", """[^/]+""",true)))
  )
  private[this] lazy val controllers_PSFController_ajaxCallForFollowUpRequiredPSFData196_invoker = createInvoker(
    PSFController_14.ajaxCallForFollowUpRequiredPSFData(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PSFController",
      "ajaxCallForFollowUpRequiredPSFData",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """ajaxCallForFollowUpRequiredPSFData/""" + "$" + """psfDay<[^/]+>"""
    )
  )

  // @LINE:428
  private[this] lazy val controllers_PSFController_ajaxCallForIncompletedSurveyPSFData197_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("ajaxCallForIncompletedSurveyPSFData/"), DynamicPart("psfDay", """[^/]+""",true)))
  )
  private[this] lazy val controllers_PSFController_ajaxCallForIncompletedSurveyPSFData197_invoker = createInvoker(
    PSFController_14.ajaxCallForIncompletedSurveyPSFData(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PSFController",
      "ajaxCallForIncompletedSurveyPSFData",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """ajaxCallForIncompletedSurveyPSFData/""" + "$" + """psfDay<[^/]+>"""
    )
  )

  // @LINE:429
  private[this] lazy val controllers_PSFController_ajaxCallForCompletedSurveyPSFData198_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("ajaxCallForCompletedSurveyPSFData/"), DynamicPart("psfDay", """[^/]+""",true)))
  )
  private[this] lazy val controllers_PSFController_ajaxCallForCompletedSurveyPSFData198_invoker = createInvoker(
    PSFController_14.ajaxCallForCompletedSurveyPSFData(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PSFController",
      "ajaxCallForCompletedSurveyPSFData",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """ajaxCallForCompletedSurveyPSFData/""" + "$" + """psfDay<[^/]+>"""
    )
  )

  // @LINE:430
  private[this] lazy val controllers_PSFController_ajaxCallForDissatisfiedPSFData199_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("ajaxCallForDissatisfiedPSFData/"), DynamicPart("psfDay", """[^/]+""",true)))
  )
  private[this] lazy val controllers_PSFController_ajaxCallForDissatisfiedPSFData199_invoker = createInvoker(
    PSFController_14.ajaxCallForDissatisfiedPSFData(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PSFController",
      "ajaxCallForDissatisfiedPSFData",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """ajaxCallForDissatisfiedPSFData/""" + "$" + """psfDay<[^/]+>"""
    )
  )

  // @LINE:431
  private[this] lazy val controllers_PSFController_ajaxCallForNonContactsPSFData200_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("ajaxCallForNonContactsPSFData/"), DynamicPart("psfDay", """[^/]+""",true)))
  )
  private[this] lazy val controllers_PSFController_ajaxCallForNonContactsPSFData200_invoker = createInvoker(
    PSFController_14.ajaxCallForNonContactsPSFData(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PSFController",
      "ajaxCallForNonContactsPSFData",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """ajaxCallForNonContactsPSFData/""" + "$" + """psfDay<[^/]+>"""
    )
  )

  // @LINE:432
  private[this] lazy val controllers_PSFController_ajaxCallForDroppedCallsPSFData201_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("ajaxCallForDroppedCallsPSFData/"), DynamicPart("psfDay", """[^/]+""",true)))
  )
  private[this] lazy val controllers_PSFController_ajaxCallForDroppedCallsPSFData201_invoker = createInvoker(
    PSFController_14.ajaxCallForDroppedCallsPSFData(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PSFController",
      "ajaxCallForDroppedCallsPSFData",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """ajaxCallForDroppedCallsPSFData/""" + "$" + """psfDay<[^/]+>"""
    )
  )

  // @LINE:433
  private[this] lazy val controllers_PSFController_ajaxCallForAppointmentPSFData202_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("ajaxCallForAppointmentPSFData/"), DynamicPart("psfDay", """[^/]+""",true)))
  )
  private[this] lazy val controllers_PSFController_ajaxCallForAppointmentPSFData202_invoker = createInvoker(
    PSFController_14.ajaxCallForAppointmentPSFData(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PSFController",
      "ajaxCallForAppointmentPSFData",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """ajaxCallForAppointmentPSFData/""" + "$" + """psfDay<[^/]+>"""
    )
  )

  // @LINE:436
  private[this] lazy val controllers_SearchControllerMR_assignedInteractionDataMR203_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("assignedInteractionTableDataMR/"), DynamicPart("CREIds", """[^/]+""",true)))
  )
  private[this] lazy val controllers_SearchControllerMR_assignedInteractionDataMR203_invoker = createInvoker(
    SearchControllerMR_10.assignedInteractionDataMR(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SearchControllerMR",
      "assignedInteractionDataMR",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """assignedInteractionTableDataMR/""" + "$" + """CREIds<[^/]+>"""
    )
  )

  // @LINE:437
  private[this] lazy val controllers_SearchControllerMR_followUpRequiredInteractionDataMR204_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("followUpCallLogTableDataMR/"), DynamicPart("CREIds", """[^/]+""",true)))
  )
  private[this] lazy val controllers_SearchControllerMR_followUpRequiredInteractionDataMR204_invoker = createInvoker(
    SearchControllerMR_10.followUpRequiredInteractionDataMR(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SearchControllerMR",
      "followUpRequiredInteractionDataMR",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """followUpCallLogTableDataMR/""" + "$" + """CREIds<[^/]+>"""
    )
  )

  // @LINE:438
  private[this] lazy val controllers_SearchControllerMR_serviceBookedInteractionDataMR205_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("serviceBookedServerDataTableMR/"), DynamicPart("CREIds", """[^/]+""",true)))
  )
  private[this] lazy val controllers_SearchControllerMR_serviceBookedInteractionDataMR205_invoker = createInvoker(
    SearchControllerMR_10.serviceBookedInteractionDataMR(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SearchControllerMR",
      "serviceBookedInteractionDataMR",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """serviceBookedServerDataTableMR/""" + "$" + """CREIds<[^/]+>"""
    )
  )

  // @LINE:439
  private[this] lazy val controllers_SearchControllerMR_serviceNotRequiredInteractionDataMR206_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("serviceNotRequiredServerDataTableMR/"), DynamicPart("CREIds", """[^/]+""",true)))
  )
  private[this] lazy val controllers_SearchControllerMR_serviceNotRequiredInteractionDataMR206_invoker = createInvoker(
    SearchControllerMR_10.serviceNotRequiredInteractionDataMR(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SearchControllerMR",
      "serviceNotRequiredInteractionDataMR",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """serviceNotRequiredServerDataTableMR/""" + "$" + """CREIds<[^/]+>"""
    )
  )

  // @LINE:440
  private[this] lazy val controllers_SearchControllerMR_nonContactsInteractionDataMR207_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("nonContactsServerDataTableMR/"), DynamicPart("CREIds", """[^/]+""",true)))
  )
  private[this] lazy val controllers_SearchControllerMR_nonContactsInteractionDataMR207_invoker = createInvoker(
    SearchControllerMR_10.nonContactsInteractionDataMR(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SearchControllerMR",
      "nonContactsInteractionDataMR",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """nonContactsServerDataTableMR/""" + "$" + """CREIds<[^/]+>"""
    )
  )

  // @LINE:441
  private[this] lazy val controllers_SearchControllerMR_droppedCallInteractionDataMR208_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("droppedCallsServerDataTableMR/"), DynamicPart("CREIds", """[^/]+""",true)))
  )
  private[this] lazy val controllers_SearchControllerMR_droppedCallInteractionDataMR208_invoker = createInvoker(
    SearchControllerMR_10.droppedCallInteractionDataMR(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SearchControllerMR",
      "droppedCallInteractionDataMR",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """droppedCallsServerDataTableMR/""" + "$" + """CREIds<[^/]+>"""
    )
  )

  // @LINE:442
  private[this] lazy val controllers_SearchControllerMR_missedCallInteractionDataMR209_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("missedCallsServerDataTableMR/"), DynamicPart("CREIds", """[^/]+""",true)))
  )
  private[this] lazy val controllers_SearchControllerMR_missedCallInteractionDataMR209_invoker = createInvoker(
    SearchControllerMR_10.missedCallInteractionDataMR(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SearchControllerMR",
      "missedCallInteractionDataMR",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """missedCallsServerDataTableMR/""" + "$" + """CREIds<[^/]+>"""
    )
  )

  // @LINE:443
  private[this] lazy val controllers_SearchControllerMR_incomingCallInteractionDataMR210_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("incomingCallsServerDataTableMR/"), DynamicPart("CREIds", """[^/]+""",true)))
  )
  private[this] lazy val controllers_SearchControllerMR_incomingCallInteractionDataMR210_invoker = createInvoker(
    SearchControllerMR_10.incomingCallInteractionDataMR(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SearchControllerMR",
      "incomingCallInteractionDataMR",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """incomingCallsServerDataTableMR/""" + "$" + """CREIds<[^/]+>"""
    )
  )

  // @LINE:444
  private[this] lazy val controllers_SearchControllerMR_outgoingCallsServerDataTableMR211_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("outgoingCallsServerDataTableMR/"), DynamicPart("CREIds", """[^/]+""",true)))
  )
  private[this] lazy val controllers_SearchControllerMR_outgoingCallsServerDataTableMR211_invoker = createInvoker(
    SearchControllerMR_10.outgoingCallsServerDataTableMR(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SearchControllerMR",
      "outgoingCallsServerDataTableMR",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """outgoingCallsServerDataTableMR/""" + "$" + """CREIds<[^/]+>"""
    )
  )

  // @LINE:446
  private[this] lazy val controllers_SearchControllerMR_getMediaFileMR212_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/downloadMediaFile/"), DynamicPart("callId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_SearchControllerMR_getMediaFileMR212_invoker = createInvoker(
    SearchControllerMR_10.getMediaFileMR(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SearchControllerMR",
      "getMediaFileMR",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """CREManager/downloadMediaFile/""" + "$" + """callId<[^/]+>"""
    )
  )

  // @LINE:451
  private[this] lazy val controllers_CallInteractionController_getExistingVehicleRegCount213_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/vehicleRegNoExist/"), DynamicPart("vehicleReg", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_getExistingVehicleRegCount213_invoker = createInvoker(
    CallInteractionController_12.getExistingVehicleRegCount(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getExistingVehicleRegCount",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """CRE/vehicleRegNoExist/""" + "$" + """vehicleReg<[^/]+>"""
    )
  )

  // @LINE:452
  private[this] lazy val controllers_SearchControllerMR_getDashboardCount214_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/dashboardCounts")))
  )
  private[this] lazy val controllers_SearchControllerMR_getDashboardCount214_invoker = createInvoker(
    SearchControllerMR_10.getDashboardCount(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SearchControllerMR",
      "getDashboardCount",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager/dashboardCounts"""
    )
  )

  // @LINE:453
  private[this] lazy val controllers_SearchController_getDashboardCountCRE215_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/dashboardCounts")))
  )
  private[this] lazy val controllers_SearchController_getDashboardCountCRE215_invoker = createInvoker(
    SearchController_0.getDashboardCountCRE(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SearchController",
      "getDashboardCountCRE",
      Nil,
      "GET",
      """""",
      this.prefix + """CRE/dashboardCounts"""
    )
  )

  // @LINE:457
  private[this] lazy val controllers_CallInteractionController_sendCustomSMSAjax216_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/sendSMS/"), DynamicPart("rerenceNumber", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_sendCustomSMSAjax216_invoker = createInvoker(
    CallInteractionController_12.sendCustomSMSAjax(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "sendCustomSMSAjax",
      Seq(classOf[Long]),
      "POST",
      """""",
      this.prefix + """CRE/sendSMS/""" + "$" + """rerenceNumber<[^/]+>"""
    )
  )

  // @LINE:458
  private[this] lazy val controllers_CallInteractionController_getDriverListBasedOnworkshop217_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/listDrivers/"), DynamicPart("workshopId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_getDriverListBasedOnworkshop217_invoker = createInvoker(
    CallInteractionController_12.getDriverListBasedOnworkshop(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getDriverListBasedOnworkshop",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """CRE/listDrivers/""" + "$" + """workshopId<[^/]+>"""
    )
  )

  // @LINE:459
  private[this] lazy val controllers_CallInteractionController_getCRESListBasedOnWorkshop218_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/listCREsByWorkshop/"), DynamicPart("workshopId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_getCRESListBasedOnWorkshop218_invoker = createInvoker(
    CallInteractionController_12.getCRESListBasedOnWorkshop(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getCRESListBasedOnWorkshop",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """CRE/listCREsByWorkshop/""" + "$" + """workshopId<[^/]+>"""
    )
  )

  // @LINE:460
  private[this] lazy val controllers_CallInteractionController_getCREListBasedOnWorkshopCallHistory219_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/listCREsByWorkshopcallhistory/"), DynamicPart("workshopId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_getCREListBasedOnWorkshopCallHistory219_invoker = createInvoker(
    CallInteractionController_12.getCREListBasedOnWorkshopCallHistory(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getCREListBasedOnWorkshopCallHistory",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """CRE/listCREsByWorkshopcallhistory/""" + "$" + """workshopId<[^/]+>"""
    )
  )

  // @LINE:464
  private[this] lazy val controllers_ServiceAdvisorController_getServiceAdvisorComplaints220_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("ServiceAdvisor/resolveComplaint")))
  )
  private[this] lazy val controllers_ServiceAdvisorController_getServiceAdvisorComplaints220_invoker = createInvoker(
    ServiceAdvisorController_20.getServiceAdvisorComplaints(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ServiceAdvisorController",
      "getServiceAdvisorComplaints",
      Nil,
      "GET",
      """""",
      this.prefix + """ServiceAdvisor/resolveComplaint"""
    )
  )

  // @LINE:468
  private[this] lazy val controllers_AutoSelectionSAController_ajaxAutoSASelection221_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/callDispositionPage/"), DynamicPart("workshopId", """[^/]+""",true), StaticPart("/"), DynamicPart("date", """[^/]+""",true)))
  )
  private[this] lazy val controllers_AutoSelectionSAController_ajaxAutoSASelection221_invoker = createInvoker(
    AutoSelectionSAController_29.ajaxAutoSASelection(fakeValue[Long], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AutoSelectionSAController",
      "ajaxAutoSASelection",
      Seq(classOf[Long], classOf[String]),
      "GET",
      """""",
      this.prefix + """CRE/callDispositionPage/""" + "$" + """workshopId<[^/]+>/""" + "$" + """date<[^/]+>"""
    )
  )

  // @LINE:469
  private[this] lazy val controllers_AutoSelectionSAController_ajaxAutoSASelectionList222_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/callDispositionPage/"), DynamicPart("saDetails", """[^/]+""",true)))
  )
  private[this] lazy val controllers_AutoSelectionSAController_ajaxAutoSASelectionList222_invoker = createInvoker(
    AutoSelectionSAController_29.ajaxAutoSASelectionList(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AutoSelectionSAController",
      "ajaxAutoSASelectionList",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """CRE/callDispositionPage/""" + "$" + """saDetails<[^/]+>"""
    )
  )

  // @LINE:470
  private[this] lazy val controllers_AutoSelectionSAController_ajaxupdateSaChange223_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/callDispositionPage/"), DynamicPart("workshopId", """[^/]+""",true), StaticPart("/"), DynamicPart("date", """[^/]+""",true), StaticPart("/"), DynamicPart("preSaDetails", """[^/]+""",true), StaticPart("/"), DynamicPart("newSaDetails", """[^/]+""",true)))
  )
  private[this] lazy val controllers_AutoSelectionSAController_ajaxupdateSaChange223_invoker = createInvoker(
    AutoSelectionSAController_29.ajaxupdateSaChange(fakeValue[Long], fakeValue[String], fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AutoSelectionSAController",
      "ajaxupdateSaChange",
      Seq(classOf[Long], classOf[String], classOf[String], classOf[String]),
      "GET",
      """""",
      this.prefix + """CRE/callDispositionPage/""" + "$" + """workshopId<[^/]+>/""" + "$" + """date<[^/]+>/""" + "$" + """preSaDetails<[^/]+>/""" + "$" + """newSaDetails<[^/]+>"""
    )
  )

  // @LINE:474
  private[this] lazy val controllers_AutoSelInsuranceAgentController_ajaxAutoInsurAgentSelection224_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/callDispositionPageIns/"), DynamicPart("date", """[^/]+""",true)))
  )
  private[this] lazy val controllers_AutoSelInsuranceAgentController_ajaxAutoInsurAgentSelection224_invoker = createInvoker(
    AutoSelInsuranceAgentController_5.ajaxAutoInsurAgentSelection(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AutoSelInsuranceAgentController",
      "ajaxAutoInsurAgentSelection",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """CRE/callDispositionPageIns/""" + "$" + """date<[^/]+>"""
    )
  )

  // @LINE:475
  private[this] lazy val controllers_AutoSelInsuranceAgentController_ajaxAutoInsurAgentSelectionList225_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/callDispositionPageIns/"), DynamicPart("saDetails", """[^/]+""",true)))
  )
  private[this] lazy val controllers_AutoSelInsuranceAgentController_ajaxAutoInsurAgentSelectionList225_invoker = createInvoker(
    AutoSelInsuranceAgentController_5.ajaxAutoInsurAgentSelectionList(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AutoSelInsuranceAgentController",
      "ajaxAutoInsurAgentSelectionList",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """CRE/callDispositionPageIns/""" + "$" + """saDetails<[^/]+>"""
    )
  )

  // @LINE:476
  private[this] lazy val controllers_AutoSelInsuranceAgentController_ajaxupdateInsuAgentChange226_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/callDispositionPageIns/"), DynamicPart("date", """[^/]+""",true), StaticPart("/"), DynamicPart("preSaDetails", """[^/]+""",true), StaticPart("/"), DynamicPart("newSaDetails", """[^/]+""",true)))
  )
  private[this] lazy val controllers_AutoSelInsuranceAgentController_ajaxupdateInsuAgentChange226_invoker = createInvoker(
    AutoSelInsuranceAgentController_5.ajaxupdateInsuAgentChange(fakeValue[String], fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AutoSelInsuranceAgentController",
      "ajaxupdateInsuAgentChange",
      Seq(classOf[String], classOf[String], classOf[String]),
      "GET",
      """""",
      this.prefix + """CRE/callDispositionPageIns/""" + "$" + """date<[^/]+>/""" + "$" + """preSaDetails<[^/]+>/""" + "$" + """newSaDetails<[^/]+>"""
    )
  )

  // @LINE:482
  private[this] lazy val controllers_CallInfoController_getUpsellLeadsSeletedInLastSB227_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/upsellSelectedLastSB/"), DynamicPart("sr_int_id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInfoController_getUpsellLeadsSeletedInLastSB227_invoker = createInvoker(
    CallInfoController_26.getUpsellLeadsSeletedInLastSB(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInfoController",
      "getUpsellLeadsSeletedInLastSB",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """CRE/upsellSelectedLastSB/""" + "$" + """sr_int_id<[^/]+>"""
    )
  )

  // @LINE:486
  private[this] lazy val controllers_InsuranceController_getCommonCallDispositionForm228_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/getDispositionFormPage/"), DynamicPart("cid", """[^/]+""",true), StaticPart("/"), DynamicPart("vehicle_id", """[^/]+""",true), StaticPart("/"), DynamicPart("typeDispo", """[^/]+""",true)))
  )
  private[this] lazy val controllers_InsuranceController_getCommonCallDispositionForm228_invoker = createInvoker(
    InsuranceController_25.getCommonCallDispositionForm(fakeValue[Long], fakeValue[Long], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.InsuranceController",
      "getCommonCallDispositionForm",
      Seq(classOf[Long], classOf[Long], classOf[String]),
      "GET",
      """""",
      this.prefix + """CRE/getDispositionFormPage/""" + "$" + """cid<[^/]+>/""" + "$" + """vehicle_id<[^/]+>/""" + "$" + """typeDispo<[^/]+>"""
    )
  )

  // @LINE:487
  private[this] lazy val controllers_InsuranceController_postCommonCallDispositionForm229_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/getDispositionPageOfTab")))
  )
  private[this] lazy val controllers_InsuranceController_postCommonCallDispositionForm229_invoker = createInvoker(
    InsuranceController_25.postCommonCallDispositionForm(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.InsuranceController",
      "postCommonCallDispositionForm",
      Nil,
      "POST",
      """""",
      this.prefix + """CRE/getDispositionPageOfTab"""
    )
  )

  // @LINE:488
  private[this] lazy val controllers_InsuranceController_getAllInsuranceAssignedList230_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/insuranceAssignedList")))
  )
  private[this] lazy val controllers_InsuranceController_getAllInsuranceAssignedList230_invoker = createInvoker(
    InsuranceController_25.getAllInsuranceAssignedList(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.InsuranceController",
      "getAllInsuranceAssignedList",
      Nil,
      "GET",
      """""",
      this.prefix + """CRE/insuranceAssignedList"""
    )
  )

  // @LINE:489
  private[this] lazy val controllers_InsuranceController_getAssignedInsuInteraction231_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("insurance/assignedInsuInteraction")))
  )
  private[this] lazy val controllers_InsuranceController_getAssignedInsuInteraction231_invoker = createInvoker(
    InsuranceController_25.getAssignedInsuInteraction(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.InsuranceController",
      "getAssignedInsuInteraction",
      Nil,
      "GET",
      """""",
      this.prefix + """insurance/assignedInsuInteraction"""
    )
  )

  // @LINE:490
  private[this] lazy val controllers_InsuranceController_getContactedDispoFormData232_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("insurance/contactedDispoInteractions/"), DynamicPart("typeOfdispo", """[^/]+""",true)))
  )
  private[this] lazy val controllers_InsuranceController_getContactedDispoFormData232_invoker = createInvoker(
    InsuranceController_25.getContactedDispoFormData(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.InsuranceController",
      "getContactedDispoFormData",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """insurance/contactedDispoInteractions/""" + "$" + """typeOfdispo<[^/]+>"""
    )
  )

  // @LINE:491
  private[this] lazy val controllers_InsuranceController_getNonContactDroppedInsuranceDispoData233_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("insurance/nonContactedDispoInterac/"), DynamicPart("typeOfdispo", """[^/]+""",true)))
  )
  private[this] lazy val controllers_InsuranceController_getNonContactDroppedInsuranceDispoData233_invoker = createInvoker(
    InsuranceController_25.getNonContactDroppedInsuranceDispoData(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.InsuranceController",
      "getNonContactDroppedInsuranceDispoData",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """insurance/nonContactedDispoInterac/""" + "$" + """typeOfdispo<[^/]+>"""
    )
  )

  // @LINE:495
  private[this] lazy val controllers_PSFController_getCommonPSFDispositionPage234_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/psfDispo/"), DynamicPart("cid", """[^/]+""",true), StaticPart("/"), DynamicPart("vehicle_id", """[^/]+""",true), StaticPart("/"), DynamicPart("interactionid", """[^/]+""",true), StaticPart("/"), DynamicPart("dispositionHistory", """[^/]+""",true), StaticPart("/"), DynamicPart("typeOfPSF", """[^/]+""",true)))
  )
  private[this] lazy val controllers_PSFController_getCommonPSFDispositionPage234_invoker = createInvoker(
    PSFController_14.getCommonPSFDispositionPage(fakeValue[Long], fakeValue[Long], fakeValue[Long], fakeValue[Long], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PSFController",
      "getCommonPSFDispositionPage",
      Seq(classOf[Long], classOf[Long], classOf[Long], classOf[Long], classOf[Long]),
      "GET",
      """""",
      this.prefix + """CRE/psfDispo/""" + "$" + """cid<[^/]+>/""" + "$" + """vehicle_id<[^/]+>/""" + "$" + """interactionid<[^/]+>/""" + "$" + """dispositionHistory<[^/]+>/""" + "$" + """typeOfPSF<[^/]+>"""
    )
  )

  // @LINE:496
  private[this] lazy val controllers_PSFController_postCommonPSFDispositionPage235_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/psfDispo")))
  )
  private[this] lazy val controllers_PSFController_postCommonPSFDispositionPage235_invoker = createInvoker(
    PSFController_14.postCommonPSFDispositionPage(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PSFController",
      "postCommonPSFDispositionPage",
      Nil,
      "POST",
      """""",
      this.prefix + """CRE/psfDispo"""
    )
  )

  // @LINE:498
  private[this] lazy val controllers_InsuranceController_upload_Excel_Sheet_insurance236_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/uploadExcelSheetInsurance/"), DynamicPart("uploadId", """[^/]+""",true), StaticPart("/"), DynamicPart("uploadType", """[^/]+""",true), StaticPart("/"), DynamicPart("resultData", """[^/]+""",true)))
  )
  private[this] lazy val controllers_InsuranceController_upload_Excel_Sheet_insurance236_invoker = createInvoker(
    InsuranceController_25.upload_Excel_Sheet_insurance(fakeValue[String], fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.InsuranceController",
      "upload_Excel_Sheet_insurance",
      Seq(classOf[String], classOf[String], classOf[String]),
      "GET",
      """""",
      this.prefix + """CREManager/uploadExcelSheetInsurance/""" + "$" + """uploadId<[^/]+>/""" + "$" + """uploadType<[^/]+>/""" + "$" + """resultData<[^/]+>"""
    )
  )

  // @LINE:500
  private[this] lazy val controllers_InsuranceController_getPageForAssigningCallsInsurance237_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/assignCallInsurance")))
  )
  private[this] lazy val controllers_InsuranceController_getPageForAssigningCallsInsurance237_invoker = createInvoker(
    InsuranceController_25.getPageForAssigningCallsInsurance(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.InsuranceController",
      "getPageForAssigningCallsInsurance",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager/assignCallInsurance"""
    )
  )

  // @LINE:501
  private[this] lazy val controllers_InsuranceController_postAssignCallInsurance238_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/assignCallInsurance")))
  )
  private[this] lazy val controllers_InsuranceController_postAssignCallInsurance238_invoker = createInvoker(
    InsuranceController_25.postAssignCallInsurance(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.InsuranceController",
      "postAssignCallInsurance",
      Nil,
      "POST",
      """""",
      this.prefix + """CREManager/assignCallInsurance"""
    )
  )

  // @LINE:505
  private[this] lazy val controllers_InsuranceController_getODPercentage239_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/getODPercentage/"), DynamicPart("cubicCap", """[^/]+""",true), StaticPart("/"), DynamicPart("vehAge", """[^/]+""",true), StaticPart("/"), DynamicPart("zoneid", """[^/]+""",true)))
  )
  private[this] lazy val controllers_InsuranceController_getODPercentage239_invoker = createInvoker(
    InsuranceController_25.getODPercentage(fakeValue[String], fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.InsuranceController",
      "getODPercentage",
      Seq(classOf[String], classOf[String], classOf[String]),
      "GET",
      """""",
      this.prefix + """CRE/getODPercentage/""" + "$" + """cubicCap<[^/]+>/""" + "$" + """vehAge<[^/]+>/""" + "$" + """zoneid<[^/]+>"""
    )
  )

  // @LINE:506
  private[this] lazy val controllers_InsuranceController_getBasicODVaue240_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/getBasicODValue/"), DynamicPart("odvValue", """[^/]+""",true), StaticPart("/"), DynamicPart("idvValue", """[^/]+""",true)))
  )
  private[this] lazy val controllers_InsuranceController_getBasicODVaue240_invoker = createInvoker(
    InsuranceController_25.getBasicODVaue(fakeValue[Double], fakeValue[Double]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.InsuranceController",
      "getBasicODVaue",
      Seq(classOf[Double], classOf[Double]),
      "GET",
      """""",
      this.prefix + """CRE/getBasicODValue/""" + "$" + """odvValue<[^/]+>/""" + "$" + """idvValue<[^/]+>"""
    )
  )

  // @LINE:507
  private[this] lazy val controllers_InsuranceController_getNCBValueByBasicValue241_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/getNCBBYBasicODValue/"), DynamicPart("ncbPercenVal", """[^/]+""",true), StaticPart("/"), DynamicPart("basicODValue", """[^/]+""",true)))
  )
  private[this] lazy val controllers_InsuranceController_getNCBValueByBasicValue241_invoker = createInvoker(
    InsuranceController_25.getNCBValueByBasicValue(fakeValue[Double], fakeValue[Double]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.InsuranceController",
      "getNCBValueByBasicValue",
      Seq(classOf[Double], classOf[Double]),
      "GET",
      """""",
      this.prefix + """CRE/getNCBBYBasicODValue/""" + "$" + """ncbPercenVal<[^/]+>/""" + "$" + """basicODValue<[^/]+>"""
    )
  )

  // @LINE:508
  private[this] lazy val controllers_InsuranceController_gettotalPremiumAndDiscValue242_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/getDiscValueVyODPremium/"), DynamicPart("odPremiumVaue", """[^/]+""",true), StaticPart("/"), DynamicPart("commercialDiscPerceValue", """[^/]+""",true), StaticPart("/"), DynamicPart("thirdPartPremiumValue", """[^/]+""",true)))
  )
  private[this] lazy val controllers_InsuranceController_gettotalPremiumAndDiscValue242_invoker = createInvoker(
    InsuranceController_25.gettotalPremiumAndDiscValue(fakeValue[Double], fakeValue[Double], fakeValue[Double]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.InsuranceController",
      "gettotalPremiumAndDiscValue",
      Seq(classOf[Double], classOf[Double], classOf[Double]),
      "GET",
      """""",
      this.prefix + """CRE/getDiscValueVyODPremium/""" + "$" + """odPremiumVaue<[^/]+>/""" + "$" + """commercialDiscPerceValue<[^/]+>/""" + "$" + """thirdPartPremiumValue<[^/]+>"""
    )
  )

  // @LINE:509
  private[this] lazy val controllers_InsuranceController_gettotalPremiumForAddOn243_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/gettotalPremiumOdAddOn/"), DynamicPart("addOnPremiumValuePer", """[^/]+""",true), StaticPart("/"), DynamicPart("odPremiumVaue", """[^/]+""",true), StaticPart("/"), DynamicPart("commercialDiscPerceValue", """[^/]+""",true), StaticPart("/"), DynamicPart("idvVal", """[^/]+""",true), StaticPart("/"), DynamicPart("thirdPartPremiumValue", """[^/]+""",true)))
  )
  private[this] lazy val controllers_InsuranceController_gettotalPremiumForAddOn243_invoker = createInvoker(
    InsuranceController_25.gettotalPremiumForAddOn(fakeValue[Double], fakeValue[Double], fakeValue[Double], fakeValue[Double], fakeValue[Double]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.InsuranceController",
      "gettotalPremiumForAddOn",
      Seq(classOf[Double], classOf[Double], classOf[Double], classOf[Double], classOf[Double]),
      "GET",
      """""",
      this.prefix + """CRE/gettotalPremiumOdAddOn/""" + "$" + """addOnPremiumValuePer<[^/]+>/""" + "$" + """odPremiumVaue<[^/]+>/""" + "$" + """commercialDiscPerceValue<[^/]+>/""" + "$" + """idvVal<[^/]+>/""" + "$" + """thirdPartPremiumValue<[^/]+>"""
    )
  )

  // @LINE:513
  private[this] lazy val controllers_InsuranceController_showRoomList244_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/showRoomList")))
  )
  private[this] lazy val controllers_InsuranceController_showRoomList244_invoker = createInvoker(
    InsuranceController_25.showRoomList(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.InsuranceController",
      "showRoomList",
      Nil,
      "GET",
      """""",
      this.prefix + """CRE/showRoomList"""
    )
  )

  // @LINE:515
  private[this] lazy val controllers_InsuranceController_downloadInsuranceErrorData245_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/downloadExcelSheet/"), DynamicPart("uploadId", """[^/]+""",true), StaticPart("/"), DynamicPart("uploadType", """[^/]+""",true)))
  )
  private[this] lazy val controllers_InsuranceController_downloadInsuranceErrorData245_invoker = createInvoker(
    InsuranceController_25.downloadInsuranceErrorData(fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.InsuranceController",
      "downloadInsuranceErrorData",
      Seq(classOf[String], classOf[String]),
      "GET",
      """""",
      this.prefix + """CREManager/downloadExcelSheet/""" + "$" + """uploadId<[^/]+>/""" + "$" + """uploadType<[^/]+>"""
    )
  )

  // @LINE:518
  private[this] lazy val controllers_CampaignController_addCampaignInsurance246_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/addInsuranceCampaign")))
  )
  private[this] lazy val controllers_CampaignController_addCampaignInsurance246_invoker = createInvoker(
    CampaignController_8.addCampaignInsurance(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CampaignController",
      "addCampaignInsurance",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager/addInsuranceCampaign"""
    )
  )

  // @LINE:520
  private[this] lazy val controllers_InsuranceController_insuranceFollowUpNotificationToday247_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/ajax/getInsuranceFollowupNotificationOfToday")))
  )
  private[this] lazy val controllers_InsuranceController_insuranceFollowUpNotificationToday247_invoker = createInvoker(
    InsuranceController_25.insuranceFollowUpNotificationToday(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.InsuranceController",
      "insuranceFollowUpNotificationToday",
      Nil,
      "GET",
      """""",
      this.prefix + """CRE/ajax/getInsuranceFollowupNotificationOfToday"""
    )
  )

  // @LINE:524
  private[this] lazy val controllers_InsuranceHistoryController_uploadHistoryViewPage248_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("insuranceHistoryUpload")))
  )
  private[this] lazy val controllers_InsuranceHistoryController_uploadHistoryViewPage248_invoker = createInvoker(
    InsuranceHistoryController_21.uploadHistoryViewPage(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.InsuranceHistoryController",
      "uploadHistoryViewPage",
      Nil,
      "GET",
      """""",
      this.prefix + """insuranceHistoryUpload"""
    )
  )

  // @LINE:525
  private[this] lazy val controllers_InsuranceHistoryController_uploadExcelDataHistory249_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("insuranceHistoryUpload")))
  )
  private[this] lazy val controllers_InsuranceHistoryController_uploadExcelDataHistory249_invoker = createInvoker(
    InsuranceHistoryController_21.uploadExcelDataHistory(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.InsuranceHistoryController",
      "uploadExcelDataHistory",
      Nil,
      "POST",
      """""",
      this.prefix + """insuranceHistoryUpload"""
    )
  )

  // @LINE:527
  private[this] lazy val controllers_InsuranceController_insuranceHistoryOfCustomerId250_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/getInsuranceHistoryOfCustomerId/"), DynamicPart("customerId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_InsuranceController_insuranceHistoryOfCustomerId250_invoker = createInvoker(
    InsuranceController_25.insuranceHistoryOfCustomerId(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.InsuranceController",
      "insuranceHistoryOfCustomerId",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """CRE/getInsuranceHistoryOfCustomerId/""" + "$" + """customerId<[^/]+>"""
    )
  )

  // @LINE:528
  private[this] lazy val controllers_DealerController_getOEMOfDealer251_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/getOEMOfDealer")))
  )
  private[this] lazy val controllers_DealerController_getOEMOfDealer251_invoker = createInvoker(
    DealerController_28.getOEMOfDealer(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.DealerController",
      "getOEMOfDealer",
      Nil,
      "GET",
      """""",
      this.prefix + """CRE/getOEMOfDealer"""
    )
  )

  // @LINE:532
  private[this] lazy val controllers_FileUploadController_uploadPage252_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("fileuploadPage")))
  )
  private[this] lazy val controllers_FileUploadController_uploadPage252_invoker = createInvoker(
    FileUploadController_7.uploadPage(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FileUploadController",
      "uploadPage",
      Nil,
      "GET",
      """""",
      this.prefix + """fileuploadPage"""
    )
  )

  // @LINE:533
  private[this] lazy val controllers_FileUploadController_startUpload253_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("postfile")))
  )
  private[this] lazy val controllers_FileUploadController_startUpload253_invoker = createInvoker(
    FileUploadController_7.startUpload(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FileUploadController",
      "startUpload",
      Nil,
      "POST",
      """""",
      this.prefix + """postfile"""
    )
  )

  // @LINE:534
  private[this] lazy val controllers_FileUploadController_headRequest254_route = Route("HEAD",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("postfile")))
  )
  private[this] lazy val controllers_FileUploadController_headRequest254_invoker = createInvoker(
    FileUploadController_7.headRequest(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FileUploadController",
      "headRequest",
      Nil,
      "HEAD",
      """""",
      this.prefix + """postfile"""
    )
  )

  // @LINE:535
  private[this] lazy val controllers_ProcessUploadedFiles_process255_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("processFile")))
  )
  private[this] lazy val controllers_ProcessUploadedFiles_process255_invoker = createInvoker(
    ProcessUploadedFiles_31.process(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ProcessUploadedFiles",
      "process",
      Nil,
      "GET",
      """""",
      this.prefix + """processFile"""
    )
  )

  // @LINE:536
  private[this] lazy val controllers_FileUploadController_deleteFile256_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("deletefile/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_FileUploadController_deleteFile256_invoker = createInvoker(
    FileUploadController_7.deleteFile(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FileUploadController",
      "deleteFile",
      Seq(classOf[String]),
      "DELETE",
      """""",
      this.prefix + """deletefile/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:537
  private[this] lazy val controllers_FileUploadController_downloadFile257_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("downloadFile/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_FileUploadController_downloadFile257_invoker = createInvoker(
    FileUploadController_7.downloadFile(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FileUploadController",
      "downloadFile",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """downloadFile/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:538
  private[this] lazy val controllers_FileUploadController_downloadErrors258_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("downloadErrors/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_FileUploadController_downloadErrors258_invoker = createInvoker(
    FileUploadController_7.downloadErrors(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FileUploadController",
      "downloadErrors",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """downloadErrors/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:540
  private[this] lazy val controllers_ProcessUploadedFiles_processFile259_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("wyzprocessFile/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_ProcessUploadedFiles_processFile259_invoker = createInvoker(
    ProcessUploadedFiles_31.processFile(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ProcessUploadedFiles",
      "processFile",
      Seq(classOf[String]),
      "POST",
      """""",
      this.prefix + """wyzprocessFile/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:542
  private[this] lazy val controllers_FileUploadController_getUploadsList260_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("uploadList/"), DynamicPart("utype", """[^/]+""",true)))
  )
  private[this] lazy val controllers_FileUploadController_getUploadsList260_invoker = createInvoker(
    FileUploadController_7.getUploadsList(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FileUploadController",
      "getUploadsList",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """uploadList/""" + "$" + """utype<[^/]+>"""
    )
  )

  // @LINE:543
  private[this] lazy val controllers_FileUploadController_getUploadsListById261_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("uploadListById/"), DynamicPart("upId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_FileUploadController_getUploadsListById261_invoker = createInvoker(
    FileUploadController_7.getUploadsListById(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FileUploadController",
      "getUploadsListById",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """uploadListById/""" + "$" + """upId<[^/]+>"""
    )
  )

  // @LINE:545
  private[this] lazy val controllers_FileUploadController_getExistingFiles262_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("getExistingFiles/"), DynamicPart("utype", """[^/]+""",true)))
  )
  private[this] lazy val controllers_FileUploadController_getExistingFiles262_invoker = createInvoker(
    FileUploadController_7.getExistingFiles(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FileUploadController",
      "getExistingFiles",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """getExistingFiles/""" + "$" + """utype<[^/]+>"""
    )
  )

  // @LINE:547
  private[this] lazy val controllers_FileUploadController_uploadReportPage263_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("uploadReportPage")))
  )
  private[this] lazy val controllers_FileUploadController_uploadReportPage263_invoker = createInvoker(
    FileUploadController_7.uploadReportPage(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FileUploadController",
      "uploadReportPage",
      Nil,
      "GET",
      """""",
      this.prefix + """uploadReportPage"""
    )
  )

  // @LINE:549
  private[this] lazy val controllers_SMSandEmailController_startTriggerSMS264_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("SMSTriggger")))
  )
  private[this] lazy val controllers_SMSandEmailController_startTriggerSMS264_invoker = createInvoker(
    SMSandEmailController_18.startTriggerSMS(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SMSandEmailController",
      "startTriggerSMS",
      Nil,
      "GET",
      """""",
      this.prefix + """SMSTriggger"""
    )
  )

  // @LINE:551
  private[this] lazy val controllers_CallInteractionController_getWorkshopsByLocations265_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("getworkshopByLocation/"), DynamicPart("locations", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_getWorkshopsByLocations265_invoker = createInvoker(
    CallInteractionController_12.getWorkshopsByLocations(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getWorkshopsByLocations",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """getworkshopByLocation/""" + "$" + """locations<[^/]+>"""
    )
  )

  // @LINE:553
  private[this] lazy val controllers_CallInteractionController_getCRESByWorkshops266_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("getCRESByWorkshop/"), DynamicPart("workshops", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_getCRESByWorkshops266_invoker = createInvoker(
    CallInteractionController_12.getCRESByWorkshops(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getCRESByWorkshops",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """getCRESByWorkshop/""" + "$" + """workshops<[^/]+>"""
    )
  )

  // @LINE:556
  private[this] lazy val controllers_CallInteractionController_getCitiesByState267_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("getCityByStates/"), DynamicPart("selState", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_getCitiesByState267_invoker = createInvoker(
    CallInteractionController_12.getCitiesByState(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getCitiesByState",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """getCityByStates/""" + "$" + """selState<[^/]+>"""
    )
  )

  // @LINE:561
  private[this] lazy val controllers_CallInteractionController_viewAllComplaintsReadOnlyAccess268_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("viewAllComplaints")))
  )
  private[this] lazy val controllers_CallInteractionController_viewAllComplaintsReadOnlyAccess268_invoker = createInvoker(
    CallInteractionController_12.viewAllComplaintsReadOnlyAccess(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "viewAllComplaintsReadOnlyAccess",
      Nil,
      "GET",
      """""",
      this.prefix + """viewAllComplaints"""
    )
  )

  // @LINE:563
  private[this] lazy val controllers_CallInteractionController_getListWorkshopByLocationById269_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/listWorkshopsByID/"), DynamicPart("selectedCity", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_getListWorkshopByLocationById269_invoker = createInvoker(
    CallInteractionController_12.getListWorkshopByLocationById(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getListWorkshopByLocationById",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """CRE/listWorkshopsByID/""" + "$" + """selectedCity<[^/]+>"""
    )
  )

  // @LINE:564
  private[this] lazy val controllers_CallInteractionController_getFunctionsListByLoc270_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/listFunctionsByLocation/"), DynamicPart("selectedCity", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_getFunctionsListByLoc270_invoker = createInvoker(
    CallInteractionController_12.getFunctionsListByLoc(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getFunctionsListByLoc",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """CRE/listFunctionsByLocation/""" + "$" + """selectedCity<[^/]+>"""
    )
  )

  // @LINE:565
  private[this] lazy val controllers_CallInteractionController_getusersByFuncandLocation271_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/listUsersByFuncandLoc/"), DynamicPart("city", """[^/]+""",true), StaticPart("/"), DynamicPart("function", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInteractionController_getusersByFuncandLocation271_invoker = createInvoker(
    CallInteractionController_12.getusersByFuncandLocation(fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInteractionController",
      "getusersByFuncandLocation",
      Seq(classOf[String], classOf[String]),
      "GET",
      """""",
      this.prefix + """CRE/listUsersByFuncandLoc/""" + "$" + """city<[^/]+>/""" + "$" + """function<[^/]+>"""
    )
  )

  // @LINE:567
  private[this] lazy val controllers_ChangeAssignmentController_changeassignedCalls272_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/changeAssignedCalls")))
  )
  private[this] lazy val controllers_ChangeAssignmentController_changeassignedCalls272_invoker = createInvoker(
    ChangeAssignmentController_22.changeassignedCalls(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ChangeAssignmentController",
      "changeassignedCalls",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager/changeAssignedCalls"""
    )
  )

  // @LINE:568
  private[this] lazy val controllers_ChangeAssignmentController_postchangeassignedCalls273_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/changeAssignedCalls")))
  )
  private[this] lazy val controllers_ChangeAssignmentController_postchangeassignedCalls273_invoker = createInvoker(
    ChangeAssignmentController_22.postchangeassignedCalls(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ChangeAssignmentController",
      "postchangeassignedCalls",
      Nil,
      "POST",
      """""",
      this.prefix + """CREManager/changeAssignedCalls"""
    )
  )

  // @LINE:570
  private[this] lazy val controllers_ChangeAssignmentController_changeAssignedCallsToAgents274_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/assignCallToAgents")))
  )
  private[this] lazy val controllers_ChangeAssignmentController_changeAssignedCallsToAgents274_invoker = createInvoker(
    ChangeAssignmentController_22.changeAssignedCallsToAgents(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ChangeAssignmentController",
      "changeAssignedCallsToAgents",
      Nil,
      "POST",
      """""",
      this.prefix + """CREManager/assignCallToAgents"""
    )
  )

  // @LINE:573
  private[this] lazy val controllers_CampaignController_getCampaignNamesByUpload275_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("getCampaignList/"), DynamicPart("uploadType", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CampaignController_getCampaignNamesByUpload275_invoker = createInvoker(
    CampaignController_8.getCampaignNamesByUpload(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CampaignController",
      "getCampaignNamesByUpload",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """getCampaignList/""" + "$" + """uploadType<[^/]+>"""
    )
  )

  // @LINE:578
  private[this] lazy val controllers_ChangeAssignmentController_assignmentFilterList276_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/assignmentFilterPage")))
  )
  private[this] lazy val controllers_ChangeAssignmentController_assignmentFilterList276_invoker = createInvoker(
    ChangeAssignmentController_22.assignmentFilterList(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ChangeAssignmentController",
      "assignmentFilterList",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager/assignmentFilterPage"""
    )
  )

  // @LINE:579
  private[this] lazy val controllers_ChangeAssignmentController_getAssignmentFilterListAjax277_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/assignedCallsList")))
  )
  private[this] lazy val controllers_ChangeAssignmentController_getAssignmentFilterListAjax277_invoker = createInvoker(
    ChangeAssignmentController_22.getAssignmentFilterListAjax(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ChangeAssignmentController",
      "getAssignmentFilterListAjax",
      Nil,
      "POST",
      """""",
      this.prefix + """CREManager/assignedCallsList"""
    )
  )

  // @LINE:580
  private[this] lazy val controllers_ChangeAssignmentController_assignListToUserSelected278_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/assignedCallsListToUser")))
  )
  private[this] lazy val controllers_ChangeAssignmentController_assignListToUserSelected278_invoker = createInvoker(
    ChangeAssignmentController_22.assignListToUserSelected(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ChangeAssignmentController",
      "assignListToUserSelected",
      Nil,
      "POST",
      """""",
      this.prefix + """CREManager/assignedCallsListToUser"""
    )
  )

  // @LINE:584
  private[this] lazy val controllers_CallInfoController_getLeadNamesbyLeadId279_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/getLeadNamesbyLeadId/"), DynamicPart("leadId", """[^/]+""",true), StaticPart("/"), DynamicPart("userId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInfoController_getLeadNamesbyLeadId279_invoker = createInvoker(
    CallInfoController_26.getLeadNamesbyLeadId(fakeValue[Long], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInfoController",
      "getLeadNamesbyLeadId",
      Seq(classOf[Long], classOf[Long]),
      "GET",
      """""",
      this.prefix + """CRE/getLeadNamesbyLeadId/""" + "$" + """leadId<[^/]+>/""" + "$" + """userId<[^/]+>"""
    )
  )

  // @LINE:586
  private[this] lazy val controllers_CallInfoController_getCheckVehicleRegExist280_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/checkingVehicleRegNo/"), DynamicPart("vehRegId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CallInfoController_getCheckVehicleRegExist280_invoker = createInvoker(
    CallInfoController_26.getCheckVehicleRegExist(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallInfoController",
      "getCheckVehicleRegExist",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """CRE/checkingVehicleRegNo/""" + "$" + """vehRegId<[^/]+>"""
    )
  )

  // @LINE:589
  private[this] lazy val controllers_SuperAdminController_LocationBySuperAdmin281_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("LocationBySuperAdmins")))
  )
  private[this] lazy val controllers_SuperAdminController_LocationBySuperAdmin281_invoker = createInvoker(
    SuperAdminController_17.LocationBySuperAdmin(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SuperAdminController",
      "LocationBySuperAdmin",
      Nil,
      "GET",
      """Super Admin - Add location""",
      this.prefix + """LocationBySuperAdmins"""
    )
  )

  // @LINE:590
  private[this] lazy val controllers_SuperAdminController_postLocationBySuperAdmin282_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("postLocationBySuperAdmins")))
  )
  private[this] lazy val controllers_SuperAdminController_postLocationBySuperAdmin282_invoker = createInvoker(
    SuperAdminController_17.postLocationBySuperAdmin(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SuperAdminController",
      "postLocationBySuperAdmin",
      Nil,
      "POST",
      """""",
      this.prefix + """postLocationBySuperAdmins"""
    )
  )

  // @LINE:593
  private[this] lazy val controllers_SuperAdminController_UsersBySuperAdmin283_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("UsersBySuperAdmins")))
  )
  private[this] lazy val controllers_SuperAdminController_UsersBySuperAdmin283_invoker = createInvoker(
    SuperAdminController_17.UsersBySuperAdmin(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SuperAdminController",
      "UsersBySuperAdmin",
      Nil,
      "GET",
      """Super Admin - Add new User detail""",
      this.prefix + """UsersBySuperAdmins"""
    )
  )

  // @LINE:594
  private[this] lazy val controllers_SuperAdminController_postUserBySuperAdmin284_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("postUserBySuperAdmins")))
  )
  private[this] lazy val controllers_SuperAdminController_postUserBySuperAdmin284_invoker = createInvoker(
    SuperAdminController_17.postUserBySuperAdmin(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SuperAdminController",
      "postUserBySuperAdmin",
      Nil,
      "POST",
      """""",
      this.prefix + """postUserBySuperAdmins"""
    )
  )

  // @LINE:595
  private[this] lazy val controllers_SuperAdminController_checkIfUserExists285_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("checkExistingUser/"), DynamicPart("uname", """[^/]+""",true)))
  )
  private[this] lazy val controllers_SuperAdminController_checkIfUserExists285_invoker = createInvoker(
    SuperAdminController_17.checkIfUserExists(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SuperAdminController",
      "checkIfUserExists",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """checkExistingUser/""" + "$" + """uname<[^/]+>"""
    )
  )

  // @LINE:599
  private[this] lazy val controllers_SMSTemplateController_SMSTemplateBySuperAdmin286_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("SMSTemplateBySuperAdmin")))
  )
  private[this] lazy val controllers_SMSTemplateController_SMSTemplateBySuperAdmin286_invoker = createInvoker(
    SMSTemplateController_2.SMSTemplateBySuperAdmin(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SMSTemplateController",
      "SMSTemplateBySuperAdmin",
      Nil,
      "GET",
      """""",
      this.prefix + """SMSTemplateBySuperAdmin"""
    )
  )

  // @LINE:600
  private[this] lazy val controllers_SMSTemplateController_postSMSTemplate287_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("postSMSTemplate/"), DynamicPart("messageTemplate", """[^/]+""",true), StaticPart("/"), DynamicPart("msgAPI", """[^/]+""",true)))
  )
  private[this] lazy val controllers_SMSTemplateController_postSMSTemplate287_invoker = createInvoker(
    SMSTemplateController_2.postSMSTemplate(fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SMSTemplateController",
      "postSMSTemplate",
      Seq(classOf[String], classOf[String]),
      "GET",
      """""",
      this.prefix + """postSMSTemplate/""" + "$" + """messageTemplate<[^/]+>/""" + "$" + """msgAPI<[^/]+>"""
    )
  )

  // @LINE:601
  private[this] lazy val controllers_SMSTemplateController_getCustomersListBySavedName288_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("getCustomersListBySavedName/"), DynamicPart("savedsearchname", """[^/]+""",true)))
  )
  private[this] lazy val controllers_SMSTemplateController_getCustomersListBySavedName288_invoker = createInvoker(
    SMSTemplateController_2.getCustomersListBySavedName(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SMSTemplateController",
      "getCustomersListBySavedName",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """getCustomersListBySavedName/""" + "$" + """savedsearchname<[^/]+>"""
    )
  )

  // @LINE:602
  private[this] lazy val controllers_SMSTemplateController_postSMSBulk289_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("postSMSBulk")))
  )
  private[this] lazy val controllers_SMSTemplateController_postSMSBulk289_invoker = createInvoker(
    SMSTemplateController_2.postSMSBulk(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SMSTemplateController",
      "postSMSBulk",
      Nil,
      "POST",
      """""",
      this.prefix + """postSMSBulk"""
    )
  )

  // @LINE:603
  private[this] lazy val controllers_SMSTemplateController_getSelectedUserListforDatatable290_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("getSelectedUserListforDatatable/"), DynamicPart("selectedUsers", """[^/]+""",true), StaticPart("/"), DynamicPart("setFlag", """[^/]+""",true), StaticPart("/"), DynamicPart("savedsearchname", """[^/]+""",true)))
  )
  private[this] lazy val controllers_SMSTemplateController_getSelectedUserListforDatatable290_invoker = createInvoker(
    SMSTemplateController_2.getSelectedUserListforDatatable(fakeValue[String], fakeValue[Boolean], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SMSTemplateController",
      "getSelectedUserListforDatatable",
      Seq(classOf[String], classOf[Boolean], classOf[String]),
      "GET",
      """""",
      this.prefix + """getSelectedUserListforDatatable/""" + "$" + """selectedUsers<[^/]+>/""" + "$" + """setFlag<[^/]+>/""" + "$" + """savedsearchname<[^/]+>"""
    )
  )

  // @LINE:607
  private[this] lazy val controllers_ServiceBookController_getReviewBookingPage291_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/getReviewBookingPage")))
  )
  private[this] lazy val controllers_ServiceBookController_getReviewBookingPage291_invoker = createInvoker(
    ServiceBookController_32.getReviewBookingPage(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ServiceBookController",
      "getReviewBookingPage",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager/getReviewBookingPage"""
    )
  )

  // @LINE:609
  private[this] lazy val controllers_ServiceBookController_getServiceAdvisorListByWorkshop292_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("serviceAdvisorListByWorkshop/"), DynamicPart("workshopid", """[^/]+""",true)))
  )
  private[this] lazy val controllers_ServiceBookController_getServiceAdvisorListByWorkshop292_invoker = createInvoker(
    ServiceBookController_32.getServiceAdvisorListByWorkshop(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ServiceBookController",
      "getServiceAdvisorListByWorkshop",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """serviceAdvisorListByWorkshop/""" + "$" + """workshopid<[^/]+>"""
    )
  )

  // @LINE:610
  private[this] lazy val controllers_ServiceBookController_getsearchServiceBoookedList293_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("searchServiceBookedList")))
  )
  private[this] lazy val controllers_ServiceBookController_getsearchServiceBoookedList293_invoker = createInvoker(
    ServiceBookController_32.getsearchServiceBoookedList(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ServiceBookController",
      "getsearchServiceBoookedList",
      Nil,
      "GET",
      """""",
      this.prefix + """searchServiceBookedList"""
    )
  )

  // @LINE:613
  private[this] lazy val controllers_ServiceBookController_cancelBookingOrPickup294_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/cancelBooking/"), DynamicPart("servicebookedId", """[^/]+""",true), StaticPart("/"), DynamicPart("cancelId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_ServiceBookController_cancelBookingOrPickup294_invoker = createInvoker(
    ServiceBookController_32.cancelBookingOrPickup(fakeValue[Long], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ServiceBookController",
      "cancelBookingOrPickup",
      Seq(classOf[Long], classOf[Long]),
      "GET",
      """cancek booking""",
      this.prefix + """CREManager/cancelBooking/""" + "$" + """servicebookedId<[^/]+>/""" + "$" + """cancelId<[^/]+>"""
    )
  )

  // @LINE:615
  private[this] lazy val controllers_ServiceBookController_reviewScheduleBooking295_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/reviewResheduleBooking/"), DynamicPart("servicebookedId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_ServiceBookController_reviewScheduleBooking295_invoker = createInvoker(
    ServiceBookController_32.reviewScheduleBooking(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ServiceBookController",
      "reviewScheduleBooking",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """CREManager/reviewResheduleBooking/""" + "$" + """servicebookedId<[^/]+>"""
    )
  )

  // @LINE:616
  private[this] lazy val controllers_ServiceBookController_postRescheduleBooking296_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/postRescheduleBooking")))
  )
  private[this] lazy val controllers_ServiceBookController_postRescheduleBooking296_invoker = createInvoker(
    ServiceBookController_32.postRescheduleBooking(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ServiceBookController",
      "postRescheduleBooking",
      Nil,
      "POST",
      """""",
      this.prefix + """CREManager/postRescheduleBooking"""
    )
  )

  // @LINE:618
  private[this] lazy val controllers_ServiceBookController_driverListScheduleByWorkshopId297_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("driverListSchedule/"), DynamicPart("workshopId", """[^/]+""",true), StaticPart("/"), DynamicPart("scheduleDate", """[^/]+""",true)))
  )
  private[this] lazy val controllers_ServiceBookController_driverListScheduleByWorkshopId297_invoker = createInvoker(
    ServiceBookController_32.driverListScheduleByWorkshopId(fakeValue[Long], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ServiceBookController",
      "driverListScheduleByWorkshopId",
      Seq(classOf[Long], classOf[String]),
      "GET",
      """""",
      this.prefix + """driverListSchedule/""" + "$" + """workshopId<[^/]+>/""" + "$" + """scheduleDate<[^/]+>"""
    )
  )

  // @LINE:621
  private[this] lazy val controllers_AudioStreamController_header298_route = Route("HEAD",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/audiostream/"), DynamicPart("interactionId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_AudioStreamController_header298_invoker = createInvoker(
    AudioStreamController_3.header(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AudioStreamController",
      "header",
      Seq(classOf[String]),
      "HEAD",
      """play callrecording file""",
      this.prefix + """CRE/audiostream/""" + "$" + """interactionId<[^/]+>"""
    )
  )

  // @LINE:622
  private[this] lazy val controllers_AudioStreamController_streamAudio299_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CRE/audiostream/"), DynamicPart("interactionId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_AudioStreamController_streamAudio299_invoker = createInvoker(
    AudioStreamController_3.streamAudio(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AudioStreamController",
      "streamAudio",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """CRE/audiostream/""" + "$" + """interactionId<[^/]+>"""
    )
  )

  // @LINE:625
  private[this] lazy val controllers_AllAudioConverter_convertAllFiles300_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/audioconverter/"), DynamicPart("startId", """[^/]+""",true), StaticPart("/"), DynamicPart("endId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_AllAudioConverter_convertAllFiles300_invoker = createInvoker(
    AllAudioConverter_9.convertAllFiles(fakeValue[Long], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AllAudioConverter",
      "convertAllFiles",
      Seq(classOf[Long], classOf[Long]),
      "GET",
      """Audio Converter""",
      this.prefix + """CREManager/audioconverter/""" + "$" + """startId<[^/]+>/""" + "$" + """endId<[^/]+>"""
    )
  )

  // @LINE:630
  private[this] lazy val controllers_CallRecordingHistoryController_callRecordingView301_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/callRecording")))
  )
  private[this] lazy val controllers_CallRecordingHistoryController_callRecordingView301_invoker = createInvoker(
    CallRecordingHistoryController_13.callRecordingView(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallRecordingHistoryController",
      "callRecordingView",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager/callRecording"""
    )
  )

  // @LINE:631
  private[this] lazy val controllers_CallRecordingHistoryController_callRecordingData302_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/recordingDataList")))
  )
  private[this] lazy val controllers_CallRecordingHistoryController_callRecordingData302_invoker = createInvoker(
    CallRecordingHistoryController_13.callRecordingData(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallRecordingHistoryController",
      "callRecordingData",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager/recordingDataList"""
    )
  )

  // @LINE:633
  private[this] lazy val controllers_CallRecordingHistoryController_callRecordingViewInsurance303_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/callRecordingInsurance")))
  )
  private[this] lazy val controllers_CallRecordingHistoryController_callRecordingViewInsurance303_invoker = createInvoker(
    CallRecordingHistoryController_13.callRecordingViewInsurance(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallRecordingHistoryController",
      "callRecordingViewInsurance",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager/callRecordingInsurance"""
    )
  )

  // @LINE:634
  private[this] lazy val controllers_CallRecordingHistoryController_callRecordingDataInsurance304_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/recordingDataListInsurance")))
  )
  private[this] lazy val controllers_CallRecordingHistoryController_callRecordingDataInsurance304_invoker = createInvoker(
    CallRecordingHistoryController_13.callRecordingDataInsurance(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallRecordingHistoryController",
      "callRecordingDataInsurance",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager/recordingDataListInsurance"""
    )
  )

  // @LINE:636
  private[this] lazy val controllers_CallRecordingHistoryController_callRecordingViewPSF305_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/callRecordingPSF")))
  )
  private[this] lazy val controllers_CallRecordingHistoryController_callRecordingViewPSF305_invoker = createInvoker(
    CallRecordingHistoryController_13.callRecordingViewPSF(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallRecordingHistoryController",
      "callRecordingViewPSF",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager/callRecordingPSF"""
    )
  )

  // @LINE:637
  private[this] lazy val controllers_CallRecordingHistoryController_callRecordingDataPSF306_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/recordingDataListPSF")))
  )
  private[this] lazy val controllers_CallRecordingHistoryController_callRecordingDataPSF306_invoker = createInvoker(
    CallRecordingHistoryController_13.callRecordingDataPSF(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallRecordingHistoryController",
      "callRecordingDataPSF",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager/recordingDataListPSF"""
    )
  )

  // @LINE:639
  private[this] lazy val controllers_CallRecordingHistoryController_otherCallRecordingView307_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/otherCallRecording")))
  )
  private[this] lazy val controllers_CallRecordingHistoryController_otherCallRecordingView307_invoker = createInvoker(
    CallRecordingHistoryController_13.otherCallRecordingView(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallRecordingHistoryController",
      "otherCallRecordingView",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager/otherCallRecording"""
    )
  )

  // @LINE:640
  private[this] lazy val controllers_CallRecordingHistoryController_otherCallRecordingData308_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/otherRecordingDataList")))
  )
  private[this] lazy val controllers_CallRecordingHistoryController_otherCallRecordingData308_invoker = createInvoker(
    CallRecordingHistoryController_13.otherCallRecordingData(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CallRecordingHistoryController",
      "otherCallRecordingData",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager/otherRecordingDataList"""
    )
  )

  // @LINE:644
  private[this] lazy val controllers_FirebaseSyncController_startSyncOperationInsuranceAgent309_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/startSyncInsuranceAgent")))
  )
  private[this] lazy val controllers_FirebaseSyncController_startSyncOperationInsuranceAgent309_invoker = createInvoker(
    FirebaseSyncController_6.startSyncOperationInsuranceAgent(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FirebaseSyncController",
      "startSyncOperationInsuranceAgent",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager/startSyncInsuranceAgent"""
    )
  )

  // @LINE:645
  private[this] lazy val controllers_FirebaseSyncController_startSyncOperationInsuranceAgentHistory310_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/startSyncInsuranceHistory")))
  )
  private[this] lazy val controllers_FirebaseSyncController_startSyncOperationInsuranceAgentHistory310_invoker = createInvoker(
    FirebaseSyncController_6.startSyncOperationInsuranceAgentHistory(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FirebaseSyncController",
      "startSyncOperationInsuranceAgentHistory",
      Nil,
      "GET",
      """""",
      this.prefix + """CREManager/startSyncInsuranceHistory"""
    )
  )

  // @LINE:647
  private[this] lazy val controllers_ServiceBookController_downloadServiceBooking311_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("CREManager/downloadBooking")))
  )
  private[this] lazy val controllers_ServiceBookController_downloadServiceBooking311_invoker = createInvoker(
    ServiceBookController_32.downloadServiceBooking(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ServiceBookController",
      "downloadServiceBooking",
      Nil,
      "POST",
      """""",
      this.prefix + """CREManager/downloadBooking"""
    )
  )


  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:7
    case controllers_Application_landingPage0_route(params) =>
      call { 
        controllers_Application_landingPage0_invoker.call(Application_27.landingPage())
      }
  
    // @LINE:9
    case controllers_Application_login1_route(params) =>
      call { 
        controllers_Application_login1_invoker.call(Application_27.login)
      }
  
    // @LINE:10
    case controllers_WyzUserController_changePassword2_route(params) =>
      call { 
        controllers_WyzUserController_changePassword2_invoker.call(WyzUserController_11.changePassword)
      }
  
    // @LINE:11
    case controllers_WyzUserController_logout3_route(params) =>
      call { 
        controllers_WyzUserController_logout3_invoker.call(WyzUserController_11.logout)
      }
  
    // @LINE:13
    case org_pac4j_play_CallbackController_callback4_route(params) =>
      call { 
        org_pac4j_play_CallbackController_callback4_invoker.call(CallbackController_30.get.callback())
      }
  
    // @LINE:14
    case org_pac4j_play_CallbackController_callback5_route(params) =>
      call { 
        org_pac4j_play_CallbackController_callback5_invoker.call(CallbackController_30.get.callback())
      }
  
    // @LINE:16
    case controllers_Application_formIndex6_route(params) =>
      call { 
        controllers_Application_formIndex6_invoker.call(Application_27.formIndex())
      }
  
    // @LINE:18
    case controllers_WyzUserController_logoutParticularUser7_route(params) =>
      call(params.fromPath[String]("userIs", None)) { (userIs) =>
        controllers_WyzUserController_logoutParticularUser7_invoker.call(WyzUserController_11.logoutParticularUser(userIs))
      }
  
    // @LINE:22
    case controllers_WyzUserController_index8_route(params) =>
      call { 
        controllers_WyzUserController_index8_invoker.call(WyzUserController_11.index)
      }
  
    // @LINE:25
    case controllers_WyzUserController_addapp9_route(params) =>
      call { 
        controllers_WyzUserController_addapp9_invoker.call(WyzUserController_11.addapp)
      }
  
    // @LINE:26
    case controllers_WyzUserController_addApplicationUser10_route(params) =>
      call { 
        controllers_WyzUserController_addApplicationUser10_invoker.call(WyzUserController_11.addApplicationUser)
      }
  
    // @LINE:28
    case controllers_WyzUserController_userInformation11_route(params) =>
      call { 
        controllers_WyzUserController_userInformation11_invoker.call(WyzUserController_11.userInformation)
      }
  
    // @LINE:29
    case controllers_WyzUserController_userInformation12_route(params) =>
      call { 
        controllers_WyzUserController_userInformation12_invoker.call(WyzUserController_11.userInformation)
      }
  
    // @LINE:31
    case controllers_WyzUserController_geteditUser13_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_WyzUserController_geteditUser13_invoker.call(WyzUserController_11.geteditUser(id))
      }
  
    // @LINE:32
    case controllers_WyzUserController_postEditUser14_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_WyzUserController_postEditUser14_invoker.call(WyzUserController_11.postEditUser(id))
      }
  
    // @LINE:33
    case controllers_WyzUserController_deleteUserData15_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_WyzUserController_deleteUserData15_invoker.call(WyzUserController_11.deleteUserData(id))
      }
  
    // @LINE:35
    case controllers_DealerController_adddel16_route(params) =>
      call { 
        controllers_DealerController_adddel16_invoker.call(DealerController_28.adddel)
      }
  
    // @LINE:36
    case controllers_DealerController_adddealerForm17_route(params) =>
      call { 
        controllers_DealerController_adddealerForm17_invoker.call(DealerController_28.adddealerForm)
      }
  
    // @LINE:38
    case controllers_DealerController_dealerInformation18_route(params) =>
      call { 
        controllers_DealerController_dealerInformation18_invoker.call(DealerController_28.dealerInformation)
      }
  
    // @LINE:39
    case controllers_DealerController_deleteDealerData19_route(params) =>
      call(params.fromPath[Long]("did", None)) { (did) =>
        controllers_DealerController_deleteDealerData19_invoker.call(DealerController_28.deleteDealerData(did))
      }
  
    // @LINE:41
    case controllers_DealerController_geteditDealer20_route(params) =>
      call(params.fromPath[Long]("did", None)) { (did) =>
        controllers_DealerController_geteditDealer20_invoker.call(DealerController_28.geteditDealer(did))
      }
  
    // @LINE:42
    case controllers_DealerController_postEditDealer21_route(params) =>
      call(params.fromPath[Long]("did", None)) { (did) =>
        controllers_DealerController_postEditDealer21_invoker.call(DealerController_28.postEditDealer(did))
      }
  
    // @LINE:44
    case controllers_ScheduledCallController_startSyncForTesting22_route(params) =>
      call { 
        controllers_ScheduledCallController_startSyncForTesting22_invoker.call(ScheduledCallController_1.startSyncForTesting())
      }
  
    // @LINE:56
    case controllers_WyzUserController_changepasswordsuperAdmin23_route(params) =>
      call { 
        controllers_WyzUserController_changepasswordsuperAdmin23_invoker.call(WyzUserController_11.changepasswordsuperAdmin)
      }
  
    // @LINE:57
    case controllers_WyzUserController_changepasswordeditingsuperAdmin24_route(params) =>
      call { 
        controllers_WyzUserController_changepasswordeditingsuperAdmin24_invoker.call(WyzUserController_11.changepasswordeditingsuperAdmin())
      }
  
    // @LINE:58
    case controllers_WyzUserController_loginfo25_route(params) =>
      call { 
        controllers_WyzUserController_loginfo25_invoker.call(WyzUserController_11.loginfo())
      }
  
    // @LINE:62
    case controllers_WyzUserController_indexPageCREManager26_route(params) =>
      call { 
        controllers_WyzUserController_indexPageCREManager26_invoker.call(WyzUserController_11.indexPageCREManager())
      }
  
    // @LINE:63
    case controllers_WyzUserController_viewReportCREManager27_route(params) =>
      call { 
        controllers_WyzUserController_viewReportCREManager27_invoker.call(WyzUserController_11.viewReportCREManager)
      }
  
    // @LINE:66
    case controllers_WyzUserController_changepassword28_route(params) =>
      call { 
        controllers_WyzUserController_changepassword28_invoker.call(WyzUserController_11.changepassword)
      }
  
    // @LINE:67
    case controllers_WyzUserController_changepasswordediting29_route(params) =>
      call { 
        controllers_WyzUserController_changepasswordediting29_invoker.call(WyzUserController_11.changepasswordediting())
      }
  
    // @LINE:71
    case controllers_SearchControllerMR_getPSFassignedInteractionTableDataMR30_route(params) =>
      call(params.fromPath[String]("CREIds", None)) { (CREIds) =>
        controllers_SearchControllerMR_getPSFassignedInteractionTableDataMR30_invoker.call(SearchControllerMR_10.getPSFassignedInteractionTableDataMR(CREIds))
      }
  
    // @LINE:72
    case controllers_SearchControllerMR_getPSFfollowUpCallLogTableDataMR31_route(params) =>
      call(params.fromPath[String]("CREIds", None), params.fromPath[Long]("buckettype", None)) { (CREIds, buckettype) =>
        controllers_SearchControllerMR_getPSFfollowUpCallLogTableDataMR31_invoker.call(SearchControllerMR_10.getPSFfollowUpCallLogTableDataMR(CREIds, buckettype))
      }
  
    // @LINE:75
    case controllers_SearchControllerMR_getPSFnonContactsServerDataTableMR32_route(params) =>
      call(params.fromPath[String]("CREIds", None), params.fromPath[Long]("buckettype", None)) { (CREIds, buckettype) =>
        controllers_SearchControllerMR_getPSFnonContactsServerDataTableMR32_invoker.call(SearchControllerMR_10.getPSFnonContactsServerDataTableMR(CREIds, buckettype))
      }
  
    // @LINE:79
    case controllers_SearchControllerMR_getInsuranceassignedInteractionTableDataMR33_route(params) =>
      call(params.fromPath[String]("CREIds", None)) { (CREIds) =>
        controllers_SearchControllerMR_getInsuranceassignedInteractionTableDataMR33_invoker.call(SearchControllerMR_10.getInsuranceassignedInteractionTableDataMR(CREIds))
      }
  
    // @LINE:80
    case controllers_SearchControllerMR_getInsurancefollowUpCallLogTableDataMR34_route(params) =>
      call(params.fromPath[String]("CREIds", None), params.fromPath[Long]("buckettype", None)) { (CREIds, buckettype) =>
        controllers_SearchControllerMR_getInsurancefollowUpCallLogTableDataMR34_invoker.call(SearchControllerMR_10.getInsurancefollowUpCallLogTableDataMR(CREIds, buckettype))
      }
  
    // @LINE:83
    case controllers_SearchControllerMR_getInsurancenonContactsServerDataTableMR35_route(params) =>
      call(params.fromPath[String]("CREIds", None), params.fromPath[Long]("buckettype", None)) { (CREIds, buckettype) =>
        controllers_SearchControllerMR_getInsurancenonContactsServerDataTableMR35_invoker.call(SearchControllerMR_10.getInsurancenonContactsServerDataTableMR(CREIds, buckettype))
      }
  
    // @LINE:88
    case controllers_CallInfoController_getCallDispositionBucketForCREMan36_route(params) =>
      call { 
        controllers_CallInfoController_getCallDispositionBucketForCREMan36_invoker.call(CallInfoController_26.getCallDispositionBucketForCREMan())
      }
  
    // @LINE:89
    case controllers_CallInfoController_getPSFCallLogViewPage37_route(params) =>
      call { 
        controllers_CallInfoController_getPSFCallLogViewPage37_invoker.call(CallInfoController_26.getPSFCallLogViewPage())
      }
  
    // @LINE:90
    case controllers_CallInfoController_getInsuranceCallLogViewPage38_route(params) =>
      call { 
        controllers_CallInfoController_getInsuranceCallLogViewPage38_invoker.call(CallInfoController_26.getInsuranceCallLogViewPage())
      }
  
    // @LINE:95
    case controllers_CallInteractionController_getAssignedCallsOfCREManager39_route(params) =>
      call(params.fromPath[String]("selectedAgent", None)) { (selectedAgent) =>
        controllers_CallInteractionController_getAssignedCallsOfCREManager39_invoker.call(CallInteractionController_12.getAssignedCallsOfCREManager(selectedAgent))
      }
  
    // @LINE:96
    case controllers_CallInteractionController_getFollowUpRequiredDataCREMan40_route(params) =>
      call(params.fromPath[String]("selectedAgent", None)) { (selectedAgent) =>
        controllers_CallInteractionController_getFollowUpRequiredDataCREMan40_invoker.call(CallInteractionController_12.getFollowUpRequiredDataCREMan(selectedAgent))
      }
  
    // @LINE:97
    case controllers_CallInteractionController_getServiceBookedDataCREMan41_route(params) =>
      call(params.fromPath[String]("selectedAgent", None)) { (selectedAgent) =>
        controllers_CallInteractionController_getServiceBookedDataCREMan41_invoker.call(CallInteractionController_12.getServiceBookedDataCREMan(selectedAgent))
      }
  
    // @LINE:98
    case controllers_CallInteractionController_getServiceNotRequiredDataCREMan42_route(params) =>
      call(params.fromPath[String]("selectedAgent", None)) { (selectedAgent) =>
        controllers_CallInteractionController_getServiceNotRequiredDataCREMan42_invoker.call(CallInteractionController_12.getServiceNotRequiredDataCREMan(selectedAgent))
      }
  
    // @LINE:99
    case controllers_CallInteractionController_getNonContactsDataCREMan43_route(params) =>
      call(params.fromPath[String]("selectedAgent", None)) { (selectedAgent) =>
        controllers_CallInteractionController_getNonContactsDataCREMan43_invoker.call(CallInteractionController_12.getNonContactsDataCREMan(selectedAgent))
      }
  
    // @LINE:100
    case controllers_CallInteractionController_getDroppedCallsDataCREMan44_route(params) =>
      call(params.fromPath[String]("selectedAgent", None)) { (selectedAgent) =>
        controllers_CallInteractionController_getDroppedCallsDataCREMan44_invoker.call(CallInteractionController_12.getDroppedCallsDataCREMan(selectedAgent))
      }
  
    // @LINE:104
    case controllers_WyzUserController_indexPageCRE45_route(params) =>
      call { 
        controllers_WyzUserController_indexPageCRE45_invoker.call(WyzUserController_11.indexPageCRE())
      }
  
    // @LINE:105
    case controllers_ReportsController_viewReportCRE46_route(params) =>
      call { 
        controllers_ReportsController_viewReportCRE46_invoker.call(ReportsController_19.viewReportCRE())
      }
  
    // @LINE:106
    case controllers_CallInfoController_getcallLogEditForCRE47_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_CallInfoController_getcallLogEditForCRE47_invoker.call(CallInfoController_26.getcallLogEditForCRE(id))
      }
  
    // @LINE:107
    case controllers_CallInfoController_postcallLogeditForCRE48_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_CallInfoController_postcallLogeditForCRE48_invoker.call(CallInfoController_26.postcallLogeditForCRE(id))
      }
  
    // @LINE:110
    case controllers_WyzUserController_changepasswordCRE49_route(params) =>
      call { 
        controllers_WyzUserController_changepasswordCRE49_invoker.call(WyzUserController_11.changepasswordCRE())
      }
  
    // @LINE:111
    case controllers_WyzUserController_changepasswordeditingCRE50_route(params) =>
      call { 
        controllers_WyzUserController_changepasswordeditingCRE50_invoker.call(WyzUserController_11.changepasswordeditingCRE())
      }
  
    // @LINE:118
    case controllers_ScheduledCallController_getCallDispositionTabPAgeCRE51_route(params) =>
      call { 
        controllers_ScheduledCallController_getCallDispositionTabPAgeCRE51_invoker.call(ScheduledCallController_1.getCallDispositionTabPAgeCRE())
      }
  
    // @LINE:123
    case controllers_CallInfoController_startInitiatingOfCall52_route(params) =>
      call(params.fromPath[Long]("phonenumber", None), params.fromPath[Long]("uniqueid", None), params.fromPath[Long]("customerId", None)) { (phonenumber, uniqueid, customerId) =>
        controllers_CallInfoController_startInitiatingOfCall52_invoker.call(CallInfoController_26.startInitiatingOfCall(phonenumber, uniqueid, customerId))
      }
  
    // @LINE:126
    case controllers_ReportsController_getAllAjaxRequestForCRE53_route(params) =>
      call { 
        controllers_ReportsController_getAllAjaxRequestForCRE53_invoker.call(ReportsController_19.getAllAjaxRequestForCRE())
      }
  
    // @LINE:128
    case controllers_ReportsController_getScheduledCallsCountOfCRE54_route(params) =>
      call { 
        controllers_ReportsController_getScheduledCallsCountOfCRE54_invoker.call(ReportsController_19.getScheduledCallsCountOfCRE())
      }
  
    // @LINE:129
    case controllers_ReportsController_getScheduledCallsPendingCountForCRE55_route(params) =>
      call { 
        controllers_ReportsController_getScheduledCallsPendingCountForCRE55_invoker.call(ReportsController_19.getScheduledCallsPendingCountForCRE())
      }
  
    // @LINE:130
    case controllers_ReportsController_getServiceBookedForCRE56_route(params) =>
      call { 
        controllers_ReportsController_getServiceBookedForCRE56_invoker.call(ReportsController_19.getServiceBookedForCRE())
      }
  
    // @LINE:131
    case controllers_ReportsController_getServiceBookedPercentageForCRE57_route(params) =>
      call { 
        controllers_ReportsController_getServiceBookedPercentageForCRE57_invoker.call(ReportsController_19.getServiceBookedPercentageForCRE())
      }
  
    // @LINE:132
    case controllers_ReportsController_getCallTypePieForCRE58_route(params) =>
      call { 
        controllers_ReportsController_getCallTypePieForCRE58_invoker.call(ReportsController_19.getCallTypePieForCRE())
      }
  
    // @LINE:133
    case controllers_ReportsController_getBookedListByTimeForCRE59_route(params) =>
      call { 
        controllers_ReportsController_getBookedListByTimeForCRE59_invoker.call(ReportsController_19.getBookedListByTimeForCRE())
      }
  
    // @LINE:137
    case controllers_CallInteractionController_getFollowUpRequiredData60_route(params) =>
      call(params.fromPath[String]("selectAgent", None)) { (selectAgent) =>
        controllers_CallInteractionController_getFollowUpRequiredData60_invoker.call(CallInteractionController_12.getFollowUpRequiredData(selectAgent))
      }
  
    // @LINE:138
    case controllers_CallInteractionController_getServiceBookedData61_route(params) =>
      call(params.fromPath[String]("selectAgent", None)) { (selectAgent) =>
        controllers_CallInteractionController_getServiceBookedData61_invoker.call(CallInteractionController_12.getServiceBookedData(selectAgent))
      }
  
    // @LINE:139
    case controllers_CallInteractionController_getNonContactsData62_route(params) =>
      call(params.fromPath[String]("selectAgent", None)) { (selectAgent) =>
        controllers_CallInteractionController_getNonContactsData62_invoker.call(CallInteractionController_12.getNonContactsData(selectAgent))
      }
  
    // @LINE:140
    case controllers_CallInteractionController_getDroppedCallsData63_route(params) =>
      call(params.fromPath[String]("selectAgent", None)) { (selectAgent) =>
        controllers_CallInteractionController_getDroppedCallsData63_invoker.call(CallInteractionController_12.getDroppedCallsData(selectAgent))
      }
  
    // @LINE:141
    case controllers_CallInteractionController_getServiceNotRequiredData64_route(params) =>
      call(params.fromPath[String]("selectAgent", None)) { (selectAgent) =>
        controllers_CallInteractionController_getServiceNotRequiredData64_invoker.call(CallInteractionController_12.getServiceNotRequiredData(selectAgent))
      }
  
    // @LINE:145
    case controllers_CallInfoController_getFollowUpTableDataOfCRE65_route(params) =>
      call { 
        controllers_CallInfoController_getFollowUpTableDataOfCRE65_invoker.call(CallInfoController_26.getFollowUpTableDataOfCRE())
      }
  
    // @LINE:146
    case controllers_CallInfoController_getFollowUpNotifyBeforeTime66_route(params) =>
      call { 
        controllers_CallInfoController_getFollowUpNotifyBeforeTime66_invoker.call(CallInfoController_26.getFollowUpNotifyBeforeTime())
      }
  
    // @LINE:147
    case controllers_CallInfoController_getFollowUpRemainderOfMissedSchedules67_route(params) =>
      call { 
        controllers_CallInfoController_getFollowUpRemainderOfMissedSchedules67_invoker.call(CallInfoController_26.getFollowUpRemainderOfMissedSchedules())
      }
  
    // @LINE:148
    case controllers_CallInfoController_getFollowUpNotificationToday68_route(params) =>
      call { 
        controllers_CallInfoController_getFollowUpNotificationToday68_invoker.call(CallInfoController_26.getFollowUpNotificationToday())
      }
  
    // @LINE:149
    case controllers_PSFController_getPSFFollowUpNotificationToday69_route(params) =>
      call { 
        controllers_PSFController_getPSFFollowUpNotificationToday69_invoker.call(PSFController_14.getPSFFollowUpNotificationToday())
      }
  
    // @LINE:156
    case controllers_CallInfoController_deleteCallLog70_route(params) =>
      call(params.fromQuery[String]("dealercode", None), params.fromPath[Long]("id", None)) { (dealercode, id) =>
        controllers_CallInfoController_deleteCallLog70_invoker.call(CallInfoController_26.deleteCallLog(dealercode, id))
      }
  
    // @LINE:157
    case controllers_CallInfoController_deleteSchCalllog71_route(params) =>
      call(params.fromQuery[String]("dealercode", None), params.fromPath[Long]("id", None)) { (dealercode, id) =>
        controllers_CallInfoController_deleteSchCalllog71_invoker.call(CallInfoController_26.deleteSchCalllog(dealercode, id))
      }
  
    // @LINE:161
    case controllers_WyzUserController_indexPageSalesManager72_route(params) =>
      call { 
        controllers_WyzUserController_indexPageSalesManager72_invoker.call(WyzUserController_11.indexPageSalesManager)
      }
  
    // @LINE:163
    case controllers_WyzUserController_viewReport73_route(params) =>
      call { 
        controllers_WyzUserController_viewReport73_invoker.call(WyzUserController_11.viewReport())
      }
  
    // @LINE:168
    case controllers_Assets_at74_route(params) =>
      call(Param[String]("path", Right("/public")), params.fromPath[String]("file", None)) { (path, file) =>
        controllers_Assets_at74_invoker.call(Assets_23.at(path, file))
      }
  
    // @LINE:176
    case controllers_UserAuthenticator_authenticateUser75_route(params) =>
      call(params.fromQuery[String]("phoneNumber", None), params.fromQuery[String]("phoneIMEINo", None), params.fromQuery[String]("registrationId", None)) { (phoneNumber, phoneIMEINo, registrationId) =>
        controllers_UserAuthenticator_authenticateUser75_invoker.call(UserAuthenticator_16.authenticateUser(phoneNumber, phoneIMEINo, registrationId))
      }
  
    // @LINE:177
    case controllers_CallInfoController_startSyncOperation76_route(params) =>
      call { 
        controllers_CallInfoController_startSyncOperation76_invoker.call(CallInfoController_26.startSyncOperation())
      }
  
    // @LINE:178
    case controllers_CallInfoController_stopSyncOperation77_route(params) =>
      call { 
        controllers_CallInfoController_stopSyncOperation77_invoker.call(CallInfoController_26.stopSyncOperation())
      }
  
    // @LINE:179
    case controllers_UserAuthenticator_updateUserAuthentication78_route(params) =>
      call(params.fromQuery[String]("phoneIMEINo", None), params.fromQuery[String]("registrationId", None)) { (phoneIMEINo, registrationId) =>
        controllers_UserAuthenticator_updateUserAuthentication78_invoker.call(UserAuthenticator_16.updateUserAuthentication(phoneIMEINo, registrationId))
      }
  
    // @LINE:181
    case controllers_SearchController_startSyncOfReports79_route(params) =>
      call { 
        controllers_SearchController_startSyncOfReports79_invoker.call(SearchController_0.startSyncOfReports())
      }
  
    // @LINE:182
    case controllers_CallInfoController_todeleteFilesFromDirectory80_route(params) =>
      call { 
        controllers_CallInfoController_todeleteFilesFromDirectory80_invoker.call(CallInfoController_26.todeleteFilesFromDirectory())
      }
  
    // @LINE:186
    case controllers_CustomerScheduledController_readCustomersFromCSV81_route(params) =>
      call { 
        controllers_CustomerScheduledController_readCustomersFromCSV81_invoker.call(CustomerScheduledController_15.readCustomersFromCSV)
      }
  
    // @LINE:188
    case controllers_CustomerScheduledController_addcustomerInfo82_route(params) =>
      call { 
        controllers_CustomerScheduledController_addcustomerInfo82_invoker.call(CustomerScheduledController_15.addcustomerInfo)
      }
  
    // @LINE:193
    case controllers_ReportsController_getScheduledCallsCountCREMan83_route(params) =>
      call { 
        controllers_ReportsController_getScheduledCallsCountCREMan83_invoker.call(ReportsController_19.getScheduledCallsCountCREMan)
      }
  
    // @LINE:195
    case controllers_ReportsController_getBookedListByTime84_route(params) =>
      call { 
        controllers_ReportsController_getBookedListByTime84_invoker.call(ReportsController_19.getBookedListByTime)
      }
  
    // @LINE:197
    case controllers_ReportsController_getCallTypePie85_route(params) =>
      call { 
        controllers_ReportsController_getCallTypePie85_invoker.call(ReportsController_19.getCallTypePie)
      }
  
    // @LINE:201
    case controllers_CallInteractionController_getCustomerByInteractionId86_route(params) =>
      call { 
        controllers_CallInteractionController_getCustomerByInteractionId86_invoker.call(CallInteractionController_12.getCustomerByInteractionId())
      }
  
    // @LINE:205
    case controllers_CallInteractionController_getAssignedInteraction87_route(params) =>
      call { 
        controllers_CallInteractionController_getAssignedInteraction87_invoker.call(CallInteractionController_12.getAssignedInteraction())
      }
  
    // @LINE:206
    case controllers_CallInteractionController_toUploadInteractionsByCREManager88_route(params) =>
      call { 
        controllers_CallInteractionController_toUploadInteractionsByCREManager88_invoker.call(CallInteractionController_12.toUploadInteractionsByCREManager())
      }
  
    // @LINE:207
    case controllers_CallInteractionController_assigningCallsToCRE89_route(params) =>
      call(params.fromPath[String]("selectAgent", None), params.fromPath[String]("fromData", None), params.fromPath[String]("toDate", None)) { (selectAgent, fromData, toDate) =>
        controllers_CallInteractionController_assigningCallsToCRE89_invoker.call(CallInteractionController_12.assigningCallsToCRE(selectAgent, fromData, toDate))
      }
  
    // @LINE:208
    case controllers_CallInteractionController_getAssignedCallsOfUser90_route(params) =>
      call(params.fromPath[String]("selectAgent", None)) { (selectAgent) =>
        controllers_CallInteractionController_getAssignedCallsOfUser90_invoker.call(CallInteractionController_12.getAssignedCallsOfUser(selectAgent))
      }
  
    // @LINE:212
    case controllers_CallInteractionController_searchByCustomer91_route(params) =>
      call { 
        controllers_CallInteractionController_searchByCustomer91_invoker.call(CallInteractionController_12.searchByCustomer())
      }
  
    // @LINE:213
    case controllers_CallInteractionController_addCustomerCRE92_route(params) =>
      call { 
        controllers_CallInteractionController_addCustomerCRE92_invoker.call(CallInteractionController_12.addCustomerCRE())
      }
  
    // @LINE:214
    case controllers_SearchController_searchCustomer93_route(params) =>
      call { 
        controllers_SearchController_searchCustomer93_invoker.call(SearchController_0.searchCustomer())
      }
  
    // @LINE:215
    case controllers_SMSTemplateController_checkIfSearchNameExists94_route(params) =>
      call(params.fromPath[String]("searchnamevalue", None)) { (searchnamevalue) =>
        controllers_SMSTemplateController_checkIfSearchNameExists94_invoker.call(SMSTemplateController_2.checkIfSearchNameExists(searchnamevalue))
      }
  
    // @LINE:220
    case controllers_CallInteractionController_postAddCustomer95_route(params) =>
      call { 
        controllers_CallInteractionController_postAddCustomer95_invoker.call(CallInteractionController_12.postAddCustomer())
      }
  
    // @LINE:222
    case controllers_CallInteractionController_getMediaFileCallInteractions96_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_CallInteractionController_getMediaFileCallInteractions96_invoker.call(CallInteractionController_12.getMediaFileCallInteractions(id))
      }
  
    // @LINE:224
    case controllers_CallInteractionController_getPageForAssigningCalls97_route(params) =>
      call { 
        controllers_CallInteractionController_getPageForAssigningCalls97_invoker.call(CallInteractionController_12.getPageForAssigningCalls())
      }
  
    // @LINE:225
    case controllers_CallInteractionController_getSelectedAssignCalls98_route(params) =>
      call { 
        controllers_CallInteractionController_getSelectedAssignCalls98_invoker.call(CallInteractionController_12.getSelectedAssignCalls())
      }
  
    // @LINE:227
    case controllers_CallInteractionController_getAssignedList99_route(params) =>
      call { 
        controllers_CallInteractionController_getAssignedList99_invoker.call(CallInteractionController_12.getAssignedList())
      }
  
    // @LINE:228
    case controllers_CallInteractionController_toChangeAssigment100_route(params) =>
      call { 
        controllers_CallInteractionController_toChangeAssigment100_invoker.call(CallInteractionController_12.toChangeAssigment())
      }
  
    // @LINE:229
    case controllers_CallInteractionController_getReAssignmentCalls101_route(params) =>
      call(params.fromPath[String]("selectAgent", None)) { (selectAgent) =>
        controllers_CallInteractionController_getReAssignmentCalls101_invoker.call(CallInteractionController_12.getReAssignmentCalls(selectAgent))
      }
  
    // @LINE:230
    case controllers_CallInteractionController_reAssigningCallsofselectCRE102_route(params) =>
      call { 
        controllers_CallInteractionController_reAssigningCallsofselectCRE102_invoker.call(CallInteractionController_12.reAssigningCallsofselectCRE())
      }
  
    // @LINE:234
    case controllers_CallInteractionController_upload_file_Format_show103_route(params) =>
      call { 
        controllers_CallInteractionController_upload_file_Format_show103_invoker.call(CallInteractionController_12.upload_file_Format_show())
      }
  
    // @LINE:235
    case controllers_CallInteractionController_upload_file_Format_submit104_route(params) =>
      call { 
        controllers_CallInteractionController_upload_file_Format_submit104_invoker.call(CallInteractionController_12.upload_file_Format_submit())
      }
  
    // @LINE:236
    case controllers_CallInteractionController_ajax_master_data_upload_fileFormat105_route(params) =>
      call(params.fromPath[String]("selected_part", None)) { (selected_part) =>
        controllers_CallInteractionController_ajax_master_data_upload_fileFormat105_invoker.call(CallInteractionController_12.ajax_master_data_upload_fileFormat(selected_part))
      }
  
    // @LINE:237
    case controllers_CallInteractionController_upload_Excel_Sheet106_route(params) =>
      call(params.fromPath[String]("uploadId", None), params.fromPath[String]("uploadType", None), params.fromPath[String]("resultData", None)) { (uploadId, uploadType, resultData) =>
        controllers_CallInteractionController_upload_Excel_Sheet106_invoker.call(CallInteractionController_12.upload_Excel_Sheet(uploadId, uploadType, resultData))
      }
  
    // @LINE:238
    case controllers_CallInteractionController_get_required_fields107_route(params) =>
      call(params.fromPath[String]("selected_part", None)) { (selected_part) =>
        controllers_CallInteractionController_get_required_fields107_invoker.call(CallInteractionController_12.get_required_fields(selected_part))
      }
  
    // @LINE:239
    case controllers_CallInteractionController_ajax_transaction_data_upload108_route(params) =>
      call(params.fromPath[String]("selected_format", None)) { (selected_format) =>
        controllers_CallInteractionController_ajax_transaction_data_upload108_invoker.call(CallInteractionController_12.ajax_transaction_data_upload(selected_format))
      }
  
    // @LINE:243
    case controllers_CallInteractionController_getDownloadExcelFormat109_route(params) =>
      call { 
        controllers_CallInteractionController_getDownloadExcelFormat109_invoker.call(CallInteractionController_12.getDownloadExcelFormat())
      }
  
    // @LINE:244
    case controllers_CallInteractionController_getExcelColumnsOFUploadFormat110_route(params) =>
      call(params.fromPath[String]("upload_format", None)) { (upload_format) =>
        controllers_CallInteractionController_getExcelColumnsOFUploadFormat110_invoker.call(CallInteractionController_12.getExcelColumnsOFUploadFormat(upload_format))
      }
  
    // @LINE:248
    case controllers_SearchController_uploadHistoryReport111_route(params) =>
      call(params.fromPath[String]("uploadId", None), params.fromPath[String]("uploadType", None)) { (uploadId, uploadType) =>
        controllers_SearchController_uploadHistoryReport111_invoker.call(SearchController_0.uploadHistoryReport(uploadId, uploadType))
      }
  
    // @LINE:249
    case controllers_SearchController_uploadHistoryViewData112_route(params) =>
      call(params.fromPath[String]("typeIds", None), params.fromPath[String]("fromDate", None), params.fromPath[String]("toDate", None), params.fromPath[String]("uploadReportId", None)) { (typeIds, fromDate, toDate, uploadReportId) =>
        controllers_SearchController_uploadHistoryViewData112_invoker.call(SearchController_0.uploadHistoryViewData(typeIds, fromDate, toDate, uploadReportId))
      }
  
    // @LINE:252
    case controllers_CallInteractionController_addComplaints113_route(params) =>
      call { 
        controllers_CallInteractionController_addComplaints113_invoker.call(CallInteractionController_12.addComplaints())
      }
  
    // @LINE:253
    case controllers_CallInteractionController_postComplaints114_route(params) =>
      call { 
        controllers_CallInteractionController_postComplaints114_invoker.call(CallInteractionController_12.postComplaints())
      }
  
    // @LINE:255
    case controllers_CallInteractionController_assignComplaints115_route(params) =>
      call { 
        controllers_CallInteractionController_assignComplaints115_invoker.call(CallInteractionController_12.assignComplaints())
      }
  
    // @LINE:256
    case controllers_CallInteractionController_updateComplaints116_route(params) =>
      call(params.fromPath[Long]("id", None), params.fromPath[String]("comments", None), params.fromPath[String]("selected_value", None)) { (id, comments, selected_value) =>
        controllers_CallInteractionController_updateComplaints116_invoker.call(CallInteractionController_12.updateComplaints(id, comments, selected_value))
      }
  
    // @LINE:257
    case controllers_CallInteractionController_addComplaintsByManager117_route(params) =>
      call { 
        controllers_CallInteractionController_addComplaintsByManager117_invoker.call(CallInteractionController_12.addComplaintsByManager())
      }
  
    // @LINE:258
    case controllers_CallInteractionController_postComplaintsByManager118_route(params) =>
      call { 
        controllers_CallInteractionController_postComplaintsByManager118_invoker.call(CallInteractionController_12.postComplaintsByManager())
      }
  
    // @LINE:261
    case controllers_FirebaseSyncController_startSyncOperationServiceAgent119_route(params) =>
      call { 
        controllers_FirebaseSyncController_startSyncOperationServiceAgent119_invoker.call(FirebaseSyncController_6.startSyncOperationServiceAgent())
      }
  
    // @LINE:262
    case controllers_FirebaseSyncController_startServiceAdvisorHistorySync120_route(params) =>
      call { 
        controllers_FirebaseSyncController_startServiceAdvisorHistorySync120_invoker.call(FirebaseSyncController_6.startServiceAdvisorHistorySync())
      }
  
    // @LINE:263
    case controllers_FirebaseSyncController_startServiceAdvisorSummaryDetailSync121_route(params) =>
      call { 
        controllers_FirebaseSyncController_startServiceAdvisorSummaryDetailSync121_invoker.call(FirebaseSyncController_6.startServiceAdvisorSummaryDetailSync())
      }
  
    // @LINE:265
    case controllers_FirebaseSyncController_stopSyncOperationServiceAgent122_route(params) =>
      call { 
        controllers_FirebaseSyncController_stopSyncOperationServiceAgent122_invoker.call(FirebaseSyncController_6.stopSyncOperationServiceAgent())
      }
  
    // @LINE:266
    case controllers_FirebaseSyncController_stopServiceAdvisorHistorySync123_route(params) =>
      call { 
        controllers_FirebaseSyncController_stopServiceAdvisorHistorySync123_invoker.call(FirebaseSyncController_6.stopServiceAdvisorHistorySync())
      }
  
    // @LINE:267
    case controllers_FirebaseSyncController_stopServiceAdvisorSummaryDetailSync124_route(params) =>
      call { 
        controllers_FirebaseSyncController_stopServiceAdvisorSummaryDetailSync124_invoker.call(FirebaseSyncController_6.stopServiceAdvisorSummaryDetailSync())
      }
  
    // @LINE:270
    case controllers_FirebaseSyncController_startSyncOperationPSFCallServiceAgent125_route(params) =>
      call { 
        controllers_FirebaseSyncController_startSyncOperationPSFCallServiceAgent125_invoker.call(FirebaseSyncController_6.startSyncOperationPSFCallServiceAgent())
      }
  
    // @LINE:271
    case controllers_FirebaseSyncController_stopSyncOperationPSFCallServiceAgent126_route(params) =>
      call { 
        controllers_FirebaseSyncController_stopSyncOperationPSFCallServiceAgent126_invoker.call(FirebaseSyncController_6.stopSyncOperationPSFCallServiceAgent())
      }
  
    // @LINE:273
    case controllers_FirebaseSyncController_startServiceAdvisorPSFHistorySync127_route(params) =>
      call { 
        controllers_FirebaseSyncController_startServiceAdvisorPSFHistorySync127_invoker.call(FirebaseSyncController_6.startServiceAdvisorPSFHistorySync())
      }
  
    // @LINE:276
    case controllers_CallInteractionController_ajaxAddPhoneNumber128_route(params) =>
      call(params.fromPath[String]("phone_no", None), params.fromPath[Long]("customer_id", None)) { (phone_no, customer_id) =>
        controllers_CallInteractionController_ajaxAddPhoneNumber128_invoker.call(CallInteractionController_12.ajaxAddPhoneNumber(phone_no, customer_id))
      }
  
    // @LINE:277
    case controllers_CallInteractionController_ajaxCallForAddcustomerinfo129_route(params) =>
      call { 
        controllers_CallInteractionController_ajaxCallForAddcustomerinfo129_invoker.call(CallInteractionController_12.ajaxCallForAddcustomerinfo())
      }
  
    // @LINE:281
    case controllers_CallInteractionController_ajaxAddChassisno130_route(params) =>
      call(params.fromPath[String]("chassisNo", None), params.fromPath[Long]("customer_id", None)) { (chassisNo, customer_id) =>
        controllers_CallInteractionController_ajaxAddChassisno130_invoker.call(CallInteractionController_12.ajaxAddChassisno(chassisNo, customer_id))
      }
  
    // @LINE:282
    case controllers_CallInteractionController_ajaxAddRegistrationno131_route(params) =>
      call(params.fromPath[String]("vehicalRegNo", None), params.fromPath[Long]("customer_id", None)) { (vehicalRegNo, customer_id) =>
        controllers_CallInteractionController_ajaxAddRegistrationno131_invoker.call(CallInteractionController_12.ajaxAddRegistrationno(vehicalRegNo, customer_id))
      }
  
    // @LINE:283
    case controllers_CallInteractionController_ajaxAddEngineno132_route(params) =>
      call(params.fromPath[String]("engineNo", None), params.fromPath[Long]("customer_id", None)) { (engineNo, customer_id) =>
        controllers_CallInteractionController_ajaxAddEngineno132_invoker.call(CallInteractionController_12.ajaxAddEngineno(engineNo, customer_id))
      }
  
    // @LINE:285
    case controllers_CallInteractionController_saveaddcustomermobno133_route(params) =>
      call(params.fromPath[String]("custMobNo", None), params.fromPath[Long]("wyzUser_id", None), params.fromPath[Long]("customer_Id", None)) { (custMobNo, wyzUser_id, customer_Id) =>
        controllers_CallInteractionController_saveaddcustomermobno133_invoker.call(CallInteractionController_12.saveaddcustomermobno(custMobNo, wyzUser_id, customer_Id))
      }
  
    // @LINE:286
    case controllers_CallInteractionController_saveaddcustomerEmail134_route(params) =>
      call(params.fromPath[String]("custEmail", None), params.fromPath[Long]("wyzUser_id", None), params.fromPath[Long]("customer_Id", None)) { (custEmail, wyzUser_id, customer_Id) =>
        controllers_CallInteractionController_saveaddcustomerEmail134_invoker.call(CallInteractionController_12.saveaddcustomerEmail(custEmail, wyzUser_id, customer_Id))
      }
  
    // @LINE:294
    case controllers_AllCallInteractionController_viewAllCallIntearctions135_route(params) =>
      call { 
        controllers_AllCallInteractionController_viewAllCallIntearctions135_invoker.call(AllCallInteractionController_4.viewAllCallIntearctions())
      }
  
    // @LINE:298
    case controllers_AllCallInteractionController_getAllCallInteractions136_route(params) =>
      call { 
        controllers_AllCallInteractionController_getAllCallInteractions136_invoker.call(AllCallInteractionController_4.getAllCallInteractions())
      }
  
    // @LINE:299
    case controllers_AllCallInteractionController_downloadCallHistoryReport137_route(params) =>
      call { 
        controllers_AllCallInteractionController_downloadCallHistoryReport137_invoker.call(AllCallInteractionController_4.downloadCallHistoryReport())
      }
  
    // @LINE:302
    case controllers_CallInteractionController_getWorkShopServiceBooked138_route(params) =>
      call(params.fromPath[Long]("workId", None), params.fromPath[Long]("userId", None)) { (workId, userId) =>
        controllers_CallInteractionController_getWorkShopServiceBooked138_invoker.call(CallInteractionController_12.getWorkShopServiceBooked(workId, userId))
      }
  
    // @LINE:306
    case controllers_CallInteractionController_getSerAdvServiceBooked139_route(params) =>
      call(params.fromPath[Long]("serviceAdvisorId", None), params.fromPath[Long]("userId", None)) { (serviceAdvisorId, userId) =>
        controllers_CallInteractionController_getSerAdvServiceBooked139_invoker.call(CallInteractionController_12.getSerAdvServiceBooked(serviceAdvisorId, userId))
      }
  
    // @LINE:310
    case controllers_CallInteractionController_getDriverServiceBooked140_route(params) =>
      call(params.fromPath[Long]("driverId", None), params.fromPath[Long]("userId", None)) { (driverId, userId) =>
        controllers_CallInteractionController_getDriverServiceBooked140_invoker.call(CallInteractionController_12.getDriverServiceBooked(driverId, userId))
      }
  
    // @LINE:311
    case controllers_CallInteractionController_reAssignAgentUpdate141_route(params) =>
      call(params.fromPath[Long]("rowId", None), params.fromPath[Long]("wyzUserId", None), params.fromPath[Long]("post_id", None)) { (rowId, wyzUserId, post_id) =>
        controllers_CallInteractionController_reAssignAgentUpdate141_invoker.call(CallInteractionController_12.reAssignAgentUpdate(rowId, wyzUserId, post_id))
      }
  
    // @LINE:315
    case controllers_CallInteractionController_getListWorkshopByLocation142_route(params) =>
      call(params.fromPath[String]("selectedCity", None)) { (selectedCity) =>
        controllers_CallInteractionController_getListWorkshopByLocation142_invoker.call(CallInteractionController_12.getListWorkshopByLocation(selectedCity))
      }
  
    // @LINE:320
    case controllers_CallInteractionController_getWorkshopSummaryDetails143_route(params) =>
      call(params.fromPath[Long]("selectedWorkshop", None), params.fromPath[String]("schDate", None)) { (selectedWorkshop, schDate) =>
        controllers_CallInteractionController_getWorkshopSummaryDetails143_invoker.call(CallInteractionController_12.getWorkshopSummaryDetails(selectedWorkshop, schDate))
      }
  
    // @LINE:324
    case controllers_CallInteractionController_ajaxsearchVehicle144_route(params) =>
      call(params.fromPath[String]("veh_number", None)) { (veh_number) =>
        controllers_CallInteractionController_ajaxsearchVehicle144_invoker.call(CallInteractionController_12.ajaxsearchVehicle(veh_number))
      }
  
    // @LINE:325
    case controllers_CallInteractionController_complaintsResolution145_route(params) =>
      call { 
        controllers_CallInteractionController_complaintsResolution145_invoker.call(CallInteractionController_12.complaintsResolution())
      }
  
    // @LINE:326
    case controllers_CallInteractionController_ajaxcomplaintNumber146_route(params) =>
      call(params.fromPath[String]("complaintNum", None), params.fromPath[String]("veh_num", None)) { (complaintNum, veh_num) =>
        controllers_CallInteractionController_ajaxcomplaintNumber146_invoker.call(CallInteractionController_12.ajaxcomplaintNumber(complaintNum, veh_num))
      }
  
    // @LINE:327
    case controllers_CallInteractionController_ajaxcomplaintNumberClosed147_route(params) =>
      call(params.fromPath[String]("complaintNumClosed", None), params.fromPath[String]("veh_numclosed", None)) { (complaintNumClosed, veh_numclosed) =>
        controllers_CallInteractionController_ajaxcomplaintNumberClosed147_invoker.call(CallInteractionController_12.ajaxcomplaintNumberClosed(complaintNumClosed, veh_numclosed))
      }
  
    // @LINE:329
    case controllers_CallInteractionController_updateComplaintsResolution148_route(params) =>
      call(params.fromPath[String]("complaintNum", None), params.fromPath[String]("reasonFor", None), params.fromPath[String]("complaintStatus", None), params.fromPath[String]("customerStatus", None), params.fromPath[String]("actionTaken", None), params.fromPath[String]("resolutionBy", None)) { (complaintNum, reasonFor, complaintStatus, customerStatus, actionTaken, resolutionBy) =>
        controllers_CallInteractionController_updateComplaintsResolution148_invoker.call(CallInteractionController_12.updateComplaintsResolution(complaintNum, reasonFor, complaintStatus, customerStatus, actionTaken, resolutionBy))
      }
  
    // @LINE:330
    case controllers_CallInteractionController_updateComplaintsResolutionByManager149_route(params) =>
      call(params.fromPath[String]("complaintNum", None), params.fromPath[String]("reasonFor", None), params.fromPath[String]("complaintStatus", None), params.fromPath[String]("customerStatus", None), params.fromPath[String]("actionTaken", None), params.fromPath[String]("resolutionBy", None)) { (complaintNum, reasonFor, complaintStatus, customerStatus, actionTaken, resolutionBy) =>
        controllers_CallInteractionController_updateComplaintsResolutionByManager149_invoker.call(CallInteractionController_12.updateComplaintsResolutionByManager(complaintNum, reasonFor, complaintStatus, customerStatus, actionTaken, resolutionBy))
      }
  
    // @LINE:331
    case controllers_CallInteractionController_updateComplaintsResolutionClosed150_route(params) =>
      call(params.fromPath[String]("complaintNumClosed", None), params.fromPath[String]("reasonForClosed", None), params.fromPath[String]("complaintStatusClosed", None), params.fromPath[String]("customerStatusClosed", None), params.fromPath[String]("actionTakenClosed", None), params.fromPath[String]("resolutionByClosed", None)) { (complaintNumClosed, reasonForClosed, complaintStatusClosed, customerStatusClosed, actionTakenClosed, resolutionByClosed) =>
        controllers_CallInteractionController_updateComplaintsResolutionClosed150_invoker.call(CallInteractionController_12.updateComplaintsResolutionClosed(complaintNumClosed, reasonForClosed, complaintStatusClosed, customerStatusClosed, actionTakenClosed, resolutionByClosed))
      }
  
    // @LINE:333
    case controllers_CallInteractionController_complaintAssignment151_route(params) =>
      call(params.fromPath[String]("complaintNum", None), params.fromPath[String]("veh_num", None)) { (complaintNum, veh_num) =>
        controllers_CallInteractionController_complaintAssignment151_invoker.call(CallInteractionController_12.complaintAssignment(complaintNum, veh_num))
      }
  
    // @LINE:336
    case controllers_CallInteractionController_saveaddComplaintAssignModile152_route(params) =>
      call(params.fromPath[String]("complaintNum", None), params.fromPath[String]("city", None), params.fromPath[String]("workshop", None), params.fromPath[String]("functions", None), params.fromPath[String]("ownership", None), params.fromPath[String]("priority", None), params.fromPath[String]("esclation1", None), params.fromPath[String]("esclation2", None)) { (complaintNum, city, workshop, functions, ownership, priority, esclation1, esclation2) =>
        controllers_CallInteractionController_saveaddComplaintAssignModile152_invoker.call(CallInteractionController_12.saveaddComplaintAssignModile(complaintNum, city, workshop, functions, ownership, priority, esclation1, esclation2))
      }
  
    // @LINE:337
    case controllers_CallInteractionController_viewAllComplaints1153_route(params) =>
      call { 
        controllers_CallInteractionController_viewAllComplaints1153_invoker.call(CallInteractionController_12.viewAllComplaints1())
      }
  
    // @LINE:340
    case controllers_CallInteractionController_downloadExcelFile154_route(params) =>
      call { 
        controllers_CallInteractionController_downloadExcelFile154_invoker.call(CallInteractionController_12.downloadExcelFile())
      }
  
    // @LINE:343
    case controllers_CallInteractionController_getComplaintsDataByFilter155_route(params) =>
      call(params.fromPath[String]("filterData", None), params.fromPath[String]("varLoc", None), params.fromPath[String]("varfunc", None), params.fromPath[String]("varraisedDate", None), params.fromPath[String]("varendDate", None)) { (filterData, varLoc, varfunc, varraisedDate, varendDate) =>
        controllers_CallInteractionController_getComplaintsDataByFilter155_invoker.call(CallInteractionController_12.getComplaintsDataByFilter(filterData, varLoc, varfunc, varraisedDate, varendDate))
      }
  
    // @LINE:344
    case controllers_CallInteractionController_getComplaintHistoryAll156_route(params) =>
      call(params.fromPath[String]("complaintNumber", None), params.fromPath[String]("vehregnumber", None)) { (complaintNumber, vehregnumber) =>
        controllers_CallInteractionController_getComplaintHistoryAll156_invoker.call(CallInteractionController_12.getComplaintHistoryAll(complaintNumber, vehregnumber))
      }
  
    // @LINE:348
    case controllers_CallInteractionController_updateRangeOfUnavailabiltyOfUsers157_route(params) =>
      call { 
        controllers_CallInteractionController_updateRangeOfUnavailabiltyOfUsers157_invoker.call(CallInteractionController_12.updateRangeOfUnavailabiltyOfUsers())
      }
  
    // @LINE:349
    case controllers_CallInteractionController_postUpdateRangeOfUnavailabiltyOfUsers158_route(params) =>
      call { 
        controllers_CallInteractionController_postUpdateRangeOfUnavailabiltyOfUsers158_invoker.call(CallInteractionController_12.postUpdateRangeOfUnavailabiltyOfUsers)
      }
  
    // @LINE:351
    case controllers_CallInteractionController_getRoasterTable159_route(params) =>
      call { 
        controllers_CallInteractionController_getRoasterTable159_invoker.call(CallInteractionController_12.getRoasterTable())
      }
  
    // @LINE:352
    case controllers_CallInteractionController_toUpdateRoasterUnAvailablity160_route(params) =>
      call { 
        controllers_CallInteractionController_toUpdateRoasterUnAvailablity160_invoker.call(CallInteractionController_12.toUpdateRoasterUnAvailablity())
      }
  
    // @LINE:354
    case controllers_CallInteractionController_rosterOfUnavailabiltyByUser161_route(params) =>
      call { 
        controllers_CallInteractionController_rosterOfUnavailabiltyByUser161_invoker.call(CallInteractionController_12.rosterOfUnavailabiltyByUser())
      }
  
    // @LINE:355
    case controllers_CallInteractionController_addRosterOfUserByAjax162_route(params) =>
      call(params.fromPath[String]("selectAgent", None), params.fromPath[String]("fromDate", None), params.fromPath[String]("toDate", None)) { (selectAgent, fromDate, toDate) =>
        controllers_CallInteractionController_addRosterOfUserByAjax162_invoker.call(CallInteractionController_12.addRosterOfUserByAjax(selectAgent, fromDate, toDate))
      }
  
    // @LINE:357
    case controllers_CallInteractionController_getRosterDataByUser163_route(params) =>
      call(params.fromPath[String]("selectAgent", None)) { (selectAgent) =>
        controllers_CallInteractionController_getRosterDataByUser163_invoker.call(CallInteractionController_12.getRosterDataByUser(selectAgent))
      }
  
    // @LINE:358
    case controllers_CallInteractionController_updateRosterOfUserByAjaxVal164_route(params) =>
      call(params.fromPath[String]("selectAgent", None), params.fromPath[String]("From_Date", None), params.fromPath[String]("To_Date", None)) { (selectAgent, From_Date, To_Date) =>
        controllers_CallInteractionController_updateRosterOfUserByAjaxVal164_invoker.call(CallInteractionController_12.updateRosterOfUserByAjaxVal(selectAgent, From_Date, To_Date))
      }
  
    // @LINE:359
    case controllers_CallInteractionController_deleteUnavialbility165_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_CallInteractionController_deleteUnavialbility165_invoker.call(CallInteractionController_12.deleteUnavialbility(id))
      }
  
    // @LINE:362
    case controllers_CallInteractionController_getServiceDataOfCustomer166_route(params) =>
      call(params.fromPath[Long]("customerId", None)) { (customerId) =>
        controllers_CallInteractionController_getServiceDataOfCustomer166_invoker.call(CallInteractionController_12.getServiceDataOfCustomer(customerId))
      }
  
    // @LINE:363
    case controllers_CallInteractionController_getCallHistoryOfvehicalId167_route(params) =>
      call(params.fromPath[Long]("vehicalId", None)) { (vehicalId) =>
        controllers_CallInteractionController_getCallHistoryOfvehicalId167_invoker.call(CallInteractionController_12.getCallHistoryOfvehicalId(vehicalId))
      }
  
    // @LINE:364
    case controllers_CallInteractionController_getServiceAdvisorOfWorkshop168_route(params) =>
      call(params.fromPath[Long]("workshopId", None)) { (workshopId) =>
        controllers_CallInteractionController_getServiceAdvisorOfWorkshop168_invoker.call(CallInteractionController_12.getServiceAdvisorOfWorkshop(workshopId))
      }
  
    // @LINE:365
    case controllers_CallInteractionController_getWorkshopListName169_route(params) =>
      call(params.fromPath[Long]("workshopId", None)) { (workshopId) =>
        controllers_CallInteractionController_getWorkshopListName169_invoker.call(CallInteractionController_12.getWorkshopListName(workshopId))
      }
  
    // @LINE:366
    case controllers_CallInteractionController_getLeadByUserLocation170_route(params) =>
      call(params.fromPath[Long]("userLocation", None)) { (userLocation) =>
        controllers_CallInteractionController_getLeadByUserLocation170_invoker.call(CallInteractionController_12.getLeadByUserLocation(userLocation))
      }
  
    // @LINE:367
    case controllers_CallInteractionController_getLeadTagByDepartment171_route(params) =>
      call(params.fromPath[Long]("userLocation", None), params.fromPath[Long]("departmentName", None)) { (userLocation, departmentName) =>
        controllers_CallInteractionController_getLeadTagByDepartment171_invoker.call(CallInteractionController_12.getLeadTagByDepartment(userLocation, departmentName))
      }
  
    // @LINE:368
    case controllers_CallInteractionController_getTagNameByUpselLeadType172_route(params) =>
      call(params.fromPath[Long]("userLocation", None), params.fromPath[Long]("upselIDTag", None)) { (userLocation, upselIDTag) =>
        controllers_CallInteractionController_getTagNameByUpselLeadType172_invoker.call(CallInteractionController_12.getTagNameByUpselLeadType(userLocation, upselIDTag))
      }
  
    // @LINE:369
    case controllers_CallInteractionController_getComplaintHistoryVeh173_route(params) =>
      call(params.fromPath[Long]("vehicalId", None)) { (vehicalId) =>
        controllers_CallInteractionController_getComplaintHistoryVeh173_invoker.call(CallInteractionController_12.getComplaintHistoryVeh(vehicalId))
      }
  
    // @LINE:370
    case controllers_CallInteractionController_getSMSHistoryOfCustomer174_route(params) =>
      call(params.fromPath[Long]("customerId", None)) { (customerId) =>
        controllers_CallInteractionController_getSMSHistoryOfCustomer174_invoker.call(CallInteractionController_12.getSMSHistoryOfCustomer(customerId))
      }
  
    // @LINE:378
    case controllers_SearchController_assignedInteractionData175_route(params) =>
      call { 
        controllers_SearchController_assignedInteractionData175_invoker.call(SearchController_0.assignedInteractionData())
      }
  
    // @LINE:379
    case controllers_SearchController_followUpRequiredInteractionData176_route(params) =>
      call { 
        controllers_SearchController_followUpRequiredInteractionData176_invoker.call(SearchController_0.followUpRequiredInteractionData())
      }
  
    // @LINE:380
    case controllers_SearchController_serviceBookedInteractionData177_route(params) =>
      call { 
        controllers_SearchController_serviceBookedInteractionData177_invoker.call(SearchController_0.serviceBookedInteractionData())
      }
  
    // @LINE:381
    case controllers_SearchController_serviceNotRequiredInteractionData178_route(params) =>
      call { 
        controllers_SearchController_serviceNotRequiredInteractionData178_invoker.call(SearchController_0.serviceNotRequiredInteractionData())
      }
  
    // @LINE:382
    case controllers_SearchController_nonContactsInteractionData179_route(params) =>
      call { 
        controllers_SearchController_nonContactsInteractionData179_invoker.call(SearchController_0.nonContactsInteractionData())
      }
  
    // @LINE:383
    case controllers_SearchController_droppedCallInteractionData180_route(params) =>
      call { 
        controllers_SearchController_droppedCallInteractionData180_invoker.call(SearchController_0.droppedCallInteractionData())
      }
  
    // @LINE:387
    case controllers_UploadExcelController_uploadExcelData181_route(params) =>
      call { 
        controllers_UploadExcelController_uploadExcelData181_invoker.call(UploadExcelController_24.uploadExcelData())
      }
  
    // @LINE:389
    case controllers_FirebaseSyncController_startSyncOperationDriverPickupDropList182_route(params) =>
      call { 
        controllers_FirebaseSyncController_startSyncOperationDriverPickupDropList182_invoker.call(FirebaseSyncController_6.startSyncOperationDriverPickupDropList())
      }
  
    // @LINE:393
    case controllers_WyzUserController_indexPageServiceAdvisor183_route(params) =>
      call { 
        controllers_WyzUserController_indexPageServiceAdvisor183_invoker.call(WyzUserController_11.indexPageServiceAdvisor())
      }
  
    // @LINE:395
    case controllers_WyzUserController_changepasswordServiceAdvisor184_route(params) =>
      call { 
        controllers_WyzUserController_changepasswordServiceAdvisor184_invoker.call(WyzUserController_11.changepasswordServiceAdvisor)
      }
  
    // @LINE:396
    case controllers_WyzUserController_changepasswordeditingServiceAdvisor185_route(params) =>
      call { 
        controllers_WyzUserController_changepasswordeditingServiceAdvisor185_invoker.call(WyzUserController_11.changepasswordeditingServiceAdvisor())
      }
  
    // @LINE:400
    case controllers_WyzUserController_indexPageOthers186_route(params) =>
      call { 
        controllers_WyzUserController_indexPageOthers186_invoker.call(WyzUserController_11.indexPageOthers())
      }
  
    // @LINE:403
    case controllers_CampaignController_addCampaign187_route(params) =>
      call { 
        controllers_CampaignController_addCampaign187_invoker.call(CampaignController_8.addCampaign())
      }
  
    // @LINE:404
    case controllers_CampaignController_postCampaign188_route(params) =>
      call { 
        controllers_CampaignController_postCampaign188_invoker.call(CampaignController_8.postCampaign())
      }
  
    // @LINE:412
    case controllers_PSFController_getPSFList189_route(params) =>
      call { 
        controllers_PSFController_getPSFList189_invoker.call(PSFController_14.getPSFList())
      }
  
    // @LINE:414
    case controllers_PSFController_getPSF15List190_route(params) =>
      call { 
        controllers_PSFController_getPSF15List190_invoker.call(PSFController_14.getPSF15List())
      }
  
    // @LINE:416
    case controllers_PSFController_getPSF30List191_route(params) =>
      call { 
        controllers_PSFController_getPSF30List191_invoker.call(PSFController_14.getPSF30List())
      }
  
    // @LINE:418
    case controllers_PSFController_getPSF3rdDayList192_route(params) =>
      call { 
        controllers_PSFController_getPSF3rdDayList192_invoker.call(PSFController_14.getPSF3rdDayList())
      }
  
    // @LINE:420
    case controllers_PSFController_getPSFNextDayList193_route(params) =>
      call { 
        controllers_PSFController_getPSFNextDayList193_invoker.call(PSFController_14.getPSFNextDayList())
      }
  
    // @LINE:422
    case controllers_PSFController_getPSF4thDayList194_route(params) =>
      call { 
        controllers_PSFController_getPSF4thDayList194_invoker.call(PSFController_14.getPSF4thDayList())
      }
  
    // @LINE:426
    case controllers_PSFController_assignedInteractionPSFData195_route(params) =>
      call(params.fromPath[String]("name", None)) { (name) =>
        controllers_PSFController_assignedInteractionPSFData195_invoker.call(PSFController_14.assignedInteractionPSFData(name))
      }
  
    // @LINE:427
    case controllers_PSFController_ajaxCallForFollowUpRequiredPSFData196_route(params) =>
      call(params.fromPath[String]("psfDay", None)) { (psfDay) =>
        controllers_PSFController_ajaxCallForFollowUpRequiredPSFData196_invoker.call(PSFController_14.ajaxCallForFollowUpRequiredPSFData(psfDay))
      }
  
    // @LINE:428
    case controllers_PSFController_ajaxCallForIncompletedSurveyPSFData197_route(params) =>
      call(params.fromPath[String]("psfDay", None)) { (psfDay) =>
        controllers_PSFController_ajaxCallForIncompletedSurveyPSFData197_invoker.call(PSFController_14.ajaxCallForIncompletedSurveyPSFData(psfDay))
      }
  
    // @LINE:429
    case controllers_PSFController_ajaxCallForCompletedSurveyPSFData198_route(params) =>
      call(params.fromPath[String]("psfDay", None)) { (psfDay) =>
        controllers_PSFController_ajaxCallForCompletedSurveyPSFData198_invoker.call(PSFController_14.ajaxCallForCompletedSurveyPSFData(psfDay))
      }
  
    // @LINE:430
    case controllers_PSFController_ajaxCallForDissatisfiedPSFData199_route(params) =>
      call(params.fromPath[String]("psfDay", None)) { (psfDay) =>
        controllers_PSFController_ajaxCallForDissatisfiedPSFData199_invoker.call(PSFController_14.ajaxCallForDissatisfiedPSFData(psfDay))
      }
  
    // @LINE:431
    case controllers_PSFController_ajaxCallForNonContactsPSFData200_route(params) =>
      call(params.fromPath[String]("psfDay", None)) { (psfDay) =>
        controllers_PSFController_ajaxCallForNonContactsPSFData200_invoker.call(PSFController_14.ajaxCallForNonContactsPSFData(psfDay))
      }
  
    // @LINE:432
    case controllers_PSFController_ajaxCallForDroppedCallsPSFData201_route(params) =>
      call(params.fromPath[String]("psfDay", None)) { (psfDay) =>
        controllers_PSFController_ajaxCallForDroppedCallsPSFData201_invoker.call(PSFController_14.ajaxCallForDroppedCallsPSFData(psfDay))
      }
  
    // @LINE:433
    case controllers_PSFController_ajaxCallForAppointmentPSFData202_route(params) =>
      call(params.fromPath[String]("psfDay", None)) { (psfDay) =>
        controllers_PSFController_ajaxCallForAppointmentPSFData202_invoker.call(PSFController_14.ajaxCallForAppointmentPSFData(psfDay))
      }
  
    // @LINE:436
    case controllers_SearchControllerMR_assignedInteractionDataMR203_route(params) =>
      call(params.fromPath[String]("CREIds", None)) { (CREIds) =>
        controllers_SearchControllerMR_assignedInteractionDataMR203_invoker.call(SearchControllerMR_10.assignedInteractionDataMR(CREIds))
      }
  
    // @LINE:437
    case controllers_SearchControllerMR_followUpRequiredInteractionDataMR204_route(params) =>
      call(params.fromPath[String]("CREIds", None)) { (CREIds) =>
        controllers_SearchControllerMR_followUpRequiredInteractionDataMR204_invoker.call(SearchControllerMR_10.followUpRequiredInteractionDataMR(CREIds))
      }
  
    // @LINE:438
    case controllers_SearchControllerMR_serviceBookedInteractionDataMR205_route(params) =>
      call(params.fromPath[String]("CREIds", None)) { (CREIds) =>
        controllers_SearchControllerMR_serviceBookedInteractionDataMR205_invoker.call(SearchControllerMR_10.serviceBookedInteractionDataMR(CREIds))
      }
  
    // @LINE:439
    case controllers_SearchControllerMR_serviceNotRequiredInteractionDataMR206_route(params) =>
      call(params.fromPath[String]("CREIds", None)) { (CREIds) =>
        controllers_SearchControllerMR_serviceNotRequiredInteractionDataMR206_invoker.call(SearchControllerMR_10.serviceNotRequiredInteractionDataMR(CREIds))
      }
  
    // @LINE:440
    case controllers_SearchControllerMR_nonContactsInteractionDataMR207_route(params) =>
      call(params.fromPath[String]("CREIds", None)) { (CREIds) =>
        controllers_SearchControllerMR_nonContactsInteractionDataMR207_invoker.call(SearchControllerMR_10.nonContactsInteractionDataMR(CREIds))
      }
  
    // @LINE:441
    case controllers_SearchControllerMR_droppedCallInteractionDataMR208_route(params) =>
      call(params.fromPath[String]("CREIds", None)) { (CREIds) =>
        controllers_SearchControllerMR_droppedCallInteractionDataMR208_invoker.call(SearchControllerMR_10.droppedCallInteractionDataMR(CREIds))
      }
  
    // @LINE:442
    case controllers_SearchControllerMR_missedCallInteractionDataMR209_route(params) =>
      call(params.fromPath[String]("CREIds", None)) { (CREIds) =>
        controllers_SearchControllerMR_missedCallInteractionDataMR209_invoker.call(SearchControllerMR_10.missedCallInteractionDataMR(CREIds))
      }
  
    // @LINE:443
    case controllers_SearchControllerMR_incomingCallInteractionDataMR210_route(params) =>
      call(params.fromPath[String]("CREIds", None)) { (CREIds) =>
        controllers_SearchControllerMR_incomingCallInteractionDataMR210_invoker.call(SearchControllerMR_10.incomingCallInteractionDataMR(CREIds))
      }
  
    // @LINE:444
    case controllers_SearchControllerMR_outgoingCallsServerDataTableMR211_route(params) =>
      call(params.fromPath[String]("CREIds", None)) { (CREIds) =>
        controllers_SearchControllerMR_outgoingCallsServerDataTableMR211_invoker.call(SearchControllerMR_10.outgoingCallsServerDataTableMR(CREIds))
      }
  
    // @LINE:446
    case controllers_SearchControllerMR_getMediaFileMR212_route(params) =>
      call(params.fromPath[Long]("callId", None)) { (callId) =>
        controllers_SearchControllerMR_getMediaFileMR212_invoker.call(SearchControllerMR_10.getMediaFileMR(callId))
      }
  
    // @LINE:451
    case controllers_CallInteractionController_getExistingVehicleRegCount213_route(params) =>
      call(params.fromPath[String]("vehicleReg", None)) { (vehicleReg) =>
        controllers_CallInteractionController_getExistingVehicleRegCount213_invoker.call(CallInteractionController_12.getExistingVehicleRegCount(vehicleReg))
      }
  
    // @LINE:452
    case controllers_SearchControllerMR_getDashboardCount214_route(params) =>
      call { 
        controllers_SearchControllerMR_getDashboardCount214_invoker.call(SearchControllerMR_10.getDashboardCount())
      }
  
    // @LINE:453
    case controllers_SearchController_getDashboardCountCRE215_route(params) =>
      call { 
        controllers_SearchController_getDashboardCountCRE215_invoker.call(SearchController_0.getDashboardCountCRE())
      }
  
    // @LINE:457
    case controllers_CallInteractionController_sendCustomSMSAjax216_route(params) =>
      call(params.fromPath[Long]("rerenceNumber", None)) { (rerenceNumber) =>
        controllers_CallInteractionController_sendCustomSMSAjax216_invoker.call(CallInteractionController_12.sendCustomSMSAjax(rerenceNumber))
      }
  
    // @LINE:458
    case controllers_CallInteractionController_getDriverListBasedOnworkshop217_route(params) =>
      call(params.fromPath[Long]("workshopId", None)) { (workshopId) =>
        controllers_CallInteractionController_getDriverListBasedOnworkshop217_invoker.call(CallInteractionController_12.getDriverListBasedOnworkshop(workshopId))
      }
  
    // @LINE:459
    case controllers_CallInteractionController_getCRESListBasedOnWorkshop218_route(params) =>
      call(params.fromPath[Long]("workshopId", None)) { (workshopId) =>
        controllers_CallInteractionController_getCRESListBasedOnWorkshop218_invoker.call(CallInteractionController_12.getCRESListBasedOnWorkshop(workshopId))
      }
  
    // @LINE:460
    case controllers_CallInteractionController_getCREListBasedOnWorkshopCallHistory219_route(params) =>
      call(params.fromPath[Long]("workshopId", None)) { (workshopId) =>
        controllers_CallInteractionController_getCREListBasedOnWorkshopCallHistory219_invoker.call(CallInteractionController_12.getCREListBasedOnWorkshopCallHistory(workshopId))
      }
  
    // @LINE:464
    case controllers_ServiceAdvisorController_getServiceAdvisorComplaints220_route(params) =>
      call { 
        controllers_ServiceAdvisorController_getServiceAdvisorComplaints220_invoker.call(ServiceAdvisorController_20.getServiceAdvisorComplaints())
      }
  
    // @LINE:468
    case controllers_AutoSelectionSAController_ajaxAutoSASelection221_route(params) =>
      call(params.fromPath[Long]("workshopId", None), params.fromPath[String]("date", None)) { (workshopId, date) =>
        controllers_AutoSelectionSAController_ajaxAutoSASelection221_invoker.call(AutoSelectionSAController_29.ajaxAutoSASelection(workshopId, date))
      }
  
    // @LINE:469
    case controllers_AutoSelectionSAController_ajaxAutoSASelectionList222_route(params) =>
      call(params.fromPath[String]("saDetails", None)) { (saDetails) =>
        controllers_AutoSelectionSAController_ajaxAutoSASelectionList222_invoker.call(AutoSelectionSAController_29.ajaxAutoSASelectionList(saDetails))
      }
  
    // @LINE:470
    case controllers_AutoSelectionSAController_ajaxupdateSaChange223_route(params) =>
      call(params.fromPath[Long]("workshopId", None), params.fromPath[String]("date", None), params.fromPath[String]("preSaDetails", None), params.fromPath[String]("newSaDetails", None)) { (workshopId, date, preSaDetails, newSaDetails) =>
        controllers_AutoSelectionSAController_ajaxupdateSaChange223_invoker.call(AutoSelectionSAController_29.ajaxupdateSaChange(workshopId, date, preSaDetails, newSaDetails))
      }
  
    // @LINE:474
    case controllers_AutoSelInsuranceAgentController_ajaxAutoInsurAgentSelection224_route(params) =>
      call(params.fromPath[String]("date", None)) { (date) =>
        controllers_AutoSelInsuranceAgentController_ajaxAutoInsurAgentSelection224_invoker.call(AutoSelInsuranceAgentController_5.ajaxAutoInsurAgentSelection(date))
      }
  
    // @LINE:475
    case controllers_AutoSelInsuranceAgentController_ajaxAutoInsurAgentSelectionList225_route(params) =>
      call(params.fromPath[String]("saDetails", None)) { (saDetails) =>
        controllers_AutoSelInsuranceAgentController_ajaxAutoInsurAgentSelectionList225_invoker.call(AutoSelInsuranceAgentController_5.ajaxAutoInsurAgentSelectionList(saDetails))
      }
  
    // @LINE:476
    case controllers_AutoSelInsuranceAgentController_ajaxupdateInsuAgentChange226_route(params) =>
      call(params.fromPath[String]("date", None), params.fromPath[String]("preSaDetails", None), params.fromPath[String]("newSaDetails", None)) { (date, preSaDetails, newSaDetails) =>
        controllers_AutoSelInsuranceAgentController_ajaxupdateInsuAgentChange226_invoker.call(AutoSelInsuranceAgentController_5.ajaxupdateInsuAgentChange(date, preSaDetails, newSaDetails))
      }
  
    // @LINE:482
    case controllers_CallInfoController_getUpsellLeadsSeletedInLastSB227_route(params) =>
      call(params.fromPath[Long]("sr_int_id", None)) { (sr_int_id) =>
        controllers_CallInfoController_getUpsellLeadsSeletedInLastSB227_invoker.call(CallInfoController_26.getUpsellLeadsSeletedInLastSB(sr_int_id))
      }
  
    // @LINE:486
    case controllers_InsuranceController_getCommonCallDispositionForm228_route(params) =>
      call(params.fromPath[Long]("cid", None), params.fromPath[Long]("vehicle_id", None), params.fromPath[String]("typeDispo", None)) { (cid, vehicle_id, typeDispo) =>
        controllers_InsuranceController_getCommonCallDispositionForm228_invoker.call(InsuranceController_25.getCommonCallDispositionForm(cid, vehicle_id, typeDispo))
      }
  
    // @LINE:487
    case controllers_InsuranceController_postCommonCallDispositionForm229_route(params) =>
      call { 
        controllers_InsuranceController_postCommonCallDispositionForm229_invoker.call(InsuranceController_25.postCommonCallDispositionForm())
      }
  
    // @LINE:488
    case controllers_InsuranceController_getAllInsuranceAssignedList230_route(params) =>
      call { 
        controllers_InsuranceController_getAllInsuranceAssignedList230_invoker.call(InsuranceController_25.getAllInsuranceAssignedList())
      }
  
    // @LINE:489
    case controllers_InsuranceController_getAssignedInsuInteraction231_route(params) =>
      call { 
        controllers_InsuranceController_getAssignedInsuInteraction231_invoker.call(InsuranceController_25.getAssignedInsuInteraction())
      }
  
    // @LINE:490
    case controllers_InsuranceController_getContactedDispoFormData232_route(params) =>
      call(params.fromPath[Long]("typeOfdispo", None)) { (typeOfdispo) =>
        controllers_InsuranceController_getContactedDispoFormData232_invoker.call(InsuranceController_25.getContactedDispoFormData(typeOfdispo))
      }
  
    // @LINE:491
    case controllers_InsuranceController_getNonContactDroppedInsuranceDispoData233_route(params) =>
      call(params.fromPath[Long]("typeOfdispo", None)) { (typeOfdispo) =>
        controllers_InsuranceController_getNonContactDroppedInsuranceDispoData233_invoker.call(InsuranceController_25.getNonContactDroppedInsuranceDispoData(typeOfdispo))
      }
  
    // @LINE:495
    case controllers_PSFController_getCommonPSFDispositionPage234_route(params) =>
      call(params.fromPath[Long]("cid", None), params.fromPath[Long]("vehicle_id", None), params.fromPath[Long]("interactionid", None), params.fromPath[Long]("dispositionHistory", None), params.fromPath[Long]("typeOfPSF", None)) { (cid, vehicle_id, interactionid, dispositionHistory, typeOfPSF) =>
        controllers_PSFController_getCommonPSFDispositionPage234_invoker.call(PSFController_14.getCommonPSFDispositionPage(cid, vehicle_id, interactionid, dispositionHistory, typeOfPSF))
      }
  
    // @LINE:496
    case controllers_PSFController_postCommonPSFDispositionPage235_route(params) =>
      call { 
        controllers_PSFController_postCommonPSFDispositionPage235_invoker.call(PSFController_14.postCommonPSFDispositionPage())
      }
  
    // @LINE:498
    case controllers_InsuranceController_upload_Excel_Sheet_insurance236_route(params) =>
      call(params.fromPath[String]("uploadId", None), params.fromPath[String]("uploadType", None), params.fromPath[String]("resultData", None)) { (uploadId, uploadType, resultData) =>
        controllers_InsuranceController_upload_Excel_Sheet_insurance236_invoker.call(InsuranceController_25.upload_Excel_Sheet_insurance(uploadId, uploadType, resultData))
      }
  
    // @LINE:500
    case controllers_InsuranceController_getPageForAssigningCallsInsurance237_route(params) =>
      call { 
        controllers_InsuranceController_getPageForAssigningCallsInsurance237_invoker.call(InsuranceController_25.getPageForAssigningCallsInsurance())
      }
  
    // @LINE:501
    case controllers_InsuranceController_postAssignCallInsurance238_route(params) =>
      call { 
        controllers_InsuranceController_postAssignCallInsurance238_invoker.call(InsuranceController_25.postAssignCallInsurance())
      }
  
    // @LINE:505
    case controllers_InsuranceController_getODPercentage239_route(params) =>
      call(params.fromPath[String]("cubicCap", None), params.fromPath[String]("vehAge", None), params.fromPath[String]("zoneid", None)) { (cubicCap, vehAge, zoneid) =>
        controllers_InsuranceController_getODPercentage239_invoker.call(InsuranceController_25.getODPercentage(cubicCap, vehAge, zoneid))
      }
  
    // @LINE:506
    case controllers_InsuranceController_getBasicODVaue240_route(params) =>
      call(params.fromPath[Double]("odvValue", None), params.fromPath[Double]("idvValue", None)) { (odvValue, idvValue) =>
        controllers_InsuranceController_getBasicODVaue240_invoker.call(InsuranceController_25.getBasicODVaue(odvValue, idvValue))
      }
  
    // @LINE:507
    case controllers_InsuranceController_getNCBValueByBasicValue241_route(params) =>
      call(params.fromPath[Double]("ncbPercenVal", None), params.fromPath[Double]("basicODValue", None)) { (ncbPercenVal, basicODValue) =>
        controllers_InsuranceController_getNCBValueByBasicValue241_invoker.call(InsuranceController_25.getNCBValueByBasicValue(ncbPercenVal, basicODValue))
      }
  
    // @LINE:508
    case controllers_InsuranceController_gettotalPremiumAndDiscValue242_route(params) =>
      call(params.fromPath[Double]("odPremiumVaue", None), params.fromPath[Double]("commercialDiscPerceValue", None), params.fromPath[Double]("thirdPartPremiumValue", None)) { (odPremiumVaue, commercialDiscPerceValue, thirdPartPremiumValue) =>
        controllers_InsuranceController_gettotalPremiumAndDiscValue242_invoker.call(InsuranceController_25.gettotalPremiumAndDiscValue(odPremiumVaue, commercialDiscPerceValue, thirdPartPremiumValue))
      }
  
    // @LINE:509
    case controllers_InsuranceController_gettotalPremiumForAddOn243_route(params) =>
      call(params.fromPath[Double]("addOnPremiumValuePer", None), params.fromPath[Double]("odPremiumVaue", None), params.fromPath[Double]("commercialDiscPerceValue", None), params.fromPath[Double]("idvVal", None), params.fromPath[Double]("thirdPartPremiumValue", None)) { (addOnPremiumValuePer, odPremiumVaue, commercialDiscPerceValue, idvVal, thirdPartPremiumValue) =>
        controllers_InsuranceController_gettotalPremiumForAddOn243_invoker.call(InsuranceController_25.gettotalPremiumForAddOn(addOnPremiumValuePer, odPremiumVaue, commercialDiscPerceValue, idvVal, thirdPartPremiumValue))
      }
  
    // @LINE:513
    case controllers_InsuranceController_showRoomList244_route(params) =>
      call { 
        controllers_InsuranceController_showRoomList244_invoker.call(InsuranceController_25.showRoomList())
      }
  
    // @LINE:515
    case controllers_InsuranceController_downloadInsuranceErrorData245_route(params) =>
      call(params.fromPath[String]("uploadId", None), params.fromPath[String]("uploadType", None)) { (uploadId, uploadType) =>
        controllers_InsuranceController_downloadInsuranceErrorData245_invoker.call(InsuranceController_25.downloadInsuranceErrorData(uploadId, uploadType))
      }
  
    // @LINE:518
    case controllers_CampaignController_addCampaignInsurance246_route(params) =>
      call { 
        controllers_CampaignController_addCampaignInsurance246_invoker.call(CampaignController_8.addCampaignInsurance())
      }
  
    // @LINE:520
    case controllers_InsuranceController_insuranceFollowUpNotificationToday247_route(params) =>
      call { 
        controllers_InsuranceController_insuranceFollowUpNotificationToday247_invoker.call(InsuranceController_25.insuranceFollowUpNotificationToday())
      }
  
    // @LINE:524
    case controllers_InsuranceHistoryController_uploadHistoryViewPage248_route(params) =>
      call { 
        controllers_InsuranceHistoryController_uploadHistoryViewPage248_invoker.call(InsuranceHistoryController_21.uploadHistoryViewPage())
      }
  
    // @LINE:525
    case controllers_InsuranceHistoryController_uploadExcelDataHistory249_route(params) =>
      call { 
        controllers_InsuranceHistoryController_uploadExcelDataHistory249_invoker.call(InsuranceHistoryController_21.uploadExcelDataHistory())
      }
  
    // @LINE:527
    case controllers_InsuranceController_insuranceHistoryOfCustomerId250_route(params) =>
      call(params.fromPath[Long]("customerId", None)) { (customerId) =>
        controllers_InsuranceController_insuranceHistoryOfCustomerId250_invoker.call(InsuranceController_25.insuranceHistoryOfCustomerId(customerId))
      }
  
    // @LINE:528
    case controllers_DealerController_getOEMOfDealer251_route(params) =>
      call { 
        controllers_DealerController_getOEMOfDealer251_invoker.call(DealerController_28.getOEMOfDealer())
      }
  
    // @LINE:532
    case controllers_FileUploadController_uploadPage252_route(params) =>
      call { 
        controllers_FileUploadController_uploadPage252_invoker.call(FileUploadController_7.uploadPage())
      }
  
    // @LINE:533
    case controllers_FileUploadController_startUpload253_route(params) =>
      call { 
        controllers_FileUploadController_startUpload253_invoker.call(FileUploadController_7.startUpload())
      }
  
    // @LINE:534
    case controllers_FileUploadController_headRequest254_route(params) =>
      call { 
        controllers_FileUploadController_headRequest254_invoker.call(FileUploadController_7.headRequest())
      }
  
    // @LINE:535
    case controllers_ProcessUploadedFiles_process255_route(params) =>
      call { 
        controllers_ProcessUploadedFiles_process255_invoker.call(ProcessUploadedFiles_31.process())
      }
  
    // @LINE:536
    case controllers_FileUploadController_deleteFile256_route(params) =>
      call(params.fromPath[String]("id", None)) { (id) =>
        controllers_FileUploadController_deleteFile256_invoker.call(FileUploadController_7.deleteFile(id))
      }
  
    // @LINE:537
    case controllers_FileUploadController_downloadFile257_route(params) =>
      call(params.fromPath[String]("id", None)) { (id) =>
        controllers_FileUploadController_downloadFile257_invoker.call(FileUploadController_7.downloadFile(id))
      }
  
    // @LINE:538
    case controllers_FileUploadController_downloadErrors258_route(params) =>
      call(params.fromPath[String]("id", None)) { (id) =>
        controllers_FileUploadController_downloadErrors258_invoker.call(FileUploadController_7.downloadErrors(id))
      }
  
    // @LINE:540
    case controllers_ProcessUploadedFiles_processFile259_route(params) =>
      call(params.fromPath[String]("id", None)) { (id) =>
        controllers_ProcessUploadedFiles_processFile259_invoker.call(ProcessUploadedFiles_31.processFile(id))
      }
  
    // @LINE:542
    case controllers_FileUploadController_getUploadsList260_route(params) =>
      call(params.fromPath[String]("utype", None)) { (utype) =>
        controllers_FileUploadController_getUploadsList260_invoker.call(FileUploadController_7.getUploadsList(utype))
      }
  
    // @LINE:543
    case controllers_FileUploadController_getUploadsListById261_route(params) =>
      call(params.fromPath[Long]("upId", None)) { (upId) =>
        controllers_FileUploadController_getUploadsListById261_invoker.call(FileUploadController_7.getUploadsListById(upId))
      }
  
    // @LINE:545
    case controllers_FileUploadController_getExistingFiles262_route(params) =>
      call(params.fromPath[String]("utype", None)) { (utype) =>
        controllers_FileUploadController_getExistingFiles262_invoker.call(FileUploadController_7.getExistingFiles(utype))
      }
  
    // @LINE:547
    case controllers_FileUploadController_uploadReportPage263_route(params) =>
      call { 
        controllers_FileUploadController_uploadReportPage263_invoker.call(FileUploadController_7.uploadReportPage())
      }
  
    // @LINE:549
    case controllers_SMSandEmailController_startTriggerSMS264_route(params) =>
      call { 
        controllers_SMSandEmailController_startTriggerSMS264_invoker.call(SMSandEmailController_18.startTriggerSMS())
      }
  
    // @LINE:551
    case controllers_CallInteractionController_getWorkshopsByLocations265_route(params) =>
      call(params.fromPath[String]("locations", None)) { (locations) =>
        controllers_CallInteractionController_getWorkshopsByLocations265_invoker.call(CallInteractionController_12.getWorkshopsByLocations(locations))
      }
  
    // @LINE:553
    case controllers_CallInteractionController_getCRESByWorkshops266_route(params) =>
      call(params.fromPath[String]("workshops", None)) { (workshops) =>
        controllers_CallInteractionController_getCRESByWorkshops266_invoker.call(CallInteractionController_12.getCRESByWorkshops(workshops))
      }
  
    // @LINE:556
    case controllers_CallInteractionController_getCitiesByState267_route(params) =>
      call(params.fromPath[String]("selState", None)) { (selState) =>
        controllers_CallInteractionController_getCitiesByState267_invoker.call(CallInteractionController_12.getCitiesByState(selState))
      }
  
    // @LINE:561
    case controllers_CallInteractionController_viewAllComplaintsReadOnlyAccess268_route(params) =>
      call { 
        controllers_CallInteractionController_viewAllComplaintsReadOnlyAccess268_invoker.call(CallInteractionController_12.viewAllComplaintsReadOnlyAccess())
      }
  
    // @LINE:563
    case controllers_CallInteractionController_getListWorkshopByLocationById269_route(params) =>
      call(params.fromPath[Long]("selectedCity", None)) { (selectedCity) =>
        controllers_CallInteractionController_getListWorkshopByLocationById269_invoker.call(CallInteractionController_12.getListWorkshopByLocationById(selectedCity))
      }
  
    // @LINE:564
    case controllers_CallInteractionController_getFunctionsListByLoc270_route(params) =>
      call(params.fromPath[String]("selectedCity", None)) { (selectedCity) =>
        controllers_CallInteractionController_getFunctionsListByLoc270_invoker.call(CallInteractionController_12.getFunctionsListByLoc(selectedCity))
      }
  
    // @LINE:565
    case controllers_CallInteractionController_getusersByFuncandLocation271_route(params) =>
      call(params.fromPath[String]("city", None), params.fromPath[String]("function", None)) { (city, function) =>
        controllers_CallInteractionController_getusersByFuncandLocation271_invoker.call(CallInteractionController_12.getusersByFuncandLocation(city, function))
      }
  
    // @LINE:567
    case controllers_ChangeAssignmentController_changeassignedCalls272_route(params) =>
      call { 
        controllers_ChangeAssignmentController_changeassignedCalls272_invoker.call(ChangeAssignmentController_22.changeassignedCalls())
      }
  
    // @LINE:568
    case controllers_ChangeAssignmentController_postchangeassignedCalls273_route(params) =>
      call { 
        controllers_ChangeAssignmentController_postchangeassignedCalls273_invoker.call(ChangeAssignmentController_22.postchangeassignedCalls())
      }
  
    // @LINE:570
    case controllers_ChangeAssignmentController_changeAssignedCallsToAgents274_route(params) =>
      call { 
        controllers_ChangeAssignmentController_changeAssignedCallsToAgents274_invoker.call(ChangeAssignmentController_22.changeAssignedCallsToAgents())
      }
  
    // @LINE:573
    case controllers_CampaignController_getCampaignNamesByUpload275_route(params) =>
      call(params.fromPath[String]("uploadType", None)) { (uploadType) =>
        controllers_CampaignController_getCampaignNamesByUpload275_invoker.call(CampaignController_8.getCampaignNamesByUpload(uploadType))
      }
  
    // @LINE:578
    case controllers_ChangeAssignmentController_assignmentFilterList276_route(params) =>
      call { 
        controllers_ChangeAssignmentController_assignmentFilterList276_invoker.call(ChangeAssignmentController_22.assignmentFilterList())
      }
  
    // @LINE:579
    case controllers_ChangeAssignmentController_getAssignmentFilterListAjax277_route(params) =>
      call { 
        controllers_ChangeAssignmentController_getAssignmentFilterListAjax277_invoker.call(ChangeAssignmentController_22.getAssignmentFilterListAjax())
      }
  
    // @LINE:580
    case controllers_ChangeAssignmentController_assignListToUserSelected278_route(params) =>
      call { 
        controllers_ChangeAssignmentController_assignListToUserSelected278_invoker.call(ChangeAssignmentController_22.assignListToUserSelected())
      }
  
    // @LINE:584
    case controllers_CallInfoController_getLeadNamesbyLeadId279_route(params) =>
      call(params.fromPath[Long]("leadId", None), params.fromPath[Long]("userId", None)) { (leadId, userId) =>
        controllers_CallInfoController_getLeadNamesbyLeadId279_invoker.call(CallInfoController_26.getLeadNamesbyLeadId(leadId, userId))
      }
  
    // @LINE:586
    case controllers_CallInfoController_getCheckVehicleRegExist280_route(params) =>
      call(params.fromPath[String]("vehRegId", None)) { (vehRegId) =>
        controllers_CallInfoController_getCheckVehicleRegExist280_invoker.call(CallInfoController_26.getCheckVehicleRegExist(vehRegId))
      }
  
    // @LINE:589
    case controllers_SuperAdminController_LocationBySuperAdmin281_route(params) =>
      call { 
        controllers_SuperAdminController_LocationBySuperAdmin281_invoker.call(SuperAdminController_17.LocationBySuperAdmin())
      }
  
    // @LINE:590
    case controllers_SuperAdminController_postLocationBySuperAdmin282_route(params) =>
      call { 
        controllers_SuperAdminController_postLocationBySuperAdmin282_invoker.call(SuperAdminController_17.postLocationBySuperAdmin())
      }
  
    // @LINE:593
    case controllers_SuperAdminController_UsersBySuperAdmin283_route(params) =>
      call { 
        controllers_SuperAdminController_UsersBySuperAdmin283_invoker.call(SuperAdminController_17.UsersBySuperAdmin())
      }
  
    // @LINE:594
    case controllers_SuperAdminController_postUserBySuperAdmin284_route(params) =>
      call { 
        controllers_SuperAdminController_postUserBySuperAdmin284_invoker.call(SuperAdminController_17.postUserBySuperAdmin())
      }
  
    // @LINE:595
    case controllers_SuperAdminController_checkIfUserExists285_route(params) =>
      call(params.fromPath[String]("uname", None)) { (uname) =>
        controllers_SuperAdminController_checkIfUserExists285_invoker.call(SuperAdminController_17.checkIfUserExists(uname))
      }
  
    // @LINE:599
    case controllers_SMSTemplateController_SMSTemplateBySuperAdmin286_route(params) =>
      call { 
        controllers_SMSTemplateController_SMSTemplateBySuperAdmin286_invoker.call(SMSTemplateController_2.SMSTemplateBySuperAdmin())
      }
  
    // @LINE:600
    case controllers_SMSTemplateController_postSMSTemplate287_route(params) =>
      call(params.fromPath[String]("messageTemplate", None), params.fromPath[String]("msgAPI", None)) { (messageTemplate, msgAPI) =>
        controllers_SMSTemplateController_postSMSTemplate287_invoker.call(SMSTemplateController_2.postSMSTemplate(messageTemplate, msgAPI))
      }
  
    // @LINE:601
    case controllers_SMSTemplateController_getCustomersListBySavedName288_route(params) =>
      call(params.fromPath[String]("savedsearchname", None)) { (savedsearchname) =>
        controllers_SMSTemplateController_getCustomersListBySavedName288_invoker.call(SMSTemplateController_2.getCustomersListBySavedName(savedsearchname))
      }
  
    // @LINE:602
    case controllers_SMSTemplateController_postSMSBulk289_route(params) =>
      call { 
        controllers_SMSTemplateController_postSMSBulk289_invoker.call(SMSTemplateController_2.postSMSBulk())
      }
  
    // @LINE:603
    case controllers_SMSTemplateController_getSelectedUserListforDatatable290_route(params) =>
      call(params.fromPath[String]("selectedUsers", None), params.fromPath[Boolean]("setFlag", None), params.fromPath[String]("savedsearchname", None)) { (selectedUsers, setFlag, savedsearchname) =>
        controllers_SMSTemplateController_getSelectedUserListforDatatable290_invoker.call(SMSTemplateController_2.getSelectedUserListforDatatable(selectedUsers, setFlag, savedsearchname))
      }
  
    // @LINE:607
    case controllers_ServiceBookController_getReviewBookingPage291_route(params) =>
      call { 
        controllers_ServiceBookController_getReviewBookingPage291_invoker.call(ServiceBookController_32.getReviewBookingPage())
      }
  
    // @LINE:609
    case controllers_ServiceBookController_getServiceAdvisorListByWorkshop292_route(params) =>
      call(params.fromPath[Long]("workshopid", None)) { (workshopid) =>
        controllers_ServiceBookController_getServiceAdvisorListByWorkshop292_invoker.call(ServiceBookController_32.getServiceAdvisorListByWorkshop(workshopid))
      }
  
    // @LINE:610
    case controllers_ServiceBookController_getsearchServiceBoookedList293_route(params) =>
      call { 
        controllers_ServiceBookController_getsearchServiceBoookedList293_invoker.call(ServiceBookController_32.getsearchServiceBoookedList())
      }
  
    // @LINE:613
    case controllers_ServiceBookController_cancelBookingOrPickup294_route(params) =>
      call(params.fromPath[Long]("servicebookedId", None), params.fromPath[Long]("cancelId", None)) { (servicebookedId, cancelId) =>
        controllers_ServiceBookController_cancelBookingOrPickup294_invoker.call(ServiceBookController_32.cancelBookingOrPickup(servicebookedId, cancelId))
      }
  
    // @LINE:615
    case controllers_ServiceBookController_reviewScheduleBooking295_route(params) =>
      call(params.fromPath[Long]("servicebookedId", None)) { (servicebookedId) =>
        controllers_ServiceBookController_reviewScheduleBooking295_invoker.call(ServiceBookController_32.reviewScheduleBooking(servicebookedId))
      }
  
    // @LINE:616
    case controllers_ServiceBookController_postRescheduleBooking296_route(params) =>
      call { 
        controllers_ServiceBookController_postRescheduleBooking296_invoker.call(ServiceBookController_32.postRescheduleBooking())
      }
  
    // @LINE:618
    case controllers_ServiceBookController_driverListScheduleByWorkshopId297_route(params) =>
      call(params.fromPath[Long]("workshopId", None), params.fromPath[String]("scheduleDate", None)) { (workshopId, scheduleDate) =>
        controllers_ServiceBookController_driverListScheduleByWorkshopId297_invoker.call(ServiceBookController_32.driverListScheduleByWorkshopId(workshopId, scheduleDate))
      }
  
    // @LINE:621
    case controllers_AudioStreamController_header298_route(params) =>
      call(params.fromPath[String]("interactionId", None)) { (interactionId) =>
        controllers_AudioStreamController_header298_invoker.call(AudioStreamController_3.header(interactionId))
      }
  
    // @LINE:622
    case controllers_AudioStreamController_streamAudio299_route(params) =>
      call(params.fromPath[String]("interactionId", None)) { (interactionId) =>
        controllers_AudioStreamController_streamAudio299_invoker.call(AudioStreamController_3.streamAudio(interactionId))
      }
  
    // @LINE:625
    case controllers_AllAudioConverter_convertAllFiles300_route(params) =>
      call(params.fromPath[Long]("startId", None), params.fromPath[Long]("endId", None)) { (startId, endId) =>
        controllers_AllAudioConverter_convertAllFiles300_invoker.call(AllAudioConverter_9.convertAllFiles(startId, endId))
      }
  
    // @LINE:630
    case controllers_CallRecordingHistoryController_callRecordingView301_route(params) =>
      call { 
        controllers_CallRecordingHistoryController_callRecordingView301_invoker.call(CallRecordingHistoryController_13.callRecordingView())
      }
  
    // @LINE:631
    case controllers_CallRecordingHistoryController_callRecordingData302_route(params) =>
      call { 
        controllers_CallRecordingHistoryController_callRecordingData302_invoker.call(CallRecordingHistoryController_13.callRecordingData())
      }
  
    // @LINE:633
    case controllers_CallRecordingHistoryController_callRecordingViewInsurance303_route(params) =>
      call { 
        controllers_CallRecordingHistoryController_callRecordingViewInsurance303_invoker.call(CallRecordingHistoryController_13.callRecordingViewInsurance())
      }
  
    // @LINE:634
    case controllers_CallRecordingHistoryController_callRecordingDataInsurance304_route(params) =>
      call { 
        controllers_CallRecordingHistoryController_callRecordingDataInsurance304_invoker.call(CallRecordingHistoryController_13.callRecordingDataInsurance())
      }
  
    // @LINE:636
    case controllers_CallRecordingHistoryController_callRecordingViewPSF305_route(params) =>
      call { 
        controllers_CallRecordingHistoryController_callRecordingViewPSF305_invoker.call(CallRecordingHistoryController_13.callRecordingViewPSF())
      }
  
    // @LINE:637
    case controllers_CallRecordingHistoryController_callRecordingDataPSF306_route(params) =>
      call { 
        controllers_CallRecordingHistoryController_callRecordingDataPSF306_invoker.call(CallRecordingHistoryController_13.callRecordingDataPSF())
      }
  
    // @LINE:639
    case controllers_CallRecordingHistoryController_otherCallRecordingView307_route(params) =>
      call { 
        controllers_CallRecordingHistoryController_otherCallRecordingView307_invoker.call(CallRecordingHistoryController_13.otherCallRecordingView())
      }
  
    // @LINE:640
    case controllers_CallRecordingHistoryController_otherCallRecordingData308_route(params) =>
      call { 
        controllers_CallRecordingHistoryController_otherCallRecordingData308_invoker.call(CallRecordingHistoryController_13.otherCallRecordingData())
      }
  
    // @LINE:644
    case controllers_FirebaseSyncController_startSyncOperationInsuranceAgent309_route(params) =>
      call { 
        controllers_FirebaseSyncController_startSyncOperationInsuranceAgent309_invoker.call(FirebaseSyncController_6.startSyncOperationInsuranceAgent())
      }
  
    // @LINE:645
    case controllers_FirebaseSyncController_startSyncOperationInsuranceAgentHistory310_route(params) =>
      call { 
        controllers_FirebaseSyncController_startSyncOperationInsuranceAgentHistory310_invoker.call(FirebaseSyncController_6.startSyncOperationInsuranceAgentHistory())
      }
  
    // @LINE:647
    case controllers_ServiceBookController_downloadServiceBooking311_route(params) =>
      call { 
        controllers_ServiceBookController_downloadServiceBooking311_invoker.call(ServiceBookController_32.downloadServiceBooking())
      }
  }
}
