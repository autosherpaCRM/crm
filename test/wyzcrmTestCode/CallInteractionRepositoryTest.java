package wyzcrmTestCode;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import configs.JinqSource;
import models.AssignedInteraction;
import models.Role;
import models.Tenant;
import models.WyzUser;
import play.Logger.ALogger;
import repositories.CallInteractionsRepository;
import repositories.WyzUserRepository;

public class CallInteractionRepositoryTest extends AbstractIntegrationTest{

	public static String USER_NAME = "MyAppTestUser";
	public static String PASSWORD = "MyAppPass";
	public static String USER_PHONE ="9999999999";
	public static String TENANT_CODE = "TEST1";
	public static String USER_ROLE = "GENERICUSER";
	
	ALogger logger = play.Logger.of("application");
	

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private JinqSource source;

	private List<Object> tobeDeleted;

	@Autowired
	private CallInteractionsRepository callIntRepo;
	
	@Autowired
	private WyzUserRepository userRepository;
	
	@Before
	public void setupData() {

		Tenant tenant = new Tenant();
		tenant.setTenantCode(TENANT_CODE);
		tenant.setTenantPhone("12344");
		tenant.setTenentAddress("test address");
		em.persist(tenant);

		em.flush();
		Long tenantid = tenant.getId();

		tobeDeleted = new ArrayList();
		tobeDeleted.add(tenant);

		Role role = new Role();
		role.setRole(USER_ROLE);

		em.persist(role);
		em.flush();
		Long roleid = role.getId();
		tobeDeleted.add(role);

		WyzUser user = new WyzUser();
		user.setFirstName("TestF");
		user.setLastName("TestL");
		user.setUserName(USER_NAME);
		user.setPassword(PASSWORD);
		user.setTenant(tenant);
		List roleList = new ArrayList();
		roleList.add(role);
		user.setRoles(roleList);
		em.persist(user);
		em.flush();
		Long userid = user.getId();

		
		tobeDeleted.add(user);

	}
	
	@Test
	public void testAssinCalls(){
		
		WyzUser user = userRepository.getUser(USER_NAME);
		
		AssignedInteraction assign=new AssignedInteraction();
		
		assign.setWyzUser(user);
		
		em.persist(assign);
		em.flush();
		
		
		tobeDeleted.add(assign);
		
		long countass=source.assignedInteractions(em).count();
		
		
		
		WyzUser newuser=new WyzUser();
		em.persist(newuser);
		
		logger.info(" countass int: "+countass+" assign : "+assign.getId()+" newuser :"+newuser.getId());
		
		callIntRepo.changeAssignmentCallBy(newuser.getId(), assign.getId());
		
		
	}
	
	
	
	@After
	public void tearDownData() {

		tobeDeleted.forEach((object) -> {

			em.remove(object);
			em.flush();

		});

	}
}
