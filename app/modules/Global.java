package modules;

import java.util.concurrent.CompletableFuture;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.pac4j.core.config.Config;
import org.pac4j.play.store.PlaySessionStore;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import controllers.multitenant.MyTenantIdentifier;
import controllers.pac4j.MyAppAuthenticator;
import play.Configuration;
import play.Environment;
import play.data.FormFactory;
import play.inject.ApplicationLifecycle;
import repositories.WyzUserRepository;

@Singleton
public class Global {

	private AnnotationConfigApplicationContext ctx;

	@Inject
	public Global(AnnotationConfigApplicationContext ctx, Configuration configuration, ApplicationLifecycle lifecycle,
			Environment environment, FormFactory formFactory, play.DefaultApplication playApp,
			WyzUserRepository wyzUserRepo, MyAppAuthenticator authenticator, MyTenantIdentifier tenantIdentifier,
			PlaySessionStore sessionStore, Config config) {

		authenticator.setWyzRepository(wyzUserRepo);

		tenantIdentifier.setConfig(config);
		tenantIdentifier.setPlaySessionStore(sessionStore);

		ctx.registerShutdownHook();

		lifecycle.addStopHook(() -> {
			ctx.close();
			return CompletableFuture.completedFuture(null);
		});

	}

	public <A> A getBean(Class<A> clazz) {
		return ctx.getBean(clazz);
	}
}
