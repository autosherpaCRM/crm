package modules;

import org.pac4j.core.client.Clients;
import org.pac4j.core.client.direct.AnonymousClient;
import org.pac4j.core.config.Config;
import org.pac4j.http.client.indirect.FormClient;
import org.pac4j.http.client.indirect.IndirectBasicAuthClient;
import org.pac4j.play.CallbackController;
import org.pac4j.play.LogoutController;
import org.pac4j.play.deadbolt2.Pac4jHandlerCache;
import org.pac4j.play.deadbolt2.Pac4jRoleHandler;
import org.pac4j.play.store.PlayCacheSessionStore;
import org.pac4j.play.store.PlaySessionStore;

import com.google.inject.AbstractModule;
import com.typesafe.config.ConfigFactory;

import be.objectify.deadbolt.java.cache.HandlerCache;
import controllers.errorhandlers.DemoHttpActionAdapter;
import controllers.pac4j.CreAuthorizer;
import controllers.pac4j.CreManagerAuthorizer;
import controllers.pac4j.InsuranceAuthorizer;
import controllers.pac4j.MyAppAuthenticator;
import controllers.pac4j.SuperAdminAuthorizer;
import play.cache.CacheApi;

public class ApplicationContextModule extends AbstractModule {

	private static class MyPac4jRoleHandler implements Pac4jRoleHandler {
	}
	

	@Override
	protected void configure() {
		bind(Global.class).asEagerSingleton();

		com.typesafe.config.Config configuration = ConfigFactory.load();
		final String baseUrl = configuration.getString("baseUrl");
		
		//final String baseUrl = "http://localhost:9000";

		bind(HandlerCache.class).to(Pac4jHandlerCache.class);
		bind(Pac4jRoleHandler.class).to(MyPac4jRoleHandler.class);

		final PlayCacheSessionStore playCacheSessionStore = new PlayCacheSessionStore(getProvider(CacheApi.class));
		bind(PlaySessionStore.class).toInstance(playCacheSessionStore);

		// HTTP

		MyAppAuthenticator authenticator = new MyAppAuthenticator();
		bind(MyAppAuthenticator.class).toInstance(authenticator);

		final FormClient formClient = new FormClient(baseUrl + "/login", authenticator);
		final IndirectBasicAuthClient indirectBasicAuthClient = new IndirectBasicAuthClient(authenticator);

		final Clients clients = new Clients(baseUrl + "/callback", formClient, indirectBasicAuthClient,
				new AnonymousClient());

		final Config config = new Config(clients);
		
		SuperAdminAuthorizer superAdminAuthorizer = new SuperAdminAuthorizer();
		CreAuthorizer creAuthorizer = new CreAuthorizer();
		CreManagerAuthorizer creManagerAuthorizer = new CreManagerAuthorizer();
		InsuranceAuthorizer insuranceAuthorizer = new InsuranceAuthorizer();
		
		bind(CreAuthorizer.class).toInstance(creAuthorizer);
		bind(SuperAdminAuthorizer.class).toInstance(superAdminAuthorizer);
		bind(CreManagerAuthorizer.class).toInstance(creManagerAuthorizer);
		bind(InsuranceAuthorizer.class).toInstance(insuranceAuthorizer);
		
		config.addAuthorizer("superAdmin", superAdminAuthorizer);
		config.addAuthorizer("cre", creAuthorizer);
		config.addAuthorizer("creManager", creManagerAuthorizer);
		config.addAuthorizer("insurance", insuranceAuthorizer);
		
		//config.addAuthorizer("custom", new CustomAuthorizer());
		config.setHttpActionAdapter(new DemoHttpActionAdapter());
		bind(Config.class).toInstance(config);

		// callback
		final CallbackController callbackController = new CallbackController();
		callbackController.setDefaultUrl("/");
		callbackController.setMultiProfile(true);
		bind(CallbackController.class).toInstance(callbackController);

		// logout
		final LogoutController logoutController = new LogoutController();
		logoutController.setDefaultUrl("/");
		// logoutController.setDestroySession(true);
		bind(LogoutController.class).toInstance(logoutController);
	}

}