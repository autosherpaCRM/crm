package models;
import javax.persistence.*;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;

@Entity
public class SavedSearchResult {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;
	
	@Column(length = 60)
	public String savedName;
	
	@Column(length = 30)
	public String upload_id;
	
	@OneToMany
	@JoinTable(name = "saved_search_results")
	public List<CustomerSearchResult> searchResults;

	
	public String getUpload_id() {
		return upload_id;
	}

	public void setUpload_id(String upload_id) {
		this.upload_id = upload_id;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSavedName() {
		return savedName;
	}

	public void setSavedName(String savedName) {
		this.savedName = savedName;
	}

	public List<CustomerSearchResult> getSearchResults() {
		return searchResults;
	}

	public void setSearchResults(List<CustomerSearchResult> searchResults) {
		this.searchResults = searchResults;
	}
	
	
	
}
