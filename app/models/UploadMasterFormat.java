package models;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class UploadMasterFormat {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;
	public String model_column;
	public String data_type;
	public String excel_column;
	public String upload_format_name;
	public String created_by;
	public Date created_on;
	public String modified_by;
	public Date modified_on;
	public Boolean modified_status;

	public void setId(long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public String getModel_column() {
		return model_column;
	}

	public String getData_type() {
		return data_type;
	}

	public String getExcel_column() {
		return excel_column;
	}

	public String getUpload_format_name() {
		return upload_format_name;
	}

	public String getCreated_by() {
		return created_by;
	}

	public Date getCreated_on() {
		return created_on;
	}

	public String getModified_by() {
		return modified_by;
	}

	public Date getModified_on() {
		return modified_on;
	}

	public Boolean getModified_status() {
		return modified_status;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setModel_column(String model_column) {
		this.model_column = model_column;
	}

	public void setData_type(String data_type) {
		this.data_type = data_type;
	}

	public void setExcel_column(String excel_column) {
		this.excel_column = excel_column;
	}

	public void setUpload_format_name(String upload_format_name) {
		this.upload_format_name = upload_format_name;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	public void setCreated_on(Date created_on) {
		this.created_on = created_on;
	}

	public void setModified_by(String modified_by) {
		this.modified_by = modified_by;
	}

	public void setModified_on(Date modified_on) {
		this.modified_on = modified_on;
	}

	public void setModified_status(Boolean modified_status) {
		this.modified_status = modified_status;
	}

}
