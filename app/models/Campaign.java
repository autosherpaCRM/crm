package models;

import java.util.List;
import javax.persistence.*;

@Entity
public class Campaign {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;

	/*
	 * Default SMR
	 * 
	 */
	public String campaignType;
	public String campaignName;
	public String campaignTypeIdValue;

	public boolean isValid;

	@Temporal(TemporalType.TIMESTAMP)
	public java.util.Date createdDate;

	@Temporal(TemporalType.TIMESTAMP)
	public java.util.Date expiryDate;

	@OneToMany(mappedBy = "campaign")
	public List<AssignedInteraction> assignedInteraction;

	@ManyToOne(fetch = FetchType.EAGER)
	public Location location;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "campaign")
	public List<PSFAssignedInteraction> psfAssignedInteraction;

	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "campaign")
	public List<InsuranceAssignedInteraction> insuranceAssignedInteraction;

	
	@ManyToOne
	public Workshop workshop;

	public String getCampaignTypeIdValue() {
		return campaignTypeIdValue;
	}

	public void setCampaignTypeIdValue(String campaignTypeIdValue) {
		this.campaignTypeIdValue = campaignTypeIdValue;
	}

	public Workshop getWorkshop() {
		return workshop;
	}

	public void setWorkshop(Workshop workshop) {
		this.workshop = workshop;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public List<AssignedInteraction> getAssignedInteraction() {
		return assignedInteraction;
	}

	public void setAssignedInteraction(List<AssignedInteraction> assignedInteraction) {
		this.assignedInteraction = assignedInteraction;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCampaignType() {
		return campaignType;
	}

	public void setCampaignType(String campaignType) {
		this.campaignType = campaignType;
	}

	public boolean isValid() {
		return isValid;
	}

	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}

	public java.util.Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(java.util.Date createdDate) {
		this.createdDate = createdDate;
	}

	public java.util.Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(java.util.Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public List<AssignedInteraction> getAssignedInteractions() {
		return assignedInteraction;
	}

	public void setAssignedInteractions(List<AssignedInteraction> assignedInteraction) {
		this.assignedInteraction = assignedInteraction;
	}

	public String getCampaignName() {
		return campaignName;
	}

	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	public boolean isIsValid() {
		return isValid;
	}

	public void setIsValid(boolean isValid) {
		this.isValid = isValid;
	}

	public List<PSFAssignedInteraction> getPsfAssignedInteraction() {
		return psfAssignedInteraction;
	}

	public void setPsfAssignedInteraction(List<PSFAssignedInteraction> psfAssignedInteraction) {
		this.psfAssignedInteraction = psfAssignedInteraction;
	}

	public List<InsuranceAssignedInteraction> getInsuranceAssignedInteraction() {
		return insuranceAssignedInteraction;
	}

	public void setInsuranceAssignedInteraction(List<InsuranceAssignedInteraction> insuranceAssignedInteraction) {
		this.insuranceAssignedInteraction = insuranceAssignedInteraction;
	}
	
	

}
