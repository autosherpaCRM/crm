package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class SMRDataError {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;

	@Column(length = 100)
	public String followUpType;
	@Column(length = 100)
	public String followUpNum;

	@Column(length = 100)
	public String followUpOldStr;

	@Column(length = 100)
	public String followUpNewStr;

	@Column(length = 100)
	public String regNo;
	@Column(length = 100)
	public String model;
	@Column(length = 100)
	public String fuelType;
	@Column(length = 100)
	public String chassis;

	@Column(length = 100)
	public String mileageStr;

	@Column(length = 100)
	public String dueDateStr;

	@Column(length = 100)
	public String dueService;
	@Column(length = 100)
	public String lastServiceType;

	@Column(length = 100)
	public String lastServiceDateStr;

	@Column(length = 100)
	public String saleDateStr;

	@Column(length = 100)
	public String custCat;
	@Column(length = 100)
	public String custName;
	@Column(length = 600)
	public String address;
	@Column(length = 100)
	public String telephone;
	@Column(length = 100)
	public String mobile;
	@Column(length = 100)
	public String custContactStatus;
	@Column(length = 100)
	public String currentJC;
	@Column(length = 100)
	public String serviceType;
	@Column(length = 100)
	public String Status;
	@Column(length = 100)
	public String nextFollowupDate;

	@Column(length = 100)
	public String custPickUpType;

	@Column(length = 100)
	public String custEmailIdStatus;

	@Column(length = 100)
	public String lastfollowUpRemarks;

	@Column(length = 100)
	public String followUpRemarks;

	@Column(length = 100)
	public String followUpResponse;

	@Column(length = 100)
	public String deliveryDateStr;

	@Column(length = 100)
	public String pickupDrop;

	@Column(length = 100)
	public String extWarranty;

	public int rowNumber;
	public boolean isError;

	@Column(length = 700)
	public String errorInformation;
	
	@Column(length = 100)
	public String upload_id;
	
//	public java.sql.Date followUpOld;
//	public java.sql.Date followUpNew;
//	public Long mileage;
//	public java.sql.Date dueDate;
//	public java.sql.Date lastServiceDate;
//	public java.sql.Date saleDate;
//	public java.sql.Date deliveryDate;

	public long getId() {
		return id;
	}

	public String getFollowUpType() {
		return followUpType;
	}

	public void setFollowUpType(String followUpType) {
		this.followUpType = followUpType;
	}

	public String getFollowUpNum() {
		return followUpNum;
	}

	public void setFollowUpNum(String followUpNum) {
		this.followUpNum = followUpNum;
	}

	public String getFollowUpOldStr() {
		return followUpOldStr;
	}

	public void setFollowUpOldStr(String followUpOldStr) {
		this.followUpOldStr = followUpOldStr;
	}

	public String getFollowUpNewStr() {
		return followUpNewStr;
	}

	public void setFollowUpNewStr(String followUpNewStr) {
		this.followUpNewStr = followUpNewStr;
	}

	public String getRegNo() {
		return regNo;
	}

	public void setRegNo(String regNo) {
		this.regNo = regNo;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getFuelType() {
		return fuelType;
	}

	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}

	public String getChassis() {
		return chassis;
	}

	public void setChassis(String chassis) {
		this.chassis = chassis;
	}

	public String getMileageStr() {
		return mileageStr;
	}

	public void setMileageStr(String mileageStr) {
		this.mileageStr = mileageStr;
	}

	public String getDueDateStr() {
		return dueDateStr;
	}

	public void setDueDateStr(String dueDateStr) {
		this.dueDateStr = dueDateStr;
	}

	public String getDueService() {
		return dueService;
	}

	public void setDueService(String dueService) {
		this.dueService = dueService;
	}

	public String getLastServiceType() {
		return lastServiceType;
	}

	public void setLastServiceType(String lastServiceType) {
		this.lastServiceType = lastServiceType;
	}

	public String getLastServiceDateStr() {
		return lastServiceDateStr;
	}

	public void setLastServiceDateStr(String lastServiceDateStr) {
		this.lastServiceDateStr = lastServiceDateStr;
	}

	public String getSaleDateStr() {
		return saleDateStr;
	}

	public void setSaleDateStr(String saleDateStr) {
		this.saleDateStr = saleDateStr;
	}

	public String getCustCat() {
		return custCat;
	}

	public void setCustCat(String custCat) {
		this.custCat = custCat;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getCustContactStatus() {
		return custContactStatus;
	}

	public void setCustContactStatus(String custContactStatus) {
		this.custContactStatus = custContactStatus;
	}

	public String getCurrentJC() {
		return currentJC;
	}

	public void setCurrentJC(String currentJC) {
		this.currentJC = currentJC;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getNextFollowupDate() {
		return nextFollowupDate;
	}

	public void setNextFollowupDate(String nextFollowupDate) {
		this.nextFollowupDate = nextFollowupDate;
	}

	public String getCustPickUpType() {
		return custPickUpType;
	}

	public void setCustPickUpType(String custPickUpType) {
		this.custPickUpType = custPickUpType;
	}

	public String getCustEmailIdStatus() {
		return custEmailIdStatus;
	}

	public void setCustEmailIdStatus(String custEmailIdStatus) {
		this.custEmailIdStatus = custEmailIdStatus;
	}

	public String getLastfollowUpRemarks() {
		return lastfollowUpRemarks;
	}

	public void setLastfollowUpRemarks(String lastfollowUpRemarks) {
		this.lastfollowUpRemarks = lastfollowUpRemarks;
	}

	public String getFollowUpRemarks() {
		return followUpRemarks;
	}

	public void setFollowUpRemarks(String followUpRemarks) {
		this.followUpRemarks = followUpRemarks;
	}

	public String getFollowUpResponse() {
		return followUpResponse;
	}

	public void setFollowUpResponse(String followUpResponse) {
		this.followUpResponse = followUpResponse;
	}

	public String getDeliveryDateStr() {
		return deliveryDateStr;
	}

	public void setDeliveryDateStr(String deliveryDateStr) {
		this.deliveryDateStr = deliveryDateStr;
	}

	public String getPickupDrop() {
		return pickupDrop;
	}

	public void setPickupDrop(String pickupDrop) {
		this.pickupDrop = pickupDrop;
	}

	public String getExtWarranty() {
		return extWarranty;
	}

	public void setExtWarranty(String extWarranty) {
		this.extWarranty = extWarranty;
	}

	public int getRowNumber() {
		return rowNumber;
	}

	public void setRowNumber(int rowNumber) {
		this.rowNumber = rowNumber;
	}

	public boolean isError() {
		return isError;
	}

	public void setError(boolean isError) {
		this.isError = isError;
	}

	public String getErrorInformation() {
		return errorInformation;
	}

	public void setErrorInformation(String errorInformation) {
		this.errorInformation = errorInformation;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUpload_id() {
		return upload_id;
	}

	public void setUpload_id(String upload_id) {
		this.upload_id = upload_id;
	}
	
	
	
	
}
