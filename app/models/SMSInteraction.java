package models;

import javax.persistence.*;

@Entity
public class SMSInteraction {

	public static String INTERACTION_TYPE_COMPLAINT = "Compliant";
	public static String INTERACTION_TYPE_OFFERS = "Offers";
	public static String INTERACTION_TYPE_CUSTOM = "Custom";
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long smsId;

	@Column(length = 160)
	public String smsMessage;

	@Column(length = 30)
	public String smsHeader;

	@Temporal(TemporalType.TIMESTAMP)
	public java.util.Date interactionDateAndTime;

	@Column(length = 30)
	public String interactionType;

	public boolean smsStatus;
	
	@Column(length = 100)
	public String responseFromGateway;


	@Column(length = 30)
	public String interactionDate;
	@Column(length = 30)
	public String interactionTime;
	@Column(length = 30)
	public String mobileNumber;

	@ManyToOne(cascade = CascadeType.ALL)
	public Customer customer;

	@ManyToOne(cascade = CascadeType.ALL)
	public WyzUser wyzUser;

	@ManyToOne(cascade = CascadeType.ALL)
	public Vehicle vehicle;

	
	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public long getSmsId() {
		return smsId;
	}

	public void setSmsId(long smsId) {
		this.smsId = smsId;
	}

	public String getSmsMessage() {
		return smsMessage;
	}

	public void setSmsMessage(String smsMessage) {
		this.smsMessage = smsMessage;
	}

	public String getSmsHeader() {
		return smsHeader;
	}

	public void setSmsHeader(String smsHeader) {
		this.smsHeader = smsHeader;
	}

	public java.util.Date getInteractionDateAndTime() {
		return interactionDateAndTime;
	}

	public void setInteractionDateAndTime(java.util.Date interactionDateAndTime) {
		this.interactionDateAndTime = interactionDateAndTime;
	}

	public String getInteractionDate() {
		return interactionDate;
	}

	public void setInteractionDate(String interactionDate) {
		this.interactionDate = interactionDate;
	}

	public String getInteractionTime() {
		return interactionTime;
	}

	public void setInteractionTime(String interactionTime) {
		this.interactionTime = interactionTime;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public WyzUser getWyzUser() {
		return wyzUser;
	}

	public void setWyzUser(WyzUser wyzUser) {
		this.wyzUser = wyzUser;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	public boolean isSmsStatus() {
		return smsStatus;
	}

	public void setSmsStatus(boolean smsStatus) {
		this.smsStatus = smsStatus;
	}

	public String getInteractionType() {
		return interactionType;
	}

	public void setInteractionType(String interactionType) {
		this.interactionType = interactionType;
	}
	
	public String getResponseFromGateway() {
		return responseFromGateway;
	}

	public void setResponseFromGateway(String responseFromGateway) {
		this.responseFromGateway = responseFromGateway;
	}


}
