package models;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity
public class ComplaintInteraction {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;

	@Column(length = 300)
	public String description;
	public String complaintStatus;

	@Temporal(TemporalType.DATE)
	public Date UpdatedDate;

	@Temporal(TemporalType.DATE)
	public Date assignedDate;

	@Column(length = 100)
	public String functionName;
	@Column(length = 150)
	public String sourceName;
	@Column(length = 100)
	public String subcomplaintType;

	public String actionTaken;
	public String resolutionBy;
	public String reasonFor;
	public String customerstatus;

	public String priority;

	@ManyToOne
	public Location location;

	@ManyToOne
	public Workshop workshop;

	@ManyToOne(fetch = FetchType.EAGER)
	public Vehicle vehicle;

	@ManyToOne(fetch = FetchType.EAGER)
	public WyzUser assignedUser;

	@ManyToOne(fetch = FetchType.EAGER)
	public Complaint complaint;

	@ManyToOne
	public Customer customer;

	public Complaint getComplaint() {
		return complaint;
	}

	public void setComplaint(Complaint complaint) {
		this.complaint = complaint;
	}

	
	public Date getAssignedDate() {
		return assignedDate;
	}

	public void setAssignedDate(Date assignedDate) {
		this.assignedDate = assignedDate;
	}

	public Date getUpdatedDate() {
		return UpdatedDate;
	}

	public void setUpdatedDate(Date UpdatedDate) {
		this.UpdatedDate = UpdatedDate;
	}

	public Workshop getWorkshop() {
		return workshop;
	}

	public void setWorkshop(Workshop workshop) {
		this.workshop = workshop;
	}

	

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getActionTaken() {
		return actionTaken;
	}

	public void setActionTaken(String actionTaken) {
		this.actionTaken = actionTaken;
	}

	public String getCustomerstatus() {
		return customerstatus;
	}

	public void setCustomerstatus(String customerstatus) {
		this.customerstatus = customerstatus;
	}

	public String getResolutionBy() {
		return resolutionBy;
	}

	public void setResolutionBy(String resolutionBy) {
		this.resolutionBy = resolutionBy;
	}

	public String getReasonFor() {
		return reasonFor;
	}

	public void setReasonFor(String reasonFor) {
		this.reasonFor = reasonFor;
	}

	public String getFunctionName() {
		return functionName;
	}

	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public String getSubcomplaintType() {
		return subcomplaintType;
	}

	public void setSubcomplaintType(String subcomplaintType) {
		this.subcomplaintType = subcomplaintType;
	}

	

	public WyzUser getAssignedUser() {
		return assignedUser;
	}
	
	public void setAssignedUser(WyzUser assignedUser) {
		this.assignedUser = assignedUser;
	}

	public String complaintNumber;

	public String getComplaintNumber() {
		return complaintNumber;
	}

	public void setComplaintNumber(String complaintNumber) {
		this.complaintNumber = complaintNumber;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}


	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	

	public String getComplaintStatus() {
		return complaintStatus;
	}

	public void setComplaintStatus(String complaintStatus) {
		this.complaintStatus = complaintStatus;
	}

	

}
