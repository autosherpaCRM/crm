package models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class UserSession {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;

	@Temporal(TemporalType.TIMESTAMP)
	public Date sessionDateandTime;

	@Column(length = 200)
	public String ipAddress;

	@Column(length = 30)
	public String userLogined;
	
	@Temporal(TemporalType.TIMESTAMP)
	public Date loggedIn;
	
	@Temporal(TemporalType.TIMESTAMP)
	public Date lastSeen;


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getSessionDateandTime() {
		return sessionDateandTime;
	}

	public void setSessionDateandTime(Date sessionDateandTime) {
		this.sessionDateandTime = sessionDateandTime;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getUserLogined() {
		return userLogined;
	}

	public void setUserLogined(String userLogined) {
		this.userLogined = userLogined;
	}

	public Date getLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(Date loggedIn) {
		this.loggedIn = loggedIn;
	}

	public Date getLastSeen() {
		return lastSeen;
	}

	public void setLastSeen(Date lastSeen) {
		this.lastSeen = lastSeen;
	}
	
}
