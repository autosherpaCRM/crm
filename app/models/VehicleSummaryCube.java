package models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class VehicleSummaryCube {
	
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		public long id;
		@Column(length = 50)
		public String ChassisNo;
		@Column(length = 50)
		public String vehicleRegNo;
	
		@Column(length = 50)
		public String phoneNumber;
		@Column(length = 50)
		public String customerName;
		@Column(length = 50)
		public String campaign;
		@Temporal(TemporalType.DATE)
		public Date serviceDueDate;
		@Column(length = 50)
		public String serviceDueType;
		@Temporal(TemporalType.DATE)
		public Date uploadedDate;
		@Temporal(TemporalType.DATE)
		public Date assignedDate;
		@Column(length = 50)
		public String assignedCre;
		@Column(length = 50)
		public String callStatus;
		@Column(length = 50)
		public String attempts;
		@Temporal(TemporalType.DATE)
		public Date firstCallDate;
		@Column(length = 50)
		public String firstCallDipso;
		@Temporal(TemporalType.DATE)
		public Date lastCallDate;
		@Column(length = 50)
		public String lastCallDispoition;
		@Column(length = 50)
		public String lastCallCre;
		@Column(length = 50)
		public String isBookingDone;
		@Temporal(TemporalType.DATE)
		public Date bookingCallDate;
		@Temporal(TemporalType.DATE)
		public Date bookingScheduledDate;
		@Column(length = 50)
		public String bookingType;
		@Column(length = 50)
		public String bookingStatus;
		@Temporal(TemporalType.DATE)
		public Date dataRemovalDate;
		@Column(length = 50)
		public String repairOrderNo;
		@Temporal(TemporalType.DATE)
		public Date roDate;
		@Column(length = 50)
		public String reportedStatus;
		public long vehicle_id;
		public long customer_id;
		public long firstCallInteractionID;
		public long firstCallDispositionID;
		public long lastCallInteractionID;
		public long lastCallDispositionID;
		public long lastCallWyzuserID;
		public long assignedWyzuserID;
		public long bookingStatusID;
		public long assignedInteractionID;
		@Column(length = 50)
		public String callMade;
		@Column(length = 50)
		public String isRemoval;
		@Column(length = 50)
		public String whenRemoved;
		public long upload_id;
		public long uploadVehicle_id;
		@Column(length = 50)
		public String updated_date;
		public long getId() {
			return id;
		}
		public void setId(long id) {
			this.id = id;
		}
		public String getChassisNo() {
			return ChassisNo;
		}
		public void setChassisNo(String chassisNo) {
			ChassisNo = chassisNo;
		}
		public String getPhoneNumber() {
			return phoneNumber;
		}
		public void setPhoneNumber(String phoneNumber) {
			this.phoneNumber = phoneNumber;
		}
		public String getCustomerName() {
			return customerName;
		}
		public void setCustomerName(String customerName) {
			this.customerName = customerName;
		}
		public String getCampaign() {
			return campaign;
		}
		public void setCampaign(String campaign) {
			this.campaign = campaign;
		}
		public Date getServiceDueDate() {
			return serviceDueDate;
		}
		public void setServiceDueDate(Date serviceDueDate) {
			this.serviceDueDate = serviceDueDate;
		}
		public String getServiceDueType() {
			return serviceDueType;
		}
		public void setServiceDueType(String serviceDueType) {
			this.serviceDueType = serviceDueType;
		}
		public Date getUploadedDate() {
			return uploadedDate;
		}
		public void setUploadedDate(Date uploadedDate) {
			this.uploadedDate = uploadedDate;
		}
		public Date getAssignedDate() {
			return assignedDate;
		}
		public void setAssignedDate(Date assignedDate) {
			this.assignedDate = assignedDate;
		}
		public String getAssignedCre() {
			return assignedCre;
		}
		public void setAssignedCre(String assignedCre) {
			this.assignedCre = assignedCre;
		}
		public String getCallStatus() {
			return callStatus;
		}
		public void setCallStatus(String callStatus) {
			this.callStatus = callStatus;
		}
		public String getAttempts() {
			return attempts;
		}
		public void setAttempts(String attempts) {
			this.attempts = attempts;
		}
		public Date getFirstCallDate() {
			return firstCallDate;
		}
		public void setFirstCallDate(Date firstCallDate) {
			this.firstCallDate = firstCallDate;
		}
		public String getFirstCallDipso() {
			return firstCallDipso;
		}
		public void setFirstCallDipso(String firstCallDipso) {
			this.firstCallDipso = firstCallDipso;
		}
		public Date getLastCallDate() {
			return lastCallDate;
		}
		public void setLastCallDate(Date lastCallDate) {
			this.lastCallDate = lastCallDate;
		}
		public String getLastCallDispoition() {
			return lastCallDispoition;
		}
		public void setLastCallDispoition(String lastCallDispoition) {
			this.lastCallDispoition = lastCallDispoition;
		}
		public String getLastCallCre() {
			return lastCallCre;
		}
		public void setLastCallCre(String lastCallCre) {
			this.lastCallCre = lastCallCre;
		}
		public String getIsBookingDone() {
			return isBookingDone;
		}
		public void setIsBookingDone(String isBookingDone) {
			this.isBookingDone = isBookingDone;
		}
		public Date getBookingCallDate() {
			return bookingCallDate;
		}
		public void setBookingCallDate(Date bookingCallDate) {
			this.bookingCallDate = bookingCallDate;
		}
		public Date getBookingScheduledDate() {
			return bookingScheduledDate;
		}
		public void setBookingScheduledDate(Date bookingScheduledDate) {
			this.bookingScheduledDate = bookingScheduledDate;
		}
		public String getBookingType() {
			return bookingType;
		}
		public void setBookingType(String bookingType) {
			this.bookingType = bookingType;
		}
		public String getBookingStatus() {
			return bookingStatus;
		}
		public void setBookingStatus(String bookingStatus) {
			this.bookingStatus = bookingStatus;
		}
		public Date getDataRemovalDate() {
			return dataRemovalDate;
		}
		public void setDataRemovalDate(Date dataRemovalDate) {
			this.dataRemovalDate = dataRemovalDate;
		}
		public String getRepairOrderNo() {
			return repairOrderNo;
		}
		public void setRepairOrderNo(String repairOrderNo) {
			this.repairOrderNo = repairOrderNo;
		}
		public Date getRoDate() {
			return roDate;
		}
		public void setRoDate(Date roDate) {
			this.roDate = roDate;
		}
		public String getReportedStatus() {
			return reportedStatus;
		}
		public void setReportedStatus(String reportedStatus) {
			this.reportedStatus = reportedStatus;
		}
		public long getVehicle_id() {
			return vehicle_id;
		}
		public void setVehicle_id(long vehicle_id) {
			this.vehicle_id = vehicle_id;
		}
		public long getCustomer_id() {
			return customer_id;
		}
		public void setCustomer_id(long customer_id) {
			this.customer_id = customer_id;
		}
		public long getFirstCallInteractionID() {
			return firstCallInteractionID;
		}
		public void setFirstCallInteractionID(long firstCallInteractionID) {
			this.firstCallInteractionID = firstCallInteractionID;
		}
		public long getFirstCallDispositionID() {
			return firstCallDispositionID;
		}
		public void setFirstCallDispositionID(long firstCallDispositionID) {
			this.firstCallDispositionID = firstCallDispositionID;
		}
		public long getLastCallInteractionID() {
			return lastCallInteractionID;
		}
		public void setLastCallInteractionID(long lastCallInteractionID) {
			this.lastCallInteractionID = lastCallInteractionID;
		}
		public long getLastCallDispositionID() {
			return lastCallDispositionID;
		}
		public void setLastCallDispositionID(long lastCallDispositionID) {
			this.lastCallDispositionID = lastCallDispositionID;
		}
		public long getLastCallWyzuserID() {
			return lastCallWyzuserID;
		}
		public void setLastCallWyzuserID(long lastCallWyzuserID) {
			this.lastCallWyzuserID = lastCallWyzuserID;
		}
		public long getAssignedWyzuserID() {
			return assignedWyzuserID;
		}
		public void setAssignedWyzuserID(long assignedWyzuserID) {
			this.assignedWyzuserID = assignedWyzuserID;
		}
		public long getBookingStatusID() {
			return bookingStatusID;
		}
		public void setBookingStatusID(long bookingStatusID) {
			this.bookingStatusID = bookingStatusID;
		}
		public long getAssignedInteractionID() {
			return assignedInteractionID;
		}
		public void setAssignedInteractionID(long assignedInteractionID) {
			this.assignedInteractionID = assignedInteractionID;
		}
		public String getCallMade() {
			return callMade;
		}
		public void setCallMade(String callMade) {
			this.callMade = callMade;
		}
		public String getIsRemoval() {
			return isRemoval;
		}
		public void setIsRemoval(String isRemoval) {
			this.isRemoval = isRemoval;
		}
		public String getWhenRemoved() {
			return whenRemoved;
		}
		public void setWhenRemoved(String whenRemoved) {
			this.whenRemoved = whenRemoved;
		}
		public long getUpload_id() {
			return upload_id;
		}
		public void setUpload_id(long upload_id) {
			this.upload_id = upload_id;
		}
		public long getUploadVehicle_id() {
			return uploadVehicle_id;
		}
		public void setUploadVehicle_id(long uploadVehicle_id) {
			this.uploadVehicle_id = uploadVehicle_id;
		}
		public String getUpdated_date() {
			return updated_date;
		}
		public void setUpdated_date(String updated_date) {
			this.updated_date = updated_date;
		}
		public String getVehicleRegNo() {
			return vehicleRegNo;
		}
		public void setVehicleRegNo(String vehicleRegNo) {
			this.vehicleRegNo = vehicleRegNo;
		}
		
		
}
