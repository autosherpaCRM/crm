
package models;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class TaggingUsers {

	public static int LEAD_INSURANCE = 1;
	public static int LEAD_WARRANTY_EW = 2;
	public static int LEAD_VAS = 3;
	public static int LEAD_RE_FINANCE = 4;
	public static int LEAD_SELL_OLD_CAR = 5;
	public static int LEAD_BUY_NEW_CAR = 6;
	public static int LEAD_SALES = 7;
	public static int LEAD_WORKSHOP = 8;
	public static int LEAD_USED_CAR = 9;
	public static int LEAD_MAXICARE=10;
	public static int LEAD_ROAD_SIDE_ASSISTANCE=11;
	public static int LEAD_SHIELD=12;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;

	public int upsellLeadId;

	@Column(length = 60)
	public String name;

	@Column(length = 60)
	public String upsellType;

	@Column(length = 60)
	public String phoneNumber;	

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "taggingUsers")
	public List<UpsellLead> upsellLead;

	@ManyToOne(fetch = FetchType.EAGER)
	public Location location;
	
	@ManyToOne(fetch = FetchType.EAGER)
	public WyzUser wyzUser;
	

	public List<UpsellLead> getUpsellLead() {
		return upsellLead;
	}

	public void setUpsellLead(List<UpsellLead> upsellLead) {
		this.upsellLead = upsellLead;
	}

	public int getUpsellLeadId() {
		return upsellLeadId;
	}

	public void setUpsellLeadId(int upsellLeadId) {
		this.upsellLeadId = upsellLeadId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUpsellType() {
		return upsellType;
	}

	public void setUpsellType(String upsellType) {
		this.upsellType = upsellType;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public WyzUser getWyzUser() {
		return wyzUser;
	}

	public void setWyzUser(WyzUser wyzUser) {
		this.wyzUser = wyzUser;
	}

	
	
	

}
