package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class JobCardStatusError {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;
	
	@Column(length = 100)
	public String slNo;
	
	@Column(length = 100)
	public String jobCardNo;
	
	@Column(length = 100)
	public String jobCardDateStr;
	
	@Column(length = 100)
	public String repeatRevisit;
	
	@Column(length = 100)
	public String custName;
	
	@Column(length = 100)
	public String phone;
	
	@Column(length = 100)
	public String custCat;
	
	@Column(length = 100)
	public String psfStatus;
	
	@Column(length = 100)
	public String regNo;
	
	@Column(length = 100)
	public String chassis;
	
	@Column(length = 100)
	public String model;
	
	@Column(length = 100)
	public String mi;
	
	@Column(length = 100)
	public String saleDateStr;
	
	@Column(length = 100)
	public String groupData;
	
	@Column(length = 100)
	public String serviceAdvisor;
	
	@Column(length = 100)
	public String technician;
	
	@Column(length = 100)
	public String circularNo;
	
	@Column(length = 100)
	public String serviceType;
	
	@Column(length = 100)
	public String mileageStr;
	@Column(length = 100)
	public String estLabAmtStr;
	@Column(length = 100)
	public String estPartAmtStr;
	@Column(length = 100)
	public String promiseDtStr;
	@Column(length = 100)
	public String revPromisedDtStr;
	@Column(length = 100)
	public String readyDateandTimeStr;
	
	@Column(length = 100)
	public String status;
	
	@Column(length = 100)
	public String billNo;
	
	@Column(length = 100)
	public String labAmtStr;
	@Column(length = 100)
	public String partsAmtStr;
	
	@Column(length = 100)
	public String pickupRequired;
	
	@Column(length = 100)
	public String pickupDateStr;
	
	@Column(length = 100)
	public String pickupLoc;
	
	@Column(length = 100)
	public String billDateStr;
	
	@Column(length = 100)
	public String billAmtStr;
	
	@Column(length = 200)
	public String address1;
	
	@Column(length = 200)
	public String address2;
	
	@Column(length = 200)
	public String address3;
	
	@Column(length = 100)
	public String city;
	
	@Column(length = 100)
	public String pincodeStr;
	@Column(length = 100)
	public String dobStr;
	@Column(length = 100)
	public String doaStr;
	
	@Column(length = 100)
	public String engineNo;
	
	@Column(length = 100)
	public String variant;
	
	@Column(length = 100)
	public String color;
	
	@Column(length = 100)
	public String mobile;
	
	@Column(length = 100)
	public String yearStr;
	
	@Column(length = 100)
	public String centre;
	
	@Column(length = 100)
	public String upload_id;
	
	public int rowNumber;
	public boolean isError;

	@Column(length = 700)
	public String errorInformation;
	
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getSlNo() {
		return slNo;
	}
	public void setSlNo(String slNo) {
		this.slNo = slNo;
	}
	public String getJobCardNo() {
		return jobCardNo;
	}
	public void setJobCardNo(String jobCardNo) {
		this.jobCardNo = jobCardNo;
	}
	
	public String getRepeatRevisit() {
		return repeatRevisit;
	}
	public void setRepeatRevisit(String repeatRevisit) {
		this.repeatRevisit = repeatRevisit;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getCustCat() {
		return custCat;
	}
	public void setCustCat(String custCat) {
		this.custCat = custCat;
	}
	public String getPsfStatus() {
		return psfStatus;
	}
	public void setPsfStatus(String psfStatus) {
		this.psfStatus = psfStatus;
	}
	public String getRegNo() {
		return regNo;
	}
	public void setRegNo(String regNo) {
		this.regNo = regNo;
	}
	public String getChassis() {
		return chassis;
	}
	public void setChassis(String chassis) {
		this.chassis = chassis;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getMi() {
		return mi;
	}
	public void setMi(String mi) {
		this.mi = mi;
	}
		
	public String getGroupData() {
		return groupData;
	}
	public void setGroupData(String groupData) {
		this.groupData = groupData;
	}
	public String getServiceAdvisor() {
		return serviceAdvisor;
	}
	public void setServiceAdvisor(String serviceAdvisor) {
		this.serviceAdvisor = serviceAdvisor;
	}
	public String getTechnician() {
		return technician;
	}
	public void setTechnician(String technician) {
		this.technician = technician;
	}
	public String getCircularNo() {
		return circularNo;
	}
	public void setCircularNo(String circularNo) {
		this.circularNo = circularNo;
	}
	public String getServiceType() {
		return serviceType;
	}
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getBillNo() {
		return billNo;
	}
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	
	public String getPickupRequired() {
		return pickupRequired;
	}
	public void setPickupRequired(String pickupRequired) {
		this.pickupRequired = pickupRequired;
	}
	
	public String getPickupLoc() {
		return pickupLoc;
	}
	public void setPickupLoc(String pickupLoc) {
		this.pickupLoc = pickupLoc;
	}
	
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getAddress3() {
		return address3;
	}
	public void setAddress3(String address3) {
		this.address3 = address3;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getEngineNo() {
		return engineNo;
	}
	public void setEngineNo(String engineNo) {
		this.engineNo = engineNo;
	}
	public String getVariant() {
		return variant;
	}
	public void setVariant(String variant) {
		this.variant = variant;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
	public String getCentre() {
		return centre;
	}
	public void setCentre(String centre) {
		this.centre = centre;
	}
	public int getRowNumber() {
		return rowNumber;
	}
	public void setRowNumber(int rowNumber) {
		this.rowNumber = rowNumber;
	}
	public boolean isError() {
		return isError;
	}
	public void setError(boolean isError) {
		this.isError = isError;
	}
	public String getErrorInformation() {
		return errorInformation;
	}
	public void setErrorInformation(String errorInformation) {
		this.errorInformation = errorInformation;
	}
	public String getJobCardDateStr() {
		return jobCardDateStr;
	}
	public void setJobCardDateStr(String jobCardDateStr) {
		this.jobCardDateStr = jobCardDateStr;
	}
	public String getSaleDateStr() {
		return saleDateStr;
	}
	public void setSaleDateStr(String saleDateStr) {
		this.saleDateStr = saleDateStr;
	}
	public String getMileageStr() {
		return mileageStr;
	}
	public void setMileageStr(String mileageStr) {
		this.mileageStr = mileageStr;
	}
	public String getEstLabAmtStr() {
		return estLabAmtStr;
	}
	public void setEstLabAmtStr(String estLabAmtStr) {
		this.estLabAmtStr = estLabAmtStr;
	}
	public String getEstPartAmtStr() {
		return estPartAmtStr;
	}
	public void setEstPartAmtStr(String estPartAmtStr) {
		this.estPartAmtStr = estPartAmtStr;
	}
	public String getPromiseDtStr() {
		return promiseDtStr;
	}
	public void setPromiseDtStr(String promiseDtStr) {
		this.promiseDtStr = promiseDtStr;
	}
	public String getRevPromisedDtStr() {
		return revPromisedDtStr;
	}
	public void setRevPromisedDtStr(String revPromisedDtStr) {
		this.revPromisedDtStr = revPromisedDtStr;
	}
	public String getReadyDateandTimeStr() {
		return readyDateandTimeStr;
	}
	public void setReadyDateandTimeStr(String readyDateandTimeStr) {
		this.readyDateandTimeStr = readyDateandTimeStr;
	}
	public String getLabAmtStr() {
		return labAmtStr;
	}
	public void setLabAmtStr(String labAmtStr) {
		this.labAmtStr = labAmtStr;
	}
	public String getPartsAmtStr() {
		return partsAmtStr;
	}
	public void setPartsAmtStr(String partsAmtStr) {
		this.partsAmtStr = partsAmtStr;
	}
	public String getPickupDateStr() {
		return pickupDateStr;
	}
	public void setPickupDateStr(String pickupDateStr) {
		this.pickupDateStr = pickupDateStr;
	}
	public String getBillDateStr() {
		return billDateStr;
	}
	public void setBillDateStr(String billDateStr) {
		this.billDateStr = billDateStr;
	}
	public String getBillAmtStr() {
		return billAmtStr;
	}
	public void setBillAmtStr(String billAmtStr) {
		this.billAmtStr = billAmtStr;
	}
	public String getPincodeStr() {
		return pincodeStr;
	}
	public void setPincodeStr(String pincodeStr) {
		this.pincodeStr = pincodeStr;
	}
	public String getDobStr() {
		return dobStr;
	}
	public void setDobStr(String dobStr) {
		this.dobStr = dobStr;
	}
	public String getDoaStr() {
		return doaStr;
	}
	public void setDoaStr(String doaStr) {
		this.doaStr = doaStr;
	}
	public String getYearStr() {
		return yearStr;
	}
	public void setYearStr(String yearStr) {
		this.yearStr = yearStr;
	}
	public String getUpload_id() {
		return upload_id;
	}
	public void setUpload_id(String upload_id) {
		this.upload_id = upload_id;
	}
	
	
	
	
}
