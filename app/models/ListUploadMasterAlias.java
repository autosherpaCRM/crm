package models;

import java.util.List;

public class ListUploadMasterAlias {

	private List<UploadMasterFormat> upload_sheet_data;

	public List<UploadMasterFormat> getUpload_sheet_data() {
		return upload_sheet_data;
	}

	public void setUpload_sheet_data(List<UploadMasterFormat> upload_sheet_data) {
		this.upload_sheet_data = upload_sheet_data;
	}
}
