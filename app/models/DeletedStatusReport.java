package models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class DeletedStatusReport {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;
	public long vehicle_id;
	public long customer_id;
	public long campaign_id;
	public long final_disposition;
	
	@Temporal(TemporalType.DATE)
	public Date deleted_date;
	
	@Temporal(TemporalType.DATE)
	public Date uploaded_date;
	
	@Column(length = 50)
	public String called_status;
	
	public long assigned_id;
	public long wyzuser_id;
	
	@Column(length = 50)
	public String whendeleted;
	public long upload_id;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getVehicle_id() {
		return vehicle_id;
	}

	public void setVehicle_id(long vehicle_id) {
		this.vehicle_id = vehicle_id;
	}

	public long getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(long customer_id) {
		this.customer_id = customer_id;
	}

	public long getCampaign_id() {
		return campaign_id;
	}

	public void setCampaign_id(long campaign_id) {
		this.campaign_id = campaign_id;
	}

	public long getFinal_disposition() {
		return final_disposition;
	}

	public void setFinal_disposition(long final_disposition) {
		this.final_disposition = final_disposition;
	}

	public Date getDeleted_date() {
		return deleted_date;
	}

	public void setDeleted_date(Date deleted_date) {
		this.deleted_date = deleted_date;
	}

	public Date getUploaded_date() {
		return uploaded_date;
	}

	public void setUploaded_date(Date uploaded_date) {
		this.uploaded_date = uploaded_date;
	}

	public String getCalled_status() {
		return called_status;
	}

	public void setCalled_status(String called_status) {
		this.called_status = called_status;
	}

	public long getAssigned_id() {
		return assigned_id;
	}

	public void setAssigned_id(long assigned_id) {
		this.assigned_id = assigned_id;
	}

	public long getWyzuser_id() {
		return wyzuser_id;
	}

	public void setWyzuser_id(long wyzuser_id) {
		this.wyzuser_id = wyzuser_id;
	}

	public String getWhendeleted() {
		return whendeleted;
	}

	public void setWhendeleted(String whendeleted) {
		this.whendeleted = whendeleted;
	}

	public long getUpload_id() {
		return upload_id;
	}

	public void setUpload_id(long upload_id) {
		this.upload_id = upload_id;
	}
	
	
}
