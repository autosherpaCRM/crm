package models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class UploadType {


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(length = 50, unique = true)
	private String uploadTypeName;

	@Column(length = 100)
	private String uploadDisplayName;

	@Column(length = 100)
	private String uploadextensions;


	@Column(length = 40)
	private String rowsToignore;

	private boolean ignoreSourceColumnNames;

	private boolean ignoreDestinationColumnNames;

	@Column(length = 150)
	private String description;

	private boolean duplicateAllowed;
	
	private boolean formParameters;
	
	
	@OneToMany(cascade = CascadeType.ALL)
	private List<ColumnDefinition> definitions;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUploadTypeName() {
		return uploadTypeName;
	}

	public void setUploadTypeName(String uploadTypeName) {
		this.uploadTypeName = uploadTypeName;
	}

	public List<ColumnDefinition> getDefinitions() {
		return definitions;
	}

	public void setDefinitions(List<ColumnDefinition> definitions) {
		this.definitions = definitions;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isDuplicateAllowed() {
		return duplicateAllowed;
	}

	public void setDuplicateAllowed(boolean duplicateAllowed) {
		this.duplicateAllowed = duplicateAllowed;
	}

	public String getUploadextensions() {
		return uploadextensions;
	}

	public void setUploadextensions(String uploadextensions) {
		this.uploadextensions = uploadextensions;
	}

	public boolean isIgnoreSourceColumnNames() {
		return ignoreSourceColumnNames;
	}

	public void setIgnoreSourceColumnNames(boolean ignoreSourceColumnNames) {
		this.ignoreSourceColumnNames = ignoreSourceColumnNames;
	}

	public boolean isIgnoreDestinationColumnNames() {
		return ignoreDestinationColumnNames;
	}

	public void setIgnoreDestinationColumnNames(boolean ignoreDestinationColumnNames) {
		this.ignoreDestinationColumnNames = ignoreDestinationColumnNames;
	}

	public String getRowsToignore() {
		return rowsToignore;
	}

	public void setRowsToignore(String rowsToignore) {
		this.rowsToignore = rowsToignore;
	}

	public String getUploadDisplayName() {
		return uploadDisplayName;
	}

	public void setUploadDisplayName(String uploadDisplayName) {
		this.uploadDisplayName = uploadDisplayName;
	}

	public boolean isFormParameters() {
		return formParameters;
	}

	public void setFormParameters(boolean formParameters) {
		this.formParameters = formParameters;
	}
}
