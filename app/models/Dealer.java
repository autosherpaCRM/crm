package models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Dealer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;
	public String dealerId;
	public String dealerName;
	public String dealerAddress;
	public String dealerCode;
	public String noOfUsers;
	public boolean webAthentication;
	public boolean AndroidAuthentication;
	public boolean webAndAndroidAuthen;
	public boolean webAuthWithCallInitiating;
	public String timeZoneOfDealer;
	public String OEM;

	@OneToMany(mappedBy = "dealer", cascade = CascadeType.ALL)
	public List<WyzUser> wyzUser;

	public List<WyzUser> getWyzUser() {
		return wyzUser;
	}

	public void setWyzUser(List<WyzUser> wyzUser) {
		this.wyzUser = wyzUser;
	}
	
	@OneToMany(mappedBy = "dealer", cascade = CascadeType.ALL)
	public List<Location> location;	

	public List<Location> getLocation() {
		return location;
	}

	public void setLocation(List<Location> location) {
		this.location = location;
	}

	
	public long getId() {
		return id;
	}

	public void setId(long did) {
		this.id = did;
	}

	public String getDealerId() {
		return dealerId;
	}

	public void setDealerId(String dealerId) {
		this.dealerId = dealerId;
	}

	public String getDealerName() {
		return dealerName;
	}

	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}

	public String getDealerAddress() {
		return dealerAddress;
	}

	public void setDealerAddress(String dealerAddress) {
		this.dealerAddress = dealerAddress;
	}

	public String getNoOfUsers() {
		return noOfUsers;
	}

	public void setNoOfUsers(String noOfUsers) {
		this.noOfUsers = noOfUsers;
	}

	public String getDealerCode() {
		return dealerCode;
	}

	public void setDealerCode(String dealerCode) {
		this.dealerCode = dealerCode;
	}

	public boolean isWebAthentication() {
		return webAthentication;
	}

	public void setWebAthentication(boolean webAthentication) {
		this.webAthentication = webAthentication;
	}

	public boolean isAndroidAuthentication() {
		return AndroidAuthentication;
	}

	public void setAndroidAuthentication(boolean androidAuthentication) {
		AndroidAuthentication = androidAuthentication;
	}

	public boolean isWebAndAndroidAuthen() {
		return webAndAndroidAuthen;
	}

	public void setWebAndAndroidAuthen(boolean webAndAndroidAuthen) {
		this.webAndAndroidAuthen = webAndAndroidAuthen;
	}

	public boolean isWebAuthWithCallInitiating() {
		return webAuthWithCallInitiating;
	}

	public void setWebAuthWithCallInitiating(boolean webAuthWithCallInitiating) {
		this.webAuthWithCallInitiating = webAuthWithCallInitiating;
	}

	public String getTimeZoneOfDealer() {
		return timeZoneOfDealer;
	}

	public void setTimeZoneOfDealer(String timeZoneOfDealer) {
		this.timeZoneOfDealer = timeZoneOfDealer;
	}

	public String getOEM() {
		return OEM;
	}

	public void setOEM(String oEM) {
		OEM = oEM;
	}

}
