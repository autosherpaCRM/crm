package models;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Upload {
	
	public static String PROCESSING_STARTED="STARTED";
	public static String PROCESSING_COMPLETED="COMPLETE";
	public static String PROCESSING_UPLOADED ="UPLOADED";
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(length = 100)
	private String fileName;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date uploadedDateTime;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private WyzUser uploadedBy;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private UploadType uploadType;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date processingStartedDT;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date processingEndedDT;
	
	@Column(length =100)
	private String uploadPath;
	
	private boolean processed;
	
	private boolean processingStarted;
	
	private long numberProcessed;

	private long numberDiscarded;
	
	private boolean uploadError;
	
	private boolean processingError;
	
	@Column(length = 60)
	private String processingStatus;
	
	@Column(length = 100)
	private String discardedEntriesPath;
	
	private long size;
	
	@Column(length = 150)
	private String error;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "upload")
	private List<FormData> formDataList;

	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getUploadedDateTime() {
		return uploadedDateTime;
	}

	public void setUploadedDateTime(Date uploadedDateTime) {
		this.uploadedDateTime = uploadedDateTime;
	}

	public WyzUser getUploadedBy() {
		return uploadedBy;
	}

	public void setUploadedBy(WyzUser uploadedBy) {
		this.uploadedBy = uploadedBy;
	}

	public String getUploadPath() {
		return uploadPath;
	}

	public void setUploadPath(String uploadPath) {
		this.uploadPath = uploadPath;
	}

	public boolean isProcessed() {
		return processed;
	}

	public void setProcessed(boolean isProcessed) {
		this.processed = isProcessed;
	}

	public long getNumberDiscarded() {
		return numberDiscarded;
	}

	public void setNumberDiscarded(long numberDiscarded) {
		this.numberDiscarded = numberDiscarded;
	}

	public String getDiscardedEntriesPath() {
		return discardedEntriesPath;
	}

	public void setDiscardedEntriesPath(String discardedEntriesPath) {
		this.discardedEntriesPath = discardedEntriesPath;
	}

	public UploadType getUploadType() {
		return uploadType;
	}

	public void setUploadType(UploadType uploadType) {
		this.uploadType = uploadType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	@Override
	public boolean equals(Object obj) {
		return this.id == ((Upload) obj).id;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public boolean isUploadError() {
		return uploadError;
	}

	public void setUploadError(boolean uploadError) {
		this.uploadError = uploadError;
	}

	public boolean isProcessingError() {
		return processingError;
	}

	public void setProcessingError(boolean processingError) {
		this.processingError = processingError;
	}

	public boolean isProcessingStarted() {
		return processingStarted;
	}

	public void setProcessingStarted(boolean processingStarted) {
		this.processingStarted = processingStarted;
	}

	public String getProcessingStatus() {
		return processingStatus;
	}

	public void setProcessingStatus(String processingStatus) {
		this.processingStatus = processingStatus;
	}

	public long getNumberProcessed() {
		return numberProcessed;
	}

	public void setNumberProcessed(long numberProcessed) {
		this.numberProcessed = numberProcessed;
	}

	public Date getProcessingStartedDT() {
		return processingStartedDT;
	}

	public void setProcessingStartedDT(Date processingStartedDT) {
		this.processingStartedDT = processingStartedDT;
	}

	public Date getProcessingEndedDT() {
		return processingEndedDT;
	}

	public void setProcessingEndedDT(Date processingEndedDT) {
		this.processingEndedDT = processingEndedDT;
	}

	

	public List<FormData> getFormDataList() {
		return formDataList;
	}

	public void setFormDataList(List<FormData> formDataList) {
		this.formDataList = formDataList;
	}


	
	
}
