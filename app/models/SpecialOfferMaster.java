package models;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class SpecialOfferMaster {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;

	@Column(length = 50)
	public String offerCode;

	@Column(length = 300)
	public String offerDetails;

	@Column(length = 300)
	public String offerConditions;

	@Column(length = 100)
	public String offerBenefit;

	@Temporal(TemporalType.DATE)
	public Date offerValidity;

	public boolean isActive;
	
	@OneToMany(mappedBy = "specialOfferMaster")
	public List<ServiceBooked> servicesBooked;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getOfferCode() {
		return offerCode;
	}

	public void setOfferCode(String offerCode) {
		this.offerCode = offerCode;
	}

	public String getOfferDetails() {
		return offerDetails;
	}

	public void setOfferDetails(String offerDetails) {
		this.offerDetails = offerDetails;
	}

	public String getOfferConditions() {
		return offerConditions;
	}

	public void setOfferConditions(String offerConditions) {
		this.offerConditions = offerConditions;
	}

	public String getOfferBenefit() {
		return offerBenefit;
	}

	public void setOfferBenefit(String offerBenefit) {
		this.offerBenefit = offerBenefit;
	}

	public Date getOfferValidity() {
		return offerValidity;
	}

	public void setOfferValidity(Date offerValidity) {
		this.offerValidity = offerValidity;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public List<ServiceBooked> getServicesBooked() {
		return servicesBooked;
	}

	public void setServicesBooked(List<ServiceBooked> servicesBooked) {
		this.servicesBooked = servicesBooked;
	}
	
	

}
