package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class WorkBillRegisterError {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;
	
	public String billNo;
	public String mcpNo;	
	public String jobCardNo;
	
	public String billDateoldStr;
	public String beforematchingStr;
	public String billDateStr;
	public String partBasicStr;
	public String partDiscStr;
	public String partChargesStr;
	public String labourBasicStr;
	public String labourDiscStr;
	public String labourChargeStr;
	public String roundOffAmtStr;
	public String billAmtStr;
	public String yearStr;
	public String monthStr;
	
	public String servicetype;
	public String regno;
	public String custId;
	public String partyName;	
	public String location;	
	public String slNo;	
	public int rowNumber;
	public boolean isError;
	
	@Column(length = 100)
	public String upload_id;

	@Column(length = 700)
	public String errorInformation;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getBillNo() {
		return billNo;
	}

	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}

	public String getMcpNo() {
		return mcpNo;
	}

	public void setMcpNo(String mcpNo) {
		this.mcpNo = mcpNo;
	}
	
	public String getJobCardNo() {
		return jobCardNo;
	}

	public void setJobCardNo(String jobCardNo) {
		this.jobCardNo = jobCardNo;
	}	

	public String getServicetype() {
		return servicetype;
	}

	public void setServicetype(String servicetype) {
		this.servicetype = servicetype;
	}

	public String getRegno() {
		return regno;
	}

	public void setRegno(String regno) {
		this.regno = regno;
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public String getPartyName() {
		return partyName;
	}

	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}	

	public String getSlNo() {
		return slNo;
	}

	public void setSlNo(String slNo) {
		this.slNo = slNo;
	}

	public int getRowNumber() {
		return rowNumber;
	}

	public void setRowNumber(int rowNumber) {
		this.rowNumber = rowNumber;
	}

	public boolean isError() {
		return isError;
	}

	public void setError(boolean isError) {
		this.isError = isError;
	}

	public String getErrorInformation() {
		return errorInformation;
	}

	public void setErrorInformation(String errorInformation) {
		this.errorInformation = errorInformation;
	}

	public String getBillDateoldStr() {
		return billDateoldStr;
	}

	public void setBillDateoldStr(String billDateoldStr) {
		this.billDateoldStr = billDateoldStr;
	}

	public String getBeforematchingStr() {
		return beforematchingStr;
	}

	public void setBeforematchingStr(String beforematchingStr) {
		this.beforematchingStr = beforematchingStr;
	}

	public String getBillDateStr() {
		return billDateStr;
	}

	public void setBillDateStr(String billDateStr) {
		this.billDateStr = billDateStr;
	}

	public String getPartBasicStr() {
		return partBasicStr;
	}

	public void setPartBasicStr(String partBasicStr) {
		this.partBasicStr = partBasicStr;
	}

	public String getPartDiscStr() {
		return partDiscStr;
	}

	public void setPartDiscStr(String partDiscStr) {
		this.partDiscStr = partDiscStr;
	}

	public String getPartChargesStr() {
		return partChargesStr;
	}

	public void setPartChargesStr(String partChargesStr) {
		this.partChargesStr = partChargesStr;
	}

	public String getLabourBasicStr() {
		return labourBasicStr;
	}

	public void setLabourBasicStr(String labourBasicStr) {
		this.labourBasicStr = labourBasicStr;
	}

	public String getLabourDiscStr() {
		return labourDiscStr;
	}

	public void setLabourDiscStr(String labourDiscStr) {
		this.labourDiscStr = labourDiscStr;
	}

	public String getLabourChargeStr() {
		return labourChargeStr;
	}

	public void setLabourChargeStr(String labourChargeStr) {
		this.labourChargeStr = labourChargeStr;
	}

	public String getRoundOffAmtStr() {
		return roundOffAmtStr;
	}

	public void setRoundOffAmtStr(String roundOffAmtStr) {
		this.roundOffAmtStr = roundOffAmtStr;
	}

	public String getBillAmtStr() {
		return billAmtStr;
	}

	public void setBillAmtStr(String billAmtStr) {
		this.billAmtStr = billAmtStr;
	}

	public String getYearStr() {
		return yearStr;
	}

	public void setYearStr(String yearStr) {
		this.yearStr = yearStr;
	}

	public String getMonthStr() {
		return monthStr;
	}

	public void setMonthStr(String monthStr) {
		this.monthStr = monthStr;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getUpload_id() {
		return upload_id;
	}

	public void setUpload_id(String upload_id) {
		this.upload_id = upload_id;
	}
	
	
	

}
