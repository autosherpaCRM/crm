/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class ServiceAdvisor implements Comparable<ServiceAdvisor> {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long advisorId;
	@Column(length = 30)
	public String advisorName;
	@Column(length = 30)
	public String advisorNumber;

	public Boolean isActive;

	public int priority;

	public int capacityPerDay;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	public WyzUser wyzUser;

	@OneToMany(mappedBy = "serviceAdvisor")
	public List<ServiceBooked> servicebooked;

	@OneToMany(mappedBy = "serviceAdvisor")
	public List<Service> service;

	@ManyToOne(cascade = CascadeType.ALL)
	public Workshop workshop;

	public String upload_id;

	public String getUpload_id() {
		return upload_id;
	}

	public void setUpload_id(String upload_id) {
		this.upload_id = upload_id;
	}

	public Workshop getWorkshop() {
		return workshop;
	}

	public void setWorkshop(Workshop workshop) {
		this.workshop = workshop;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public long getAdvisorId() {
		return advisorId;
	}

	public void setAdvisorId(long advisorId) {
		this.advisorId = advisorId;
	}

	public String getAdvisorName() {
		return advisorName;
	}

	public void setAdvisorName(String AdvisorName) {
		this.advisorName = AdvisorName;
	}

	public String getAdvisorNumber() {
		return advisorNumber;
	}

	public void setAdvisorNumber(String AdvisorNumber) {
		this.advisorNumber = AdvisorNumber;
	}

	public List<ServiceBooked> getServicebooked() {
		return servicebooked;
	}

	public void setServicebooked(List<ServiceBooked> servicebooked) {
		this.servicebooked = servicebooked;
	}

	public List<Service> getService() {
		return service;
	}

	public void setService(List<Service> service) {
		this.service = service;
	}

	public WyzUser getWyzUser() {
		return wyzUser;
	}

	public void setWyzUser(WyzUser wyzUser) {
		this.wyzUser = wyzUser;
	}

	public int getCapacityPerDay() {
		return capacityPerDay;
	}

	public void setCapacityPerDay(int capacityPerDay) {
		this.capacityPerDay = capacityPerDay;
	}

	public int compareTo(ServiceAdvisor sa) {
		if (this.priority > sa.priority)
			return 1;
		if (this.priority == sa.priority)
			return 0;
		else
			return -1;
	}

}
