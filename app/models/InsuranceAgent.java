package models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
@Entity
public class InsuranceAgent implements Comparable<InsuranceAgent>{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long insuranceAgentId;
	
	@Column(length = 30)
	public String insuranceAgentName;
	@Column(length = 30)
	public String insuranceAgentNumber;

	public Boolean isActive;

	public int priority;

	public int capacityPerDay;
	
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	public WyzUser wyzUser;
	
	@OneToMany(mappedBy = "insuranceAgent")
	public List<AppointmentBooked> appointmentBooked;

	public long getInsuranceAgentId() {
		return insuranceAgentId;
	}

	public void setInsuranceAgentId(long insuranceAgentId) {
		this.insuranceAgentId = insuranceAgentId;
	}

	public String getInsuranceAgentName() {
		return insuranceAgentName;
	}

	public void setInsuranceAgentName(String insuranceAgentName) {
		this.insuranceAgentName = insuranceAgentName;
	}

	public String getInsuranceAgentNumber() {
		return insuranceAgentNumber;
	}

	public void setInsuranceAgentNumber(String insuranceAgentNumber) {
		this.insuranceAgentNumber = insuranceAgentNumber;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public int getCapacityPerDay() {
		return capacityPerDay;
	}

	public void setCapacityPerDay(int capacityPerDay) {
		this.capacityPerDay = capacityPerDay;
	}

	public WyzUser getWyzUser() {
		return wyzUser;
	}

	public void setWyzUser(WyzUser wyzUser) {
		this.wyzUser = wyzUser;
	}

	public List<AppointmentBooked> getAppointmentBooked() {
		return appointmentBooked;
	}

	public void setAppointmentBooked(List<AppointmentBooked> appointmentBooked) {
		this.appointmentBooked = appointmentBooked;
	}

	@Override
	public int compareTo(InsuranceAgent sa) {
		if (this.priority > sa.priority)
			return 1;
		if (this.priority == sa.priority)
			return 0;
		else
			return -1;
	}

	
	

}
