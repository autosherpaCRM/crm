/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author wct-09
 */
public class workshopBill {
       public long id;
       public String billNumber;
       public String mcpNo;
      @Temporal(TemporalType.DATE)
        public Date billDate;
      public String jobCardNumber;
      public String serviceType;
      public String vehicleRegNumber;
      public String customerId;
      public String customerName;
      public String partBasic;
      public String partDiscount;
      public String partCharges;
      public String labourBasic;
      public String labourDiscount;
      public String labourCharges;
      public String roundOffamt;
      public double billAmt;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public String getMcpNo() {
        return mcpNo;
    }

    public void setMcpNo(String mcpNo) {
        this.mcpNo = mcpNo;
    }

    public Date getBillDate() {
        return billDate;
    }

    public void setBillDate(Date billDate) {
        this.billDate = billDate;
    }

    public String getJobCardNumber() {
        return jobCardNumber;
    }

    public void setJobCardNumber(String jobCardNumber) {
        this.jobCardNumber = jobCardNumber;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getVehicleRegNumber() {
        return vehicleRegNumber;
    }

    public void setVehicleRegNumber(String vehicleRegNumber) {
        this.vehicleRegNumber = vehicleRegNumber;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getPartBasic() {
        return partBasic;
    }

    public void setPartBasic(String partBasic) {
        this.partBasic = partBasic;
    }

    public String getPartDiscount() {
        return partDiscount;
    }

    public void setPartDiscount(String partDiscount) {
        this.partDiscount = partDiscount;
    }

    public String getPartCharges() {
        return partCharges;
    }

    public void setPartCharges(String partCharges) {
        this.partCharges = partCharges;
    }

    public String getLabourBasic() {
        return labourBasic;
    }

    public void setLabourBasic(String labourBasic) {
        this.labourBasic = labourBasic;
    }

    public String getLabourDiscount() {
        return labourDiscount;
    }

    public void setLabourDiscount(String labourDiscount) {
        this.labourDiscount = labourDiscount;
    }

    public String getLabourCharges() {
        return labourCharges;
    }

    public void setLabourCharges(String labourCharges) {
        this.labourCharges = labourCharges;
    }

    public String getRoundOffamt() {
        return roundOffamt;
    }

    public void setRoundOffamt(String roundOffamt) {
        this.roundOffamt = roundOffamt;
    }

    public double getBillAmt() {
        return billAmt;
    }

    public void setBillAmt(double billAmt) {
        this.billAmt = billAmt;
    }

 

   
      
      
      
    
}
