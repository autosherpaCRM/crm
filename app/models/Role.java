package models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Role {

	public static String ROLE_SUPER_ADMIN = "SuperAdmin";
	public static String ROLE_CRE = "CRE";
	public static String ROLE_CRE_MANAGER = "CREManager";
	public static String ROLE_INSURANCE = "Insurance";
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(length = 50)
	private String role;

	@ManyToMany(cascade = CascadeType.ALL, mappedBy = "roles")
	private List<WyzUser> users;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public List<WyzUser> getUsers() {
		return users;
	}

	public void setUsers(List<WyzUser> users) {
		this.users = users;
	}

}
