package actors;

import java.io.File;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.metamodel.DataContextFactory;
import org.apache.metamodel.create.CreateTable;
import org.apache.metamodel.create.CreateTableColumnBuilder;
import org.apache.metamodel.data.DataSet;
import org.apache.metamodel.data.Row;
import org.apache.metamodel.excel.ExcelConfiguration;
import org.apache.metamodel.excel.ExcelDataContext;
import org.apache.metamodel.insert.InsertInto;
import org.apache.metamodel.jdbc.JdbcDataContext;
import org.apache.metamodel.query.OperatorType;
import org.apache.metamodel.query.Query;
import org.apache.metamodel.query.SelectItem;
import org.apache.metamodel.schema.Column;
import org.apache.metamodel.schema.ColumnType;
import org.apache.metamodel.schema.Schema;
import org.apache.metamodel.schema.Table;
import org.apache.metamodel.schema.naming.CustomColumnNamingStrategy;
import org.apache.metamodel.update.Update;
import org.apache.metamodel.util.DateUtils;

import models.ColumnDefinition;
import models.Upload;
import play.Logger.ALogger;

public class DataSourceHelper {

	public static ALogger logger = play.Logger.of(DataSourceHelper.class);

	public static ArrayList<String> getSingleColumnWhere(String tenantCode, String tableName, String selectColumn,
			String whereColumn, long id, JdbcDataContext context) {

		Schema schema = context.getSchemaByName(tenantCode);

		Table table = schema.getTableByName(tableName);
		Column select = table.getColumnByName(selectColumn);
		Column where = table.getColumnByName(whereColumn);
		Query query = new Query();

		query.from(table).select(select).where(where, OperatorType.EQUALS_TO, id);

		logger.debug("####About to excecute query ->" + query.toSql());

		DataSet ds = context.executeQuery(query);

		ArrayList<String> returnvalues = new ArrayList<String>();
		ds.forEach(row -> {
			SelectItem[] items = row.getSelectItems();
			for (SelectItem item : items) {
				Column col = item.getColumn();
				Object value = row.getValue(col);
				if (value.getClass() == Long.class) {
					Long lvalue = (Long) value;
					returnvalues.add(Long.toString(lvalue));
				} else if (value.getClass() == String.class) {
					String svalue = (String) value;
					returnvalues.add(svalue);
				}
			}
		});

		return returnvalues;
	}

	public static boolean updateSingleColumn(String tenantCode, String tableName, String updateColumn,
			Object updateValue, String whereColumn, long id, JdbcDataContext context) {
		Table table = context.getSchemaByName(tenantCode).getTableByName(tableName);
		Column updateCol = table.getColumnByName(updateColumn);
		Update update = new Update(table);
		update.value(updateCol, updateValue).where(whereColumn).eq(id);
		context.executeUpdate(update);
		return true;
	}

	public static boolean updateMultiColumns(String tenantCode, String tableName, HashMap<String, Object> updates,
			String whereColumn, long id, JdbcDataContext context) {

		Table table = context.getSchemaByName(tenantCode).getTableByName(tableName);
		Update update = new Update(table);
		update.where(whereColumn).eq(id);
		updates.forEach((column, value) -> {
			update.value(column, value);
		});

		context.executeUpdate(update);
		return true;
	}

	public static ArrayList<String> getSingleColumnWhere(String tenantCode, String tableName, String selectColumn,
			String whereColumn, String id, JdbcDataContext context) {

		Schema schema = context.getSchemaByName(tenantCode);

		Table table = schema.getTableByName(tableName);
		Column select = table.getColumnByName(selectColumn);
		Column where = table.getColumnByName(whereColumn);
		Query query = new Query();

		query.from(table).select(select).where(where, OperatorType.EQUALS_TO, id);

		logger.debug("####About to excecute query ->" + query.toSql());

		DataSet ds = context.executeQuery(query);

		ArrayList<String> returnvalues = new ArrayList<String>();
		ds.forEach(row -> {
			SelectItem[] items = row.getSelectItems();
			for (SelectItem item : items) {
				Column col = item.getColumn();
				Object value = row.getValue(col);
				if (value.getClass() == Long.class) {
					Long lvalue = (Long) value;
					returnvalues.add(Long.toString(lvalue));
				} else if (value.getClass() == String.class) {
					String svalue = (String) value;
					returnvalues.add(svalue);
				}
			}
		});

		return returnvalues;
	}

	public static boolean checkTableExists(String tenantCode, String tableName, JdbcDataContext context) {

		Schema schema = context.getSchemaByName(tenantCode);
		String[] tableNames = schema.getTableNames();

		return Arrays.stream(tableNames).anyMatch(s -> s.equalsIgnoreCase(tableName.toLowerCase()));
	}

	public static DataSet getAllColumnsFrom(String tenantCode, String tableName, String where, long value,
			JdbcDataContext context) {

		Query query = new Query();
		Schema schema = context.getSchemaByName(tenantCode);
		Table table = schema.getTableByName(tableName);
		Column wherecol = table.getColumnByName(where);

		query.from(table).where(wherecol, OperatorType.EQUALS_TO, value).selectAll();

		DataSet ds = context.executeQuery(query);
		return ds;
	}

	public static boolean createTable(String tenantCode, String tableName, JdbcDataContext context) {

		try {
			if (checkTableExists(tenantCode, tableName, context))
				return true;
			else {
				ArrayList<String> utypeIds = DataSourceHelper.getSingleColumnWhere(tenantCode, "uploadtype", "id",
						"uploadTypeName", tableName, context);

				String utypeid = utypeIds.get(0);
				logger.debug("Obtained upload type id for " + tableName + " utypeid:" + utypeid);

				ArrayList<String> colDefIds = DataSourceHelper.getSingleColumnWhere(tenantCode,
						"uploadtype_columndefinition", "definitions_id", "UploadType_id", Long.parseLong(utypeid),
						context);

				if (colDefIds == null) {
					logger.debug("Could not obtain the column definitions: null");
					return false;
				} else if (colDefIds.size() == 0) {
					logger.debug("Column definitions are not present for table:" + tableName);
					return false;
				} else {
					CreateTable createTable = new CreateTable(tenantCode, tableName);

					colDefIds.forEach(def -> {
						DataSet ds = DataSourceHelper.getAllColumnsFrom(tenantCode, "columndefinition", "id",
								Long.parseLong(def), context);
						ds.forEach(row -> {
							SelectItem[] items = row.getSelectItems();
							Column destColumn = null;
							Column columnDataType = null;
							Column columnSize = null;

							for (SelectItem item : items) {
								if (item.getColumn().getName().equals("destinationColumnName")) {
									destColumn = item.getColumn();
								} else if (item.getColumn().getName().equals("columnDataType")) {
									columnDataType = item.getColumn();
								} else if (item.getColumn().getName().equals("size")) {
									columnSize = item.getColumn();
								}
							}
							String strColName = (String) row.getValue(destColumn);
							String strColDType = (String) row.getValue(columnDataType);
							Integer intColSize = (Integer) row.getValue(columnSize);
							logger.debug("Adding column with Name:" + strColName + "DataType:" + strColDType
									+ "ColumnSize:" + intColSize);
							ColumnType type = null;
							boolean sizeflag = false;
							if (strColDType.equals(ColumnDefinition.DATA_TYPE_BIGINT)) {
								type = ColumnType.BIGINT;
							} else if (strColDType.equals(ColumnDefinition.DATA_TYPE_BOOLEAN)) {
								type = ColumnType.BOOLEAN;
							} else if (strColDType.equals(ColumnDefinition.DATA_TYPE_CHAR)) {
								type = ColumnType.CHAR;
							} else if (strColDType.equals(ColumnDefinition.DATA_TYPE_DATE)) {
								type = ColumnType.DATE;
							} else if (strColDType.equals(ColumnDefinition.DATA_TYPE_DOUBLE)) {
								type = ColumnType.DOUBLE;
							} else if (strColDType.equals(ColumnDefinition.DATA_TYPE_FLOAT)) {
								type = ColumnType.FLOAT;
							} else if (strColDType.equals(ColumnDefinition.DATA_TYPE_INTEGER)) {
								type = ColumnType.INTEGER;
							} else if (strColDType.equals(ColumnDefinition.DATA_TYPE_TIMESTAMP)) {
								type = ColumnType.TIMESTAMP;
							} else if (strColDType.equals(ColumnDefinition.DATA_TYPE_TINYINT)) {
								type = ColumnType.TINYINT;
							} else if (strColDType.equals(ColumnDefinition.DATA_TYPE_VARCHAR)) {
								type = ColumnType.VARCHAR;
								sizeflag = true;
							}

							CreateTableColumnBuilder builder = createTable.withColumn(strColName).ofType(type);

							if (sizeflag)
								builder.ofSize(intColSize);

						});

					});
					createTable.withColumn("uploadid").ofType(ColumnType.BIGINT);
					context.executeUpdate(createTable);
					return true;
				}

			}

		} catch (Exception e) {
			logger.debug("Could not create the table" + tableName);
			logger.error("Error in table creation", e);
			return false;
		}
	}

	public static boolean populateData(String tenantCode, long uploadId, String uploadTypeName, String rowsToIgnore,
			Boolean duplicateAllowed, Boolean ignoreSourceColumnNames,
			List<Map<String, String>> additionalFormData, JdbcDataContext context) {

		Schema schema = context.getSchemaByName(tenantCode);

		if (!checkTableExists(tenantCode, uploadTypeName, context)) {
			logger.debug("Table does not exist:" + uploadTypeName);
			return false;
		}

		DataSet ds = getAllColumnsFrom(tenantCode, "upload", "id", uploadId, context);
		ArrayList<String> excelSheetPaths = new ArrayList<String>();
		ds.forEach(row -> {
			SelectItem items[] = row.getSelectItems();
			Column excelPath = null;
			for (SelectItem item : items) {
				if (item.getColumn().getName().equals("uploadPath")) {
					excelPath = item.getColumn();
				}

			}
			String path = (String) row.getValue(excelPath);
			excelSheetPaths.add(path);
		});
		if (excelSheetPaths.size() == 0) {
			logger.debug("Could not get the excel sheet path from upload id" + uploadId);
			return false;
		}
		String excelSheetPath = excelSheetPaths.get(0);
		logger.debug("Obtained excel sheet path:" + excelSheetPath);

		List<String> columnList = getDefinedColumnList(tenantCode, uploadTypeName, ignoreSourceColumnNames, context);

		
		Set<Long> ignoreRows = new HashSet<Long>();
		int firstRowsToIgnore=0;
		int lastRowsToIgnore=0;
		if (rowsToIgnore != null) {
			
			String[] rows = rowsToIgnore.split(",");
			for (String number : rows) {
				if (number != null) {
					if(number.startsWith("first")){
						logger.debug("String first encountered..");
						int indx = number.indexOf(":");
						String firstRows = number.substring(indx+1);
						firstRowsToIgnore = Integer.parseInt(firstRows);
						logger.debug("First rows to Ignore: "+firstRowsToIgnore);
					}else if (number.startsWith("last")) {
						logger.debug("String last encountered..");
						int indx = number.indexOf(":");
						String lastRows = number.substring(indx+1);
						lastRowsToIgnore = Integer.parseInt(lastRows);
						logger.debug("Last rows to Ignore: "+lastRowsToIgnore);
						
					} else {
						ignoreRows.add(new Long(Long.parseLong(number)));
					}
				}
			}
		}

		if(firstRowsToIgnore ==0 ){
			firstRowsToIgnore=1;
		}
		
		ExcelConfiguration config = null;
		if (ignoreSourceColumnNames) {
			CustomColumnNamingStrategy strategy = new CustomColumnNamingStrategy(columnList);
			config = new ExcelConfiguration(firstRowsToIgnore, strategy, true, true);
		} else {
			config = new ExcelConfiguration(firstRowsToIgnore, true, true);
		}
		
		
		DataSet excelDataSet = getAllDataFromExcel(excelSheetPath, config);

		ExcelDataContext excelDataContext = createErrorDataFile(excelSheetPath, uploadTypeName, tenantCode, uploadId,
				context);

		boolean insertStatus = insertExcelDataIntoTable(excelDataSet, tenantCode, uploadTypeName, uploadId,
				rowsToIgnore, ignoreSourceColumnNames, context, excelDataContext);

		List<String> additionalColumnList = getAddlDefinedColumnList(tenantCode, uploadTypeName,
				ignoreSourceColumnNames, context);

		if (additionalColumnList.size() > 0) {
			updateAddlData(additionalFormData, tenantCode, uploadTypeName, uploadId, ignoreSourceColumnNames, context);
		}

		return insertStatus;
	}

	public static DataSet getAllDataFromExcel(String path, ExcelConfiguration config) {
		File file = new File(path);
		ExcelDataContext excelContext = (ExcelDataContext) DataContextFactory.createExcelDataContext(file, config);
		Schema schema = excelContext.getDefaultSchema();
		logger.debug("About to read excel sheet:" + schema.getName());
		logger.debug("About to read from worksheet:" + schema.getTables()[0].getName());
		Table table = schema.getTable(0);
		Query query = new Query();
		query.from(table).selectAll();
		DataSet ds = excelContext.executeQuery(query);
		return ds;
	}

	public static ExcelDataContext createErrorDataFile(String path, String utypeName, String tenantCode, long uploadId,
			JdbcDataContext context) {

		int indx = path.lastIndexOf("\\");

		String directoryPath = path.substring(0, indx + 1);
		directoryPath += "error";
		File directory = new File(directoryPath);
		if (!directory.exists())
			directory.mkdirs();

		directoryPath += "\\";
		String fileName = path.substring(indx + 1);
		String newFileName = directoryPath + fileName;
		File file = new File(newFileName);
		ExcelConfiguration config = new ExcelConfiguration(1, true, false);
		ExcelDataContext excelContext = (ExcelDataContext) DataContextFactory.createExcelDataContext(file, config);

		updateSingleColumn(tenantCode, "upload", "discardedEntriesPath", newFileName, "id", uploadId, context);
		return excelContext;

	}

	public static void createErrorFileColumns(String tenantCode, String uploadTypeName, Boolean ignoreSourceColumnNames,
			JdbcDataContext jdbcContext, ExcelDataContext excelDataContext) {
		HashMap<String, ArrayList<String>> mapping = getColumnMapping(tenantCode, uploadTypeName,
				ignoreSourceColumnNames, jdbcContext);
		Schema excelSchemaName = excelDataContext.getDefaultSchema();
		CreateTable createTable = new CreateTable(excelSchemaName, "Error");
		Set<String> columnNames = mapping.keySet();
		columnNames.forEach(columnName -> {
			createTable.withColumn(columnName).ofType(ColumnType.VARCHAR);
		});
		createTable.withColumn("Error").ofType(ColumnType.VARCHAR);
		excelDataContext.executeUpdate(createTable);
	}

	public static List<String> getDefinedColumnList(String tenantCode, String uploadTypeName,
			Boolean ignoreSourceColumnNames, JdbcDataContext context) {
		ArrayList<String> utypeIds = DataSourceHelper.getSingleColumnWhere(tenantCode, "uploadtype", "id",
				"uploadTypeName", uploadTypeName, context);

		String utypeid = utypeIds.get(0);
		logger.debug("Obtained upload type id for " + uploadTypeName + " utypeid:" + utypeid);

		ArrayList<String> colDefIds = DataSourceHelper.getSingleColumnWhere(tenantCode, "uploadtype_columndefinition",
				"definitions_id", "UploadType_id", Long.parseLong(utypeid), context);

		if (colDefIds == null) {
			logger.debug("Could not obtain the column definitions: null");
			return null;
		} else if (colDefIds.size() == 0) {
			logger.debug("Column definitions are not present for table:" + uploadTypeName);
			return null;
		} else {

			ArrayList<Long> colDefsIdsL = new ArrayList<Long>();

			colDefIds.forEach(def -> {
				Long id = new Long(Long.parseLong(def));
				colDefsIdsL.add(id);
			});
			Collections.sort(colDefsIdsL);

			List<String> columnList = new ArrayList<String>();

			colDefsIdsL.forEach(def -> {
				DataSet ds = DataSourceHelper.getAllColumnsFrom(tenantCode, "columndefinition", "id", def, context);
				ds.forEach(row -> {
					SelectItem[] items = row.getSelectItems();
					Column destColumn = null;
					Column sourceColumn = null;
					Column dataPattern = null;
					Column metaColumn = null;

					for (SelectItem item : items) {
						if (item.getColumn().getName().equals("destinationColumnName")) {
							destColumn = item.getColumn();
						} else if (item.getColumn().getName().equals("sourceColumnName")) {
							sourceColumn = item.getColumn();
						} else if (item.getColumn().getName().equals("dataPattern")) {
							dataPattern = item.getColumn();
						} else if (item.getColumn().getName().equals("metaColumnName")) {
							metaColumn = item.getColumn();
						}
					}
					String strdestColName = (String) row.getValue(destColumn);
					String strsrcColName = (String) row.getValue(sourceColumn);
					String strPattern = (String) row.getValue(dataPattern);
					String strMetaColumnName = (String) row.getValue(metaColumn);

					if (ignoreSourceColumnNames) {
						columnList.add(strMetaColumnName);
					} else {
						columnList.add(strsrcColName);
					}

				});

			});
			return columnList;
		}

	}

	public static List<String> getAddlDefinedColumnList(String tenantCode, String uploadTypeName,
			Boolean ignoreSourceColumnNames, JdbcDataContext context) {

		ArrayList<String> utypeIds = DataSourceHelper.getSingleColumnWhere(tenantCode, "uploadtype", "id",
				"uploadTypeName", uploadTypeName, context);

		String utypeid = utypeIds.get(0);
		logger.debug("Obtained upload type id for " + uploadTypeName + " utypeid:" + utypeid);

		ArrayList<String> colDefIds = DataSourceHelper.getSingleColumnWhere(tenantCode, "uploadtype_columndefinition",
				"definitions_id", "UploadType_id", Long.parseLong(utypeid), context);

		if (colDefIds == null) {
			logger.debug("Could not obtain the column definitions: null");
			return null;
		} else if (colDefIds.size() == 0) {
			logger.debug("Column definitions are not present for table:" + uploadTypeName);
			return null;
		} else {

			ArrayList<Long> colDefsIdsL = new ArrayList<Long>();

			colDefIds.forEach(def -> {
				Long id = new Long(Long.parseLong(def));
				colDefsIdsL.add(id);
			});
			Collections.sort(colDefsIdsL);

			List<String> columnList = new ArrayList<String>();

			colDefsIdsL.forEach(def -> {
				DataSet ds = DataSourceHelper.getAllColumnsFrom(tenantCode, "columndefinition", "id", def, context);
				ds.forEach(row -> {
					SelectItem[] items = row.getSelectItems();
					Column destColumn = null;
					Column sourceColumn = null;
					Column dataPattern = null;
					Column metaColumn = null;
					Column formParameters = null;

					for (SelectItem item : items) {
						if (item.getColumn().getName().equals("destinationColumnName")) {
							destColumn = item.getColumn();
						} else if (item.getColumn().getName().equals("sourceColumnName")) {
							sourceColumn = item.getColumn();
						} else if (item.getColumn().getName().equals("dataPattern")) {
							dataPattern = item.getColumn();
						} else if (item.getColumn().getName().equals("metaColumnName")) {
							metaColumn = item.getColumn();
						} else if (item.getColumn().getName().equals("additionalFormData")) {
							formParameters = item.getColumn();
						}
					}
					String strdestColName = (String) row.getValue(destColumn);
					String strsrcColName = (String) row.getValue(sourceColumn);
					String strPattern = (String) row.getValue(dataPattern);
					String strMetaColumnName = (String) row.getValue(metaColumn);
					Boolean blnFormParameters = (Boolean) row.getValue(formParameters);

					if (blnFormParameters) {
						if (ignoreSourceColumnNames) {
							columnList.add(strMetaColumnName);
						} else {
							columnList.add(strsrcColName);
						}
					}

				});

			});
			return columnList;
		}

	}

	public static HashMap<String, ArrayList<String>> getColumnMapping(String tenantCode, String uploadTypeName,
			Boolean ignoreSourceColumnNames, JdbcDataContext context) {

		ArrayList<String> utypeIds = DataSourceHelper.getSingleColumnWhere(tenantCode, "uploadtype", "id",
				"uploadTypeName", uploadTypeName, context);

		String utypeid = utypeIds.get(0);
		logger.debug("Obtained upload type id for " + uploadTypeName + " utypeid:" + utypeid);

		ArrayList<String> colDefIds = DataSourceHelper.getSingleColumnWhere(tenantCode, "uploadtype_columndefinition",
				"definitions_id", "UploadType_id", Long.parseLong(utypeid), context);

		if (colDefIds == null) {
			logger.debug("Could not obtain the column definitions: null");
			return null;
		} else if (colDefIds.size() == 0) {
			logger.debug("Column definitions are not present for table:" + uploadTypeName);
			return null;
		} else {

			HashMap<String, ArrayList<String>> columnMap = new HashMap<String, ArrayList<String>>();

			colDefIds.forEach(def -> {
				DataSet ds = DataSourceHelper.getAllColumnsFrom(tenantCode, "columndefinition", "id",
						Long.parseLong(def), context);
				ds.forEach(row -> {
					SelectItem[] items = row.getSelectItems();
					Column destColumn = null;
					Column sourceColumn = null;
					Column dataPattern = null;
					Column metaColumn = null;

					for (SelectItem item : items) {
						if (item.getColumn().getName().equals("destinationColumnName")) {
							destColumn = item.getColumn();
						} else if (item.getColumn().getName().equals("sourceColumnName")) {
							sourceColumn = item.getColumn();
						} else if (item.getColumn().getName().equals("dataPattern")) {
							dataPattern = item.getColumn();
						} else if (item.getColumn().getName().equals("metaColumnName")) {
							metaColumn = item.getColumn();
						}
					}
					String strdestColName = (String) row.getValue(destColumn);
					String strsrcColName = (String) row.getValue(sourceColumn);
					String strPattern = (String) row.getValue(dataPattern);
					String strMetaColumnName = (String) row.getValue(metaColumn);
					ArrayList<String> data = new ArrayList<String>();
					data.add(strdestColName);
					if (strPattern != null)
						data.add(strPattern);

					if (ignoreSourceColumnNames) {
						columnMap.put(strMetaColumnName, data);
					} else {
						columnMap.put(strsrcColName, data);
					}

				});

			});
			return columnMap;
		}
	}

	public static boolean insertExcelDataIntoTable(DataSet ds, String tenantCode, String tableName, long uploadId,
			String rowToIgnore, Boolean ignoreSourceColumnNames, JdbcDataContext context,
			ExcelDataContext errorContext) {

		final ArrayList<Long> results = new ArrayList<Long>();
		results.add(new Long(0));
		results.add(new Long(0));

		HashMap<String, ArrayList<String>> columnMap = getColumnMapping(tenantCode, tableName, ignoreSourceColumnNames,
				context);

		if (columnMap == null) {
			logger.debug("Could not obtain the column Mapping");
			return false;
		} else if (columnMap.isEmpty()) {
			logger.debug("Colum map returned is empty");
			return false;
		} else {
			Table table = context.getSchemaByName(tenantCode).getTableByName(tableName);
			InsertInto insertInto = new InsertInto(table);
			logger.debug("#########Start of processing");
			boolean lastrowignore = false;
			Set<Long> ignoreRows = new HashSet<Long>();
			int firstRowsToIgnore=0;
			int lastRowsToIgnore=0;
			if (rowToIgnore != null) {
				
				String[] rows = rowToIgnore.split(",");
				for (String number : rows) {
					if (number != null) {
						if(number.startsWith("first")){
							logger.debug("String first encountered..");
							int indx = number.indexOf(":");
							String firstRows = number.substring(indx+1);
							firstRowsToIgnore = Integer.parseInt(firstRows);
							logger.debug("First rows to Ignore: "+firstRowsToIgnore);
						}else if (number.startsWith("last")) {
							logger.debug("String last encountered..");
							int indx = number.indexOf(":");
							String lastRows = number.substring(indx+1);
							lastRowsToIgnore = Integer.parseInt(lastRows);
							logger.debug("Last rows to Ignore: "+lastRowsToIgnore);
							
						} else {
							ignoreRows.add(new Long(Long.parseLong(number)));
						}
					}
				}
			}
			
			
			
			long rowcount = 0;

			logger.debug("Rows to ignore: " + rowToIgnore);
			logger.debug("Ignore rows array length: "+ignoreRows.size());

			List<Row> rowList = ds.toRows();
			logger.debug("Length of the original excel sheet:"+rowList.size());
			int size = rowList.size();
			if(firstRowsToIgnore>0 && lastRowsToIgnore>0){
				int toIndex = size-lastRowsToIgnore;
				
				rowList = rowList.subList(firstRowsToIgnore, toIndex);
				logger.debug("Sublist Size: "+rowList.size());
			}else if(firstRowsToIgnore>0){
				for(int i=0;i<firstRowsToIgnore;i++){
					ignoreRows.add(new Long(i));
				}
			}else if(lastRowsToIgnore>0){
				int toIndex = size-lastRowsToIgnore;
				
				rowList = rowList.subList(firstRowsToIgnore, toIndex);
				logger.debug("Sublist Size: "+rowList.size());
			}
			
			Iterator<Row> iterator = rowList.iterator();
			
			while (iterator.hasNext()) {
				rowcount++;

				Row row = null;
				try {
					row = iterator.next();
					if (!iterator.hasNext()) {
						if (lastrowignore)
							break;
					}
					if (!ignoreRows.contains(rowcount)) {
						SelectItem[] items = row.getSelectItems();
						for (SelectItem item : items) {
							Column col = item.getColumn();
							logger.debug("Getting data in excel: column: "+col.getName());
							ArrayList<String> data = columnMap.get(col.getName());
							Object value = row.getValue(col);
							logger.debug("Getting value for column in excel: column: "+value);
							String destColName = data.get(0);
							Column destCol = table.getColumnByName(destColName);
							String strVal = null;
							if (value == null) {
								strVal = "";
							} else {
								strVal = (String) value;
							}
							strVal = strVal.trim();
							if (destCol.getType() == ColumnType.DATE || destCol.getType() == ColumnType.TIMESTAMP
									|| destCol.getType() == ColumnType.TIME) {
								if (value != null && data.size() > 1) {
									String strPattern = data.get(1);
									DateFormat format = DateUtils.createDateFormat(strPattern);

									try {
										Date date = format.parse(strVal);
										value = date;
									} catch (Exception e) {
										logger.debug("Some exception occured while parsing the date");
										e.printStackTrace();
									}
								}
							} else if (destCol.getType() == ColumnType.BIGINT || destCol.getType() == ColumnType.DECIMAL
									|| destCol.getType() == ColumnType.DOUBLE) {
								if (value != null && data.size() > 1) {

									String strPattern = data.get(1);
									DecimalFormat formatter = new DecimalFormat(strPattern);
									// formatter.setGroupingUsed(true);
									Number numVal = formatter.parse(strVal);
									value = numVal;
								}
							} else {
								value = strVal;
							}

							insertInto.value(destCol, value);
						}
						Column upCol = table.getColumnByName("uploadid");
						insertInto.value(upCol, uploadId);
						context.executeUpdate(insertInto);

						Long scnt = results.get(0);
						long cnt = scnt.longValue();
						cnt++;
						Long nscnt = new Long(cnt);
						results.set(0, nscnt);
						//logger.debug("Inserted success --->"+cnt);
					}
				} catch (Exception e) {
					try {

						logger.debug("###########Failure during processing of the data");
						logger.debug(e.getMessage());

						String errorMessage = e.getMessage();
						int tablecount = errorContext.getDefaultSchema().getTableCount();
						if (tablecount == 0) {
							logger.debug("No tables found in error file creating the table..");
							createErrorFileColumns(tenantCode, tableName, ignoreSourceColumnNames, context,
									errorContext);
						}
						int colCount = errorContext.getDefaultSchema().getTable(0).getColumnCount();
						// logger.debug("Column Count in error file:" +
						// colCount);
						Table errorTable = errorContext.getDefaultSchema().getTable(0);
						InsertInto errorInserts = new InsertInto(errorTable);

						SelectItem[] items = row.getSelectItems();
						for (SelectItem item : items) {
							Column col = item.getColumn();
							String name = col.getName();
							Column errCol = errorTable.getColumnByName(name);
							Object val = row.getValue(col);

							errorInserts.value(errCol, val);
						}
						Column errCol = errorTable.getColumnByName("Error");
						errorInserts.value(errCol, errorMessage);
						errorContext.executeUpdate(errorInserts);
						Long ecnt = results.get(1);
						long cnt = ecnt.longValue();
						cnt++;
						Long necnt = new Long(cnt);
						results.set(1, necnt);
						// logger.debug("Inserted Failure --->"+cnt);
					} catch (Exception ex) {
						logger.debug("################Failure during Error file insertions!!!!!!");
						logger.debug(ex.getMessage());
						ex.printStackTrace();
					}

				}
			}

			Long fscntL = results.get(0);
			Long fecntL = results.get(1);
			GregorianCalendar cal = new GregorianCalendar();
			Date date = cal.getTime();
			HashMap<String, Object> updates = new HashMap<String, Object>();
			updates.put("numberDiscarded", fecntL.longValue());
			updates.put("numberProcessed", fscntL.longValue());
			updates.put("processingStatus", Upload.PROCESSING_COMPLETED);
			updates.put("processed", true);
			updates.put("processingEndedDT", date);
			if (fecntL > 0) {
				updates.put("processingError", true);
			} else {
				updates.put("processingError", false);
			}
			updateMultiColumns(tenantCode, "upload", updates, "id", uploadId, context);

			logger.debug("#########End of processing");

			return true;
		}
	}

	public static boolean updateAddlData(List<Map<String, String>> data, String tenantCode, String tableName,
			long uploadId, Boolean ignoreSourceColumnNames, JdbcDataContext context) {

		Table table = context.getSchemaByName(tenantCode).getTableByName(tableName);

		Update update = new Update(table);

		HashMap<String, ArrayList<String>> columnMap = getColumnMapping(tenantCode, tableName, ignoreSourceColumnNames,
				context);

		data.forEach(updatable -> {
			Set<String> keyset = updatable.keySet();
			keyset.forEach(key -> {
				String strVal = updatable.get(key);
				Object value = new Object();
				ArrayList<String> mapdata = columnMap.get(key);

				String destColName = mapdata.get(0);
				Column destCol = table.getColumnByName(destColName);

				if (destCol.getType() == ColumnType.DATE || destCol.getType() == ColumnType.TIMESTAMP
						|| destCol.getType() == ColumnType.TIME) {

					if (value != null && data.size() > 1) {
						String strPattern = mapdata.get(1);
						DateFormat format = DateUtils.createDateFormat(strPattern);

						try {
							Date date = format.parse(strVal);
							value = date;
						} catch (Exception e) {
							logger.debug("Some exception occured while parsing the date");
							e.printStackTrace();
						}
					}
				} else if (destCol.getType() == ColumnType.BIGINT || destCol.getType() == ColumnType.DECIMAL
						|| destCol.getType() == ColumnType.DOUBLE) {
					if (value != null && data.size() > 1) {

						String strPattern = mapdata.get(1);
						DecimalFormat formatter = new DecimalFormat(strPattern);
						// formatter.setGroupingUsed(true);
						Number numVal = null;
						try {
							numVal = formatter.parse(strVal);
						} catch (Exception e) {
							logger.debug("Some exception occured while parsing the number");
							e.printStackTrace();
						}
						value = numVal;
					}
				} else {
					value = strVal;
				}
				update.where("uploadid").eq(uploadId).value(destCol, value);
			});
		});

		context.executeUpdate(update);
		
		return false;
	}
}
