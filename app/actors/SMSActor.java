package actors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import akka.actor.Props;
import akka.actor.UntypedActor;
import play.Logger.ALogger;
import repositories.SMSTriggerRespository;

public class SMSActor extends UntypedActor{
	
	ALogger logger = play.Logger.of(SMSActor.class);
	
	public static Props props = Props.create(SMSActor.class);

	@Override
	public void onReceive(Object protocol) throws Exception {
		// TODO Auto-generated method stub
		
		SMSActorProtocol.startSMSActor smsprotocol=(SMSActorProtocol.startSMSActor) protocol;
		
		SMSTriggerRespository smsRepo=smsprotocol.repository;
		
		List<Long> vehList=smsprotocol.vehicleId;
		String smsTemplete=smsprotocol.smsTemplete;		
		long userId=smsprotocol.userId;
		String tenentCode=smsprotocol.tenentCode;
		String type=smsprotocol.smsType;
		
		smsRepo.sendBulkSMSAtOnce(vehList,smsTemplete,userId,tenentCode,type);		
		
		
	}

}
