package actors;

import akka.actor.UntypedActor;
import play.Logger;
import repositories.ExcelUploadRepo;

public class InsuranceDataInsert extends UntypedActor{

	
	Logger.ALogger logger = play.Logger.of("application");

	@Override
	public void onReceive(Object protocol) throws Exception {
		InsuranceDataInsertProtocol.StartSync fireproto = (InsuranceDataInsertProtocol.StartSync) protocol;

		logger.info("Recieved the message to start the history sync task");
		
		String uploadIdForInsert = fireproto.uploadId;
		ExcelUploadRepo repo = fireproto.excelUploadRepo;
		
		repo.insertDataByStoreProcedure(uploadIdForInsert);

		
	}
}
