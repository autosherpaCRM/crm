package actors;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.metamodel.schema.Column;
import org.apache.metamodel.schema.Schema;
import org.apache.metamodel.schema.Table;
import org.apache.metamodel.data.DataSet;
import org.apache.metamodel.jdbc.JdbcDataContext;
import org.apache.metamodel.query.OperatorType;
import org.apache.metamodel.query.Query;
import org.apache.metamodel.query.SelectItem;
import org.hsqldb.jdbc.jdbcConnection;

import akka.actor.Props;
import akka.actor.UntypedActor;
import models.Upload;
import play.Logger.ALogger;
import play.db.DBApi;

public class ProcessUploadActor extends UntypedActor {

	ALogger logger = play.Logger.of(ProcessUploadActor.class);

	public static Props props = Props.create(ProcessUploadActor.class);

	@Override
	public void onReceive(Object protocol) throws Exception {
		logger.debug("##########Received Messaage to start the process##############");

		ProcessUploadProtocol.BeginProcessing bpProtocol = (ProcessUploadProtocol.BeginProcessing) protocol;

		DBApi dbapi = bpProtocol.dbapi;
		String tenantCode = bpProtocol.tenantCode;
		List<Map<String, String>> formData = bpProtocol.formData;
		long uploadId = bpProtocol.uploadId;
		logger.debug("Obtained tenantCode:" + tenantCode);
		logger.debug("Obtained upload Id:" + uploadId);

		DataSource dataSource = dbapi.getDatabase("spring").getDataSource();

		JdbcDataContext jdbcDataContext = new JdbcDataContext(dataSource);

		ArrayList<String> utypidsColl = DataSourceHelper.getSingleColumnWhere(tenantCode, "upload", "uploadType_id",
				"id", uploadId, jdbcDataContext);

		String utypeid = utypidsColl.get(0);

		logger.debug("Obtained uplaod type id:" + utypeid);

		GregorianCalendar cal = new GregorianCalendar();
		Date date = cal.getTime();

		HashMap<String, Object> updates = new HashMap<String, Object>();
		updates.put("processingStartedDT", date);
		updates.put("processingStatus", Upload.PROCESSING_STARTED);
		updates.put("processingStarted", true);
		DataSourceHelper.updateMultiColumns(tenantCode, "upload", updates, "id", uploadId, jdbcDataContext);

		/*
		 * ArrayList<String> utypesColl =
		 * DataSourceHelper.getSingleColumnWhere(tenantCode, "uploadType",
		 * "uploadTypeName", "id", Long.parseLong(utypeid), jdbcDataContext);
		 * 
		 * String utype = utypesColl.get(0);
		 */
		DataSet ds = DataSourceHelper.getAllColumnsFrom(tenantCode, "uploadType", "id", Long.parseLong(utypeid),
				jdbcDataContext);

		HashMap<String, Object> values = new HashMap<String, Object>();
		ds.forEach(row -> {
			SelectItem[] items = row.getSelectItems();
			for (SelectItem item : items) {
				Column col = item.getColumn();
				Object value = row.getValue(col);
				values.put(col.getName(), value);
			}
		});
		String utype = (String) values.get("uploadTypeName");
		String rowsToignore = (String) values.get("rowsToignore");
		Boolean duplicateAllowed = (Boolean) values.get("duplicateAllowed");
		Boolean ignoreSourceColumnNames = (Boolean) values.get("ignoreSourceColumnNames");

		logger.debug("Obtained Upload Type for processing:" + utype);
		logger.debug("Obtained rows to ignore:" + rowsToignore);
		logger.debug("Obtained duplicate allowed:" + duplicateAllowed.booleanValue());
		// Check if the table already exists

		boolean creationstatus = DataSourceHelper.createTable(tenantCode, utype, jdbcDataContext);

		if (creationstatus) {
			DataSourceHelper.populateData(tenantCode, uploadId, utype, rowsToignore, duplicateAllowed,
					ignoreSourceColumnNames, formData, jdbcDataContext);
		}

	}

}
