package actors;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import actors.model.ServiceAdvisorHistoryInfo;
import akka.actor.UntypedActor;

import models.AssignedInteraction;
import models.Dealer;
import models.Segment;
import models.Service;
import models.PSFAssignedInteraction;
import play.Logger;
import repositories.FirebaseSyncRepository;

public class PSFMobileCallSync extends UntypedActor{
	
	Logger.ALogger logger = play.Logger.of("application");

	@Override
	public void onReceive(Object protocol) throws Exception {
		PSFMobileCallSyncProtocol.StartSync fireproto = (PSFMobileCallSyncProtocol.StartSync) protocol;
		FirebaseSyncRepository repo = fireproto.firebaseRepo;
		
				
				String firebaseUrl = fireproto.urlToSync;
				
				RestTemplate restTemplate = new RestTemplate();	
				String dealerkeysUrl = firebaseUrl
						+ ".json?auth=vki9nCdP8QQpaWX0NG7iDrxznBlrmzo56enmA34L&shallow=true";

				ResponseEntity<Map> result = restTemplate.getForEntity(dealerkeysUrl,
						Map.class);

				Map dealerMap = result.getBody();

				if (dealerMap != null) {

					Set dealerkeys = dealerMap.keySet();

					for (Object key : dealerkeys) {
						String dealer = (String) key;
				
				String userkeysUrl = firebaseUrl + dealer;
				userkeysUrl += "/ServiceAgent";
				
				logger.info("userkeysUrl url:" + userkeysUrl);

				DatabaseReference ref;

				if (repo != null) {
					
					
					List<PSFAssignedInteraction> listOfService=repo.getAllUnsynchedPSFMobileCalls(dealer);
					
					logger.info("listOfService : "+listOfService.size());
					for(PSFAssignedInteraction assData:listOfService){
						
						logger.info("");
						String userName = assData
								.getWyzUser().getUserName();

						String fireUrl = userkeysUrl + "/";
						fireUrl += userName;
						fireUrl += "/";
						fireUrl += "PSFCalls";
						fireUrl += "/";
						fireUrl += "CallInfo";
						fireUrl += "/";

						logger.info("fireUrl is :" + fireUrl);
						ref = FirebaseDatabase.getInstance()
								.getReferenceFromUrl(fireUrl);

						DatabaseReference newref = ref.push();

						String referencekey = newref.getKey();
						logger.info("referencekey : " + referencekey);
						
						Segment segment=repo.getSegmentByCustomerId(assData.getCustomer().getId());
						
						ServiceAdvisorHistoryInfo service_advisor=new ServiceAdvisorHistoryInfo();
						
						if (assData.getId() != 0) {
							service_advisor.setAssignedId(String.valueOf(assData.getId()));
						}
						
						if (assData.getVehicle().getVehicle_id() != 0) {
								service_advisor.setVehicleId(String.valueOf(assData.getVehicle().getVehicle_id()));
						}	
					
						if (assData.getCustomer().getId() != 0) {
							service_advisor.setCustId(String.valueOf(assData.getCustomer().getId()));
						}	
						
						if (assData.getWyzUser() != null) {
							service_advisor.setUserId(String.valueOf(assData.getWyzUser().getId()));
						}
						
						if (assData.getCustomer()
								.getCustomerName() != null) {
							service_advisor.setCustomerName(assData.getCustomer().getCustomerName());
						}
						if (assData.getCustomer().getPreferredPhone()!= null) {
							
							if (assData.getCustomer().getPreferredPhone().getPhoneNumber()!= null) {
								service_advisor.setCustomerPhone(assData.getCustomer().getPreferredPhone().getPhoneNumber());
							}
							
							
						}
						if (assData.getCustomer()
								.getPreferredAdress() != null) {
							service_advisor.setCustomerAddress(assData.getCustomer().getPreferredAdress().getConcatenatedAdress());
						}
							
							
						if (segment.getName() != null) {
							service_advisor.setCustomerCategory(segment.getName());
						}
						
						if (segment.getType() != null) {
							service_advisor.setLoyaltyType(segment.getType());
						}
						
						

						if (assData.getCustomer().getDob() != null) {
							service_advisor.setDob(assData.getCustomer().getDob());
						}
						
						if (assData.getCustomer()
								.getAnniversary_date() != null) {
							service_advisor.setAnniversary_date(assData.getCustomer().getAnniversary_date());
						}
						
						if (assData.getWyzUser().getDealerCode() != null) {
							service_advisor.setDealerCode(assData.getWyzUser().getDealerCode());
						}
						
						if (assData.getVehicle().getVehicleRegNo() != null) {

							service_advisor.setVehicalRegNo(assData.getVehicle().getVehicleRegNo());
						}
						
						if (assData.getVehicle().getVehicleNumber() != null) {
							service_advisor.setVehicleNumber(assData.getVehicle().getVehicleNumber());
						}
						
						if (assData.getVehicle().getModel() != null) {
							service_advisor.setModel(assData.getVehicle().getModel());
						}

						if (assData.getVehicle().getVariant() != null) {
							service_advisor.setVariant(assData.getVehicle().getVariant());
						}
						
						if (assData.getVehicle().getFuelType() != null) {
							service_advisor.setFuelType(assData.getVehicle().getFuelType());
						}


						if (assData.getVehicle().getLastServiceDate() != null) {

							service_advisor.setLastServiceDate(assData.getVehicle().getLastServiceDate().toString());
						}
						
						if (assData.getVehicle().getLastServiceType()!= null) {

							service_advisor.setLastServiceType(assData.getVehicle().getLastServiceType());
						}
						
						if (assData.getVehicle().getNextServicedate() != null) {

							service_advisor.setNextServiceDate(assData.getVehicle().getNextServicedate().toString());				
						}

												
						if (assData.getVehicle().getSaleDate() != null) {
							service_advisor.setSaleDate( assData.getVehicle().getSaleDate().toString());
							
						}
						
						if (assData.getVehicle().getForecastLogic() != null) {
							service_advisor.setForecastLogic(assData.getVehicle().getForecastLogic());
						}
						
						if (assData.getVehicle().getChassisNo() != null) {
							service_advisor.setChassisNo(assData.getVehicle().getChassisNo());
						}
						
						
						if (assData.getVehicle().getLastServiceType()!= null) {
							
							service_advisor.setServiceType(assData.getVehicle().getLastServiceType());
						}
						
						if (assData.getVehicle().getJobCardNumber()!= null) {
							
							service_advisor.setJobCardNumber(assData.getVehicle().getJobCardNumber());
						}
						
						if (assData.getVehicle().getLastServiceDate()!= null) {
							
							service_advisor.setBillDate(assData.getVehicle().getLastServiceDate().toString());
						}
						if (assData.getVehicle().getCity()!= null) {
							
							service_advisor.setWorkshop(assData.getVehicle().getCity());
						}
						service_advisor.setStatus("Not yet Called");
						service_advisor.setCallCount("0");
						service_advisor.setMakeCallFrom("PSF");
						newref.setValue(service_advisor);
						repo.updateSynchedToFirebasePSFCall(assData.getId(),referencekey);
						
					}
					

				}
			}
		
	}
	}

}
