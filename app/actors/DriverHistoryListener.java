package actors;

import play.Logger;
import repositories.FirebaseSyncRepository;
import actors.model.DriverPickupInfo;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;

public class DriverHistoryListener implements ChildEventListener {
	
	Logger.ALogger logger = play.Logger.of("application");
	
	private FirebaseSyncRepository firebaseSyncRepository;
	
	public DriverHistoryListener(FirebaseSyncRepository repository){
		firebaseSyncRepository = repository;
	}

	@Override
	public void onChildAdded(DataSnapshot dataSnapshot,
			String prevChildKey) {
		
		String firebaseKey = dataSnapshot.getKey();
		logger.info("Received onChildAdded Message for key: "+firebaseKey);
		
		DriverPickupInfo info =  dataSnapshot.getValue(DriverPickupInfo.class);
		dataSnapshot.getRef().removeValue();
		
		logger.info("Received Object: "+info.customerName);
		firebaseSyncRepository.addDriverInteraction(firebaseKey, info);
		
	}

	@Override
	public void onCancelled(DatabaseError arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onChildChanged(DataSnapshot arg0, String arg1) {
		logger.info("Received onChildChanged Message");
		logger.info("String value: "+arg1);

	}

	@Override
	public void onChildMoved(DataSnapshot arg0, String arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onChildRemoved(DataSnapshot arg0) {
		logger.info("Received onChildRemoved Message");

	}

}
