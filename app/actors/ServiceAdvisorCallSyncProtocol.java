/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package actors;

import repositories.FirebaseSyncRepository;

/**
 *
 * @author W-885
 */
public class ServiceAdvisorCallSyncProtocol {
    public static class StartSync{
    
    public final String urlToSync;
    public final FirebaseSyncRepository firebaseRepo;
    public StartSync(String url, FirebaseSyncRepository firebaseRepository){
    
    this.urlToSync = url;
    this.firebaseRepo=firebaseRepository;
    
    }
    
    }
}
