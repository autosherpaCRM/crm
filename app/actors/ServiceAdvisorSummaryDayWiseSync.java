package actors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.*;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import actors.model.ServiceAdvisorHistoryInfo;
import akka.actor.UntypedActor;
import models.ServiceAdvisor;
import play.Logger;
import repositories.FirebaseSyncRepository;

public class ServiceAdvisorSummaryDayWiseSync extends UntypedActor {

	Logger.ALogger logger = play.Logger.of("application");

	@Override
	public void onReceive(Object protocol) throws Exception {
		ServiceAdvisorHistorySyncProtocol.StartSync fireproto = (ServiceAdvisorHistorySyncProtocol.StartSync) protocol;

		logger.info("Recieved the message to start the summary details  sync task");

		String firebaseUrl = fireproto.urlToSync;
		FirebaseSyncRepository repo = fireproto.firebaseSyncRepo;

		RestTemplate restTemplate = new RestTemplate();
		String dealerkeysUrl = firebaseUrl + ".json?auth=vki9nCdP8QQpaWX0NG7iDrxznBlrmzo56enmA34L&shallow=true";

		ResponseEntity<Map> result = restTemplate.getForEntity(dealerkeysUrl, Map.class);

		Map dealerMap = result.getBody();

		if (dealerMap != null) {

			Set dealerkeys = dealerMap.keySet();

			for (Object key : dealerkeys) {
				String dealer = (String) key;
		
				//String dealer = "MAHINDRATEST";

				String currentDateForSummary = repo.getCurrentDateInStringFormate();
				List<ServiceAdvisor> serviceAdvisors = repo.getAllActiveServiceAdvisors(dealer);

				logger.info("count of serviceAdvisors for summary: " + serviceAdvisors.size());

				for (ServiceAdvisor advisor : serviceAdvisors) {

					String user = advisor.getWyzUser().getUserName();
					logger.info("username of service advisor is :" + user);

					String fireUrl1 = fireproto.urlToSync;
					fireUrl1 += dealer;
					fireUrl1 += "/";
					fireUrl1 += "ServiceAgent/";
					fireUrl1 += user;
					fireUrl1 += "/";
					fireUrl1 += "Summary/CallInfo/";

					DatabaseReference ref1 = FirebaseDatabase.getInstance().getReferenceFromUrl(fireUrl1);
					ref1.removeValue();

					Map<String, Long> countOfServiceBookedToday = repo
							.getCountOfServicesBookedtoday(advisor.getAdvisorId());

					for (Map.Entry<String, Long> entry : countOfServiceBookedToday.entrySet()) {
						logger.info("Key : " + entry.getKey() + " Value : " + entry.getValue());

						String fireUrl2 = fireUrl1;
						fireUrl2 += entry.getKey();

						Map<String, Object> addReportCounts = new HashMap<String, Object>();

						addReportCounts.put("booked", String.valueOf(entry.getValue()));
						addReportCounts.put("noShow", String.valueOf(0));
						addReportCounts.put("received", String.valueOf(0));

						DatabaseReference ref2 = FirebaseDatabase.getInstance().getReferenceFromUrl(fireUrl2);
						ref2.setValue(addReportCounts);
					}

				}
			}
		}
	}

}
