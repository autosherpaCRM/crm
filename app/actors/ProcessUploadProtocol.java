package actors;

import java.util.List;
import java.util.Map;

import play.db.DBApi;

public class ProcessUploadProtocol {

	public static class BeginProcessing {

		public final DBApi dbapi;
		public final String tenantCode;
		public final long uploadId;
		
		public final List<Map<String,String>> formData;

		public BeginProcessing(DBApi api, String tCode, long uId,List<Map<String,String>> frmData) {
			this.dbapi = api;
			this.tenantCode = tCode;
			this.uploadId = uId;
			this.formData = frmData;
		}

	}

}
