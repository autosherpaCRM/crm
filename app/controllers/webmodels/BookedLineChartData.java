package controllers.webmodels;

import java.util.List;

public class BookedLineChartData {
	
	public List callList;
	public List bookedList;
	public List<String> callCountList;
	public List<String> serviceBookedCountList;
	public List<String> pendingCountList; 
	
	public List getCallList() {
		return callList;
	}
	public void setCallList(List callList) {
		this.callList = callList;
	}
	public List getBookedList() {
		return bookedList;
	}
	public void setBookedList(List bookedList) {
		this.bookedList = bookedList;
	}
	public List<String> getCallCountList() {
		return callCountList;
	}
	public void setCallCountList(List<String> callCountList) {
		this.callCountList = callCountList;
	}
	public List<String> getServiceBookedCountList() {
		return serviceBookedCountList;
	}
	public void setServiceBookedCountList(List<String> serviceBookedCountList) {
		this.serviceBookedCountList = serviceBookedCountList;
	}
	public List<String> getPendingCountList() {
		return pendingCountList;
	}
	public void setPendingCountList(List<String> pendingCountList) {
		this.pendingCountList = pendingCountList;
	}
	
	
	

}
