/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.webmodels;

/**
 *
 * @author W-885
 */
public class FollowUpNotificationModel {
    
    public String followUpTime;
    public String dealerCode;
    public long callInteraction_id;
    public String customerName;
    public long customer_id;
    public long vehicle_vehicle_id;
    
	public FollowUpNotificationModel(String followUpTime, long callInteraction_id,
			String customerName, long customer_id, long vehicle_vehicle_id) {
		super();
		this.followUpTime = followUpTime;		
		this.callInteraction_id = callInteraction_id;
		this.customerName = customerName;
		this.customer_id = customer_id;
		this.vehicle_vehicle_id = vehicle_vehicle_id;
	}
	
	public FollowUpNotificationModel() {
		super();
		this.followUpTime = "";		
		this.callInteraction_id = (long)0;
		this.customerName = "";
		this.customer_id = (long)0;
		this.vehicle_vehicle_id = (long)0;
	}

	public String getFollowUpTime() {
		return followUpTime;
	}

	public void setFollowUpTime(String followUpTime) {
		this.followUpTime = followUpTime;
	}

	public String getDealerCode() {
		return dealerCode;
	}

	public void setDealerCode(String dealerCode) {
		this.dealerCode = dealerCode;
	}

	public long getCallInteraction_id() {
		return callInteraction_id;
	}

	public void setCallInteraction_id(long callInteraction_id) {
		this.callInteraction_id = callInteraction_id;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public long getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(long customer_id) {
		this.customer_id = customer_id;
	}

	public long getVehicle_vehicle_id() {
		return vehicle_vehicle_id;
	}

	public void setVehicle_vehicle_id(long vehicle_vehicle_id) {
		this.vehicle_vehicle_id = vehicle_vehicle_id;
	}

    
    
    
}
