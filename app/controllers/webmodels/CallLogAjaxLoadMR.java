package controllers.webmodels;

import java.util.Date;

public class CallLogAjaxLoadMR {
	
	private String userName;
	private String customer_name;
	private String vehicle_RegNo;
	private String veh_model;
	private String category;
	private String Loyalty;
	private String duedate;
	private String due_type;
	private String forecast_type;
	private String last_psfstatus;
	private String DND_status;
	private String campaignName;
	private String complaints_count;
	private String vehical_Id;
	private String wyzUser_id;
	private String customer_id;
	private String assigned_intercation_id;
	
	private String CREName;
	private String Mobile_number;
	private String model;
	private String RONumber;
	private Date ROdate;
	private Date BillDate;
	private String vehicle_id;
	private String Category;

	private String creName;
	private String campaign;
	private String customerName;
	private String phone;
	private String regNo;
	private String chassisNo;
	private String policyDueMonth;
	private Date policyDueDate;
	private String nextRenewalType;
	private String lastInsuranceCompany;
	private long insuranceassigned_id;
	private long vehicle_vehicle_id;

	public CallLogAjaxLoadMR(String creName,String campaign, String customerName, String phone, String regNo, String chassisNo,
			String policyDueMonth, Date policyDueDate, String nextRenewalType,String lastInsuranceCompany,
			long insuranceassigned_id, String customer_id,long vehicle_vehicle_id){
		this.creName=creName;
		this.campaign=campaign;
		this.customerName=customerName;
		this.phone=phone;
		this.regNo=regNo;
		this.chassisNo=chassisNo;
		this.policyDueMonth=policyDueMonth;
		this.policyDueDate=policyDueDate;
		this.nextRenewalType=nextRenewalType;
		this.lastInsuranceCompany=lastInsuranceCompany;
		this.insuranceassigned_id=insuranceassigned_id;
		this.customer_id=customer_id;
		this.vehicle_vehicle_id=vehicle_vehicle_id;
				
		
	}
	public CallLogAjaxLoadMR(String campaignName,String CREName, String customer_name, String Mobile_number,
			String vehicle_RegNo, String model, String RONumber, Date ROdate, 
			Date BillDate, String vehicle_id, String customer_id, String Category, String assigned_intercation_id){
		
		this.campaignName=campaignName;
		this.CREName=CREName;
		this.customer_name=customer_name;
		this.Mobile_number=Mobile_number;
		this.vehicle_RegNo=vehicle_RegNo;
		this.model=model;
		this.RONumber=RONumber;
		this.ROdate=ROdate;
		this.BillDate=BillDate;
		this.vehicle_id=vehicle_id;
		this.customer_id=customer_id;
		this.Category=Category;
		this.assigned_intercation_id=assigned_intercation_id;
				
	}
	public CallLogAjaxLoadMR(String customer_name, String vehicle_RegNo, String veh_model, String category,
			String Loyalty, String duedate, String due_type, String forecast_type, String last_psfstatus,
			String DND_status, String campaignName, String complaints_count, String vehical_Id, String wyzUser_id,
			String customer_id, String assigned_intercation_id, String userName) {
		this.customer_name = customer_name;
		this.vehicle_RegNo = vehicle_RegNo;
		this.veh_model = veh_model;
		this.category = category;
		this.Loyalty = Loyalty;
		this.duedate = duedate;
		this.due_type = due_type;
		this.forecast_type = forecast_type;
		this.last_psfstatus = last_psfstatus;
		this.DND_status = DND_status;
		this.campaignName = campaignName;
		this.complaints_count = complaints_count;
		this.vehical_Id = vehical_Id;
		this.wyzUser_id = wyzUser_id;
		this.customer_id = customer_id;
		this.assigned_intercation_id = assigned_intercation_id;
		this.userName = userName;
	}

	public CallLogAjaxLoadMR(String customer_name, String vehicle_RegNo, String category, String campaignName,
			String customer_id, String assigned_intercation_id, String userName, String mobile_number, String model,
			String rONumber, Date rOdate, Date billDate, String vehical_id, String vehicle_id) {

		this.customer_name = customer_name;
		this.vehicle_RegNo = vehicle_RegNo;
		this.category = category;
		this.campaignName = campaignName;
		this.customer_id = customer_id;
		this.assigned_intercation_id = assigned_intercation_id;
		this.userName = userName;
		this.Mobile_number = mobile_number;
		this.model = model;
		this.RONumber = rONumber;
		this.ROdate = rOdate;
		this.BillDate = billDate;
		this.vehicle_id = vehicle_id;
	}

	public CallLogAjaxLoadMR() {
		this.customer_name = "";
		this.vehicle_RegNo = "";
		this.veh_model = "";
		this.category = "";
		this.Category="";
		this.Loyalty = "";
		this.duedate = "";
		this.due_type = "";
		this.forecast_type = "";
		this.last_psfstatus = "";
		this.DND_status = "";
		this.campaignName = "";
		this.complaints_count = "";
		this.vehical_Id = "";
		this.wyzUser_id = "";
		this.customer_id = "";
		this.assigned_intercation_id = "";
		this.userName = "";
		this.Mobile_number = "";
		this.model = "";
		this.RONumber = "";
		this.ROdate = null;
		this.BillDate = null;
		this.vehicle_id = "";
	}

	
	public String getCreName() {
		return creName;
	}
	public void setCreName(String creName) {
		this.creName = creName;
	}
	public String getCampaign() {
		return campaign;
	}
	public void setCampaign(String campaign) {
		this.campaign = campaign;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getRegNo() {
		return regNo;
	}
	public void setRegNo(String regNo) {
		this.regNo = regNo;
	}
	public String getChassisNo() {
		return chassisNo;
	}
	public void setChassisNo(String chassisNo) {
		this.chassisNo = chassisNo;
	}
	public String getPolicyDueMonth() {
		return policyDueMonth;
	}
	public void setPolicyDueMonth(String policyDueMonth) {
		this.policyDueMonth = policyDueMonth;
	}
	public Date getPolicyDueDate() {
		return policyDueDate;
	}
	public void setPolicyDueDate(Date policyDueDate) {
		this.policyDueDate = policyDueDate;
	}
	public String getNextRenewalType() {
		return nextRenewalType;
	}
	public void setNextRenewalType(String nextRenewalType) {
		this.nextRenewalType = nextRenewalType;
	}
	public String getLastInsuranceCompany() {
		return lastInsuranceCompany;
	}
	public void setLastInsuranceCompany(String lastInsuranceCompany) {
		this.lastInsuranceCompany = lastInsuranceCompany;
	}
	public long getInsuranceassigned_id() {
		return insuranceassigned_id;
	}
	public void setInsuranceassigned_id(long insuranceassigned_id) {
		this.insuranceassigned_id = insuranceassigned_id;
	}
	public long getVehicle_vehicle_id() {
		return vehicle_vehicle_id;
	}
	public void setVehicle_vehicle_id(long vehicle_vehicle_id) {
		this.vehicle_vehicle_id = vehicle_vehicle_id;
	}
	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public String getVehicle_RegNo() {
		return vehicle_RegNo;
	}

	public void setVehicle_RegNo(String vehicle_RegNo) {
		this.vehicle_RegNo = vehicle_RegNo;
	}

	public String getVeh_model() {
		return veh_model;
	}

	public void setVeh_model(String veh_model) {
		this.veh_model = veh_model;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getLoyalty() {
		return Loyalty;
	}

	public void setLoyalty(String Loyalty) {
		this.Loyalty = Loyalty;
	}

	public String getDuedate() {
		return duedate;
	}

	public void setDuedate(String duedate) {
		this.duedate = duedate;
	}

	public String getDue_type() {
		return due_type;
	}

	public void setDue_type(String due_type) {
		this.due_type = due_type;
	}

	public String getForecast_type() {
		return forecast_type;
	}

	public void setForecast_type(String forecast_type) {
		this.forecast_type = forecast_type;
	}

	public String getLast_psfstatus() {
		return last_psfstatus;
	}

	public void setLast_psfstatus(String last_psfstatus) {
		this.last_psfstatus = last_psfstatus;
	}

	public String getDND_status() {
		return DND_status;
	}

	public void setDND_status(String DND_status) {
		this.DND_status = DND_status;
	}

	public String getComplaints_count() {
		return complaints_count;
	}

	public void setComplaints_count(String complaints_count) {
		this.complaints_count = complaints_count;
	}

	public String getVehical_Id() {
		return vehical_Id;
	}

	public void setVehical_Id(String vehical_Id) {
		this.vehical_Id = vehical_Id;
	}

	public String getWyzUser_id() {
		return wyzUser_id;
	}

	public void setWyzUser_id(String wyzUser_id) {
		this.wyzUser_id = wyzUser_id;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public String getAssigned_intercation_id() {
		return assigned_intercation_id;
	}

	public void setAssigned_intercation_id(String assigned_intercation_id) {
		this.assigned_intercation_id = assigned_intercation_id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCampaignName() {
		return campaignName;
	}

	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	public String getMobile_number() {
		return Mobile_number;
	}

	public void setMobile_number(String mobile_number) {
		Mobile_number = mobile_number;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getRONumber() {
		return RONumber;
	}

	public void setRONumber(String rONumber) {
		RONumber = rONumber;
	}

	public Date getROdate() {
		return ROdate;
	}

	public void setROdate(Date rOdate) {
		ROdate = rOdate;
	}

	public Date getBillDate() {
		return BillDate;
	}

	public void setBillDate(Date billDate) {
		BillDate = billDate;
	}

	public String getVehicle_id() {
		return vehicle_id;
	}

	public void setVehicle_id(String vehicle_id) {
		this.vehicle_id = vehicle_id;
	}

	public String getCREName() {
		return CREName;
	}

	public void setCREName(String cREName) {
		CREName = cREName;
	}

}
