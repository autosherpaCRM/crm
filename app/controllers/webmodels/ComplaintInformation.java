/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.webmodels;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author wct-09
 */
public class ComplaintInformation {
    
    public String model;
    public String vehicleRegNo;
    public String variant;
    @Temporal(TemporalType.DATE)
    public Date saleDate;
    public String chassisNo;
    public String customerName;
    public String customerEmail;
    public String preferredAdress;
    public String customerMobileNo;
   public String concatenatedAdress;
    public String complaintNumber;
    public String complaintType;
    public String functionName;
    public String sourceName;
    public String subcomplaintType;
    public String assignedUser;
    public String resolutionBy;
    public String location;
    public String priority;
    public String complaintStatus;
    public Date issueDate;
    public long ageOfComplaint;
    public String wyzUser;
    public String customerstatus;
    public String reasonFor;
    public String actionTaken;
    public Date lastServiceDate;
    public String raisedDate;
    public String serviceadvisor;  
    public String workshop;
    public String endDate;

    
	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

    public String getReasonFor() {
		return reasonFor;
	}

	public void setReasonFor(String reasonFor) {
		this.reasonFor = reasonFor;
	}

	public String getActionTaken() {
		return actionTaken;
	}

	public void setActionTaken(String actionTaken) {
		this.actionTaken = actionTaken;
	}

	public String getWyzUser() {
        return wyzUser;
    }

    public void setWyzUser(String wyzUser) {
        this.wyzUser = wyzUser;
    }
    
    

    public long getAgeOfComplaint() {
        return ageOfComplaint;
    }

    public void setAgeOfComplaint(long ageOfComplaint) {
        this.ageOfComplaint = ageOfComplaint;
    }

 public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    
    public String getComplaintStatus() {
        return complaintStatus;
    }

    public void setComplaintStatus(String complaintStatus) {
        this.complaintStatus = complaintStatus;
    }
    
  public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

  

   

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }
    public String getAssignedUser() {
        return assignedUser;
    }

    public void setAssignedUser(String assignedUser) {
        this.assignedUser = assignedUser;
    }
        
public String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getComplaintNumber() {
        return complaintNumber;
    }

    public void setComplaintNumber(String complaintNumber) {
        this.complaintNumber = complaintNumber;
    }

    public String getComplaintType() {
        return complaintType;
    }

    public void setComplaintType(String complaintType) {
        this.complaintType = complaintType;
    }

    public String getFunctionName() {
        return functionName;
    }

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public String getSubcomplaintType() {
        return subcomplaintType;
    }

    public void setSubcomplaintType(String subcomplaintType) {
        this.subcomplaintType = subcomplaintType;
    }
   
    public String getConcatenatedAdress() {
        return concatenatedAdress;
    }

    public void setConcatenatedAdress(String concatenatedAdress) {
        this.concatenatedAdress = concatenatedAdress;
    }

    
    
    public String serviceAdvisor;

    public String getServiceAdvisor() {
        return serviceAdvisor;
    }

    public String getPreferredAdress() {
        return preferredAdress;
    }

    public void setPreferredAdress(String preferredAdress) {
        this.preferredAdress = preferredAdress;
    }

    public void setServiceAdvisor(String serviceAdvisor) {
        this.serviceAdvisor = serviceAdvisor;
    }
    
    public String isError;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getVehicleRegNo() {
        return vehicleRegNo;
    }

    public void setVehicleRegNo(String vehicleRegNo) {
        this.vehicleRegNo = vehicleRegNo;
    }

    public String getVariant() {
        return variant;
    }

    public void setVariant(String variant) {
        this.variant = variant;
    }

    public Date getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(Date saleDate) {
        this.saleDate = saleDate;
    }



    public String getChassisNo() {
        return chassisNo;
    }

    public void setChassisNo(String chassisNo) {
        this.chassisNo = chassisNo;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }



 

    public String getCustomerMobileNo() {
        return customerMobileNo;
    }

    public void setCustomerMobileNo(String customerMobileNo) {
        this.customerMobileNo = customerMobileNo;
    }

 

    public String getIsError() {
        return isError;
    }

    public void setIsError(String isError) {
        this.isError = isError;
    }

	public String getCustomerstatus() {
		return customerstatus;
	}

	public void setCustomerstatus(String customerstatus) {
		this.customerstatus = customerstatus;
	}

	public String getResolutionBy() {
		return resolutionBy;
	}

	public void setResolutionBy(String resolutionBy) {
		this.resolutionBy = resolutionBy;
	}

	public Date getLastServiceDate() {
		return lastServiceDate;
	}

	public void setLastServiceDate(Date lastServiceDate) {
		this.lastServiceDate = lastServiceDate;
	}

	public String getServiceadvisor() {
		return serviceadvisor;
	}

	public void setServiceadvisor(String serviceadvisor) {
		this.serviceadvisor = serviceadvisor;
	}

	public String getWorkshop() {
		return workshop;
	}

	public void setWorkshop(String workshop) {
		this.workshop = workshop;
	}

	public String getRaisedDate() {
		return raisedDate;
	}

	public void setRaisedDate(String raisedDate) {
		this.raisedDate = raisedDate;
	}
    
    
    
    
}
