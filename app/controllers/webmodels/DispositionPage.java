package controllers.webmodels;

public class DispositionPage {
	
	public String typeOfDispoPageView;
	
	public long typeOfPSF;
	
	public String oem;

	public String getTypeOfDispoPageView() {
		return typeOfDispoPageView;
	}

	public void setTypeOfDispoPageView(String typeOfDispoPageView) {
		this.typeOfDispoPageView = typeOfDispoPageView;
	}

	public long getTypeOfPSF() {
		return typeOfPSF;
	}

	public void setTypeOfPSF(long typeOfPSF) {
		this.typeOfPSF = typeOfPSF;
	}
	
	public String getOem() {
		return oem;
	}

	public void setOem(String oem) {
		this.oem = oem;
	}


	
}
