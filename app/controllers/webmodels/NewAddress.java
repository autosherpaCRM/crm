package controllers.webmodels;

public class NewAddress {
	
	public String address1New;
	public String address2New;
	public String cityNew;
	public String stateNew;
	public long pincodeNew;
	public String getAddress1New() {
		return address1New;
	}
	public void setAddress1New(String address1New) {
		this.address1New = address1New;
	}
	public String getAddress2New() {
		return address2New;
	}
	public void setAddress2New(String address2New) {
		this.address2New = address2New;
	}
	public String getCityNew() {
		return cityNew;
	}
	public void setCityNew(String cityNew) {
		this.cityNew = cityNew;
	}
	public String getStateNew() {
		return stateNew;
	}
	public void setStateNew(String stateNew) {
		this.stateNew = stateNew;
	}
	public long getPincodeNew() {
		return pincodeNew;
	}
	public void setPincodeNew(long pincodeNew) {
		this.pincodeNew = pincodeNew;
	}
	
	

}
