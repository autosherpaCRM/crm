package controllers.webmodels;

public class WyzUserList {
	long id;
	String userName;
	String role;
	String phoneNumber;
	String phoneIMEINo;
	boolean unAvailable;
	
	public boolean isUnAvailable() {
		return unAvailable;
	}
	public void setUnAvailable(boolean unAvailable) {
		this.unAvailable = unAvailable;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getPhoneIMEINo() {
		return phoneIMEINo;
	}
	public void setPhoneIMEINo(String phoneIMEINo) {
		this.phoneIMEINo = phoneIMEINo;
	}
	public WyzUserList(long id, String userName, String role, String phoneNumber, String phoneIMEINo, boolean unAvailable) {
		
		this.id = id;
		this.userName = userName;
		this.role = role;
		this.phoneNumber = phoneNumber;
		this.phoneIMEINo = phoneIMEINo;
		this.unAvailable = unAvailable;
	}
	public WyzUserList() {
		this.id = (long)0;
		this.userName = null;
		this.role = null;
		this.phoneNumber = null;
		this.phoneIMEINo = null;
		this.unAvailable = false;
	}
	
	
}
