package controllers.webmodels;

import java.util.Date;

import javax.persistence.ColumnResult;

public class DailyAvailabilityData {
	
	public String userName;
	public String campaignName;
	public long freshCalls;
	public long followUps;
	public long overDueBookings;
	public long nonContacts;
	public Date assignedDate;
		
	
	public DailyAvailabilityData(String userName,String campaignName, long freshCalls,
			long followUps,long overDueBookings, long nonContacts,Date assignedDate){
		this.userName=userName;
		this.campaignName=campaignName;
		this.freshCalls=freshCalls;
		this.followUps=followUps;
		this.overDueBookings=overDueBookings;
		this.nonContacts=nonContacts;
		this.assignedDate=assignedDate;
		
	}
	
	public DailyAvailabilityData(){
		this.userName="";
		this.campaignName="";
		this.freshCalls=(long) 0;
		this.followUps=(long) 0;
		this.overDueBookings=(long) 0;
		this.nonContacts=(long) 0;
		this.assignedDate=null;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCampaignName() {
		return campaignName;
	}

	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	public long getFreshCalls() {
		return freshCalls;
	}

	public void setFreshCalls(long freshCalls) {
		this.freshCalls = freshCalls;
	}

	public long getFollowUps() {
		return followUps;
	}

	public void setFollowUps(long followUps) {
		this.followUps = followUps;
	}

	public long getOverDueBookings() {
		return overDueBookings;
	}

	public void setOverDueBookings(long overDueBookings) {
		this.overDueBookings = overDueBookings;
	}

	public long getNonContacts() {
		return nonContacts;
	}

	public void setNonContacts(long nonContacts) {
		this.nonContacts = nonContacts;
	}

	public Date getAssignedDate() {
		return assignedDate;
	}

	public void setAssignedDate(Date assignedDate) {
		this.assignedDate = assignedDate;
	}

	


	
	

	
	

}
