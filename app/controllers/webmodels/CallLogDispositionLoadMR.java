package controllers.webmodels;

import java.util.Date;

public class CallLogDispositionLoadMR {

	private String isCallinitaited;
    private String creName;
    private long droppedCount;
    private String campaignName;
    private Date call_date;
    private String customerName;
    private String mobileNumber;
    private String vehicle_RegNo;
    private Date appointmentDate;
    private String appointmentTime;
    private String renewalType;
    private String appointmentstatus;
    private String insurance_agent;
    private String Last_disposition;
    private String reason;
    private String followUpDate;
    private String followUpTime;
    private Date policyDueDate;
    private String vehicle_id;   
    private String customer_id;

    private String scheduled_date;
    private String scheduled_time;
    private String duedate;
    private String userName;
	private String mediaFileLob;
    private String Media_file;
    private String dialedNoIs;
    private Date psfAppointmentDate;
    private String psfAppointmentTime;
    private String CREName;
    private String callinteraction_id;
	private String customer_name;
    private String Mobile_number;
    private String RONumber;
    private Date ROdate;
    private Date BillDate;
    private Date Servey_date;
    private String model;



    public CallLogDispositionLoadMR(String isCallinitaited, String creName, long droppedCount,String campaignName,
    		Date call_date, String customerName, String mobileNumber, String vehicle_RegNo, Date appointmentDate,
    		String appointmentTime, String renewalType,String appointmentstatus,String insurance_agent,
    		String Last_disposition, String reason, String followUpDate, String followUpTime,
    		Date policyDueDate,String vehicle_id, String customer_id){
    	this.isCallinitaited=isCallinitaited;
    	this.creName=creName;
    	this.droppedCount=droppedCount;
    	this.campaignName=campaignName;
    	this.call_date=call_date;
    	this.customerName=customerName;
    	this.mobileNumber=mobileNumber;
    	this.vehicle_RegNo=vehicle_RegNo;
    	this.appointmentDate=appointmentDate;
    	this.appointmentTime=appointmentTime;
    	this.renewalType=renewalType;
    	this.appointmentstatus=appointmentstatus;
    	this.insurance_agent=insurance_agent;
    	this.Last_disposition=Last_disposition;
    	this.reason=reason;
    	this.followUpDate=followUpDate;
    	this.followUpTime=followUpTime;
    	this.policyDueDate=policyDueDate;
    	this.vehicle_id=vehicle_id;
    	this.customer_id=customer_id;
    }
    public CallLogDispositionLoadMR(String isCallinitaited,String creName,String campaignName, Date call_date,String customerName, String mobileNumber,String vehicle_RegNo,
    		Date appointmentDate , String appointmentTime, String renewalType, String appointmentstatus, String insurance_agent, String Last_disposition,
    		String reason,String followUpDate, String followUpTime, Date policyDueDate, String vehicle_id, String customer_id){
    	this.isCallinitaited=isCallinitaited;
    	this.creName=creName;
    	this.campaignName=campaignName;
    	this.call_date=call_date;
    	this.customerName=customerName;
    	this.mobileNumber=mobileNumber;
    	this.vehicle_RegNo=vehicle_RegNo;
    	this.appointmentDate=appointmentDate;
    	this.appointmentTime=appointmentTime;
    	this.renewalType=renewalType;
    	this.appointmentstatus=appointmentstatus;
    	this.insurance_agent=insurance_agent;
    	this.Last_disposition=Last_disposition;
    	this.reason=reason;
    	this.followUpDate=followUpDate;
    	this.followUpTime=followUpTime;
    	this.policyDueDate=policyDueDate;
    	this.vehicle_id=vehicle_id;
    	this.customer_id=customer_id;
    			
    	
    }

    
    public String getMedia_file() {
		return Media_file;
	}

	public void setMedia_file(String media_file) {
		Media_file = media_file;
	}


    
    public CallLogDispositionLoadMR(Date call_date,String customer_name, String vehicle_RegNo , String username, 
    		String Media_file , String dialedNoIs,String callinteraction_id){
    	this.call_date= call_date;
    	this.customer_name = customer_name;
    	this.vehicle_RegNo=vehicle_RegNo;
    	this.userName = username;
    	this.Media_file= Media_file;
    	this.dialedNoIs= dialedNoIs;
    	this.callinteraction_id=callinteraction_id;
    	
    }
    public CallLogDispositionLoadMR(String campaignName,String creName, String callinteraction_id, String customer_name, String Mobile_number,
    		String vehicle_RegNo,String followUpDate, String followUpTime, Date call_date,String RONumber, 
    		Date ROdate, Date BillDate,Date Servey_date, String vehicle_id, String model, 
    		String customer_id, Date psfAppointmentDate, String psfAppointmentTime){
    	this.campaignName = campaignName;
    	this.creName=creName;
    	this.callinteraction_id=callinteraction_id;
    	this.customer_name=customer_name;
    	this.Mobile_number=Mobile_number;
    	this.vehicle_RegNo=vehicle_RegNo;
    	this.followUpDate=followUpDate;
    	this.followUpTime=followUpTime;
    	this.call_date=call_date;
    	this.RONumber=RONumber;
    	this.ROdate=ROdate;
    	this.BillDate=BillDate;
    	this.Servey_date=Servey_date;
    	this.vehicle_id=vehicle_id;
    	this.model=model;
    	this.customer_id=customer_id;
    	this.psfAppointmentDate=psfAppointmentDate;
    	this.psfAppointmentTime=psfAppointmentTime;
    	
    }
    
    public CallLogDispositionLoadMR(String campaignName,String CREName, String callinteraction_id, String customer_name, String Mobile_number,
    		String vehicle_RegNo,String followUpDate, String followUpTime, Date call_date,String RONumber, 
    		Date ROdate, Date BillDate,Date Servey_date, String vehicle_id, String model, 
    		String customer_id){
    	this.campaignName = campaignName;
    	this.CREName=CREName;
    	this.callinteraction_id=callinteraction_id;
    	this.customer_name=customer_name;
    	this.Mobile_number=Mobile_number;
    	this.vehicle_RegNo=vehicle_RegNo;
    	this.followUpDate=followUpDate;
    	this.followUpTime=followUpTime;
    	this.call_date=call_date;
    	this.RONumber=RONumber;
    	this.ROdate=ROdate;
    	this.BillDate=BillDate;
    	this.Servey_date=Servey_date;
    	this.vehicle_id=vehicle_id;
    	this.model=model;
    	this.customer_id=customer_id;
    	
    	
    }

    public CallLogDispositionLoadMR(String campaignName,String CREName, String callinteraction_id, String customer_name, String Mobile_number,
    		String vehicle_RegNo,String followUpDate, String followUpTime, Date call_date,String RONumber, 
    		Date ROdate, Date BillDate,Date Servey_date, String vehicle_id, String model, 
    		String customer_id,String Last_disposition){
    	this.campaignName = campaignName;
    	this.CREName=CREName;
    	this.callinteraction_id=callinteraction_id;
    	this.customer_name=customer_name;
    	this.Mobile_number=Mobile_number;
    	this.vehicle_RegNo=vehicle_RegNo;
    	this.followUpDate=followUpDate;
    	this.followUpTime=followUpTime;
    	this.call_date=call_date;
    	this.RONumber=RONumber;
    	this.ROdate=ROdate;
    	this.BillDate=BillDate;
    	this.Servey_date=Servey_date;
    	this.vehicle_id=vehicle_id;
    	this.model=model;
    	this.customer_id=customer_id;
    	this.Last_disposition=Last_disposition;
    	
    	
    }
    public CallLogDispositionLoadMR(String customer_name, String vehicle_RegNo, String Mobile_number, 
            String callinteraction_id, String followUpDate, String followUpTime, String Last_disposition, 
            String scheduled_date, String scheduled_time, String reason, String vehicle_id, String customer_id, 
            String duedate,String userName,String mediaFileLob,String campaignName,Date call_date,String isCallinitaited) {
    	
        this.customer_name = customer_name;
        this.vehicle_RegNo = vehicle_RegNo;
        this.Mobile_number = Mobile_number;
        this.callinteraction_id = callinteraction_id;
        this.followUpDate = followUpDate;
        this.followUpTime = followUpTime;
        this.Last_disposition = Last_disposition;
        this.scheduled_date = scheduled_date;
        this.scheduled_time = scheduled_time;
        this.reason = reason;
        this.vehicle_id = vehicle_id;
        this.customer_id = customer_id;
        this.duedate = duedate;
        this.userName = userName;
        this.mediaFileLob = mediaFileLob;
        this.campaignName=campaignName;
        this.call_date=call_date;
		this.isCallinitaited=isCallinitaited;

    }
    
     public CallLogDispositionLoadMR() {
    	 
        this.customer_name = "";
        this.vehicle_RegNo = "";
        this.Mobile_number = "";
        this.callinteraction_id = "";
        this.followUpDate = "";
        this.followUpTime = "";
        this.Last_disposition = "";
        this.scheduled_date = "";
        this.scheduled_time = "";
        this.reason = "";
        this.vehicle_id = "";
        this.customer_id = "";
        this.duedate = "";
        this.userName = "";
        this.mediaFileLob = "";
        this.campaignName="";
        this.call_date=null;
		this.isCallinitaited="";
        this.dialedNoIs="";
        this.policyDueDate=null;
    }

     
	public long getDroppedCount() {
		return droppedCount;
	}
	public void setDroppedCount(long droppedCount) {
		this.droppedCount = droppedCount;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public Date getAppointmentDate() {
		return appointmentDate;
	}
	public void setAppointmentDate(Date appointmentDate) {
		this.appointmentDate = appointmentDate;
	}
	public String getAppointmentTime() {
		return appointmentTime;
	}
	public void setAppointmentTime(String appointmentTime) {
		this.appointmentTime = appointmentTime;
	}
	public String getRenewalType() {
		return renewalType;
	}
	public void setRenewalType(String renewalType) {
		this.renewalType = renewalType;
	}
	public String getAppointmentstatus() {
		return appointmentstatus;
	}
	public void setAppointmentstatus(String appointmentstatus) {
		this.appointmentstatus = appointmentstatus;
	}
	public String getInsurance_agent() {
		return insurance_agent;
	}
	public void setInsurance_agent(String insurance_agent) {
		this.insurance_agent = insurance_agent;
	}
	public Date getPolicyDueDate() {
		return policyDueDate;
	}
	public void setPolicyDueDate(Date policyDueDate) {
		this.policyDueDate = policyDueDate;
	}
	public String getCREName() {
		return CREName;
	}

	public void setCREName(String cREName) {
		CREName = cREName;
	}

	public String getCreName() {
		return creName;
	}

	public void setCreName(String creName) {
		this.creName = creName;
	}

	public String getRONumber() {
		return RONumber;
	}

	public void setRONumber(String rONumber) {
		RONumber = rONumber;
	}

	public Date getROdate() {
		return ROdate;
	}

	public void setROdate(Date rOdate) {
		ROdate = rOdate;
	}

	public Date getBillDate() {
		return BillDate;
	}

	public void setBillDate(Date billDate) {
		BillDate = billDate;
	}

	

	public Date getServey_date() {
		return Servey_date;
	}

	public void setServey_date(Date servey_date) {
		Servey_date = servey_date;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public Date getPsfAppointmentDate() {
		return psfAppointmentDate;
	}

	public void setPsfAppointmentDate(Date psfAppointmentDate) {
		this.psfAppointmentDate = psfAppointmentDate;
	}

	public String getPsfAppointmentTime() {
		return psfAppointmentTime;
	}

	public void setPsfAppointmentTime(String psfAppointmentTime) {
		this.psfAppointmentTime = psfAppointmentTime;
	}

	public String getIsCallinitaited() {
		return isCallinitaited;
	}

	public void setIsCallinitaited(String isCallinitaited) {
		this.isCallinitaited = isCallinitaited;
	}
	
	public String getDialedNoIs() {
		return dialedNoIs;
	}

	public void setDialedNoIs(String dialedNoIs) {
		this.dialedNoIs = dialedNoIs;
	}
	
	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public String getVehicle_RegNo() {
		return vehicle_RegNo;
	}

	public void setVehicle_RegNo(String vehicle_RegNo) {
		this.vehicle_RegNo = vehicle_RegNo;
	}

	public String getMobile_number() {
		return Mobile_number;
	}

	public void setMobile_number(String mobile_number) {
		Mobile_number = mobile_number;
	}

	public String getCallinteraction_id() {
		return callinteraction_id;
	}

	public void setCallinteraction_id(String callinteraction_id) {
		this.callinteraction_id = callinteraction_id;
	}
	

	public String getFollowUpDate() {
		return followUpDate;
	}

	public void setFollowUpDate(String followUpDate) {
		this.followUpDate = followUpDate;
	}

	public String getFollowUpTime() {
		return followUpTime;
	}

	public void setFollowUpTime(String followUpTime) {
		this.followUpTime = followUpTime;
	}

	public String getLast_disposition() {
		return Last_disposition;
	}

	public void setLast_disposition(String last_disposition) {
		Last_disposition = last_disposition;
	}

	public String getScheduled_date() {
		return scheduled_date;
	}

	public void setScheduled_date(String scheduled_date) {
		this.scheduled_date = scheduled_date;
	}

	public String getScheduled_time() {
		return scheduled_time;
	}

	public void setScheduled_time(String scheduled_time) {
		this.scheduled_time = scheduled_time;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getVehicle_id() {
		return vehicle_id;
	}

	public void setVehicle_id(String vehicle_id) {
		this.vehicle_id = vehicle_id;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public String getDuedate() {
		return duedate;
	}

	public void setDuedate(String duedate) {
		this.duedate = duedate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getMediaFileLob() {
		return mediaFileLob;
	}

	public void setMediaFileLob(String mediaFileLob) {
		this.mediaFileLob = mediaFileLob;
	}

	public String getCampaignName() {
		return campaignName;
	}

	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	public Date getCall_date() {
		return call_date;
	}

	public void setCall_date(Date call_date) {
		this.call_date = call_date;
	}
	
	
	
}
