/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.webmodels;

import java.util.Date;

/**
 *
 * @author W-885
 */
public class CallLogDispositionLoad {
        private String customer_name;
        private String vehicle_RegNo;
        private String Mobile_number;
        private String callinteraction_id;
        private String followupdate;
        private String followUpTime;
        private String Last_disposition;
        private String scheduled_date;
        private String scheduled_time;
        private String reason;
        private String vehicle_id;       
        private String customer_id;
        private String duedate;
        private String campaignName;
        public  Date call_date;
    	private String RONumber;
    	private Date ROdate;
    	private Date BillDate;
    	private String model;
    	private String category;
    	private String followUpDate;
    	private Date Servey_date;
    	private Date psfAppointmentDate;
    	private String psfAppointmentTime;
		private String veh_model;
		

		public CallLogDispositionLoad(String callinteraction_id,String customer_name, String Mobile_number, 
				String vehicle_RegNo, String followUpDate, String followUpTime, Date call_date, 
				String RONumber, Date ROdate, Date BillDate, Date Servey_date, String vehicle_id, String model,
				String customer_id,Date psfAppointmentDate,String psfAppointmentTime){
			this.callinteraction_id=callinteraction_id;
			this.customer_name = customer_name;
			this.Mobile_number = Mobile_number;
			this.vehicle_RegNo= vehicle_RegNo;
			this.followUpDate= followUpDate;
			this.followUpTime=followUpTime;
			this.call_date=call_date;
			this.RONumber=RONumber;
			this.ROdate=ROdate;
			this.BillDate=BillDate;
			this.Servey_date=Servey_date;
			this.vehicle_id=vehicle_id;
			this.model=model;
			this.customer_id=customer_id;
			this.psfAppointmentDate=psfAppointmentDate;
			this.psfAppointmentTime=psfAppointmentTime;
		}
        
	 	 public CallLogDispositionLoad(String callinteraction_id ,String customer_name,String Mobile_number,String vehicle_RegNo , String followUpDate , String followuptime, Date call_date , String RONumber, Date ROdate, Date BillDate, Date Servey_date, String vehicle_id,String model, String customer_id){
	 	    	this.callinteraction_id = callinteraction_id;
	 		 	this.customer_name = customer_name;
	 	    	this.Mobile_number = Mobile_number;
	 	    	this.vehicle_RegNo =vehicle_RegNo;
	 	    	this.followUpDate  = followUpDate;
	 	    	this.followUpTime  = followUpTime;
	 	    	this.call_date     =call_date;
	 	    	this.RONumber      =RONumber;
	 	    	this.ROdate        =ROdate;
	 	    	this.BillDate      =BillDate;
	 	    	this.Servey_date   =Servey_date;
	 	    	this.vehicle_id    =vehicle_id;
	 	    	this.model         =model;
	 	    	this.customer_id   =customer_id;
	 	    }
    
    	 public CallLogDispositionLoad(String callinteraction_id, String customer_name,String Mobile_number,
    			 String vehicle_RegNo , String followUpDate , String followUpTime, 
    			 Date call_date , String RONumber, Date ROdate, Date BillDate, Date servey_date ,String vehicle_id, 
    			 String model,String customer_id, Date psfAppointmentDate,String psfAppointmentTime, Date Servey_date){
 	    	this.callinteraction_id =callinteraction_id;
    		this.customer_name = customer_name;
 	    	this.Mobile_number = Mobile_number;
 	    	this.vehicle_RegNo=vehicle_RegNo;
 	    	this.followUpDate= followUpDate;
 	    	this.followUpTime= followUpTime;
 	    	this.call_date=call_date;
 	    	this.RONumber=RONumber;
 	    	this.ROdate=ROdate;
 	    	this.BillDate=BillDate;
 	    	this.Servey_date=Servey_date;
 	    	this.vehicle_id=vehicle_id;
 	    	this.model=model;
 	    	this.customer_id=customer_id;
 	    	this.psfAppointmentDate=psfAppointmentDate;
 	    	this.psfAppointmentTime=psfAppointmentTime;
 	    }
    public CallLogDispositionLoad(String customer_name, String vehicle_RegNo, String Mobile_number, 
            String callinteraction_id, String followUpDate, String followUpTime, String Last_disposition, 
            String scheduled_date, String scheduled_time, String reason, String vehicle_id, String customer_id, 
            String duedate,String campaignName,Date call_date) {
        this.customer_name = customer_name;
        this.vehicle_RegNo = vehicle_RegNo;
        this.Mobile_number = Mobile_number;
        this.callinteraction_id = callinteraction_id;
        this.followUpDate = followUpDate;
        this.followUpTime = followUpTime;
        this.Last_disposition = Last_disposition;
        this.scheduled_date = scheduled_date;
        this.scheduled_time = scheduled_time;
        this.reason = reason;
        this.vehicle_id = vehicle_id;
        this.customer_id = customer_id;
        this.duedate = duedate;
        this.campaignName=campaignName;
        this.call_date=call_date;
    }
    
    
    
    public CallLogDispositionLoad() {
	
		this.customer_name = "";
		this.vehicle_RegNo = "";
		this.Mobile_number = "";
		this.callinteraction_id = "";
		this.followupdate = "";
		this.followUpTime = "";
		this.Last_disposition = "";
		this.scheduled_date = "";
		this.scheduled_time = "";
		this.reason = "";
		this.vehicle_id = "";
		this.customer_id = "";
		this.duedate = "";
		this.campaignName = "";
		this.call_date = null;
		this.RONumber = "";
		this.ROdate = null;
		this.BillDate = null;
		this.model = "";
		this.category = "";
		this.followUpDate = "";
		this.Servey_date = null;
		this.psfAppointmentDate = null;
		this.psfAppointmentTime = "";
		this.veh_model = "";
	}

	public String getVeh_model() {
		return veh_model;
	}
	public void setVeh_model(String veh_model) {
		this.veh_model = veh_model;
	}
	
	public String getFollowUpDate() {
		return followUpDate;
	}

	public void setFollowUpDate(String followUpDate) {
		this.followUpDate = followUpDate;
	}

	public Date getServey_date() {
		return Servey_date;
	}
	public void setServey_date(Date servey_date) {
		Servey_date = servey_date;
	}
	public Date getPsfAppointmentDate() {
		return psfAppointmentDate;
	}
	public void setPsfAppointmentDate(Date psfAppointmentDate) {
		this.psfAppointmentDate = psfAppointmentDate;
	}
	public String getPsfAppointmentTime() {
		return psfAppointmentTime;
	}
	public void setPsfAppointmentTime(String psfAppointmentTime) {
		this.psfAppointmentTime = psfAppointmentTime;
	}
	public String getRONumber() {
		return RONumber;
	}
	public void setRONumber(String rONumber) {
		RONumber = rONumber;
	}
	public Date getROdate() {
		return ROdate;
	}
	public void setROdate(Date rOdate) {
		ROdate = rOdate;
	}
	public Date getBillDate() {
		return BillDate;
	}
	public void setBillDate(Date billDate) {
		BillDate = billDate;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getVehicle_RegNo() {
        return vehicle_RegNo;
    }

    public void setVehicle_RegNo(String vehicle_RegNo) {
        this.vehicle_RegNo = vehicle_RegNo;
    }

    public String getMobile_number() {
        return Mobile_number;
    }

    public void setMobile_number(String Mobile_number) {
        this.Mobile_number = Mobile_number;
    }

    public String getCallinteraction_id() {
        return callinteraction_id;
    }

    public void setCallinteraction_id(String callinteraction_id) {
        this.callinteraction_id = callinteraction_id;
    }

    

    public String getFollowupdate() {
        return followupdate;
    }

    public void setFollowupdate(String followupdate) {
        this.followupdate = followupdate;
    }

    

    public String getFollowUpTime() {
		return followUpTime;
	}

	public void setFollowUpTime(String followUpTime) {
		this.followUpTime = followUpTime;
	}

	public String getLast_disposition() {
        return Last_disposition;
    }

    public void setLast_disposition(String Last_disposition) {
        this.Last_disposition = Last_disposition;
    }

    public String getScheduled_date() {
        return scheduled_date;
    }

    public void setScheduled_date(String scheduled_date) {
        this.scheduled_date = scheduled_date;
    }

    public String getScheduled_time() {
        return scheduled_time;
    }

    public void setScheduled_time(String scheduled_time) {
        this.scheduled_time = scheduled_time;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(String vehicle_id) {
        this.vehicle_id = vehicle_id;
    }

    

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getDuedate() {
        return duedate;
    }

    public void setDuedate(String duedate) {
        this.duedate = duedate;
    }

	public String getCampaignName() {
		return campaignName;
	}

	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	public Date getCall_date() {
		return call_date;
	}

	public void setCall_date(Date call_date) {
		this.call_date = call_date;
	}

        
        
}
