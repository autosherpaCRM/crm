/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.webmodels;

import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author W-885
 */
public class SRDispositionDataOnTabLoad {
    public String callDisposition;
    public String comments;
    public String typeOfCall;
    
    
    @Temporal(TemporalType.DATE)
	public Date followUpDate;

	public String followUpTime;
        public String noServiceReason;

    public String getNoServiceReason() {
        return noServiceReason;
    }

    public void setNoServiceReason(String noServiceReason) {
        this.noServiceReason = noServiceReason;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
        
        
    
    public String getCallDisposition() {
        return callDisposition;
    }
    

    public void setCallDisposition(String callDisposition) {
        this.callDisposition = callDisposition;
    }

    public Date getFollowUpDate() {
        return followUpDate;
    }

    public void setFollowUpDate(Date followUpDate) {
        this.followUpDate = followUpDate;
    }

    public String getFollowUpTime() {
        return followUpTime;
    }

    public void setFollowUpTime(String followUpTime) {
        this.followUpTime = followUpTime;
    }

	public String getTypeOfCall() {
		return typeOfCall;
	}

	public void setTypeOfCall(String typeOfCall) {
		this.typeOfCall = typeOfCall;
	}
    
    
}
