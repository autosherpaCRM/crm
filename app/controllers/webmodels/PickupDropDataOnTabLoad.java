/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.webmodels;

import java.util.Date;
import java.util.List;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author W-885
 */
public class PickupDropDataOnTabLoad {
    
	public long id;
	public String userName;
	@Temporal(TemporalType.TIME)
	public List<Date> listTime;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public List<Date> getListTime() {
		return listTime;
	}
	public void setListTime(List<Date> listTime) {
		this.listTime = listTime;
	}
	
	
}
