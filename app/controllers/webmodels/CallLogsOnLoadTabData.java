/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.webmodels;

import java.util.List;
import models.AssignedInteraction;
import models.Customer;
import models.Insurance;
import models.CallInteraction;
import models.SRDisposition;
import models.Service;
import models.ServiceBooked;
import models.Vehicle;
import models.Workshop;
import models.WyzUser;
import models.ServiceAdvisor;
import models.PickupDrop;
import models.Driver;


/**
 *
 * @author W-885
 */
public class CallLogsOnLoadTabData {
    
   
    public List<WorkShopSummaryDataOnTabLoad> workshopSummaryLoadList;

    public List<WorkShopSummaryDataOnTabLoad> getWorkshopSummaryLoadList() {
        return workshopSummaryLoadList;
    }

    public void setWorkshopSummaryLoadList(List<WorkShopSummaryDataOnTabLoad> workshopSummaryLoadList) {
        this.workshopSummaryLoadList = workshopSummaryLoadList;
    }
    
    public List<CustomerDataOnTabLoad> customerLoadList;
    public List<VehicleDataOnTabLoad> vehicleLoadList;
    public List<WyzUserDataOnTabLoad> wyzUserLoadList;
    public List<AssignedInteractionDataOnTabLoad> assignedInteractionLoadList;
    public List<ServiceDataOnTabLoad> serviceLoadList;
    
    public List<InsuranceDataOnTabLoad> insuranceLoadList;
    public List<SRDispositionDataOnTabLoad> srdispositionLoadList;
    public List<CallInteractionDataOnTabLoad> callInteractionLoadList;  
    public List<ServiceAdvisorDataOnTabLoad> serviceAdvisorLoadList;
    public List<PickupDropDataOnTabLoad> pickupDropLoadList;
    public List<DriverDataOnTabLoad> driverLoadList;
    
    
    public List<ServicebookedDataOnTabLoad> servicebookedLoadList;
    
    
    
    
    
    public List<Customer>  customerList;
    public List<Vehicle>   vehicleList;
    public List<WyzUser> wyzUserList;
    public List<AssignedInteraction> assignedInteractionsList;
    public List<Service> serviceList;
    
    public List<Insurance> insuranceList;
    public List<SRDisposition> srdispositionList;
    public List<CallInteraction> interactionCallList;  
    public List<ServiceAdvisor> serviceAdvisorList;
    public List<PickupDrop> pickupDropList;
    public List<Driver> driverList;

    public List<Driver> getDriverList() {
        return driverList;
    }

    public void setDriverList(List<Driver> driverList) {
        this.driverList = driverList;
    }

    public List<PickupDrop> getPickupDropList() {
        return pickupDropList;
    }

    public void setPickupDropList(List<PickupDrop> pickupDropList) {
        this.pickupDropList = pickupDropList;
    }

    public List<ServiceAdvisor> getServiceAdvisorList() {
        return serviceAdvisorList;
    }

    public void setServiceAdvisorList(List<ServiceAdvisor> serviceAdvisorList) {
        this.serviceAdvisorList = serviceAdvisorList;
    }
	
	public List<ServiceBooked> serviceBookedList;
    public List<Workshop> workshopList;

    public List<Workshop> getWorkshopList() {
        return workshopList;
    }

    public void setWorkshopList(List<Workshop> workshopList) {
        this.workshopList = workshopList;
    }
        
        
    public List<WyzUser> getWyzUserList() {
        return wyzUserList;
    }

    public void setWyzUserList(List<WyzUser> wyzUserList) {
        this.wyzUserList = wyzUserList;
    }

    public List<AssignedInteraction> getAssignedInteractionsList() {
        return assignedInteractionsList;
    }

    public void setAssignedInteractionsList(List<AssignedInteraction> assignedInteractionsList) {
        this.assignedInteractionsList = assignedInteractionsList;
    }
    
    
    
    public List<ServiceBooked> getServiceBookedList() {
        return serviceBookedList;
    }

    public void setServiceBookedList(List<ServiceBooked> serviceBookedList) {
        this.serviceBookedList = serviceBookedList;
    }
    
     public List<Insurance> getInsuranceList() {
        return insuranceList;
    }

    public void setInsuranceList(List<Insurance> insuranceList) {
        this.insuranceList = insuranceList;
    }

    public List<Customer> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(List<Customer> customerList) {
        this.customerList = customerList;
    }

    public List<Vehicle> getVehicleList() {
        return vehicleList;
    }

    public void setVehicleList(List<Vehicle> vehicleList) {
        this.vehicleList = vehicleList;
    }

    public List<SRDisposition> getSrdispositionList() {
        return srdispositionList;
    }

    public void setSrdispositionList(List<SRDisposition> srdispositionList) {
        this.srdispositionList = srdispositionList;
    }

    public List<CallInteraction> getInteractionCallList() {
        return interactionCallList;
    }

    public void setInteractionCallList(List<CallInteraction> interactionCallList) {
        this.interactionCallList = interactionCallList;
    }

    public List<Service> getServiceList() {
        return serviceList;
    }

    public void setServiceList(List<Service> serviceList) {
        this.serviceList = serviceList;
    }

    public List<CustomerDataOnTabLoad> getCustomerLoadList() {
        return customerLoadList;
    }

    public void setCustomerLoadList(List<CustomerDataOnTabLoad> customerLoadList) {
        this.customerLoadList = customerLoadList;
    }

    public List<VehicleDataOnTabLoad> getVehicleLoadList() {
        return vehicleLoadList;
    }

    public void setVehicleLoadList(List<VehicleDataOnTabLoad> vehicleLoadList) {
        this.vehicleLoadList = vehicleLoadList;
    }

    public List<WyzUserDataOnTabLoad> getWyzUserLoadList() {
        return wyzUserLoadList;
    }

    public void setWyzUserLoadList(List<WyzUserDataOnTabLoad> wyzUserLoadList) {
        this.wyzUserLoadList = wyzUserLoadList;
    }

    public List<AssignedInteractionDataOnTabLoad> getAssignedInteractionLoadList() {
        return assignedInteractionLoadList;
    }

    public void setAssignedInteractionLoadList(List<AssignedInteractionDataOnTabLoad> assignedInteractionLoadList) {
        this.assignedInteractionLoadList = assignedInteractionLoadList;
    }

    public List<ServiceDataOnTabLoad> getServiceLoadList() {
        return serviceLoadList;
    }

    public void setServiceLoadList(List<ServiceDataOnTabLoad> serviceLoadList) {
        this.serviceLoadList = serviceLoadList;
    }

    public List<InsuranceDataOnTabLoad> getInsuranceLoadList() {
        return insuranceLoadList;
    }

    public void setInsuranceLoadList(List<InsuranceDataOnTabLoad> insuranceLoadList) {
        this.insuranceLoadList = insuranceLoadList;
    }

    public List<SRDispositionDataOnTabLoad> getSrdispositionLoadList() {
        return srdispositionLoadList;
    }

    public void setSrdispositionLoadList(List<SRDispositionDataOnTabLoad> srdispositionLoadList) {
        this.srdispositionLoadList = srdispositionLoadList;
    }

    public List<CallInteractionDataOnTabLoad> getCallInteractionLoadList() {
        return callInteractionLoadList;
    }

    public void setCallInteractionLoadList(List<CallInteractionDataOnTabLoad> callInteractionLoadList) {
        this.callInteractionLoadList = callInteractionLoadList;
    }

    public List<ServiceAdvisorDataOnTabLoad> getServiceAdvisorLoadList() {
        return serviceAdvisorLoadList;
    }

    public void setServiceAdvisorLoadList(List<ServiceAdvisorDataOnTabLoad> serviceAdvisorLoadList) {
        this.serviceAdvisorLoadList = serviceAdvisorLoadList;
    }

    public List<PickupDropDataOnTabLoad> getPickupDropLoadList() {
        return pickupDropLoadList;
    }

    public void setPickupDropLoadList(List<PickupDropDataOnTabLoad> pickupDropLoadList) {
        this.pickupDropLoadList = pickupDropLoadList;
    }

    public List<DriverDataOnTabLoad> getDriverLoadList() {
        return driverLoadList;
    }

    public void setDriverLoadList(List<DriverDataOnTabLoad> driverLoadList) {
        this.driverLoadList = driverLoadList;
    }

    public List<ServicebookedDataOnTabLoad> getServicebookedLoadList() {
        return servicebookedLoadList;
    }

    public void setServicebookedLoadList(List<ServicebookedDataOnTabLoad> servicebookedLoadList) {
        this.servicebookedLoadList = servicebookedLoadList;
    }
    
    
}
