package controllers.webmodels;

public class AssignedListForCRE {

	public String serviceType;
	public Long psfinteraction_id;
	public String customer_name;
	public String Mobile_number;
	public String vehicle_RegNo;
	public String variant;
	public String veh_model;
	public Long customer_id;
	public Long vehicle_vehicle_id;
	public Long service_id;
	public String nextservicedate;
	public String category;
	public Long assigninteraction_id;

	public AssignedListForCRE(String serviceType, Long psfinteraction_id, String customer_name, String mobile_number,
			String vehicle_RegNo, String variant, String veh_model, Long customer_id, Long vehicle_vehicle_id,
			Long service_id, String nextservicedate, String category, Long assigninteraction_id) {
		super();
		this.serviceType = serviceType;
		this.psfinteraction_id = psfinteraction_id;
		this.customer_name = customer_name;
		this.Mobile_number = mobile_number;
		this.vehicle_RegNo = vehicle_RegNo;
		this.variant = variant;
		this.veh_model = veh_model;
		this.customer_id = customer_id;
		this.vehicle_vehicle_id = vehicle_vehicle_id;
		this.service_id = service_id;
		this.nextservicedate = nextservicedate;
		this.category = category;
		this.assigninteraction_id = assigninteraction_id;
	}

	public AssignedListForCRE() {
		super();
		this.serviceType = "";
		this.psfinteraction_id = (long) 0;
		this.customer_name = "";
		this.Mobile_number = "";
		this.vehicle_RegNo = "";
		this.variant = "";
		this.veh_model = "";
		this.customer_id = (long) 0;
		this.vehicle_vehicle_id = (long) 0;
		this.service_id = (long) 0;
		this.nextservicedate = "";
		this.category = "";
		this.assigninteraction_id = (long) 0;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public Long getPsfinteraction_id() {
		return psfinteraction_id;
	}

	public void setPsfinteraction_id(Long psfinteraction_id) {
		this.psfinteraction_id = psfinteraction_id;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public String getMobile_number() {
		return Mobile_number;
	}

	public void setMobile_number(String mobile_number) {
		Mobile_number = mobile_number;
	}

	public String getVehicle_RegNo() {
		return vehicle_RegNo;
	}

	public void setVehicle_RegNo(String vehicle_RegNo) {
		this.vehicle_RegNo = vehicle_RegNo;
	}

	public String getVariant() {
		return variant;
	}

	public void setVariant(String variant) {
		this.variant = variant;
	}

	public String getVeh_model() {
		return veh_model;
	}

	public void setVeh_model(String veh_model) {
		this.veh_model = veh_model;
	}

	public Long getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(Long customer_id) {
		this.customer_id = customer_id;
	}

	public Long getVehicle_vehicle_id() {
		return vehicle_vehicle_id;
	}

	public void setVehicle_vehicle_id(Long vehicle_vehicle_id) {
		this.vehicle_vehicle_id = vehicle_vehicle_id;
	}

	public Long getService_id() {
		return service_id;
	}

	public void setService_id(Long service_id) {
		this.service_id = service_id;
	}

	public String getNextservicedate() {
		return nextservicedate;
	}

	public void setNextservicedate(String nextservicedate) {
		this.nextservicedate = nextservicedate;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Long getAssigninteraction_id() {
		return assigninteraction_id;
	}

	public void setAssigninteraction_id(Long assigninteraction_id) {
		this.assigninteraction_id = assigninteraction_id;
	}

	public AssignedListForCRE(String serviceType, String customer_name, String mobile_number, String vehicle_RegNo,
			String variant, String veh_model, Long customer_id, Long vehicle_vehicle_id, String nextservicedate,
			String category, Long assigninteraction_id) {
		super();
		this.serviceType = serviceType;
		this.customer_name = customer_name;
		this.Mobile_number = mobile_number;
		this.vehicle_RegNo = vehicle_RegNo;
		this.variant = variant;
		this.veh_model = veh_model;
		this.customer_id = customer_id;
		this.vehicle_vehicle_id = vehicle_vehicle_id;
		this.nextservicedate = nextservicedate;
		this.category = category;
		this.assigninteraction_id = assigninteraction_id;
	}

}
