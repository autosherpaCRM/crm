/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.webmodels;

import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author W-885
 */
public class VehicleDataOnTabLoad {
    
    public String vehicleRegNo;
    public String model;
    public String variant;
    public String color;
    @Temporal(TemporalType.DATE)
    public Date nextServicedate;
    public String nextServicetype;
    public String odometerReading;
    public String forecastLogic;
    public String category;
    
    
    public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	@Temporal(TemporalType.DATE)
    public Date saleDate;

    public String getForecastLogic() {
        return forecastLogic;
    }

    public void setForecastLogic(String forecastLogic) {
        this.forecastLogic = forecastLogic;
    }
    
    
    public Date getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(Date saleDate) {
        this.saleDate = saleDate;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
    
    
    
    public String getOdometerReading() {
        return odometerReading;
    }

    public void setOdometerReading(String odometerReading) {
        this.odometerReading = odometerReading;
    }
    
    

    public String getNextServicetype() {
        return nextServicetype;
    }

    public void setNextServicetype(String nextServicetype) {
        this.nextServicetype = nextServicetype;
    }
    

    public String getVehicleRegNo() {
        return vehicleRegNo;
    }

    public void setVehicleRegNo(String vehicleRegNo) {
        this.vehicleRegNo = vehicleRegNo;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getVariant() {
        return variant;
    }

    public void setVariant(String variant) {
        this.variant = variant;
    }

    public Date getNextServicedate() {
        return nextServicedate;
    }

    public void setNextServicedate(Date nextServicedate) {
        this.nextServicedate = nextServicedate;
    }
    
}
