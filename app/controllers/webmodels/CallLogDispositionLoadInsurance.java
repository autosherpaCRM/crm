package controllers.webmodels;

import java.util.Date;

public class CallLogDispositionLoadInsurance {

	private long callinteraction_id;
	private String customer_name;
	//private String mobile_number;
	private String vehicle_RegNo;
	private Date duedate;
	private String dispoDate;
	private String renewalType;
	//private String last_disposition;
	private String dispoTime;
	private String appointmentToTime;
	private String reason;
	private long vehicle_id;
	private long customer_id;
	private long no_of_attempts;
	private String campaignName;
	private Date call_date;
	private String Mobile_number;
	private Date appointmentDate;
	private String appointmentTime;
	private String insurance_agent;
	private String Last_disposition;
	private String followUpDate;
	private String followUpTime;
	private Date policyDueDate;
	private String appointmentstatus;
	private long droppedCount;

	public CallLogDispositionLoadInsurance(String campaignName, Date call_date, String customer_name,
			String Mobile_number, String vehicle_RegNo, Date appointmentDate, String appointmentTime,
			String renewalType, String appointmentstatus, String insurance_agent, String Last_disposition,
			String reason, String followUpDate, String followUpTime, Date policyDueDate,
			long vehicle_id, long customer_id) {

		this.campaignName = campaignName;
		this.call_date = call_date;
		this.customer_name = customer_name;
		this.Mobile_number = Mobile_number;
		this.vehicle_RegNo = vehicle_RegNo;
		this.appointmentDate = appointmentDate;
		this.appointmentTime = appointmentTime;
		this.renewalType = renewalType;
		this.appointmentstatus = appointmentstatus;
		this.insurance_agent = insurance_agent;
		this.Last_disposition = Last_disposition;
		this.reason = reason;
		this.followUpDate = followUpDate;
		this.followUpTime = followUpTime;
		this.policyDueDate = policyDueDate;
		this.vehicle_id = vehicle_id;
		this.customer_id = customer_id;

	}

	public CallLogDispositionLoadInsurance(long droppedCount, String campaignName, Date call_date, String customer_name,
			String Mobile_number, String vehicle_RegNo, Date appointmentDate, String appointmentTime,
			String renewalType, String appointmentstatus, String insurance_agent, String Last_disposition,
			String reason, String followUpDate, String followUpTime, Date policyDueDate, long vehicle_id,
			long customer_id) {
		this.droppedCount = droppedCount;
		this.campaignName = campaignName;
		this.call_date = call_date;
		this.customer_name = customer_name;
		this.Mobile_number = Mobile_number;
		this.vehicle_RegNo = vehicle_RegNo;
		this.appointmentDate = appointmentDate;
		this.appointmentTime = appointmentTime;
		this.renewalType = renewalType;
		this.appointmentstatus = appointmentstatus;
		this.insurance_agent = insurance_agent;
		this.Last_disposition = Last_disposition;
		this.reason = reason;
		this.followUpDate = followUpDate;
		this.followUpTime = followUpTime;
		this.policyDueDate = policyDueDate;
		this.vehicle_id = vehicle_id;
		this.customer_id = customer_id;

	}

	public CallLogDispositionLoadInsurance() {
		super();
		this.campaignName = "";
		this.call_date = null;
		this.Mobile_number = "";
		this.appointmentDate = null;
		this.appointmentTime = "";
		this.insurance_agent = "";
		this.Last_disposition = "";
		this.followUpDate = "";
		this.followUpTime = "";
		this.policyDueDate = null;
		this.callinteraction_id = (long) 0;
		this.customer_name = "";
		this.Mobile_number = "";
		this.vehicle_RegNo = "";
		this.duedate = null;
		this.dispoDate = "";
		this.renewalType = "";
		this.Last_disposition = "";
		this.dispoTime = "";
		this.appointmentToTime = "";
		this.reason = "";
		this.vehicle_id = (long) 0;
		this.customer_id = (long) 0;
		this.no_of_attempts = (long) 0;
		this.droppedCount = (long) 0;
	}

	public CallLogDispositionLoadInsurance(long callinteraction_id, String customer_name, String mobile_number,
			String vehicle_RegNo, Date duedate, String dispoDate, String renewalType, String last_disposition,
			String dispoTime, String appointmentToTime, String reason, long vehicle_id, long customer_id) {
		super();
		this.callinteraction_id = callinteraction_id;
		this.customer_name = customer_name;
		this.Mobile_number = mobile_number;
		this.vehicle_RegNo = vehicle_RegNo;
		this.duedate = duedate;
		this.dispoDate = dispoDate;
		this.renewalType = renewalType;
		this.Last_disposition = last_disposition;
		this.dispoTime = dispoTime;
		this.appointmentToTime = appointmentToTime;
		this.reason = reason;
		this.vehicle_id = vehicle_id;
		this.customer_id = customer_id;
	}

	public CallLogDispositionLoadInsurance(long callinteraction_id, String customer_name, String mobile_number,
			String vehicle_RegNo, Date duedate, String renewalType, String last_disposition, long vehicle_id,
			long customer_id, long no_of_attempts) {
		super();
		this.callinteraction_id = callinteraction_id;
		this.customer_name = customer_name;
		this.Mobile_number = mobile_number;
		this.vehicle_RegNo = vehicle_RegNo;
		this.duedate = duedate;
		this.renewalType = renewalType;
		this.Last_disposition = last_disposition;
		this.vehicle_id = vehicle_id;
		this.customer_id = customer_id;
		this.no_of_attempts = no_of_attempts;
	}

	public String getCampaignName() {
		return campaignName;
	}

	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	public Date getCall_date() {
		return call_date;
	}

	public void setCall_date(Date call_date) {
		this.call_date = call_date;
	}

	public Date getAppointmentDate() {
		return appointmentDate;
	}

	public void setAppointmentDate(Date appointmentDate) {
		this.appointmentDate = appointmentDate;
	}

	public String getAppointmentTime() {
		return appointmentTime;
	}

	public void setAppointmentTime(String appointmentTime) {
		this.appointmentTime = appointmentTime;
	}

	public String getInsurance_agent() {
		return insurance_agent;
	}

	public void setInsurance_agent(String insurance_agent) {
		this.insurance_agent = insurance_agent;
	}

	public String getFollowUpDate() {
		return followUpDate;
	}

	public void setFollowUpDate(String followUpDate) {
		this.followUpDate = followUpDate;
	}

	public String getFollowUpTime() {
		return followUpTime;
	}

	public void setFollowUpTime(String followUpTime) {
		this.followUpTime = followUpTime;
	}

	public Date getPolicyDueDate() {
		return policyDueDate;
	}

	public void setPolicyDueDate(Date policyDueDate) {
		this.policyDueDate = policyDueDate;
	}

	public long getCallinteraction_id() {
		return callinteraction_id;
	}

	public void setCallinteraction_id(long callinteraction_id) {
		this.callinteraction_id = callinteraction_id;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public String getMobile_number() {
		return Mobile_number;
	}

	public void setMobile_number(String mobile_number) {
		this.Mobile_number = mobile_number;
	}

	public String getVehicle_RegNo() {
		return vehicle_RegNo;
	}

	public void setVehicle_RegNo(String vehicle_RegNo) {
		this.vehicle_RegNo = vehicle_RegNo;
	}

	public Date getDuedate() {
		return duedate;
	}

	public void setDuedate(Date duedate) {
		this.duedate = duedate;
	}

	public String getDispoDate() {
		return dispoDate;
	}

	public void setDispoDate(String dispoDate) {
		this.dispoDate = dispoDate;
	}

	public String getRenewalType() {
		return renewalType;
	}

	public void setRenewalType(String renewalType) {
		this.renewalType = renewalType;
	}

	public String getLast_disposition() {
		return Last_disposition;
	}

	public void setLast_disposition(String last_disposition) {
		this.Last_disposition = last_disposition;
	}

	public String getDispoTime() {
		return dispoTime;
	}

	public void setDispoTime(String dispoTime) {
		this.dispoTime = dispoTime;
	}

	public String getAppointmentToTime() {
		return appointmentToTime;
	}

	public void setAppointmentToTime(String appointmentToTime) {
		this.appointmentToTime = appointmentToTime;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public long getVehicle_id() {
		return vehicle_id;
	}

	public void setVehicle_id(long vehicle_id) {
		this.vehicle_id = vehicle_id;
	}

	public long getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(long customer_id) {
		this.customer_id = customer_id;
	}

	public long getNo_of_attempts() {
		return no_of_attempts;
	}

	public void setNo_of_attempts(long no_of_attempts) {
		this.no_of_attempts = no_of_attempts;
	}

	public String getAppointmentstatus() {
		return appointmentstatus;
	}

	public void setAppointmentstatus(String appointmentstatus) {
		this.appointmentstatus = appointmentstatus;
	}

	public long getDroppedCount() {
		return droppedCount;
	}

	public void setDroppedCount(long droppedCount) {
		this.droppedCount = droppedCount;
	}
	
	
	

}
