package controllers;


import org.pac4j.play.PlayWebContext;
import org.pac4j.play.store.PlaySessionStore;
import org.pac4j.core.profile.ProfileManager;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import javax.inject.Inject;
import javax.sql.DataSource;

import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.model.SharedStringsTable;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.pac4j.core.profile.CommonProfile;

import org.springframework.stereotype.Controller;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import models.Location;
import models.UploadData;
import models.Workshop;
import play.Logger;
import play.data.Form;
import play.db.DB;
import play.libs.F;
import play.libs.F.Promise;
import play.mvc.Result;
import play.mvc.Http.MultipartFormData;
import repositories.CallInteractionsRepository;
import repositories.ExcelUploadRepo;

import utils.InsuranceExcel;
import utils.SheetHandler;
import views.html.insuranceHistroyUploadPage;

@Controller
public class InsuranceHistoryController extends play.mvc.Controller{
	
	Logger.ALogger logger = play.Logger.of("application");
	
	
	private final ExcelUploadRepo excelRepo;
	private final CallInteractionsRepository call_int_repo;
	private PlaySessionStore playSessionStore;
	@Inject
	public InsuranceHistoryController(ExcelUploadRepo excelRepository,CallInteractionsRepository callinteractionRepo,
			org.pac4j.core.config.Config config,PlaySessionStore playsession) {
		
		
		this.excelRepo=excelRepository;
		this.call_int_repo=callinteractionRepo;
		config = (org.pac4j.core.config.Config) config;
		playSessionStore=playsession;
	}
	
	private CommonProfile getUserProfile() {
		final PlayWebContext context = new PlayWebContext(ctx(), playSessionStore);
		final ProfileManager<CommonProfile> profileManager = new ProfileManager(context);
		logger.debug("............About to obtain the profiles..........");
		List<CommonProfile> profiles = profileManager.getAll(true);
		logger.debug("..............Obtained profiles........." + profiles);
		return profiles.get(0);
	}
	
	private long getUserLoginId() {
		long user_Id = (long) getUserProfile().getAttribute("USER_ID");

		return user_Id;

	}
	
	private String getUserLogindealerCode() {
		String dealer_Code = (String) getUserProfile().getAttribute("DEALER_CODE");

		return dealer_Code;

	}

	private String getUserLoginName() {
		String userloginname = getUserProfile().getId();

		return userloginname;

	}
	
	private String getFileExtension(String filename) {
		try {
			return filename.substring(filename.lastIndexOf(".") + 1);
		} catch (Exception e) {
			return "";
		}
	}

	
	public Result uploadHistoryViewPage(){
		
		List<Location> locationList = call_int_repo.getLocationList();
		
		return ok(insuranceHistroyUploadPage.render(locationList,getUserLogindealerCode(),getUserLoginName()));
	}
	
	public Result uploadExcelDataHistory() {

		MultipartFormData body = request().body().asMultipartFormData();
		MultipartFormData.FilePart profile = body.getFile("uploadfiles");
		File file = (File)profile.getFile();
		String filename = profile.getFilename();
		logger.info("filename new one" + filename);

		String fileExt = getFileExtension(filename);
		String result = filename.substring(0, filename.lastIndexOf("."));
		String fileNameAfterTime = new SimpleDateFormat("yyyyMMddhhmm'.xlsx'").format(new Date());

		String uploadIdAfterTime = new SimpleDateFormat("yyyyMMddhhmm").format(new Date());

		String finalUploadId = "U".concat(uploadIdAfterTime);
		String finalName = result.concat(fileNameAfterTime);
		logger.info("file concatinate " + finalName);
		logger.info("finalUploadId for excel sheet is : " + finalUploadId);

		if (fileExt.equalsIgnoreCase("xlsx")) {
			logger.info("upload excel sheet");
			Form<UploadData> upload_data = Form.form(UploadData.class).bindFromRequest();
			UploadData uploadData = upload_data.get();

			String sheetname = uploadData.getSheetName();
			String dataTypefile = uploadData.getDataType();
			
			Form<Location> loc_upload_data = Form.form(Location.class).bindFromRequest();
			Location uploadData_loc = loc_upload_data.get();

			String locationdata = uploadData_loc.getName();

			Form<Workshop> loc_upload_work = Form.form(Workshop.class).bindFromRequest();
			Workshop uploadData_work = loc_upload_work.get();

			Long workshopId = uploadData_work.getId();
			
			com.typesafe.config.Config configuration = com.typesafe.config.ConfigFactory.load();
			String getDirectory = configuration.getString("app.filepath");
			logger.info("directory name" + getDirectory);
			String resultName = getDirectory.concat(finalName);
			logger.info("result name" + resultName);
			boolean results = file.renameTo(new File(getDirectory + finalName));
			logger.info("boolean remane" + results);
			logger.info("I am here............");
			logger.info("data type file" + dataTypefile);

			UploadData insertInUpload = excelRepo.setRequiredFieldsForUploadInsurance(getUserLoginId(), sheetname,
					 locationdata, workshopId, finalName, finalUploadId);

							
				ArrayList<HashMap> mappedDataInsurance=getInsuranceMapingData();
				
				HashMap<Integer, String> fieldsOfObjectInsurance = mappedDataInsurance.get(0);
				HashMap<Integer, String> excelColumnNamesInsurance = mappedDataInsurance.get(1);
				
				HashMap<String, String> formDataInsurance = new HashMap<String, String>();

				
				
				formDataInsurance.put("dataType", dataTypefile);
				formDataInsurance.put("uploadId", insertInUpload.getUpload_id());
				formDataInsurance.put("modelType", "utils.InsuranceExcel");

				InsuranceExcel pojoInsurance = new InsuranceExcel();
				try {
					logger.info("Before exceltoDataBase method");
					
					excelToDataBase(resultName, fieldsOfObjectInsurance, excelColumnNamesInsurance,
							formDataInsurance, pojoInsurance, insertInUpload);
					logger.info("After exceltoDataBase method");
				} catch (IOException e) {
					logger.error("Error while reading the file", e);
				} catch (OpenXML4JException e) {
					logger.error("XML exception", e);
				} catch (SAXException e) {
					logger.error("SAX Parser exception", e);
				}

				
			}

			

			logger.info("Exiting the controller");
			
			
			return ok("done");

	}

	private void excelToDataBase(String fileName, HashMap modelFields, HashMap excelHeaders,
			HashMap<String, String> additonalParameters, Object uploadModel, UploadData insertInUpload)
			throws IOException, OpenXML4JException, SAXException {

		HashMap<Date, Integer> uploadSuccessRecord = new HashMap<Date, Integer>();
		HashMap<Date, Integer> uploadRejectedRecord = new HashMap<Date, Integer>();

		// Hashmap Key value :- will be used to match the Object Field,Data Type
		// and Excel Headers.
		// additonalParameters :- Data's which need to post from Form.(Optinal);

		List<HashMap> uploadResultFields = new ArrayList<HashMap>();

		List<HashMap> mappingFields = new ArrayList<HashMap>();
		mappingFields.add(modelFields); // Fields of the Object uploadModel
		// mappingFields.add(dataType); // Data Type of the Object Fields
		mappingFields.add(excelHeaders); // Matching Excel Headers for the

		// Object Fields

		logger.info("controller mappingFields.size : " + mappingFields.size());
		// final String fileName = fileName;
		/*
		 * Opens a package (archive / xlsx file) with read / write permissions.
		 * It is also possible to access it read only, which should be the first
		 * choice for read operations in case the file is already accessed by
		 * another user. To open read only provide an InputStream instead of a
		 * file path.
		 */
		InputStream input = new FileInputStream(fileName);

		XSSFWorkbook wb = new XSSFWorkbook(input);
		XSSFSheet sheet = wb.getSheetAt(0);
		int row_count = sheet.getPhysicalNumberOfRows();

		// String upload_id = additonalParameters.get("uploadId");
		// long upload_id_concver = Integer.parseInt(upload_id);

		// exlRepo.updateTherowCountOfExcelsheet(row_count,upload_id_concver);

		logger.info("controller row_count of excel sheet : " + row_count);

		input.close();

		InputStream input1 = new FileInputStream(fileName);
		OPCPackage pkg = OPCPackage.open(input1);
		XSSFReader r = new XSSFReader(pkg);

		/*
		 * Read the sharedStrings.xml from an xlsx file into an object
		 * representation.
		 */
		SharedStringsTable sst = r.getSharedStringsTable();

		/*
		 * Hand a read SharedStringsTable for further reference to the SAXParser
		 * and the underlying ContentHandler.
		 */
		XMLReader parser = fetchSheetParser(sst, mappingFields, additonalParameters, uploadModel, uploadSuccessRecord,
				uploadRejectedRecord);

		/*
		 * O To look up the Sheet Name / Sheet Order / rID, you need to process
		 * the core Workbook stream. Normally it's of the form rId# or rSheet#
		 * 
		 * Great! How do I know, if it is rSheet or rId? Thanks Microsoft.
		 * Anyhow, let's carry on with the noise.
		 * 
		 * I reference the third sheet from the left since the index starts at
		 * one.
		 */
		InputStream sheet1 = r.getSheet("rId1");
		InputSource sheetSource = new InputSource(sheet1);

		/*
		 * Run through a Sheet using a window of several XML tags instead of
		 * attempting to read the whole file into RAM at once. Leaves the
		 * handling of file content to the ContentHandler, which is in this case
		 * the nested class SheetHandler.
		 */
		logger.info("controller Before Parse");
		parser.parse(sheetSource);
		mappingFields.clear();
		logger.info(" controller After Parse");

		/*
		 * Close the underlying InputStream for a Sheet XML.
		 */
		sheet1.close();

		Map<Date, Integer> map = uploadRejectedRecord;

		for (Map.Entry<Date, Integer> entrydata : map.entrySet()) {
			// logger.info("add data to String array");
			// System.out.println("Key for Rejected is = " + entrydata.getKey()
			// + ", Value = " + entrydata.getValue());

			insertInUpload.setId(0);
			if (uploadSuccessRecord.containsKey(entrydata.getKey())) {

			} else {

				logger.info("22");
				insertInUpload.setRecordedDate(entrydata.getKey());
				insertInUpload.setSuccessRecords(0);
				insertInUpload.setRejectedRecords(entrydata.getValue());

				excelRepo.addTheUplodedExcelData(insertInUpload);
			}

		}

		Map<Date, Integer> mapData = uploadSuccessRecord;

		for (Map.Entry<Date, Integer> entrydata1 : mapData.entrySet()) {

			insertInUpload.setId(0);
			// logger.info("add data to String array");
			// System.out.println("Key for Success is = " + entrydata1.getKey()
			// + ", Value = " + entrydata1.getValue());

			if (uploadRejectedRecord.containsKey(entrydata1.getKey())) {

				insertInUpload.setRecordedDate(entrydata1.getKey());
				insertInUpload.setSuccessRecords(entrydata1.getValue());

				Object rejectedRecordis = uploadRejectedRecord.get(entrydata1.getKey());

				insertInUpload.setRejectedRecords((int) rejectedRecordis);

				excelRepo.addTheUplodedExcelData(insertInUpload);

			} else {

				logger.info("33");
				insertInUpload.setRecordedDate(entrydata1.getKey());
				insertInUpload.setSuccessRecords(entrydata1.getValue());
				insertInUpload.setRejectedRecords(0);
				excelRepo.addTheUplodedExcelData(insertInUpload);

			}

		}
		
		insertDataByStoreProcedure(insertInUpload.getUpload_id());
		

	}

	private XMLReader fetchSheetParser(SharedStringsTable sst, List<HashMap> mappingFields, HashMap formData,
			Object uploadObject, HashMap uploadSuccessRecord, HashMap uploadRejectedRecord) throws SAXException {
		/*
		 * XMLReader parser = XMLReaderFactory
		 * .createXMLReader("org.apache.xerces.parsers.SAXParser");
		 */
		XMLReader parser = XMLReaderFactory.createXMLReader();
		logger.info("Controller Before Sheet Handler");
		ContentHandler handler = new SheetHandler(sst, mappingFields, formData, uploadObject, excelRepo,
				uploadSuccessRecord, uploadRejectedRecord);
		logger.info("Controller After Sheet Handler");
		parser.setContentHandler(handler);
		return parser;
	}
	
	
private ArrayList<HashMap> getInsuranceMapingData() {
		
		
		
		HashMap<Integer, String> fieldsOfObject = new HashMap<Integer, String>();
		HashMap<Integer, String> excelColumnNames = new HashMap<Integer, String>();

		fieldsOfObject.put(0, "customerName");
		excelColumnNames.put(0, "CUSTOMER NAME");
		
		fieldsOfObject.put(1, "contactNo");
		excelColumnNames.put(1, "CONTACT No.");
		
		fieldsOfObject.put(2, "email");
		excelColumnNames.put(2, "EMAIL");
		
		fieldsOfObject.put(3, "doa");
		excelColumnNames.put(3, "DOA");
		
		fieldsOfObject.put(4, "dob");
		excelColumnNames.put(4, "DOB");
		
		fieldsOfObject.put(5, "address");
		excelColumnNames.put(5, "ADDRESS");
		
		fieldsOfObject.put(6, "city");
		excelColumnNames.put(6, "CITY");
		
		fieldsOfObject.put(7, "state");
		excelColumnNames.put(7, "STATE");
		
		fieldsOfObject.put(8, "vehicleRegNo");
		excelColumnNames.put(8, "REGISTRATION No.");
		
		fieldsOfObject.put(9, "saleDate");
		excelColumnNames.put(9, "SALE DATE");
		
		fieldsOfObject.put(10, "engineNo");
		excelColumnNames.put(10, "ENGINE No.");
		
		fieldsOfObject.put(11, "chassisNo");
		excelColumnNames.put(11, "CHASSIS No.");
		
		fieldsOfObject.put(12, "model");
		excelColumnNames.put(12, "MODEL");
		
		fieldsOfObject.put(13, "variant");
		excelColumnNames.put(13, "VARIANT");
		
		fieldsOfObject.put(14, "color");
		excelColumnNames.put(14, "COLOR");
		
		fieldsOfObject.put(15, "lastMileage");
		excelColumnNames.put(15, "LAST MILEAGE");
		
		fieldsOfObject.put(16, "policyNo");
		excelColumnNames.put(16, "POLICY No.");
		
		fieldsOfObject.put(17, "coveragePeriod");
		excelColumnNames.put(17, "COVERAGE PERIOD");
		
		fieldsOfObject.put(18, "policyDueDate");
		excelColumnNames.put(18, "POLICY DUE DATE");
		
		fieldsOfObject.put(19, "insuranceCompanyName");
		excelColumnNames.put(19, "INSURANCE COMPANY");
		
		fieldsOfObject.put(20, "idv");
		excelColumnNames.put(20, "IDV");
		
		fieldsOfObject.put(21, "odPercentage");
		excelColumnNames.put(21, "OD %");
		
		fieldsOfObject.put(22, "odAmount");
		excelColumnNames.put(22, "OD (Rs.)");
		
		fieldsOfObject.put(23, "ncBPercentage");
		excelColumnNames.put(23, "NCB %");
		
		fieldsOfObject.put(24, "ncBAmount");
		excelColumnNames.put(24, "NCB (Rs.)");
		
		fieldsOfObject.put(25, "discountPercentage");
		excelColumnNames.put(25, "DISCOUNT%");
		
		fieldsOfObject.put(26, "ODpremium");
		excelColumnNames.put(26, "OD PREMIUM");
		
		fieldsOfObject.put(27, "liabilityPremium");
		excelColumnNames.put(27, "LIABILITY PREMIUM");
		
		fieldsOfObject.put(28, "add_ON_Premium");
		excelColumnNames.put(28, "ADD-ON PREMIUM");
		
		fieldsOfObject.put(29, "premiumAmountBeforeTax");
		excelColumnNames.put(29, "PREMIUM (Before Tax)");
		
		fieldsOfObject.put(30, "serviceTax");
		excelColumnNames.put(30, "TAX %");
		
		fieldsOfObject.put(31, "premiumAmountAfterTax");
		excelColumnNames.put(31, "PREMIUM (After Tax)");
		
		fieldsOfObject.put(32, "lastRenewalBy");
		excelColumnNames.put(32, "LAST RENEWAL BY");
		
		fieldsOfObject.put(33, "renewalType");
		excelColumnNames.put(33, "NEXT RENEWAL TYPE");
		
		
		ArrayList<HashMap> returnData = new ArrayList<HashMap>();
		returnData.add(fieldsOfObject);
		returnData.add(excelColumnNames);

		return returnData;
	}
	
	private void insertDataByStoreProcedure(String uploadId){		
	
			PreparedStatement cs = null;		
			Connection connection = null;
			try {
			DataSource datasource = DB.getDataSource();
			connection = datasource.getConnection();
			cs= connection.prepareStatement("call insurance_history_data_upload(?);");
			cs.setString(1, uploadId);
			cs.executeQuery();
			
				
			}
			catch (SQLException ex) {
				ex.printStackTrace();
				}
			finally
			 {
				 try {
					 if (cs != null) {
						 cs.close();
				 		}
				 	}
				 catch (Exception e) {
				 	};
				 try {
					 if (connection != null) {
						 if (!connection.isClosed()) {
							 connection.close();
						 }
					 	}
				 	}
				 catch (Exception e) {
				 };
			}
			
	}
}
