/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import static play.libs.Json.*;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.swing.JOptionPane;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.type.TypeFactory;

import org.pac4j.core.profile.CommonProfile;
import org.pac4j.core.profile.ProfileManager;
import org.pac4j.play.PlayWebContext;
import org.pac4j.play.java.Secure;
import org.pac4j.play.store.PlaySessionStore;
import org.springframework.stereotype.Controller;
import play.libs.Akka;
import play.libs.Json;
import play.mvc.Result;
import play.mvc.BodyParser;
import play.mvc.Http.MultipartFormData;
import controllers.webmodels.AssignedIntreactionNoCall;
import controllers.webmodels.WyzUserList;
import models.Campaign;
import models.ListingForm;
import models.Location;
import models.Role;
import models.Workshop;
import models.WyzUser;
import play.Logger.ALogger;
import play.data.Form;
import repositories.CallInfoRepository;
import repositories.CallInteractionsRepository;
import repositories.SMSTriggerRespository;
import repositories.SuperAdminRepository;
import repositories.WyzUserRepository;
import views.html.addMasterDataSuperAdmin;
import views.html.addMasterDataUsersBySuperAdmin;
import views.html.deleteNoCallAssignedInteraction;

@Controller
public class SuperAdminController extends play.mvc.Controller {

	ALogger logger = play.Logger.of("application");
	private final CallInfoRepository repo;
	private final CallInteractionsRepository call_int_repo;
	private final WyzUserRepository wyzRepo;
	private final SMSTriggerRespository sms_repo;
	private final SuperAdminRepository superAdminRepo;
	private PlaySessionStore playSessionStore;

	@Inject
	public SuperAdminController(CallInfoRepository repository, org.pac4j.core.config.Config config,
			CallInteractionsRepository interRepo, WyzUserRepository wyzRepository, SMSTriggerRespository smsRepository, SuperAdminRepository AdminRepo,
			PlaySessionStore plstore) {
		repo 			 = repository;
		call_int_repo 	 = interRepo;
		wyzRepo 		 = wyzRepository;
		sms_repo 		 = smsRepository;
		superAdminRepo 	 = AdminRepo;
		config 		     = (org.pac4j.core.config.Config) config;
		playSessionStore = plstore;

	}

	private CommonProfile getUserProfile() {
		final PlayWebContext context = new PlayWebContext(ctx(), playSessionStore);
		final ProfileManager<CommonProfile> profileManager = new ProfileManager(context);
		logger.debug("............About to obtain the profiles..........");
		List<CommonProfile> profiles = profileManager.getAll(true);
		logger.debug("..............Obtained profiles........." + profiles);
		return profiles.get(0);
	}

	private String getUserLogindealerCode() {
		String dealer_Code = (String) getUserProfile().getAttribute("DEALER_CODE");
		return dealer_Code;

	}

	private String getUserLoginName() {
		String userloginname = getUserProfile().getId();
		return userloginname;

	}

	private String getDealerName() {
		String dealerName = (String) getUserProfile().getAttribute("DEALER_NAME");
		return dealerName;

	}

	private String getOEMOfDealer() {

		String oemIS = (String) getUserProfile().getAttribute("OEM");

		return oemIS;

	}
	
	
	
	
	// Add location by Super admin method
	/**
	 *
	 * @return
	 */
	



@BodyParser.Of(BodyParser.Json.class)
public Result ajaxCallForAdduserinfo() throws IOException {

	JsonNode json_data = request().body().asJson();
	String jsonString = String.valueOf(json_data);
	String userId = json_data.findPath("id").textValue();
	//long userId = (long)json_data.findPath("userId").longValue();
	logger.info("json_data userId: " + userId);
	
	String phoneNumber = json_data.findPath("phoneNumber").textValue();
	String phoneIMEINo = json_data.findPath("phoneIMEINo").textValue();
	//logger.info("json_data userName: " + userName);
	/*ObjectMapper mapper = new ObjectMapper();
	DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	mapper.setDateFormat(df);

	CustomerJson customer_detailed = mapper.readValue(jsonString, CustomerJson.class);*/
	superAdminRepo.updateUserInfo(userId,phoneNumber,phoneIMEINo);

	return ok(toJson(0));
}

public Result updateUserAvailability() throws ParseException{
	/*JsonNode json_data = request().body().asJson();
	String jsonString = String.valueOf(json_data);
	String userId = json_data.findPath("wyz_id").textValue();
	String status1 = json_data.findPath("wyz_status").textValue();
	*/
	Map<String, String[]> parambody = request().body().asFormUrlEncoded();

	String userId = (parambody.get("id")[0]);
	String status1 = (parambody.get("status")[0]);
	//logger.info("availability controller"+userId+" "+status1);
	superAdminRepo.updateUserAvailability(userId,status1);
	return ok(toJson(0));
}
private List<WyzUser> getAllWyzUsers() {
	List<WyzUser> users = wyzRepo.getAllUsers("BALAJI");
	return users;
}
@Secure(clients = "FormClient")
public Result wyzUserData() {

	//List<WyzUser> wyzUsersList = getAllWyzUsers();

	Map<String, String[]> paramMap = request().queryString();
	ObjectNode result = Json.newObject();

	
	boolean allflag=false;
	
	String searchPattern = "";
	
	if (paramMap.get("search[value]") != null) {
		searchPattern = paramMap.get("search[value]")[0];
	}
	
	if(searchPattern.length() == 0){
		
		allflag=true;
	}
	
	long fromIndex = Long.valueOf(paramMap.get("start")[0]);
	long toIndex = Long.valueOf(paramMap.get("length")[0]);
	logger.info("from Index :"+fromIndex);
	logger.info("to index:"+toIndex);;
	long totalSize = getAllWyzUsers().size();
	long patternCount=0;
	logger.info("total size : "+totalSize);

	if (toIndex < 0) {
		toIndex = 10;

	}

	if (toIndex > totalSize) {
		toIndex = totalSize;
	}

	List<WyzUserList> wyzUserList = superAdminRepo.getWyzUserListData(fromIndex, toIndex,searchPattern);

	logger.info("return length of wyzUserList :" + wyzUserList.size());
	
	result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
	//result.put("recordsTotal", totalSize);
	
	
	if (!allflag) {
		patternCount = wyzUserList.size();
		logger.info("patternCount of wyzUserList : " + patternCount);

	}
	
	result.put("recordsTotal", totalSize);
	if (allflag) {
		result.put("recordsFiltered", totalSize);
	} else {
		result.put("recordsFiltered", patternCount);
	}
//	result.put("recordsFiltered", serviceTypeList.size());

	ArrayNode an = result.putArray("data");

	for (WyzUserList z : wyzUserList) {
		long id1 = z.getId();
		String name1 = z.getUserName();
		String phnNum = z.getPhoneNumber();
		String phnIMEInum = z.getPhoneIMEINo();
		boolean unvail = z.isUnAvailable();
		String avail;
		String editButton="<button type=\"button\" class=\"wyzuser_edit\" data-id="+id1+" data-user_name="+name1+" data-phn_num="+phnNum+" data-phn_imei="+phnIMEInum+">EDIT</button>";
		if(unvail == true) {
			avail = "<input type=\"checkbox\" id=\"onoffswitch\" class=\"onoffswitch\" data-id="+id1+" data-status="+unvail+" checked readonly>";
		}
		else {
			avail = "<input type=\"checkbox\" data-id="+id1+" class=\"onoffswitch\" data-status="+unvail+" id=\"onoffswitch\" readonly>";
		}
		ObjectNode row = Json.newObject();
		row.put("0", z.getId());
		row.put("1", z.getUserName());
		row.put("2", z.getRole());
		row.put("3", z.getPhoneNumber());
		row.put("4", z.getPhoneIMEINo());
		row.put("5", avail);
		row.put("6", editButton);
		an.add(row);
	}

	return ok(result);

}


	
	@Secure(clients = "FormClient")
	public Result AssignedInteractionDeletion() {
		List<Location> locationList = call_int_repo.getLocationList();
		List<Workshop> workshopList = call_int_repo.getWorkshopList();
		List<Campaign> campaignList = call_int_repo.getAllCampaignList();
		List<WyzUser> users = wyzRepo.getAllUsers("BALAJI");
		
		return ok(deleteNoCallAssignedInteraction.render(getOEMOfDealer(), getUserLogindealerCode(), getDealerName(), getUserLoginName(),users,locationList,workshopList,campaignList));
	}
	
	@SuppressWarnings("deprecation")
	@Secure(clients = "FormClient")
	public Result deleteAssignedInteractionNoCall() throws Exception{
		
		Form<ListingForm> listingForm = Form.form(ListingForm.class).bindFromRequest();
		ListingForm Listing_Form = listingForm.get();
		//logger.info(""+Listing_Form.getAssignedNoCallId().size());
		superAdminRepo.deleteAssignedNoCall(Listing_Form);
		/*JOptionPane.showMessageDialog(null, 
	            "No Call Made Assignments Deleted", 
	            "Done", 
	            JOptionPane.PLAIN_MESSAGE);*/
		return redirect(controllers.routes.SuperAdminController.AssignedInteractionDeletion());
		
	}
	
	@Secure(clients = "FormClient")
	public Result assignedInteractionData(long wyzuserId,String location, String moduleName,String campaignName,String fromDate,String toDate) throws ParseException {

		//List<WyzUser> wyzUsersList = getAllWyzUsers();

		Map<String, String[]> paramMap = request().queryString();
		ObjectNode result = Json.newObject();

		Date date1=new SimpleDateFormat("yyyy-MM-dd").parse(fromDate); 
		Date date2=new SimpleDateFormat("yyyy-MM-dd").parse(toDate); 
		List<AssignedIntreactionNoCall> assignedNoCallList = superAdminRepo.getAssignedInteractionListData(wyzuserId, location, moduleName, campaignName, date1, date2);

		logger.info("assignedNoCallList size " +assignedNoCallList.size());
		long toIndex = Long.valueOf(paramMap.get("length")[0]);
		
		long totalSize = assignedNoCallList.size();
		
		logger.info("total size : "+totalSize);

		if (toIndex < 0) {
			toIndex = 10;

		}

		if (toIndex > totalSize) {
			toIndex = totalSize;
		}

		result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
		result.put("recordsTotal", totalSize);
		result.put("recordsFiltered", totalSize);
		ArrayNode an = result.putArray("data");

		for (AssignedIntreactionNoCall a : assignedNoCallList) {
			long id1 = a.getNoCallAssignId();
			ObjectNode row = Json.newObject();
			row.put("0", a.getWyzUserName());
			row.put("1", a.getLocations());
			row.put("2", a.getCampaignName());
			row.put("3", a.getCustomerName());
			row.put("4", a.getVehicleId());
			row.put("5", a.getChassisNo());
			row.put("6", a.getModel());
			row.put("7", a.getVehicleRegNo());
			row.put("8", a.getUploadDate().toString());
			row.put("9","<input type=\"hidden\" name=\"assignedNoCallId[]\" value="+id1+">");
			an.add(row);
		}

		return ok(result);

	}
	
	@Secure(clients = "FormClient")
	public Result getListCampaignByModule(String selectedCampaigns) {

		logger.info("selected Campaign :"+selectedCampaigns);
		List<String> selectedCampaign = Arrays.asList(selectedCampaigns.split(","));
		List<String> campaign_full_list = new ArrayList<String>();;
		for(String cname :selectedCampaign) {
			logger.info(cname);
			List<String> campaign_list = superAdminRepo.getCampaignByType(cname);
			if(campaign_list == null) {
				continue;
			}
			campaign_full_list.addAll(campaign_list);
		}
		
		return ok(toJson(campaign_full_list));

	}
	
	@Secure(clients = "FormClient")
	public Result LocationBySuperAdmin() {
		List<Location> locationList = call_int_repo.getLocationList();
		List<Workshop> workshopList = call_int_repo.getWorkshopList();
		return ok(addMasterDataSuperAdmin.render(getOEMOfDealer(), getUserLogindealerCode(), getDealerName(), getUserLoginName(), locationList , workshopList));
	}
	
	
	@Secure(clients = "FormClient")
	public Result postLocationBySuperAdmin(){
		Form<Location> Location_data    = Form.form(Location.class).bindFromRequest();
		Location Location			    = Location_data.get();
		
		
	
		Form<Workshop> Workshop_data 	= Form.form(Workshop.class).bindFromRequest();
		Workshop Workshop 				= Workshop_data.get();
		
		
		superAdminRepo.addLocationdataBySuperAdmin(Location, Workshop, getUserLoginName());
		return redirect(controllers.routes.SuperAdminController.LocationBySuperAdmin());
	}
	
	private List<WyzUser> getCreManager() {
		List<WyzUser> creManager = wyzRepo.getUsersByRole("CREManager");
		return creManager;
	}
	
	
	
	
@Secure(clients = "FormClient")
public Result UsersBySuperAdmin(){
	List<Location> locationList = call_int_repo.getLocationList();
	List<Workshop> workshopList = call_int_repo.getWorkshopList();
	List<Role> roleList 		= call_int_repo.getRoles();
	return ok(addMasterDataUsersBySuperAdmin.render(getOEMOfDealer(), getUserLogindealerCode(), getDealerName(), getUserLoginName(),locationList,workshopList,roleList, getCreManager(),getAllWyzUsers()));
}



@Secure(clients = "FormClient")
public Result postUserBySuperAdmin(){
	Form<WyzUser> Wyzuser_data 		= Form.form(WyzUser.class).bindFromRequest();
	WyzUser WyzUser 				= Wyzuser_data.get();
	
	Form<Location> Location_data 	= Form.form(Location.class).bindFromRequest();
	Location Location 				= Location_data.get();
	
	Form<Workshop> workshop_data 	= Form.form(Workshop.class).bindFromRequest();
	Workshop Workshop 				= workshop_data.get();
	
	Form<Role> roleData 			= Form.form(Role.class).bindFromRequest();
	Role Role 						= roleData.get();
	
	Form<ListingForm> listingForm = Form.form(ListingForm.class).bindFromRequest();
	ListingForm Listing_Form = listingForm.get();
	
	//logger.info("inside controller:"+ Location.getName());
	superAdminRepo.addUsersBySuperAdmin(WyzUser , Location, Workshop ,Role,getUserLoginName(),getUserLogindealerCode(),Listing_Form);
	
	return redirect(controllers.routes.SuperAdminController.UsersBySuperAdmin());
}
	
	@Secure(clients = "FormClient")
	public Result checkIfUserExists(String uname){
		List<WyzUser> UsersList = superAdminRepo.getExistingWyzUsers(uname);
		
		long existcount=UsersList.size();
		return ok(toJson(existcount));
	}
	

}