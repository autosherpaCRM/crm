package controllers.errorhandlers;

import org.pac4j.core.context.HttpConstants;
import org.pac4j.play.PlayWebContext;
import org.pac4j.play.http.DefaultHttpActionAdapter;

import play.Logger.ALogger;
import play.mvc.Result;

import static play.mvc.Results.*;

public class DemoHttpActionAdapter extends DefaultHttpActionAdapter {

	public static ALogger logger = play.Logger.of("application");
	
    @Override
    public Result adapt(int code, PlayWebContext context) {
    	
    	logger.debug("..........HTTP Code............: "+code);
       if (code == HttpConstants.UNAUTHORIZED) {
        	
            return unauthorized(views.html.error401.render().toString()).as((HttpConstants.HTML_CONTENT_TYPE));
        } else if (code == HttpConstants.FORBIDDEN) {
            return forbidden(views.html.error403.render().toString()).as((HttpConstants.HTML_CONTENT_TYPE));
        } else {
            return super.adapt(code, context);
        }
    }
}
