package controllers;

import static play.libs.Json.toJson;
import static play.mvc.Results.ok;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.pac4j.core.profile.CommonProfile;
import org.pac4j.core.profile.ProfileManager;
import org.pac4j.play.PlayWebContext;
import org.pac4j.play.java.Secure;
import org.pac4j.play.store.PlaySessionStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import controllers.webmodels.InsuranceAgentIdName;
import play.Logger.ALogger;
import play.mvc.Result;
import repositories.AutoSelInsuranceAgentRepository;
import utils.WyzCRMCacheInsurance;

@Controller
public class AutoSelInsuranceAgentController extends play.mvc.Controller{
	
	ALogger logger = play.Logger.of("application");
	
	private final AutoSelInsuranceAgentRepository repo;
	private PlaySessionStore playSessionStore;

	

	private final WyzCRMCacheInsurance saCache;

	@Inject
	public AutoSelInsuranceAgentController(AutoSelInsuranceAgentRepository repository,WyzCRMCacheInsurance wyzsaCache,PlaySessionStore playses) {
		
		this.repo = repository;
		saCache=wyzsaCache;
		playSessionStore = playses;
	}
	
	@Secure(clients = "FormClient")
	public Result ajaxAutoInsurAgentSelection(String date_value) throws java.lang.Exception {
		
		logger.info(" ajaxAutoInsurAgentSelection : "+date_value);
		
		logger.info(" stringToDate(date_value) : "+stringToDate(date_value));
		
		List<InsuranceAgentIdName> sa = saCache.getInsuranceAgentRecomendation(stringToDate(date_value),getUserLogindealerCode());
		
		return ok(toJson(sa));
	}

	@Secure(clients = "FormClient")
	public Result ajaxAutoInsurAgentSelectionList(String preSaDetails) throws java.lang.Exception {

		logger.info("preSaDetails sa automatic selection : " + preSaDetails);
		if (!preSaDetails.equals("NA")) {
			InsuranceAgentIdName preSA = new InsuranceAgentIdName();
			preSA = getInsAgentObjectFromString(preSaDetails);
			saCache.getInsurAgentList(preSA.getDate(), preSA,getUserLogindealerCode());
		}
		return ok();
	}
	
	@Secure(clients = "FormClient")
	public Result ajaxupdateInsuAgentChange(String date_value, String preSaDetails, String newSaDetails)
			throws java.lang.Exception {
		if (preSaDetails.equals("NA") && newSaDetails.equals("NA")) {
			saCache.updateInsurAgentChange(stringToDate(date_value), null, null,getUserLogindealerCode());
		} else if (preSaDetails.equals("NA") && !newSaDetails.equals("NA")) {
			saCache.updateInsurAgentChange(stringToDate(date_value), null,
					getInsAgentObjectFromString(newSaDetails),getUserLogindealerCode());
		} else if (!preSaDetails.equals("NA") && newSaDetails.equals("NA")) {
			saCache.updateInsurAgentChange(stringToDate(date_value),
					getInsAgentObjectFromString(preSaDetails), null,getUserLogindealerCode());
		} else {
			saCache.updateInsurAgentChange(stringToDate(date_value),
					getInsAgentObjectFromString(preSaDetails), getInsAgentObjectFromString(newSaDetails),getUserLogindealerCode());
		}

		return ok();
	}
	
	
	private InsuranceAgentIdName getInsAgentObjectFromString(String saDetails) {
		InsuranceAgentIdName sa = new InsuranceAgentIdName();
		List<String> items = Arrays.asList(saDetails.split("\\s*,\\s*"));
		sa.setAdvisorId(Long.parseLong(items.get(0)));
		sa.setAdvisorName(items.get(1));
		sa.setPriority(Long.parseLong(items.get(2)));
		sa.setDate(stringToDate(items.get(3)));		
		return sa;
	}

	private Date stringToDate(String date_value) {
		SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		try {
			date = dmyFormat.parse(date_value);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return date;
	}
	
	private CommonProfile getUserProfile() {
		final PlayWebContext context = new PlayWebContext(ctx(), playSessionStore);
		final ProfileManager<CommonProfile> profileManager = new ProfileManager(context);
		logger.debug("............About to obtain the profiles..........");
		List<CommonProfile> profiles = profileManager.getAll(true);
		logger.debug("..............Obtained profiles........." + profiles);
		return profiles.get(0);
	}
	
	private String getUserLogindealerCode() {
		String dealer_Code = (String) getUserProfile().getAttribute("DEALER_CODE");

		return dealer_Code;

	}

}
