package controllers;

import static play.libs.Json.toJson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.pac4j.core.profile.CommonProfile;
import org.pac4j.core.profile.ProfileManager;
import org.pac4j.play.PlayWebContext;
import org.pac4j.play.java.Secure;
import org.pac4j.play.store.PlaySessionStore;
import org.springframework.stereotype.Controller;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import controllers.webmodels.AssignedListForCRE;
import controllers.webmodels.CampaignNamesAjaxLoadData;
import controllers.webmodels.ChangeAssignment;
import models.CallDispositionData;
import models.Campaign;
import models.Location;
import models.ServiceTypes;
import models.WyzUser;
import play.Logger.ALogger;
import play.libs.Json;
import play.mvc.Result;
import repositories.CallInteractionsRepository;
import repositories.ChangeAssignmentRepository;
import repositories.WyzUserRepository;
import views.html.assignmentFilterListPage;
import views.html.changeAssinedCalls;

@Controller
public class ChangeAssignmentController extends play.mvc.Controller {

	ALogger logger = play.Logger.of("application");
	private PlaySessionStore playSessionStore;
	private WyzUserRepository wyzRepo;
	private ChangeAssignmentRepository changeRepo;
	private CallInteractionsRepository call_int_repo;

	@Inject
	public ChangeAssignmentController(PlaySessionStore playSessionStore, WyzUserRepository wyzRepository,
			ChangeAssignmentRepository changeRepository, CallInteractionsRepository callinteractRepo) {

		this.playSessionStore = playSessionStore;
		wyzRepo = wyzRepository;
		changeRepo = changeRepository;
		call_int_repo = callinteractRepo;
	}

	private CommonProfile getUserProfile() {
		final PlayWebContext context = new PlayWebContext(ctx(), playSessionStore);
		final ProfileManager<CommonProfile> profileManager = new ProfileManager(context);
		logger.debug("............About to obtain the profiles..........");
		List<CommonProfile> profiles = profileManager.getAll(true);
		logger.debug("..............Obtained profiles........." + profiles);
		return profiles.get(0);
	}

	private String getUserLogindealerCode() {
		String dealer_Code = (String) getUserProfile().getAttribute("DEALER_CODE");

		return dealer_Code;

	}

	private String getUserLoginName() {
		String userloginname = getUserProfile().getId();

		return userloginname;

	}

	private String getDealerName() {
		String dealerName = (String) getUserProfile().getAttribute("DEALER_NAME");

		return dealerName;

	}

	private String getOEMOfDealer() {

		String oemIS = (String) getUserProfile().getAttribute("OEM");

		return oemIS;

	}

	@Secure(clients = "FormClient")
	public Result changeassignedCalls() {

		List<WyzUser> userlist = wyzRepo.getCREofCREManager(getUserLoginName(), getUserLogindealerCode());
		
		List<WyzUser> sorteduserlist=userlist.stream().sorted(Comparator.comparing(WyzUser::getUserName)).collect(Collectors.toList());;

		List<Campaign> camplist = wyzRepo.getCampaignList();
		List<Campaign> campaign_list = call_int_repo.getAllCampaignList();

		List<CallDispositionData> dispoList = wyzRepo.getDispositionList();
		//List<CallDispositionData> newlist = dispoList.subList(2, 10);

		return ok(changeAssinedCalls.render(sorteduserlist, dispoList, camplist, campaign_list,getUserLoginName(), getUserLogindealerCode()));

	}

	@Secure(clients = "FormClient")
	public Result postchangeassignedCalls() {

		Map<String, String[]> paramMap = request().queryString();

		Map<String, String[]> parambody = request().body().asFormUrlEncoded();

		//long campSelec = Long.valueOf(parambody.get("campSelec")[0]);
		
		
		
		String campSelec="";
		if ((parambody.get("campSelec")) != null) {
			if ((parambody.get("campSelec")[0]).equals("0")) {
				campSelec = "";
			} else if ((parambody.get("campSelec")[0]).equals("All")) {
				(parambody.get("campSelec")[0]) = "";
			} else {
				String[] dispositionArray= (parambody.get("campSelec"));
				int j = 0;
			    for (int i = 0; i < dispositionArray.length; i++) {
			        
			            if (j == 0)
			            	campSelec = dispositionArray[i];
			            else
			            	campSelec = campSelec + "," + dispositionArray[i];
			            
			            j++;
			    }
			}
		}		
		
		logger.info("campSelect::"+campSelec);
		String campSelecType =(parambody.get("campSelecType")[0]);
		String fromdate = (parambody.get("fromdate")[0]);
		String todate = (parambody.get("todate")[0]);
		String selected_dispositions = (parambody.get("selected_dispositions")[0]);
		String selected_cre = (parambody.get("selected_cres")[0]);

		logger.info(" campSelec " + campSelec + "fromdate : " + fromdate + " todate : " + todate
				+ " selected_dispositions :" + selected_dispositions + " selected_cre : " + selected_cre);

		ObjectNode result = Json.newObject();

		String searchPattern = "";
		boolean allflag = false;

		if (paramMap.get("search[value]") != null) {
			searchPattern = paramMap.get("search[value]")[0];
		}

		if (searchPattern.length() == 0) {
			allflag = true;
		}
		logger.info("searchPattern :" + searchPattern);
		long fromIndex = Long.valueOf(parambody.get("start")[0]);
		long toIndex = Long.valueOf(parambody.get("length")[0]);

		logger.info("fromIndex : " + fromIndex);
		logger.info("toIndex : " + toIndex);

		long totalSize = changeRepo.filteredAssignDataListCount(selected_cre, selected_dispositions, fromdate, todate,
				campSelec,campSelecType);
		logger.info("totalSize of assigned interaction : " + totalSize);

		if (toIndex < 0) {
			toIndex = 10;

		}

		if (toIndex > totalSize) {

			toIndex = totalSize;
		}
		long patternCount = 0;

		List<ChangeAssignment> assignList = changeRepo.filteredAssignDataList(selected_cre, selected_dispositions,
				fromdate, todate, campSelec,campSelecType, fromIndex, toIndex);

		if (!allflag) {
			patternCount = assignList.size();
			logger.info("patternCount of assignedList : " + patternCount);

		}

		result.put("draw", Integer.valueOf(parambody.get("draw")[0]));
		result.put("recordsTotal", totalSize);

		if (allflag) {
			result.put("recordsFiltered", totalSize);
		} else {
			result.put("recordsFiltered", patternCount);
		}
		ArrayNode an = result.putArray("data");

		for (ChangeAssignment c : assignList) {

			ObjectNode row = Json.newObject();

			String callIDandCamId = c.getId().toString() + "," + c.getCampaign_id();

			logger.info(" callIDandCamId " + callIDandCamId);

			String selectIndex = "<input type='checkbox'/ class = 'chcktbl'/ name='callId[]'/value=" + callIDandCamId
					+ ">";
			String campType = "<input type='text'/  style='display:none;'/ name='campainId[]'/value="
					+ c.getCampaign_id() + ">";
			String campaignName = changeRepo.getCampaignByID(c.getCampaign_id());
			String dispoName = "";
			if (c.getFinalDisposition_id().equals("0")) {

				dispoName = "Not yet Called";
			} else {

				dispoName = changeRepo.getDisositionById(Long.valueOf(c.getFinalDisposition_id()));
			}

			row.put("0", selectIndex);
			row.put("1", c.getCreName());
			row.put("2", c.getCustomer_name());
			row.put("3", c.getMobile_number());
			row.put("4", c.getVehicle_RegNo());
			row.put("5", dispoName);
			row.put("6", campaignName);
			row.put("7", campType);

			an.add(row);

		}
		return ok(result);
	}	
	@Secure(clients = "FormClient")
	public Result changeAssignedCallsToAgents() {

		Map<String, String[]> parambody = request().body().asFormUrlEncoded();
		String selected_cre = (parambody.get("selected_cres")[0]);
		String selected_camps = (parambody.get("selected_camps")[0]);
		String statusSelectAll = (parambody.get("selectAllStatus")[0]);

		logger.info("selectAllStatus : " + statusSelectAll);

		List<Long> newListcallIds = new ArrayList<Long>();
		List<Long> newListcampIds = new ArrayList<Long>();
		List<String> userlist = Arrays.asList(selected_cre.split(","));

		List<Long> newListuserlist = new ArrayList<Long>(userlist.size());
		for (String myInt : userlist) {
			newListuserlist.add(Long.valueOf(myInt));
		}

			List<String> callIds = Arrays.asList(selected_camps.split(","));

			for (int i = 0; i < callIds.size(); i += 2) {

				newListcallIds.add(Long.valueOf(callIds.get(i)));
				newListcampIds.add(Long.valueOf(callIds.get(i + 1)));

			}

			logger.info(" newListcallIds : " + newListcallIds.size() + " newListcampIds : " + newListcampIds.size()
					+ " newListuserlist : " + newListuserlist.size());

			changeRepo.changeAssignedDataCalls(newListcallIds, newListcampIds, newListuserlist);

		//}

		return ok(toJson(""));
	}

	@Secure(clients = "FormClient")
	public Result assignmentFilterList() {
		List<String> cresList = call_int_repo.getCREandServiceAdvOFMan(getUserLoginName(), getUserLogindealerCode());

		List<Campaign> campaign_list = call_int_repo.getAllCampaignList();
		List<Campaign> campaignList = call_int_repo.getCampaignNames();
		List<Location> locationList = call_int_repo.getLocationList();
		List<Campaign> campaignListPSF = call_int_repo.getCampaignNamesPSF();
		List<ServiceTypes> servicetypes = call_int_repo.getAllServiceTypeList();

		return ok(assignmentFilterListPage.render(cresList, getDealerName(), getUserLoginName(), campaign_list,
				locationList, campaignList, campaignListPSF, servicetypes));

	}

	@Secure(clients = "FormClient")
	public Result getAssignmentFilterListAjax() {

		Map<String, String[]> paramMap = request().queryString();

		Map<String, String[]> parambody = request().body().asFormUrlEncoded();

		String fromdate = (parambody.get("fromDate")[0]);
		String todate = (parambody.get("toDate")[0]);
		String customerCat = (parambody.get("customerCatgy")[0]);
		String serviceCat = (parambody.get("servicetype")[0]);
		String modelname= "";//(parambody.get("modelname")[0]);
		
		
		if (parambody.get("modelname") != null) {
			modelname = (parambody.get("modelname")[0]);
		}
		
		logger.info("modelname : "+modelname);
		long campaignNameCat = 0;
		long campaignNamePSF = 0;

		String campNameIs = (parambody.get("campName")[0]);
		String psfNameIs = (parambody.get("psfName")[0]);

		if (campNameIs != ("0")) {

			campaignNameCat = Long.valueOf(campNameIs);
		}

		if (psfNameIs != ("0")) {

			campaignNamePSF = Long.valueOf(psfNameIs);
		}

		// logger.info("campNameIs : "+campNameIs+" psfNameIs : "+psfNameIs);

		long campaignTypeCat = Long.valueOf(parambody.get("campaignTypeIs")[0]);

		String cityName = (parambody.get("city")[0]);

		String cityId = changeRepo.getLocationIdByName(cityName);

		String workshopName = (parambody.get("workshop")[0]);
		
		logger.info("view cityId : "+cityId+" workshopName : "+workshopName);

		// logger.info(" customerCat " + customerCat + "fromdate : " + fromdate
		// + " todate : " + todate
		// + " serviceCat :" + serviceCat + " campaignTypeCat : " +
		// campaignTypeCat);

		ObjectNode result = Json.newObject();

		String searchPattern = "";
		boolean allflag = false;

		if (paramMap.get("search[value]") != null) {
			searchPattern = paramMap.get("search[value]")[0];
		}

		if (searchPattern.length() == 0) {
			allflag = true;
		}
		logger.info("searchPattern :" + searchPattern);
		long fromIndex = Long.valueOf(parambody.get("start")[0]);
		long toIndex = Long.valueOf(parambody.get("length")[0]);

		logger.info("fromIndex : " + fromIndex);
		logger.info("toIndex : " + toIndex);

		SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date fromDateNew = new Date();
		Date toDateNew = new Date();
		try {
			fromDateNew = dmyFormat.parse(fromdate);
			toDateNew = dmyFormat.parse(todate);
		} catch (Exception e) {
			e.printStackTrace();
		}

		String selectedCampaignType = call_int_repo.getCampaignTypeById(campaignTypeCat);

		List<AssignedListForCRE> assign_data = new ArrayList<AssignedListForCRE>();

		logger.info("selectedCampaignType  is : " + selectedCampaignType);

		long totalSize = 0;
		logger.info("totalSize of assigned interaction : " + totalSize);

		if (selectedCampaignType.equals("PSF")) {

			 logger.info("PSf List for call assignment");

			assign_data = call_int_repo.getAssignListOfPSFByProcedureServer(fromDateNew, toDateNew, customerCat,
					serviceCat, campaignTypeCat, campaignNameCat, campaignNamePSF,modelname, fromIndex, toIndex,cityId,workshopName);

			totalSize = call_int_repo.getAssignListOfPSFByProcedureCount(fromDateNew, toDateNew, customerCat,
					serviceCat, campaignNamePSF,modelname,cityId,workshopName);
		} else if (selectedCampaignType.equals("Campaign")) {

			logger.info("Campaign List for call assignment"+campaignNameCat+" campaignNameCat : ");

			assign_data = call_int_repo.getAssignListOfCampaignAndSMRByProcedure(fromDateNew, toDateNew, customerCat,
					serviceCat, campaignNameCat, cityId, fromIndex, toIndex);

			totalSize = call_int_repo.getAssignListOfCampaignAndSMRByProcedureCount(fromDateNew, toDateNew, customerCat,
					serviceCat, campaignNameCat, cityId);
		} else {

			assign_data = call_int_repo.getAssignListOfCampaignAndSMRByProcedure(fromDateNew, toDateNew, customerCat,
					serviceCat, campaignTypeCat, cityId, fromIndex, toIndex);

			totalSize = call_int_repo.getAssignListOfCampaignAndSMRByProcedureCount(fromDateNew, toDateNew, customerCat,
					serviceCat, campaignTypeCat, cityId);
		}

		if (toIndex < 0) {
			toIndex = 10;

		}

		if (toIndex > totalSize) {

			toIndex = totalSize;
		}
		long patternCount = 0;

		List<AssignedListForCRE> assignList = assign_data;

		if (!allflag) {
			patternCount = assignList.size();
			logger.info("patternCount of assignedList : " + patternCount);

		}

		result.put("draw", Integer.valueOf(parambody.get("draw")[0]));
		result.put("recordsTotal", totalSize);

		if (allflag) {
			result.put("recordsFiltered", totalSize);
		} else {
			result.put("recordsFiltered", patternCount);
		}
		ArrayNode an = result.putArray("data");

		for (AssignedListForCRE c : assignList) {

			ObjectNode row = Json.newObject();

			row.put("0", c.getCustomer_name());
			row.put("1", c.getMobile_number());
			row.put("2", c.getVehicle_RegNo());
			row.put("3", c.getVeh_model());
			row.put("4", c.getVariant());
			row.put("5", c.getNextservicedate());

			an.add(row);

		}
		return ok(result);

	}

	public Result assignListToUserSelected() {

		Map<String, String[]> parambody = request().body().asFormUrlEncoded();

		String fromdate = (parambody.get("fromDate")[0]);
		String todate = (parambody.get("toDate")[0]);
		String customerCat = (parambody.get("customerCatgy")[0]);
		String serviceCat = (parambody.get("servicetype")[0]);
		String modelname = "";//(parambody.get("model")[0]);
		if (parambody.get("modelname") != null) {
			modelname = (parambody.get("modelname")[0]);
		}
		
		logger.info("modelname assignment : "+modelname);
		
		long campaignNameCat = 0;
		long campaignNamePSF = 0;

		String campNameIs = (parambody.get("campName")[0]);
		String psfNameIs = (parambody.get("psfName")[0]);

		if (campNameIs != ("0")) {

			campaignNameCat = Long.valueOf(campNameIs);
		}

		if (psfNameIs != ("0")) {

			campaignNamePSF = Long.valueOf(psfNameIs);
		}

		logger.info("campNameIs : " + campNameIs + " psfNameIs : " + psfNameIs);

		long campaignTypeCat = Long.valueOf(parambody.get("campaignTypeIs")[0]);

		String cityName = (parambody.get("city")[0]);

		String cityId = changeRepo.getLocationIdByName(cityName);

		String workshopName = (parambody.get("workshop")[0]);
		
		logger.info("cityId : "+cityId+" workshopName : "+workshopName);
		

		String selected_cre = (parambody.get("selected_cres")[0]);

		List<String> selectedList = Arrays.asList(selected_cre.split(","));

		String selectedCampaignType = call_int_repo.getCampaignTypeById(campaignTypeCat);

		List<AssignedListForCRE> scheduleData = new ArrayList<AssignedListForCRE>();

		logger.info("selectedCampaignType  is : " + selectedCampaignType);

		logger.info(" customerCat " + customerCat + "fromdate : " + fromdate + " todate : " + todate + " serviceCat :"
				+ serviceCat + " campaignTypeCat : " + campaignTypeCat);

		SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date fromDateNew = new Date();
		Date toDateNew = new Date();
		try {
			fromDateNew = dmyFormat.parse(fromdate);
			toDateNew = dmyFormat.parse(todate);
		} catch (Exception e) {
			e.printStackTrace();
		}

		long countData = 0;

		if (selectedCampaignType.equals("PSF")) {

			logger.info("PSf List for call assignment");

			countData = call_int_repo.getAssignListOfPSFByProcedureCount(fromDateNew, toDateNew, customerCat,
					serviceCat, campaignNamePSF,modelname,cityId,workshopName);

			scheduleData = call_int_repo.getAssignListOfPSFByProcedureServer(fromDateNew, toDateNew, customerCat,
					serviceCat, campaignTypeCat, campaignNameCat, campaignNamePSF,modelname, 0, countData,cityId,workshopName);

			logger.info("PSf List for call assignment" + countData);

		} else if (selectedCampaignType.equals("Campaign")) {

			logger.info("Campaign List for call assignment");

			countData = call_int_repo.getAssignListOfCampaignAndSMRByProcedureCount(fromDateNew, toDateNew, customerCat,
					serviceCat, campaignNameCat, cityId);

			scheduleData = call_int_repo.getAssignListOfCampaignAndSMRByProcedure(fromDateNew, toDateNew, customerCat,
					serviceCat, campaignNameCat, cityId, 0, countData);
		} else {

			countData = call_int_repo.getAssignListOfCampaignAndSMRByProcedureCount(fromDateNew, toDateNew, customerCat,
					serviceCat, campaignTypeCat, cityId);

			scheduleData = call_int_repo.getAssignListOfCampaignAndSMRByProcedure(fromDateNew, toDateNew, customerCat,
					serviceCat, campaignTypeCat, cityId, 0, countData);
		}

		int scheduledCallCount = scheduleData.size();
		logger.info(" scheduledCallCount : " + scheduledCallCount);

		int psfCallCount = scheduleData.size();

		int creSize = selectedList.size();

		if (selectedCampaignType.equals("PSF")) {

			logger.info("PSf Assignemt");

			if (creSize != 0) {
				logger.info("count of CRE :" + creSize);

				int divCountRemainder = psfCallCount % creSize;

				logger.info("divCountRemainder of CRE and call from CRE Manager:" + divCountRemainder);

				int divCount = (psfCallCount) / (creSize);
				logger.info("divCount of CRE and call :" + divCount);

				Collections.shuffle(scheduleData);

				int calls_count = 0;
				for (int i = 0; i < creSize; i++) {

					for (int j = calls_count; j < divCount + calls_count; j++) {

						String userdata = selectedList.get(i);
						AssignedListForCRE psfAssignId = scheduleData.get(j);

						call_int_repo.assignCallByManagerForPSF(userdata, psfAssignId, getUserLogindealerCode());

					}

					calls_count = divCount + calls_count;
					if (calls_count >= psfCallCount) {
						break;
					}

				}
				int call_countcompleted = divCount * creSize;
				logger.info("call_countcompleted : " + call_countcompleted);

				for (int i = 0; i < creSize; i++) {

					if (call_countcompleted < psfCallCount) {

						String userdata = selectedList.get(i);
						AssignedListForCRE psfAssignId = scheduleData.get(call_countcompleted);

						call_int_repo.assignCallByManagerForPSF(userdata, psfAssignId, getUserLogindealerCode());

						call_countcompleted++;
					} else {
						break;
					}

				}

			}

		} else {

			// SMR and Campaign Assignment

			if (creSize != 0) {
				logger.info("count of CRE :" + creSize);

				int divCountRemainder = scheduledCallCount % creSize;

				logger.info("divCountRemainder of CRE and call from CRE Manager:" + divCountRemainder);

				int divCount = (scheduledCallCount) / (creSize);
				logger.info("divCount of CRE and call :" + divCount);

				Collections.shuffle(scheduleData);

				int calls_count = 0;
				for (int i = 0; i < creSize; i++) {

					for (int j = calls_count; j < divCount + calls_count; j++) {

						String userdata = selectedList.get(i);
						AssignedListForCRE scall = scheduleData.get(j);

						long scallId = scall.getAssigninteraction_id();

						call_int_repo.assignCallByManagerSMRCamp(userdata, scallId, getUserLogindealerCode());
					}

					calls_count = divCount + calls_count;
					if (calls_count >= scheduledCallCount) {
						break;
					}

				}
				int call_countcompleted = divCount * creSize;
				logger.info("call_countcompleted : " + call_countcompleted);

				for (int i = 0; i < creSize; i++) {

					if (call_countcompleted < scheduledCallCount) {

						String userdata = selectedList.get(i);

						AssignedListForCRE scall = scheduleData.get(call_countcompleted);

						long scallId = scall.getAssigninteraction_id();

						call_int_repo.assignCallByManagerSMRCamp(userdata, scallId, getUserLogindealerCode());
						call_countcompleted++;

					} else {
						break;
					}

				}

			}

		}

		return ok(toJson(""));

	}

	@Secure(clients = "FormClient")
	public Result getCampaignNameBasedOnType(String campaignTypeId) {

		List<Campaign> listCampaignNames = changeRepo.getCampaignNameBasedOnType(campaignTypeId);
		logger.info("listCampaignNames:"+listCampaignNames);
		List<CampaignNamesAjaxLoadData> listCampaign = new ArrayList<CampaignNamesAjaxLoadData>();

		for (Campaign camp : listCampaignNames) {
			CampaignNamesAjaxLoadData addcampaign = new CampaignNamesAjaxLoadData();
			addcampaign.setId(camp.getId());
			addcampaign.setCampaignName(camp.getCampaignName());
			listCampaign.add(addcampaign);
		}
		logger.info("count of listCampaignNames for this campaigntype : " + listCampaignNames.size());
		return ok(toJson(listCampaign));

	}
}
