package controllers.pac4j;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import org.pac4j.core.context.Pac4jConstants;
import org.pac4j.core.context.WebContext;
import org.pac4j.core.credentials.UsernamePasswordCredentials;
import org.pac4j.core.credentials.authenticator.Authenticator;
import org.pac4j.core.exception.CredentialsException;
import org.pac4j.core.exception.HttpAction;
import org.pac4j.core.profile.CommonProfile;
import org.pac4j.core.profile.ProfileManager;
import org.pac4j.core.util.CommonHelper;
import configs.JinqSource;
import models.Dealer;
import models.Role;
import models.Tenant;
import models.WyzUser;
import org.springframework.beans.factory.annotation.Autowired;
import play.Logger.ALogger;
import repositories.WyzUserRepository;

public class MyAppAuthenticator implements Authenticator<UsernamePasswordCredentials> {

	public static String TENANT_KEY = "TENANT";
	public static String IP_ADDR = "IP_ADDR";

	protected static final ALogger logger = play.Logger.of(MyAppAuthenticator.class);

	private WyzUserRepository wyzRepository;

	public WyzUserRepository getWyzRepository() {
		return wyzRepository;
	}

	public void setWyzRepository(WyzUserRepository wyzRepository) {
		this.wyzRepository = wyzRepository;
	}

	@Override
	public void validate(final UsernamePasswordCredentials credentials, final WebContext webContext)
			throws HttpAction, CredentialsException {

		if (credentials == null) {
			throwsException("No credential");
		}

		String username = credentials.getUsername();
		String password = credentials.getPassword();

		logger.info(" username : " + username);
		logger.info(" password : " + password);

		if (CommonHelper.isBlank(username)) {
			throwsException("Username cannot be blank");
		}
		if (CommonHelper.isBlank(password)) {
			throwsException("Password cannot be blank");
		}

		String dealerCode = null;
		String userRole = null;
		String userRole2 = null;
		String time_Zone = null;
		String dealerName = null;
		long user_ID = 0;
		boolean insurancerole = false;

		boolean webAthentication = false;
		boolean AndroidAuthentication = false;
		boolean webAndAndroidAuthen = false;
		boolean webAuthWithCallInitiating = false;

		WyzUser userAuthenticating = getWyzRepository().getUserDetails(username, password);
		// boolean result = getWyzRepository().authenticateUser(username,
		// password);

		if (userAuthenticating != null) {

			dealerCode = userAuthenticating.getDealerCode();
			dealerName = userAuthenticating.getDealerName();
			userRole = userAuthenticating.getRole();
			userRole2 = userAuthenticating.getRole1();
			user_ID = userAuthenticating.getId();

			String oem = getWyzRepository().getOEMBYDealerCode(dealerCode);

			if (!"".equalsIgnoreCase(dealerCode) && dealerCode != null) {
				logger.info("User from database obtained successfully for the username:" + username);

				logger.info("user_ID : is :" + user_ID);

				logger.debug("Authentication successfull for user: " + username);

				List<Role> roles = getWyzRepository().getUserRoles(username);
				logger.debug("Obtained roles" + roles);

				Tenant tenant = getWyzRepository().getUserTenant(username);

				logger.debug("Obtained tenant code:" + tenant.getTenantCode() + " for user:" + username);
				final CommonProfile profile = new CommonProfile();
				profile.setId(username);
				profile.addAttribute(Pac4jConstants.USERNAME, username);
				profile.addAttribute("USER_ROLE", userRole);
				profile.addAttribute("USER_ID", user_ID);
				profile.addAttribute("USER_ROLE2", userRole2);
				profile.addAttribute("INSUR_ROLE", insurancerole);
				profile.addAttribute("DEALER_CODE", dealerCode);
				profile.addAttribute("DEALER_NAME", dealerName);
				profile.addAttribute("OEM", oem);

				profile.addAttribute("ANDROID", AndroidAuthentication);
				profile.addAttribute("WEB_ANDROID", webAndAndroidAuthen);
				profile.addAttribute("WEB_AUTH", webAthentication);
				profile.addAttribute("WEB_AUTH_CALL_INI", webAuthWithCallInitiating);
				profile.addAttribute("TIME_ZONE", time_Zone);
				logger.debug("Set the Attribute:" + Pac4jConstants.USERNAME + "to ->" + username);

				profile.addAttribute(TENANT_KEY, tenant.getTenantCode());
				logger.debug("Set the Attribute:" + TENANT_KEY + "to ->" + tenant.getTenantCode());
				
				String remoteAddr = webContext.getRemoteAddr();
                profile.addAttribute(IP_ADDR, remoteAddr);
                
                logger.debug("Set the Attribute:" + IP_ADDR + "to ->" + remoteAddr);


				roles.forEach((role) -> {
					profile.addRole(role.getRole());
					logger.debug("Add role to CommonProfile- role:" + role.getRole());
				});

				credentials.setUserProfile(profile);
			} else {
				throwsException("Invalid User");

			}
		} else {
			throwsException("User not valid");
		}

	}

	protected void throwsException(final String message) throws CredentialsException {
		throw new CredentialsException(message);
	}

}
