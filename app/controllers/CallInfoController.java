package controllers;

import org.json.JSONObject;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Connection;
import javax.sql.DataSource;
import java.sql.SQLException;
import play.db.DB;

import static play.libs.Json.toJson;
import play.data.Form;

import org.pac4j.play.PlayWebContext;
import org.pac4j.play.store.PlaySessionStore;
import org.pac4j.core.profile.ProfileManager;

import java.io.FileReader;

import play.Logger.ALogger;
import play.api.Play;
import play.libs.Akka;
import play.libs.concurrent.HttpExecution;
import play.mvc.Http;
import play.mvc.Result;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import models.UpsellLead;
import models.ServiceTypes;
import models.SpecialOfferMaster;
import models.TaggingUsers;
import models.ListingForm;
import models.WyzUser;
import models.CallSummary;
import models.Campaign;
import models.ServiceBooked;
import repositories.CallInfoRepository;
import repositories.ScheduledCallRepository;
import repositories.SearchRepository;
import models.Complaint;
import repositories.SynchedKeyRepository;
import repositories.WyzUserRepository;
import scala.concurrent.ExecutionContext;
import scala.concurrent.ExecutionContextExecutor;
import scala.concurrent.duration.Duration;
import views.html.editCallInfo;

import views.html.followUpCallsOfTodayCRE;
import views.html.PSFCallLogPageCREManager;
import views.html.InsuranceCallLogPageCREManager;

import views.html.callDispositionFormServiceDivision;
import views.html.callDispositionPageCREManager;

import views.html.noAcceccToPage;

import static play.mvc.Results.notFound;
import static play.mvc.Results.ok;
import static play.mvc.Http.Response;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.pac4j.core.profile.CommonProfile;
import org.pac4j.play.java.Secure;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ParseDouble;
import org.supercsv.cellprocessor.ParseInt;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.*;
import org.supercsv.prefs.CsvPreference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import controllers.webmodels.BookedLineChartData;
import controllers.webmodels.FirebaseResponse;
import controllers.webmodels.PieChartCallTypeData;
import actors.FireBaseCallLogSync;
import actors.FireBaseCallLogSyncProtocol;

import akka.actor.ActorRef;
import akka.actor.Cancellable;
import akka.actor.Props;
import com.sun.javafx.scene.control.skin.VirtualFlow;
import controllers.webmodels.FollowUpNotificationModel;
import java.text.ParseException;
import models.Address;
import models.CallDispositionData;
import models.Insurance;
import models.CallInteraction;
import models.Customer;
import models.PickupDrop;
import models.SMSTemplate;
import models.SRDisposition;
import models.ServiceAdvisor;
import models.Vehicle;
import models.Workshop;
import models.Driver;
import models.Location;
import models.Phone;
import models.Service;
import org.hibernate.Hibernate;
import repositories.CallInteractionsRepository;
import repositories.SMSTriggerRespository;
import static play.mvc.Results.notFound;
import static play.mvc.Results.ok;

@Controller
public class CallInfoController extends play.mvc.Controller {

	private final CallInfoRepository repo;

	private final WyzUserRepository userRepository;
	private final SMSTriggerRespository sms_repo;
	public final SearchRepository searchRepo;
	private final ScheduledCallRepository schduledRepositroy;
	private final CallInteractionsRepository call_int_repo;
	private Cancellable schedulerCallLog = null;
	private Cancellable userSyncAuthenticate = null;
	private Cancellable serviceBookedScheduler = null;

	private PlaySessionStore playSessionStore;

	ALogger logger = play.Logger.of("application");

	@Inject
	public CallInfoController(CallInfoRepository repository, org.pac4j.core.config.Config config,
			WyzUserRepository userRepo, ScheduledCallRepository schRepo, CallInteractionsRepository interRepo,
			SMSTriggerRespository smsRepository, SearchRepository search_Repo,PlaySessionStore plstore) {
		repo = repository;
		userRepository = userRepo;
		config = (org.pac4j.core.config.Config) config;
		schduledRepositroy = schRepo;
		call_int_repo = interRepo;
		sms_repo = smsRepository;
		searchRepo = search_Repo;
		this.playSessionStore = plstore;

	}

	private List<String> ajaxDataForDashboardOfCREMan() {
		/*
		 * String user = getUserLoginName();
		 * 
		 * long count = schduledRepositroy
		 * .getScheduledCallCountByCREMan(user,getUserLogindealerCode());
		 * 
		 * //long count1 = schduledRepositroy //
		 * .getScheduledCallsPendingCountCREMan(user,getUserLogindealerCode());
		 * 
		 * //long count2 = schduledRepositroy
		 * //.getServiceBookedCountCreMan(user,getUserLogindealerCode());
		 * 
		 * //double count3 = schduledRepositroy
		 * //.getConversionPercentageCreMan(user,getUserLogindealerCode());
		 * 
		 * String pattern = "###.##";
		 * 
		 * DecimalFormat formatter = new DecimalFormat(pattern);
		 * 
		 * String displayPercentage = formatter.format(count3);
		 * 
		 * ArrayList<String> ajaxData = new ArrayList<String>();
		 * ajaxData.add(Long.toString(count));
		 * ajaxData.add(Long.toString(count1));
		 * ajaxData.add(Long.toString(count2)); ajaxData.add(displayPercentage);
		 * 
		 * return ajaxData;
		 */
		ArrayList<String> ajaxData = new ArrayList<String>();
		return ajaxData;

	}

	private List<String> ajaxDataForDashboardOfCRE() {

		// String userName = getUserLoginName();
		//
		// long countOfSchCallOfCRE = schduledRepositroy
		// .getScheduledCallCountByOfCRE(userName,
		// getUserLogindealerCode());
		// long count = schduledRepositroy.getScheduledCallsPendingCountForCRE(
		// userName, getUserLogindealerCode());
		// long count1 =
		// schduledRepositroy.getServiceBookedCountForCRE(userName,
		// getUserLogindealerCode());
		//
		// double count2 = schduledRepositroy.getConversionPercentageOfCRE(
		// userName, getUserLogindealerCode());
		//
		// String pattern = "###.##";
		//
		// DecimalFormat formatter = new DecimalFormat(pattern);
		//
		// String displayPercentage = formatter.format(count2);
		//
		// long countOFFollowUp =
		// repo.getCountOfFollowUpsTobeDoneToday(userName,
		// getUserLogindealerCode());
		//
		// ArrayList<String> ajaxData = new ArrayList<String>();
		// ajaxData.add(Long.toString(countOfSchCallOfCRE));
		// ajaxData.add(Long.toString(count));
		// ajaxData.add(Long.toString(count1));
		// ajaxData.add(displayPercentage);
		// ajaxData.add(Long.toString(countOFFollowUp));

		ArrayList<String> ajaxData = new ArrayList<String>();

		return ajaxData;
	}

	public Result startSyncOperation() {

		String userrole = (String) getUserProfile().getAttribute("USER_ROLE");
		if (userrole.equals("CREManager")) {

			boolean startsyncState = false;

			if (schedulerCallLog != null) {
				if (schedulerCallLog.isCancelled()) {
					startsyncState = true;
				} else {
					startsyncState = false;
				}
			} else {
				startsyncState = true;
			}

			if (startsyncState) {

				ActorRef actor = Akka.system().actorOf(Props.create(FireBaseCallLogSync.class));

				// ExecutionContext myEc =
				// HttpExecution.fromThread(Akka.system().dispatcher());

				ExecutionContextExecutor myEc = Akka.system().dispatcher();

				schedulerCallLog = Akka.system().scheduler().schedule(Duration.create(0, TimeUnit.MILLISECONDS), // Initial
																													// delay
																													// 0
																													// milliseconds
						Duration.create(10, TimeUnit.MINUTES), // Frequency
																// 1
																// minutes
																// actor,
						actor,
						new FireBaseCallLogSyncProtocol.StartSync(
								play.Play.application().configuration().getString("app.myfirebaseurl"), repo,
								userRepository, call_int_repo),
						myEc, null);

				logger.info("On Start of Global Applciation. Before Scheduler start");

			}
			logger.info("On finish of Global Applciation. Before Scheduler start");

			return ok("Call log sync started");
			// return
			// redirect(routes.ScheduledCallController.scheduledCallList());

		} else {
			return forbidden(views.html.error403.render());
		}

	}

	public Result stopSyncOperation() {
		String userrole = (String) getUserProfile().getAttribute("USER_ROLE");
		if (userrole.equals("CREManager")) {
			if (schedulerCallLog != null) {
				if (!schedulerCallLog.isCancelled())
					schedulerCallLog.cancel();
			}
			logger.info("Calllog sync stopped");

			return ok("Call log sync stopped");
			// return
			// redirect(routes.ScheduledCallController.scheduledCallList());
		} else {
			return forbidden(views.html.error403.render());
		}
	}

	// delete the files from tmp directory
	@Secure(clients = "FormClient")
	public Result todeleteFilesFromDirectory() {
		String userrole = (String) getUserProfile().getAttribute("USER_ROLE");
		if (userrole.equals("CREManager")) {
			Config configuration = ConfigFactory.load();
			String pathOfTemp = configuration.getString("app.configuration");

			File listFilesFromTmp = new File(pathOfTemp);
			String[] entries = listFilesFromTmp.list();
			logger.info("the count of files are :" + entries.length);

			for (String s : entries) {
				File currentFile = new File(listFilesFromTmp.getPath(), s);

				currentFile.delete();
			}

			return ok("Files Deleted");
		} else {
			return forbidden(views.html.error403.render());
		}

	}

	// delete call log

	public Result deleteCallLog(String dealercode, Long id) {

		String userloginname = getUserLoginName();
		String dealername = getDealerName();

		repo.deletCall(id, getUserLogindealerCode());

		return ok("done");
	}

	public Result deleteSchCalllog(String dealercode, Long id) {

		String userloginname = getUserLoginName();
		String dealername = getDealerName();

		repo.deletSchCall(id, getUserLogindealerCode());

		return ok("done");
	}

	// Edit the CallLog of CRE

	public Result getcallLogEditForCRE(Long id) {

		String userloginname = getUserLoginName();
		// CallInteraction postcallinfo =
		// repo.getCallLogsByCustomer(id,getUserLogindealerCode());
		CallInteraction postcallinfo = call_int_repo.getCallLogsByCRE(id, getUserLogindealerCode());
		String dealername = getDealerName();

		Service latest_service = call_int_repo.getLatestServiceData(postcallinfo.getVehicle().getVehicle_id());

		return ok(editCallInfo.render(getOEMOfDealer(), getUserLogindealerCode(), dealername, userloginname, id,
				postcallinfo, latest_service));

	}

	@Secure(clients = "FormClient")
	public Result postcallLogeditForCRE(Long id) {

		String userloginname = getUserLoginName();
		String dealername = getDealerName();

		Form<CallInteraction> form = Form.form(CallInteraction.class).bindFromRequest();
		CallInteraction call_interaction = form.get();

		Form<Vehicle> vehicle_data = Form.form(Vehicle.class).bindFromRequest();
		Vehicle vehicle = vehicle_data.get();

		Form<Customer> customer_data = Form.form(Customer.class).bindFromRequest();
		Customer customer = customer_data.get();

		Form<ServiceBooked> service_Booked_data = Form.form(ServiceBooked.class).bindFromRequest();
		ServiceBooked service_Booked = service_Booked_data.get();

		Form<SRDisposition> sr_Disposition_data = Form.form(SRDisposition.class).bindFromRequest();
		SRDisposition sr_Disposition = sr_Disposition_data.get();

		Form<PickupDrop> pick_up_data = Form.form(PickupDrop.class).bindFromRequest();
		PickupDrop pick_up = pick_up_data.get();

		call_int_repo.postEditOfCallInteractions(customer, call_interaction, service_Booked, sr_Disposition, pick_up,
				vehicle, id);

		return redirect(controllers.routes.ScheduledCallController.getCallDispositionTabPAgeCRE());

	}

	private static CellProcessor[] getUserDataCellProcessor() {
		return new CellProcessor[] { new Optional(), new Optional(), new Optional(), new Optional(), new Optional(),
				new Optional(), new Optional(), new Optional(), new Optional(), new Optional(), new Optional(),
				new Optional(), new Optional(), new Optional(), new Optional(), new Optional(), new Optional(),
				new Optional(), new Optional(), new Optional(), new Optional(), new Optional(), new Optional(),
				new Optional(), new Optional(), new Optional(), new Optional(), new Optional(), new Optional(),
				new Optional(), new Optional(), new Optional(), new Optional(), new Optional(), new Optional(),
				new Optional(), new Optional(), new Optional(), new Optional(), new Optional(), new Optional(),
				new Optional(), new Optional(), new Optional(), new Optional() };

	}

	private static CellProcessor[] getUserDataCellProcessorForDownload() {
		return new CellProcessor[] { new Optional(), new Optional(), new Optional(), new Optional(), new Optional(),
				new Optional(), new Optional(), new Optional(), new Optional(), new Optional(), new Optional(),
				new Optional(), new Optional(), new Optional() };

	}

	public Result getChartView(String selectedAgentadd, String selectedType) {
		logger.info("the multiselect agent is :" + selectedAgentadd);

		// List<String> selectedList =
		// Arrays.asList(selectedAgentAdded.split(","));
		// String selectedAgent="Naveen_tata";

		int count = 0;
		if (selectedType.equals("Last 1 Week")) {
			count = 6;
		} else if (selectedType.equals("Last 2 Week")) {
			count = 13;
		} else if (selectedType.equals("Last 3 Week")) {
			count = 20;

		} else {
			count = 0;
		}
		Calendar c = Calendar.getInstance();
		Date currentDate = c.getTime();
		currentDate.setHours(23);
		currentDate.setMinutes(59);
		currentDate.setSeconds(59);
		logger.info("date converted is : " + currentDate);

		c.setTime(new Date()); // Now use today date.
		c.add(Calendar.DATE, -count); // minus 6 days
		Date addedDate = c.getTime();
		addedDate.setHours(0);
		addedDate.setMinutes(0);
		addedDate.setSeconds(0);

		logger.info("one week :" + addedDate);
		List<String> chart1 = repo.getCallTypeCountsForCRE(selectedAgentadd, getUserLogindealerCode(), currentDate,
				addedDate);
		/*
		 * logger.info("the outgoing count is : "+chart1.get(0)); logger.info(
		 * "the missedCall count is : "+chart1.get(1)); logger.info(
		 * "the incoimng count is : "+chart1.get(2));
		 */
		List<String> selectedList = Arrays.asList(selectedAgentadd.split(","));
		logger.info("Number of selectedList is: " + selectedList.size());
		List<String> toaddMiss = new ArrayList<String>();
		List<String> toaddOut = new ArrayList<String>();
		List<String> toaddInc = new ArrayList<String>();
		PieChartCallTypeData returnData = new PieChartCallTypeData();
		for (int i = 0; i < 3; i++) {
			if (i == 0) {
				for (int l = 0, j = 0; l < selectedList.size(); l++, j = j + 3) {
					toaddMiss.add(chart1.get(j));
				}
				returnData.setMissedList(toaddMiss);

			}
			if (i == 1) {
				for (int l = 0, j = 0; l < selectedList.size(); l++, j = j + 3) {
					toaddOut.add(chart1.get(j + 1));
				}
				returnData.setOutGoingList(toaddOut);

			}
			if (i == 2) {
				for (int l = 0, j = 0; l < selectedList.size(); l++, j = j + 3) {
					toaddInc.add(chart1.get(j + 2));

				}
				returnData.setIncomingList(toaddInc);

			}

		}

		logger.info("chart Form submit");

		return ok(toJson(returnData));

	}

	@Secure(clients = "FormClient")
	public Result startInitiatingOfCall(String phoneNumberId, long uniqueid, long customerId) {

		String cre = getUserLoginName();
		
		Customer customer=call_int_repo.getCustomerById(customerId);
		String customername="unknown";
		
		if(customer.getCustomerName()!=null){
			
			customername=customer.getCustomerName();
		}

		logger.info(" phoneNumber Id to call initiate : " + phoneNumberId);

		//Phone phoneNumber = repo.getPhoneById(phoneNumberId);

		//logger.info(" phoneNumber Id : " + phoneNumberId + " phoneNumber is " + phoneNumber.getPhoneNumber());

		// long uniqueNumber=1289544;
		if (uniqueid == 1) {

			uniqueid = repo.getUniqueIdForListInitaiating();
			logger.info("uniqueid from list intialting is :" + uniqueid);
		}

		logger.info("uniqueid is :" + uniqueid);
		WyzUser getResistrationId = userRepository.getUserbyUserName(cre);
		String regId = getResistrationId.getRegistrationId();
		logger.info("Reg id " + regId);
		logger.info("phoneNumber " + String.valueOf(phoneNumberId));
		
		String API_KEY = "AIzaSyBO6z7Yw55Mgv3hBLcgDNAis3RF8bSGe_U";
		String url = "https://fcm.googleapis.com/fcm/send";
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "key=" + API_KEY);
		headers.add("Accept", "application/json");
		headers.add("Content-Type", "application/json");

		try {
			JSONObject data1 = new JSONObject();
			data1.put("to", regId);
			JSONObject data2 = new JSONObject();
			data2.put("command", "Instiate");
			data2.put("phonenumber", phoneNumberId);
			data2.put("id", uniqueid);
			data2.put("makeCallFrom", "Service");
			data2.put("type", "Web");
			data2.put("customername", customername);
			data2.put("priority", "high");
			data1.put("data", data2);

			// logger.info("data1.toString(): " + data1.toString());

			HttpEntity<String> entityCredentials = new HttpEntity<String>(data1.toString(), headers);
			FirebaseResponse firebaseResponse = restTemplate.postForObject(url, entityCredentials,
					FirebaseResponse.class);

			logger.info("response is: " + firebaseResponse.toString());
			logger.info("resultOFCall :" + firebaseResponse.getSuccess());
			int resultOFCall = firebaseResponse.getSuccess();

			if (resultOFCall == 1) {
				return ok(toJson("success"));

			} else {

				return ok(toJson("Failure"));

			}

		} catch (Exception e) {
			logger.info("Exception :" + e);

			return ok(toJson("problem"));
		}

	}

	@Secure(clients = "FormClient")
	public Result getCallDispositionBucketForCREMan() {

		String userName = getUserLoginName();
		String dealername = getDealerName();

		HashMap<Long, String> listCREManCRE = repo.getCREUserNameIdofCREManager(userName, getUserLogindealerCode());

		List<Location> listCity = call_int_repo.getLocationList();
		
		//List<Campaign> campaignList = call_int_repo.getAllCampaignList();
		List<Campaign> campaignList=new ArrayList<Campaign>();
		List<Campaign> campaignListCampign = call_int_repo.getCampaignNamesBYType("Campaign");
		List<Campaign> campaignListremainder = call_int_repo.getCampaignNamesBYType("Service Reminder");
		campaignList.addAll(campaignListCampign);
		campaignList.addAll(campaignListremainder);
		List<ServiceTypes> serviceTypeList = call_int_repo.getAllServiceTypeList();
		List<String> bookedServiceTypeList = searchRepo.getBookedServiceType();
		/*List<CallDispositionData> dispoList = userRepository.getDispositionList();
		List<CallDispositionData> newlistDispo = dispoList.subList(5, 10);
*/
		List<CallDispositionData> newlistDispo = userRepository.getDispositionListOfNoncontacts();
		return ok(callDispositionPageCREManager.render(listCity, dealername, userName, listCREManCRE,campaignList,serviceTypeList,bookedServiceTypeList,newlistDispo));

	}

	// Follow up is not done on scheduled time today

	@Secure(clients = "FormClient")
	public Result getFollowUpRemainderOfMissedSchedules() {

		FollowUpNotificationModel folowupData = new FollowUpNotificationModel();
		String dealercode = getUserLogindealerCode();
		String cre = getUserLoginName();
		List<CallInteraction> missedFollowUp = repo.getListOfFollowUpMissedByAgent(cre, dealercode);
		// logger.info("missedFollowUp by agent : "+missedFollowUp.size());

		return ok(toJson(""));

	}

	// FollowUp Notification before 5 min

	@Secure(clients = "FormClient")
	public Result getFollowUpNotifyBeforeTime() {
		FollowUpNotificationModel folowupData = new FollowUpNotificationModel();
		String dealercode = getUserLogindealerCode();
		String cre = getUserLoginName();

		List<CallInteraction> followUpdata = new ArrayList<CallInteraction>();
		// List<CallInteraction> followUpdata =
		// repo.getListOfFollowUpOfTodayCRE(cre, dealercode);

		CallInteraction callinfo_data = new CallInteraction();

		if (followUpdata.size() > 0) {

			String pattern1 = "HH:mm";
			SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat(pattern1);
			Calendar c = Calendar.getInstance();
			c.setTime(new java.util.Date()); // Now use today date.
			c.add(Calendar.MINUTE, 5);

			String callTime = simpleDateFormat1.format(c.getTime());
			// String updatetime=new String();
			for (CallInteraction callList : followUpdata) {

				if (callList.getSrdisposition().getFollowUpTime().equals(callTime)) {

					// logger.info("callList.getSrdisposition().getFollowUpTime():
					// "+callList.getSrdisposition().getFollowUpTime());
					// logger.info("callTime plus 5 min: "+callTime);

					callinfo_data = callList;

					folowupData.setCallInteraction_id(callinfo_data.getId());
					folowupData.setCustomerName(callinfo_data.getCustomer().getCustomerName());
					folowupData.setFollowUpTime(callinfo_data.getSrdisposition().getFollowUpTime());
					folowupData.setDealerCode(getUserLogindealerCode());
					// logger.info("customert name is to update beflore 5 min
					// :"+callinfo_data.getCustomer().getCustomerName());

					break;

				}

			}
		}

		return ok(toJson(folowupData));

	}

	// Follow up notification of today pop up

	@Secure(clients = "FormClient")
	public Result getFollowUpNotificationToday() {
		String dealercode = getUserLogindealerCode();
		String cre = getUserLoginName();
		WyzUser userdata = userRepository.getUserbyUserName(cre);
		List<FollowUpNotificationModel> folowupData = call_int_repo.getFollowUpNotification(userdata.getId(),
				dealercode);

		return ok(toJson(folowupData));

	}

	// Table of todays followup onclick of notification

	@Secure(clients = "FormClient")
	public Result getFollowUpTableDataOfCRE() {

		String cre = getUserLoginName();
		WyzUser userdata = userRepository.getUserbyUserName(cre);
		List<Long> folowupDataId = new ArrayList();
		List<FollowUpNotificationModel> followupdata=call_int_repo.getFollowUpNotification(userdata.getId(),getUserLogindealerCode());
		
		for(FollowUpNotificationModel sa: followupdata){
			
			folowupDataId.add(sa.getCallInteraction_id());
		}


		List<CallInteraction> followUpdata = repo.getListOfFollowUpOfTodayCRE(folowupDataId);

		return ok(followUpCallsOfTodayCRE.render(getOEMOfDealer(), getUserLogindealerCode(), getDealerName(),
				getUserLoginName(), followUpdata));
	}

	@Secure(clients = "FormClient")
	public Result getUpsellLeadsSeletedInLastSB(long sr_int_id) {

		logger.info(" sr_int_id : " + sr_int_id);

		List<UpsellLead> selectedupselLead = repo.getUpselLeadSelectedBySRDispo(sr_int_id);

		List<UpsellLead> upseldLeadJsonData = new ArrayList<UpsellLead>();

		for (UpsellLead up : selectedupselLead) {

			up.setSrDisposition(null);
			up.setTaggingUsers(null);
			up.setVehicle(null);
			upseldLeadJsonData.add(up);
		}

		return ok(toJson(upseldLeadJsonData));
	}

	private CommonProfile getUserProfile() {
		final PlayWebContext context = new PlayWebContext(ctx(), playSessionStore);
		final ProfileManager<CommonProfile> profileManager = new ProfileManager(context);
		logger.debug("............About to obtain the profiles..........");
		List<CommonProfile> profiles = profileManager.getAll(true);
		logger.debug("..............Obtained profiles........." + profiles);
		return profiles.get(0);
	}

	private boolean checkCREManagerRole() {

		String role = (String) getUserProfile().getAttribute("USER_ROLE");
		if ("CREManager".equals(role))
			return true;
		else
			return false;
	}

	private String getUserLoginName() {
		String userloginname = getUserProfile().getId();

		return userloginname;

	}

	private String getDealerName() {
		String dealerName = (String) getUserProfile().getAttribute("DEALER_NAME");

		return dealerName;

	}

	private String getUserLogindealerCode() {
		String dealer_Code = (String) getUserProfile().getAttribute("DEALER_CODE");

		return dealer_Code;

	}

	private String getUserpsfRole() {
		String userPSFRole = (String) getUserProfile().getAttribute("USER_ROLE2");

		return userPSFRole;
	}

	private boolean validatingAndroidAuthentication() {

		boolean dealerAndroidAuth = (boolean) getUserProfile().getAttribute("ANDROID");
		return dealerAndroidAuth;

	}

	private boolean validatingWebAndAndroidAuthen() {

		boolean dealerWebAndAndroidAuth = (boolean) getUserProfile().getAttribute("WEB_ANDROID");

		return dealerWebAndAndroidAuth;

	}

	private boolean validatingWebAthentication() {

		boolean dealerWebAuth = (boolean) getUserProfile().getAttribute("WEB_AUTH");
		return dealerWebAuth;
	}

	private boolean validatingWebAuthWithCallInitiating() {

		boolean dealerWebCallsAuth = (boolean) getUserProfile().getAttribute("WEB_AUTH_CALL_INI");

		return dealerWebCallsAuth;
	}

	private String getTimeZoneOfDealerCode() {

		String time_zone = (String) getUserProfile().getAttribute("TIME_ZONE");

		return time_zone;

	}

	private String getOEMOfDealer() {

		String oemIS = (String) getUserProfile().getAttribute("OEM");

		return oemIS;

	}

	private void inside_Finally_comment(PreparedStatement cs, ResultSet rs, Connection connection) {
		try {
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
		}
		;
		try {
			if (cs != null) {
				cs.close();
			}
		} catch (Exception e) {
		}
		;
		try {
			if (connection != null) {
				if (!connection.isClosed()) {
					connection.close();
				}
			}
		} catch (Exception e) {
		}
		;
	}

	public Result getLeadNamesbyLeadId(long leadId, long userId) {

		String taggingType = repo.getLeadNameById(leadId);

		List<WyzUser> usersListByRole = userRepository.getUsersByRole(taggingType);

		List<Location> loginUserlocations = userRepository.getUserLocationById(userId);

		List<TaggingUsers> tagginguser = new ArrayList<TaggingUsers>();

		for (Location sa : loginUserlocations) {

			for (WyzUser la : usersListByRole) {

				List<Location> userlocations = userRepository.getUserLocationById(la.getId());

				List<Location> userlocation = userlocations.stream().filter(u -> u.getCityId() == sa.getCityId())
						.collect(Collectors.toList());

				logger.info(" userlocation : " + userlocation.size());
				if (userlocation.size() > 0) {
					
					boolean exit=true;
					
					TaggingUsers taguser = new TaggingUsers();
					taguser.setName(la.getUserName());					
					
					for(TaggingUsers tag:tagginguser){
						if(tag.getName().equals(la.getUserName())){
							
							 exit = false;
						}else{
							exit=true;
						}
						
					}
					
					if(exit){
						
						tagginguser.add(taguser);
					}
					
					
				}
			}
		}

		return ok(toJson(tagginguser));

	}
	
	public Result getCheckVehicleRegExist(String vehicleReg){
		
		
		long countRegExist=repo.getVehicleCountExisting(vehicleReg);
		return ok(toJson(countRegExist));
		
	}

	@Secure(clients = "FormClient")
	public Result getPSFCallLogViewPage(){
		String userName = getUserLoginName();
		String dealername = getDealerName();

		HashMap<Long, String> listCREManCRE = repo.getCREUserNameIdofCREManager(userName, getUserLogindealerCode());

		List<Location> listCity = call_int_repo.getLocationList();
		
		List<Campaign> campaignListPSF = call_int_repo.getAllCampaignListPSF();
		//List<Campaign> campaignList=new ArrayList<Campaign>();
		//List<Campaign> campaignListCampign = call_int_repo.getCampaignNamesBYType("Campaign");
		//List<Campaign> campaignListremainder = call_int_repo.getCampaignNamesBYType("Service Reminder");
		//campaignList.addAll(campaignListCampign);
		//campaignList.addAll(campaignListremainder);
		List<ServiceTypes> serviceTypeList = call_int_repo.getAllServiceTypeList();
		List<String> bookedServiceTypeList = searchRepo.getBookedServiceType();
		/*List<CallDispositionData> dispoList = userRepository.getDispositionList();
		List<CallDispositionData> newlistDispo = dispoList.subList(5, 10);*/
		List<CallDispositionData> newlistDispo = userRepository.getDispositionListOfNoncontacts();
		return ok(PSFCallLogPageCREManager.render(listCity, dealername, userName, listCREManCRE,campaignListPSF,serviceTypeList,newlistDispo));
	}
	@Secure(clients = "FormClient")
	public Result getInsuranceCallLogViewPage(){
		String userName = getUserLoginName();
		String dealername = getDealerName();

		HashMap<Long, String> listCREManCRE = repo.getCREUserNameIdofCREManager(userName, getUserLogindealerCode());

		List<Location> listCity = call_int_repo.getLocationList();
		
		List<Campaign> campaignListInsurance = call_int_repo.getAllCampaignListInsurance();
		//List<Campaign> campaignList=new ArrayList<Campaign>();
		//List<Campaign> campaignListCampign = call_int_repo.getCampaignNamesBYType("Campaign");
		//List<Campaign> campaignListremainder = call_int_repo.getCampaignNamesBYType("Service Reminder");
		//campaignList.addAll(campaignListCampign);
		//campaignList.addAll(campaignListremainder);
		List<ServiceTypes> serviceTypeList = call_int_repo.getAllServiceTypeList();
		List<String> bookedServiceTypeList = searchRepo.getBookedServiceType();
		/*List<CallDispositionData> dispoList = userRepository.getDispositionList();
		List<CallDispositionData> newlistDispo = dispoList.subList(5, 10);*/
		List<CallDispositionData> newlistDispo = userRepository.getDispositionListOfNoncontacts();
		return ok(InsuranceCallLogPageCREManager.render(listCity, dealername, userName, listCREManCRE,campaignListInsurance,serviceTypeList,newlistDispo));
	}
	
}
