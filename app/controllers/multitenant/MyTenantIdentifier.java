package controllers.multitenant;

import java.util.List;

import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.pac4j.core.config.Config;
import org.pac4j.core.profile.CommonProfile;
import org.pac4j.core.profile.ProfileManager;
import org.pac4j.play.PlayWebContext;
import org.pac4j.play.store.PlaySessionStore;

import com.typesafe.config.ConfigFactory;

import controllers.pac4j.MyAppAuthenticator;
import play.Logger.ALogger;
import play.mvc.Controller;

public class MyTenantIdentifier extends Controller implements CurrentTenantIdentifierResolver {

	protected static ALogger logger = play.Logger.of(MyTenantIdentifier.class);

	private Config config;
	private PlaySessionStore playSessionStore;

	@Override
	public String resolveCurrentTenantIdentifier() {

		com.typesafe.config.Config configuration = ConfigFactory.load();
		String defaultDataBaseName = configuration.getString("app.defaultdatabase");

		try {
			List<CommonProfile> profiles = getProfiles();

			if (profiles == null) {
				logger.debug("profile list is null returning default database name:" + defaultDataBaseName);
				return defaultDataBaseName;
			} else if (profiles.size() == 0) {
				logger.debug("Profile list is empty returning default database name:" + defaultDataBaseName);
				return defaultDataBaseName;
			} else {
				CommonProfile profile = profiles.get(0);
				logger.debug("Trying to get Tenant information from profile:" + profile);

				String tenantCode = (String) profile.getAttribute(MyAppAuthenticator.TENANT_KEY);
				logger.debug("Obtained tenant code from profile:" + tenantCode);
				return tenantCode;
			}
		} catch (Exception e) {
			logger.debug("There was an exception while trying to obtain the profile:" + e.toString());
			logger.debug("returning default database name" + defaultDataBaseName);
			return defaultDataBaseName;
		}

	}

	@Override
	public boolean validateExistingCurrentSessions() {
		return true;
	}

	public Config getConfig() {
		return config;
	}

	public void setConfig(Config config) {
		this.config = config;
	}

	public PlaySessionStore getPlaySessionStore() {
		return playSessionStore;
	}

	public void setPlaySessionStore(PlaySessionStore playSessionStore) {
		this.playSessionStore = playSessionStore;
	}

	private List<CommonProfile> getProfiles() {
		final PlayWebContext context = new PlayWebContext(ctx(), playSessionStore);
		final ProfileManager<CommonProfile> profileManager = new ProfileManager(context);
		logger.debug("............About to obtain the profiles..........");
		List<CommonProfile> profiles = profileManager.getAll(true);
		logger.debug("..............Obtained profiles........." + profiles);
		return profiles;
	}

}
