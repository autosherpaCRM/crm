package controllers;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.List;

import javax.inject.Inject;

import org.pac4j.play.java.Secure;

import models.CallInteraction;
import play.Logger.ALogger;
import play.mvc.Result;
import repositories.AllCallInteractionRepository;
import repositories.CallInteractionsRepository;

public class AllAudioConverter extends play.mvc.Controller {
	
	ALogger logger = play.Logger.of("application");
	
	private final CallInteractionsRepository call_int_repo;
	private AllCallInteractionRepository repository;
	@Inject
	public AllAudioConverter(CallInteractionsRepository interRepo,AllCallInteractionRepository repo) {		
		call_int_repo = interRepo;
		repository = repo;		
	}

	
	
	
	@Secure(clients = "FormClient")
	public Result convertAllFiles(long startId, long endId) throws IOException{		
		com.typesafe.config.Config configuration = com.typesafe.config.ConfigFactory.load();
		String getDirectory = configuration.getString("app.audiofilepath");
		
		List<CallInteraction> call_list=call_int_repo.getAllCallInteraction(startId,endId);
		
		
		
		logger.info("call_list size "+call_list.size());
		for(CallInteraction ca:call_list){
			
		long callInteractionId=ca.getId();	
			
		
		CallInteraction callInte = call_int_repo.getCallInteractionById(callInteractionId);
		
		String reno="";
		if(callInte.getVehicle()!=null){
			
			reno=callInte.getVehicle().getVehicleRegNo();
		}
		
		String nameMedia =reno+"_"+callInte.getWyzUser().getUserName()+"_"+callInte.getId();
		
		
		byte[] ConvertedFileBytes = null;	
		
		byte[] bytes=repository.getMediaFileMR(callInteractionId);
		String file_name = "aud_";
		
		File audFile = new File(getDirectory+"//"+file_name+nameMedia+".3gp");
		String absolutePath = audFile.getAbsolutePath();
		logger.info("file_name " + file_name);
		logger.info("getDirectory " + getDirectory);
		//int size = bytes.length;
		
		
		BufferedOutputStream bs = null;
		FileOutputStream fs = null;

		try {

				 fs = new FileOutputStream(audFile);
				bs = new BufferedOutputStream(fs);
				bs.write(bytes);				
				bs.close();
				bs = null;

			} catch (Exception e) {
				e.printStackTrace();
			}

		if (bs != null) try { bs.close(); } catch (Exception e) {}
		String path = "C:/ffmpeg/";
		
		try
		{
			
			File file = null;
			
			try{
			file = new File(absolutePath);
			logger.info("Fileurl " + absolutePath);
			
			if(file.exists())
			{
				logger.info("File existed");
				String filename = "conAud";
				
				 //String infoFile = getDirectory+filename+count+".mp3";
				 String infoFile = getDirectory+nameMedia+".mp3";
				 logger.info("infoFile " + infoFile);
				// String command = "ffmpeg -i \""+ file +"\" ""+infoFile""";
				 String command = "ffmpeg -i \""+ file +"\" "+infoFile+" ";

				logger.info("command " + command);
				
				//Process p = Runtime.getRuntime().exec("cmd /c ffmpeg -i C:/ffmpeg/aud.3gp C:/ffmpeg/nameMedia.mp3");
				Process p = Runtime.getRuntime().exec("cmd.exe /c " + command);
				//count++;
				InputStream in = p.getErrorStream();
				int c;
				
				while ((c = in.read()) != -1)
				{
					System.out.print((char)c);
				}
				in.close();
				try
				{
					ConvertedFileBytes = Files.readAllBytes(new File(infoFile).toPath());	
					logger.info("File converted to bytes");
					 callInte.setMediaFileLob(ConvertedFileBytes);
					 call_int_repo.updateMediaByteData(callInte);
					
				}
				catch(Exception e)
				{
					logger.info("BytesExceptionAllCallInteractionRepo" + e);
				}					
				
				logger.info("File downloaded");
				
			}
			else
			{
				logger.info("File not found!");
			}
			}
			catch(IOException io)
			{
				logger.info("ExceptionIO " + io);
			}
		}catch(Exception fe)
		{
			logger.info("ExceptionMain " + fe);
		}
		
		 
		
	}
		return ok("");
	}
	}