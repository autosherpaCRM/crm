package controllers;


import org.pac4j.play.PlayWebContext;
import org.pac4j.play.store.PlaySessionStore;
import org.pac4j.core.profile.ProfileManager;
import static play.libs.Json.toJson;
import play.data.Form;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.pac4j.core.config.Config;
import org.pac4j.core.profile.CommonProfile;
import org.pac4j.play.java.Secure;


import play.api.Play;
import play.mvc.Result;
import static play.mvc.Results.notFound;
import static play.mvc.Results.ok;
import models.Customer;
import views.html.customerForm;



import org.pac4j.core.profile.CommonProfile;

import org.springframework.stereotype.Controller;

import repositories.CustomerScheduledRepository;

import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ParseDouble;
import org.supercsv.cellprocessor.ParseInt;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.prefs.CsvPreference;




@Controller

public class CustomerScheduledController extends play.mvc.Controller{

	private final CustomerScheduledRepository repo;
	
	private PlaySessionStore playSessionStore;

	
	
	@Inject
	public CustomerScheduledController(CustomerScheduledRepository repository,PlaySessionStore playses){
		repo = repository;
		playSessionStore=playses;
	}
	private CommonProfile getUserProfile() {
		final PlayWebContext context = new PlayWebContext(ctx(), playSessionStore);
		final ProfileManager<CommonProfile> profileManager = new ProfileManager(context);
		//logger.debug("............About to obtain the profiles..........");
		List<CommonProfile> profiles = profileManager.getAll(true);
		//logger.debug("..............Obtained profiles........." + profiles);
		return profiles.get(0);
	}
	public Result readCustomersFromCSV(){
		
		ICsvBeanReader beanReader = null;
		
		
				try {
			
			
			
			beanReader = new CsvBeanReader(new BufferedReader(new InputStreamReader(Play.current().classloader().getResourceAsStream("Customer.csv"))),
					CsvPreference.STANDARD_PREFERENCE);

			
			final String[] header = beanReader.getHeader(true);
			
			final CellProcessor[] processors = getUserDataCellProcessor();
			
			Customer custScheduled = null;
			while ((custScheduled = beanReader.read(Customer.class, header, processors)) != null) {
				// process course

				
				if (custScheduled != null)
					repo.addCustomerData(custScheduled);
				
			}

		} catch (IOException ioe){
			ioe.printStackTrace();
		}
		finally {
			if (beanReader != null)
				try {
					beanReader.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		
		
		List<Customer> custScheduleds = repo.getAllCustScheduleds();
		 Form<Customer> form = Form.form(Customer.class).bindFromRequest();
		return ok("customer whole data uploaded");
// 		custScheduleds.isEmpty() ? notFound("No customer available")
// 		                            : ok(toJson(custScheduleds));
		                          //:ok(customer.render(Form.form(CustomerScheduled.class)));
		
	}
	
		private static CellProcessor[] getUserDataCellProcessor() {
		return new CellProcessor[] { new Optional(),  new Optional(), new Optional(), new Optional(), new Optional(),  new Optional(), new Optional()};
	}
	
	
    //creating the customer
    public Result addcust(){
    	String userloginname = getUserLoginName();
    	String dealername=getDealerName();
    	
        return ok(customerForm.render(dealername,userloginname,Form.form(Customer.class)));
    }
    public Result addcustomerInfo(){
    	String userloginname = getUserLoginName();
        Form<Customer> form=Form.form(Customer.class).bindFromRequest();
	    Customer cust=form.get();
	    Customer savedapplication=repo.addCustomerData(cust);
	    String dealername=getDealerName();
	    return ok(customerForm.render(dealername,userloginname,Form.form(Customer.class)));
         
    }

    private String getUserLoginName() {
		String userloginname = getUserProfile().getId();
		
		return userloginname;

	}
    private String getDealerName() {
		String dealerName=(String)getUserProfile().getAttribute("DEALER_NAME");
		
		return dealerName;

	}
	    
	

	
}
