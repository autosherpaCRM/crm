/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.FileInputStream;
import org.springframework.stereotype.Controller;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import org.apache.commons.io.IOUtils;
import play.Logger;
import play.api.Play;
/**
 *
 * @author W-885
 */
@Controller
public class FirebaseInitializer {

    public void initializeFirebase() throws FileNotFoundException {
        //"D:\\CRM\\WyzCRM\\conf\\WYZCRM-91b67209ecfa.json"
        //Play.current().path("conf/WYZCRM-91b67209ecfa.json")
        
        Logger.ALogger logger = play.Logger.of("application");
         Config configuration = ConfigFactory.load();
         
         List<? extends Config> configs = configuration.getConfigList("firebaseAuth");
         String test=new String();
          test=configs.get(0).toString();
            //logger.info("test :"+test);
            
         String re1=test.replace("Config(SimpleConfigObject(","");
         //logger.info("re1 :"+re1);
         String re2=re1.replace("))", "");
        // logger.info("re2 :"+re2);
            
          InputStream convertedString=IOUtils.toInputStream(re2);
        
          FirebaseOptions options = new FirebaseOptions.Builder()                  
                 .setDatabaseUrl("https://wyzcrm-2feff.firebaseio.com/")  
                 .setServiceAccount(convertedString)
                 .build();
                FirebaseApp.initializeApp(options);
    }
    
}
