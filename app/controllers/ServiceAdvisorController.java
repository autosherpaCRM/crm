package controllers;

import java.util.List;

import javax.inject.Inject;

import org.pac4j.core.profile.CommonProfile;
import org.pac4j.play.java.Secure;


import models.Complaint;
import play.Logger.ALogger;
import play.mvc.Result;
import repositories.CallInteractionsRepository;
import views.html.complaintResolutionSA;
import org.pac4j.play.PlayWebContext;
import org.pac4j.play.store.PlaySessionStore;
import org.pac4j.core.profile.ProfileManager;

public class ServiceAdvisorController extends play.mvc.Controller{
	
	ALogger logger = play.Logger.of("application");
	
	private final CallInteractionsRepository call_int_repo;
	private PlaySessionStore playSessionStore;
	
	@Inject
	public ServiceAdvisorController(CallInteractionsRepository interRepo,PlaySessionStore playSession) {		
		call_int_repo = interRepo;
		playSessionStore =playSession;
	}

	private CommonProfile getUserProfile() {
		final PlayWebContext context = new PlayWebContext(ctx(), playSessionStore);
		final ProfileManager<CommonProfile> profileManager = new ProfileManager(context);
		logger.debug("............About to obtain the profiles..........");
		List<CommonProfile> profiles = profileManager.getAll(true);
		logger.debug("..............Obtained profiles........." + profiles);
		return profiles.get(0);
	}
	
	private String getUserLoginName() {
		String userloginname = getUserProfile().getId();

		return userloginname;

	}

	private String getDealerName() {
		String dealerName = (String) getUserProfile().getAttribute("DEALER_NAME");

		return dealerName;

	}

	private String getUserLogindealerCode() {
		String dealer_Code = (String) getUserProfile().getAttribute("DEALER_CODE");

		return dealer_Code;

	}

	@Secure(clients = "FormClient")
	public Result getServiceAdvisorComplaints(){
		
		List<Complaint> complaintData = call_int_repo.getAllComplaintsForResolution(getUserLoginName());
		List<Complaint> compalintDataclosed = call_int_repo.getClosedComplaintsForResolution(getUserLoginName());
		return ok(complaintResolutionSA.render(getUserLogindealerCode(), getDealerName(), getUserLoginName(),
				complaintData, compalintDataclosed));
	}

}
