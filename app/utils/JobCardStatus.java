package utils;

public class JobCardStatus extends CommanUploadModel {

	public String slNo;
	public String jobCardNo;
	
	public java.sql.Date jobCardDate;
	public java.sql.Date saleDate;
	public Long mileage;
	public Double estLabAmt;
	public Double estPartAmt;
	public java.sql.Date promiseDt;
	public java.sql.Date revPromisedDt;
	public java.sql.Date readyDateandTime;
	public Double labAmt;
	public Double partsAmt;
	public java.sql.Date pickupDate;
	public java.sql.Date billDate;
	public Double billAmt;
	public Long pincode;
	public java.sql.Date dob;
	public java.sql.Date doa;
	public Long year;
	
	public String repeatRevisit;
	public String custName;
	public String phone;
	public String custCat;
	public String psfStatus;
	public String regNo;
	public String chassis;
	public String model;
	public String mi;	
	public String group;
	public String serviceAdvisor;
	public String technician;
	public String circularNo;
	public String serviceType;	
	public String status;
	public String billNo;	
	public String pickupRequired;	
	public String pickupLoc;	
	public String address1;
	public String address2;
	public String address3;
	public String city;	
	public String engineNo;
	public String variant;
	public String color;
	public String mobile;	
	public String centre;
	
	
	public String jobCardDateStr;
	public String saleDateStr;
	public String mileageStr;
	public String estLabAmtStr;
	public String estPartAmtStr;
	public String promiseDtStr;
	public String revPromisedDtStr;
	public String readyDateandTimeStr;
	public String labAmtStr;
	public String partsAmtStr;
	public String pickupDateStr;
	public String billDateStr;
	public String billAmtStr;
	public String pincodeStr;
	public String dobStr;
	public String doaStr;
	public String yearStr;
	

	public String getSlNo() {
		return slNo;
	}

	public void setSlNo(String slNo) {
		this.slNo = slNo;
	}

	public String getBillNo() {
		return billNo;
	}

	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getRepeatRevisit() {
		return repeatRevisit;
	}

	public void setRepeatRevisit(String repeatRevisit) {
		this.repeatRevisit = repeatRevisit;
	}

	public String getJobCardNo() {
		return jobCardNo;
	}

	public void setJobCardNo(String jobCardNo) {
		this.jobCardNo = jobCardNo;
	}

	public java.sql.Date getJobCardDate() {
		return jobCardDate;
	}

	public void setJobCardDate(java.sql.Date jobCardDate) {
		this.jobCardDate = jobCardDate;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCustCat() {
		return custCat;
	}

	public void setCustCat(String custCat) {
		this.custCat = custCat;
	}

	public String getPsfStatus() {
		return psfStatus;
	}

	public void setPsfStatus(String psfStatus) {
		this.psfStatus = psfStatus;
	}

	public String getRegNo() {
		return regNo;
	}

	public void setRegNo(String regNo) {
		this.regNo = regNo;
	}

	public String getChassis() {
		return chassis;
	}

	public void setChassis(String chassis) {
		this.chassis = chassis;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getMi() {
		return mi;
	}

	public void setMi(String mi) {
		this.mi = mi;
	}

	public java.sql.Date getSaleDate() {
		return saleDate;
	}

	public void setSaleDate(java.sql.Date saleDate) {
		this.saleDate = saleDate;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getServiceAdvisor() {
		return serviceAdvisor;
	}

	public void setServiceAdvisor(String serviceAdvisor) {
		this.serviceAdvisor = serviceAdvisor;
	}

	public String getTechnician() {
		return technician;
	}

	public void setTechnician(String technician) {
		this.technician = technician;
	}

	public String getCircularNo() {
		return circularNo;
	}

	public void setCircularNo(String circularNo) {
		this.circularNo = circularNo;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public Long getMileage() {
		return mileage;
	}

	public void setMileage(Long mileage) {
		this.mileage = mileage;
	}

	public Double getEstLabAmt() {
		return estLabAmt;
	}

	public void setEstLabAmt(Double estLabAmt) {
		this.estLabAmt = estLabAmt;
	}

	public Double getEstPartAmt() {
		return estPartAmt;
	}

	public void setEstPartAmt(Double estPartAmt) {
		this.estPartAmt = estPartAmt;
	}

	public java.sql.Date getPromiseDt() {
		return promiseDt;
	}

	public void setPromiseDt(java.sql.Date promiseDt) {
		this.promiseDt = promiseDt;
	}

	public java.sql.Date getRevPromisedDt() {
		return revPromisedDt;
	}

	public void setRevPromisedDt(java.sql.Date revPromisedDt) {
		this.revPromisedDt = revPromisedDt;
	}

	public java.sql.Date getReadyDateandTime() {
		return readyDateandTime;
	}

	public void setReadyDateandTime(java.sql.Date readyDateandTime) {
		this.readyDateandTime = readyDateandTime;
	}

	public Double getLabAmt() {
		return labAmt;
	}

	public void setLabAmt(Double labAmt) {
		this.labAmt = labAmt;
	}

	public Double getPartsAmt() {
		return partsAmt;
	}

	public void setPartsAmt(Double partsAmt) {
		this.partsAmt = partsAmt;
	}

	public String getPickupRequired() {
		return pickupRequired;
	}

	public void setPickupRequired(String pickupRequired) {
		this.pickupRequired = pickupRequired;
	}

	public java.sql.Date getPickupDate() {
		return pickupDate;
	}

	public void setPickupDate(java.sql.Date pickupDate) {
		this.pickupDate = pickupDate;
	}

	public String getPickupLoc() {
		return pickupLoc;
	}

	public void setPickupLoc(String pickupLoc) {
		this.pickupLoc = pickupLoc;
	}

	public java.sql.Date getBillDate() {
		return billDate;
	}

	public void setBillDate(java.sql.Date billDate) {
		this.billDate = billDate;
	}

	public Double getBillAmt() {
		return billAmt;
	}

	public void setBillAmt(Double billAmt) {
		this.billAmt = billAmt;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Long getPincode() {
		return pincode;
	}

	public void setPincode(Long pincode) {
		this.pincode = pincode;
	}

	public java.sql.Date getDob() {
		return dob;
	}

	public void setDob(java.sql.Date dob) {
		this.dob = dob;
	}

	public java.sql.Date getDoa() {
		return doa;
	}

	public void setDoa(java.sql.Date doa) {
		this.doa = doa;
	}

	public String getEngineNo() {
		return engineNo;
	}

	public void setEngineNo(String engineNo) {
		this.engineNo = engineNo;
	}

	public String getVariant() {
		return variant;
	}

	public void setVariant(String variant) {
		this.variant = variant;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Long getYear() {
		return year;
	}

	public void setYear(Long year) {
		this.year = year;
	}

	public String getCentre() {
		return centre;
	}

	public void setCentre(String centre) {
		this.centre = centre;
	}

	public String getJobCardDateStr() {
		return jobCardDateStr;
	}

	public void setJobCardDateStr(String jobCardDateStr) {
		this.jobCardDateStr = jobCardDateStr;
	}

	public String getSaleDateStr() {
		return saleDateStr;
	}

	public void setSaleDateStr(String saleDateStr) {
		this.saleDateStr = saleDateStr;
	}

	public String getMileageStr() {
		return mileageStr;
	}

	public void setMileageStr(String mileageStr) {
		this.mileageStr = mileageStr;
	}

	public String getEstLabAmtStr() {
		return estLabAmtStr;
	}

	public void setEstLabAmtStr(String estLabAmtStr) {
		this.estLabAmtStr = estLabAmtStr;
	}

	public String getEstPartAmtStr() {
		return estPartAmtStr;
	}

	public void setEstPartAmtStr(String estPartAmtStr) {
		this.estPartAmtStr = estPartAmtStr;
	}

	public String getPromiseDtStr() {
		return promiseDtStr;
	}

	public void setPromiseDtStr(String promiseDtStr) {
		this.promiseDtStr = promiseDtStr;
	}

	public String getRevPromisedDtStr() {
		return revPromisedDtStr;
	}

	public void setRevPromisedDtStr(String revPromisedDtStr) {
		this.revPromisedDtStr = revPromisedDtStr;
	}

	public String getReadyDateandTimeStr() {
		return readyDateandTimeStr;
	}

	public void setReadyDateandTimeStr(String readyDateandTimeStr) {
		this.readyDateandTimeStr = readyDateandTimeStr;
	}

	public String getLabAmtStr() {
		return labAmtStr;
	}

	public void setLabAmtStr(String labAmtStr) {
		this.labAmtStr = labAmtStr;
	}

	public String getPartsAmtStr() {
		return partsAmtStr;
	}

	public void setPartsAmtStr(String partsAmtStr) {
		this.partsAmtStr = partsAmtStr;
	}

	public String getPickupDateStr() {
		return pickupDateStr;
	}

	public void setPickupDateStr(String pickupDateStr) {
		this.pickupDateStr = pickupDateStr;
	}

	public String getBillDateStr() {
		return billDateStr;
	}

	public void setBillDateStr(String billDateStr) {
		this.billDateStr = billDateStr;
	}

	public String getBillAmtStr() {
		return billAmtStr;
	}

	public void setBillAmtStr(String billAmtStr) {
		this.billAmtStr = billAmtStr;
	}

	public String getPincodeStr() {
		return pincodeStr;
	}

	public void setPincodeStr(String pincodeStr) {
		this.pincodeStr = pincodeStr;
	}

	public String getDobStr() {
		return dobStr;
	}

	public void setDobStr(String dobStr) {
		this.dobStr = dobStr;
	}

	public String getDoaStr() {
		return doaStr;
	}

	public void setDoaStr(String doaStr) {
		this.doaStr = doaStr;
	}

	public String getYearStr() {
		return yearStr;
	}

	public void setYearStr(String yearStr) {
		this.yearStr = yearStr;
	}
	
	
}
