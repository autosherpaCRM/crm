package utils;

import java.sql.Date;

import javax.persistence.Column;

public class SaleRegisterExcel extends CommanUploadModel {

	public String outlet;
	public String locCode;
	public String saleType;
	public String status;
	public String invNo;
	public java.sql.Date invDate;
	public String invCancelNo;
	public java.sql.Date invCancelDate;
	public java.sql.Date delDate;
	public java.sql.Date promisedDelvDate;
	public String model;
	public String fuelType;
	public String variantCode;
	public String varaintDesc;
	public String colorcode;
	public String colorDesc;
	public String chassis;
	public String engine;
	public String mulInvNo;
	public java.sql.Date mulInvDate;
	public String customerId;
	public String salutation;
	public String customerName;
	public String address1;
	public String address2;
	public String address3;
	public String city;
	public Long pincode;
	public String picDesc;
	public String teamLead;
	public String dse;
	public String dsa;
	public String hypthecation;
	public String hyp_address;
	public Double hypAmt;
	public String regNo;
	public String exWarrantyType;
	public String stdCode;
	public String resCode;
	public String stdCodeComp;
	public String offPhone;
	public String mobile1;
	public String mobile2;
	public String emailId;
	public Double basicPrice;
	public Double additionalTax;
	public Double schemeOfGoi;
	public Double instiCust;
	public Double csdExShowRoom;
	public Double loyaltyBonusDisc;
	public Double discount;
	public Double vat;
	public Double roadTax;
	public Double insurance;
	public Double extWarranty;
	public Double sum;
	public Double roundOff;
	public Double ivnAmt;
	public Double purchasePrice;
	public Double panNo;
	public String enquiryNo;
	public String enquirySources;
	public String enquirySubSource;
	public String buyerType;
	public String tradeIn;
	public String indivScheme;
	public String corporate;
	public String orderNum;
	public java.sql.Date orderDate;
	public java.sql.Date allotment;
	public String area;
	public String areaType;
	public Double mgaSoldAmt;
	public String evaluatorMSPIN;
	public String oldCarRegNo;
	public String oldCarMfg;
	public String oldCarModelCode;
	public String oldCarStatus;
	public String oldCarOwner;
	public String oldCarRelation;
	public java.sql.Date exchangeCancDate;
	public String exchCancReason;
	public String refType;
	public String refby;
	public String refNo;
	public String refRegNo;
	public String refMobileNo;
	public String miname;
	public String miFlag;
	public java.sql.Date miDate;
	public String stateDesc;
	public String district;
	public String tehsilDesc;
	public String villageDesc;
	public String ceName;
	public String year;

	public String invDateStr;

	public String invCancelDateStr;

	public String delDateStr;

	public String promisedDelvDateStr;

	public String mulInvDateStr;

	public String pincodeStr;

	public String hypAmtStr;

	public String basicPriceStr;

	public String additionalTaxStr;

	public String schemeOfGoiStr;

	public String instiCustStr;

	public String csdExShowRoomStr;

	public String loyaltyBonusDiscStr;

	public String discountStr;

	public String vatStr;

	public String roadTaxStr;

	public String insuranceStr;

	public String extWarrantyStr;

	public String sumStr;

	public String roundOffStr;

	public String ivnAmtStr;

	public String purchasePriceStr;

	public String panNoStr;

	public String orderDateStr;

	public String allotmentStr;

	public String mgaSoldAmtStr;

	public String exchangeCancDateStr;

	public String miDateStr;

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

	public String getInvCancelNo() {
		return invCancelNo;
	}

	public void setInvCancelNo(String invCancelNo) {
		this.invCancelNo = invCancelNo;
	}

	public java.sql.Date getInvCancelDate() {
		return invCancelDate;
	}

	public void setInvCancelDate(java.sql.Date invCancelDate) {
		this.invCancelDate = invCancelDate;
	}

	public java.sql.Date getPromisedDelvDate() {
		return promisedDelvDate;
	}

	public void setPromisedDelvDate(java.sql.Date promisedDelvDate) {
		this.promisedDelvDate = promisedDelvDate;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getExWarrantyType() {
		return exWarrantyType;
	}

	public void setExWarrantyType(String exWarrantyType) {
		this.exWarrantyType = exWarrantyType;
	}

	public String getStdCodeComp() {
		return stdCodeComp;
	}

	public void setStdCodeComp(String stdCodeComp) {
		this.stdCodeComp = stdCodeComp;
	}

	public String getBuyerType() {
		return buyerType;
	}

	public void setBuyerType(String buyerType) {
		this.buyerType = buyerType;
	}

	public String getTradeIn() {
		return tradeIn;
	}

	public void setTradeIn(String tradeIn) {
		this.tradeIn = tradeIn;
	}

	public String getAreaType() {
		return areaType;
	}

	public void setAreaType(String areaType) {
		this.areaType = areaType;
	}

	public String getCeName() {
		return ceName;
	}

	public void setCeName(String ceName) {
		this.ceName = ceName;
	}

	public Double getRoadTax() {
		return roadTax;
	}

	public void setRoadTax(Double roadTax) {
		this.roadTax = roadTax;
	}

	public Double getInsurance() {
		return insurance;
	}

	public void setInsurance(Double insurance) {
		this.insurance = insurance;
	}

	public Double getExtWarranty() {
		return extWarranty;
	}

	public void setExtWarranty(Double extWarranty) {
		this.extWarranty = extWarranty;
	}

	public Double getPanNo() {
		return panNo;
	}

	public void setPanNo(Double panNo) {
		this.panNo = panNo;
	}

	public String getEnquirySubSource() {
		return enquirySubSource;
	}

	public void setEnquirySubSource(String enquirySubSource) {
		this.enquirySubSource = enquirySubSource;
	}

	public String getIndivScheme() {
		return indivScheme;
	}

	public void setIndivScheme(String indivScheme) {
		this.indivScheme = indivScheme;
	}

	public String getCorporate() {
		return corporate;
	}

	public void setCorporate(String corporate) {
		this.corporate = corporate;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getOffPhone() {
		return offPhone;
	}

	public void setOffPhone(String offPhone) {
		this.offPhone = offPhone;
	}

	public String getMobile1() {
		return mobile1;
	}

	public void setMobile1(String mobile1) {
		this.mobile1 = mobile1;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getTeamLead() {
		return teamLead;
	}

	public void setTeamLead(String teamLead) {
		this.teamLead = teamLead;
	}

	public String getDse() {
		return dse;
	}

	public void setDse(String dse) {
		this.dse = dse;
	}

	public String getDsa() {
		return dsa;
	}

	public void setDsa(String dsa) {
		this.dsa = dsa;
	}

	public String getHypthecation() {
		return hypthecation;
	}

	public void setHypthecation(String hypthecation) {
		this.hypthecation = hypthecation;
	}

	public String getHyp_address() {
		return hyp_address;
	}

	public void setHyp_address(String hyp_address) {
		this.hyp_address = hyp_address;
	}

	public Double getHypAmt() {
		return hypAmt;
	}

	public void setHypAmt(Double hypAmt) {
		this.hypAmt = hypAmt;
	}

	public String getRegNo() {
		return regNo;
	}

	public void setRegNo(String regNo) {
		this.regNo = regNo;
	}

	public String getStdCode() {
		return stdCode;
	}

	public void setStdCode(String stdCode) {
		this.stdCode = stdCode;
	}

	public String getResCode() {
		return resCode;
	}

	public void setResCode(String resCode) {
		this.resCode = resCode;
	}

	public String getMobile2() {
		return mobile2;
	}

	public void setMobile2(String mobile2) {
		this.mobile2 = mobile2;
	}

	public String getSalutation() {
		return salutation;
	}

	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	public java.sql.Date getInvDate() {
		return invDate;
	}

	public void setInvDate(java.sql.Date invDate) {
		this.invDate = invDate;
	}

	public String getOutlet() {
		return outlet;
	}

	public void setOutlet(String outlet) {
		this.outlet = outlet;
	}

	public String getLocCode() {
		return locCode;
	}

	public void setLocCode(String locCode) {
		this.locCode = locCode;
	}

	public String getSaleType() {
		return saleType;
	}

	public void setSaleType(String saleType) {
		this.saleType = saleType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getInvNo() {
		return invNo;
	}

	public void setInvNo(String invNo) {
		this.invNo = invNo;
	}

	public java.sql.Date getDelDate() {
		return delDate;
	}

	public void setDelDate(java.sql.Date delDate) {
		this.delDate = delDate;
	}

	public String getFuelType() {
		return fuelType;
	}

	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}

	public String getVariantCode() {
		return variantCode;
	}

	public void setVariantCode(String variantCode) {
		this.variantCode = variantCode;
	}

	public String getVaraintDesc() {
		return varaintDesc;
	}

	public void setVaraintDesc(String varaintDesc) {
		this.varaintDesc = varaintDesc;
	}

	public String getColorcode() {
		return colorcode;
	}

	public void setColorcode(String colorcode) {
		this.colorcode = colorcode;
	}

	public String getColorDesc() {
		return colorDesc;
	}

	public void setColorDesc(String colorDesc) {
		this.colorDesc = colorDesc;
	}

	public String getChassis() {
		return chassis;
	}

	public void setChassis(String chassis) {
		this.chassis = chassis;
	}

	public String getEngine() {
		return engine;
	}

	public void setEngine(String engine) {
		this.engine = engine;
	}

	public String getMulInvNo() {
		return mulInvNo;
	}

	public void setMulInvNo(String mulInvNo) {
		this.mulInvNo = mulInvNo;
	}

	public java.sql.Date getMulInvDate() {
		return mulInvDate;
	}

	public void setMulInvDate(java.sql.Date mulInvDate) {
		this.mulInvDate = mulInvDate;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Double getInstiCust() {
		return instiCust;
	}

	public void setInstiCust(Double instiCust) {
		this.instiCust = instiCust;
	}

	public Long getPincode() {
		return pincode;
	}

	public void setPincode(Long pincode) {
		this.pincode = pincode;
	}

	public String getPicDesc() {
		return picDesc;
	}

	public void setPicDesc(String picDesc) {
		this.picDesc = picDesc;
	}

	public Double getBasicPrice() {
		return basicPrice;
	}

	public void setBasicPrice(Double basicPrice) {
		this.basicPrice = basicPrice;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Double getVat() {
		return vat;
	}

	public void setVat(Double vat) {
		this.vat = vat;
	}

	public Double getSum() {
		return sum;
	}

	public void setSum(Double sum) {
		this.sum = sum;
	}

	public Double getRoundOff() {
		return roundOff;
	}

	public void setRoundOff(Double roundOff) {
		this.roundOff = roundOff;
	}

	public Double getIvnAmt() {
		return ivnAmt;
	}

	public void setIvnAmt(Double ivnAmt) {
		this.ivnAmt = ivnAmt;
	}

	public String getEnquiryNo() {
		return enquiryNo;
	}

	public void setEnquiryNo(String enquiryNo) {
		this.enquiryNo = enquiryNo;
	}

	public String getEnquirySources() {
		return enquirySources;
	}

	public void setEnquirySources(String enquirySources) {
		this.enquirySources = enquirySources;
	}

	public Double getAdditionalTax() {
		return additionalTax;
	}

	public void setAdditionalTax(Double additionalTax) {
		this.additionalTax = additionalTax;
	}

	public Double getSchemeOfGoi() {
		return schemeOfGoi;
	}

	public void setSchemeOfGoi(Double schemeOfGoi) {
		this.schemeOfGoi = schemeOfGoi;
	}

	public Double getCsdExShowRoom() {
		return csdExShowRoom;
	}

	public void setCsdExShowRoom(Double csdExShowRoom) {
		this.csdExShowRoom = csdExShowRoom;
	}

	public Double getLoyaltyBonusDisc() {
		return loyaltyBonusDisc;
	}

	public void setLoyaltyBonusDisc(Double loyaltyBonusDisc) {
		this.loyaltyBonusDisc = loyaltyBonusDisc;
	}

	public Double getPurchasePrice() {
		return purchasePrice;
	}

	public void setPurchasePrice(Double purchasePrice) {
		this.purchasePrice = purchasePrice;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public Date getAllotment() {
		return allotment;
	}

	public void setAllotment(Date allotment) {
		this.allotment = allotment;
	}

	public Double getMgaSoldAmt() {
		return mgaSoldAmt;
	}

	public void setMgaSoldAmt(Double mgaSoldAmt) {
		this.mgaSoldAmt = mgaSoldAmt;
	}

	public String getEvaluatorMSPIN() {
		return evaluatorMSPIN;
	}

	public void setEvaluatorMSPIN(String evaluatorMSPIN) {
		this.evaluatorMSPIN = evaluatorMSPIN;
	}

	public String getOldCarRegNo() {
		return oldCarRegNo;
	}

	public void setOldCarRegNo(String oldCarRegNo) {
		this.oldCarRegNo = oldCarRegNo;
	}

	public String getOldCarMfg() {
		return oldCarMfg;
	}

	public void setOldCarMfg(String oldCarMfg) {
		this.oldCarMfg = oldCarMfg;
	}

	public String getOldCarModelCode() {
		return oldCarModelCode;
	}

	public void setOldCarModelCode(String oldCarModelCode) {
		this.oldCarModelCode = oldCarModelCode;
	}

	public String getOldCarStatus() {
		return oldCarStatus;
	}

	public void setOldCarStatus(String oldCarStatus) {
		this.oldCarStatus = oldCarStatus;
	}

	public String getOldCarOwner() {
		return oldCarOwner;
	}

	public void setOldCarOwner(String oldCarOwner) {
		this.oldCarOwner = oldCarOwner;
	}

	public String getOldCarRelation() {
		return oldCarRelation;
	}

	public void setOldCarRelation(String oldCarRelation) {
		this.oldCarRelation = oldCarRelation;
	}

	public Date getExchangeCancDate() {
		return exchangeCancDate;
	}

	public void setExchangeCancDate(Date exchangeCancDate) {
		this.exchangeCancDate = exchangeCancDate;
	}

	public String getExchCancReason() {
		return exchCancReason;
	}

	public void setExchCancReason(String exchCancReason) {
		this.exchCancReason = exchCancReason;
	}

	public String getRefType() {
		return refType;
	}

	public void setRefType(String refType) {
		this.refType = refType;
	}

	public String getRefby() {
		return refby;
	}

	public void setRefby(String refby) {
		this.refby = refby;
	}

	public String getRefNo() {
		return refNo;
	}

	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}

	public String getRefRegNo() {
		return refRegNo;
	}

	public void setRefRegNo(String refRegNo) {
		this.refRegNo = refRegNo;
	}

	public String getRefMobileNo() {
		return refMobileNo;
	}

	public void setRefMobileNo(String refMobileNo) {
		this.refMobileNo = refMobileNo;
	}

	public String getMiname() {
		return miname;
	}

	public void setMiname(String miname) {
		this.miname = miname;
	}

	public String getMiFlag() {
		return miFlag;
	}

	public void setMiFlag(String miFlag) {
		this.miFlag = miFlag;
	}

	public Date getMiDate() {
		return miDate;
	}

	public void setMiDate(Date miDate) {
		this.miDate = miDate;
	}

	public String getStateDesc() {
		return stateDesc;
	}

	public void setStateDesc(String stateDesc) {
		this.stateDesc = stateDesc;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getTehsilDesc() {
		return tehsilDesc;
	}

	public void setTehsilDesc(String tehsilDesc) {
		this.tehsilDesc = tehsilDesc;
	}

	public String getVillageDesc() {
		return villageDesc;
	}

	public void setVillageDesc(String villageDesc) {
		this.villageDesc = villageDesc;
	}

	public String getInvDateStr() {
		return invDateStr;
	}

	public void setInvDateStr(String invDateStr) {
		this.invDateStr = invDateStr;
	}

	public String getInvCancelDateStr() {
		return invCancelDateStr;
	}

	public void setInvCancelDateStr(String invCancelDateStr) {
		this.invCancelDateStr = invCancelDateStr;
	}

	public String getDelDateStr() {
		return delDateStr;
	}

	public void setDelDateStr(String delDateStr) {
		this.delDateStr = delDateStr;
	}

	public String getPromisedDelvDateStr() {
		return promisedDelvDateStr;
	}

	public void setPromisedDelvDateStr(String promisedDelvDateStr) {
		this.promisedDelvDateStr = promisedDelvDateStr;
	}

	public String getMulInvDateStr() {
		return mulInvDateStr;
	}

	public void setMulInvDateStr(String mulInvDateStr) {
		this.mulInvDateStr = mulInvDateStr;
	}

	public String getPincodeStr() {
		return pincodeStr;
	}

	public void setPincodeStr(String pincodeStr) {
		this.pincodeStr = pincodeStr;
	}

	public String getHypAmtStr() {
		return hypAmtStr;
	}

	public void setHypAmtStr(String hypAmtStr) {
		this.hypAmtStr = hypAmtStr;
	}

	public String getBasicPriceStr() {
		return basicPriceStr;
	}

	public void setBasicPriceStr(String basicPriceStr) {
		this.basicPriceStr = basicPriceStr;
	}

	public String getAdditionalTaxStr() {
		return additionalTaxStr;
	}

	public void setAdditionalTaxStr(String additionalTaxStr) {
		this.additionalTaxStr = additionalTaxStr;
	}

	public String getSchemeOfGoiStr() {
		return schemeOfGoiStr;
	}

	public void setSchemeOfGoiStr(String schemeOfGoiStr) {
		this.schemeOfGoiStr = schemeOfGoiStr;
	}

	public String getInstiCustStr() {
		return instiCustStr;
	}

	public void setInstiCustStr(String instiCustStr) {
		this.instiCustStr = instiCustStr;
	}

	public String getCsdExShowRoomStr() {
		return csdExShowRoomStr;
	}

	public void setCsdExShowRoomStr(String csdExShowRoomStr) {
		this.csdExShowRoomStr = csdExShowRoomStr;
	}

	public String getLoyaltyBonusDiscStr() {
		return loyaltyBonusDiscStr;
	}

	public void setLoyaltyBonusDiscStr(String loyaltyBonusDiscStr) {
		this.loyaltyBonusDiscStr = loyaltyBonusDiscStr;
	}

	public String getDiscountStr() {
		return discountStr;
	}

	public void setDiscountStr(String discountStr) {
		this.discountStr = discountStr;
	}

	public String getVatStr() {
		return vatStr;
	}

	public void setVatStr(String vatStr) {
		this.vatStr = vatStr;
	}

	public String getRoadTaxStr() {
		return roadTaxStr;
	}

	public void setRoadTaxStr(String roadTaxStr) {
		this.roadTaxStr = roadTaxStr;
	}

	public String getInsuranceStr() {
		return insuranceStr;
	}

	public void setInsuranceStr(String insuranceStr) {
		this.insuranceStr = insuranceStr;
	}

	public String getExtWarrantyStr() {
		return extWarrantyStr;
	}

	public void setExtWarrantyStr(String extWarrantyStr) {
		this.extWarrantyStr = extWarrantyStr;
	}

	public String getSumStr() {
		return sumStr;
	}

	public void setSumStr(String sumStr) {
		this.sumStr = sumStr;
	}

	public String getRoundOffStr() {
		return roundOffStr;
	}

	public void setRoundOffStr(String roundOffStr) {
		this.roundOffStr = roundOffStr;
	}

	public String getIvnAmtStr() {
		return ivnAmtStr;
	}

	public void setIvnAmtStr(String ivnAmtStr) {
		this.ivnAmtStr = ivnAmtStr;
	}

	public String getPurchasePriceStr() {
		return purchasePriceStr;
	}

	public void setPurchasePriceStr(String purchasePriceStr) {
		this.purchasePriceStr = purchasePriceStr;
	}

	public String getPanNoStr() {
		return panNoStr;
	}

	public void setPanNoStr(String panNoStr) {
		this.panNoStr = panNoStr;
	}

	public String getOrderDateStr() {
		return orderDateStr;
	}

	public void setOrderDateStr(String orderDateStr) {
		this.orderDateStr = orderDateStr;
	}

	public String getAllotmentStr() {
		return allotmentStr;
	}

	public void setAllotmentStr(String allotmentStr) {
		this.allotmentStr = allotmentStr;
	}

	public String getMgaSoldAmtStr() {
		return mgaSoldAmtStr;
	}

	public void setMgaSoldAmtStr(String mgaSoldAmtStr) {
		this.mgaSoldAmtStr = mgaSoldAmtStr;
	}

	public String getExchangeCancDateStr() {
		return exchangeCancDateStr;
	}

	public void setExchangeCancDateStr(String exchangeCancDateStr) {
		this.exchangeCancDateStr = exchangeCancDateStr;
	}

	public String getMiDateStr() {
		return miDateStr;
	}

	public void setMiDateStr(String miDateStr) {
		this.miDateStr = miDateStr;
	}
	
	

}
