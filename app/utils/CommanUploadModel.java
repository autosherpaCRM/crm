package utils;

import java.util.HashMap;

public class CommanUploadModel {

	public int rowNumber;
	public boolean isError;
	public HashMap<String, String> errorInfo;

	
	
	public int getRowNumber() {
		return rowNumber;
	}

	public void setRowNumber(int rowNumber) {
		this.rowNumber = rowNumber;
	}

	public boolean isError() {
		return isError;
	}

	public void setError(boolean isError) {
		this.isError = isError;
	}

	public HashMap<String, String> getErrorInfo() {
		return errorInfo;
	}

	public void setErrorInfo(HashMap<String, String> errorInfo) {
		this.errorInfo = errorInfo;
	}

}
