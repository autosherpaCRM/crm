package utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.PriorityBlockingQueue;
import org.springframework.stereotype.Component;

import play.Logger.ALogger;
import controllers.webmodels.ServiceAdvisorIdName;
import models.ServiceAdvisor;
import play.cache.Cache;
import repositories.AutoSelectionSARepository;
import org.springframework.beans.factory.annotation.Autowired;


@Component
public class WyzCRMCache {
	ALogger logger = play.Logger.of("application");
	public static final String SEPARATOR="$";
	
	@Autowired(required=true)
	private AutoSelectionSARepository saRepo;
	
	
	public void setupServiceAdvisorQueue(Date date, Long workshop, List<ServiceAdvisorIdName> serviceAdvisors,String dealercodeIs){
		logger.info("Inside Wyzchzche setupServiceAdvisorQueue");
		PriorityBlockingQueue<ServiceAdvisorIdName> serviceAdvisorQueue = new PriorityBlockingQueue<ServiceAdvisorIdName>(serviceAdvisors);
		String key = getCacheKey(date,workshop,dealercodeIs);
		Cache.set(key, serviceAdvisorQueue);
	}
	
	public List<ServiceAdvisorIdName> getServiceAdvisorRecomendation(Date date, Long workshopId,String dealercodeIs) throws Exception{
		String key = getCacheKey(date, workshopId,dealercodeIs);
		Object cacheObject =  Cache.get(key);
		try {
			if(cacheObject ==  null) throw new Exception("Data");
		} catch (Exception e) {
			Date newDate = new Date();
			
			Calendar c = Calendar.getInstance();
			c.setTime(new Date()); // Now use today date.
			c.add(Calendar.DATE, 30); // Adding 5 days
			newDate = c.getTime();
			
			if((newDate.compareTo(date)) < 0 ){ //newDate < date, returns less than 0  // date is greater than 30 days from current Date.
				logger.info("Inside Wyzchzche > 30");
				List<ServiceAdvisorIdName> serviceAdvisors = saRepo.getSaDetails(date, workshopId);
				setupServiceAdvisorQueue(date,workshopId,serviceAdvisors,dealercodeIs);
			}
			else{
				logger.info("Inside Wyzchzche < 30 and initial setup");
				saRepo.getAllSaDetails(dealercodeIs);
			}
			cacheObject = Cache.get(key);
			if(cacheObject ==  null) throw new Exception("Details are not availabe for that Date");
		}
		PriorityBlockingQueue <ServiceAdvisorIdName> serviceAdvisorQueue = (PriorityBlockingQueue<ServiceAdvisorIdName>)cacheObject;
		List<ServiceAdvisorIdName> saList = new ArrayList<ServiceAdvisorIdName>(serviceAdvisorQueue);
		ServiceAdvisorIdName sa = serviceAdvisorQueue.take();
		sa.setPriority(sa.getPriority()+1);
		logger.info("Logger SA:" + sa.getAdvisorName());
		serviceAdvisorQueue.put(sa);
		return saList;
	}

	public void getServiceAdvisorList(Date date, Long workshopId,ServiceAdvisorIdName saDetails,String dealerId) throws Exception{
		logger.info("Inside Wyzchzche getServiceAdvisorList");
		logger.info("Inside Wyzchzche getServiceAdvisorList preSA :" + saDetails.getAdvisorName());
		String key = getCacheKey(date, workshopId,dealerId);
		Object cacheObject = Cache.get(key);
		if(cacheObject ==  null) throw new Exception("Service Advisor Cache is not setup");
		PriorityBlockingQueue <ServiceAdvisorIdName> serviceAdvisorQueue = (PriorityBlockingQueue<ServiceAdvisorIdName>)cacheObject;
		serviceAdvisorQueue.remove(saDetails);
		saDetails.setPriority(saDetails.getPriority()-1);
		serviceAdvisorQueue.put(saDetails);
	}
	
	public void updateServiceAdvisorChange(Date date, Long workshopId,ServiceAdvisorIdName preSelectedSaDetails,ServiceAdvisorIdName newSelectedSaDetails,String dealerId) throws Exception{
		logger.info("Inside Wyzchzche updateServiceAdvisorChange");
		String key = getCacheKey(date, workshopId,dealerId);
		Object cacheObject = Cache.get(key);
		if(cacheObject ==  null) throw new Exception("Service Advisor Cache is not setup");
		PriorityBlockingQueue <ServiceAdvisorIdName> serviceAdvisorQueue = (PriorityBlockingQueue<ServiceAdvisorIdName>)cacheObject;
		if(preSelectedSaDetails != null)
		{
			serviceAdvisorQueue.remove(preSelectedSaDetails);
			preSelectedSaDetails.setPriority(preSelectedSaDetails.getPriority()-1);
			serviceAdvisorQueue.put(preSelectedSaDetails);
		}
		if(newSelectedSaDetails != null)
		{
			serviceAdvisorQueue.remove(newSelectedSaDetails);
			preSelectedSaDetails.setPriority(newSelectedSaDetails.getPriority()+1);
			serviceAdvisorQueue.put(newSelectedSaDetails);
		}
	}
	
	private String getCacheKey(Date date, Long workshopId,String dealercodeIs){
		String pattern = "dd-MM-yyyy";
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		String dateStr = formatter.format(date);
		String key = dateStr+SEPARATOR+workshopId+SEPARATOR+dealercodeIs;
		return key;
	}
	
	public void evictCacheFor(Date date, Long workshopId,String dealercodeIs){
		String key = getCacheKey(date,workshopId,dealercodeIs);
		Cache.remove(key);
	}
}
