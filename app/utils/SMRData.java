package utils;

public class SMRData extends CommanUploadModel {

	public String followUpType;
	public String followUpNum;
	public java.sql.Date followUpOld;
	public java.sql.Date followUpNew;
	public String regNo;
	public String model;
	public String fuelType;
	public String chassis;
	public Long mileage;
	public java.sql.Date dueDate;
	public String dueService;
	public String lastServiceType;
	public java.sql.Date lastServiceDate;
	public java.sql.Date saleDate;
	public String custCat;
	public String custName;
	public String address;
	public String telephone;
	public String mobile;
	public String custContactStatus;
	public String currentJC;
	public String serviceType;
	public String Status;
	public String nextFollowupDate;
	public String custPickUpType;
	public String custEmailIdStatus;
	public String lastfollowUpRemarks;
	public String followUpRemarks;
	public String followUpResponse;
	public java.sql.Date deliveryDate;
	public String pickupDrop;
	public String extWarranty;
	
	public String lastServiceDateStr;
	public String saleDateStr;
	public String followUpOldStr;
	public String followUpNewStr;
	public String mileageStr;
	public String dueDateStr;
	public String deliveryDateStr;

	public String getCurrentJC() {
		return currentJC;
	}

	public void setCurrentJC(String currentJC) {
		this.currentJC = currentJC;
	}

	public String getFollowUpType() {
		return followUpType;
	}

	public void setFollowUpType(String followUpType) {
		this.followUpType = followUpType;
	}

	public String getFollowUpNum() {
		return followUpNum;
	}

	public void setFollowUpNum(String followUpNum) {
		this.followUpNum = followUpNum;
	}

	public java.sql.Date getFollowUpOld() {
		return followUpOld;
	}

	public void setFollowUpOld(java.sql.Date followUpOld) {
		this.followUpOld = followUpOld;
	}

	public java.sql.Date getFollowUpNew() {
		return followUpNew;
	}

	public void setFollowUpNew(java.sql.Date followUpNew) {
		this.followUpNew = followUpNew;
	}

	public String getRegNo() {
		return regNo;
	}

	public void setRegNo(String regNo) {
		this.regNo = regNo;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getFuelType() {
		return fuelType;
	}

	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}

	public String getChassis() {
		return chassis;
	}

	public void setChassis(String chassis) {
		this.chassis = chassis;
	}

	public Long getMileage() {
		return mileage;
	}

	public void setMileage(Long mileage) {
		this.mileage = mileage;
	}

	public java.sql.Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(java.sql.Date dueDate) {
		this.dueDate = dueDate;
	}

	public String getDueService() {
		return dueService;
	}

	public void setDueService(String dueService) {
		this.dueService = dueService;
	}

	public String getLastServiceType() {
		return lastServiceType;
	}

	public void setLastServiceType(String lastServiceType) {
		this.lastServiceType = lastServiceType;
	}

	public java.sql.Date getLastServiceDate() {
		return lastServiceDate;
	}

	public void setLastServiceDate(java.sql.Date lastServiceDate) {
		this.lastServiceDate = lastServiceDate;
	}

	public java.sql.Date getSaleDate() {
		return saleDate;
	}

	public void setSaleDate(java.sql.Date saleDate) {
		this.saleDate = saleDate;
	}

	public String getCustCat() {
		return custCat;
	}

	public void setCustCat(String custCat) {
		this.custCat = custCat;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getCustContactStatus() {
		return custContactStatus;
	}

	public void setCustContactStatus(String custContactStatus) {
		this.custContactStatus = custContactStatus;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getNextFollowupDate() {
		return nextFollowupDate;
	}

	public void setNextFollowupDate(String nextFollowupDate) {
		this.nextFollowupDate = nextFollowupDate;
	}

	public String getCustPickUpType() {
		return custPickUpType;
	}

	public void setCustPickUpType(String custPickUpType) {
		this.custPickUpType = custPickUpType;
	}

	public String getCustEmailIdStatus() {
		return custEmailIdStatus;
	}

	public void setCustEmailIdStatus(String custEmailIdStatus) {
		this.custEmailIdStatus = custEmailIdStatus;
	}

	public String getLastfollowUpRemarks() {
		return lastfollowUpRemarks;
	}

	public void setLastfollowUpRemarks(String lastfollowUpRemarks) {
		this.lastfollowUpRemarks = lastfollowUpRemarks;
	}

	public String getFollowUpRemarks() {
		return followUpRemarks;
	}

	public void setFollowUpRemarks(String followUpRemarks) {
		this.followUpRemarks = followUpRemarks;
	}

	public String getFollowUpResponse() {
		return followUpResponse;
	}

	public void setFollowUpResponse(String followUpResponse) {
		this.followUpResponse = followUpResponse;
	}

	public java.sql.Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(java.sql.Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getPickupDrop() {
		return pickupDrop;
	}

	public void setPickupDrop(String pickupDrop) {
		this.pickupDrop = pickupDrop;
	}

	public String getExtWarranty() {
		return extWarranty;
	}

	public void setExtWarranty(String extWarranty) {
		this.extWarranty = extWarranty;
	}

	public String getLastServiceDateStr() {
		return lastServiceDateStr;
	}

	public void setLastServiceDateStr(String lastServiceDateStr) {
		this.lastServiceDateStr = lastServiceDateStr;
	}

	public String getSaleDateStr() {
		return saleDateStr;
	}

	public void setSaleDateStr(String saleDateStr) {
		this.saleDateStr = saleDateStr;
	}

	public String getFollowUpOldStr() {
		return followUpOldStr;
	}

	public void setFollowUpOldStr(String followUpOldStr) {
		this.followUpOldStr = followUpOldStr;
	}

	public String getFollowUpNewStr() {
		return followUpNewStr;
	}

	public void setFollowUpNewStr(String followUpNewStr) {
		this.followUpNewStr = followUpNewStr;
	}

	public String getMileageStr() {
		return mileageStr;
	}

	public void setMileageStr(String mileageStr) {
		this.mileageStr = mileageStr;
	}

	public String getDueDateStr() {
		return dueDateStr;
	}

	public void setDueDateStr(String dueDateStr) {
		this.dueDateStr = dueDateStr;
	}

	public String getDeliveryDateStr() {
		return deliveryDateStr;
	}

	public void setDeliveryDateStr(String deliveryDateStr) {
		this.deliveryDateStr = deliveryDateStr;
	}
	
	
		
	
}
