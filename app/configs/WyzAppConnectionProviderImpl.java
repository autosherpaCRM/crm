package configs;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;
import org.hibernate.service.UnknownUnwrapTypeException;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.mchange.v2.c3p0.DataSources;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import play.Logger.ALogger;

public class WyzAppConnectionProviderImpl implements ConnectionProvider {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4L;
	ALogger logger = play.Logger.of("application");
	
	private ComboPooledDataSource comboPooleddataSource;
	private DataSource dataSource;

	public WyzAppConnectionProviderImpl(String databaseName) {
		
		comboPooleddataSource = new ComboPooledDataSource();
		Config configuration = ConfigFactory.load();
		String driverClass = configuration.getString("db.spring.driver");
		String dbUrl = configuration.getString("db.spring.url");
		String user = configuration.getString("db.spring.user");
		String password = configuration.getString("db.spring.password");
		
	      int index = dbUrl.lastIndexOf("/");
	      String newdbUrl = dbUrl.substring(0, index);
	      newdbUrl+="/";
	      newdbUrl+=databaseName;  
	      logger.info("Database Url:"+newdbUrl);

		
		
		try {
			comboPooleddataSource.setDriverClass(driverClass);
		} catch (PropertyVetoException e) {
			logger.info("Could not set the sriver class:" + driverClass);
			
			e.printStackTrace();
		}
		comboPooleddataSource.setJdbcUrl(newdbUrl);
		comboPooleddataSource.setUser(user);
		comboPooleddataSource.setPassword(password);

		// the settings below are optional -- c3p0 can work with defaults
		comboPooleddataSource.setMinPoolSize(3);
		comboPooleddataSource.setAcquireIncrement(2);
		comboPooleddataSource.setMaxPoolSize(9);
		comboPooleddataSource.setMaxIdleTime(3000);
		
		logger.info(" newdbUrl : "+newdbUrl);
		final DataSource unpooled;
		try {
			unpooled = DataSources.unpooledDataSource(newdbUrl, user, password);
			
			Properties c3props = comboPooleddataSource.getProperties();
			
			dataSource = DataSources.pooledDataSource(unpooled, c3props);
			
		} catch (SQLException e) {
			logger.info("Could not obtain the datasource object");
			e.printStackTrace();
		}


		
	}

	@Override
	public boolean isUnwrappableAs(Class unwrapType) {
		return ConnectionProvider.class.equals( unwrapType ) ||
				WyzAppConnectionProviderImpl.class.isAssignableFrom( unwrapType ) ||
				DataSource.class.isAssignableFrom( unwrapType );
	}

	@Override
	@SuppressWarnings({"unchecked"})
	public <T> T unwrap(Class<T> unwrapType) {

		if ( ConnectionProvider.class.equals( unwrapType ) ||
				WyzAppConnectionProviderImpl.class.isAssignableFrom( unwrapType ) ) {
			return (T) this;
		}
		else if ( DataSource.class.isAssignableFrom( unwrapType ) ) {
			return (T) dataSource;
		}
		else {
			throw new UnknownUnwrapTypeException( unwrapType );
		}
	}

	@Override
	public void closeConnection(Connection conn) throws SQLException {
		conn.close();
	}

	@Override
	@SuppressWarnings("UnnecessaryUnboxing")
	public Connection getConnection() throws SQLException {

		final Connection conn= comboPooleddataSource.getConnection();
		conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
		
		return conn;
	}

	@Override
	public boolean supportsAggressiveRelease() {
		return false;
	}
}
