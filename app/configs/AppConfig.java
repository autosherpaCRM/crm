package configs;




import java.io.FileNotFoundException;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import controllers.FirebaseInitializer;
import utils.WyzCRMCache;
import utils.WyzCRMCacheInsurance;

@Configuration
@ComponentScan({"repositories","utils" })
public class AppConfig {
	
/*	@Autowired
	private WyzAuthenticator authenticator;
	
	@Autowired
	private  Config config;
	
	@Autowired
	private CallbackController callbackController;

	@Autowired
	private LogoutController logoutController;
*/
	
	
	@Bean
	public WyzCRMCache wyzCRMCache() {
		return new WyzCRMCache();
	}
	
	@Bean
	public WyzCRMCacheInsurance wyzCRMCacheInsurance() {
		return new WyzCRMCacheInsurance();
	}

	/*@Bean
	public FirebaseInitializer firebaseInitializer() throws FileNotFoundException {

		FirebaseInitializer initializer = new FirebaseInitializer();
		initializer.initializeFirebase();
		return initializer;
	}*/
	
	
}