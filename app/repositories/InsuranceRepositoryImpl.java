package repositories;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import configs.JinqSource;
import controllers.webmodels.CallLogAjaxLoadInsurance;
import controllers.webmodels.CallLogDispositionLoadInsurance;
import controllers.webmodels.CallRecordingDataTable;
import controllers.webmodels.FollowUpNotificationModel;
import controllers.webmodels.NewAddress;
import models.Address;
import models.AppointmentBooked;
import models.AssignedCallsReport;
import models.CallDispositionData;
import models.CallInteraction;
import models.Customer;
import models.IRDA_OD_PREMIUM;
import models.Insurance;
import models.InsuranceAgent;
import models.InsuranceAssignedInteraction;
import models.InsuranceDisposition;
import models.InsuranceExcelError;
import models.ListingForm;
import models.SRDisposition;
import models.ShowRooms;
import models.TaggingUsers;
import models.UpsellLead;
import models.Vehicle;
import models.WyzUser;
import models.Campaign;
import play.Logger;

@Repository("insuranceRepository")
@Transactional
public class InsuranceRepositoryImpl implements InsuranceRepository {

	Logger.ALogger logger = play.Logger.of("application");

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private JinqSource source;

	@Override
	public void addInsurCallInter(ListingForm listData, CallInteraction call_interaction,
			CallDispositionData dispo_data, InsuranceDisposition insurDispo_data, String userLogindealerCode) {
		Vehicle veh = em.find(Vehicle.class, listData.getVehicalId());
		Customer cus = em.find(Customer.class, listData.getCustomer_Id());
		WyzUser wyz = em.find(WyzUser.class, listData.getWyzUser_Id());

		String pattern = "dd/MM/yyyy";
		String pattern1 = "HH:mm:ss";

		java.util.TimeZone tz = java.util.TimeZone.getTimeZone("IST");
		Calendar c = Calendar.getInstance(tz);

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat(pattern1);

		simpleDateFormat.setTimeZone(tz);
		simpleDateFormat1.setTimeZone(tz);

		String callDate = simpleDateFormat.format(c.getTime());
		String callTime = simpleDateFormat1.format(c.getTime());

		String dateTimeStr = callDate + " " + callTime;
		SimpleDateFormat formating = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		try {
			Date datetime = formating.parse(dateTimeStr);
			// logger.info("datetime :"+datetime);
			call_interaction.setCallMadeDateAndTime(datetime);
		} catch (ParseException e) {
			// logger.info("Could not parse date");
			e.printStackTrace();
		}

		call_interaction.setCallDate(callDate);
		call_interaction.setCallTime(callTime);
		call_interaction.setDealerCode(userLogindealerCode);

		call_interaction.setCallCount(call_interaction.getCallCount() + 1);
		call_interaction.setCustomer(cus);
		call_interaction.setVehicle(veh);
		call_interaction.setWyzUser(wyz);

		CallDispositionData callDataDispo = getCallDispositionVariable(dispo_data.getDisposition());

		insurDispo_data.setCallInteraction(call_interaction);
		insurDispo_data.setCallDispositionData(callDataDispo);

		insurDispo_data.setUpsellCount(0);
		
		
		List<String> remakListData = listData.getRemarksList();
		String remaks = "";
		for (String sa : remakListData) {

			if (sa != null) {

				remaks = remaks + sa;
			}

		}
		insurDispo_data.setComments(remaks);

		updateTheNewStatusInassignedData(listData, insurDispo_data.getTypeOfDisposition(),callDataDispo,
				call_interaction);

		em.persist(call_interaction);
		em.persist(insurDispo_data);

	}

	public CallDispositionData getCallDispositionVariable(String callDisposition) {

		CallDispositionData call_data = source.callDispositionDatas(em)
				.where(u -> u.getDisposition().equals(callDisposition)).getOnlyValue();
		return call_data;

	}

	@Override
	public void addBookAppointmentOfInsurance(ListingForm listData, CallInteraction call_interaction,
			CallDispositionData dispo_data, InsuranceDisposition insurDispo_data, AppointmentBooked bookAppointment,
			String userLogindealerCode,ShowRooms showroom,Insurance insuranceData) {

		Vehicle veh = em.find(Vehicle.class, listData.getVehicleId_SB());
		Customer cus = em.find(Customer.class, listData.getCustomer_Id());
		WyzUser wyz = em.find(WyzUser.class, listData.getWyzUser_Id());

		String pattern = "dd/MM/yyyy";
		String pattern1 = "HH:mm:ss";

		java.util.TimeZone tz = java.util.TimeZone.getTimeZone("IST");
		Calendar c = Calendar.getInstance(tz);

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat(pattern1);

		simpleDateFormat.setTimeZone(tz);
		simpleDateFormat1.setTimeZone(tz);

		String callDate = simpleDateFormat.format(c.getTime());
		String callTime = simpleDateFormat1.format(c.getTime());

		String dateTimeStr = callDate + " " + callTime;
		SimpleDateFormat formating = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		try {
			Date datetime = formating.parse(dateTimeStr);
			// logger.info("datetime :"+datetime);
			call_interaction.setCallMadeDateAndTime(datetime);
		} catch (ParseException e) {
			// logger.info("Could not parse date");
			e.printStackTrace();
		}

		call_interaction.setCallDate(callDate);
		call_interaction.setCallTime(callTime);
		call_interaction.setDealerCode(userLogindealerCode);
		call_interaction.setFirebaseKey("false");
		call_interaction.setSynchedToFirebase("false");
		call_interaction.setCallCount(call_interaction.getCallCount() + 1);
		call_interaction.setCustomer(cus);
		call_interaction.setVehicle(veh);
		call_interaction.setWyzUser(wyz);

		CallDispositionData callDataDispo = getCallDispositionVariable(dispo_data.getDisposition());

		insurDispo_data.setCallInteraction(call_interaction);
		insurDispo_data.setCallDispositionData(callDataDispo);

		insurDispo_data.setUpsellCount(0);
		
		List<String> remakListData = listData.getRemarksList();
		String remaks = "";
		for (String sa : remakListData) {

			if (sa != null) {

				remaks = remaks + sa;
			}

		}
		insurDispo_data.setComments(remaks);

		//bookAppointment.setInsuranceDisposition(insurDispo_data);

		updateTheNewStatusInassignedData(listData, insurDispo_data.getTypeOfDisposition(), callDataDispo,
				call_interaction);
		
		
		List<String> selectedPrefferedAddons = listData.getAddOnsPrefered_PopularOptions();
		if(selectedPrefferedAddons!=null){
		
		long preffAddon=selectedPrefferedAddons.size();

		String addonPref = "";

		if (preffAddon != 0) {

			for (String sa : selectedPrefferedAddons) {
				if(sa !=null){
				
				if (addonPref.equals("")) {

					addonPref = sa;

				} else {

					addonPref = addonPref + "," + sa;
				}
				
				}
			}
			
			insurDispo_data.setAddOnsPrefered_PopularOptionsData(addonPref);

		}
		}
		List<String> selectedPrefferedAddonsOthers = listData.getAddOnsPrefered_OtherOptions();
		
		if(selectedPrefferedAddonsOthers!=null){
		
		long prefCount=selectedPrefferedAddonsOthers.size();
		
		String addonPrefOthers = "";

		if ( prefCount != 0) {

			for (String sa : selectedPrefferedAddonsOthers) {
				if(sa !=null){
				
				if (addonPrefOthers.equals("")) {

					addonPrefOthers = sa;

				} else {

					addonPrefOthers = addonPrefOthers + "," + sa;
				}
				
				}
			}
			
			insurDispo_data.setAddOnsPrefered_OtherOptionsData(addonPrefOthers);

		}
		
	}
		
		logger.info("premium with tax  : "+bookAppointment.getPremiumwithTax());
		
		if(showroom.getShowRoom_id()!=0){
			long showroomid=showroom.getShowRoom_id();
			
			ShowRooms showroomData=em.find(ShowRooms.class, showroomid);
			bookAppointment.setShowRooms(showroomData);
		}
		
		bookAppointment.setCustomer(cus);
		
		if(bookAppointment.getPremiumYes().equals("Yes") ){
			logger.info("adding calculations");
			long veh_id=veh.getVehicle_id();
			
			long countOfIns=source.insurances(em).where(u -> u.getVehicle()!=null)
					.where(u -> u.getVehicle().getVehicle_id() ==veh_id ).count();
			
			if(countOfIns!=0){
				
				List<Insurance> insList=source.insurances(em).where(u -> u.getVehicle()!=null)
						.where(u -> u.getVehicle().getVehicle_id() ==veh_id ).toList();
						
				for(Insurance sa:insList){
					
					sa.setCurrent(false);
					em.merge(sa);
					
				}	
				
				if(insuranceData.getIdv()!=0){
				
					insuranceData.setLiabilityPremium(2337);
				}
				
				insuranceData.setCurrent(true);
				
			}else{
				
				insuranceData.setCurrent(true);
			}
			
			
			insuranceData.setTypeCode("Cote");			
			insuranceData.setCustomer(cus);
			insuranceData.setVehicle(veh);
			insuranceData.setInsuranceCompanyName(bookAppointment.getInsuranceCompany());
			em.persist(insuranceData);			
			
		}
		
		long insurAgentId=Long.parseLong(bookAppointment.getInsuranceAgentData());
		
		InsuranceAgent inAgent=em.find(InsuranceAgent.class,insurAgentId);
		bookAppointment.setInsuranceAgent(inAgent);		
		call_interaction.setAppointmentBooked(bookAppointment);
		bookAppointment.setInsuranceBookStatus(callDataDispo);
		
		if (listData.getCallInteractionId() != 0) {

			long callId = listData.getCallInteractionId();

			long countOfExist = source.callinteractions(em).where(u -> u.getId() == callId)
					.where(u -> u.getAppointmentBooked() != null).select(u -> u.getAppointmentBooked()).count();

			if (countOfExist != 0) {

				List<AppointmentBooked> lastServiceBookList = source.callinteractions(em).where(u -> u.getId() == callId)
						.where(u -> u.getAppointmentBooked() != null).select(u -> u.getAppointmentBooked()).toList();

				AppointmentBooked lastServiceBook = Collections.max(lastServiceBookList,
						Comparator.comparing(a -> a.appointmentId));

				CallDispositionData callDispo = source.callDispositionDatas(em).where(u -> u.getDispositionId() == 35)
						.getOnlyValue();
				lastServiceBook.setInsuranceBookStatus(callDispo);
				em.merge(lastServiceBook);

			}

		}
		
		em.persist(bookAppointment);
		em.persist(call_interaction);
		em.persist(insurDispo_data);
		

	}


	@Override
	public void addUpsellLeadOfInsurance(InsuranceDisposition insurDispo_data, ListingForm listData, long vehicalId) {
		List<UpsellLead> upselldata = listData.getUpsellLead();

		long countOfUpsell = upselldata.size();
		logger.info("count of upsell : " + countOfUpsell);
		Vehicle linkedVeh = em.find(Vehicle.class, vehicalId);
		if (countOfUpsell != 0) {
			for (UpsellLead typeData : upselldata) {

				if (typeData.getUpsellId() != 0) {

					TaggingUsers taggedUserData = getTaggedUserByType(typeData.getUpsellId());

					UpsellLead addupselllead = new UpsellLead();
					addupselllead.setTaggedTo(typeData.getTaggedTo());
					addupselllead.setUpSellType(typeData.getUpSellType());
					addupselllead.setUpsellComments(typeData.getUpsellComments());
					addupselllead.setUpsellId(typeData.getUpsellId());
					addupselllead.setInsuranceDisposition(insurDispo_data);
					addupselllead.setVehicle(linkedVeh);
					addupselllead.setTaggingUsers(taggedUserData);
					em.persist(addupselllead);

				}
			}

		}
		long call_int_id = insurDispo_data.getCallInteraction().getId();
		long ins_id = insurDispo_data.getId();
		long countOfUpsellIs = source.upsellLeads(em).where(u -> u.getInsuranceDisposition() != null)
				.where(u -> u.getInsuranceDisposition().getId() == ins_id).count();
		logger.info("countOfUpsellIs : " + countOfUpsellIs);
		InsuranceDisposition insCount = em.find(InsuranceDisposition.class, ins_id);
		insCount.setUpsellCount(countOfUpsellIs);
		em.merge(insCount);

	}

	private TaggingUsers getTaggedUserByType(int upSellId) {
		// logger.info("upSellType is : "+upSellType);

		return source.taggingUsers(em).where(u -> u.getUpsellLeadId() == upSellId).getOnlyValue();
	}

	@Override
	public CallInteraction getLatestInterOfVehicleFromInsurance(long vehicle_id) {
		CallInteraction final_call_int = new CallInteraction();

		long call_id_count = source.callinteractions(em).where(u -> u.getVehicle() != null)
				.where(u -> u.getInsuranceDisposition()!=null)
				.where(u -> u.getInsuranceAssignedInteraction() != null)
				.where(u -> u.getVehicle().getVehicle_id() == vehicle_id).count();

		// logger.info(" vehiId is : " + vehicle_id + " call_id_count : " +
		// call_id_count);

		if (call_id_count > 0) {

			long call_id = source.callinteractions(em)
					.where(u -> u.getInsuranceDisposition()!=null)
					.where(u -> u.getVehicle() != null).where(u -> u.getVehicle().getVehicle_id() == vehicle_id)
					.max(u -> u.getId());

			// logger.info(" vehiId is : " + vehicle_id + " call_id : " +
			// call_id);

			if (call_id != 0) {

				final_call_int = source.callinteractions(em).where(u -> u.getId() == call_id).getOnlyValue();				
				

				Hibernate.initialize(final_call_int.getInsuranceDisposition());
				Hibernate.initialize(final_call_int.getServiceBooked());
				Hibernate.initialize(final_call_int.getVehicle());			
				Hibernate.initialize(final_call_int.getVehicle().getCurrentInsurance());			
				if (final_call_int.getAppointmentBooked() != null) {
					//Hibernate.initialize(final_call_int.getAppointmentBooked().getWorkshop());
					Hibernate.initialize(final_call_int.getAppointmentBooked().getInsuranceAgent());
				}
				

				return final_call_int;

			} else {
				
				Hibernate.initialize(final_call_int.getVehicle());			
				Hibernate.initialize(final_call_int.getVehicle().getCurrentInsurance());			
				

				return final_call_int;
			}

		} else {

			return final_call_int;
		}

	}

	@Override
	public List<InsuranceAssignedInteraction> getAllAssignedListByUser(long userLoginId) {

		return source.insuranceAssignedInteraction(em).where(u -> u.getWyzUser() != null)
				.where(u -> u.getWyzUser().getId() == userLoginId).where(u -> u.getCallMade().equals("No")).toList();

	}

	private void updateTheNewStatusInassignedData(ListingForm listData, String typeOFDispos,CallDispositionData callDataDispo,
			CallInteraction call_interaction) {
		
		long vehId = listData.getVehicalId();
		long custId = listData.getCustomer_Id();
		WyzUser wyz = em.find(WyzUser.class, listData.getWyzUser_Id());

		logger.info(" updateTheNewStatusInassignedData vehId: " + vehId + " custId: " + custId);
		
		
		

		long countOfExistinginAssigned = source.insuranceAssignedInteraction(em)
				.where(u -> u.getVehicle() != null && u.getCustomer() != null)
				.where(u -> u.getVehicle().getVehicle_id() == vehId).where(u -> u.getCustomer().getId() == custId)
				.where(u -> u.getCallMade().equals("No")).count();

		// logger.info(" countOfExistinginAssigned :
		// "+countOfExistinginAssigned);

		if (countOfExistinginAssigned != 0) {
			long insAssignedId = source.insuranceAssignedInteraction(em)
					.where(u -> u.getVehicle() != null && u.getCustomer() != null)
					.where(u -> u.getVehicle().getVehicle_id() == vehId).where(u -> u.getCustomer().getId() == custId)

					.where(u -> u.getCallMade().equals("No")).max(u -> u.getId());
			 logger.info(" insAssignedId : "+insAssignedId);

			InsuranceAssignedInteraction updateAssi = em.find(InsuranceAssignedInteraction.class, insAssignedId);
			updateAssi.setCallMade("Yes");
			updateAssi.setFinalDisposition(callDataDispo);
			updateAssi.setLastDisposition("Not yet Called");
			em.merge(updateAssi);

			call_interaction.setInsuranceAssignedInteraction(updateAssi);
			call_interaction.setCampaign(updateAssi.getCampaign());

		} else {

			long countOfExistingCallInt = source.callinteractions(em)
					.where(u -> u.getVehicle() != null && u.getCustomer() != null)
					.where(u -> u.getVehicle().getVehicle_id() == vehId).where(u -> u.getCustomer().getId() == custId)
					.where(u -> u.getInsuranceAssignedInteraction() != null).count();

		  logger.info(" countOfExistingCallInt : "+countOfExistingCallInt);

			if (countOfExistingCallInt != 0) {

				long callIntId = source.callinteractions(em)
						.where(u -> u.getVehicle() != null && u.getCustomer() != null)
						.where(u -> u.getVehicle().getVehicle_id() == vehId)
						.where(u -> u.getCustomer().getId() == custId)
						.where(u -> u.getInsuranceAssignedInteraction() != null).max(u -> u.getId());

				CallInteraction latest_callId = source.callinteractions(em).where(u -> u.getId() == callIntId).getOnlyValue();
				call_interaction.setInsuranceAssignedInteraction(latest_callId.getInsuranceAssignedInteraction());
				call_interaction.setCampaign(latest_callId.getInsuranceAssignedInteraction().getCampaign());
				if(typeOFDispos!=null){
				
				if(typeOFDispos.equals("NonContact")){	
					
					
					call_interaction.setDroppedCount(latest_callId.getDroppedCount()+1);
					
				}
				}

				InsuranceAssignedInteraction insAssigInt = latest_callId.getInsuranceAssignedInteraction();

				insAssigInt.setLastDisposition(
						latest_callId.getInsuranceAssignedInteraction().getFinalDisposition().getDisposition());
				insAssigInt.setFinalDisposition(callDataDispo);
			}else{
				logger.info("search customer insurance");
				Customer customerData=em.find(Customer.class, custId);
				Vehicle vehicleData=em.find(Vehicle.class, vehId);
				
				Campaign smrCam = source.campaigns(em).where(u -> u.getId() == 1).getOnlyValue();
				InsuranceAssignedInteraction  insAssign=new InsuranceAssignedInteraction();	
				insAssign.setWyzUser(wyz);
				insAssign.setCampaign(smrCam);				
				insAssign.setInteractionType(smrCam.getCampaignName());				
				insAssign.setCallMade("Yes");
				insAssign.setDisplayFlag(true);
				insAssign.setCustomer(customerData);
				insAssign.setVehicle(vehicleData);
				insAssign.setLastDisposition("Not yet Called");
				insAssign.setFinalDisposition(callDataDispo);
				
				em.persist(insAssign);

				call_interaction.setInsuranceAssignedInteraction(insAssign);
				call_interaction.setCampaign(insAssign.getCampaign());
				
				
			}

		}
		
		
		if (listData.getCallInteractionId() != 0) {

			long callId = listData.getCallInteractionId();

			long countOfExist = source.callinteractions(em).where(u -> u.getId() == callId)
					.where(u -> u.getAppointmentBooked() != null).select(u -> u.getAppointmentBooked()).count();

			if (countOfExist != 0) {

				List<AppointmentBooked> lastAppointmentBookList = source.callinteractions(em).where(u -> u.getId() == callId)
						.where(u -> u.getAppointmentBooked() != null).select(u -> u.getAppointmentBooked()).toList();

				AppointmentBooked lastAppointmentBook = Collections.max(lastAppointmentBookList,
						Comparator.comparing(a -> a.appointmentId));

				call_interaction.setAppointmentBooked(lastAppointmentBook);

			}

		}

	}

	@Override
	public long getAllInsuranceAssignedInteraction(long id,String campaignType,String fromDueDate,String toDueDate,String renewalType,String searchPattern) {

		StoredProcedureQuery sQuery = em.createStoredProcedureQuery("insuranceScheduledCallsCount");

		sQuery.registerStoredProcedureParameter("wyzuserid", Integer.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("in_campaign_id", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("instartdate", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("inenddate", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("in_RenewalType", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("pattern", String.class, ParameterMode.IN);
		
		sQuery.setParameter("wyzuserid", (int) id);
		sQuery.setParameter("in_campaign_id", campaignType);
		sQuery.setParameter("instartdate", fromDueDate);
		sQuery.setParameter("inenddate", toDueDate);
		sQuery.setParameter("in_RenewalType",renewalType);
		sQuery.setParameter("pattern",searchPattern);

		BigInteger bigCount = (BigInteger) sQuery.getSingleResult();

		return bigCount.longValue();
	}
	@Override
	public List<CallLogAjaxLoadInsurance> assignedInsuranceListOfUser(long id,
			String campaignType,String fromDueDate,String toDueDate,String renewalType,String searchPattern,
			long fromIndex,long toIndex) {

		javax.persistence.Query query = em.createNativeQuery(
				"CALL insuranceScheduledCalls(:wyzuserid,:in_campaign_id,:instartdate,:inenddate,:in_RenewalType,:pattern, :start_with, :length)", "CallLogAjaxLoadInsuranceAssignedLog");

		query.setParameter("wyzuserid", id);
		query.setParameter("in_campaign_id", campaignType);
		query.setParameter("instartdate", fromDueDate);
		query.setParameter("inenddate", toDueDate);
		query.setParameter("in_RenewalType", renewalType);
		query.setParameter("pattern", searchPattern);
		query.setParameter("start_with", fromIndex);
		query.setParameter("length", toIndex);

		logger.info("pattern : " + searchPattern + " Id : " + id + " fromIndex :" + fromIndex + "toIndex :" + toIndex);

		return query.getResultList();

	}

	/*@Override
	public List<CallLogAjaxLoadInsurance> assignedInsuranceListOfUser(String searchPattern, long id, long fromIndex,
			long toIndex) {

		javax.persistence.Query query = em.createNativeQuery(
				"CALL InsuranceServiceReminder(:pattern,:wyzuserid, :start_with, :length)", "CallLogAjaxLoadInsurance");

		query.setParameter("pattern", searchPattern);
		query.setParameter("wyzuserid", id);
		query.setParameter("start_with", fromIndex);
		query.setParameter("length", toIndex);

		logger.info("pattern : " + searchPattern + " Id : " + id + " fromIndex :" + fromIndex + "toIndex :" + toIndex);

		return query.getResultList();

	}
*/
	@Override
	public long getAllInsuranceDispoInter(String fromDueDate,String toDueDate,String campaignType,String renewalType,String searchPattern,long id,long typeOfdispo) {

		StoredProcedureQuery sQuery = em.createStoredProcedureQuery("insurancefilterforDispositionContactsCount");

		sQuery.registerStoredProcedureParameter("instartdate", String.class, ParameterMode.IN);
		sQuery.setParameter("instartdate", fromDueDate);
		
		sQuery.registerStoredProcedureParameter("inenddate", String.class, ParameterMode.IN);
		sQuery.setParameter("inenddate", toDueDate);
		
		sQuery.registerStoredProcedureParameter("incampaignname", String.class, ParameterMode.IN);
		sQuery.setParameter("incampaignname", campaignType);
		
		sQuery.registerStoredProcedureParameter("induetype", String.class, ParameterMode.IN);
		sQuery.setParameter("induetype", renewalType);
		
		sQuery.registerStoredProcedureParameter("pattern", String.class, ParameterMode.IN);
		sQuery.setParameter("pattern", searchPattern);

		sQuery.registerStoredProcedureParameter("wyzuser_id", Integer.class, ParameterMode.IN);
		sQuery.setParameter("wyzuser_id", (int) id);

		sQuery.registerStoredProcedureParameter("dispositiontype", Integer.class, ParameterMode.IN);
		sQuery.setParameter("dispositiontype", (int) typeOfdispo);

		BigInteger bigCount = (BigInteger) sQuery.getSingleResult();

		return bigCount.longValue();

	}
	@Override
	public List<CallLogDispositionLoadInsurance> dispoInsuranceListOfUser(String fromDueDate , String toDueDate, String campaignType , String renewalType, String searchPattern, long id, long typeOfdispo,
			long fromIndex, long toIndex) {

		
			javax.persistence.Query query = em.createNativeQuery(
					"CALL insurancefilterforDispositionContacts(:instartdate,:inenddate, :incampaignname,:induetype,:pattern,:wyzuserid, :dispositiontype,:start_with, :length)",
					"CallLogDispositionLoadInsuranceContacts");
			query.setParameter("instartdate", fromDueDate);
			query.setParameter("inenddate", toDueDate);
			query.setParameter("incampaignname", campaignType);
			query.setParameter("induetype", renewalType);

			query.setParameter("pattern", searchPattern);
			query.setParameter("wyzuserid", id);
			query.setParameter("dispositiontype", typeOfdispo);
			query.setParameter("start_with", fromIndex);
			query.setParameter("length", toIndex);

			logger.info("pattern : " + searchPattern + " Id : " + id + " fromIndex :" + fromIndex + "toIndex :" + toIndex
					+ " typeOfdispo : " + typeOfdispo);

			return query.getResultList();
	
		
	}

	@Override
	public long getAllInsuranceNonContactInter(String fromDueDate,String toDueDate,String campaignType,String renewalType,String searchPattern,long id,long typeOfdispo,String droppedcount,String lastdispo) {

		StoredProcedureQuery sQuery = em.createStoredProcedureQuery("insurancefilterforNonContactsCount");

		sQuery.registerStoredProcedureParameter("instartdate", String.class, ParameterMode.IN);
		sQuery.setParameter("instartdate",fromDueDate);
		
		sQuery.registerStoredProcedureParameter("inenddate", String.class, ParameterMode.IN);
		sQuery.setParameter("inenddate",toDueDate);
		
		sQuery.registerStoredProcedureParameter("incampaignname", String.class, ParameterMode.IN);
		sQuery.setParameter("incampaignname",campaignType);
		
		sQuery.registerStoredProcedureParameter("induetype", String.class, ParameterMode.IN);
		sQuery.setParameter("induetype",renewalType);
		
		sQuery.registerStoredProcedureParameter("pattern", String.class, ParameterMode.IN);
		sQuery.setParameter("pattern",searchPattern);
		
		sQuery.registerStoredProcedureParameter("wyzuser_id", Integer.class, ParameterMode.IN);
		sQuery.setParameter("wyzuser_id", (int) id);
		
		sQuery.registerStoredProcedureParameter("dispositiontype", Integer.class, ParameterMode.IN);
		sQuery.setParameter("dispositiontype", (int) typeOfdispo);

		sQuery.registerStoredProcedureParameter("indroppedcount", String.class, ParameterMode.IN);
		sQuery.setParameter("indroppedcount", droppedcount);

		sQuery.registerStoredProcedureParameter("inLastDisposition", String.class, ParameterMode.IN);
		sQuery.setParameter("inLastDisposition",lastdispo);



		BigInteger bigCount = (BigInteger) sQuery.getSingleResult();

		return bigCount.longValue();
	}
	
	@Override
	public List<CallLogDispositionLoadInsurance> nonContactInsuranceListOfUser(String fromDueDate,String toDueDate,String campaignType,String renewalType,String searchPattern,long id,long typeOfdispo,String droppedcount,String lastdispo,long fromIndex,long toIndex){
		javax.persistence.Query query = em.createNativeQuery(
				"CALL insurancefilterforNonContacts(:instartdate,:inenddate,:incampaignname,:induetype,:pattern,:wyzuserid,:disposition,:indroppedcount,:inLastDisposition,:start_with, :length)",
				"CallLogDispositionLoadInsuranceNonContacts");

		query.setParameter("instartdate", fromDueDate);
		query.setParameter("inenddate", toDueDate);
		query.setParameter("incampaignname", campaignType);
		query.setParameter("induetype", renewalType);
		query.setParameter("wyzuserid", id);
		query.setParameter("disposition",typeOfdispo);
		query.setParameter("indroppedcount", droppedcount);
		query.setParameter("inLastDisposition", lastdispo);
		query.setParameter("pattern", searchPattern);
		query.setParameter("start_with", fromIndex);
		query.setParameter("length", toIndex);

		logger.info("pattern : " + searchPattern + " Id : " + id + " fromIndex :" + fromIndex + "toIndex :" + toIndex
				+ " typeOfdispo : " + typeOfdispo);

		return query.getResultList();
		
	}
	
	@Override
	public List<Campaign> getCampaignNamesOfinsurance() {

		return source.campaigns(em).where(u -> u.getCampaignType().equals("Insurance")).toList();
	}
	
	@Override
	public List<Campaign> getCampainListOfInsurance(){
		
		List<Campaign> otherList = source.campaigns(em).where(u -> u.getCampaignType().equals("Insurance")).toList();

		List<Campaign> listnew = new ArrayList<Campaign>();
		
		if(otherList.size()>0){
		listnew.add(otherList.get(0));
		}
		
		
		return listnew;
	}
	
	@Override
	public List<Double> getODPercentageByRestParams(String cubicCap, String vehAge, String zoneid){
		
		//logger.info("cubicCap : "+cubicCap+" vehAge : "+vehAge+" zoneid : "+zoneid);
		
		IRDA_OD_PREMIUM irda_od=source.iRDA_OD_PREMIUMS(em).where(u -> u.getCubicCapacity().equals(cubicCap) &&
				u.getVehicleAge().equals(vehAge) && u.getZone().equals(zoneid)).getOnlyValue();
		
	
		
		List<Double> odandthirdparty=new ArrayList<Double>();
		
		odandthirdparty.add(irda_od.getOdPercentage());
		odandthirdparty.add(irda_od.getThirdPartyPremium());
		
		return odandthirdparty;
	}
	
	@Override
	public List<ShowRooms> getShowRoomListData(){
	
		return source.showRooms(em).toList();
		
	}
	
	@Override
	public List<InsuranceExcelError> getErrorOccuredDataInsurance(String uploadId){
		
		return source.insuranceExcelErrors(em).where(u -> u.getUpload_id()!=null)
				.where(u -> u.getUpload_id().equals(uploadId)).toList();
	}
	
	@Override
	public List<CallLogAjaxLoadInsurance> getAssignedListOfInsurance(Date fromDateNew, Date toDateNew, long campaignTypeCat, long campaignNameCat,
			String cityName, long workshopName){
		
		logger.info("campaignNameCat : " + campaignNameCat + " fromDateNew : " + fromDateNew + " toDateNew :" + toDateNew + "cityName :" + cityName
				+ " workshopName : " + workshopName);

		
		javax.persistence.Query query = em.createNativeQuery(
				"CALL insurance_assign_list(:in_campaign_id,:start_date, :end_date,:in_location, :in_workshop_id)",	"CallLogAjaxLoadInsuranceAssignedLog");

		query.setParameter("in_campaign_id", (int) campaignNameCat);
		query.setParameter("start_date", fromDateNew);
		query.setParameter("end_date", toDateNew);
		query.setParameter("in_location", cityName);
		query.setParameter("in_workshop_id", (int)workshopName);

		List<CallLogAjaxLoadInsurance> assignlist=query.getResultList();

		return assignlist;
	}
	
	@Override
	public void assignCallByManagerForInsurance(String userdata, long insAssigId, String userLogindealerCode){
		
		WyzUser user = source.wyzUsers(em).where(u -> u.getUserName().equals(userdata)).getOnlyValue();
		
		InsuranceAssignedInteraction insAssigned=source.insuranceAssignedInteraction(em).where(u -> u.getId() == insAssigId).getOnlyValue();
		insAssigned.setWyzUser(user);
		insAssigned.setDisplayFlag(true);
		insAssigned.setLastDisposition("Not yet Called");
		em.merge(insAssigned);
		
		
		AssignedCallsReport addRecordAssigned = new AssignedCallsReport();

		addRecordAssigned.setWyzuserId(user.getId());
		addRecordAssigned.setUploadId(insAssigned.getVehicle().getUpload_id());
		addRecordAssigned.setAssignedDate(new Date());
		addRecordAssigned.setAssignInteractionID(insAssigId);
		addRecordAssigned.setAssignmentType("Insurance");
		addRecordAssigned.setVehicleId(insAssigned.getVehicle().getVehicle_id());
		em.persist(addRecordAssigned);
		
		
	}
	
	@Override
	public long getLatestInsuranceOfVehicle(long vehicle_id){
		
		return source.insurances(em).where(u -> u.getVehicle()!=null)
				.where(u -> u.getVehicle().getVehicle_id() == vehicle_id).count();
	}
	
	@Override
	public Insurance getLatestInsuranceData(long vehicle_id){
		
		long maxCallId=source.insurances(em).where(u -> u.getVehicle()!=null)
				.where(u -> u.getVehicle().getVehicle_id() == vehicle_id)
				.max(u -> u.getId());
		
		Insurance insuranceData=em.find(Insurance.class, maxCallId);
		
		return insuranceData;
		
	}
	
	@Override
	public List<AppointmentBooked> getAppointMentsByCustomerId(long customerId){
		
		List<AppointmentBooked> appointList=new ArrayList<AppointmentBooked>();
		
		long countExis=source.appointmentsBooked(em).where(u -> u.getCustomer()!=null)
				.where(u -> u.getCustomer().getId() == customerId).count();
		logger.info(" countExis "+countExis);
		
		if(countExis!=0){
		
		appointList=source.appointmentsBooked(em).where(u -> u.getCustomer()!=null)
				.where(u -> u.getCustomer().getId() == customerId).toList();
		
				
		return appointList;
		}else{
			
			return appointList;
		}
	}

	@Override
	public List<InsuranceAgent> getAllInsuranceAgents() {
		// TODO Auto-generated method stub
		return source.insuranceAgents(em).toList();
	}

	@Override
	public void addNewAddressToCustomer(NewAddress newAdressData, long customer_Id) {
		Customer cust=em.find(Customer.class, customer_Id);
		
		if(newAdressData.getAddress1New()!=null && newAdressData.getAddress1New()!="" ){
			
			logger.info("adding address");;
			
			List<Address> adresslist=source.address(em).where(u -> u.getCustomer()!=null)
					.where(u -> u.getCustomer().getId() == customer_Id).toList();
			
			if(adresslist.size()!=0){
				
				for(Address sa:adresslist){
					
					sa.setIsPreferred(false);
					em.merge(sa);
					
					
				}
				
				Address newAddress=new Address();
				newAddress.setAddressLine1(newAdressData.getAddress1New());
				newAddress.setAddressLine2(newAdressData.getAddress2New());
				newAddress.setCity(newAdressData.getCityNew());
				newAddress.setState(newAdressData.getStateNew());
				newAddress.setPincode(newAdressData.getPincodeNew());
				newAddress.setIsPreferred(true);
				newAddress.setAddressType(Address.ADDRESS_TYPE_OTHERS);
				newAddress.setCustomer(cust);
				em.persist(newAddress);
				
				
			}else{
				
				Address newAddress=new Address();
				newAddress.setAddressLine1(newAdressData.getAddress1New());
				newAddress.setAddressLine2(newAdressData.getAddress2New());
				newAddress.setCity(newAdressData.getCityNew());
				newAddress.setState(newAdressData.getStateNew());
				newAddress.setPincode(newAdressData.getPincodeNew());
				newAddress.setAddressType(Address.ADDRESS_TYPE_OFFICE);
				newAddress.setIsPreferred(true);
				newAddress.setCustomer(cust);
				em.persist(newAddress);
			}
			
			
		}
		
	}

	@Override
	public List<String> getAllDistinctStates() {
		
		
		List<String> states_list = source.citystates(em).select(u -> u.getState()).distinct().toList();
		
		// TODO Auto-generated method stub
		return states_list;
	}

	@Override
	public List<FollowUpNotificationModel> getFollowupNotificationOfInsu(long id, String dealercode) {
		javax.persistence.Query query = em.createNativeQuery("CALL insurance_followUp_notifications(:id,:dealercode)","FollowUpNotificationModel");        
        query.setParameter("id", id);
        query.setParameter("dealercode", dealercode);            
		return query.getResultList();
	}

	@Override
	public long getAllRecordingHistoryCount(String userLoginName, String startDate, String endDate, String calltype,
			String campainName, String calledStatus, String dispoType, String users, String pattern, int modelType) {
		// TODO Auto-generated method stub
		StoredProcedureQuery sQuery = em.createStoredProcedureQuery("callRecordingHistorycount");

		sQuery.registerStoredProcedureParameter("managerUserId", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("instartDate", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("inEndDate", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("incallType", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("inCampaign", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("calledStatus", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("dipsoType", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("users", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("pattern", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("modeltype", String.class, ParameterMode.IN);
		

		sQuery.setParameter("managerUserId", userLoginName);
		sQuery.setParameter("instartDate", startDate);
		sQuery.setParameter("inEndDate", endDate);
		sQuery.setParameter("incallType", calltype);
		sQuery.setParameter("inCampaign", campainName);
		sQuery.setParameter("calledStatus", calledStatus);
		sQuery.setParameter("dipsoType", dispoType);
		sQuery.setParameter("users", users);
		sQuery.setParameter("pattern", pattern);
		sQuery.setParameter("modeltype", String.valueOf(modelType));

		BigInteger bigCount = (BigInteger) sQuery.getSingleResult();

		return bigCount.longValue();
	}

	@Override
	public List<CallRecordingDataTable> getAllRecordingHistoryData(String userLoginName, String fromDateNew,
			String toDateNew, String callType, String campaignName, String creInitiated, String disposition,
			String searchPattern, String usernameslist, int modelType, long fromIndex, long toIndex) {
		// TODO Auto-generated method stub
		logger.info("creInitiated"+ creInitiated+ "disposition" +disposition+
				"searchPattern"+ searchPattern+ ""+ usernameslist+ "modelType"+ modelType+ "fromIndex "+ fromIndex +"toIndex"+ toIndex);

		
		javax.persistence.Query query = em.createNativeQuery(
				"CALL callRecordingHistory(:managerUserId,:instartDate, :inEndDate,:incallType, :inCampaign,:calledStatus,:dipsoType,:users,:pattern,:modeltype,:startfrom,:length)","CallRecordingDataTable");

		query.setParameter("managerUserId", userLoginName);
		query.setParameter("instartDate", fromDateNew);
		query.setParameter("inEndDate", toDateNew);
		query.setParameter("incallType", callType);
		query.setParameter("inCampaign", campaignName);
		query.setParameter("calledStatus", creInitiated);
		query.setParameter("dipsoType", disposition);
		query.setParameter("users", usernameslist);
		query.setParameter("pattern", searchPattern);
		query.setParameter("modeltype", String.valueOf(modelType));
		query.setParameter("startfrom", fromIndex);
		query.setParameter("length", toIndex);

		

		return query.getResultList();
	}

	@Override
	public long getOtherAllRecordingHistoryCount(String userLoginName, String startDate, String endDate,
			String users, String callType, String pattern) {
		// TODO Auto-generated method stub
		
		logger.info("Repo counht  getUserLoginName()" +userLoginName+ " fromDateNew : "+startDate+" toDateNew : "+endDate+
				" usernameslist : "+users+" callType : "+callType+" searchPattern : "+pattern );
		
		
		
		StoredProcedureQuery sQuery = em.createStoredProcedureQuery("otherCallRecordingHistoryCount");

		sQuery.registerStoredProcedureParameter("managerUserId", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("instartDate", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("inEndDate", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("users", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("incallType", String.class, ParameterMode.IN);			
		sQuery.registerStoredProcedureParameter("pattern", String.class, ParameterMode.IN);
		
		

		sQuery.setParameter("managerUserId", userLoginName);
		sQuery.setParameter("instartDate", startDate);
		sQuery.setParameter("inEndDate", endDate);
		sQuery.setParameter("users", users);
		sQuery.setParameter("incallType", callType);		
		sQuery.setParameter("pattern", pattern);
		

		BigInteger bigCount = (BigInteger) sQuery.getSingleResult();

		return bigCount.longValue();
	}

	@Override
	public List<CallRecordingDataTable> getOtherAllRecordingHistoryData(String userLoginName, String fromDateNew,
			String toDateNew, String usernameslist, String callType, String searchPattern, long fromIndex,
			long toIndex) {
		
		logger.info("Repo getUserLoginName()" +userLoginName+ " fromDateNew : "+fromDateNew+" toDateNew : "+toDateNew+
				" usernameslist : "+usernameslist+" callType : "+callType+" searchPattern : "+searchPattern );
		
		
		// TODO Auto-generated method stub
		javax.persistence.Query query = em.createNativeQuery(
				"CALL otherCallRecordingHistory(:managerUserId,:instartDate, :inEndDate, :users,:in_calltype,:pattern,:startfrom,:length)","CallRecordingDataTable");

		query.setParameter("managerUserId", userLoginName);
		query.setParameter("instartDate", fromDateNew);
		query.setParameter("inEndDate", toDateNew);
		query.setParameter("in_calltype", callType);		
		query.setParameter("users", usernameslist);
		query.setParameter("pattern", searchPattern);		
		query.setParameter("startfrom", fromIndex);
		query.setParameter("length", toIndex);

		

		return query.getResultList();
	}
	
	@Override
	public String getLocationIdByName(String cityName) {
		// TODO Auto-generated method stub
		return source.locations(em).where(u -> u.getName().equals(cityName)).select(u -> u.getCityId()).getOnlyValue().toString();
	}


}
