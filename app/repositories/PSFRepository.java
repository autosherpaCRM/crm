/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import controllers.webmodels.CallLogAjaxLoad;
import controllers.webmodels.CallLogDispositionLoad;
import controllers.webmodels.PSFFollowupNotificationModel;

import java.util.List;
import models.CallDispositionData;
import models.Complaint;
import models.ListingForm;
import models.PSFAssignedInteraction;
import models.PSFInteraction;
import models.PickupDrop;
import models.AssignedInteraction;
import models.Campaign;

/**
 *
 * @author W-885
 */
public interface PSFRepository {


//    public List<AssignedInteraction> getPSFListAllForWyzUser();

    public List<AssignedInteraction> getPSFListAllForWyzUserAssigned(String userLoginName);

    // modified on  jan 4th 
    public void addPSFFeedBackFormData(PSFInteraction psf_interaction, CallDispositionData call_dispo, ListingForm list_dispo, String userLogindealerCode,PickupDrop pick_up);

   // public long getAllAssignedInteractionPsf(long id);

    //public List<CallLogAjaxLoad> assignedListOfUserPsf(String searchPattern, long id, long fromIndex, long toIndex);

//    public long PSFServiceReminderwithSearchCount(long id);
//
//    public List<CallLogAjaxLoad> PSFServiceReminderwithSearch(String searchPattern, long id, long fromIndex, long toIndex);
    
    //public long PSFServiceReminderwithSearchCount(long id);

    //public List<CallLogAjaxLoad> PSFServiceReminderwithSearch(String searchPattern, long id, long fromIndex, long toIndex);

    public List<CallLogDispositionLoad> PSFFollowUPRequiredwithSearch(String searchPattern, long id, long fromIndex, long toIndex,long typeOfPSF);

    public long PSFFollowUPRequiredwithSearchCount(long id,long typeOfPSF);
    
    public List<CallLogDispositionLoad> PSF_Non_contacts(String searchPattern, long id, long fromIndex, long toIndex,long typeOfPSF);

    public long PSF_Non_contactsCount(long id,long typeOfPSF);

   // public long PSF_Servey_completeCount(long id,long typeOfPSF);

  //  public List<CallLogDispositionLoad> PSF_Servey_complete(String searchPattern, long id, long fromIndex, long toIndex,long typeOfPSF);

   // public long PSF_droppedCount(long id,long typeOfPSF);

  //  public List<CallLogDispositionLoad> PSF_dropped(String searchPattern, long id, long fromIndex, long toIndex,long typeOfPSF);

    public List<Campaign> getPsflistCampaign();

    public long PSFServiceReminderwithSearchCount(long id, long typeOfPSF);

    public List<CallLogAjaxLoad> PSFServiceReminderwithSearch(String searchPattern, long id, long fromIndex, long toIndex, long typeOfPSF);
	
	
	 public long PSF15ServiceReminderwithSearchCount(long id, String name);

    public List<CallLogAjaxLoad> PSF15ServiceReminderwithSearch(String searchPattern, long id, long fromIndex, long toIndex, String name);

    public long PSF15FollowUPRequiredwithSearchCount(long id);

    public List<CallLogDispositionLoad> PSF15FollowUPRequiredwithSearch(String searchPattern, long id, long fromIndex, long toIndex);

    public long PSF15_Servey_completeCount(long id);

    public List<CallLogDispositionLoad> PSF15_Servey_complete(String searchPattern, long id, long fromIndex, long toIndex);

    public long PSF15_Non_contactsCount(long id);

    public List<CallLogDispositionLoad> PSF15_Non_contacts(String searchPattern, long id, long fromIndex, long toIndex);

    public long PSF15_droppedCount(long id);

    public List<CallLogDispositionLoad> PSF15_dropped(String searchPattern, long id, long fromIndex, long toIndex);

    public long PSF30ServiceReminderwithSearchCount(long id, String name);

    public List<CallLogAjaxLoad> PSF30ServiceReminderwithSearch(String searchPattern, long id, long fromIndex, long toIndex, String name);

    public long PSF30FollowUPRequiredwithSearchCount(long id);

    public List<CallLogDispositionLoad> PSF30FollowUPRequiredwithSearch(String searchPattern, long id, long fromIndex, long toIndex);

    public long PSF30_Servey_completeCount(long id);

    public List<CallLogDispositionLoad> PSF30_Servey_complete(String searchPattern, long id, long fromIndex, long toIndex);

    public long PSF30_Non_contactsCount(long id);

    public List<CallLogDispositionLoad> PSF30_Non_contacts(String searchPattern, long id, long fromIndex, long toIndex);

    public long PSF30_droppedCount(long id);

    public List<CallLogDispositionLoad> PSF30_dropped(String searchPattern, long id, long fromIndex, long toIndex);
    
    public void addUpsellLeadOfPSF(PSFInteraction psf_interaction, ListingForm list_dispo);

	public List<PSFFollowupNotificationModel> getFollowUpNotification(long id);
	
	// scheduled
		public long PSFScheduledCallsCount(String fromBillDate,String toBillDate,String typeOfPSF,String searchPattern,long id,long fromIndex,long toIndex);
		
		public List<CallLogAjaxLoad> PSFScheduledCalls(String fromBillDate,String toBillDate,String typeOfPSF,String searchPattern,long id,long fromIndex,long toIndex);
		
	// followup , complete survey , appointments
	public long PSFCountForFollowUpCompleteSurveyAppointments(String fromBillDate,String toBillDate,String typeOfPSF,String searchPattern ,long id,long dispoType);
	
	public List<CallLogDispositionLoad> PSFCallsFollowUpCompleteSurveyAppointments (String fromBillDate,String toBillDate,String typeOfPSF,String searchPattern ,long id,long dispoType,long fromIndex,long toIndex);
	
	// non contacts and dropped
	public long PSFNoncontactsCount(String fromBillDate,String toBillDate,String typeOfPSF,String searchPattern,long id,long dispoType);
	
	public List<CallLogDispositionLoad> PSFNoncontacts(String fromBillDate,String toBillDate,String typeOfPSF,String searchPattern ,long id,long dispoType,long fromIndex,long toIndex);

	public PSFAssignedInteraction getLastPSFAssignStatusOfVehicle(long vehicle_id);
	
}

