package repositories;

import java.util.List;
import play.data.Form;

import java.util.ArrayList;
import java.util.Date;
import play.Logger.ALogger;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import configs.JinqSource;
import models.Campaign;
import models.Customer;
import models.Location;
import models.Workshop;

@Repository("campaignRepository")
@Transactional
public class CampaignRepositoryImpl implements CampaignRepository {

	ALogger logger = play.Logger.of("application");
	@PersistenceContext
	private EntityManager em;

	@Autowired
	private JinqSource source;

	// @Override
	// public void addNewCampaign(Campaign Campaign, String userLoginName) {
	//
	//
	// List<Location> locId =
	// source.locations(em).where(u->u.getName().equals())
	//
	// }

	@Override
	public void addNewCampaign(Campaign Campaign, String userLoginName, String locationdata, Workshop uploadData_work,
			long workid) {

		String campaignName = Campaign.getCampaignName();
		// long workshopinfo = uploadData_work.getId();
		logger.info("campaignName" + campaignName);
		logger.info("location name:" + locationdata);
		logger.info("workshop name:" + workid);
		String campaignType = Campaign.getCampaignType();

		Date startDate = Campaign.getCreatedDate();
		Date expiryDate = Campaign.getExpiryDate();

		Campaign newCampaign = new Campaign();
		// long locCount = source.locations(em).where(v ->
		// v.getName().equals(locationdata)).count();

		long locCount = source.locations(em).where(v -> v.getName().equals(locationdata)).count();
		if (locCount > 0) {
			// Location locData = source.locations(em).where(v ->
			// v.getName().equals(locationdata)).getOnlyValue();
			// long locId = locData.getCityId();

			Location loc = source.locations(em).where(u -> u.getName().equals(locationdata)).getOnlyValue();
			Workshop workShopID = em.find(Workshop.class, workid);

			newCampaign.setCampaignName(campaignName);
			newCampaign.setCampaignType(campaignType);
			newCampaign.setCreatedDate(startDate);
			newCampaign.setExpiryDate(expiryDate);
			newCampaign.setIsValid(true);
			newCampaign.setLocation(loc);
			newCampaign.setWorkshop(workShopID);
			em.persist(newCampaign);
		}
	}

	@Override
	public List<Campaign> getCampaignListUploadType(String uploadType) {
		// TODO Auto-generated method stub

		List<Campaign> camplist = new ArrayList<Campaign>();
		long CampaTypecount = source.uploadTypes(em)
				.where(u -> u.getUploadTypeName() != null && u.getUploadTypeName().equals(uploadType))
				.where(u -> u.getUploadDisplayName() != null).count();

		if (CampaTypecount != 0) {

			String CampaType = source.uploadTypes(em)
					.where(u -> u.getUploadTypeName() != null && u.getUploadTypeName().equals(uploadType))
					.where(u -> u.getUploadDisplayName() != null).select(u -> u.getUploadDisplayName()).getOnlyValue();

			camplist = source.campaigns(em).where(u -> u.getCampaignType().equals(CampaType)).toList();

		}

		return camplist;
	}

}
