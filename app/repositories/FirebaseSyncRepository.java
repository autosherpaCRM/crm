/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import java.util.List;
import java.util.*;

import javax.inject.Named;

import com.google.firebase.database.DataSnapshot;

import actors.model.DriverPickupInfo;
import actors.model.ServiceAdvisorHistoryInfo;
import models.CallInteraction;
import models.Dealer;
import models.Driver;
import models.Insurance;
import models.InsuranceAgent;
import models.Segment;
import models.Service;
import models.ServiceBooked;
import models.ServiceAdvisor;
import models.WyzUser;
import models.AppointmentBooked;
import models.AssignedInteraction;
import models.PSFAssignedInteraction;
/**
 *
 * @author W-885
 */
@Named
public interface FirebaseSyncRepository {

	public List<Dealer> getListOfDealers();

	public List<CallInteraction> getListOfServicesNotSynced(String dealer);

	public Service getServiceDataByVehicleId(long vehicle_id);

	public String getDispositionValueById(int disposition_id);

	public ServiceBooked getSBbyCallInterId(long id);

	public void updateSynchedToFirebase(long id, String referencekey);

	public void updateStatusOfServiceBookedOfSA(String fkeyString,
			String vehicle_Received);

	public List<WyzUser> getListOfServiceAdvisorData();

	public List<ServiceBooked> getListOfServicebookedData(String user);

	public List<ServiceBooked> getDriverPickupDropList(String user);

	public List<Driver> getAllDrivers();

	public void addTestServiceBooked();

	public Segment getSegmentByCustomerId(long id);
	
	public void addDriverInteraction(String firebaseKey, DriverPickupInfo info);

	public List<ServiceAdvisor> getAllActiveServiceAdvisors(String dealer);

	public void addServiceAdvisorHistory(String firebaseKey, ServiceAdvisorHistoryInfo serviceAdvi_History, DataSnapshot dataSnapshot);

	public String getCurrentDateInStringFormate();

	public long getVehicleRecivedToday(String fkeyString);

	public Map<String, Long> getCountOfServicesBookedtoday(long advisorId);

	public List<PSFAssignedInteraction> getAllUnsynchedPSFMobileCalls(String dealer);

	public void updateSynchedToFirebasePSFCall(long id, String referencekey);

	public void addServiceAdvisorPSFHistorySync(String firebaseKey, ServiceAdvisorHistoryInfo serviceAdvi_History);

	public List<CallInteraction> getAllNotSyncedAppointment(String dealer);

	public AppointmentBooked getAppointmentById(long appointmentId);

	public Insurance getLatestInsuranceOfVehicle(long vehicle_id);

	public List<InsuranceAgent> getAllActiveInsuranceAgent(String dealer);

	public void addInsuranceAgentCallHistory(String firebaseKey, ServiceAdvisorHistoryInfo insuranceHistory,
			DataSnapshot dataSnapshot);

	public void startEventsOfCRM(String dealer);

	

}
