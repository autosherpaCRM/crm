package repositories;

import java.util.List;
import play.data.Form;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import configs.JinqSource;
import models.Customer;

@Repository("customerScheduledRepository")
@Transactional
public class CustomerScheduledRepositoryImpl implements CustomerScheduledRepository {

	
	@PersistenceContext
    private EntityManager em;

    @Autowired
    private JinqSource source;
    
    
	
	
	@Override
	public Customer getUserByCustomerMobileNo(String customerMobileNo) {

		Customer custScheduled = (Customer) source.customers(em);
		
		return custScheduled;
	}

	@Override
	public List<Customer> getAllCustScheduleds() {

		return source.customers(em).toList();

	}

	@Override
	public Customer addCustomerData(Customer custScheduled) {
		em.persist(custScheduled);
		return custScheduled;
	}
    @Override
    public Customer editCustomer(Long cid){
        Customer edi=em.find(Customer.class,cid);
        
        return edi;
    }
    @Override
    public  Customer getUserById(Long cid){
        Customer gettingdataById=em.find(Customer.class,cid);
        return gettingdataById;
    }
    @Override
    public Customer posteditedData(Long cid,Customer customer){
        Customer editedDataValue=em.find(Customer.class,cid);
        
        Form<Customer> form=Form.form(Customer.class).bindFromRequest();
	    Customer cust=form.get();
	   
	   // editedDataValue.setCustomerAddress(cust.getCustomerAddress());
	    //editedDataValue.setCustomerEmail(cust.getCustomerEmail());
	    editedDataValue.setCustomerName(cust.getCustomerName());
	   // editedDataValue.setCustomerMobileNo(cust.getCustomerMobileNo());
	  
	    editedDataValue.setDob(cust.getDob());
	    
       
	    return customer;
        
    }
    @Override
    public Customer deleteCustomer(Long cid){
       Customer deletingData=em.find(Customer.class,cid); 
       em.remove(deletingData);
       return deletingData;
    }
}
