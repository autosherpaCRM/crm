/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import configs.JinqSource;
import controllers.webmodels.CallLogAjaxLoad;
import controllers.webmodels.CallLogDispositionLoad;
import controllers.webmodels.CallLogDispositionLoadMR;
import controllers.webmodels.PSFFollowupNotificationModel;

import java.math.BigInteger;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.ParameterMode;

import models.CallDispositionData;
import models.Complaint;
import models.ListingForm;
import models.PSFInteraction;
import models.PickupDrop;
import models.TaggingUsers;
import models.UpsellLead;
import models.AssignedInteraction;
import models.BookingDateTime;
import models.CallInteraction;
import models.Campaign;
import models.Customer;
import models.Driver;
import models.Vehicle;
import models.WyzUser;
import models.PSFAssignedInteraction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.hibernate.Hibernate;
import play.Logger;
import play.data.Form;

/**
 *
 * @author W-885
 */
@Repository("pSFRepository")
@Transactional
public class PSFRepositoryImpl implements PSFRepository {

    Logger.ALogger logger = play.Logger.of("application");

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private JinqSource source;

    // modified on the jan 4th
  
 
    @Override
	public void addPSFFeedBackFormData(PSFInteraction psf_interaction, CallDispositionData call_dispo,
			ListingForm list_dispo, String userLogindealerCode, PickupDrop pick_up) {
		Vehicle veh = em.find(Vehicle.class, list_dispo.getVehicalId());
		Customer cus = em.find(Customer.class, list_dispo.getCustomer_Id());
		WyzUser wyz = em.find(WyzUser.class, list_dispo.getWyzUser_Id());

		CallInteraction callInteraction = new CallInteraction();

		String pattern = "dd/MM/yyyy";
		String pattern1 = "HH:mm:ss";

		java.util.TimeZone tz = java.util.TimeZone.getTimeZone("IST");
		Calendar c = Calendar.getInstance(tz);

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat(pattern1);

		simpleDateFormat.setTimeZone(tz);
		simpleDateFormat1.setTimeZone(tz);

		String callDate = simpleDateFormat.format(c.getTime());
		String callTime = simpleDateFormat1.format(c.getTime());
		String dateTimeStr = callDate + " " + callTime;
		SimpleDateFormat formating = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		try {
			Date datetime = formating.parse(dateTimeStr);
			// logger.info("datetime :"+datetime);
			callInteraction.setCallMadeDateAndTime(datetime);
		} catch (ParseException e) {
			// logger.info("Could not parse date");
			e.printStackTrace();
		}
		callInteraction.setUniqueidForCallSync(list_dispo.getUniqueidForCallSync());
		callInteraction.setCallDate(callDate);
		callInteraction.setCallTime(callTime);
		callInteraction.setDealerCode(userLogindealerCode);

		// callInteraction.setInteractionType("PSF");
		callInteraction.setCustomer(cus);
		callInteraction.setVehicle(veh);
		callInteraction.setWyzUser(wyz);
		
		List<String> remakListData = list_dispo.getRemarksList();
		String remaks = "";
		for (String sa : remakListData) {

			if (sa != null) {

				remaks = remaks + sa;
			}

		}
		psf_interaction.setRemarks(remaks);

		callInteraction.setIsCallinitaited(list_dispo.getIsCallinitaited());
		if (list_dispo.getIsCallinitaited().equals("Initiated")) {

			callInteraction.setDailedStatus(true);

		}

		long interaction_id = list_dispo.getInteractionid();

		if (list_dispo.getDispositionHistory() == 0) {

			PSFAssignedInteraction assignedLink = em.find(PSFAssignedInteraction.class, interaction_id);
			assignedLink.setCallMade("Yes");
			
			if(assignedLink.getWyzUser()!=null){
				
			}else{
				assignedLink.setWyzUser(wyz);
				
			}
			

			if (call_dispo.getDisposition() != null) {
				logger.info("first");
				CallDispositionData callDataDispo = getCallDispositionVariable(call_dispo.getDisposition());
				psf_interaction.setCallDispositionData(callDataDispo);

				if (call_dispo.getDispositionId() == call_dispo.STATUS_CALL_ME_LATER) {

					psf_interaction.setIsFollowUpDone("No");
					psf_interaction.setIsFollowupRequired("Yes");

				}

				assignedLink.setFinalDisposition(callDataDispo);
				assignedLink.setLastDisposition("Not yet Called");
			} else if (psf_interaction.getPSFDispositon() != null) {
				logger.info("second : " + psf_interaction.getPSFDispositon());
				CallDispositionData callDataDispo = getCallDispositionVariable(psf_interaction.getPSFDispositon());
				psf_interaction.setCallDispositionData(callDataDispo);
				assignedLink.setFinalDisposition(callDataDispo);
				assignedLink.setLastDisposition("Not yet Called");

			}

			em.merge(assignedLink);
			callInteraction.setCampaign(assignedLink.getCampaign());
			callInteraction.setPsfAssignedInteraction(assignedLink);
			callInteraction.setCallCount(callInteraction.getCallCount() + 1);
			callInteraction.setDroppedCount(callInteraction.getDroppedCount() + 1);

		} else {

			CallInteraction lastCallInit = em.find(CallInteraction.class, interaction_id);

			PSFAssignedInteraction assignedLink = lastCallInit.getPsfAssignedInteraction();
			if(assignedLink.getWyzUser()!=null){
				
			}else{
				assignedLink.setWyzUser(wyz);
				
			}

			if (call_dispo.getDisposition() != null) {
				logger.info("first");
				CallDispositionData callDataDispo = getCallDispositionVariable(call_dispo.getDisposition());
				psf_interaction.setCallDispositionData(callDataDispo);

				if (callDataDispo.getDispositionId() == callDataDispo.STATUS_CALL_ME_LATER) {

					psf_interaction.setIsFollowUpDone("No");
					psf_interaction.setIsFollowupRequired("Yes");

				}

				assignedLink.setFinalDisposition(callDataDispo);
				assignedLink
						.setLastDisposition(lastCallInit.getPsfdisposition().getCallDispositionData().getDisposition());

				if (lastCallInit.getPsfdisposition().getCallDispositionData()
						.getDispositionId() == callDataDispo.STATUS_CALL_ME_LATER) {

					lastCallInit.getPsfdisposition().setIsFollowUpDone("Yes");
					em.merge(lastCallInit.getPsfdisposition());

				}

			} else if (psf_interaction.getPSFDispositon() != null) {
				logger.info("second : " + psf_interaction.getPSFDispositon());
				CallDispositionData callDataDispo = getCallDispositionVariable(psf_interaction.getPSFDispositon());
				psf_interaction.setCallDispositionData(callDataDispo);
				assignedLink.setFinalDisposition(callDataDispo);
				assignedLink
						.setLastDisposition(lastCallInit.getPsfdisposition().getCallDispositionData().getDisposition());

				if (lastCallInit.getPsfdisposition().getCallDispositionData()
						.getDispositionId() == callDataDispo.STATUS_CALL_ME_LATER) {

					lastCallInit.getPsfdisposition().setIsFollowUpDone("Yes");
					em.merge(lastCallInit.getPsfdisposition());

				}

			}
			em.merge(assignedLink);
			callInteraction.setPsfAssignedInteraction(lastCallInit.getPsfAssignedInteraction());
			callInteraction.setCampaign(assignedLink.getCampaign());
			callInteraction.setCallCount(lastCallInit.getCallCount() + 1);
			if (psf_interaction.getPSFDispositon() != null) {

				callInteraction.setDroppedCount(lastCallInit.getDroppedCount() + 1);

			}
		}

		if (call_dispo.getDisposition() != null && call_dispo.getDisposition().equals("Book Appointment")
				&& pick_up.getPickupDate() != null) {

			logger.info("adding pick up data");

			long wyzuser_id = list_dispo.getDriverId();
			if (wyzuser_id != 0) {
				Driver driver_link = source.driver(em).where(u -> u.getWyzUser().getId() == wyzuser_id).getOnlyValue();
				pick_up.setDriver(driver_link);
				BookingDateTime bookingFrom = getBookingTimeById(list_dispo.getTime_From());
				BookingDateTime bookingTo = getBookingTimeById(list_dispo.getTime_To());
				pick_up.setTimeFrom(bookingFrom.getStartTime());

				long ONE_MINUTE_IN_MILLIS = 60000;
				long initailMin = bookingTo.getStartTime().getTime();
				long timeslotdifference = 30;
				long afterAddingMins = initailMin + (timeslotdifference * ONE_MINUTE_IN_MILLIS);
				Time time = new Time(afterAddingMins);
				pick_up.setTimeTo(time);

			}
			em.persist(pick_up);

			psf_interaction.setPsfAppointmentDate(pick_up.getPickupDate());

			psf_interaction.setPickupDrop(pick_up);
		}

		em.persist(callInteraction);

		psf_interaction.setCallInteraction(callInteraction);
		em.persist(psf_interaction);

	}

    private BookingDateTime getBookingTimeById(long bookId) {
		// TODO Auto-generated method stub
		return source.bookingDateTime(em).where(u -> u.getId() == bookId).getOnlyValue();
	}
    
    private CallDispositionData getCallDispositionVariable(String callDisposition) {
        logger.info(" callDisposition: " + callDisposition);
        CallDispositionData call_data = source.callDispositionDatas(em).where(u -> u.getDisposition().equals(callDisposition)).getOnlyValue();
        return call_data;
    }

    @Override
    public List<AssignedInteraction> getPSFListAllForWyzUserAssigned(String userLoginName) {
        WyzUser wyzUserData = source.wyzUsers(em).where(u -> u.getUserName().equals(userLoginName)).getOnlyValue();
        long wyzData = wyzUserData.getId();
        logger.info("user is:" + wyzData);
       
        Campaign campaignData = source.campaigns(em).where(u -> u.getCampaignName().equals("PSF1")).getOnlyValue();
            if(campaignData.getCampaignName().equals("PSF1")){
                        long campaignid = campaignData.getId();

                 Hibernate.initialize(campaignData.getAssignedInteractions());
        logger.info("campaignId:" + campaignData.getId());
        List<AssignedInteraction> assignedPsfData = source.assignedInteractions(em)
                .where(u -> u.getCallMade().equals("No"))
                .where(u -> u.getCampaign().getId() == campaignid)
                .where(u -> u.getWyzUser().getId() == wyzData)
                .toList();
        logger.info("assignedPsfData:" + assignedPsfData.size());
        return assignedPsfData;
//            }else  if(campaignData.getCampaignName().equals("PSF2")){
//                                        long campaignid = campaignData.getId();
// Hibernate.initialize(campaignData.getAssignedInteractions());
//        logger.info("campaignId:" + campaignData.getId());
//        List<AssignedInteraction> assignedPsfData = source.assignedInteractions(em)
//                .where(u -> u.getCallMade().equals("No"))
//                .where(u -> u.getCampaign().getId() == campaignid)
//                .where(u -> u.getWyzUser().getId() == wyzData)
//                .toList();
//        logger.info("assignedPsfData:" + assignedPsfData.size());
//        return assignedPsfData;
//                
//            }else  if(campaignData.getCampaignName().equals("PSF3")){
//                                        long campaignid = campaignData.getId();
//
//                 Hibernate.initialize(campaignData.getAssignedInteractions());
//        logger.info("campaignId:" + campaignData.getId());
//        List<AssignedInteraction> assignedPsfData = source.assignedInteractions(em)
//                .where(u -> u.getCallMade().equals("No"))
//                .where(u -> u.getCampaign().getId() == campaignid)
//                .where(u -> u.getWyzUser().getId() == wyzData)
//                .toList();
//        logger.info("assignedPsfData:" + assignedPsfData.size());
//        return assignedPsfData;
//                
//            }else  if(campaignData.getCampaignName().equals("PSF4")){
//                
//                                        long campaignid = campaignData.getId();
//
//                 Hibernate.initialize(campaignData.getAssignedInteractions());
//        logger.info("campaignId:" + campaignData.getId());
//        List<AssignedInteraction> assignedPsfData = source.assignedInteractions(em)
//                .where(u -> u.getCallMade().equals("No"))
//                .where(u -> u.getCampaign().getId() == campaignid)
//                .where(u -> u.getWyzUser().getId() == wyzData)
//                .toList();
//        logger.info("assignedPsfData:" + assignedPsfData.size());
//        return assignedPsfData;
            }else {
                
                return null;
            }
       
    }
    
    
    
//    @Override
//    public long getAllAssignedInteractionPsf(long id) {
//        
//                StoredProcedureQuery sQuery = em.createStoredProcedureQuery("PSFassigned_intercation_count");
//		
//		sQuery.registerStoredProcedureParameter("wyzuserid", Integer.class, ParameterMode.IN);
//		sQuery.setParameter("wyzuserid", (int)id);
//		
//		BigInteger bigCount = (BigInteger)sQuery.getSingleResult();
//
//		return bigCount.longValue();
//    }
    
//        @Override
//    public List<CallLogAjaxLoad> assignedListOfUserPsf(String pattern,long id, long fromIndex, long toIndex) {
//        javax.persistence.Query query = em.createNativeQuery("CALL PSFServiceReminderwithSearch(:pattern,:wyzuserid, :start_with, :length)","CallLogAjaxLoad");
//                
//                query.setParameter("pattern", pattern);
//		query.setParameter("wyzuserid", id);
//		query.setParameter("start_with", fromIndex);
//		query.setParameter("length", toIndex);
//		
//                logger.info("id is ass: "+id+"fromIndex : "+fromIndex+"toIndex :"+toIndex); 
//            
//		return query.getResultList();
//    }

//    @Override
//    public long PSFServiceReminderwithSearchCount(long id) {
//
//                StoredProcedureQuery sQuery = em.createStoredProcedureQuery("PSFServiceReminderwithSearchCount");
//		
//		sQuery.registerStoredProcedureParameter("wyzuserid", Integer.class, ParameterMode.IN);
//		sQuery.setParameter("wyzuserid", (int)id);
//		
//		BigInteger bigCount = (BigInteger)sQuery.getSingleResult();
//
//		return bigCount.longValue();   
//    
//    }
//
//    @Override
//    public List<CallLogAjaxLoad> PSFServiceReminderwithSearch(String searchPattern, long id, long fromIndex, long toIndex) {
//  javax.persistence.Query query = em.createNativeQuery("CALL PSFServiceReminderwithSearch(:pattern,:wyzuserid, :start_with, :length)","CallLogAjaxLoad");
//                
//                query.setParameter("pattern", pattern);
//		query.setParameter("wyzuserid", id);
//		query.setParameter("start_with", fromIndex);
//		query.setParameter("length", toIndex);
//		
//                logger.info("id is ass: "+id+"fromIndex : "+fromIndex+"toIndex :"+toIndex); 
//            
//		return query.getResultList();
//    }  
 @Override
    public long PSFServiceReminderwithSearchCount(long id ,long typeOfPSF) {

    //StoredProcedureQuery sQuery = em.createStoredProcedureQuery("1stPSFServiceReminderwithSearchCount(:name)");
    
      javax.persistence.Query query = em.createNativeQuery("CALL 1stPSFServiceReminderwithSearchCount(:wyzuserid,:psf_id)");


//query.registerStoredProcedureParameter("wyzuserid", Integer.class, ParameterMode.IN);
    query.setParameter("wyzuserid", (int)id);
    query.setParameter("psf_id", (int)typeOfPSF);

    BigInteger bigCount = (BigInteger)query.getSingleResult();

    return bigCount.longValue();   
    
    }

    @Override
    public List<CallLogAjaxLoad> PSFServiceReminderwithSearch(String searchPattern, long id, long fromIndex, long toIndex,long typeOfPSF) {
  javax.persistence.Query query = em.createNativeQuery("CALL 1stPSFServiceReminderwithSearch(:pattern,:wyzuserid, :start_with, :length,:psf_id)","CallLogAjaxLoad");
                
                query.setParameter("pattern", searchPattern);
                query.setParameter("wyzuserid", (int)id);
                query.setParameter("start_with", fromIndex);
                query.setParameter("length", toIndex);
                 query.setParameter("psf_id", (int)typeOfPSF);

                logger.info("id is ass: "+id+"fromIndex : "+fromIndex+"toIndex :"+toIndex); 

                return query.getResultList();
    }  

    @Override
    public long PSFFollowUPRequiredwithSearchCount(long id,long typeOfPSF) {
      javax.persistence.Query query = em.createNativeQuery("CALL psffollwuprequiredCount(:wyzuserid,:psf_id)");


//query.registerStoredProcedureParameter("wyzuserid", Integer.class, ParameterMode.IN);
    query.setParameter("wyzuserid", (int)id);
    query.setParameter("psf_id", (int)typeOfPSF);

    BigInteger bigCount = (BigInteger)query.getSingleResult();

    return bigCount.longValue();  
    }
    @Override
    public List<CallLogDispositionLoad> PSFFollowUPRequiredwithSearch(String searchPattern, long id, long fromIndex, long toIndex,long typeOfPSF) {
       javax.persistence.Query query = em.createNativeQuery("CALL psffollwuprequired(:pattern,:wyzuserid, :start_with, :length,:psf_id)","CallLogDispositionLoad");
              
                query.setParameter("pattern", searchPattern);
                query.setParameter("wyzuserid", id);
                query.setParameter("start_with", fromIndex);
                query.setParameter("length", toIndex);
                query.setParameter("psf_id", typeOfPSF);

                logger.info("id is ass: "+id+"fromIndex : "+fromIndex+"toIndex :"+toIndex); 

                return query.getResultList();
    }


    
     @Override
    public long PSF_Non_contactsCount(long id,long typeOfPSF) {

         javax.persistence.Query query = em.createNativeQuery("CALL psfnoncontactsCount(:wyzuserid,:psf_id)");


//query.registerStoredProcedureParameter("wyzuserid", Integer.class, ParameterMode.IN);
    query.setParameter("wyzuserid", (int)id);
    query.setParameter("psf_id", (int)typeOfPSF);

    BigInteger bigCount = (BigInteger)query.getSingleResult();

    return bigCount.longValue();  
    }
    
    @Override
    public List<CallLogDispositionLoad> PSF_Non_contacts(String searchPattern, long id, long fromIndex, long toIndex,long typeOfPSF) {

          javax.persistence.Query query = em.createNativeQuery("CALL psfnoncontacts(:pattern,:wyzuserid, :start_with, :length,:psf_id)","CallLogDispositionLoad");
                
                query.setParameter("pattern", searchPattern);
query.setParameter("wyzuserid", id);
query.setParameter("start_with", fromIndex);
query.setParameter("length", toIndex);
    query.setParameter("psf_id", (int)typeOfPSF);


               // logger.info("id is ass: "+id+"fromIndex : "+fromIndex+"toIndex :"+toIndex); 
            
return query.getResultList();
    }

   /* @Override
    public long PSF_Servey_completeCount(long id,long typeOfPSF) {
               javax.persistence.Query query = em.createNativeQuery("CALL psfsurveyCompletedCount(:wyzuserid,:psf_id)");


//query.registerStoredProcedureParameter("wyzuserid", Integer.class, ParameterMode.IN);
    query.setParameter("wyzuserid", (int)id);
    query.setParameter("psf_id", (int)typeOfPSF);

    BigInteger bigCount = (BigInteger)query.getSingleResult();

    return bigCount.longValue();   
    }

    @Override
    public List<CallLogDispositionLoad> PSF_Servey_complete(String searchPattern, long id, long fromIndex, long toIndex,long typeOfPSF) {
   javax.persistence.Query query = em.createNativeQuery("CALL psfsurveycompleted(:pattern,:wyzuserid, :start_with, :length,:psf_id)","CallLogDispositionLoad");
                
                query.setParameter("pattern", searchPattern);
                query.setParameter("wyzuserid", id);
                query.setParameter("start_with", fromIndex);
                query.setParameter("length", toIndex);
                query.setParameter("psf_id", (int)typeOfPSF);

               // logger.info("id is ass: "+id+"fromIndex : "+fromIndex+"toIndex :"+toIndex); 

                return query.getResultList();
    }

    @Override
    public long PSF_droppedCount(long id,long typeOfPSF) {
         javax.persistence.Query query = em.createNativeQuery("CALL PSF_droppedCount(:wyzuserid,:psf_id)");


//query.registerStoredProcedureParameter("wyzuserid", Integer.class, ParameterMode.IN);
    query.setParameter("wyzuserid", (int)id);
    query.setParameter("psf_id", (int)typeOfPSF);

    BigInteger bigCount = (BigInteger)query.getSingleResult();

    return bigCount.longValue();   
    }

    @Override
    public List<CallLogDispositionLoad> PSF_dropped(String searchPattern, long id, long fromIndex, long toIndex ,long typeOfPSF) {
   javax.persistence.Query query = em.createNativeQuery("CALL PSF_dropped(:pattern,:wyzuserid, :start_with, :length,:psf_id)","CallLogDispositionLoad");
                
                query.setParameter("pattern", searchPattern);
                query.setParameter("wyzuserid", id);
                query.setParameter("start_with", fromIndex);
                query.setParameter("length", toIndex);
                query.setParameter("psf_id", typeOfPSF);

                                //logger.info("id is ass: "+id+"fromIndex : "+fromIndex+"toIndex :"+toIndex); 

                return query.getResultList();

    }*/

    @Override
    public List<Campaign> getPsflistCampaign() {
        List<Campaign> listpsf =source.campaigns(em).where(u -> u.getCampaignName().equals("PSF")).toList();
        return listpsf;
    }
	
	    @Override
    public long PSF15ServiceReminderwithSearchCount(long id, String name) {
       StoredProcedureQuery sQuery = em.createStoredProcedureQuery("1stPSFServiceReminderwithSearchCount(:name)");
    
      javax.persistence.Query query = em.createNativeQuery("CALL 1stPSFServiceReminderwithSearchCount(:wyzuserid,:name)");


//query.registerStoredProcedureParameter("wyzuserid", Integer.class, ParameterMode.IN);
    query.setParameter("wyzuserid", (int)id);
    query.setParameter("name", 5);

    BigInteger bigCount = (BigInteger)query.getSingleResult();

    return bigCount.longValue(); 
    }

    @Override
    public List<CallLogAjaxLoad> PSF15ServiceReminderwithSearch(String searchPattern, long id, long fromIndex, long toIndex, String name) {
    javax.persistence.Query query = em.createNativeQuery("CALL 1stPSFServiceReminderwithSearch(:pattern,:wyzuserid, :start_with, :length,:name)","CallLogAjaxLoad");
                
                query.setParameter("pattern", searchPattern);
                query.setParameter("wyzuserid", id);
                query.setParameter("start_with", fromIndex);
                query.setParameter("length", toIndex);
                 query.setParameter("name", 5);

               // logger.info("id is ass: "+id+"fromIndex : "+fromIndex+"toIndex :"+toIndex); 

                return query.getResultList();
    }

    @Override
    public long PSF15FollowUPRequiredwithSearchCount(long id) {
    javax.persistence.Query query = em.createNativeQuery("CALL psffollwuprequiredCount(:wyzuserid,:name)");


//query.registerStoredProcedureParameter("wyzuserid", Integer.class, ParameterMode.IN);
    query.setParameter("wyzuserid", (int)id);
    query.setParameter("name", 5);

    BigInteger bigCount = (BigInteger)query.getSingleResult();

    return bigCount.longValue();  
    }

    @Override
    public List<CallLogDispositionLoad> PSF15FollowUPRequiredwithSearch(String searchPattern, long id, long fromIndex, long toIndex) {
    javax.persistence.Query query = em.createNativeQuery("CALL psffollwuprequired(:pattern,:wyzuserid, :start_with, :length,:name)","CallLogDispositionLoad");
              
                query.setParameter("pattern", searchPattern);
                query.setParameter("wyzuserid", id);
                query.setParameter("start_with", fromIndex);
                query.setParameter("length", toIndex);
                query.setParameter("name", 5);

               // logger.info("id is ass: "+id+"fromIndex : "+fromIndex+"toIndex :"+toIndex); 

                return query.getResultList();
    }

    @Override
    public long PSF15_Servey_completeCount(long id) {
    javax.persistence.Query query = em.createNativeQuery("CALL psfsurveyCompletedCount(:wyzuserid,:name)");


//query.registerStoredProcedureParameter("wyzuserid", Integer.class, ParameterMode.IN);
    query.setParameter("wyzuserid", (int)id);
    query.setParameter("name", 5);

    BigInteger bigCount = (BigInteger)query.getSingleResult();

    return bigCount.longValue();  
    }

    @Override
    public List<CallLogDispositionLoad> PSF15_Servey_complete(String searchPattern, long id, long fromIndex, long toIndex) {
   javax.persistence.Query query = em.createNativeQuery("CALL psfsurveycompleted(:pattern,:wyzuserid, :start_with, :length,:name)","CallLogDispositionLoad");
                
                query.setParameter("pattern", searchPattern);
                query.setParameter("wyzuserid", id);
                query.setParameter("start_with", fromIndex);
                query.setParameter("length", toIndex);
                query.setParameter("name", 5);

               // logger.info("id is ass: "+id+"fromIndex : "+fromIndex+"toIndex :"+toIndex); 

                return query.getResultList();
				
    }

    @Override
    public long PSF15_Non_contactsCount(long id) {
    javax.persistence.Query query = em.createNativeQuery("CALL psfnoncontactsCount(:wyzuserid,:name)");


//query.registerStoredProcedureParameter("wyzuserid", Integer.class, ParameterMode.IN);
    query.setParameter("wyzuserid", (int)id);
    query.setParameter("name", 5);

    BigInteger bigCount = (BigInteger)query.getSingleResult();

    return bigCount.longValue();  
    }

    @Override
    public List<CallLogDispositionLoad> PSF15_Non_contacts(String searchPattern, long id, long fromIndex, long toIndex) {
   javax.persistence.Query query = em.createNativeQuery("CALL psfnoncontacts(:pattern,:wyzuserid, :start_with, :length,:name)","CallLogDispositionLoad");
                
                query.setParameter("pattern", searchPattern);
query.setParameter("wyzuserid", id);
query.setParameter("start_with", fromIndex);
query.setParameter("length", toIndex);
    query.setParameter("name", 5);


              //  logger.info("id is ass: "+id+"fromIndex : "+fromIndex+"toIndex :"+toIndex); 
            
return query.getResultList();

    }

    @Override
    public long PSF15_droppedCount(long id) {
     javax.persistence.Query query = em.createNativeQuery("CALL PSF_droppedCount(:wyzuserid,:name)");


//query.registerStoredProcedureParameter("wyzuserid", Integer.class, ParameterMode.IN);
    query.setParameter("wyzuserid", (int)id);
    query.setParameter("name", 5);

    BigInteger bigCount = (BigInteger)query.getSingleResult();

    return bigCount.longValue();   
    }

    @Override
    public List<CallLogDispositionLoad> PSF15_dropped(String searchPattern, long id, long fromIndex, long toIndex) {
   javax.persistence.Query query = em.createNativeQuery("CALL PSF_dropped(:pattern,:wyzuserid, :start_with, :length,:name)","CallLogDispositionLoad");
                
                query.setParameter("pattern", searchPattern);
                query.setParameter("wyzuserid", id);
                query.setParameter("start_with", fromIndex);
                query.setParameter("length", toIndex);
                query.setParameter("name", 5);

                              //  logger.info("id is ass: "+id+"fromIndex : "+fromIndex+"toIndex :"+toIndex); 

                return query.getResultList();
    }

    @Override
    public long PSF30ServiceReminderwithSearchCount(long id, String name) {
   javax.persistence.Query query = em.createNativeQuery("CALL 1stPSFServiceReminderwithSearchCount(:wyzuserid,:name)");


//query.registerStoredProcedureParameter("wyzuserid", Integer.class, ParameterMode.IN);
    query.setParameter("wyzuserid", (int)id);
    query.setParameter("name", 6);

    BigInteger bigCount = (BigInteger)query.getSingleResult();

    return bigCount.longValue();   
    }

    @Override
    public List<CallLogAjaxLoad> PSF30ServiceReminderwithSearch(String searchPattern, long id, long fromIndex, long toIndex, String name) {
   javax.persistence.Query query = em.createNativeQuery("CALL 1stPSFServiceReminderwithSearch(:pattern,:wyzuserid, :start_with, :length,:name)","CallLogAjaxLoad");
                
                query.setParameter("pattern", searchPattern);
                query.setParameter("wyzuserid", id);
                query.setParameter("start_with", fromIndex);
                query.setParameter("length", toIndex);
                 query.setParameter("name", 6);

               // logger.info("id is ass: "+id+"fromIndex : "+fromIndex+"toIndex :"+toIndex); 

                return query.getResultList();
    }

    @Override
    public long PSF30FollowUPRequiredwithSearchCount(long id) {
   javax.persistence.Query query = em.createNativeQuery("CALL psffollwuprequiredCount(:wyzuserid,:name)");


//query.registerStoredProcedureParameter("wyzuserid", Integer.class, ParameterMode.IN);
    query.setParameter("wyzuserid", (int)id);
    query.setParameter("name", 6);

    BigInteger bigCount = (BigInteger)query.getSingleResult();

    return bigCount.longValue();  
    }

    @Override
    public List<CallLogDispositionLoad> PSF30FollowUPRequiredwithSearch(String searchPattern, long id, long fromIndex, long toIndex) {
   javax.persistence.Query query = em.createNativeQuery("CALL psffollwuprequired(:pattern,:wyzuserid, :start_with, :length,:name)","CallLogDispositionLoad");
              
                query.setParameter("pattern", searchPattern);
                query.setParameter("wyzuserid", id);
                query.setParameter("start_with", fromIndex);
                query.setParameter("length", toIndex);
                query.setParameter("name", 6);

                logger.info("id is ass: "+id+"fromIndex : "+fromIndex+"toIndex :"+toIndex); 

                return query.getResultList();
    }

    @Override
    public long PSF30_Servey_completeCount(long id) {
  javax.persistence.Query query = em.createNativeQuery("CALL psfsurveyCompletedCount(:wyzuserid,:name)");


//query.registerStoredProcedureParameter("wyzuserid", Integer.class, ParameterMode.IN);
    query.setParameter("wyzuserid", (int)id);
    query.setParameter("name", 6);

    BigInteger bigCount = (BigInteger)query.getSingleResult();

    return bigCount.longValue();   
    }

    @Override
    public List<CallLogDispositionLoad> PSF30_Servey_complete(String searchPattern, long id, long fromIndex, long toIndex) {
    javax.persistence.Query query = em.createNativeQuery("CALL psfsurveycompleted(:pattern,:wyzuserid, :start_with, :length,:name)","CallLogDispositionLoad");
                
                query.setParameter("pattern", searchPattern);
                query.setParameter("wyzuserid", id);
                query.setParameter("start_with", fromIndex);
                query.setParameter("length", toIndex);
                query.setParameter("name", 6);

                logger.info("id is ass: "+id+"fromIndex : "+fromIndex+"toIndex :"+toIndex); 

                return query.getResultList();
				
    }

    @Override
    public long PSF30_Non_contactsCount(long id) {
   javax.persistence.Query query = em.createNativeQuery("CALL psfnoncontactsCount(:wyzuserid,:name)");


//query.registerStoredProcedureParameter("wyzuserid", Integer.class, ParameterMode.IN);
    query.setParameter("wyzuserid", (int)id);
    query.setParameter("name", 6);

    BigInteger bigCount = (BigInteger)query.getSingleResult();

    return bigCount.longValue();  
    }

    @Override
    public List<CallLogDispositionLoad> PSF30_Non_contacts(String searchPattern, long id, long fromIndex, long toIndex) {
    javax.persistence.Query query = em.createNativeQuery("CALL psfnoncontacts(:pattern,:wyzuserid, :start_with, :length,:name)","CallLogDispositionLoad");
                
                query.setParameter("pattern", searchPattern);
query.setParameter("wyzuserid", id);
query.setParameter("start_with", fromIndex);
query.setParameter("length", toIndex);
    query.setParameter("name", 6);


                logger.info("id is ass: "+id+"fromIndex : "+fromIndex+"toIndex :"+toIndex); 
            
return query.getResultList();
    }

    @Override
    public long PSF30_droppedCount(long id) {
   javax.persistence.Query query = em.createNativeQuery("CALL PSF_droppedCount(:wyzuserid,:name)");


//query.registerStoredProcedureParameter("wyzuserid", Integer.class, ParameterMode.IN);
    query.setParameter("wyzuserid", (int)id);
    query.setParameter("name", 6);

    BigInteger bigCount = (BigInteger)query.getSingleResult();

    return bigCount.longValue();  
    }

    @Override
    public List<CallLogDispositionLoad> PSF30_dropped(String searchPattern, long id, long fromIndex, long toIndex) {
  javax.persistence.Query query = em.createNativeQuery("CALL PSF_dropped(:pattern,:wyzuserid, :start_with, :length,:name)","CallLogDispositionLoad");
                
                query.setParameter("pattern", searchPattern);
                query.setParameter("wyzuserid", id);
                query.setParameter("start_with", fromIndex);
                query.setParameter("length", toIndex);
                query.setParameter("name", 6);

                                logger.info("id is ass: "+id+"fromIndex : "+fromIndex+"toIndex :"+toIndex); 

                return query.getResultList();
    }
    
    @Override
	public void addUpsellLeadOfPSF(PSFInteraction psf_interaction, ListingForm list_dispo) {
		
    	if(list_dispo.getUpsellLead()!=null){
    	
		List<UpsellLead> upselldata = list_dispo.getUpsellLead();

		long countOfUpsell = upselldata.size();
		logger.info("count of upsell In PSf : " + countOfUpsell);
		Vehicle linkedVeh = em.find(Vehicle.class, list_dispo.getVehicalId());
		if (countOfUpsell != 0) {
			for (UpsellLead typeData : upselldata) {
				if (typeData.getUpsellId() != 0) {

					logger.info("typeData.getUpsellId() is "+typeData.getUpsellId());
					
					TaggingUsers taggedUserData = getTaggedUserByType(typeData.getUpsellId());

					UpsellLead addupselllead = new UpsellLead();
					addupselllead.setTaggedTo(typeData.getTaggedTo());
					addupselllead.setUpSellType(typeData.getUpSellType());
					addupselllead.setUpsellComments(typeData.getUpsellComments());
					addupselllead.setPsfInteraction(psf_interaction);
					addupselllead.setVehicle(linkedVeh);
					addupselllead.setTaggingUsers(taggedUserData);
					em.persist(addupselllead);

				}
			}
		
	}
		
		long call_int_id = psf_interaction.getCallInteraction().getId();
		long ins_id = psf_interaction.getId();
		long countOfUpsellIs = source.upsellLeads(em).where(u -> u.getPsfInteraction() != null)
				.where(u -> u.getPsfInteraction().getId() == ins_id).count();
		logger.info("countOfUpsellIs : " + countOfUpsellIs);
		PSFInteraction insCount = em.find(PSFInteraction.class, ins_id);
		insCount.setUpsellCount(countOfUpsellIs);
		em.merge(insCount);
    	}
    
}
    
    private TaggingUsers getTaggedUserByType(int upSellId) {
		// logger.info("upSellType is : "+upSellType);

		return source.taggingUsers(em).where(u -> u.getUpsellLeadId() == upSellId).getOnlyValue();
	}

	@Override
	public List<PSFFollowupNotificationModel> getFollowUpNotification(long id) {
		javax.persistence.Query query = em.createNativeQuery("CALL psffollowupnotification(:id)","PSFFollowupNotificationModel");        
        query.setParameter("id", id);
                  
		return query.getResultList();
	}
	
	//scheduled or assigned 
	@Override
	public long PSFScheduledCallsCount(String fromBillDate,String toBillDate,String typeOfPSF,String searchPattern,long id,long fromIndex,long toIndex) {
		
		StoredProcedureQuery sQuery = em.createStoredProcedureQuery("PSFScheduledCallsCount");
		sQuery.registerStoredProcedureParameter("instartdate", String.class, ParameterMode.IN); // coulmn names
		sQuery.registerStoredProcedureParameter("inenddate", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("psfcampaignid", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("pattern", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("inwyzuser_id", Integer.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("start_with", Integer.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("length", Integer.class, ParameterMode.IN);
		
		
		sQuery.setParameter("instartdate", fromBillDate);
		sQuery.setParameter("inenddate", toBillDate);
		sQuery.setParameter("psfcampaignid", typeOfPSF);
		sQuery.setParameter("pattern",searchPattern);
		sQuery.setParameter("inwyzuser_id", (int)id);
		sQuery.setParameter("start_with", (int)fromIndex);
		sQuery.setParameter("length", (int)toIndex);
		

		BigInteger bigCount = (BigInteger) sQuery.getSingleResult();

		return bigCount.longValue();
	}
	
	@Override
	public List<CallLogAjaxLoad> PSFScheduledCalls(String fromBillDate,String toBillDate,String typeOfPSF,String searchPattern,long id,long fromIndex,long toIndex) {
		
		javax.persistence.Query query = em.createNativeQuery(
				"CALL PSFScheduledCalls(:instartdate,:inenddate,:psfcampaignid,:pattern,:inwyzuser_id,:start_with,:length)",
				"CallLogAjaxLoadPSFScheduled");

		query.setParameter("instartdate", fromBillDate);
		query.setParameter("inenddate", toBillDate);
		query.setParameter("psfcampaignid", typeOfPSF);
		query.setParameter("pattern",searchPattern);
		query.setParameter("inwyzuser_id", (int)id);
		query.setParameter("start_with", (int)fromIndex);
		query.setParameter("length", (int)toIndex);

		// logger.info("id is dropped : "+id+"fromIndex : "+fromIndex+"toIndex
		// :"+toIndex);

		return query.getResultList();
	}



// followup , completed survey, appointments
	@Override
	public long PSFCountForFollowUpCompleteSurveyAppointments(String fromBillDate, String toBillDate, String typeOfPSF,
			String searchPattern, long id, long dispoType) {
		
		logger.info("fromBillDate"+fromBillDate+" toBillDate : "+toBillDate+" typeOfPSF"+ typeOfPSF+
			" searchPattern : "+ searchPattern +" id : "+ id +" dispoType : " + dispoType);
		
			StoredProcedureQuery sQuery = em.createStoredProcedureQuery("PSFfileterForContactsCount");
			sQuery.registerStoredProcedureParameter("instartdate", String.class, ParameterMode.IN); // coulmn names
			sQuery.registerStoredProcedureParameter("inenddate", String.class, ParameterMode.IN);
			sQuery.registerStoredProcedureParameter("psfcampaignid", String.class, ParameterMode.IN);
			sQuery.registerStoredProcedureParameter("pattern", String.class, ParameterMode.IN);
			sQuery.registerStoredProcedureParameter("inwyzuser_id", Integer.class, ParameterMode.IN);
			sQuery.registerStoredProcedureParameter("dispositiontype", Integer.class, ParameterMode.IN);
			
			
			sQuery.setParameter("instartdate", fromBillDate);
			sQuery.setParameter("inenddate", toBillDate);
			sQuery.setParameter("psfcampaignid", typeOfPSF);
			sQuery.setParameter("pattern",searchPattern);
			sQuery.setParameter("inwyzuser_id", (int)id);
			sQuery.setParameter("dispositiontype", (int)dispoType);
			
	
			BigInteger bigCount = (BigInteger) sQuery.getSingleResult();
	
			return bigCount.longValue();
	}

	@Override
	public List<CallLogDispositionLoad> PSFCallsFollowUpCompleteSurveyAppointments(String fromBillDate,
			String toBillDate, String typeOfPSF, String searchPattern, long id, long dispoType, long fromIndex,
			long toIndex) {
		
		logger.info("fromBillDate list : "+fromBillDate+" toBillDate : "+toBillDate+" typeOfPSF"+ typeOfPSF+
				" searchPattern : "+ searchPattern +" id : "+ id +" dispoType : " + dispoType);
			
		
		javax.persistence.Query query = em.createNativeQuery(
				"CALL PSFfileterForContacts(:instartdate,:inenddate,:psfcampaignid,:pattern,:inwyzuser_id,:disposition_type,:start_with,:length)",
				"PSFCallsFollowUpCompleteSurveyAppointmentsIncompletedSurvey");

		query.setParameter("instartdate", fromBillDate);
		query.setParameter("inenddate", toBillDate);
		query.setParameter("psfcampaignid", typeOfPSF);
		query.setParameter("pattern",searchPattern);
		query.setParameter("inwyzuser_id", id);
		query.setParameter("disposition_type" , dispoType);
		query.setParameter("start_with", fromIndex);
		query.setParameter("length", toIndex);

		// logger.info("id is dropped : "+id+"fromIndex : "+fromIndex+"toIndex
		// :"+toIndex);

		return query.getResultList();
	}

	
    
// non cojtacts and dropped

	@Override
	public long PSFNoncontactsCount(String fromBillDate, String toBillDate, String typeOfPSF, String searchPattern,
			long id, long dispoType) {
		StoredProcedureQuery sQuery = em.createStoredProcedureQuery("PSFfileterForNonContactsCount");
		sQuery.registerStoredProcedureParameter("instartdate", String.class, ParameterMode.IN); // coulmn names
		sQuery.registerStoredProcedureParameter("inenddate", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("psfcampaignid", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("pattern", String.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("inwyzuser_id", Integer.class, ParameterMode.IN);
		sQuery.registerStoredProcedureParameter("disposition_type", Integer.class, ParameterMode.IN);
		
		
		sQuery.setParameter("instartdate", fromBillDate);
		sQuery.setParameter("inenddate", toBillDate);
		sQuery.setParameter("psfcampaignid", typeOfPSF);
		sQuery.setParameter("pattern",searchPattern);
		sQuery.setParameter("inwyzuser_id", (int)id);
		sQuery.setParameter("disposition_type", (int)dispoType);
		

		BigInteger bigCount = (BigInteger) sQuery.getSingleResult();

		return bigCount.longValue();
		
	}

	@Override
	public List<CallLogDispositionLoad> PSFNoncontacts(String fromBillDate, String toBillDate, String typeOfPSF,
			String searchPattern, long id, long dispoType, long fromIndex, long toIndex) {
		javax.persistence.Query query = em.createNativeQuery(
				"CALL PSFfileterForNonContacts(:instartdate,:inenddate,:psfcampaignid,:pattern,:inwyzuser_id,:disposition_type,:start_with,:length)",
				"CallLogDispositionLoadPSFNonContactDropped");

		query.setParameter("instartdate", fromBillDate);
		query.setParameter("inenddate", toBillDate);
		query.setParameter("psfcampaignid", typeOfPSF);
		query.setParameter("pattern",searchPattern);
		query.setParameter("inwyzuser_id", id);
		query.setParameter("disposition_type" ,dispoType);
		query.setParameter("start_with", fromIndex);
		query.setParameter("length", toIndex);

		// logger.info("id is dropped : "+id+"fromIndex : "+fromIndex+"toIndex
		// :"+toIndex);

		return query.getResultList();
	}

	@Override
	public PSFAssignedInteraction getLastPSFAssignStatusOfVehicle(long vehicle_id) {
		// TODO Auto-generated method stub
		
		long psfStatusExist=source.psfAssignedInteraction(em).where(u -> u.getVehicle()!=null)
				.where(u -> u.getVehicle().getVehicle_id()==vehicle_id)
				.where(u -> u.getFinalDisposition()!=null)
				.count();
		
		logger.info("count of psfStatusExist : "+psfStatusExist);
		
		PSFAssignedInteraction lastPSFAssigned=new PSFAssignedInteraction();
		if(psfStatusExist>0){
			
			long idIs=source.psfAssignedInteraction(em).where(u -> u.getVehicle()!=null)
					.where(u -> u.getVehicle().getVehicle_id()==vehicle_id)
					.where(u -> u.getFinalDisposition()!=null).max(u -> u.getId());
			
			lastPSFAssigned=source.psfAssignedInteraction(em).where(u -> u.getId() == idIs).getOnlyValue();
			Hibernate.initialize(lastPSFAssigned.getService());
			Hibernate.initialize(lastPSFAssigned.getCampaign());
			
		}
		
		return lastPSFAssigned;
	}
}

