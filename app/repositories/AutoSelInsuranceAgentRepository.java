package repositories;

import java.util.Date;
import java.util.List;

import javax.inject.Named;

import controllers.webmodels.InsuranceAgentIdName;

@Named
public interface AutoSelInsuranceAgentRepository {

	public void getAllInsurAgentDetails(String dealercodeIs);

	public List<InsuranceAgentIdName> getInsurAgentDetails(Date date);

}
