/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.inject.Named;
import javax.inject.Singleton;

import com.google.firebase.database.DataSnapshot;

import models.Address;
import models.AssignedInteraction;
import models.CallInteraction;
import models.Campaign;
import models.Complaint;
import models.ServiceBooked;
import models.ComplaintInteraction;
import models.Customer;
import models.CustomerJson;
import models.SRDisposition;
import models.PickupDrop;
import models.UploadFileFields;
import models.Vehicle;
import models.ServiceAdvisor;
import models.Workshop;
import models.Driver;
import models.Email;
import models.JobCardDetailsUpload;
import models.Location;
import models.NewCustomer;
import models.Phone;
import models.Service;
import models.TaggingUsers;
import models.UnAvailability;
import models.UploadData;
import models.UploadMasterFormat;
import models.WorkshopSummary;
import models.WyzUser;
import models.Role;
import models.workshopBill;
import models.ServiceTypes;
import models.SMSTemplate;
import models.MessageParameters;
import models.MessageTemplete;
import models.SavedSearchResult;
import models.CustomerSearchResult;
import controllers.webmodels.AssignedListForCRE;
import controllers.webmodels.CustomerDataOnTabLoad;

/**
 *
 * @author W-1004
 */

@Named
public interface SMSTemplateRepository{

	public List<CustomerSearchResult> getCustomerList(long savedsearchname);

	public List<MessageParameters> getAllMsgParameters();

	public List<SMSTemplate> getAllSMSTemplate();
	
	public List<SavedSearchResult> getAllSavedSearchList();
	
	public SMSTemplate addNewSMSTemplate(String msgTemp , String msgapi , String userLoginName, String userLogindealerCode);
		
	//public SMSTemplate addNewSMSTemplate(String messageTemplate ,String userLoginName, String userLogindealerCode);
	public List<CustomerSearchResult> getFilteredSearchResult(long savedName, List<Long> ids, boolean selectAllFlag);
	
	public Map<String,String> getMessagesForSMSBlast(String userLoginName,long savedName,List<Long> searchResultIds, long smstemplateId, boolean selectAll);

	boolean SendBulkSMSToUsers(String userLoginName,Map<String, String> dataList, long smstemplateId);


	public List<CustomerSearchResult> getSelectedCustomerList(long savedsearchname, List<Long> listOfIds,boolean setFlag);

	//public void sendbulkSMSTemplate(long userId, String tenentCode, List<Map<String, String>> listData);

	public SMSTemplate getSmsTemplate(String smsType);
	public Phone getPhonenumberbyUploadId(long upId);

	public SMSTemplate getSMSAPI(String userLogindealerCode);

	//public SMSTemplate getSMSAPIbyDealer(String dealer_Code);
}