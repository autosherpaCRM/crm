/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.http.client.utils.URIBuilder;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import configs.JinqSource;
import models.SMSParameters;
import models.Customer;
import models.Driver;
import models.MessageTemplete;
import models.SMSInteraction;
import models.SMSTemplate;
import models.Service;
import models.Vehicle;
import models.WyzUser;
import play.Logger.ALogger;

/**
 *
 * @author W-09
 */
@Repository("sMSTriggerRespository")
@Transactional
public class SMSTriggerRespositoryImpl implements SMSTriggerRespository {
	ALogger logger = play.Logger.of("application");

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private JinqSource source;

	@Override
	public void SMSTriggerBulk(String userLogindealerCode, long vehicleId_SB, MessageTemplete message, String smsUrl) {

		String dealerName = userLogindealerCode;
		// List<Vehicle> vehicleInfo = call_int_repo.getAllvehList();
		// MessageTemplete
		// message=repo.getMessageTempleteOfDealerServiceBooked("Service
		// Booked",dealerName);
		// Service serviceData=call_int_repo.getLatestServiceData(vehicleId_SB);
		Service serviceData = getLatestServiceData(vehicleId_SB);

		String messageUrl = smsUrl;
		messageUrl += serviceData.getVehicle().getCustomer().getPreferredPhone().getPhoneNumber();
		messageUrl += "&mobile=";
		messageUrl += "&source=";
		messageUrl += "ATULMS";
		messageUrl += "&message=";
		messageUrl += message.getMessageHeader();
		messageUrl += message.getMessageBody();
		messageUrl += "&senderid=ATULMS&accusage=1";
		messageUrl = messageUrl.replace("(Model)", serviceData.getVehicle().getModel());
		messageUrl = messageUrl.replace("(RegistrationNo.)", serviceData.getVehicle().getVehicleRegNo());
		// getting vehicle id from service table
		// from the same service table get nextservicedate
		messageUrl = messageUrl.replace("(Date)", serviceData.getNextServiceDate().toString());
		messageUrl = messageUrl.replace("(customername)", serviceData.getVehicle().getCustomer().getCustomerName());
		messageUrl = messageUrl.replace("(dealername)", dealerName);
		// messageUrl =
		// messageUrl.repalce("(city)",serviceData.getVehicle().getWorkshop().getLocation_cityId().toString());

		messageUrl = messageUrl.replace("+", "%2B");
		messageUrl = messageUrl.replace(" ", "%20");
		logger.info("messageUrl : " + messageUrl);

		try {
			URL url = new URL(messageUrl);
			java.net.HttpURLConnection urlConnection = (java.net.HttpURLConnection) url.openConnection();
			try {
				BufferedReader bufferedReader = new BufferedReader(
						new InputStreamReader(urlConnection.getInputStream()));
				StringBuilder stringBuilder = new StringBuilder();
				String line;
				while ((line = bufferedReader.readLine()) != null) {
					stringBuilder.append(line).append("\n");
				}

				String data = stringBuilder.toString();
				String responsedata = data.substring(0, 4);
				logger.info("Final Response Of Data : " + data + " responsedata : " + responsedata);
				if (responsedata.equals("1701")) {
					updateMessageSentStatus(serviceData.getId(), "Yes");
					bufferedReader.close();
				}

			} finally {
				urlConnection.disconnect();
			}
		} catch (Exception e) {

			logger.info("Exception Raised");
		}

	}

	private Service getLatestServiceData(long vehicle_id) {
		logger.info("vehicle_id tui listr" + vehicle_id);
		List<Service> service_list = source.services(em).where(u -> u.getVehicle().getVehicle_id() == vehicle_id)
				.toList();
		List<Service> finalData = new ArrayList<>();
		if (service_list.size() != 0) {

			Date tmp = null;
			int i = 0;
			if (service_list.size() != 0) {
				for (Service uniqueNo_list : service_list) {
					if (i == 0) {
						tmp = uniqueNo_list.getLastServiceDate();
						i++;
					} else {
						if (tmp.before(uniqueNo_list.getLastServiceDate())) {
							tmp = uniqueNo_list.getLastServiceDate();
							i++;
						}
					}
				}
				logger.info("getting latest disposition :" + tmp);
				finalData.add(service_list.get(i - 1));
			}

			return finalData.get(0);
		} else {
			Service data = new Service();
			return data;
		}

	}

	private void updateMessageSentStatus(long id, String message) {

		Service scall = em.find(Service.class, id);
		scall.setMessageSentStatus(message);
		em.merge(scall);
	}

	@Override
	public void SMSTriggerBulkComplaints(String userLogindealerCode, String complaintNumber, String customerName,
			String vehicleRegNo, String customerPhone, MessageTemplete message, String smsUrl) {
		String dealerName = userLogindealerCode;
		// List<Vehicle> vehicleInfo = call_int_repo.getAllvehList();
		// MessageTemplete
		// message=repo.getMessageTempleteOfDealerServiceBooked("Service
		// Booked",dealerName);
		// Service serviceData=call_int_repo.getLatestServiceData(vehicleId_SB);
		// Service serviceData=getLatestServiceData(vehicleId_SB);

		String messageUrl = smsUrl;
		messageUrl += "&mobile=";
		messageUrl += customerPhone;
		messageUrl += "&source=";
		messageUrl += "ATULMS";
		messageUrl += "&message=";
		messageUrl += message.getMessageHeader();
		messageUrl += message.getMessageBody();
		messageUrl += "&senderid=ATULMS&accusage=1";

		logger.info("complaint number :: " + complaintNumber);
		logger.info("message header:" + message.getMessageHeader());

		messageUrl = messageUrl.replace("(RegistrationNo.)", vehicleRegNo);
		// getting vehicle id from service table
		// from the same service table get nextservicedate
		messageUrl = messageUrl.replace("(customername)", customerName);
		messageUrl = messageUrl.replace("(dealername)", dealerName);
		messageUrl = messageUrl.replace("(complaintnum)", complaintNumber);

		// messageUrl =
		// messageUrl.repalce("(city)",serviceData.getVehicle().getWorkshop().getLocation_cityId().toString());
		messageUrl = messageUrl.replace("+", "%2B");
		messageUrl = messageUrl.replace(" ", "%20");
		logger.info("messageUrl : " + messageUrl);

		try {
			URL url = new URL(messageUrl);
			java.net.HttpURLConnection urlConnection = (java.net.HttpURLConnection) url.openConnection();
			try {
				BufferedReader bufferedReader = new BufferedReader(
						new InputStreamReader(urlConnection.getInputStream()));
				StringBuilder stringBuilder = new StringBuilder();
				String line;
				while ((line = bufferedReader.readLine()) != null) {
					stringBuilder.append(line).append("\n");
				}

				String data = stringBuilder.toString();
				String responsedata = data.substring(0, 4);
				logger.info("Final Response Of Data : " + data + " responsedata : " + responsedata);
				if (responsedata.equals("1701")) {
					// updateMessageSentStatus(serviceData.getId(),"Yes");
					bufferedReader.close();
				}

			} finally {
				urlConnection.disconnect();
			}
		} catch (Exception e) {

			logger.info("Exception Raised");
		}
	}

	@Override
	public boolean sendCustomSMS(long schcallid, String message, String type, WyzUser userdata) {

		logger.info(" type is: " + type);
		logger.info("inside send custom sms while non contact");
		long smstemplateId = source.smsTemplates(em).where(u -> u.getSmsType().equals(type)).select(u -> u.getSmsId())
				.getOnlyValue();

		Map<String, String> dataList = new HashMap<String, String>();

		Vehicle dispoVeh = source.vehicle(em).where(u -> u.getVehicle_id() == schcallid).getOnlyValue();

		// logger.info(" schcallid dispo:
		// "+dispoVeh.getCustomer().getPreferredPhone().getPhoneNumber());
		Hibernate.initialize(dispoVeh.getCustomer().getPreferredPhone());

		String phoneNumber = dispoVeh.getCustomer().getPreferredPhone().getPhoneNumber();
		dataList.put(phoneNumber, message);

		logger.info("Phone number for SMS to be sent:" + phoneNumber);
		return SendBulkSMSToUsers(userdata.getUserName(), dataList, smstemplateId);

	}

	@Override
	public List<SMSInteraction> getAllSMSInteracOfCustomer(long customerId) {
		// TODO Auto-generated method stub
		return source.smsInteractions(em).where(u -> u.getCustomer() != null)
				.where(u -> u.getCustomer().getId() == customerId).toList();
	}

	@Override
	public boolean sendComplaintSMS(String customerPhone, Vehicle veh_new, Customer cut_new, String message,
			String type, WyzUser userData) {
		// TODO Auto-generated method stub

		long smstemplateId = source.smsTemplates(em).where(u -> u.getSmsType().equals(type)).select(u -> u.getSmsId())
				.getOnlyValue();
		logger.info("smstemplateId is:" + smstemplateId);
		Map<String, String> dataList = new HashMap<String, String>();
		dataList.put(customerPhone, message);

		String phoneNumber = customerPhone;

		logger.info("Phone number for SMS to be sent by complaint :" + phoneNumber);

		return SendBulkSMSToUsers(userData.getUserName(), dataList, smstemplateId);
	}

	private String removeSpace(String s) {
		String withoutspaces = "";
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) != ' ')
				withoutspaces += s.charAt(i);

		}
		return withoutspaces;

	}

	@Override
	public void sendBulkSMSAtOnce(List<Long> vehList, String smsTemplete, long userId, String tenentCode, String type) {
		// TODO Auto-generated method stub

		long countOfVehi = vehList.size();

		String query = "USE " + tenentCode + ";";
		Session hibernateSession = em.unwrap(Session.class);
		hibernateSession.doWork(new org.hibernate.jdbc.Work() {

			@Override
			public void execute(Connection connection) {
				Statement smt;
				try {
					smt = connection.createStatement();
					Boolean resultOfExecution = smt.execute(query);

					logger.info(" countOfVehi in sendBulkSMSAtOnce : " + countOfVehi);

					if (countOfVehi != 0) {

						for (long vehiId : vehList) {

							com.typesafe.config.Config configuration = com.typesafe.config.ConfigFactory.load();
							String smsUrl = configuration.getString("app.smsUrl");

							logger.info(" vehiId : " + vehiId);

							Vehicle veh = em.find(Vehicle.class, vehiId);
							WyzUser user = em.find(WyzUser.class, userId);

							Hibernate.initialize(veh.getCustomer().getPreferredPhone());

							String phoneNumber = veh.getCustomer().getPreferredPhone().getPhoneNumber();

							logger.info("Phone number for SMS to be sent:" + phoneNumber);

							URIBuilder uribuilder;

							try {
								uribuilder = new URIBuilder(smsUrl);
								uribuilder.addParameter("mobiles", phoneNumber);
								uribuilder.addParameter("sms", smsTemplete);
								uribuilder.addParameter("senderid", "SAMTAH");
								// uribuilder.addParameter("accusage", "1");

								/*
								 * uribuilder.addParameter("SMSText",
								 * smsTemplete);
								 * uribuilder.addParameter("type","longsms");
								 * uribuilder.addParameter("GSM",
								 * "91"+phoneNumber);
								 */

								logger.info(uribuilder.toString());
								URI uri = uribuilder.build();

								RestTemplate restTemplate = new RestTemplate();

								ResponseEntity<String> response = restTemplate.getForEntity(uri, String.class);

								if (response.getStatusCode() == HttpStatus.OK) {
									String responseBody = response.getBody();
									logger.info("Response Body:" + responseBody);

									// Insert into SMS interaction table
									String pattern = "dd/MM/yyyy";
									String pattern1 = "HH:mm:ss";

									// logger.info("time_zone :"+time_zone);
									java.util.TimeZone tz = java.util.TimeZone.getTimeZone("IST");
									Calendar c = Calendar.getInstance(tz);
									Date now = c.getTime();

									SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
									SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat(pattern1);

									simpleDateFormat.setTimeZone(tz);
									simpleDateFormat1.setTimeZone(tz);

									String date = simpleDateFormat.format(now);
									String time = simpleDateFormat1.format(now);

									SMSInteraction smsInteraction = new SMSInteraction();
									smsInteraction.setCustomer(veh.getCustomer());
									smsInteraction.setInteractionDateAndTime(now);
									smsInteraction.setInteractionDate(date);
									smsInteraction.setInteractionTime(time);
									smsInteraction.setInteractionType(type);
									smsInteraction.setResponseFromGateway(responseBody);
									smsInteraction.setSmsMessage(smsTemplete);
									smsInteraction.setSmsStatus(true);
									smsInteraction.setVehicle(veh);
									smsInteraction.setWyzUser(user);

									em.merge(smsInteraction);

								} else {

									logger.info("Some problem in the sms gateway response");
									logger.info("Response from gateway:" + response.getBody());

								}

							} catch (URISyntaxException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();

							}

						}

					}
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					logger.info("SQL Exceptrion");
				}

			}
		});

	}

	@Override
	public boolean SendBulkSMSToUsers(String userLoginName, Map<String, String> dataList, long smstemplateId) {

		// com.typesafe.config.Config configuration =
		// com.typesafe.config.ConfigFactory.load();
		// String smsUrl = configuration.getString("app.smsUrl");
		URIBuilder uribuilder;

		SMSTemplate smsTemplateData = source.smsTemplates(em).where(u -> u.getSmsId() == smstemplateId).getOnlyValue();
		String smsUrl = smsTemplateData.getSmsAPI();
		String dealerName = smsTemplateData.getDealerName();
		String Dealer = smsTemplateData.getDealer();
		logger.info("sms api:" + smsUrl);

		for (Map.Entry<String, String> entry : dataList.entrySet()) {
			System.out.println("Key for error is = " + entry.getKey() + ", Value = " + entry.getValue());
			String phonenum = entry.getKey();
			String messageTemplate = entry.getValue();

			WyzUser wyzUserName = source.wyzUsers(em).where(u -> u.getUserName().equals(userLoginName)).getOnlyValue();

			Customer customeris = new Customer();
			Vehicle vehId = new Vehicle();

			long customerCount = source.phones(em).where(u -> u.getPhoneNumber().equals(phonenum))
					.select(u -> u.getCustomer()).count();

			logger.info(" customerCount for phone number : " + customerCount);
			// customer id is in the vehicle
			if (customerCount > 0) {
				customeris = source.phones(em).where(u -> u.getPhoneNumber().equals(phonenum))
						.select(u -> u.getCustomer()).toList().get(0);
				long custId = customeris.getId();
				vehId = source.vehicle(em).where(u -> u.getCustomer().getId() == custId).toList().get(0);
			}

			String message;

			try {
				uribuilder = new URIBuilder(smsUrl);
				List<SMSParameters> smsParameters = source.smsParameters(em).toList();
				
				SMSParameters smsSentParam = smsParameters.get(0);
				String sentSuccess = smsSentParam.getSucessStatus();
				logger.info("sentSuccess:"+sentSuccess);

				
				for (SMSParameters listParams : smsParameters) {
					String phone = listParams.getPhone();
					String msg = listParams.getMessage();
					String senderid = listParams.getSenderid();

					uribuilder.addParameter(phone, phonenum);
					uribuilder.addParameter(msg, messageTemplate);
					uribuilder.addParameter(senderid, dealerName);
					uribuilder.addParameter("accusage", "1");

				}

				URI uri = uribuilder.build();

				logger.info("sms url after building:" + uri);
				RestTemplate restTemplate = new RestTemplate();

				ResponseEntity<String> response = restTemplate.getForEntity(uri, String.class);

				if (response.getStatusCode() == HttpStatus.OK) {
					String responseBody = response.getBody();
					logger.info("Response Body:" + responseBody);

					// Insert into SMS interaction table
					String pattern = "dd/MM/yyyy";
					String pattern1 = "HH:mm:ss";

					// logger.info("time_zone :"+time_zone);
					java.util.TimeZone tz = java.util.TimeZone.getTimeZone("IST");
					Calendar c = Calendar.getInstance(tz);
					Date now = c.getTime();

					SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
					SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat(pattern1);

					simpleDateFormat.setTimeZone(tz);
					simpleDateFormat1.setTimeZone(tz);

					String date = simpleDateFormat.format(now);
					String time = simpleDateFormat1.format(now);

					SMSInteraction smsInteraction = new SMSInteraction();
					smsInteraction.setCustomer(customeris);
					smsInteraction.setVehicle(vehId);
					smsInteraction.setMobileNumber(phonenum);
					smsInteraction.setInteractionDateAndTime(now);
					smsInteraction.setInteractionDate(date);
					smsInteraction.setInteractionTime(time);
					smsInteraction.setResponseFromGateway(responseBody);
					smsInteraction.setSmsMessage(messageTemplate);
					smsInteraction.setWyzUser(wyzUserName);
					smsInteraction.setSmsStatus(true);
				//	logger.info("to update status " + responseBody);

					if (responseBody.contains(sentSuccess)) {
						smsInteraction.setSmsStatus(true);
					} else {

						smsInteraction.setSmsStatus(false);
					}

					em.merge(smsInteraction);

					return true;
				} else {

					return false;
				}

			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

				return false;
			}

		}
		return true;

	}

	@Override
	public boolean sendDriverSMS(Driver driver_link, String message, String type, WyzUser userdata) {
		logger.info(" type is: " + type);

		long smstemplateId = source.smsTemplates(em).where(u -> u.getSmsType().equals(type)).select(u -> u.getSmsId())
				.getOnlyValue();

		Map<String, String> dataList = new HashMap<String, String>();

		String phoneNumber = driver_link.getDriverPhoneNum();
		dataList.put(phoneNumber, message);

		logger.info("Phone number for SMS to be sent:" + phoneNumber);
		return SendBulkSMSToUsers(userdata.getUserName(), dataList, smstemplateId);

	}

}
