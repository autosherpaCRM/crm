/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import controllers.webmodels.AssignedIntreactionNoCall;
import controllers.webmodels.WyzUserList;
import configs.JinqSource;
import models.AssignedInteraction;
import models.DeletedStatusReport;
import models.Driver;
import models.InsuranceAgent;
import models.ListingForm;
import models.Location;
import models.Role;
import models.ServiceAdvisor;
import models.TaggingUsers;
import models.Tenant;
import models.UnAvailability;
import models.VehicleSummaryCube;
import models.Workshop;
import models.WyzUser;
import play.Logger.ALogger;

/**
 *
 * @author W-1004
 */
@Repository("SuperAdminRepository")
@Transactional
public class SuperAdminRepositoryImpl implements SuperAdminRepository {

	ALogger logger = play.Logger.of("application");

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private JinqSource source;

	@Autowired
	private CallInteractionsRepository call_int_repo;

	public void updateUserAvailability(String userId, String status) throws ParseException {
		long user_Id = Long.parseLong(userId);
		boolean status1 = Boolean.parseBoolean(status);
		logger.info("availability repo" + user_Id + " " + status1);
		WyzUser oldWyzUser = source.wyzUsers(em).where(u -> u.getId() == user_Id).getOnlyValue();
		oldWyzUser.setUnAvailable(!status1);
		em.merge(oldWyzUser);

		if (status1 == true) {
			UnAvailability avail = new UnAvailability();
			java.util.Date date = new java.util.Date();
			int year = Calendar.getInstance().get(Calendar.YEAR);
			int month = Calendar.getInstance().get(Calendar.MONTH);
			avail.setFromDate(date);
			avail.setToDate(date);
			avail.setWyzUser(oldWyzUser);
			avail.setYear(Integer.toString(year));
			avail.setMonthNumber(month + 1);
			em.persist(avail);
		} else {
			UnAvailability avail = source.unAvailabilitys(em).where(u -> u.getWyzUser().getId() == user_Id)
					.getOnlyValue();
			Date dateBefore = new Date();
			Calendar c = Calendar.getInstance();
			c.setTime(dateBefore);
			c.add(Calendar.DATE, -1);
			dateBefore = c.getTime();
			String modifiedDate = new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime());
			Date d1 = new SimpleDateFormat("dd-MM-yyyy").parse(modifiedDate);
			if (d1.equals(avail.getFromDate())) {
				long id1 = avail.getId();
				logger.info("id1 : " + id1);
				Query q = em.createQuery("DELETE FROM UnAvailability WHERE id = " + id1 + "");
				q.executeUpdate();
			} else {
				avail.setToDate(dateBefore);
				em.merge(avail);
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<WyzUserList> getWyzUserListData(long fromIndex, long toIndex, String searchPattern) {
		javax.persistence.Query query = em.createNativeQuery("CALL wyzUserData(:start_with, :length, :searchValue)",
				"WyzUserProcedure");

		query.setParameter("start_with", fromIndex);
		query.setParameter("length", toIndex);
		query.setParameter("searchValue", searchPattern);
		return query.getResultList();
	}

	public void updateUserInfo(String user_Id, String phoneNumber, String phoneIMEIno) {
		long userId = Long.parseLong(user_Id);
		// logger.info("userId is :"+userId);

		WyzUser oldWyzUser = source.wyzUsers(em).where(u -> u.getId() == userId).getOnlyValue();
		oldWyzUser.setPhoneNumber(phoneNumber);
		oldWyzUser.setPhoneIMEINo(phoneIMEIno);
		em.merge(oldWyzUser);
		// logger.info("oldWyzUser.getRole() :" + oldWyzUser.getRole());

		if (oldWyzUser.getRole().equals("Service Advisor")) {
			ServiceAdvisor oldServiceAdvisor = source.serviceadvisor(em).where(u -> u.getWyzUser().getId() == userId)
					.getOnlyValue();
			oldServiceAdvisor.setAdvisorNumber(phoneNumber);
			em.merge(oldServiceAdvisor);
		} else if (oldWyzUser.getRole().equals("Driver")) {
			Driver oldDriver = source.driver(em).where(u -> u.getWyzUser().getId() == userId).getOnlyValue();
			oldDriver.setDriverAltPhoneNum(phoneNumber);
			oldDriver.setDriverPhoneNum(phoneNumber);
			em.merge(oldDriver);
		} else if (oldWyzUser.getRole().equals("Insurance Agent")) {
			InsuranceAgent oldInsuranceAgent = source.insuranceAgents(em).where(u -> u.getWyzUser().getId() == userId)
					.getOnlyValue();
			oldInsuranceAgent.setInsuranceAgentNumber(phoneNumber);
			em.persist(oldInsuranceAgent);
		} else if (!oldWyzUser.getRole().equals("CRE") && !oldWyzUser.getRole().equals("CREManager")) {
			TaggingUsers oldTaggingUser = source.taggingUsers(em).where(u -> u.getWyzUser().getId() == userId)
					.getOnlyValue();
			oldTaggingUser.setPhoneNumber(phoneNumber);
			em.merge(oldTaggingUser);
		}
		/*
		 * Config configuration = ConfigFactory.load(); String
		 * defaultDataBaseName = configuration.getString("app.defaultdatabase");
		 * logger.info("defaultDataBaseName : "+defaultDataBaseName);
		 * 
		 * String query="USE "+defaultDataBaseName+";"; Session hibernateSession
		 * = em.unwrap(Session.class); hibernateSession.doWork(new
		 * org.hibernate.jdbc.Work() {
		 * 
		 * @Override public void execute(Connection connection) throws
		 * SQLException { Statement smt=connection.createStatement(); Boolean
		 * resultOfExecution=smt.execute(query);
		 * 
		 * Tenant tenant=source.tenant(em).where(u
		 * ->u.getTenantCode().equals(userLogindealerCode)).getOnlyValue();
		 * WyzUser newWyzUserInmain = source.wyzUsers(em).where(u -> u.getId()
		 * == userId).getOnlyValue();
		 * newWyzUserInmain.setPhoneNumber(phoneNumber);
		 * newWyzUserInmain.setPhoneIMEINo(phoneIMEIno);
		 * em.persist(newWyzUserInmain); }
		 * 
		 * });
		 */
	}

	@Override
	public void addUsersBySuperAdmin(WyzUser wyzUser, Location location, Workshop workshop, Role role,
			String userLoginName, String userLogindealerCode, ListingForm listingForms) {
		// TODO Auto-generated method stub
		long user_code = listingForms.getUser_code();
		String locName = location.getName();
		String firstName = wyzUser.getFirstName();
		String lastName = wyzUser.getLastName();
		String userName = wyzUser.getUserName();
		long workshopId = workshop.getId();
		String workshopName = workshop.getWorkshopName();
		String pincode = workshop.getPincode();
		String phonenum = wyzUser.getPhoneNumber();
		String phoneIMEINo = wyzUser.getPhoneIMEINo();
		String password = wyzUser.getPassword();
		String roleName = wyzUser.getRole();
		String cremanager = wyzUser.getCreManager();
		String emailId = wyzUser.getEmailId();

		logger.info("role name:" + roleName);
		logger.info("location name:" + locName);
		logger.info("workshop name:" + workshopId);
		logger.info("dealer code is:" + userLogindealerCode);
		logger.info("user code is: " + user_code);

		WyzUser newWyzUser = new WyzUser();
		long loccountId = source.locations(em).where(u -> u.getName().equals(locName)).count();

		if (loccountId > 0) {
			Location locdata = source.locations(em).where(u -> u.getName().equals(locName)).getOnlyValue();
			long workshopcount = source.workshop(em).where(u -> u.getId() == workshopId).count();
			Workshop workshopdata = source.workshop(em).where(u -> u.getId() == workshopId).getOnlyValue();
			Tenant tenant = source.tenant(em).where(u -> u.getTenantCode().equals(userLogindealerCode)).getOnlyValue();

			logger.info("workshopcount is: " + workshopcount + "locdata : " + locdata.getName());

			newWyzUser.setFirstName(firstName);
			newWyzUser.setLastName(lastName);
			newWyzUser.setEmailId(emailId);
			newWyzUser.setPhoneNumber(phonenum);
			newWyzUser.setPhoneIMEINo(phoneIMEINo);
			newWyzUser.setUserName(userName);
			newWyzUser.setRole(roleName);
			newWyzUser.setDealerCode(userLogindealerCode);
			newWyzUser.setDealerId(userLogindealerCode);
			newWyzUser.setDealerName(userLogindealerCode);
			newWyzUser.setPassword(password);
			newWyzUser.setLocation(locdata);
			newWyzUser.setCreManager(cremanager);
			newWyzUser.setDealer(null);
			newWyzUser.setTenant(tenant);
			newWyzUser.setUnAvailable(false);

			if (workshopcount > 0) {

				newWyzUser.setWorkshop(workshopdata);
				em.persist(newWyzUser);

			} else {

				em.persist(newWyzUser);

			}
			if (roleName.equals("Service Advisor")) {
				ServiceAdvisor newServiceAdvisor = new ServiceAdvisor();
				newServiceAdvisor.setAdvisorName(firstName);
				newServiceAdvisor.setWorkshop(workshopdata);
				newServiceAdvisor.setWyzUser(newWyzUser);
				newServiceAdvisor.setIsActive(true);
				newServiceAdvisor.setCapacityPerDay(20);
				newServiceAdvisor.setAdvisorNumber(phonenum);
				newServiceAdvisor.priority = 1;
				em.persist(newServiceAdvisor);
			} else if (roleName.equals("Driver")) {
				Driver newDriver = new Driver();
				logger.info("it is Driver");
				newDriver.setDriverName(firstName);
				newDriver.setWyzUser(newWyzUser);
				newDriver.setDriverAltPhoneNum(phonenum);
				newDriver.setDriverPhoneNum(phonenum);
				newDriver.setWorkshop(workshopdata);
				em.persist(newDriver);

			} else if (roleName.equals("Insurance Agent")) {
				InsuranceAgent newInsuranceAgent = new InsuranceAgent();
				newInsuranceAgent.setInsuranceAgentName(firstName);
				newInsuranceAgent.setInsuranceAgentNumber(phonenum);
				newInsuranceAgent.setWyzUser(newWyzUser);
				newInsuranceAgent.setIsActive(true);
				newInsuranceAgent.setPriority(1);
				newInsuranceAgent.setCapacityPerDay(60);
				em.persist(newInsuranceAgent);
			} else if (!roleName.equals("CRE") && !roleName.equals("CREManager")) {
				TaggingUsers newTaggingUser = new TaggingUsers();
				newTaggingUser.setName(firstName);
				newTaggingUser.setPhoneNumber(phonenum);
				newTaggingUser.setUpsellType(roleName);
				newTaggingUser.setLocation(locdata);
				newTaggingUser.setWyzUser(newWyzUser);
				em.persist(newTaggingUser);
			}

		}

		else {

			Tenant tenant = source.tenant(em).where(u -> u.getTenantCode().equals(userLogindealerCode)).getOnlyValue();
			newWyzUser.setFirstName(firstName);
			newWyzUser.setLastName(lastName);
			newWyzUser.setUserName(userName);
			newWyzUser.setPhoneNumber(phonenum);
			newWyzUser.setPhoneIMEINo(phoneIMEINo);
			newWyzUser.setPassword(password);
			newWyzUser.setDealerCode(userLogindealerCode);
			newWyzUser.setDealerId(userLogindealerCode);
			newWyzUser.setDealerName(userLogindealerCode);
			newWyzUser.setDealer(null);
			newWyzUser.setTenant(tenant);
			newWyzUser.setCreManager(cremanager);
			newWyzUser.setRole(roleName);
			newWyzUser.setEmailId(emailId);
			em.persist(newWyzUser);

			if (roleName.equals("Service Advisor")) {
				ServiceAdvisor newServiceAdvisor = new ServiceAdvisor();
				newServiceAdvisor.setAdvisorName(firstName);
				newServiceAdvisor.setWyzUser(newWyzUser);
				newServiceAdvisor.setIsActive(true);
				newServiceAdvisor.setCapacityPerDay(20);
				newServiceAdvisor.setAdvisorNumber(phonenum);
				newServiceAdvisor.priority = 1;
				em.persist(newServiceAdvisor);

			}

			if (roleName.equals("Driver")) {
				Driver newDriver = new Driver();
				logger.info("it is Driver");
				newDriver.setDriverName(firstName);
				newDriver.setWyzUser(newWyzUser);
				newDriver.setDriverAltPhoneNum(phonenum);
				newDriver.setDriverPhoneNum(phonenum);
				em.persist(newDriver);

			} else if (roleName.equals("Insurance Agent")) {
				InsuranceAgent newInsuranceAgent = new InsuranceAgent();
				newInsuranceAgent.setInsuranceAgentName(firstName);
				newInsuranceAgent.setInsuranceAgentNumber(phonenum);
				newInsuranceAgent.setIsActive(true);
				newInsuranceAgent.setPriority(1);
				newInsuranceAgent.setCapacityPerDay(60);
				newInsuranceAgent.setWyzUser(newWyzUser);
				em.persist(newInsuranceAgent);
			} else if (!roleName.equals("CRE") && !roleName.equals("CREManager")) {
				TaggingUsers newTaggingUser = new TaggingUsers();
				newTaggingUser.setName(firstName);
				newTaggingUser.setPhoneNumber(phonenum);
				newTaggingUser.setUpsellType(roleName);
				newTaggingUser.setWyzUser(newWyzUser);
				em.persist(newTaggingUser);
			}
		}
		assignLocationToUser(locName, userName);
		assignRoleToUser(userName, roleName);
		assignWorkshopToUser(userName, workshopId);

		Config configuration = ConfigFactory.load();
		String defaultDataBaseName = configuration.getString("app.defaultdatabase");
		logger.info("defaultDataBaseName : " + defaultDataBaseName);

		String query = "USE " + defaultDataBaseName + ";";
		Session hibernateSession = em.unwrap(Session.class);
		hibernateSession.doWork(new org.hibernate.jdbc.Work() {

			@Override
			public void execute(Connection connection) throws SQLException {
				Statement smt = connection.createStatement();
				Boolean resultOfExecution = smt.execute(query);

				Tenant tenant = source.tenant(em).where(u -> u.getTenantCode().equals(userLogindealerCode))
						.getOnlyValue();
				WyzUser newWyzUserInmain = new WyzUser();
				newWyzUserInmain.setFirstName(firstName);
				newWyzUserInmain.setLastName(lastName);
				newWyzUserInmain.setUserName(userName);
				newWyzUserInmain.setPhoneNumber(phonenum);
				newWyzUserInmain.setPhoneIMEINo(phoneIMEINo);
				newWyzUserInmain.setPassword(password);
				newWyzUserInmain.setDealerCode(userLogindealerCode);
				newWyzUserInmain.setDealerId(userLogindealerCode);
				newWyzUserInmain.setDealerName(userLogindealerCode);
				newWyzUserInmain.setLocation(null);
				newWyzUserInmain.setWorkshop(null);
				newWyzUserInmain.setTenant(tenant);
				newWyzUserInmain.setDealer(null);
				newWyzUserInmain.setCreManager(cremanager);
				newWyzUserInmain.setRole(roleName);
				newWyzUserInmain.setEmailId(emailId);
				em.persist(newWyzUserInmain);
			}

		});

	}

	private void assignRoleToUser(String Uname, String roleName) {
		// TODO Auto-generated method stub
		logger.info("role name is " + roleName);
		WyzUser user = source.wyzUsers(em).where(u -> u.getUserName().equals(Uname)).getOnlyValue();
		List<Role> role = source.roles(em).where(u -> u.getRole().equals(roleName)).toList();
		user.setRoles(role);
	}

	public void assignLocationToUser(String locName, String Uname) {
		WyzUser user = source.wyzUsers(em).where(u -> u.getUserName().equals(Uname)).getOnlyValue();
		List<Location> loc = source.locations(em).where(u -> u.getName().equals(locName)).toList();
		user.setLocationList(loc);
	}

	public void assignWorkshopToUser(String Uname, long workshopId) {
		logger.info("workshop is " + workshopId);
		WyzUser user = source.wyzUsers(em).where(u -> u.getUserName().equals(Uname)).getOnlyValue();

		logger.info("username is " + user.getUserName());

		List<Workshop> work = source.workshop(em).where(u -> u.getId() == workshopId).toList();
		user.setWorkshopList(work);
		/*
		 * WyzUser user=source.wyzUsers(em).where(u -> u.getId() ==
		 * 51).getOnlyValue(); List<Workshop>
		 * worklist=source.workshop(em).where(u -> u.getId() == 1).toList();
		 * user.setWorkshopList(worklist);
		 */
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AssignedIntreactionNoCall> getAssignedInteractionListData(long wyzuserId, String location1,
			String moduleName, String campaignName, Date fromDate, Date toDate) {
		String fromDate1 = new SimpleDateFormat("yyyy-MM-dd").format(fromDate);
		String toDate1 = new SimpleDateFormat("yyyy-MM-dd").format(toDate);
		long locId = (long) source.locations(em).where(u -> u.getName().equals(location1)).select(u -> u.getCityId())
				.getOnlyValue();
		logger.info(
				"" + wyzuserId + " " + locId + " " + moduleName + " " + campaignName + " " + fromDate1 + " " + toDate1);
		javax.persistence.Query query = em.createNativeQuery(
				"CALL cre_assignment_selection_within_date_range(:wyzuserId, :locId, :moduleName, :campaignName, :fromDate, :toDate)",
				"AssignedIntreactionNoCallProcedure");
		query.setParameter("locId", locId);
		query.setParameter("wyzuserId", wyzuserId);
		query.setParameter("locId", locId);
		query.setParameter("moduleName", moduleName);
		query.setParameter("campaignName", campaignName);
		query.setParameter("fromDate", fromDate1);
		query.setParameter("toDate", toDate1);

		return query.getResultList();
	}

	@Override
	public List<String> getCampaignByType(String typeIs) {

		return source.campaigns(em).where(u -> u.getCampaignType().equals(typeIs)).select(u -> u.getCampaignName())
				.toList();
	}

	@Override
	public List<WyzUser> getExistingWyzUsers(String uname) {
		logger.info("user name is :" + uname);
		return source.wyzUsers(em).where(u -> u.getUserName().equals(uname)).toList();
	}

	@Override
	public void deleteAssignedNoCall(ListingForm listing_Form) throws Exception {
		List<Long> assignId = listing_Form.getAssignedNoCallId();
		String module = listing_Form.getModuleAssign();
		String modifiedDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
		if (module.equals("Campaign")) {
			for (int i = 0; i < assignId.size(); i++) {
				long id1 = assignId.get(i);
				logger.info("" + id1);
				AssignedInteraction assignInteraction = source.assignedInteractions(em).where(u -> u.getId() == id1)
						.getOnlyValue();

				logger.info("" + assignInteraction.getId() + " " + assignInteraction.getVehicle().getVehicle_id());
				boolean exists = false;
				try {
					DeletedStatusReport d1 = source.deletedStatusReports(em).where(u -> u.getAssigned_id() == id1)
							.getOnlyValue();
					exists = true;
				} catch (Exception ex) {
					exists = false;
				} finally {
					if (exists) {
						DeletedStatusReport delStatusReport = source.deletedStatusReports(em)
								.where(u -> u.getAssigned_id() == id1).getOnlyValue();
						delStatusReport.setDeleted_date(new SimpleDateFormat("yyyy-MM-dd").parse(modifiedDate));
						delStatusReport.setCalled_status("No");
						delStatusReport.setWhendeleted("Manually Deleted");
						em.merge(delStatusReport);
					} else {
						DeletedStatusReport delStatusReport1 = new DeletedStatusReport();
						delStatusReport1.setAssigned_id(assignInteraction.getId());
						delStatusReport1.setCalled_status("No");
						delStatusReport1.setCampaign_id(assignInteraction.getCampaign().getId());
						delStatusReport1.setCustomer_id(assignInteraction.getCampaign().getId());
						delStatusReport1.setDeleted_date(new SimpleDateFormat("yyyy-MM-dd").parse(modifiedDate));

						delStatusReport1.setUploaded_date(assignInteraction.getUplodedCurrentDate());
						delStatusReport1.setVehicle_id(assignInteraction.getVehicle().getVehicle_id());
						delStatusReport1.setWhendeleted("Manually Deleted");
						delStatusReport1.setWyzuser_id(assignInteraction.getWyzUser().getId());
						em.persist(delStatusReport1);

					}
					boolean exist = false;
					try {
						VehicleSummaryCube vehicleSummaryCube2 = source.vehicleSummaryCubes(em)
								.where(u -> u.getAssignedInteractionID() == id1).getOnlyValue();
						exist = true;
					} catch (Exception ex) {
						exist = false;
					} finally {
						if (exist) {
							VehicleSummaryCube vehicleSummaryCube = source.vehicleSummaryCubes(em)
									.where(u -> u.getAssignedInteractionID() == id1).getOnlyValue();
							vehicleSummaryCube.setCallStatus("No");
							vehicleSummaryCube
									.setDataRemovalDate(new SimpleDateFormat("yyyy-MM-dd").parse(modifiedDate));
							vehicleSummaryCube.setWhenRemoved("Manually Deleted");
							em.merge(vehicleSummaryCube);
						} else {
							VehicleSummaryCube vehicleSummaryCube1 = new VehicleSummaryCube();
							vehicleSummaryCube1.setAssignedCre(assignInteraction.getWyzUser().getUserName());
							vehicleSummaryCube1.setAssignedInteractionID(assignInteraction.getId());
							vehicleSummaryCube1.setChassisNo(assignInteraction.getVehicle().getChassisNo());
							vehicleSummaryCube1.setVehicleRegNo(assignInteraction.getVehicle().getVehicleRegNo());
							vehicleSummaryCube1.setPhoneNumber(
									assignInteraction.getCustomer().getPreferredPhone().getPhoneNumber());
							vehicleSummaryCube1.setCustomerName(assignInteraction.getCustomer().getCustomerName());
							vehicleSummaryCube1.setCampaign(assignInteraction.getCampaign().getCampaignName());
							vehicleSummaryCube1.setServiceDueDate(assignInteraction.getVehicle().getNextServicedate());
							vehicleSummaryCube1.setServiceDueType(assignInteraction.getVehicle().getNextServicetype());
							vehicleSummaryCube1.setUploadedDate(assignInteraction.getUplodedCurrentDate());
							vehicleSummaryCube1.setAssignedDate(assignInteraction.getUplodedCurrentDate());
							long v1 = assignInteraction.getVehicle().getVehicle_id();
							if (call_int_repo.getLatestServiceDataFiltering(v1) != null) {
								vehicleSummaryCube1.setRepairOrderNo(call_int_repo
										.getLatestServiceDataFiltering(assignInteraction.getVehicle().getVehicle_id())
										.getJobCardNumber());
								vehicleSummaryCube1.setRoDate(call_int_repo
										.getLatestServiceDataFiltering(assignInteraction.getVehicle().getVehicle_id())
										.getJobCardDate());
							}
							vehicleSummaryCube1.setVehicle_id(assignInteraction.getVehicle().getVehicle_id());
							vehicleSummaryCube1.setCustomer_id(assignInteraction.getCustomer().getId());
							vehicleSummaryCube1.setAssignedWyzuserID(assignInteraction.getWyzUser().getId());
							vehicleSummaryCube1.setAssignedInteractionID(assignInteraction.getId());
							vehicleSummaryCube1
									.setUploadVehicle_id(Long.parseLong(assignInteraction.getVehicle().getUpload_id()));
							vehicleSummaryCube1.setCallMade("No");
							vehicleSummaryCube1
									.setDataRemovalDate(new SimpleDateFormat("yyyy-MM-dd").parse(modifiedDate));
							vehicleSummaryCube1.setIsRemoval("Yes");
							vehicleSummaryCube1.setWhenRemoved("Manually Deleted");
							em.persist(vehicleSummaryCube1);
						}
						Query q = em.createQuery("DELETE FROM AssignedInteraction WHERE id = " + id1 + "");
						q.executeUpdate();
					}
				}
			}
		}
	}

	@Override
	public void addLocationdataBySuperAdmin(Location location, Workshop workshop, String userLoginName) {

		String locName = location.getName();
		String locCode = location.getLocCode();
		String pinCode = location.getPinCode();
		String stateName = location.getState();
		String workshopName = workshop.getWorkshopName();

		location.setName(locName);
		location.setLocCode(locCode);
		location.setPinCode(pinCode);
		location.setState(stateName);

		em.persist(location);

		workshop.setWorkshopName(workshopName);
		workshop.setLocation(location);

		em.persist(workshop);
	}

	@Override
	public void addUsersBySuperAdmin(WyzUser wyzUser, Location location, Workshop workshop, Role role,
			String userLoginName, String userLogindealerCode) {
		// TODO Auto-generated method stub

		String locName = location.getName();
		String firstName = wyzUser.getFirstName();
		String lastName = wyzUser.getLastName();
		String userName = wyzUser.getUserName();
		long workshopId = workshop.getId();
		String pincode = workshop.getPincode();
		String phonenum = wyzUser.getPhoneNumber();
		String password = wyzUser.getPassword();
		String roleName = wyzUser.getRole();

		logger.info("role name:" + roleName);
		logger.info("location name:" + locName);
		logger.info("workshop name:" + workshopId);
		logger.info("dealer code is:" + userLogindealerCode);

		WyzUser newWyzUser = new WyzUser();
		long loccountId = source.locations(em).where(u -> u.getName().equals(locName)).count();

		if (loccountId > 0) {
			Location locdata = source.locations(em).where(u -> u.getName().equals(locName)).getOnlyValue();
			long workshopcount = source.workshop(em).where(u -> u.getId() == workshopId).count();
			Workshop workshopdata = source.workshop(em).where(u -> u.getId() == workshopId).getOnlyValue();
			Tenant tenant = source.tenant(em).where(u -> u.getTenantCode().equals(userLogindealerCode)).getOnlyValue();

			logger.info("workshopcount : " + workshopcount + "locdata : " + locdata.getName());

			newWyzUser.setFirstName(firstName);
			newWyzUser.setLastName(lastName);
			newWyzUser.setPhoneNumber(phonenum);
			newWyzUser.setUserName(userName);
			newWyzUser.setDealerCode(userLogindealerCode);
			newWyzUser.setDealerId(userLogindealerCode);
			newWyzUser.setDealerName(userLogindealerCode);
			newWyzUser.setPassword(password);
			newWyzUser.setLocation(locdata);
			newWyzUser.setDealer(null);
			newWyzUser.setTenant(tenant);

			if (workshopcount > 0) {

				newWyzUser.setWorkshop(workshopdata);
				em.persist(newWyzUser);

			} else {

				em.persist(newWyzUser);

			}
			if (roleName.equals("SA")) {
				ServiceAdvisor newServiceAdvisor = new ServiceAdvisor();
				newServiceAdvisor.setAdvisorName(firstName);
				newServiceAdvisor.setWorkshop(workshopdata);
				newServiceAdvisor.setWyzUser(newWyzUser);
				em.persist(newServiceAdvisor);
			}

			if (roleName.equals("Driver")) {
				Driver newDriver = new Driver();
				logger.info("it is Driver");
				newDriver.setDriverName(firstName);
				newDriver.setWyzUser(newWyzUser);
				em.persist(newDriver);

			}

		} else {

			Tenant tenant = source.tenant(em).where(u -> u.getTenantCode().equals(userLogindealerCode)).getOnlyValue();
			newWyzUser.setFirstName(firstName);
			newWyzUser.setLastName(lastName);
			newWyzUser.setUserName(userName);
			newWyzUser.setPhoneNumber(phonenum);
			newWyzUser.setPassword(password);
			newWyzUser.setDealerCode(userLogindealerCode);
			newWyzUser.setDealerId(userLogindealerCode);
			newWyzUser.setDealerName(userLogindealerCode);
			newWyzUser.setDealer(null);
			newWyzUser.setTenant(tenant);
			em.persist(newWyzUser);

			if (roleName.equals("SA")) {
				ServiceAdvisor newServiceAdvisors = new ServiceAdvisor();
				newServiceAdvisors.setAdvisorName(firstName);
				newServiceAdvisors.setWyzUser(newWyzUser);
				em.persist(newServiceAdvisors);

			}

			if (roleName.equals("Driver")) {
				Driver newDrivers = new Driver();
				logger.info("it is Driver");
				newDrivers.setDriverName(firstName);
				newDrivers.setWyzUser(newWyzUser);
				em.persist(newDrivers);

			}
		}
		Config configuration = ConfigFactory.load();
		String defaultDataBaseName = configuration.getString("app.defaultdatabase");
		logger.info("defaultDataBaseName : " + defaultDataBaseName);

		String query = "USE " + defaultDataBaseName + ";";
		Session hibernateSession = em.unwrap(Session.class);
		hibernateSession.doWork(new org.hibernate.jdbc.Work() {

			@Override
			public void execute(Connection connection) throws SQLException {
				Statement smt = connection.createStatement();
				Boolean resultOfExecution = smt.execute(query);

				Tenant tenant = source.tenant(em).where(u -> u.getTenantCode().equals(userLogindealerCode))
						.getOnlyValue();
				WyzUser newWyzUserInmain = new WyzUser();
				newWyzUserInmain.setFirstName(firstName);
				newWyzUserInmain.setLastName(lastName);
				newWyzUserInmain.setUserName(userName);
				newWyzUserInmain.setPhoneNumber(phonenum);
				newWyzUserInmain.setPassword(password);
				newWyzUserInmain.setDealerCode(userLogindealerCode);
				newWyzUserInmain.setDealerId(userLogindealerCode);
				newWyzUserInmain.setDealerName(userLogindealerCode);
				newWyzUserInmain.setLocation(null);
				newWyzUserInmain.setWorkshop(null);
				newWyzUserInmain.setTenant(tenant);
				newWyzUserInmain.setDealer(null);
				em.persist(newWyzUserInmain);
			}

		});
	}

}