package repositories;

import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import models.SMSTemplate;
import models.AssignedInteraction;
import models.BookingDateTime;
import models.CallDispositionData;
import models.CallInteraction;
import models.CallSummary;
import models.Campaign;
import models.Customer;
import models.Driver;
import models.ListingForm;
import models.MessageTemplete;
import models.Phone;
import models.PickupDrop;
import models.Role;
import models.SRDisposition;
import models.Service;
import models.ServiceAdvisor;
import models.ServiceBooked;
import models.ServiceTypes;
import models.Vehicle;
import models.Workshop;
import models.WyzUser;
import models.Complaint;
import models.ComplaintSource;
import models.ComplaintTypes;
import models.SpecialOfferMaster;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import play.Logger.ALogger;
import configs.JinqSource;
import models.Location;
import models.TaggingUsers;
import models.UnAvailability;
import models.UpsellLead;
import models.WorkshopSummary;
import org.hibernate.Hibernate;

@Repository("callInfoRepository")
@Transactional
public class CallInfoRepositoryImpl implements CallInfoRepository {

	ALogger logger = play.Logger.of("application");

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private JinqSource source;

	@Autowired
	private SMSTriggerRespository smsTriggerRepo;

	@Override
	public CallInteraction getCallInteractionbyId(Long id, String dealerDB) {

		CallInteraction edi = em.find(CallInteraction.class, id);

		Hibernate.initialize(edi.getCustomer().getPhones());
		Hibernate.initialize(edi.getCustomer().getAddresses());
		Hibernate.initialize(edi.getCustomer().getPreferredAdress());
		Hibernate.initialize(edi.getCustomer().getPreferredEmail());
		Hibernate.initialize(edi.getCustomer().getPreferredPhone());
		Hibernate.initialize(edi.getCustomer().getSegment());
		Hibernate.initialize(edi.getCustomer().getVehicles());
		Hibernate.initialize(edi.getCustomer().getInsurances());
		Hibernate.initialize(edi.getCustomer().getOfficeAddressData());
		Hibernate.initialize(edi.getCustomer().getResidenceAddressData());
		Hibernate.initialize(edi.getCustomer().getPermanentAddressData());

		Hibernate.initialize(edi.getSrdisposition());
		Hibernate.initialize(edi.getServiceBooked());

		if (edi.getServiceBooked() != null) {
			Hibernate.initialize(edi.getServiceBooked().getWorkshop());
			Hibernate.initialize(edi.getServiceBooked().getServiceAdvisor());
		}
		Hibernate.initialize(edi.getVehicle());

		return edi;
	}

	// @Override
	// public List<CallInteraction> getCallLogsByCREManager(String
	// userName,String dealerDB) {
	//
	// List<WyzUser> userList = source.wyzUsers(em)
	// .where(u -> u.getCreManager().equals(userName)).toList();
	// logger.info("User List count:" + userList.size());
	//
	// ArrayList<String> names = new
	// ArrayList<String>();
	// for(WyzUser user : userList){
	// names.add(user.getUserName());
	// }
	// return source.callInfos(em).where(c->
	// names.contains(c.getAgentName())).toList();
	// }

	@Override
	public MessageTemplete getMessageTempleteOfDealerServiceBooked(String messagetype, String dealer) {

		List<MessageTemplete> message = source.messageTempletes(em).where(u -> u.getDealerCode().equals(dealer))
				.toList();
		MessageTemplete messageData = new MessageTemplete();
		for (MessageTemplete data : message) {

			if (data.getMessageType().equals(messagetype)) {

				messageData.setId(data.getId());
				messageData.setDealerCode(data.getDealerCode());
				messageData.setMessageBody(data.getMessageBody());
				messageData.setMessageHeader(data.getMessageHeader());
				messageData.setMessageType(data.getMessageType());

			}

		}

		return messageData;
	}

	@Override
	public List<CallInteraction> getServicBookedCalls(String username, String dealercode) {

		List<CallInteraction> serviceBookedcalls = source.callinteractions(em)
				.where(c -> c.wyzUser.getUserName().equals(username) && c.srdisposition.getCallDispositionData()
						.getDispositionId() == (c.srdisposition.getCallDispositionData().STATUS_BOOK_MY_SERVICE))
				.toList();
		logger.info("servicebooked size is :" + serviceBookedcalls.size());
		return serviceBookedcalls;

	}

	@Override
	public List<CallInteraction> getServiceNotRequiredCalls(String username, String dealercode) {
		List<CallInteraction> serviceNotRequiredcalls = source.callinteractions(em)
				.where(c -> c.wyzUser.getUserName().equals(username) && c.srdisposition.getCallDispositionData()
						.getDispositionId() == (c.srdisposition.getCallDispositionData().STATUS_SERVICE_NOT_REQUIRED))
				.toList();

		return serviceNotRequiredcalls;
	}

	@Override
	public List<CallInteraction> getNonContactsCalls(String username, String dealercode) {

		List<CallInteraction> nonContactscalls = source.callinteractions(em)
				.where(c -> c.wyzUser.getUserName().equals(username)).toList();

		logger.info("nxcvcv" + nonContactscalls.size());

		List<CallInteraction> finalDatanonContactsCall = new ArrayList<CallInteraction>();
		if (nonContactscalls.size() != 0) {

			for (CallInteraction addingData : nonContactscalls) {
				if (addingData.srdisposition != null) {

					if (addingData.srdisposition.getCallDispositionData()
							.getDispositionId() == (addingData.srdisposition
									.getCallDispositionData().STATUS_RINGING_NO_RESPONSE)) {

						finalDatanonContactsCall.add(addingData);

					} else if (addingData.srdisposition.getCallDispositionData()
							.getDispositionId() == (addingData.srdisposition
									.getCallDispositionData().STATUS_INVALID_NUMBER)) {

						finalDatanonContactsCall.add(addingData);

					} else if (addingData.srdisposition.getCallDispositionData()
							.getDispositionId() == (addingData.srdisposition.getCallDispositionData().STATUS_BUSY)) {

						finalDatanonContactsCall.add(addingData);

					} else if (addingData.srdisposition.getCallDispositionData()
							.getDispositionId() == (addingData.srdisposition.getCallDispositionData().STATUS_OTHERS)) {

						finalDatanonContactsCall.add(addingData);

					} else if (addingData.srdisposition.getCallDispositionData()
							.getDispositionId() == (addingData.srdisposition
									.getCallDispositionData().STATUS_SWITCHED_OFF_UNREACHABLE)) {

						finalDatanonContactsCall.add(addingData);

					} else {
						logger.info("no info" + addingData.srdisposition.getCallDispositionData().getDisposition());
					}
				}
			}

		}

		logger.info("dghjdfg" + finalDatanonContactsCall.size());

		return finalDatanonContactsCall;
	}

	@Override
	public List<CallInteraction> getCallHistoryByPhoneNumber(String phonenumber, String cre, String dealercode) {
		logger.info("phonenumber : " + phonenumber);
		List<CallInteraction> callHistoryOFNumber = source.callinteractions(em)
				.where(k -> k.getWyzUser().getUserName().equals(cre)).toList();

		logger.info("xbcvvb  : " + callHistoryOFNumber.size());
		return callHistoryOFNumber;
	}

	@Override
	public List<String> getCREUserNameofCREManager(String username, String dealercode) {

		List<String> availableUserList = new ArrayList<String>();
		List<WyzUser> userList = source.wyzUsers(em).where(k -> k.getCreManager().equals(username))
				.where(k -> k.getRole().equals("CRE") || k.getRole().equals("Service Advisor")).toList();
		if (userList.size() != 0) {

			for (WyzUser d : userList) {
				if (d.isUnAvailable() == false) {
					availableUserList.add(d.getUserName());
					// logger.info("users is :"+d.getUserName());
				}

			}

		}

		List<String> availableUsers = new ArrayList<String>();

		java.util.Date todayData = new java.sql.Date(System.currentTimeMillis());

		for (String userData : availableUserList) {

			WyzUser user = source.wyzUsers(em).where(u -> u.getUserName().equals(userData)).getOnlyValue();

			Hibernate.initialize(user.getUnAvailabilities());

			List<UnAvailability> unaviabiltylist = user.getUnAvailabilities();

			if (unaviabiltylist.size() != 0) {

				int i = 0;
				for (UnAvailability unavailable : unaviabiltylist) {

					if (unavailable.getFromDate().before(todayData) && unavailable.getToDate().after(todayData)) {

						i = i + 1;

					} else if (unavailable.getFromDate().toString().equals(todayData.toString())) {
						i = i + 1;

					} else if (unavailable.getToDate().toString().equals(todayData.toString())) {
						i = i + 1;

					}
				}
				if (i == 0) {
					availableUsers.add(user.getUserName());

				}

			} else {

				availableUsers.add(user.getUserName());

			}

		}
		return availableUsers;
	}

	@Override
	public long getUniqueIdForCallInitaiating() {

		Query q = em.createQuery("SELECT a.callinitiating_id FROM UniqueIdForCall a");
		List<Integer> data = q.getResultList();

		// logger.info("unique id to set in db is :" + data.get(0));
		int increamented = data.get(0) + 1;
		em.createQuery("update UniqueIdForCall set callinitiating_id =" + increamented + "  where id=1")
				.executeUpdate();

		return data.get(0);
	}

	@Override
	public long getUniqueIdForListInitaiating() {
		Query q = em.createQuery("SELECT a.callinitiating_id FROM UniqueIdForCall a");
		List<Integer> data = q.getResultList();

		logger.info("unique id to set in db is from list:" + data.get(0));

		return data.get(0);
	}

	public Customer getCustomerByInteractionId(long id) {
		List<Customer> data = source.customers(em).toList();

		for (Customer data1 : data) {

			logger.info("size is " + data1.getCallInteractions().size());

			logger.info("id is :" + data1.getCallInteractions().get(0).getId());

		}

		return data.get(0);
	}

	public CallInteraction getInteractionCallByCustomer(long id) {

		CallInteraction data = source.callinteractions(em).where(u -> u.getCustomer().getId() == id).toList().get(0);

		return data;
	}

	@Override
	public void addCallInteraction(CallInteraction call_interaction, ListingForm listData, SRDisposition sr_Disposition,
			String dealercode, CallDispositionData dispo_data) {
		Vehicle veh = em.find(Vehicle.class, listData.getVehicalId());
		Customer cus = em.find(Customer.class, listData.getCustomer_Id());
		WyzUser wyz = em.find(WyzUser.class, listData.getWyzUser_Id());

		long vehi_id = veh.getVehicle_id();
		long customer_id = cus.getId();

		String pattern = "dd/MM/yyyy";
		String pattern1 = "HH:mm:ss";

		// logger.info("time_zone :"+time_zone);
		java.util.TimeZone tz = java.util.TimeZone.getTimeZone("IST");
		Calendar c = Calendar.getInstance(tz);

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat(pattern1);

		simpleDateFormat.setTimeZone(tz);
		simpleDateFormat1.setTimeZone(tz);

		// c.setTime(new java.util.Date()); // Now use today date.
		String callDate = simpleDateFormat.format(c.getTime());
		String callTime = simpleDateFormat1.format(c.getTime());

		// logger.info("the time zone of Dealer is " + tz.getDisplayName());
		// logger.info("callDate : " + callDate + "callTime :" + callTime);

		String dateTimeStr = callDate + " " + callTime;
		SimpleDateFormat formating = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		try {
			Date datetime = formating.parse(dateTimeStr);
			// logger.info("datetime :"+datetime);
			call_interaction.setCallMadeDateAndTime(datetime);
		} catch (ParseException e) {
			// logger.info("Could not parse date");
			e.printStackTrace();
		}

		call_interaction.setCallDate(callDate);
		call_interaction.setCallTime(callTime);
		call_interaction.setDealerCode(dealercode);

		call_interaction.setCallCount(call_interaction.getCallCount() + 1);
		call_interaction.setCustomer(cus);
		call_interaction.setVehicle(veh);
		call_interaction.setWyzUser(wyz);

		// Adding all remarks

		List<String> remakListData = listData.getRemarksList();
		String remaks = "";
		for (String sa : remakListData) {

			if (sa != null) {

				remaks = remaks + sa;
			}

		}
		sr_Disposition.setRemarks(remaks);
		logger.info(" remakListData : " + remakListData.size());

		if (call_interaction.getIsCallinitaited().equals("Initiated")) {

			call_interaction.setDailedStatus(true);

		}

		CallDispositionData callDataDispo = getCallDispositionVariable(dispo_data.getDisposition());

		sr_Disposition.setCallInteraction(call_interaction);
		sr_Disposition.setCallDispositionData(callDataDispo);

		logger.info(" call interaction id :" + listData.getCallInteractionId());

		if (listData.getCallInteractionId() == 0) {

			long assId = listData.getSingleId();

			long countAssigne = source.assignedInteractions(em).where(u -> u.getId() == assId).count();
			if (countAssigne > 0) {

				if (listData.getSingleId() != 0) {
					AssignedInteraction assignId = em.find(AssignedInteraction.class, listData.getSingleId());
					assignId.setCallMade("Yes");
					assignId.setFinalDisposition(callDataDispo);
					assignId.setLastDisposition("Not yet Called");
					assignId.setWyzUser(wyz);
					em.merge(assignId);
					call_interaction.setAssignedInteraction(assignId);
					call_interaction.setCampaign(assignId.getCampaign());

				}

			} else {

				List<AssignedInteraction> assigendList = source.assignedInteractions(em)
						.where(u -> u.getCustomer().getId() == customer_id && u.getVehicle().getVehicle_id() == vehi_id)
						.toList();

				int countAss = assigendList.size();

				if (countAss != 0) {

					if (countAss < 1) {

						call_interaction.setAssignedInteraction(assigendList.get(0));
						call_interaction.setCampaign(assigendList.get(0).getCampaign());

						if (assigendList.get(0).getFinalDisposition() != null) {
							assigendList.get(0)
									.setLastDisposition(assigendList.get(0).getFinalDisposition().getDisposition());
						} else {
							assigendList.get(0).setLastDisposition("Not yet Called");
							assigendList.get(0).setCallMade("Yes");
						}
						assigendList.get(0).setWyzUser(wyz);
						assigendList.get(0).setFinalDisposition(callDataDispo);
						em.merge(assigendList.get(0));
					} else {

						call_interaction.setAssignedInteraction(assigendList.get(countAss - 1));
						call_interaction.setCampaign(assigendList.get(countAss - 1).getCampaign());

						if (assigendList.get(countAss - 1).getFinalDisposition() != null) {
							assigendList.get(countAss - 1).setLastDisposition(
									assigendList.get(countAss - 1).getFinalDisposition().getDisposition());
						} else {
							assigendList.get(countAss - 1).setLastDisposition("Not yet Called");
							assigendList.get(countAss - 1).setCallMade("Yes");
						}
						assigendList.get(countAss - 1).setWyzUser(wyz);
						assigendList.get(countAss - 1).setFinalDisposition(callDataDispo);
						em.merge(assigendList.get(countAss - 1));

					}

				} else {

					Campaign smrCam = source.campaigns(em).where(u -> u.getId() == 1).getOnlyValue();
					AssignedInteraction newAssignId = new AssignedInteraction();
					newAssignId.setVehicle(veh);
					newAssignId.setCustomer(cus);
					newAssignId.setWyzUser(wyz);
					newAssignId.setCampaign(smrCam);
					newAssignId.setCallMade("Yes");
					newAssignId.setInteractionType(smrCam.getCampaignName());
					newAssignId.setFinalDisposition(callDataDispo);
					newAssignId.setUplodedCurrentDate(currentDate());
					newAssignId.setLastDisposition("Not yet Called");
					em.persist(newAssignId);

					call_interaction.setAssignedInteraction(newAssignId);
					call_interaction.setCampaign(newAssignId.getCampaign());

				}

				logger.info("count of assigned list for incoming call callint : " + assigendList.size());

			}

		} else if (listData.getSingleId() == 0) {

			long callId = listData.getCallInteractionId();

			long countCallInte = source.callinteractions(em).where(u -> u.getId() == callId).count();
			if (countCallInte > 0) {

				if (listData.getCallInteractionId() != 0) {
					CallInteraction callInterac_Id = source.callinteractions(em).where(u -> u.getId() == callId)
							.getOnlyValue();

					AssignedInteraction assignData = callInterac_Id.getAssignedInteraction();

					if (assignData != null) {
						call_interaction.setAssignedInteraction(assignData);
						call_interaction.setCampaign(assignData.getCampaign());
						call_interaction.setDroppedCount(callInterac_Id.getDroppedCount());
						assignData.setFinalDisposition(callDataDispo);
						assignData.setWyzUser(wyz);
						if (callInterac_Id.getSrdisposition() != null) {

							if (callInterac_Id.getSrdisposition().getCallDispositionData() != null) {

								assignData.setLastDisposition(
										callInterac_Id.getSrdisposition().getCallDispositionData().getDisposition());

								if (callInterac_Id.getSrdisposition().getCallDispositionData()
										.getDispositionId() == CallDispositionData.STATUS_CALL_ME_LATER) {

									SRDisposition oldSR = callInterac_Id.getSrdisposition();
									oldSR.setIsFollowUpDone("Yes");
									em.merge(oldSR);
								}
							}
							if (sr_Disposition.getTypeOfDisposition().equals("NonContact")) {

								logger.info("add 1 to dropped count");
								call_interaction.setDroppedCount(callInterac_Id.getDroppedCount() + 1);

							}

						} else if (sr_Disposition.getTypeOfDisposition().equals("NonContact")) {

							logger.info("add 1 to dropped count");
							call_interaction.setDroppedCount(callInterac_Id.getDroppedCount() + 1);

						}

						em.merge(assignData);
					} else {

						List<AssignedInteraction> assigendList = source.assignedInteractions(em)
								.where(u -> u.getCustomer().getId() == customer_id
										&& u.getVehicle().getVehicle_id() == vehi_id)
								.toList();
						int countAss = assigendList.size();

						if (countAss != 0) {

							if (countAss < 1) {
								call_interaction.setAssignedInteraction(assigendList.get(0));
								call_interaction.setCampaign(assigendList.get(0).getCampaign());

								if (assigendList.get(0).getFinalDisposition() != null) {
									assigendList.get(0).setLastDisposition(
											assigendList.get(0).getFinalDisposition().getDisposition());
								} else {
									assigendList.get(0).setLastDisposition("Not yet Called");
									assigendList.get(0).setCallMade("Yes");
								}
								assigendList.get(0).setWyzUser(wyz);
								assigendList.get(0).setFinalDisposition(callDataDispo);
								em.merge(assigendList.get(0));
							} else {

								call_interaction.setAssignedInteraction(assigendList.get(countAss - 1));
								call_interaction.setCampaign(assigendList.get(countAss - 1).getCampaign());

								if (assigendList.get(countAss - 1).getFinalDisposition() != null) {
									assigendList.get(countAss - 1).setLastDisposition(
											assigendList.get(countAss - 1).getFinalDisposition().getDisposition());
								} else {
									assigendList.get(countAss - 1).setLastDisposition("Not yet Called");
									assigendList.get(countAss - 1).setCallMade("Yes");
								}
								assigendList.get(countAss - 1).setWyzUser(wyz);
								assigendList.get(countAss - 1).setFinalDisposition(callDataDispo);
								em.merge(assigendList.get(countAss - 1));

							}

						} else {
							Campaign smrCam = source.campaigns(em).where(u -> u.getId() == 1).getOnlyValue();
							AssignedInteraction newAssignId = new AssignedInteraction();
							newAssignId.setVehicle(veh);
							newAssignId.setCustomer(cus);
							newAssignId.setWyzUser(wyz);
							newAssignId.setCampaign(smrCam);
							newAssignId.setCallMade("Yes");
							newAssignId.setInteractionType(smrCam.getCampaignName());
							newAssignId.setFinalDisposition(callDataDispo);
							newAssignId.setUplodedCurrentDate(currentDate());
							newAssignId.setLastDisposition("Not yet Called");
							em.persist(newAssignId);

							call_interaction.setAssignedInteraction(newAssignId);
							call_interaction.setCampaign(newAssignId.getCampaign());

						}
					}

				}

			} else {

				List<AssignedInteraction> assigendList = source.assignedInteractions(em)
						.where(u -> u.getCustomer().getId() == customer_id && u.getVehicle().getVehicle_id() == vehi_id)
						.toList();

				int countAss = assigendList.size();

				if (countAss != 0) {

					if (countAss < 1) {
						call_interaction.setAssignedInteraction(assigendList.get(0));
						call_interaction.setCampaign(assigendList.get(0).getCampaign());

						if (assigendList.get(0).getFinalDisposition() != null) {
							assigendList.get(0)
									.setLastDisposition(assigendList.get(0).getFinalDisposition().getDisposition());
						} else {
							assigendList.get(0).setLastDisposition("Not yet Called");
							assigendList.get(0).setCallMade("Yes");
						}
						assigendList.get(0).setWyzUser(wyz);
						assigendList.get(0).setFinalDisposition(callDataDispo);
						em.merge(assigendList.get(0));
					} else {

						call_interaction.setAssignedInteraction(assigendList.get(countAss - 1));
						call_interaction.setCampaign(assigendList.get(countAss - 1).getCampaign());

						if (assigendList.get(countAss - 1).getFinalDisposition() != null) {
							assigendList.get(countAss - 1).setLastDisposition(
									assigendList.get(countAss - 1).getFinalDisposition().getDisposition());
						} else {
							assigendList.get(countAss - 1).setLastDisposition("Not yet Called");
							assigendList.get(countAss - 1).setCallMade("Yes");
						}
						assigendList.get(countAss - 1).setWyzUser(wyz);
						assigendList.get(countAss - 1).setFinalDisposition(callDataDispo);
						em.merge(assigendList.get(countAss - 1));

					}

				} else {

					Campaign smrCam = source.campaigns(em).where(u -> u.getId() == 1).getOnlyValue();
					AssignedInteraction newAssignId = new AssignedInteraction();
					newAssignId.setVehicle(veh);
					newAssignId.setCustomer(cus);
					newAssignId.setWyzUser(wyz);
					newAssignId.setCampaign(smrCam);
					newAssignId.setCallMade("Yes");
					newAssignId.setInteractionType(smrCam.getCampaignName());
					newAssignId.setFinalDisposition(callDataDispo);
					newAssignId.setUplodedCurrentDate(currentDate());
					newAssignId.setLastDisposition("Not yet Called");
					em.persist(newAssignId);

					call_interaction.setAssignedInteraction(newAssignId);
					call_interaction.setCampaign(newAssignId.getCampaign());

				}

				logger.info("count of assigned list for incoming call single id : " + assigendList.size());

			}
		}
		sr_Disposition.setUpsellCount(0);

		if (listData.getCallInteractionId() != 0) {

			long callId = listData.getCallInteractionId();

			long countOfExist = source.callinteractions(em).where(u -> u.getId() == callId)
					.where(u -> u.getServiceBooked() != null).select(u -> u.getServiceBooked()).count();

			if (countOfExist != 0) {

				List<ServiceBooked> lastServiceBookList = source.callinteractions(em).where(u -> u.getId() == callId)
						.where(u -> u.getServiceBooked() != null).select(u -> u.getServiceBooked()).toList();

				ServiceBooked lastServiceBook = Collections.max(lastServiceBookList,
						Comparator.comparing(a -> a.serviceBookedId));

				call_interaction.setServiceBooked(lastServiceBook);

			}

		}

		em.persist(call_interaction);
		em.persist(sr_Disposition);

		if (sr_Disposition.getTypeOfDisposition().equals("NonContact")) {
			String type = "NONCONTACT";
			// SMSTemplate smstemplateData = source.smsTemplates(em).where(u ->
			// u.getSmsType().equals(type)).getOnlyValue();
			// String servicebookedtype = SMSTemplate.SERVICE_BOOKED;
			String message = source.smsTemplates(em).where(m -> m.getSmsType().equals(type))
					.select(m -> m.getSmsTemplate()).getOnlyValue();
			// String
			// updateMess=getUpdateMessForSMSTemplate(smstemplateData.getSmsTemplate(),veh,cus);
			message = message.replace("(customer name)", cus.getCustomerName());
			message = message.replace("(Vehicle)", veh.getVehicleRegNo());
			message = message.replace("(phonenumber)", wyz.getPhoneNumber());
			logger.info("inside non contact:" + message);
			smsTriggerRepo.sendCustomSMS(vehi_id, message, type, wyz);
		}

		if (sr_Disposition.getNoServiceReason() != null) {
			if (sr_Disposition.getNoServiceReason().equals("Dissatisfied with previous service")
					|| sr_Disposition.getNoServiceReason().equals("Dissatisfied with Sales")
					|| sr_Disposition.getNoServiceReason().equals("Dissatisfied with Insurance")) {
				String typeone = "COMPLAINT";
				SMSTemplate smstemplateData = source.smsTemplates(em).where(u -> u.getSmsType().equals(typeone))
						.getOnlyValue();
				String updateMess = getUpdateMessForSMSTemplate(smstemplateData.getSmsTemplate(), veh, cus);
				// String smstemplatestype = updateMess.getSmsTemplate();
				// smsTriggerRepo.sendCustomSMS(vehi_id,updateMess,typeone,wyz);

			}
		}

	}

	// get latest service by vehical id

	private String getUpdateMessForSMSTemplate(String template, Vehicle vehicleData, Customer customerData) {
		// TODO Auto-generated method stub
		if (vehicleData != null) {

			if (vehicleData.getVehicleRegNo() != null) {
				template.replace("(Vehicle)", vehicleData.getVehicleRegNo());
			} else {

				template.replace("(Vehicle)", "");
			}
			if (vehicleData.getNextServicedate() != null) {

				template.replace("(Service Due Date)", vehicleData.getNextServicedate().toString());
			} else {

				template.replace("(Service Due Date)", "");
			}

		} else {
			template.replace("(Vehicle)", "");
		}

		if (customerData != null) {
			if (customerData.getCustomerName() != null) {
				template.replace("(customer name)", customerData.getCustomerName());

			}

		}
		return template;
	}

	private Service getLatestServiceData(long vehicle_id) {
		// logger.info("vehicle_id tui listr" + vehicle_id);
		List<Service> service_list = source.services(em).where(u -> u.getLastServiceDate() != null)
				.where(u -> u.getVehicle().getVehicle_id() == vehicle_id).toList();
		List<Service> finalData = new ArrayList<>();
		if (service_list.size() != 0) {

			Date tmp = null;
			int i = 0;
			if (service_list.size() != 0) {
				for (Service uniqueNo_list : service_list) {

					if (i == 0) {
						tmp = uniqueNo_list.getLastServiceDate();
						i++;
					} else {
						if (tmp.before(uniqueNo_list.getLastServiceDate())) {
							tmp = uniqueNo_list.getLastServiceDate();
							i++;
						}
					}

				}
				// logger.info("getting latest disposition :" + tmp);
				finalData.add(service_list.get(i - 1));
			}

			return finalData.get(0);
		} else {
			Service data = new Service();
			return data;
		}

	}

	private java.sql.Date currentDate() {

		return new java.sql.Date(System.currentTimeMillis());
	}

	@Override
	public void addUpsellLead(SRDisposition sr_Disposition, ListingForm listData, long vehicleid) {
		List<UpsellLead> upselldata = listData.getUpsellLead();

		long countOfUpsell = upselldata.size();
		logger.info("count of upsell : " + countOfUpsell);
		Vehicle linkedVeh = em.find(Vehicle.class, vehicleid);
		if (countOfUpsell != 0) {
			for (UpsellLead typeData : upselldata) {

				if (typeData.getUpsellId() != 0) {

					TaggingUsers taggedUserData = getTaggedUserByType(typeData.getUpsellId());

					UpsellLead addupselllead = new UpsellLead();
					addupselllead.setTaggedTo(typeData.getTaggedTo());
					addupselllead.setUpSellType(typeData.getUpSellType());
					addupselllead.setUpsellComments(typeData.getUpsellComments());
					addupselllead.setUpsellId(typeData.getUpsellId());
					addupselllead.setSrDisposition(sr_Disposition);
					addupselllead.setVehicle(linkedVeh);
					addupselllead.setTaggingUsers(taggedUserData);
					em.persist(addupselllead);

				}
			}

		}
		long call_int_id = sr_Disposition.getCallInteraction().getId();
		long sr_id = sr_Disposition.getId();
		long countOfUpsellIs = source.upsellLeads(em).where(u -> u.getSrDisposition() != null)
				.where(u -> u.getSrDisposition().getId() == sr_id).count();
		logger.info("countOfUpsellIs : " + countOfUpsellIs);
		SRDisposition serviceCount = source.srDisposition(em).where(u -> u.getId() == sr_id).getOnlyValue();
		serviceCount.setUpsellCount(countOfUpsellIs);
		em.merge(serviceCount);

	}

	public void addCallInteractionOfServiceBooked(CallInteraction call_interaction, SRDisposition sr_Disposition,
			ListingForm listData, ServiceBooked service_Booked, PickupDrop pick_up, String dealercode,
			CallDispositionData dispo_data) {

		logger.info("listData.getVehicleId_SB() :" + listData.getVehicleId_SB() + "listData.getCustomer_Id() : "
				+ listData.getCustomer_Id() + "listData.getWyzUser_Id() : " + listData.getWyzUser_Id()
				+ "listData.getWorkshopId() :  " + listData.getWorkshopId());

		Vehicle veh = em.find(Vehicle.class, listData.getVehicleId_SB());
		Customer cus = em.find(Customer.class, listData.getCustomer_Id());
		WyzUser wyz = em.find(WyzUser.class, listData.getWyzUser_Id());

		long vehi_id = veh.getVehicle_id();
		long customer_id = cus.getId();

		ServiceAdvisor service_advisor = em.find(ServiceAdvisor.class, listData.getServiceAdvisorId());

		Workshop work_shop = em.find(Workshop.class, listData.getWorkshopId());

		String advisor_name = service_advisor.getAdvisorName();
		logger.info("advisor_name :" + advisor_name);

		Service service_latest = getLatestServiceData(veh.getVehicle_id());

		if (service_latest.id != 0) {

			service_latest.setServiceAdvisor(service_advisor);
			em.merge(service_latest);
		} else {
			// Service newService = new Service();
			// newService.setServiceAdvisor(service_advisor);
			// em.persist(newService);
		}

		String pattern = "dd/MM/yyyy";
		String pattern1 = "HH:mm:ss";

		// logger.info("time_zone :"+time_zone);
		java.util.TimeZone tz = java.util.TimeZone.getTimeZone("IST");
		Calendar c = Calendar.getInstance(tz);

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat(pattern1);

		simpleDateFormat.setTimeZone(tz);
		simpleDateFormat1.setTimeZone(tz);

		// c.setTime(new java.util.Date()); // Now use today date.
		String callDate = simpleDateFormat.format(c.getTime());
		String callTime = simpleDateFormat1.format(c.getTime());

		// logger.info("the time zone of Dealer is " + tz.getDisplayName());
		// logger.info("callDate : " + callDate + "callTime :" + callTime);

		String dateTimeStr = callDate + " " + callTime;
		SimpleDateFormat formating = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		try {
			Date datetime = formating.parse(dateTimeStr);
			logger.info("datetime :" + datetime);
			call_interaction.setCallMadeDateAndTime(datetime);
		} catch (ParseException e) {
			logger.info("Could not parse date");
			e.printStackTrace();
		}

		call_interaction.setCallDate(callDate);
		call_interaction.setCallTime(callTime);
		call_interaction.setDealerCode(dealercode);
		call_interaction.setFirebaseKey("false");
		call_interaction.setSynchedToFirebase("false");
		call_interaction.setCallCount(call_interaction.getCallCount() + 1);
		call_interaction.setCustomer(cus);
		call_interaction.setVehicle(veh);
		call_interaction.setWyzUser(wyz);

		List<String> remakListData = listData.getRemarksList();
		String remaks = "";
		for (String sa : remakListData) {

			if (sa != null) {

				remaks = remaks + sa;
			}

		}
		sr_Disposition.setRemarks(remaks);
		logger.info(" remakListData : " + remakListData.size());

		if (call_interaction.getIsCallinitaited().equals("Initiated")) {

			call_interaction.setDailedStatus(true);

		}

		CallDispositionData callDataDispo = getCallDispositionVariable(dispo_data.getDisposition());

		sr_Disposition.setCallDispositionData(callDataDispo);
		// logger.info("sr_Disposition.getInBoundLeadSource() :
		// "+sr_Disposition.getInBoundLeadSource());

		// if(sr_Disposition.getInOutCallName().equals("OutCall")){

		logger.info("Call interaction : " + listData.getCallInteractionId());

		logger.info("Assigned interaction : " + listData.getSingleId());

		if (listData.getCallInteractionId() == 0) {
			logger.info("1 checking");

			long assId = listData.getSingleId();

			long countAssigne = source.assignedInteractions(em).where(u -> u.getId() == assId).count();
			if (countAssigne > 0) {
				logger.info("2 checking");

				if (listData.getSingleId() != 0) {
					logger.info("3 checking");
					AssignedInteraction assignId = em.find(AssignedInteraction.class, listData.getSingleId());
					assignId.setCallMade("Yes");
					assignId.setLastDisposition("Not yet Called");
					assignId.setFinalDisposition(callDataDispo);
					assignId.setWyzUser(wyz);
					em.merge(assignId);
					call_interaction.setAssignedInteraction(assignId);
					call_interaction.setCampaign(assignId.getCampaign());

				} else {
					logger.info("4 checking");

					List<AssignedInteraction> assigendList = source.assignedInteractions(em).where(
							u -> u.getCustomer().getId() == customer_id && u.getVehicle().getVehicle_id() == vehi_id)
							.toList();

					int countAss = assigendList.size();

					if (countAss != 0) {

						if (countAss < 1) {
							call_interaction.setAssignedInteraction(assigendList.get(0));
							call_interaction.setCampaign(assigendList.get(0).getCampaign());

							if (assigendList.get(0).getFinalDisposition() != null) {
								assigendList.get(0)
										.setLastDisposition(assigendList.get(0).getFinalDisposition().getDisposition());
							} else {
								assigendList.get(0).setLastDisposition("Not yet Called");
								assigendList.get(0).setCallMade("Yes");
							}
							assigendList.get(0).setWyzUser(wyz);
							assigendList.get(0).setFinalDisposition(callDataDispo);
							em.merge(assigendList.get(0));
						} else {

							call_interaction.setAssignedInteraction(assigendList.get(countAss - 1));
							call_interaction.setCampaign(assigendList.get(countAss - 1).getCampaign());

							if (assigendList.get(countAss - 1).getFinalDisposition() != null) {
								assigendList.get(countAss - 1).setLastDisposition(
										assigendList.get(countAss - 1).getFinalDisposition().getDisposition());
							} else {
								assigendList.get(countAss - 1).setLastDisposition("Not yet Called");
								assigendList.get(countAss - 1).setCallMade("Yes");
							}
							assigendList.get(countAss - 1).setWyzUser(wyz);
							assigendList.get(countAss - 1).setFinalDisposition(callDataDispo);
							em.merge(assigendList.get(countAss - 1));

						}

					} else {

						logger.info("7 checking");

						Campaign smrCam = source.campaigns(em).where(u -> u.getId() == 1).getOnlyValue();
						AssignedInteraction newAssignId = new AssignedInteraction();
						newAssignId.setVehicle(veh);
						newAssignId.setCustomer(cus);
						newAssignId.setWyzUser(wyz);
						newAssignId.setCampaign(smrCam);
						newAssignId.setCallMade("Yes");
						newAssignId.setInteractionType(smrCam.getCampaignName());
						newAssignId.setFinalDisposition(callDataDispo);
						newAssignId.setUplodedCurrentDate(currentDate());
						newAssignId.setLastDisposition("Not yet Called");
						em.persist(newAssignId);

						call_interaction.setAssignedInteraction(newAssignId);
						call_interaction.setCampaign(newAssignId.getCampaign());

					}

					logger.info("count of assigned list for incoming call single id : " + assigendList.size());

				}

			} else {
				logger.info("4 checking");

				List<AssignedInteraction> assigendList = source.assignedInteractions(em)
						.where(u -> u.getCustomer().getId() == customer_id && u.getVehicle().getVehicle_id() == vehi_id)
						.toList();

				int countAss = assigendList.size();

				if (countAss != 0) {

					if (countAss < 1) {
						call_interaction.setAssignedInteraction(assigendList.get(0));
						call_interaction.setCampaign(assigendList.get(0).getCampaign());

						if (assigendList.get(0).getFinalDisposition() != null) {
							assigendList.get(0)
									.setLastDisposition(assigendList.get(0).getFinalDisposition().getDisposition());
						} else {
							assigendList.get(0).setLastDisposition("Not yet Called");
							assigendList.get(0).setCallMade("Yes");
						}
						assigendList.get(0).setWyzUser(wyz);
						assigendList.get(0).setFinalDisposition(callDataDispo);
						em.merge(assigendList.get(0));
					} else {

						call_interaction.setAssignedInteraction(assigendList.get(countAss - 1));
						call_interaction.setCampaign(assigendList.get(countAss - 1).getCampaign());

						if (assigendList.get(countAss - 1).getFinalDisposition() != null) {
							assigendList.get(countAss - 1).setLastDisposition(
									assigendList.get(countAss - 1).getFinalDisposition().getDisposition());
						} else {
							assigendList.get(countAss - 1).setLastDisposition("Not yet Called");
							assigendList.get(countAss - 1).setCallMade("Yes");
						}
						assigendList.get(countAss - 1).setWyzUser(wyz);
						assigendList.get(countAss - 1).setFinalDisposition(callDataDispo);
						em.merge(assigendList.get(countAss - 1));

					}

				} else {

					logger.info("7 checking");

					Campaign smrCam = source.campaigns(em).where(u -> u.getId() == 1).getOnlyValue();
					AssignedInteraction newAssignId = new AssignedInteraction();
					newAssignId.setVehicle(veh);
					newAssignId.setCustomer(cus);
					newAssignId.setWyzUser(wyz);
					newAssignId.setCampaign(smrCam);
					newAssignId.setCallMade("Yes");
					newAssignId.setInteractionType(smrCam.getCampaignName());
					newAssignId.setFinalDisposition(callDataDispo);
					newAssignId.setUplodedCurrentDate(currentDate());
					newAssignId.setLastDisposition("Not yet Called");
					em.persist(newAssignId);

					call_interaction.setAssignedInteraction(newAssignId);
					call_interaction.setCampaign(newAssignId.getCampaign());

				}

				logger.info("count of assigned list for incoming call single id : " + assigendList.size());

			}

		} else if (listData.getSingleId() == 0) {
			logger.info("8 checking");

			long callId = listData.getCallInteractionId();

			long countCallInte = source.callinteractions(em).where(u -> u.getId() == callId).count();
			if (countCallInte > 0) {
				logger.info("9 checking");

				if (listData.getCallInteractionId() != 0) {

					logger.info("10 checking");
					// CallInteraction callInterac_Id =
					// em.find(CallInteraction.class,
					// listData.getCallInteractionId());

					CallInteraction callInterac_Id = source.callinteractions(em).where(u -> u.getId() == callId)
							.getOnlyValue();
					AssignedInteraction assignData = callInterac_Id.getAssignedInteraction();

					if (assignData != null) {
						call_interaction.setAssignedInteraction(assignData);
						call_interaction.setCampaign(assignData.getCampaign());
						call_interaction.setDroppedCount(callInterac_Id.getDroppedCount());
						assignData.setFinalDisposition(callDataDispo);
						assignData.setWyzUser(wyz);
						if (callInterac_Id.getSrdisposition() != null) {

							if (callInterac_Id.getSrdisposition().getCallDispositionData() != null) {

								assignData.setLastDisposition(
										callInterac_Id.getSrdisposition().getCallDispositionData().getDisposition());

								if (callInterac_Id.getSrdisposition().getCallDispositionData()
										.getDispositionId() == CallDispositionData.STATUS_CALL_ME_LATER) {

									SRDisposition oldSR = callInterac_Id.getSrdisposition();
									oldSR.setIsFollowUpDone("Yes");
									em.merge(oldSR);

								}
							}
						}

						em.merge(assignData);
					} else {

						logger.info("11 checking");

						List<AssignedInteraction> assigendList = source.assignedInteractions(em)
								.where(u -> u.getCustomer().getId() == customer_id
										&& u.getVehicle().getVehicle_id() == vehi_id)
								.toList();

						int countAss = assigendList.size();

						if (countAss != 0) {

							if (countAss < 1) {
								call_interaction.setAssignedInteraction(assigendList.get(0));
								call_interaction.setCampaign(assigendList.get(0).getCampaign());
								if (assigendList.get(0).getFinalDisposition() != null) {
									assigendList.get(0).setLastDisposition(
											assigendList.get(0).getFinalDisposition().getDisposition());
								} else {
									assigendList.get(0).setLastDisposition("Not yet Called");
									assigendList.get(0).setCallMade("Yes");
								}
								assigendList.get(0).setWyzUser(wyz);
								assigendList.get(0).setFinalDisposition(callDataDispo);
								em.merge(assigendList.get(0));
							} else {

								call_interaction.setAssignedInteraction(assigendList.get(countAss - 1));
								call_interaction.setCampaign(assigendList.get(countAss - 1).getCampaign());

								if (assigendList.get(countAss - 1).getFinalDisposition() != null) {
									assigendList.get(countAss - 1).setLastDisposition(
											assigendList.get(countAss - 1).getFinalDisposition().getDisposition());
								} else {
									assigendList.get(countAss - 1).setLastDisposition("Not yet Called");
									assigendList.get(countAss - 1).setCallMade("Yes");
								}
								assigendList.get(countAss - 1).setWyzUser(wyz);
								assigendList.get(countAss - 1).setFinalDisposition(callDataDispo);
								em.merge(assigendList.get(countAss - 1));

							}

						} else {

							logger.info("13 checking");
							Campaign smrCam = source.campaigns(em).where(u -> u.getId() == 1).getOnlyValue();
							AssignedInteraction newAssignId = new AssignedInteraction();
							newAssignId.setVehicle(veh);
							newAssignId.setCustomer(cus);
							newAssignId.setWyzUser(wyz);
							newAssignId.setCampaign(smrCam);
							newAssignId.setCallMade("Yes");
							newAssignId.setInteractionType(smrCam.getCampaignName());
							newAssignId.setFinalDisposition(callDataDispo);
							newAssignId.setUplodedCurrentDate(currentDate());
							newAssignId.setLastDisposition("Not yet Called");
							em.persist(newAssignId);

							call_interaction.setAssignedInteraction(newAssignId);
							call_interaction.setCampaign(newAssignId.getCampaign());

						}
					}
				}

			} else {

				List<AssignedInteraction> assigendList = source.assignedInteractions(em)
						.where(u -> u.getCustomer().getId() == customer_id && u.getVehicle().getVehicle_id() == vehi_id)
						.toList();

				int countAss = assigendList.size();

				if (countAss != 0) {

					if (countAss < 1) {
						call_interaction.setAssignedInteraction(assigendList.get(0));
						call_interaction.setCampaign(assigendList.get(0).getCampaign());

						if (assigendList.get(0).getFinalDisposition() != null) {
							assigendList.get(0)
									.setLastDisposition(assigendList.get(0).getFinalDisposition().getDisposition());
						} else {
							assigendList.get(0).setLastDisposition("Not yet Called");
							assigendList.get(0).setCallMade("Yes");
						}
						assigendList.get(0).setWyzUser(wyz);
						assigendList.get(0).setFinalDisposition(callDataDispo);
						em.merge(assigendList.get(0));
					} else {

						call_interaction.setAssignedInteraction(assigendList.get(countAss - 1));
						call_interaction.setCampaign(assigendList.get(countAss - 1).getCampaign());

						if (assigendList.get(countAss - 1).getFinalDisposition() != null) {
							assigendList.get(countAss - 1).setLastDisposition(
									assigendList.get(countAss - 1).getFinalDisposition().getDisposition());
						} else {
							assigendList.get(countAss - 1).setLastDisposition("Not yet Called");
							assigendList.get(countAss - 1).setCallMade("Yes");
						}
						assigendList.get(countAss - 1).setWyzUser(wyz);
						assigendList.get(countAss - 1).setFinalDisposition(callDataDispo);
						em.merge(assigendList.get(countAss - 1));

					}

				} else {

					logger.info("15 checking");
					Campaign smrCam = source.campaigns(em).where(u -> u.getId() == 1).getOnlyValue();
					AssignedInteraction newAssignId = new AssignedInteraction();
					newAssignId.setVehicle(veh);
					newAssignId.setCustomer(cus);
					newAssignId.setWyzUser(wyz);
					newAssignId.setCampaign(smrCam);
					newAssignId.setCallMade("Yes");
					newAssignId.setInteractionType(smrCam.getCampaignName());
					newAssignId.setFinalDisposition(callDataDispo);
					newAssignId.setUplodedCurrentDate(currentDate());
					newAssignId.setLastDisposition("Not yet Called");
					em.persist(newAssignId);

					call_interaction.setAssignedInteraction(newAssignId);
					call_interaction.setCampaign(newAssignId.getCampaign());

				}

				logger.info("count of assigned list for incoming call single id : " + assigendList.size());

			}
		} else if (sr_Disposition.getInOutCallName().equals("InCall")) {

			Campaign smrCam = source.campaigns(em).where(u -> u.getId() == 1).getOnlyValue();
			logger.info("in bound call is going");
			AssignedInteraction newAssignId = new AssignedInteraction();
			newAssignId.setVehicle(veh);
			newAssignId.setCustomer(cus);
			newAssignId.setWyzUser(wyz);
			newAssignId.setCampaign(smrCam);
			newAssignId.setCallMade("Yes");
			newAssignId.setInteractionType(smrCam.getCampaignName());
			newAssignId.setFinalDisposition(callDataDispo);
			newAssignId.setUplodedCurrentDate(currentDate());
			newAssignId.setLastDisposition("Not yet Called");
			em.persist(newAssignId);

			call_interaction.setAssignedInteraction(newAssignId);
			call_interaction.setCampaign(newAssignId.getCampaign());

		}

		SimpleDateFormat formatingData = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		String scheduledDateAndtime = listData.getServiceScheduledDate() + " " + listData.getServiceScheduledTime()
				+ ":00";

		// logger.info("scheduledDateAndtime :"+scheduledDateAndtime);
		try {
			Date datetime = formatingData.parse(scheduledDateAndtime);
			// logger.info("datetime os setScheduledDateTime:"+datetime);
			service_Booked.setScheduledDateTime(datetime);
		} catch (ParseException e) {
			// logger.info("Could not parse date");
			e.printStackTrace();
		}

		long wyzuser_id = listData.getDriverId();

		if (service_Booked.getTypeOfPickup().equals("true")
				|| service_Booked.getTypeOfPickup().equals("Maruthi Mobile Support")
				|| service_Booked.getTypeOfPickup().equals("Road Side Assistance")
				|| service_Booked.getTypeOfPickup().equals("Door Step Service")) {
			SimpleDateFormat formatingData1 = new SimpleDateFormat("HH:mm:ss");

			if (wyzuser_id != 0) {
				Driver driver_link = source.driver(em).where(u -> u.getWyzUser().getId() == wyzuser_id).getOnlyValue();
				pick_up.setDriver(driver_link);
				BookingDateTime bookingFrom = getBookingTimeById(listData.getTime_From());
				BookingDateTime bookingTo = getBookingTimeById(listData.getTime_To());
				pick_up.setTimeFrom(bookingFrom.getStartTime());

				long ONE_MINUTE_IN_MILLIS = 60000;
				long initailMin = bookingTo.getStartTime().getTime();
				long timeslotdifference = 30;
				long afterAddingMins = initailMin + (timeslotdifference * ONE_MINUTE_IN_MILLIS);
				Time time = new Time(afterAddingMins);
				pick_up.setTimeTo(time);
				service_Booked.setDriver(driver_link);
			}
			pick_up.setPickupDate(service_Booked.getScheduledDateTime());
			pick_up.setPickUpAddress(service_Booked.getServiceBookingAddress());

			em.persist(pick_up);
			service_Booked.setIsPickupRequired(true);
			service_Booked.setPickupDrop(pick_up);

		}

		logger.info("offerId selected is : " + listData.getOfferSelId());
		long offerid = listData.getOfferSelId();
		if (offerid != 0) {

			SpecialOfferMaster selected_offer = em.find(SpecialOfferMaster.class, offerid);

			service_Booked.setSpecialOfferMaster(selected_offer);

		}
		long serviceTypeId = Long.parseLong(service_Booked.getServiceBookedType());
		ServiceTypes servicetypes = source.serviceTypes(em).where(u -> u.getId() == serviceTypeId).getOnlyValue();
		service_Booked.setServiceBookType(servicetypes);
		service_Booked.setServiceBookedType(servicetypes.getServiceTypeName());
		if (listData.getCallInteractionId() != 0) {

			long callId = listData.getCallInteractionId();

			long countOfExist = source.callinteractions(em).where(u -> u.getId() == callId)
					.where(u -> u.getServiceBooked() != null).select(u -> u.getServiceBooked()).count();

			if (countOfExist != 0) {

				List<ServiceBooked> lastServiceBookList = source.callinteractions(em).where(u -> u.getId() == callId)
						.where(u -> u.getServiceBooked() != null).select(u -> u.getServiceBooked()).toList();

				ServiceBooked lastServiceBook = Collections.max(lastServiceBookList,
						Comparator.comparing(a -> a.serviceBookedId));

				CallDispositionData callDispo = source.callDispositionDatas(em).where(u -> u.getDispositionId() == 35)
						.getOnlyValue();
				lastServiceBook.setServiceBookStatus(callDispo);

			}

		}

		service_Booked.setServiceBookStatus(callDataDispo);
		// service_Booked.setCallInteraction(call_interaction);
		service_Booked.setCustomer(cus);
		service_Booked.setServiceAdvisor(service_advisor);
		service_Booked.setWorkshop(work_shop);
		service_Booked.setVehicle(veh);
		service_Booked.setWyzUser(wyz);
		em.persist(service_Booked);

		call_interaction.setServiceBooked(service_Booked);
		em.persist(call_interaction);

		sr_Disposition.setCallInteraction(call_interaction);
		sr_Disposition.setUpsellCount(0);
		em.persist(sr_Disposition);

		long workshopidis = listData.getWorkshopId();

		long countOfworkshopid = source.workshopSummarys(em).where(u -> u.getWorkshop().getId() == workshopidis)
				.count();

		// logger.info("countOfworkshopid :"+countOfworkshopid);
		if (countOfworkshopid > 0) {
			List<WorkshopSummary> data_summary = source.workshopSummarys(em)
					.where(u -> u.getWorkshop().getId() == workshopidis).toList();
			for (WorkshopSummary work_data : data_summary) {
				if (work_data.getServiceBookedDate().equals(currentDate())) {

					WorkshopSummary work_summay = source.workshopSummarys(em)
							.where(u -> u.getServiceBookedDate().equals(currentDate())
									&& u.getWorkshop().getId() == workshopidis)
							.getOnlyValue();
					work_summay.setNoOfServicesBooked(work_summay.getNoOfServicesBooked() + 1);
					em.merge(work_summay);
				}

			}

		} else {

			WorkshopSummary work_summay = new WorkshopSummary();
			work_summay.setNoOfServicesBooked(1);
			work_summay.setServiceBookedDate(currentDate());
			work_summay.setWorkshop(work_shop);
			em.persist(work_summay);

		}

		String servicebookedtype = SMSTemplate.SERVICE_BOOKED;
		String message = source.smsTemplates(em).where(m -> m.getSmsType().equals(servicebookedtype))
				.select(m -> m.getSmsTemplate()).getOnlyValue();

		message = message.replace("(customer name)", cus.getCustomerName());
		message = message.replace("(Vehicle)", veh.getVehicleRegNo());
		message = message.replace("(Date)", listData.getServiceScheduledDate().toString());
		message = message.replace("(SA Name)", service_advisor.getAdvisorName());
		message = message.replace("(location)", work_shop.workshopName);
		message = message.replace("(Time am/pm)", pick_up.getTimeFrom() + " to " + pick_up.getTimeTo());
		// message = message.replace("(Booking number)", listData.);
		logger.info("message : " + message);

		logger.info("vehi_id :" + vehi_id);
		logger.info("Service booked message :" + message);
		logger.info("wyz :" + wyz.getId());
		// static MyInterface ref=new MyInterface();
		smsTriggerRepo.sendCustomSMS(vehi_id, message, "Service Booked", wyz);
		if (wyzuser_id != 0) {
			String messageDriver = source.smsTemplates(em).where(m -> m.getSmsType().equals("Driver"))
					.select(m -> m.getSmsTemplate()).getOnlyValue();

			messageDriver = messageDriver.replace("(customer name)", cus.getCustomerName());
			messageDriver = messageDriver.replace("(Vehicle)", veh.getVehicleRegNo());
			messageDriver = messageDriver.replace("(Date)", pick_up.getPickupDateStr());
			messageDriver = messageDriver.replace("(customerNumber)", cus.getPreferredPhone().getPhoneNumber());
			messageDriver = messageDriver.replace("(Model)", veh.getModel());
			messageDriver = messageDriver.replace("(Time am/pm)", pick_up.getTimeFrom() + " to " + pick_up.getTimeTo());

			logger.info("messageDriver is " + messageDriver);

			Driver driver_link = source.driver(em).where(u -> u.getWyzUser().getId() == wyzuser_id).getOnlyValue();
			smsTriggerRepo.sendDriverSMS(driver_link, messageDriver, "Driver", wyz);
		}
		long lastCallintId = listData.getCallInteractionId();

		if (lastCallintId != 0) {

			long countOfSBExist = source.callinteractions(em).where(u -> u.getServiceBooked() != null)
					.where(u -> u.getId() == lastCallintId).select(u -> u.getServiceBooked()).count();
			if (countOfSBExist != 0) {

				SRDisposition lastSBDispoCancel = source.srDisposition(em).where(u -> u.getCallInteraction() != null)
						.where(u -> u.getCallInteraction().getId() == lastCallintId).getOnlyValue();

				CallDispositionData service_cancel = source.callDispositionDatas(em)
						.where(u -> u.getDispositionId() == 24).getOnlyValue();

				lastSBDispoCancel.setCallDispositionData(service_cancel);
				em.merge(lastSBDispoCancel);

			}

		}

	}

	private BookingDateTime getBookingTimeById(long bookId) {
		// TODO Auto-generated method stub
		return source.bookingDateTime(em).where(u -> u.getId() == bookId).getOnlyValue();
	}

	@Override
	public List<CallInteraction> CallHistoryOfCustomer(long vehicle_id, String dealercode) {

		List<CallInteraction> callHistoryOFNumber = source.callinteractions(em)
				.where(k -> k.getVehicle().getVehicle_id() == (vehicle_id)).toList();

		logger.info("xbcvvb : " + callHistoryOFNumber.size());
		return callHistoryOFNumber;
	}

	/*
	 * @Override public List<ServiceAdvisorInteraction>
	 * getSAInteractionList(long vehicle_id, String dealercode) {
	 * 
	 * List<ServiceAdvisorInteraction> interactionHistory =
	 * source.serviceAdvisorInteractions(em) .where(k ->
	 * k.getVehicle().getVehicle_id() == (vehicle_id)).toList();
	 * 
	 * return interactionHistory;
	 * 
	 * }
	 */

	// @Override
	// public String getDispositionValueById(String disposition_id) {
	//
	// CallDispositionData call_disposition = source.callDispositionData(em)
	//
	// .getOnlyValue();
	// String data_disposition = call_disposition.getDisposition();
	// return data_disposition;
	// }

	@Override
	public List<CallInteraction> getCallLogsByManager(String userName, String userLogindealerCode) {
		// logger.info("username in log list" + userName);
		List<WyzUser> userList = source.wyzUsers(em).where(u -> u.getCreManager().equals(userName)).toList();
		// logger.info("User List count:" + userList.size());
		ArrayList<String> names = new ArrayList<String>();
		for (WyzUser user : userList) {
			names.add(user.getUserName());
		}

		List<CallInteraction> returnlist = source.callinteractions(em)
				.where(c -> names.contains(c.getAgentName().toString())).toList();
		// logger.info("return list in call logs:" + returnlist.size());
		return returnlist;
	}

	@Override
	public void deletCall(Long id, String dealerDB) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<List> getServiceBookedBetweenTimeCREManager(String creMan, String dealerDB) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getCallTypePie(String creMan, String dealerDB) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getCallTypePieForCRE(String cre, String userLogindealerCode) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<List> getServiceBookedBetweenTimeForCRE(String cre, String userLogindealerCode) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getCallTypeCountsForCRE(String cre, String userLogindealerCode, Date currentDate,
			Date addedDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getCallsMadeVsServiceBookedForCRE(String cre, String userLogindealerCode, Date currentDate,
			Date addedDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateMessageSentStatusServiceBooked(long id, String string) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateMessageSentStatusServiceBookedOneDayBefore(long id, String string) {
		// TODO Auto-generated method stub

	}

	@Override
	public long getCountOfFollowUpsTobeDoneToday(String cre, String userLogindealerCode) {

		String pattern = "yyyy-MM-dd";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		Calendar c = Calendar.getInstance();
		c.setTime(new java.util.Date()); // Now use today date.

		String callDate = simpleDateFormat.format(c.getTime());
		// logger.info("currrent data is comparing for follow up is :
		// "+callDate);

		// long followBookedCount = source.callinteractions(em)
		// .where(k -> k.getWyzUser().getUserName().equals(cre))
		// .filter(k -> k.getSrdisposition().getFollowUpDate() != null)
		// .filter(k ->
		// k.getSrdisposition().getFollowUpDate().toString().equals(callDate))
		// .filter(k -> k.getSrdisposition().getIsFollowUpDone() != null)
		// .filter(k -> k.getSrdisposition().getIsFollowUpDone().equals("No"))
		// .count();

		List<String> vehical_numbers = source.vehicle(em).select(u -> u.getVehicleRegNo()).distinct().toList();
		// logger.info("vehical_numbers :"+vehical_numbers.size());

		List<CallInteraction> finalData = new ArrayList<CallInteraction>();

		for (String vehical_num : vehical_numbers) {

			List<CallInteraction> intData = source.callinteractions(em)
					.where(k -> k.getWyzUser().getUserName().equals(cre))
					.where(k -> k.getVehicle().getVehicleRegNo().equals(vehical_num))
					.where(k -> k.getSrdisposition().getIsFollowUpDone().equals("No")).toList();

			// List<CallInteraction> intData=source.callinteractions(em)
			// .where(k -> k.getWyzUser().getUserName().equals(username))
			// .where(k -> k.getVehicle().getVehicleRegNo().equals(vehical_num))
			// .toList();

			Date tmp = null;
			int i = 0;
			if (intData.size() != 0) {

				for (CallInteraction uniqueNo_list : intData) {

					if (i == 0) {

						tmp = uniqueNo_list.getCallMadeDateAndTime();
						i++;
					} else {

						if (tmp.before(uniqueNo_list.getCallMadeDateAndTime())) {

							tmp = uniqueNo_list.getCallMadeDateAndTime();
							i++;
						}

					}

				}
				// logger.info("getting latest disposition");

				finalData.add(intData.get(i - 1));

			}
		}

		List<CallInteraction> followBookedwithDate = new ArrayList<CallInteraction>();
		for (CallInteraction filteredData : finalData) {

			if (filteredData.getSrdisposition().getFollowUpDate().toString().equals(callDate)) {

				followBookedwithDate.add(filteredData);

			}

		}

		// logger.info("count of follow up for notification:
		// "+followBookedwithDate.size());

		return followBookedwithDate.size();

	}

	@Override
	public List<CallInteraction> getListOfFollowUpOfTodayCRE(List<Long> folowupDataId) {

		List<CallInteraction> finalData = new ArrayList<CallInteraction>();

		if (folowupDataId.size() > 0) {
			for (long call_id : folowupDataId) {
				CallInteraction call_interac = source.callinteractions(em).where(u -> u.getId() == call_id)
						.getOnlyValue();
				Hibernate.initialize(call_interac.getCustomer().getPreferredPhone());
				finalData.add(call_interac);

			}
		}

		logger.info("count of follow up for notification for pop is : " + finalData.size());

		return finalData;
	}

	@Override
	public void updateCallCountOfCallAndFollowUp(long id, String dealercode) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<String> getPendingCallByUserListForChart(String userlist, String dealercode, Date currentDate,
			Date addedDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getFollowUpCallsCountForReportPage(String userlist, String dealercode, Date currentDate,
			Date addedDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<CallSummary> getAllCallCountsOfCREs(List<String> crelist, String dealercode) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deletSchCall(Long id, String dealerDB) {
		// TODO Auto-generated method stub

	}

	public CallDispositionData getCallDispositionVariable(String callDisposition) {

		CallDispositionData call_data = source.callDispositionDatas(em)
				.where(u -> u.getDisposition().equals(callDisposition)).getOnlyValue();
		return call_data;

	}

	@Override
	public List<CallInteraction> getListOfFollowUpMissedByAgent(String cre, String dealercode) {

		List<CallInteraction> servicesNotFollowedUp = new ArrayList<CallInteraction>();
		List<CallInteraction> listOffollowUp = new ArrayList<CallInteraction>();
		// List<CallInteraction> listOffollowUp =
		// getListOfFollowUpOfTodayCRE(cre, dealercode);

		// logger.info("list of follow up of today : "+listOffollowUp.size());

		for (CallInteraction iterateFollowUp : listOffollowUp) {

			SimpleDateFormat formatingData = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String scheduledDateAndtime = iterateFollowUp.getSrdisposition().getFollowUpDate().toString() + " "
					+ iterateFollowUp.getSrdisposition().getFollowUpTime() + ":00";
			Date datetime = new Date();
			// logger.info("scheduledDateAndtime :"+scheduledDateAndtime);
			try {
				datetime = formatingData.parse(scheduledDateAndtime);
				// logger.info("datetime os setScheduledDateTime:"+datetime);

			} catch (ParseException e) {
				// logger.info("Could not parse date");
				e.printStackTrace();
			}

			if (datetime.before(currentDate())) {

				servicesNotFollowedUp.add(iterateFollowUp);

			}

		}
		return servicesNotFollowedUp;

	}

	private TaggingUsers getTaggedUserByType(int upSellId) {
		// logger.info("upSellType is : "+upSellType);

		return source.taggingUsers(em).where(u -> u.getUpsellLeadId() == upSellId).getOnlyValue();
	}

	@Override
	public List<String> getCREUserNameofCREManagerByLocation(String userLoginName, String userLogindealerCode,
			String cityName, long workshopName) {
		
		logger.info("cityname is : "+cityName);
		
		Location locId = source.locations(em).where(u -> u.getName().equals(cityName)).getOnlyValue();
		logger.info("location data:" + locId);
		long locationId = locId.getCityId();
		logger.info("location info:" + locationId);
		List<String> availableUserList = new ArrayList<String>();

		List<WyzUser> userList = source.wyzUsers(em).where(k -> k.getCreManager().equals(userLoginName))
				.where(k -> k.getRole().equals("CRE") || k.getRole().equals("Service Advisor"))
				.where(k -> k.getLocation().getCityId() == locationId).toList();

		if (userList.size() != 0) {

			for (WyzUser d : userList) {
				if (d.isUnAvailable() == false) {
					availableUserList.add(d.getUserName());
					// logger.info("users is :"+d.getUserName());
				}

			}

		}

		List<String> availableUsers = new ArrayList<String>();

		java.util.Date todayData = new java.sql.Date(System.currentTimeMillis());

		for (String userData : availableUserList) {

			WyzUser user = source.wyzUsers(em).where(u -> u.getUserName().equals(userData)).getOnlyValue();

			Hibernate.initialize(user.getUnAvailabilities());

			List<UnAvailability> unaviabiltylist = user.getUnAvailabilities();

			if (unaviabiltylist.size() != 0) {

				int i = 0;
				for (UnAvailability unavailable : unaviabiltylist) {

					if (unavailable.getFromDate().before(todayData) && unavailable.getToDate().after(todayData)) {

						i = i + 1;

					} else if (unavailable.getFromDate().toString().equals(todayData.toString())) {
						i = i + 1;

					} else if (unavailable.getToDate().toString().equals(todayData.toString())) {
						i = i + 1;

					}
				}
				if (i == 0) {
					availableUsers.add(user.getUserName());

				}

			} else {

				availableUsers.add(user.getUserName());

			}

		}
		return availableUsers;

	}

	@Override
	public List<Workshop> getWorkshopListById(long workshop_id) {

		return source.workshop(em).where(u -> u.getId() == workshop_id).toList();

	}

	@Override
	public HashMap<Long, String> getCREUserNameIdofCREManager(String username, String dealercode) {
		HashMap<Long, String> userDetails = new HashMap<Long, String>();
		List<Object[]> resultList = em
				.createQuery("select w.id,w.userName from WyzUser w where creManager=:a and role=:b")
				.setParameter("a", username).setParameter("b", "CRE").getResultList();
		// List<Object[]> resultList = source.wyzUsers(em).where(k ->
		// k.getCreManager().equals(username) && k.getRole().equals("CRE"))
		// .select(k ->new Pair<>(k.getId(),k.getUserName())).toList();
		for (Object[] result : resultList)
			userDetails.put((Long) result[0], (String) result[1]);
		// for(Pair<Long,String> userId: resultList){
		// userDetails.put(userId.getKey(),userId.getValue());
		// }
		return userDetails;
	}

	@Override
	public List<Complaint> getAllComplaintsOFVehicle(long vehicle_id) {

		return source.complaint(em).where(u -> u.getVehicle() != null)
				.where(u -> u.getVehicle().getVehicle_id() == vehicle_id).toList();

	}

	@Override
	public List<SMSTemplate> getMessageTemplatesForDisplay() {

		String complainttype = SMSTemplate.COMPLIANT;
		String servicebookedtype = SMSTemplate.SERVICE_BOOKED;
		String serviceduetype = SMSTemplate.SERVICE_DUE;
		logger.info("complainttype : " + complainttype);
		logger.info("servicebookedtype : " + servicebookedtype);
		logger.info("serviceduetype : " + serviceduetype);

		List<SMSTemplate> listSMS = source.smsTemplates(em).toList();

		List<SMSTemplate> filterdSMS = listSMS.stream().filter(p -> p.getSmsType() != null)
				.filter(distinctByKey(SMSTemplate::getSmsType)).filter(p -> !p.getSmsType().equals(complainttype))
				.filter(p -> !p.getSmsType().equals(servicebookedtype))
				.filter(p -> !p.getSmsType().equals(serviceduetype)).collect(Collectors.toList());

		// return source.smsTemplates(em)
		// .where(m -> m.getSmsType().equals(complainttype)
		// || m.getSmsType().equals(servicebookedtype)
		// || m.getSmsType().equals(serviceduetype))
		// .toList();

		return filterdSMS;

	}

	public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
		Set<Object> seen = ConcurrentHashMap.newKeySet();
		return t -> seen.add(keyExtractor.apply(t));
	}

	@Override
	public List<UpsellLead> getUpselLeadSelectedBySRDispo(long sr_int_id) {

		return source.upsellLeads(em).where(u -> u.getSrDisposition() != null)
				.where(u -> u.getSrDisposition().getId() == sr_int_id).toList();

	}

	@Override
	public List<SpecialOfferMaster> getActiveSpecialOffers() {

		List<SpecialOfferMaster> listOffer = source.specialOfferMasters(em).toList();

		return listOffer.stream().filter(p -> p.isActive() == (true)).collect(Collectors.toList());
	}

	@Override
	public Phone getPhoneById(long phoneNumberId) {

		return em.find(Phone.class, phoneNumberId);
	}

	@Override
	public String getLeadNameById(long leadId) {
		// TODO Auto-generated method stub

		int leadIdis = (int) leadId;

		return source.taggingUsers(em).where(u -> u.getUpsellLeadId() == leadIdis).select(u -> u.getUpsellType())
				.getOnlyValue();

	}

	@Override
	public void sendComplaintSMS(CallInteraction call_interaction) {
		// TODO Auto-generated method stub

		long intId = call_interaction.getId();

		Complaint complaint = source.complaint(em).where(u -> u.getCallInteraction().getId() == intId).getOnlyValue();

		String complaintType = SMSTemplate.COMPLIANT;
		String message = source.smsTemplates(em).where(m -> m.getSmsType().equals(complaintType))
				.select(m -> m.getSmsTemplate()).getOnlyValue();

		message = message.replace("(complaintnum)", complaint.getComplaintNumber());
		message = message.replace("(Registration number)", call_interaction.getVehicle().getVehicleRegNo());

		smsTriggerRepo.sendCustomSMS(call_interaction.getVehicle().getVehicle_id(), message, "COMPLAINT",
				call_interaction.getWyzUser());

	}

	public long getVehicleCountExisting(String vehicleReg) {

		return source.vehicle(em).where(u -> u.getVehicleRegNo() != null && u.getVehicleRegNo().equals(vehicleReg))
				.count();
	}

	@Override
	public List<TaggingUsers> listAllTaggingUsers() {
		// TODO Auto-generated method stub

		return source.taggingUsers(em).toList();
	}

	@Override
	public List<ComplaintSource> getComplaintSourceList() {
		// TODO Auto-generated method stub
		return source.complaintSource(em).toList();
	}

	@Override
	public List<ComplaintTypes> listAllComplaintTaggingUsers() {
		// TODO Auto-generated method stub
		return source.complaintTypes(em).toList();
	}

}
