package repositories;

import java.util.Date;
import java.util.HashMap;
import java.util.*;

import javax.inject.Named;

import models.UploadData;

@Named
public interface ExcelUploadRepo {
	  void insertExcelDataToDB (Object excelData,HashMap<String,String> formData, HashMap<Date, Integer> uploadTotalRecord, HashMap<Date, Integer> uploadRejectedRecord);

	void addTheUplodedExcelData(UploadData insertInUpload);

	List<String> getExcelTotalAndRejectedCounts(String upload_id);

	long getCampaignIdByCamp(String campName, String typeData);

	//void updateTherowCountOfExcelsheet(int row_count, long upload_id_concver);

	//  void insertJobCardDataToDB (Object excelData,HashMap formData);
	
	void insertDataByStoreProcedure(String uploadId);

	UploadData setRequiredFieldsForUploadInsurance(long userLoginId, String sheetname, 
			String locationdata, Long workshopId, String finalName, String finalUploadId);

}
