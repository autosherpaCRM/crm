package repositories;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.inject.Named;

import controllers.webmodels.CallInteractionHistory;
import models.CallDispositionData;
import play.mvc.Result;




@Named
public interface AllCallInteractionRepository {
	public byte[] getMediaFilefromDB(String calldate,String calltime) throws IOException;
	public List<String> getAllLocations();
	public List<CallDispositionData> getAllDispositions();
	public List<String> getAllCreNames(String userName);

	public List<CallInteractionHistory> getAllCallInteractionDetails(String userName, Date fromdate, Date todate,
			String filter_crename, String filter_disposition,String locations) throws ParseException;
	public List<String> getCRENAmeAndNumber(String calldate, String calltime);
	
	public byte[] getMediaFileMR(long callInteractionId) throws IOException;
	List<String> getAllUsers();
	public List<String> getAllWorkshops();
	public List<String> getWorkshopMulti(List<String> selectedworkshop);
	public List<String> getWorkshopListMulti(List<String> selectedLocations);
	public String getWorkshopIdsMulti(String workshopstring);
	public String getCREMulti(String creidsare);


}
