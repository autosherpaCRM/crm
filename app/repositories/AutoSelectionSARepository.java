package repositories;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.inject.Named;

import controllers.webmodels.ServiceAdvisorIdName;
import play.mvc.Result;

@Named
public interface AutoSelectionSARepository {
	
	public void getAllSaDetails(String dealercodeIs);
	
	public List<ServiceAdvisorIdName> getSaDetails(Date date, Long workshopId);

}
