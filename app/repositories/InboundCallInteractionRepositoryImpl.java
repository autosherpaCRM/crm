/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import configs.JinqSource;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import models.CallInteraction;
import models.Customer;
import models.Vehicle;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import play.Logger;

/**
 *
 * @author W-885
 */
@Repository("inboundCallInteractionRepository")
@Transactional
public class InboundCallInteractionRepositoryImpl implements InboundCallInteractionRepository {

	Logger.ALogger logger = play.Logger.of("application");
	@PersistenceContext
	private EntityManager em;

	@Autowired
	private JinqSource source;

	@Override
	public Customer getCustomerInforById(long customerid) {

		Customer customer = em.find(Customer.class, customerid);

		Hibernate.initialize(customer.getAddresses());
		Hibernate.initialize(customer.getPreferredAdress());
		Hibernate.initialize(customer.getPreferredEmail());
		Hibernate.initialize(customer.getPreferredPhone());
		Hibernate.initialize(customer.getPhones());
		Hibernate.initialize(customer.getSegment());
		Hibernate.initialize(customer.getVehicles());
		Hibernate.initialize(customer.getInsurances());
		Hibernate.initialize(customer.getOfficeAddressData());
		Hibernate.initialize(customer.getResidenceAddressData());
		Hibernate.initialize(customer.getPermanentAddressData());

		return customer;
	}

	@Override
	public Vehicle getVehicleInformationById(long vehicleid) {

		Vehicle vehicle = em.find(Vehicle.class, vehicleid);

		Hibernate.initialize(vehicle.getInsurances());
		Hibernate.initialize(vehicle.getServices());
		Hibernate.initialize(vehicle.getCustomer());
		Hibernate.initialize(vehicle.getCustomer().getAddresses());

		return vehicle;
	}

	@Override
	public CallInteraction getLatestInterOfVehicleById(long vehiId) {

		CallInteraction final_call_int = new CallInteraction();

		long call_id_count = source.callinteractions(em).where(u -> u.getVehicle() != null)
				.where(u -> u.getSrdisposition()!=null)
				.where(u -> u.getAssignedInteraction()!=null)
				.where(u -> u.getVehicle().getVehicle_id() == vehiId).count();

		logger.info(" vehiId is : " + vehiId + " call_id_count : " + call_id_count);

		if (call_id_count > 0) {
			
			/*List<Long> callids=source.callinteractions(em).where(u -> u.getVehicle() != null)
					.where(u -> u.getSrdisposition()!=null)
					.where(u -> u.getAssignedInteraction()!=null)
					.where(u -> u.getVehicle().getVehicle_id() == vehiId).select(u -> u.getId()).toList();
			
			for(long ca : callids){
				
				logger.info("call int ids are "+ca);
				
			}*/

			long call_id = source.callinteractions(em).where(u -> u.getVehicle() != null)
					.where(u -> u.getSrdisposition()!=null)
					.where(u -> u.getAssignedInteraction()!=null)
					.where(u -> u.getVehicle().getVehicle_id() == vehiId).max(u -> u.getId());

			logger.info(" vehiId is : " + vehiId + " call_id : " + call_id);

			if (call_id != 0) {

				final_call_int = source.callinteractions(em).where(u -> u.getId() == call_id).getOnlyValue();

				Hibernate.initialize(final_call_int.getSrdisposition());
				Hibernate.initialize(final_call_int.getServiceBooked());
				Hibernate.initialize(final_call_int.getVehicle());
				Hibernate.initialize(final_call_int.getVehicle().getCurrentInsurance());

				if (final_call_int.getServiceBooked() != null) {
					Hibernate.initialize(final_call_int.getServiceBooked().getWorkshop());
					Hibernate.initialize(final_call_int.getServiceBooked().getServiceAdvisor());
				}

				Hibernate.initialize(final_call_int.getVehicle());
				Hibernate.initialize(final_call_int.getVehicle().getCurrentInsurance());

				return final_call_int;

			} else {

				Hibernate.initialize(final_call_int.getVehicle());
				Hibernate.initialize(final_call_int.getVehicle().getCurrentInsurance());

				return final_call_int;
			}

		} else {

			return final_call_int;
		}

	}

}
