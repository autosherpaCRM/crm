package repositories;

import javax.inject.Named;

@Named
public interface SynchedKeySchCallsRepository {
	
	public boolean checkIfSynched(String key,String dealerDB);
	
	public void addToSynchedKeyList(String key,String dealerDB);

}
